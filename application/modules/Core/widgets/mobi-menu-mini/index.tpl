<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style type="text/css">

.core_mini_profile span img.thumb_icon, .core_mini_profile span img.item_nophoto {
  border: medium none;
    border-radius: 2px !important;
    height: 25px;
    margin-right: 4px;
    width: 25px;
  padding-right:0 !important;
}
#core_menu_mini_menu_mobile > div > ul > li > a{
     /* text-transform: uppercase;*/
}
.login .layout_core_mobi_menu_mini #core_menu_mini_menu_mobile ul.header_menu_class2 {
    float: left; !important;
    margin-right: 2%;
    margin-top: -7%;
    display: -webkit-box;
    margin-left: -2%;
}
.login .layout_core_mobi_menu_mini #core_menu_mini_menu_mobile > ul li.no-dloader .core_mini_auth {
    width: 25px;
    margin-left: 0px;
}
.login .layout_core_mobi_menu_mini #core_menu_mini_menu_mobile > ul li.no-dloader .core_mini_admin {
    width: 25px;
    margin-left: 0px;
}
.login .updates_pulldownM > a{background: transparent !important;}
.login .updates_pulldownM > a:hover{text-decoration:none !important;}
.login .updates_pulldownM > a, .updates_pulldownM_active > a{color: #959595 !important;
    font-family: robotoregular;    font-size: 12px;    font-weight: 700;    padding: 18px 0; text-transform: capitalize; position:relative;}

.updates_pulldownM > a, .updates_pulldownM_active > a{padding:0px !important; }
.updates_pulldownM_active > a{background:none !important;}

.updates_pulldownM_active {
    position: relative;
}
.notifications_menu_m .notification_type_post_page, .notifications .notification_type_post_page{background-image:url("images/commented-on-wall.png") !important; background-repeat:no-repeat; line-height: normal; background-position: left center;}
ul.notifications_menu_m > li{color: #666 !important; padding: 7px 10px !important;}
ul.notifications_menu_m > li > span {font-size: 11px !important; min-height: 20px !important; padding-left: 33px !important; padding-top: 0px !important;}
ul.notifications_menu_m > li > span > a{font-size: 12px !important; color: #232323;}
ul.notifications_menu_m {width: 100%;}
#core_menu_mini_menu_mobile_update > span.updates_pulldownM_active > div.pulldown_contents_wrapper
{
    display: block;
    /*-moz-box-shadow: 2px 3px 4px 0 rgba(1,1,1,.5);
    -webkit-box-shadow: 2px 3px 4px 0 rgba(1,1,1,.5);*/
    box-shadow: 2px 3px 4px 0 rgba(1,1,1,.5);
}
.header_menu_class2 > li {
    padding-left: 5%;
}
#Mopagenames > li > a
{
      color: #fb933c;
}
#core_menu_mini_menu_mobile > ul.privacy_options1 > div.accordion
{
  float: right;
  margin-right: 5%;
  margin-top: -4%;
}
/*#core_menu_mini_menu_mobile > ul.header_menu_class2 > li.no-dloader.menu_core_mini.core_mini_auth
{
  float: left;
  margin-left: -15%;
}*/
/*.login .layout_core_mobi_menu_mini #core_menu_mini_menu_mobile > ul li.no-dloader a.core_mini_auth {
    background-image: url(images/sign-out-header-icon.png);
    background-repeat: no-repeat;
    background-position: right center;
    text-align: right;
    padding: 15px 10px 4px;
    margin-right: -62%;
    float: right;
}

#core_menu_mini_menu_mobile ul li.core_mini_messages {
    background-image: none;
    background-repeat: no-repeat;
    margin-left: -15%;
    margin-top: 0%;
}
#core_menu_mini_menu_mobile > ul.header_menu_class2 > li.menu_core_mini.core_mini_profile > a.mobile_profile_icon.menu_core_mini.core_mini_profile {
    margin-top: 10%;
    margin-left: 0%;
    width: 100%;
}*/
</style>
<div id='core_menu_mini_menu_mobile' class="mobile_on">
  
  <ul class="tp_center_search_mobile">
  
   <?php if($this->search_check):?>
      <li id="global_search_form_container">
        <form id="global_search_form" action="<?php echo $this->url(array('module' => 'user', 'controller' => 'index' , 'action' => 'browse'), 'default', true) ?>" method="get">
          <input type='text' class='text suggested' name='displayname' id='global_search_fieldm' size='20' maxlength='100' alt='<?php echo $this->translate('Search') ?>' placeholder="Search"/>
          <button type="submit" id="submit" name="submit" class="top_serach">Search</button>
     </form>
      </li>
    <?php endif;?>
  </ul>

   <ul class="header_menu_class">
  
    <?php if( $this->viewer->getIdentity()) :?>
    <li id='core_menu_mini_menu_mobile_update'>
      <span onclick="toggleUpdatesPulldownM(event, this, '4');" style="display: inline-block;" class="updates_pulldownM<?php echo ($this->locale()->toNumber($this->notificationCount)>0) ? ' notif_update':'';?>">
        <div class="pulldown_contents_wrapper">
          <div class="pulldown_contents">
            <ul class="notifications_menu_m" id="notifications_menu_m">
              <div class="notifications_loading_m" id="notifications_loading_m">
                <img src='<?php echo $this->layout()->staticBaseUrl ?>application/modules/Core/externals/images/loading.gif' style='float:left; margin-right: 5px;' />
                <?php echo $this->translate("Loading ...") ?>
              </div>
            </ul>
          </div>
          <div class="pulldown_options">
            <?php echo $this->htmlLink(array('route' => 'default', 'module' => 'activity', 'controller' => 'notifications'),
               $this->translate('View All Updates'),
               array('id' => 'notifications_viewall_link')) ?>
          </div>
        </div>
        <a href="javascript:void(0);" id="updates_toggle" title="Updates" <?php if( $this->notificationCount ):?> class="new_updates"<?php endif;?>>
        <span <?php if( $this->notificationCount ){?> class="new_updates count_update"<?php }else{echo 'class="no_count"';}?>>
        <?php 
        echo ($this->locale()->toNumber($this->notificationCount)>100)?'99+':$this->locale()->toNumber($this->notificationCount);
        //echo $this->translate(array('%s', '%s', $this->notificationCount), $this->locale()->toNumber($this->notificationCount)) ?>
        </span>
        <span class="update_icon"></span>
        </a>
      </span>
    </li>
    <li id='core_menu_mini_menu_mobile_edit_profile'>
      <a href="<?php echo $this->baseUrl().'/members/edit/profile';?>" style="padding: 0px 6px !important;" title="Edit profile"><span class="edit_profile_icon"></span></a>
    </li>
  
    <?php endif; ?>
    </ul>
    <ul class="header_menu_class2">
    
    <?php foreach( $this->navigation as $item ):
    //echo $item->class;
     ?>
      <li class="<?php echo  !empty($item->class) ? $item->class : null;?>">
      <?php 
        if($item->class == 'menu_core_mini core_mini_auth'){
          echo $this->htmlLink('login', (($item->class == 'menu_core_mini core_mini_profile' ||  $item->class =='no-dloader menu_core_mini core_mini_auth')?'':$this->translate($item->getLabel())), array_filter(array(
            'class' => ( !empty($item->class) ? ($item->class == 'menu_core_mini core_mini_profile'?'mobile_profile_icon '.$item->class:$item->class) : null ),
            'alt' => ( !empty($item->alt) ? $item->alt : null ),
            'target' => ( !empty($item->target) ? $item->target : null ),
            'title' => ( !empty($item->class) ? (($item->class == 'menu_core_mini core_mini_profile'?'My Profile':($item->class == 'menu_core_mini core_mini_messages'?'Messages':($item->class == 'no-dloader menu_core_mini core_mini_authm'?'Settings':'')))) : null ),
          )));
        }else{
     
        echo $this->htmlLink(($item->class != 'menu_core_mini core_mini_profile' && $item->class != 'no-dloader menu_core_mini core_mini_auth')?$item->getHref():'javascript:void(0);', (($item->class == 'menu_core_mini core_mini_profile' ||  $item->class =='no-dloader menu_core_mini core_mini_auth')?'&nbsp;':$this->translate($item->getLabel())), array_filter(array(
            'class' => ( !empty($item->class) ? ($item->class == 'menu_core_mini core_mini_profile'?'mobile_profile_icon '.$item->class:$item->class) : null ),
            'alt' => ( !empty($item->alt) ? $item->alt : null ),
            'target' => ( !empty($item->target) ? $item->target : null ),
            'title' => ( !empty($item->class) ? (($item->class == 'menu_core_mini core_mini_profile'?'My Profile':($item->class == 'menu_core_mini core_mini_messages'?'Messages':($item->class == 'no-dloader menu_core_mini core_mini_authm'?'Settings':'')))) : null ),
          ))); 
         }
      ?>
     
     <?php if( $this->viewer->getIdentity()) :?>
        <ul style="display:none;" id="Mopagenames">
          <li>Use Crezilla As</li>
          <li class="active">
            <?php $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($this->viewer->getIdentity());
if($pic == '')$pic  = $this->baseUrl().'/application/modules/User/externals/images/nophoto_user_thumb_profile.png';
?><span class="muia_profile" style="background-image:url(<?php echo $pic;?>)"></span>
            <?php //echo $this->itemPhoto($this->viewer, "thumb.icon");?>
            <a href="<?php echo $this->viewer->getHref();?>">My Profile</a></li>
          <?php $userpages = Engine_Api::_()->getDbtable('pages', 'page')->getUserPages(Engine_Api::_()->user()->getViewer()->getIdentity());
      $k=1;
      if($userpages){
        foreach($userpages as $page){
                    if($k>10){
                          break;
                        }
                    $companypic = Engine_Api::_()->getDbtable('users', 'user')->getUserPageProfilePicPath($page['user_id'], $page['photo_id'], $page['page_id']);
            $companypic = ($companypic=='')?'application/modules/Page/externals/images/nophoto_page_thumb_profile.png':$companypic;
                    $p  = Engine_Api::_()->getItem('page', $page['page_id']);
                        ?>
            <li class="page_active_<?php echo $page['page_id'];?>">
                        <span class="muia_profile" style="background-image:url(<?php echo $companypic;?>)">
                        </span>
                        <a href="<?php echo $this->baseUrl().'/page/'.$page['url'];?>">
                          <?php echo $page['title'];?>
                        </a>
                        </li>
        <?php $k++;}
      }?>
            <?php if($k>6){?>
              <li class="see_all_pages  page_active_'.$page['page_id'].'">
                    <a href="<?php echo $this->baseUrl().'/manage-pages';?>" style="color: #34aee8; text-align:center;">
                        See All
                    </a>
                </li>
            <?php }?>
            </ul>
       <?php if($item->class == 'no-dloader menu_core_mini core_mini_auth') : 
       //echo $item->class;
       ?>
        
       <?php endif; ?>
    <?php endif; ?>
     
     
      </li>
    <?php endforeach; ?>
      <li class="core_mini_mobile_auth" id="auth_mobile">
    <a href="javascript:void(0);" class="core_mini_mobile_auth">&nbsp;</a>   
      </li>

  </ul>
   <?php if( $this->viewer->getIdentity()) :?>
   <ul class="privacy_options1" style="display:none;">
          <div class="accordion">
      <div class="accordion-section">
        <a class="accordion-section-title" href="#accordion-1">Privacy</a>
        <div id="accordion-1" class="accordion-section-content">
          <p>
                      <label>Who can message me?</label>
                      <span>
                        <select id="msg_setting" class="psetting">
                          <option value="1" <?php if($this->viewer->msg_setting == '1'){echo "selected='selected'";}?>>Everyone</option>
                            <option value="2" <?php if($this->viewer->msg_setting == '2'){echo "selected='selected'";}?>>My Friends</option>
                            <option value="3" <?php if($this->viewer->msg_setting == '3'){echo "selected='selected'";}?>>Only Me</option>
                        </select>
                      </span>
                    </p>
                    
                    <p>
                      <label>Who can see my friend list?</label>
                      <span>
                        <select id="friend_setting"  class="psetting">
                          <option value="1" <?php if($this->viewer->friend_setting == '1'){echo "selected='selected'";}?>>Everyone</option>
                            <option value="2" <?php if($this->viewer->friend_setting == '2'){echo "selected='selected'";}?>>My Friends</option>
                            <option value="3" <?php if($this->viewer->friend_setting == '3'){echo "selected='selected'";}?>>Only Me</option>
                        </select>
                      </span>
                    </p>
                    
                    <p style="display:none;">
                      <label>Calendar</label>
                      <span>
                        <select id="cal_setting"  class="psetting">
                          <option value="1" <?php if($this->viewer->cal_setting == '1'){echo "selected='selected'";}?>>Everyone</option>
                            <option value="2" <?php if($this->viewer->cal_setting == '2'){echo "selected='selected'";}?>>My Friends</option>
                            <option value="3" <?php if($this->viewer->cal_setting == '3'){echo "selected='selected'";}?>>Only Me</option>
                        </select>
                      </span>
                    </p>
        </div><!--end .accordion-section-content-->
      </div><!--end .accordion-section-->

      <div class="accordion-section">
        <a class="accordion-section-title" href="#accordion-2">Change Password</a>
        <div id="accordion-2" class="accordion-section-content">
          <div id="passsuccess" style=" color:#009900;display:none; font-weight:bold;">Your changes have been saved.</div>
                    <div id="passerror" style="color:#dc2324;display: none; font-weight: bold;">Password does not match.</div>
                    <?php echo $this->passform->render($this) ?>
        </div><!--end .accordion-section-content-->
      </div>
            
      <div class="accord_footer">
      <div class="accordion-section">
        <div class="accord" style="padding-bottom:6px;">
                 <a class="accordion-content" href="<?php echo $this->baseUrl().'/logout';?>">Sign Out</a>
                </div>
      </div><!--end .accordion-section-->
    </div>
    </div>
    
        </ul>
      <?php endif; ?>
</div>
<!-- toggle_dropdown gr_bg-->

<script type='text/javascript'>

jQuery("#oldPassword1, #password1, #passwordConfirm1").keypress(function(e) {
   if(e.which == 13 || e.which == 1 ) {
      validatechangepass();
   }
});

function validatechangepass(){
 
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
if(jQuery.trim(jQuery("#oldPassword1").val()) == ''){
    jQuery("#oldPassword1").focus();
    jQuery("#oldPassword1").addClass("com_form_error_validatin");
    return false;
}
if(jQuery.trim(jQuery("#password1").val()) == ''){
    jQuery("#password1").focus();
    jQuery("#password1").addClass("com_form_error_validatin");
    return false;
}
if(jQuery.trim(jQuery("#passwordConfirm1").val()) == ''){
    jQuery("#passwordConfirm1").focus();
    jQuery("#passwordConfirm1").addClass("com_form_error_validatin");
    return false;
}
if(jQuery.trim(jQuery("#passwordConfirm1").val()) != jQuery.trim(jQuery("#password1").val())){
    jQuery("#passerror").show();
    jQuery("#passerror").html("Password does not match.");
    jQuery("#passerror").fadeOut(7000);
    jQuery("#passwordConfirm1").focus();
    jQuery("#passwordConfirm1").addClass("com_form_error_validatin");
    return false;
}

jQuery.post("<?php echo $this->baseUrl();?>/members/edit/chkoldpass",{oldPassword:jQuery.trim(jQuery("#oldPassword1").val()),password:jQuery.trim(jQuery("#password1").val()),passwordConfirm:jQuery.trim(jQuery("#passwordConfirm1").val())}, function(resp){
  if(!jQuery.trim(resp)){
    jQuery("#user_form_changepass")[0].reset();
    jQuery("#passsuccess").show();
    jQuery("#passsuccess").html("Password changed successfully.");
    jQuery("#passsuccess").fadeOut(7000);
    return false;
  }else{
    jQuery("#passerror").show();
    jQuery("#passerror").html("Old password did not match.");
    jQuery("#passerror").fadeOut(7000);
    return false;
  }
  //$('#signupform').submit();
});
  
}

jQuery(document).on('click','.mobile_profile_icon', function(){
  //alert('hello');
  jQuery('#Mopagenames').slideToggle(); 
});
  var notificationUpdater;
  
  en4.core.runonce.add(function(){

  new Autocompleter.Request.JSON('global_search_fieldm', '<?php echo $this->url(array('module' => 'user', 'controller' => 'public', 'action' => 'suggest','message' => true), 'default', true) ?>', { 
        'minLength': 1,
        'delay' : 150,
        'selectMode': 'pick',
        'autocompleteType': 'tag',
        'multiple': false,
        'className': 'message-autosuggest',
        'filterSubset' : true,

        'tokenFormat' : 'object',
        'tokenValueKey' : 'label',
    'keycommand':'continue',
        'injectChoice': function(token){
          if(token.type == 'user'){
            var choice = new Element('li', {
              'class': 'autocompleter-choices',
              'id':token.label
            });
            new Element('div', {
              <!--'html': '<a href="'+token.url+'">'+this.markQueryValue(token.photo)+this.markQueryValue(token.label)+'</a>',-->
        'html': '<a href="'+token.url+'">'+token.photo+this.markQueryValue(token.label)+'</a>',
              'class': 'autocompleter-choice'
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
          else {
            var choice = new Element('li', {
              'class': 'autocompleter-choices friendlist',
              'id':token.label
            });
                new Element('div', {
              'html': this.markQueryValue(token.label),
              'class': 'autocompleter-choice'
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
            
        },
        onPush : function(){
         if( $('toValues').value.split(',').length >= maxRecipients ){
            $('to').disabled = true;
          }
        }
    });
        

    if($('notifications_markread_link')){
      $('notifications_markread_link').addEvent('click', function() {
        //$('notifications_markread').setStyle('display', 'none');
        en4.activity.hideNotifications('<?php echo $this->string()->escapeJavascript($this->translate("0"));?>');
      });
    }

    <?php if ($this->updateSettings && $this->viewer->getIdentity()): ?>
    notificationUpdater = new NotificationUpdateHandler({
              'delay' : <?php echo $this->updateSettings;?>
            });
    notificationUpdater.start();
    window._notificationUpdater = notificationUpdater;
    <?php endif;?>
  });


  var toggleUpdatesPulldownM = function(event, element, user_id) {
    console.log('event'+ event);
    
    if( element.className=='updates_pulldownM' || element.className=='updates_pulldownM notif_update') {
      console.log('element'+element.className);
      element.className= 'updates_pulldownM_active';
      jQuery('#Mopagenames').css('display','none');
      showNotificationsM();
    } else {
      element.className='updates_pulldownM';
    }
  }

  var showNotificationsM = function() {
    en4.activity.updateNotifications();
    new Request.HTML({
      'url' : en4.core.baseUrl + 'activity/notifications/pulldown',
      'data' : {
        'format' : 'html',
        'page' : 1
      },
      'onComplete' : function(responseTree, responseElements, responseHTML, responseJavaScript) {
        if(responseHTML) {
          // hide loading icon
          if($('notifications_loading_m')){
            console.log('get');
           $('notifications_loading_m').setStyle('display', 'none');
          }
          console.log('response'+responseHTML);
          $('notifications_menu_m').innerHTML = responseHTML;
          $('notifications_menu_m').addEvent('click', function(event){
            event.stop(); //Prevents the browser from following the link.

            var current_link = event.target;
            var notification_li = $(current_link).getParent('li');

            // if this is true, then the user clicked on the li element itself
            if( notification_li.id == 'core_menu_mini_menu_mobile_update' ) {
              notification_li = current_link;
            }

            var forward_link;
            if( current_link.get('href') ) {
              forward_link = current_link.get('href');
            } else{
              forward_link = $(current_link).getElements('a:last-child').get('href');
            }

            if( notification_li.get('class') == 'notifications_unread' ){
              notification_li.removeClass('notifications_unread');
              en4.core.request.send(new Request.JSON({
                url : en4.core.baseUrl + 'activity/notifications/markread',
                data : {
                  format     : 'json',
                  'actionid' : notification_li.get('value')
                },
                onSuccess : function() {
                  window.location = forward_link;
                }
              }));
            } else {
              window.location = forward_link;
            }
          });
        } else {
          $('notifications_loading_m').innerHTML = '<?php echo $this->string()->escapeJavascript($this->translate("You have no new updates."));?>';
        }
      }
    }).send();
  };
  
/*jQuery('#core_menu_mini_menu_mobile > ul.header_menu_class2 > li.no-dloader.menu_core_mini.core_mini_auth > a.no-dloader.menu_core_mini.core_mini_auth').on('click', function(){
  console.log('click');
  //jQuery("#user_form_changepass")[0].reset();
  if(jQuery('#core_menu_mini_menu_mobile > ul.header_menu_class2 > li.no-dloader.menu_core_mini.core_mini_auth > ul.privacy_options1').css("display") == "none"){
    console.log('if');
    jQuery('#core_menu_mini_menu_mobile > ul.header_menu_class2 > li.no-dloader.menu_core_mini.core_mini_auth > ul.privacy_options1').css("display", "block");
  }else{
    console.log('else');
    jQuery('#core_menu_mini_menu_mobile > ul.header_menu_class2 > li.no-dloader.menu_core_mini.core_mini_auth > ul.privacy_options1').css("display", "none");
  } 
  });*/


</script>

 <?php $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($this->viewer->getIdentity());
 if($pic == ''){
 ?>
<script type="text/javascript">
        jQuery(document).ready(function(){
        var data  = '<img src="'+jQuery('#pic').val()+'"/>';
        var img   = "<span><img src='<?php echo $this->baseUrl().'/public/custom/images/profile_avtar_icon.png'?>'><img src='<?php echo $this->baseUrl().'/public/custom/images/profile-dd-arrow.png'?>'></span>";
        jQuery('.mobile_profile_icon').prepend(img);  
      });
    </script>
    
    
<?php }else{?>
  <script type="text/javascript">
        jQuery(document).ready(function(){
        var data  = '<img src="'+jQuery('#pic').val()+'"/>';
        var icon_msg  = "<img src='<?php echo $this->baseUrl().'/public/custom/images/profile-dd-arrow.png'?>";
        jQuery('.mobile_profile_icon').prepend('<?php echo "<span>".$this->itemPhoto($this->viewer(), 'thumb.icon')."</span>" ;?>'); 
        var img   = "<img src='<?php echo $this->baseUrl().'/public/custom/images/profile-dd-arrow.png'?>'>";
        jQuery('.mobile_profile_icon span').append(img);
      });
    </script>
<?php } ?>



<?php 
  $url  = $_SERVER['REQUEST_URI'];
    $newurl = explode('/', $url);
    $furl = $newurl[count($newurl)-1];
    $surl = $newurl[count($newurl)-2];
?>

<script type="text/javascript">

jQuery(document).ready(function(){
  console.log('click');
jQuery('#auth_mobile').click(function(){
  jQuery('#Mopagenames').css('display','none');
  console.log('click');
  if(jQuery('#core_menu_mini_menu_mobile > ul.privacy_options1').css("display") == "none"){
    jQuery('#core_menu_mini_menu_mobile > ul.privacy_options1').css("display", "block");
  }else{
    jQuery('#core_menu_mini_menu_mobile > ul.privacy_options1').css("display", "none");
  } 
});


  var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
if( isMobile.any() )
{
  //alert('Mobile');
    jQuery(".heiglights-section2").css('display','none');
    jQuery(".heiglights-section3").css('display','block');
    //jQuery(".mobile_on").css('display','none');
    //jQuery(".mobile_on").css('display','block');
    jQuery(".mobile_on" ).insertBefore( "div.generic_layout_container layout_middle" );
    //$( "div#global_wrapper" ).insertBefore( ".mobile_on" );
}
else
{
    jQuery(".heiglights-section2").css('display','block');
    jQuery(".heiglights-section3").css('display','none');
    jQuery(".mobile_on").css('display','none');
} 
//if( isMobile.iOS() ) alert('iOS');
  /////////
    //jQuery(".generic_layout_container.layout_classified_home_page_jobs").hide();
    jQuery("#global_content > div.headline > div.tabs").hide();
    // PRIVACY SETTINGS
    var user_id= "<?php echo $this->viewer()->getIdentity();?>";
    console.log('user_id'+user_id);
    if(user_id == 1)
    {}
    else
    {
      jQuery("a.menu_core_main.custom_1021").closest("li").hide();;
    }
    jQuery('.psetting').on('change', function(){
      var type  =   jQuery(this).attr('id');
      var value = jQuery(this).val();
      jQuery.post('<?php echo $this->baseUrl();?>/members/edit/messagesettings',{setting:value, type:type}, function(resp){
        jQuery('#core_menu_mini_menu_mobile > ul.privacy_options1').css("display", "none");
      }); 
    });
  
    /*var firsturlpart= '<?php echo $furl; ?>';

    var securlpart  = '<?php echo $surl; ?>'; 
    if(firsturlpart == 'home'){
      jQuery('#core_menu_mini_menu_mobile li a.core_main_home').addClass('active');
    }else if(firsturlpart ==  'search' && securlpart == 'job'){
      jQuery('#core_menu_mini_menu_mobile li a.custom_259').addClass('active');
    }else if(firsturlpart ==  'casting'){
      jQuery('#core_menu_mini_menu_mobile li a.custom_258').addClass('active');
    }else if(firsturlpart ==  'services'){
      jQuery('#core_menu_mini_menu_mobile li a.custom_262').addClass('active');
    }else if((firsturlpart  ==  'manage-pages') || (firsturlpart  ==  'create-pages') || (firsturlpart  ==  'events' && securlpart == 'pages') || (firsturlpart ==  'manage' && securlpart == 'classifieds') || (firsturlpart ==  'job' && securlpart == 'user') || (firsturlpart ==  'parent' && securlpart == 'index')){
      jQuery('#core_menu_mini_menu_mobile li a.custom_831').addClass('active');
    }else if((firsturlpart  ==  'create' && securlpart == 'classifieds') || (firsturlpart ==  'packpage' && securlpart =='ads') || (firsturlpart  ==  'postclassified') || (firsturlpart  ==  'premiumsearch') || (firsturlpart ==  'notices')){
      jQuery('#core_menu_mini_menu_mobile li a.custom_861').addClass('active');
    }
*/
    
 });

/*jQuery("#tog_button").click(function(){
  if(jQuery(".toggle_dropdown").css('display')=='none'){
   jQuery(".toggle_dropdown").slideDown();
  }else{
   jQuery(".toggle_dropdown").slideUp();
  }
 });*/
 
jQuery(document).mouseup(function (e)
{
  /*  var container = jQuery("#pagenames");
  var maincontainer = jQuery(".core_mini_profile");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0 && !maincontainer.is(e.target) // if the target of the click isn't the container...
        && maincontainer.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
  */
  // CLOSE UPDATES DROPDOWN
  var updatecontainer = jQuery(".updates_pulldownM_active");

    if (!updatecontainer.is(e.target) // if the target of the click isn't the container...
        && updatecontainer.has(e.target).length === 0) // ... nor a descendant of the container
    {
        updatecontainer.addClass('updates_pulldownM');
    updatecontainer.removeClass('updates_pulldownM_active');
    }
  
 /* // CLOSE MENU ON MY PROFILE
  var menucontainer = jQuery(".more_tab");

    if (!menucontainer.is(e.target) // if the target of the click isn't the container...
        && menucontainer.has(e.target).length === 0) // ... nor a descendant of the container
    { 
     menucontainer.addClass('tab_closed');
     menucontainer.removeClass('tab_open');
    }
  */
  // CLOSE MENU ON SETTINGS
  var menucontainer = jQuery("#auth_mobile");

    if (!menucontainer.is(e.target) // if the target of the click isn't the container...
        && menucontainer.has(e.target).length === 0) // ... nor a descendant of the container
    { 
     jQuery('#core_menu_mini_menu_mobile > ul.privacy_options1').css("display", "none");
    }
});

/*function showsignup(){
  var offset = jQuery('#why_join').offset();
  //jQuery(window).scrollTop(offset.top);
  jQuery('html, body').animate({scrollTop: offset.top}, 800);
}*/
 
</script>
