<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>


<?php /*?><?php echo $this->translate('Copyright &copy;%s', date('Y')) ?>
<?php foreach( $this->navigation as $item ):
  $attribs = array_diff_key(array_filter($item->toArray()), array_flip(array(
    'reset_params', 'route', 'module', 'controller', 'action', 'type',
    'visible', 'label', 'href'
  )));
  ?>
  <?php echo $this->htmlLink($item->getHref(), $this->translate($item->getLabel()), $attribs) ?>
<?php endforeach; ?><?php */?>

<?php if( 1 !== count($this->languageNameList) ): ?>
    <form method="post" action="<?php echo $this->url(array('controller' => 'utility', 'action' => 'locale'), 'default', true) ?>" style="display:none;">
      <?php $selectedLanguage = $this->translate()->getLocale() ?>
      <?php echo $this->formSelect('language', $selectedLanguage, array('onchange' => '$(this).getParent(\'form\').submit();'), $this->languageNameList) ?>
      <?php echo $this->formHidden('return', $this->url()) ?>
    </form>
<?php endif; ?>

<?php if( !empty($this->affiliateCode) ): ?>
  <div class="affiliate_banner">
    <?php 
      echo $this->translate('Powered by %1$s', 
        $this->htmlLink('http://www.socialengine.com/?source=v4&aff=' . urlencode($this->affiliateCode), 
        $this->translate('SocialEngine Community Software'),
        array('target' => '_blank')))
    ?>
  </div>
<?php endif; ?>

<footer class="footer-bg">
			<div class="containerssdsds">
				<div class="row-fliud">
				<h3 class="footer-title">Quick Links</h3>
						
			
            <div class="menu-footer">
				<div class="col-md-6 col-sm-8 col-xs-12">
                <ul class="footer-nav">
					
                    
                    
                   
                    
                    
					<li class=""><a href="<?php echo $this->baseUrl().'/';?>">Home</a></li>
          <li class=""><a href="<?php echo $this->baseUrl().'/cms/about';?>">About Us</a></li>
                     <li class=""><a href="<?php echo $this->baseUrl().'/ads/campaigns'; ?>">Advertise</a></li>
                      <li class=""><a href="<?php echo $this->baseUrl().'/cms/privacy';?>">Contact Us</a></li>
                       <li class=""><a href="<?php echo $this->baseUrl().'/cms/faq';?>">FAQ's</a></li>
                       <li class=""><a href="<?php echo $this->baseUrl().'/help/privacy';?>">Privacy</a></li>
                       <li class="" ><a href="<?php echo $this->baseUrl().'/help/terms';?>">Term of services</a></li>
                        <!--<li class=""><a href="<?php echo $this->baseUrl().'/cms/help';?>">SITEMAP</a></li>-->
                        
                        
					<!--<li class=""><a href="<?php echo $this->baseUrl().'/cms/privacy';?>">Privacy</a></li>
					<li class=""><a href="<?php echo $this->baseUrl().'/cms/faq';?>">FAQ</a></li>
					<li class=""><a href="<?php echo $this->baseUrl().'/cms/help';?>">Help</a></li>
					<li class="" ><a href="<?php echo $this->baseUrl().'/cms/terms';?>">Term of services</a></li>-->
				</ul>
                </div>
                
                
                	<div class="col-md-6 col-sm-4 col-xs-12 pull-right">
                    
                     <ul class="footer-social">
			<li><a href="https://www.facebook.com/Crezilla-1256684177699324/" target="_blank"><img src="<?php echo $this->baseUrl().'/public/custom/images/fb.png'  ?>"></a></li>
            <li><a href=" https://www.instagram.com/crezilla_community/" target="_blank"><img src="<?php echo $this->baseUrl().'/public/custom/images/instagram.png'  ?>"></a></li>
            <li><a href="https://twitter.com/crezilla" target="_blank"><img src="<?php echo $this->baseUrl().'/public/custom/images/twitter.png'  ?>"></a></li>
                    <li><a href="#" target="_blank"><img src="<?php echo $this->baseUrl().'/public/custom/images/linked.png'  ?>"></a></li>

                    
				
					
				</ul>
                
                    </div>
                
                
                
                
			</div>					
					
				</div>
				<div class="col-md-3 col-md-offset-1 only_ft_p">
				

					<aside role="complementary" class="fatfooter">

							<div class="widget-container widget_footer_plugin" id="footer_plugin-2">                
							<h3 class="footer-title">Our office address</h3>
                           <p style="" class="footer-text">F313C, Third floor, Lado Sarai, New Delhi (110030) India.</p>
                          <p><strong style="font-weight: normal; color:#fff;">Mob. : 011.2952.1169</strong></p>
                        <p><strong style="font-weight: normal; color:#fff;">Mail us : 
				<a href="mailto:">info@crezilla.com</a></strong></p>
                     <p><strong style="font-weight: normal; color:#fff;">Website : <a href="" target="_blank">www.crezilla.com</a></strong></p>

           </div>
                    </aside>

				</div>
				
				<div class="col-md-4 col-md-offset-1 footer_inputs">
					<h3 class="footer-title">Quick reach</h3>
                    <?php echo $this->content()->renderWidget('core.contact') ?>
						
</div> 
	</div>
		  
				

		</footer>
		
		<!--<script src="<?php echo $this->baseUrl().'/';?>public/js/scrollReveal.js"></script>-->
	