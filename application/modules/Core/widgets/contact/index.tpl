<style type="text/css">
#contactmessage-label{display:none;}
.alert-success, .alert-danger {
    padding: 5px !important;
}
</style>
  <div class="alert alert-success" style="display:none;" id="contactmsg"></div>
  <div class="alert alert-danger" style="display:none;" id="contacterror"></div>
  <?php echo $this->form->render($this) ?>
  
  
  
<script type="text/javascript">
jQuery(document).ready(function(){
	
	var user_id='<?php echo $this->pagevalues;?>';
	if(user_id!=0)
	{
		var name_contact='<?php echo $this->name_contact;?>';
		var email_contact='<?php echo $this->email_contact;?>';
 		
		jQuery("#user_contact_from [name='name']").val(name_contact);
		jQuery("#user_contact_from [name='email']").val(email_contact);
 	}
 });

/*jQuery('div.layout_user_home_photo').each(function() {
      var newDiv = jQuery('<div/>').addClass('comb_dat');
	  jQuery(this).before(newDiv);
	  var next = jQuery(this).next();
	  newDiv.append(this).append(next);
});*/

jQuery(document).on('keypress', '.contactname, .contactemail, .contactbody', function(e){
	 if(e.which == 13 || e.which == 1 ) {
      validatecontact();
   }
});

  function validatecontact(){
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery(".contactname").val()) == ''){
			jQuery(".contactname").focus();
			jQuery(".contactname").addClass("com_form_error_validatin");
			return false;
	}
	if(jQuery.trim(jQuery(".contactemail").val()) == ''){
			jQuery(".contactemail").focus();
			jQuery(".contactemail").addClass("com_form_error_validatin");
			return false;
	}
	if(regex.test(jQuery.trim(jQuery(".contactemail").val())) == false){
 		jQuery(".contactemail").focus();
		jQuery(".contactemail").val('');
		jQuery(".contactemail").addClass("com_form_error_validatin");
		return false;
  	 }
	if(jQuery.trim(jQuery(".contactbody").val()) == ''){
			jQuery(".contactbody").focus();
			jQuery(".contactbody").addClass("com_form_error_validatin");
			return false;
	}
	//$('#user_contact_from').submit();
	jQuery.post("<?php echo $this->baseUrl();?>/help/contact",{email:jQuery.trim(jQuery(".contactemail").val()), name:jQuery.trim(jQuery(".contactname").val()), body:jQuery.trim(jQuery(".contactbody").val())}, function(resp){
 		if(jQuery.trim(resp)==""){
			jQuery(".contactbody").val('');
 			jQuery("#contactmsg").show();
 			jQuery("#contactmsg").html("Thankyou for Contacting Us.");
  			jQuery("#contactmsg").fadeOut(10000);
 			//setTimeout(close_panel, 4000);
 			return false;
		}else{
			jQuery("#contacterror").show();
			jQuery("#contacterror").html(resp);
			jQuery("#contacterror").fadeOut(10000);
			return false;
		}
		//$('#signupform').submit();
	});
	  
  }
</script>
<style>
#header {
    background-color: #398bcc;
    border: 1px solid #d8d8d8;
    border-radius: 5px 0 0;
    box-shadow: 4px 4px 8px #9a9a9a;
    height: 315px;
    margin-left: 40px;
    padding: 20px 40px;
    position: absolute;
    right: 0;
    width: 343px;
    z-index: 9;
}
.alert {
    border: 1px solid transparent;
    border-radius: 4px;
    margin-bottom: 10px;
    padding: 15px;
}

</style>
 