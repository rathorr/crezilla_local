<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Core_Widget_ContactController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	 
	$viewer = Engine_Api::_()->user()->getViewer();
 	$this->view->pagevalues	=	$viewer->getIdentity();
	 
 		$file = APPLICATION_PATH . '/application/settings/database.php';
		$options = include $file;
		$db = Zend_Db::factory($options['adapter'], $options['params']);
		$select = new Zend_Db_Select($db);
		$db->getConnection();
		$query2 = "select email,displayname
				  from engine4_users u 
 		 		  where u.user_id in (".$viewer->getIdentity().")";
		   $stmt = $db->query($query2);
           $result2 = $stmt->fetchAll();
 		   if(count($result2)>0)
		   {

			$this->view->email_contact	=	$result2[0]['email'];
			$this->view->name_contact	=	$result2[0]['displayname']; 
		   }
	 
    $this->view->form = $form = new Core_Form_Contact();
    $form->setAction(Zend_Controller_Front::getInstance()
      ->getRouter()->assemble(array(
        'module' => 'core',
        'controller' => 'help',
        'action' => 'contact',
      ), 'default', true))
      ;
  }
}