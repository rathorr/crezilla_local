<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9831 2012-11-27 20:42:43Z jung $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Core_Widget_AdminMenuMainController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	$viewer	=	Engine_Api::_()->user()->getViewer();
    $this->view->navigation = Engine_Api::_()
      ->getApi('menus', 'core')
      ->getNavigation('core_admin_main');

    foreach ($this->view->navigation->getPages() as $key => $page)
    {
      // Code to hide Wibiya from admin panel without removing it from DB
      // ****** START CODE ******
      if ('Settings' == $page->getLabel() )
      {
        foreach($page->getPages() as $page)
        {//echo $page->getClass().'<br>';
          if('menu_core_admin_main_settings core_admin_main_wibiya' == $page->getClass())
          {
            $page->setVisible(false);
          }
		  
		  if(('menu_core_admin_main_settings core_admin_main_settings_locale' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_signup' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_janrain' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_settings_activity' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_settings_spam' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_settings_friends' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_settings_mailsettings' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_settings_performance' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_settings_tasks' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_settings_password' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_settings_notifications' == $page->getClass() || 'menu_core_admin_main_settings core_admin_main_settings_storage' == $page->getClass()) && $viewer->level_id == 2)
          {
            $page->setVisible(false);
          }
        }
      }
      // ****** END CODE ******
      //echo $page->getLabel();
      if ('Plugins' == $page->getLabel() && 0 == count($page->getPages()))
      {
        $page->setVisible(false);        
      }
	  /*CUSTOM CODE TO HIDE MENUS*/
	  /*if ('Plugins' == $page->getLabel() && $viewer->level_id==2)
      {
        $page->setVisible(false);        
      }*/
	  
	  if ('Layout' == $page->getLabel() && $viewer->level_id==2)
      {
        $page->setVisible(false);        
      }
	  
	  if ('Ads' == $page->getLabel() && $viewer->level_id==2)
      {
        $page->setVisible(false);        
      }
	  
	  if ('Billing' == $page->getLabel() && $viewer->level_id==2)
      {
        $page->setVisible(false);        
      }
	  
	  if ('Stats' == $page->getLabel())
      {
		foreach($page->getPages() as $page)
        {//echo $page->getClass().'<br>';
        if(('menu_core_admin_main_stats core_admin_main_stats_logs' == $page->getClass() || 'menu_core_admin_main_stats core_admin_main_stats_resources' == $page->getClass() || 'menu_core_admin_main_stats core_admin_main_stats_url' == $page->getClass()) && $viewer->level_id == 2)
          {
            $page->setVisible(false);
          }
		}
      }
	  
	  
	  if ('Manage' == $page->getLabel() )
      {
        foreach($page->getPages() as $page)
        {//echo $page->getClass().'<br>';
          if(('menu_core_admin_main_manage core_admin_main_manage_levels' == $page->getClass() || 'menu_core_admin_main_manage core_admin_main_manage_networks' == $page->getClass() || 'menu_core_admin_main_manage core_admin_main_manage_reports' == $page->getClass() || 'menu_core_admin_main_manage core_admin_main_manage_packages' == $page->getClass() || 'menu_core_admin_main_manage core_admin_main_manage_announcements' == $page->getClass()) && $viewer->level_id == 2)
          {
            $page->setVisible(false);
          }
        }
      }
	  
    }

  }
}