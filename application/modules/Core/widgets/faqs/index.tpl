<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: privacy.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Alex
 */
?>
<style>
	.privacy_statement
	{
		margin: 5px 5px 0px 10px;
		width: 90%;
    	padding-left: 30px;
	}
	.privacy_statement span
	{

    font-size: 16px;
    font-weight: bold;
	}
	p
	{
		margin: 0 10px 10px;
    	font-size: 15px;
	}
</style>
<!-- <h2><?php echo $this->translate("FAQ'S") ?></h2>
 -->
 <div class="privacy_statement">

<span>Q. What is Crezilla?</span>
<p>A.  Crezilla is a global creative network built on a web and mobile platform, that helps professional and amateur content creators discover fabulous on and off  screen talent, the smartest professionals, and the best service providers, allowing them to put together productions of any scale or budget quickly and easily. Crezilla is also a great platform to promote yourself and your work. Its portfolio tools allow you to showcase your work to the global creative community. </p>
<span>Q. How do I use Crezilla?</span>
<p>A.  In order to use Crezilla, you need to create an account. However while the service is in “Alpha” mode, you need an invitation code in order to create an account. We will open the service to all users very soon. </p>

<span>Q. Is Crezilla available as a mobile app?</span>
<p>A. At present Crezilla is only available as a web platform. However, the site has been designed to work across mobile devices. An IOS and Android App version of the service is being developed. </p>

<span>Q.  How can I advertise on Crezilla?</span>
<p>A. Crezilla has an advanced Advertising engine that allows users to create their own advertisements, set advertising rates and target specific groups by geography, gender, age group and professional attributes. For example, our engine allows you to create an advertisement that will only go out to Male, Animators in Canada over the age of 25!  Or you could narrowly target all Writers in Hindi who live in Mumbai! Our advertising engine is currently in development and will be launched in the next few weeks.  </p>

<span>Q. How old do I need to be to open an Account? </span>
<p>A. You need to be at least 13 years old to open a Crezilla Account. If you’re younger than 13, then you need to get your legal guardian to open your account for you. </p>

<span>Q. What’s the difference between a regular account and a Page? </span>
<p>A.  Think of a Page as an account that’s created to promote a person, place or thing. 
Anyone who has a Crezilla account, can create a Page. At present the only category of Pages we encourage are those for Local and International Businesses. For example, an independent production company. Or an advertising agency. Or any kind of business. In the near future we plan to open many new categories of Pages - Fans, Alumni, Technology etc.  Organizations can use their Pages to post jobs or reach out to clients, customers and everyone else interested in the company.
</p>

<span>Q. Is my Crezilla account linked to other social networks like Facebook?</span>
<p>A. At present Crezilla accounts are not linked to other social networks. However, we are working on providing this functionality to our users. </p>

<span>Q. How will Crezilla be rolled out?</span>
<p>A. Crezilla will be rolled out in phases. Initially we will launch the site with limited functionality – social, portfolio tools, pages, calendar, messaging and manage invitations. Later we will launch Job Searches, Advertising and a host of other interesting features. These will be launched in phase.</p>

</div>