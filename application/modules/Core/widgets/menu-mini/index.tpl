<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style type="text/css">

.core_mini_profile span img.thumb_icon, .core_mini_profile span img.item_nophoto {
  border: medium none;
    border-radius: 2px !important;
    height: 25px;
    margin-right: 4px;
    width: 25px;
  padding-right:0 !important;
}
#core_menu_mini_menu > div > ul > li > a{
     /* text-transform: uppercase;*/
}
#core_menu_mini_menu_edit_profile > a
{
  padding: 10px 6px 0px 6px !important;
}
#core_menu_mini_menu > ul.header_menu_class > li.menu_core_mini.core_mini_messages
{
  margin-left: 0px;
  margin-top: 8px;
}
</style>



<div class="device_on"> 
  <a class="tog_btn" id="tog_button" href="javascript:;">
   <!-- <img src="http://stage2.famcominc.com/careyguides/www/images/responsive_menu.png"> -->
   <img src="<?php echo $this->baseUrl().'/public/custom/images/responsive_menu.png'?>">
  </a>
 </div>
 
 
<input type="hidden" id="pic" value="<?php echo $this->baseUrl().'/'.$pic;?>" />





<div id='core_menu_mini_menu' class="web_on">

  <?php
    // Reverse the navigation order (they're floating right)
    $count = count($this->navigation);
    foreach( $this->navigation->getPages() as $item ) $item->setOrder(--$count);
  ?>
  
  <div class="main_menu">
    <?php
    if( $this->viewer()->getIdentity() ):
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation2)
        ->setPartial(null)
        ->setUlClass('navigation')
        ->render();
    endif;
    ?>
    
    </div>
  
  
  
  <ul class="header_menu_class">
  
    <?php if( $this->viewer->getIdentity()) :?>
    <li id='core_menu_mini_menu_update'>
      <span onclick="toggleUpdatesPulldown(event, this, '4');" style="display: inline-block;" class="updates_pulldown<?php echo ($this->locale()->toNumber($this->notificationCount)>0) ? ' notif_update':'';?>">
        <div class="pulldown_contents_wrapper">
          <div class="pulldown_contents">
            <ul class="notifications_menu" id="notifications_menu">
              <div class="notifications_loading" id="notifications_loading">
                <img src='<?php echo $this->layout()->staticBaseUrl ?>application/modules/Core/externals/images/loading.gif' style='float:left; margin-right: 5px;' />
                <?php echo $this->translate("Loading ...") ?>
              </div>
            </ul>
          </div>
          <div class="pulldown_options">
            <?php echo $this->htmlLink(array('route' => 'default', 'module' => 'activity', 'controller' => 'notifications'),
               $this->translate('View All Updates'),
               array('id' => 'notifications_viewall_link')) ?>
            
          </div>
        </div>
        <a href="javascript:void(0);" id="updates_toggle" title="Updates" <?php if( $this->notificationCount ):?> class="new_updates"<?php endif;?>>
        <span <?php if( $this->notificationCount ){?> class="new_updates count_update"<?php }else{echo 'class="no_count"';}?>>
        <?php 
        echo ($this->locale()->toNumber($this->notificationCount)>100)?'99+':$this->locale()->toNumber($this->notificationCount);
        //echo $this->translate(array('%s', '%s', $this->notificationCount), $this->locale()->toNumber($this->notificationCount)) ?>
        </span>
        <span class="update_icon"></span>
        </a>
      </span>
    </li>
    <li id='core_menu_mini_menu_edit_profile'>
      <a href="<?php echo $this->baseUrl().'/members/edit/profile';?>" title="Edit profile"><span class="edit_profile_icon"></span></a>
    </li>
    <?php endif; ?>
    
    
    <!--<?php if( $this->viewer->getIdentity()) :?>
    
    <li class="menu_core_mini custom_389">
      
      <a class="menu_core_mini custom_389" href="<?php echo $this->baseUrl();?>/create-pages" title="Create Page">
      <span class="create_page_icon"></span>
      </a></li>
      
    <?php endif; ?>-->
   
    <?php foreach( $this->navigation as $item ):
     ?>
      <li class="<?php echo  !empty($item->class) ? $item->class : null;?>">
      <?php 
        if($item->class == 'menu_core_mini core_mini_auth'){
          echo $this->htmlLink('login', (($item->class == 'menu_core_mini core_mini_profile' ||  $item->class =='no-dloader menu_core_mini core_mini_auth')?'&nbsp;':$this->translate($item->getLabel())), array_filter(array(
            'class' => ( !empty($item->class) ? ($item->class == 'menu_core_mini core_mini_profile'?'profile_icon '.$item->class:$item->class) : null ),
            'alt' => ( !empty($item->alt) ? $item->alt : null ),
            'target' => ( !empty($item->target) ? $item->target : null ),
            'title' => ( !empty($item->class) ? (($item->class == 'menu_core_mini core_mini_profile'?'My Profile':($item->class == 'menu_core_mini core_mini_messages'?'Messages':($item->class == 'no-dloader menu_core_mini core_mini_auth'?'Settings':'')))) : null ),
          )));
        }else{
      ?>
      <?php 
        echo $this->htmlLink(($item->class != 'menu_core_mini core_mini_profile' && $item->class != 'no-dloader menu_core_mini core_mini_auth')?$item->getHref():'javascript:void(0);', (($item->class == 'menu_core_mini core_mini_profile' ||  $item->class =='no-dloader menu_core_mini core_mini_auth')?'&nbsp;':$this->translate($item->getLabel())), array_filter(array(
            'class' => ( !empty($item->class) ? ($item->class == 'menu_core_mini core_mini_profile'?'profile_icon '.$item->class:$item->class) : null ),
            'alt' => ( !empty($item->alt) ? $item->alt : null ),
            'target' => ( !empty($item->target) ? $item->target : null ),
            'title' => ( !empty($item->class) ? (($item->class == 'menu_core_mini core_mini_profile'?'My Profile':($item->class == 'menu_core_mini core_mini_messages'?'Messages':($item->class == 'no-dloader menu_core_mini core_mini_auth'?'Settings':'')))) : null ),
          ))); 
         }
      ?>
     
     <?php if( $this->viewer->getIdentity()) :?>
        <ul style="display:none;" id="pagenames">
          <li>Use Crezilla As</li>
          <li class="active">
            <?php $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($this->viewer->getIdentity());
if($pic == '')$pic  = $this->baseUrl().'/application/modules/User/externals/images/nophoto_user_thumb_profile.png';
?><span class="uia_profile" style="background-image:url(<?php echo $pic;?>)"></span>
            <?php //echo $this->itemPhoto($this->viewer, "thumb.icon");?>
            <a href="<?php echo $this->viewer->getHref();?>">My Profile</a></li>
          <?php $userpages = Engine_Api::_()->getDbtable('pages', 'page')->getUserPages(Engine_Api::_()->user()->getViewer()->getIdentity());
      $k=1;
      if($userpages){
        foreach($userpages as $page){
                    if($k>10){
                          break;
                        }
                    $companypic = Engine_Api::_()->getDbtable('users', 'user')->getUserPageProfilePicPath($page['user_id'], $page['photo_id'], $page['page_id']);
            $companypic = ($companypic=='')?'application/modules/Page/externals/images/nophoto_page_thumb_profile.png':$companypic;
                    $p  = Engine_Api::_()->getItem('page', $page['page_id']);
                        ?>
            <li class="page_active_<?php echo $page['page_id'];?>">
                        <span class="uia_profile" style="background-image:url(<?php echo $companypic;?>)">
                        </span>
                        <a href="<?php echo $this->baseUrl().'/page/'.$page['url'];?>">
                          <?php echo $page['title'];?>
                        </a>
                        </li>
        <?php $k++;}
      }?>
            <?php if($k>10){?>
              <li class="see_all_pages  page_active_'.$page['page_id'].'">
                    <a href="<?php echo $this->baseUrl().'/manage-pages';?>" style="color: #34aee8; text-align:center;">
                        See All
                    </a>
                </li>
            <?php }?>
            </ul>
       <?php if($item->class == 'no-dloader menu_core_mini core_mini_auth') : 
       ?>
        <ul class="privacy_options" style="display:none;">
          <div class="accordion">
      <div class="accordion-section">
        <a class="accordion-section-title" href="#accordion-1">Privacy</a>
        <div id="accordion-1" class="accordion-section-content">
          <p>
                      <label>Who can message me?</label>
                      <span>
                        <select id="msg_setting" class="psetting">
                          <option value="1" <?php if($this->viewer->msg_setting == '1'){echo "selected='selected'";}?>>Everyone</option>
                            <option value="2" <?php if($this->viewer->msg_setting == '2'){echo "selected='selected'";}?>>My Friends</option>
                            <option value="3" <?php if($this->viewer->msg_setting == '3'){echo "selected='selected'";}?>>Only Me</option>
                        </select>
                      </span>
                    </p>
                    
                    <p>
                      <label>Who can see my friend list?</label>
                      <span>
                        <select id="friend_setting"  class="psetting">
                          <option value="1" <?php if($this->viewer->friend_setting == '1'){echo "selected='selected'";}?>>Everyone</option>
                            <option value="2" <?php if($this->viewer->friend_setting == '2'){echo "selected='selected'";}?>>My Friends</option>
                            <option value="3" <?php if($this->viewer->friend_setting == '3'){echo "selected='selected'";}?>>Only Me</option>
                        </select>
                      </span>
                    </p>
                    
                    <p style="display:none;">
                      <label>Calendar</label>
                      <span>
                        <select id="cal_setting"  class="psetting">
                          <option value="1" <?php if($this->viewer->cal_setting == '1'){echo "selected='selected'";}?>>Everyone</option>
                            <option value="2" <?php if($this->viewer->cal_setting == '2'){echo "selected='selected'";}?>>My Friends</option>
                            <option value="3" <?php if($this->viewer->cal_setting == '3'){echo "selected='selected'";}?>>Only Me</option>
                        </select>
                      </span>
                    </p>
        </div><!--end .accordion-section-content-->
      </div><!--end .accordion-section-->

      <div class="accordion-section">
        <a class="accordion-section-title" href="#accordion-2">Change Password</a>
        <div id="accordion-2" class="accordion-section-content">
          <div id="passsuccess" style=" color:##009900;display:none; font-weight:bold;">Your changes have been saved.</div>
                    <div id="passerror" style="color:#dc2324;display: none; font-weight: bold;">Password does not match.</div>
                    <?php echo $this->passform->render($this) ?>
        </div><!--end .accordion-section-content-->
      </div>
            
      <div class="accord_footer">
      <div class="accordion-section">
        <div class="accord" style="padding-bottom:6px;">
                 <a class="accordion-content" href="<?php echo $this->baseUrl().'/logout';?>">Sign Out</a>
                </div>
      </div><!--end .accordion-section-->
    </div>
    </div>
    
        </ul>
       <?php endif; ?>
    <?php endif; ?>
     
     
      </li>
    <?php endforeach; ?>
    
  </ul>
  
  
  
  
  <ul class="tp_center_search">
  
   <?php if($this->search_check):?>
      <li id="global_search_form_container">
        <form id="global_search_form" action="<?php echo $this->url(array('module' => 'user', 'controller' => 'index' , 'action' => 'browse'), 'default', true) ?>" method="get">
          <input type='text' class='text suggested' name='displayname' id='global_search_field' size='20' maxlength='100' alt='<?php echo $this->translate('Search') ?>' placeholder="Search"/>
          <button type="submit" id="submit" name="submit" class="top_serach">Search</button>
     </form>
      </li>
    <?php endif;?>
  </ul>
  
  
</div>





<div class="toggle_dropdown gr_bg" style="display:none;">
  <?php
    // Reverse the navigation order (they're floating right)
    $count = count($this->navigation);
    foreach( $this->navigation->getPages() as $item ) $item->setOrder(--$count);
  ?>
  
  
   <?php
      if( $this->viewer()->getIdentity() ):
      echo $this->navigation()
      ->menu()
      ->setContainer($this->navigation2)
      ->setPartial(null)
      ->setUlClass('navigation')
      ->render();
    endif;
    ?>
    
    
     
    
  <!-- <ul class="media_ul" style="float: left !important;width:100% !important;">
   <?php if( $this->viewer->getIdentity()) :?>
    <li id='core_menu_mini_menu_update'>
      <span onclick="toggleUpdatesPulldown(event, this, '4');" style="display: inline-block;" class="updates_pulldown<?php echo ($this->locale()->toNumber($this->notificationCount)>0) ? ' notif_update':'';?>">
        <div class="pulldown_contents_wrapper">
          <div class="pulldown_contents">
            <ul class="notifications_menu" id="notifications_menu">
              <div class="notifications_loading" id="notifications_loading">
                <img src='<?php echo $this->layout()->staticBaseUrl ?>application/modules/Core/externals/images/loading.gif' style='float:left; margin-right: 5px;' />
                <?php echo $this->translate("Loading ...") ?>
              </div>
            </ul>
        
          
          
          
          
          
          <div class="pulldown_options">
            <?php echo $this->htmlLink(array('route' => 'default', 'module' => 'activity', 'controller' => 'notifications'),
               $this->translate('View All Updates'),
               array('id' => 'notifications_viewall_link')) ?>
            <?php /*?><?php echo $this->htmlLink('javascript:void(0);', $this->translate('Mark All Read'), array(
              'id' => 'notifications_markread_link',
            )) ?><?php */?>
          </div>
        </div>
        
        
        <a href="javascript:void(0);" id="updates_toggle" <?php if( $this->notificationCount ):?> class="new_updates"<?php endif;?>>
        <span <?php if( $this->notificationCount ){?> class="new_updates count_update"<?php }else{echo 'class="no_count"';}?>>
        <?php 
        echo ($this->locale()->toNumber($this->notificationCount)>100)?'99+':$this->locale()->toNumber($this->notificationCount);
        //echo $this->translate(array('%s', '%s', $this->notificationCount), $this->locale()->toNumber($this->notificationCount)) ?>
        </span>
        <span class="update_icon"></span>
        </a>
      </span>
    </li>
    <?php endif; ?>
   
    <?php foreach( $this->navigation as $item ):
    //echo '<pre>'; print_r($item); echo '</pre>';
     ?>
      <li class="<?php echo  !empty($item->class) ? $item->class : null;?>">
      <?php echo $this->htmlLink($item->getHref(), $this->translate($item->getLabel()), array_filter(array(
        'class' => ( !empty($item->class) ? $item->class : null ),
        'alt' => ( !empty($item->alt) ? $item->alt : null ),
        'target' => ( !empty($item->target) ? $item->target : null ),
      ))) ?></li>
    <?php endforeach; ?>
    
  </ul> -->
  
  

  
 
  
</div>
</div>
</div>

<script type='text/javascript'>

jQuery("#oldPassword1, #password1, #passwordConfirm1").keypress(function(e) {
   if(e.which == 13 || e.which == 1 ) {
      validatechangepass();
   }
});

function validatechangepass(){
 
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
if(jQuery.trim(jQuery("#oldPassword1").val()) == ''){
    jQuery("#oldPassword1").focus();
    jQuery("#oldPassword1").addClass("com_form_error_validatin");
    return false;
}
if(jQuery.trim(jQuery("#password1").val()) == ''){
    jQuery("#password1").focus();
    jQuery("#password1").addClass("com_form_error_validatin");
    return false;
}
if(jQuery.trim(jQuery("#passwordConfirm1").val()) == ''){
    jQuery("#passwordConfirm1").focus();
    jQuery("#passwordConfirm1").addClass("com_form_error_validatin");
    return false;
}
if(jQuery.trim(jQuery("#passwordConfirm1").val()) != jQuery.trim(jQuery("#password1").val())){
    jQuery("#passerror").show();
    jQuery("#passerror").html("Password does not match.");
    jQuery("#passerror").fadeOut(7000);
    jQuery("#passwordConfirm1").focus();
    jQuery("#passwordConfirm1").addClass("com_form_error_validatin");
    return false;
}

jQuery.post("<?php echo $this->baseUrl();?>/members/edit/chkoldpass",{oldPassword:jQuery.trim(jQuery("#oldPassword1").val()),password:jQuery.trim(jQuery("#password1").val()),passwordConfirm:jQuery.trim(jQuery("#passwordConfirm1").val())}, function(resp){
  if(!jQuery.trim(resp)){
    jQuery("#user_form_changepass")[0].reset();
    jQuery("#passsuccess").show();
    jQuery("#passsuccess").html("Password changed successfully.");
    jQuery("#passsuccess").fadeOut(7000);
    return false;
  }else{
    jQuery("#passerror").show();
    jQuery("#passerror").html("Old password did not match.");
    jQuery("#passerror").fadeOut(7000);
    return false;
  }
  //$('#signupform').submit();
});
  
}

jQuery(document).on('click','.profile_icon', function(){
  jQuery('#pagenames').slideToggle(); 
});
  var notificationUpdater;
  
  en4.core.runonce.add(function(){

  new Autocompleter.Request.JSON('global_search_field', '<?php echo $this->url(array('module' => 'user', 'controller' => 'public', 'action' => 'suggest','message' => true), 'default', true) ?>', { 
        'minLength': 1,
        'delay' : 150,
        'selectMode': 'pick',
        'autocompleteType': 'tag',
        'multiple': false,
        'className': 'message-autosuggest',
        'filterSubset' : true,

        'tokenFormat' : 'object',
        'tokenValueKey' : 'label',
    'keycommand':'continue',
        'injectChoice': function(token){
          if(token.type == 'user'){
            var choice = new Element('li', {
              'class': 'autocompleter-choices',
              'id':token.label
            });
            new Element('div', {
              <!--'html': '<a href="'+token.url+'">'+this.markQueryValue(token.photo)+this.markQueryValue(token.label)+'</a>',-->
        'html': '<a href="'+token.url+'">'+token.photo+this.markQueryValue(token.label)+'</a>',
              'class': 'autocompleter-choice'
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
          else {
            var choice = new Element('li', {
              'class': 'autocompleter-choices friendlist',
              'id':token.label
            });
                new Element('div', {
              'html': this.markQueryValue(token.label),
              'class': 'autocompleter-choice'
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
            
        },
        onPush : function(){
         if( $('toValues').value.split(',').length >= maxRecipients ){
            $('to').disabled = true;
          }
        }
    });
        

    if($('notifications_markread_link')){
      $('notifications_markread_link').addEvent('click', function() {
        //$('notifications_markread').setStyle('display', 'none');
        en4.activity.hideNotifications('<?php echo $this->string()->escapeJavascript($this->translate("0"));?>');
      });
    }

    <?php if ($this->updateSettings && $this->viewer->getIdentity()): ?>
    notificationUpdater = new NotificationUpdateHandler({
              'delay' : <?php echo $this->updateSettings;?>
            });
    notificationUpdater.start();
    window._notificationUpdater = notificationUpdater;
    <?php endif;?>
  });


  var toggleUpdatesPulldown = function(event, element, user_id) {
    console.log('event'+ event);
    
    if( element.className=='updates_pulldown' || element.className=='updates_pulldown notif_update') {
      console.log('element'+element.className);
      element.className= 'updates_pulldown_active';
      showNotifications();
    } else {
      element.className='updates_pulldown';
    }
  }

  var showNotifications = function() {
    en4.activity.updateNotifications();
    new Request.HTML({
      'url' : en4.core.baseUrl + 'activity/notifications/pulldown',
      'data' : {
        'format' : 'html',
        'page' : 1
      },
      'onComplete' : function(responseTree, responseElements, responseHTML, responseJavaScript) {
        if( responseHTML ) {
          // hide loading icon
          if($('notifications_loading')){
            console.log('get');
           $('notifications_loading').setStyle('display', 'none');
          }

          $('notifications_menu').innerHTML = responseHTML;
          $('notifications_menu').addEvent('click', function(event){
            event.stop(); //Prevents the browser from following the link.

            var current_link = event.target;
            var notification_li = $(current_link).getParent('li');

            // if this is true, then the user clicked on the li element itself
            if( notification_li.id == 'core_menu_mini_menu_update' ) {
              notification_li = current_link;
            }

            var forward_link;
            if( current_link.get('href') ) {
              forward_link = current_link.get('href');
            } else{
              forward_link = $(current_link).getElements('a:last-child').get('href');
            }

            if( notification_li.get('class') == 'notifications_unread' ){
              notification_li.removeClass('notifications_unread');
              en4.core.request.send(new Request.JSON({
                url : en4.core.baseUrl + 'activity/notifications/markread',
                data : {
                  format     : 'json',
                  'actionid' : notification_li.get('value')
                },
                onSuccess : function() {
                  window.location = forward_link;
                }
              }));
            } else {
              window.location = forward_link;
            }
          });
        } else {
          $('notifications_loading').innerHTML = '<?php echo $this->string()->escapeJavascript($this->translate("You have no new updates."));?>';
        }
      }
    }).send();
  };


jQuery('li a.core_mini_auth').on('click', function(){
  //jQuery("#user_form_changepass")[0].reset();
  if(jQuery('.privacy_options').css("display") == "none"){
    jQuery('.privacy_options').css("display", "block");
  }else{
    jQuery('.privacy_options').css("display", "none");
  } 
});


</script>

 <?php $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($this->viewer->getIdentity());
 if($pic == ''){
 ?>
<script type="text/javascript">
        jQuery(document).ready(function(){
        var data  = '<img src="'+jQuery('#pic').val()+'"/>';
        var img   = "<span><img src='<?php echo $this->baseUrl().'/public/custom/images/profile_avtar_icon.png'?>'><img src='<?php echo $this->baseUrl().'/public/custom/images/profile-dd-arrow.png'?>'></span>";
        jQuery('.profile_icon').prepend(img);  
      });
    </script>
    
    
<?php }else{?>
  <script type="text/javascript">
        jQuery(document).ready(function(){
        var data  = '<img src="'+jQuery('#pic').val()+'"/>';
        var icon_msg  = "<img src='<?php echo $this->baseUrl().'/public/custom/images/profile-dd-arrow.png'?>";
        jQuery('.profile_icon').prepend('<?php echo "<span>".$this->itemPhoto($this->viewer(), 'thumb.icon')."</span>" ;?>'); 
        var img   = "<img src='<?php echo $this->baseUrl().'/public/custom/images/profile-dd-arrow.png'?>'>";
        jQuery('.profile_icon span').append(img);
      });
    </script>
<?php } ?>



<?php 
  $url  = $_SERVER['REQUEST_URI'];
    $newurl = explode('/', $url);
    $furl = $newurl[count($newurl)-1];
    $surl = $newurl[count($newurl)-2];
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl().'/public/custom/accordion.css';?>">

<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/accordion.js?'.time();?>"></script>
<script type="text/javascript">

jQuery(document).ready(function(){

  var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
if( isMobile.any() )
{
  //alert('Mobile');
    jQuery(".heiglights-section2").css('display','none');
    jQuery(".heiglights-section3").css('display','block');
    jQuery("li#desk > a").attr("href","#highlightsm");
    jQuery("#core_menu_mini_menu_mobile > ul.header_menu_class2 > li.no-dloader.menu_core_mini.core_mini_admin > a").css('display','none');
    jQuery("#core_menu_mini_menu_mobile > ul.header_menu_class2 > li.no-dloader.menu_core_mini.core_mini_auth > a").css('display','none');
   //jQuery("#core_menu_mini_menu").css('display','none');
}
else
{
    jQuery(".heiglights-section2").css('display','block');
    jQuery(".heiglights-section3").css('display','none');    
    jQuery("li#desk > a").attr("href","#highlights");
    jQuery("#core_menu_mini_menu_mobile").css('display','none');
} 
//if( isMobile.iOS() ) alert('iOS');
  /////////
    //jQuery(".generic_layout_container.layout_classified_home_page_jobs").hide();
    jQuery("#global_content > div.headline > div.tabs").hide();
    // PRIVACY SETTINGS
    var user_id= "<?php echo $this->viewer()->getIdentity();?>";
    console.log('user_id'+user_id);
    if(user_id == 1)
    {}
    else
    {
      jQuery("a.menu_core_main.custom_1021").closest("li").hide();;
    }
    jQuery('.psetting').on('change', function(){
      var type  =   jQuery(this).attr('id');
      var value = jQuery(this).val();
      jQuery.post('<?php echo $this->baseUrl();?>/members/edit/messagesettings',{setting:value, type:type}, function(resp){
        jQuery('.privacy_options').css("display", "none");
      }); 
    });
  
    var firsturlpart= '<?php echo $furl; ?>';
    console.log('firsturlpart'+ firsturlpart);
    var securlpart  = '<?php echo $surl; ?>'; 
    console.log('securlpart' + securlpart);
    if(firsturlpart == 'home'){
      jQuery('#core_menu_mini_menu li a.core_main_home').addClass('active');
    }else if(firsturlpart ==  'search' && securlpart == 'job' || (firsturlpart ==  'job' && securlpart == 'user' || (firsturlpart ==  'manage' && securlpart == 'classifieds') ) ){
      jQuery('#core_menu_mini_menu li a.custom_259').addClass('active');
    /*}else if(firsturlpart ==  'casting'){
      jQuery('#core_menu_mini_menu li a.custom_258').addClass('active');*/
    }else if(firsturlpart ==  'services' || firsturlpart ==  'casting' || firsturlpart == 'suppliers' || firsturlpart == 'classifiedboard'){
      jQuery('#core_menu_mini_menu li a.custom_262').addClass('active');
    }else if((firsturlpart  ==  'manage-pages') || (firsturlpart  ==  'create-pages') || (firsturlpart  ==  'events' && securlpart == 'pages') || (firsturlpart ==  'parent' && securlpart == 'index') || (firsturlpart ==  'campaigns' && securlpart =='ads') || (firsturlpart == 'myclassified') || (firsturlpart  ==  'postclassified') || (securlpart  ==  'editpostclassified') || (firsturlpart == 'shortlist')  ){
      jQuery('#core_menu_mini_menu li a.custom_831').addClass('active');
    }else if((firsturlpart  ==  'create' && securlpart == 'classifieds') || (firsturlpart ==  'packpage' && securlpart =='ads') ||  (firsturlpart  ==  'premiumsearch') || (firsturlpart ==  'notices') ){
      jQuery('#core_menu_mini_menu li a.custom_861').addClass('active');
    }

    
 });

jQuery("#tog_button").click(function(){
  if(jQuery(".toggle_dropdown").css('display')=='none'){
   jQuery(".toggle_dropdown").slideDown();
  }else{
   jQuery(".toggle_dropdown").slideUp();
  }
 });
 
jQuery(document).mouseup(function (e)
{
    var container = jQuery("#pagenames");
  var maincontainer = jQuery(".core_mini_profile");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0 && !maincontainer.is(e.target) // if the target of the click isn't the container...
        && maincontainer.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
  
  // CLOSE UPDATES DROPDOWN
  var updatecontainer = jQuery(".updates_pulldown_active");

    if (!updatecontainer.is(e.target) // if the target of the click isn't the container...
        && updatecontainer.has(e.target).length === 0) // ... nor a descendant of the container
    {
        updatecontainer.addClass('updates_pulldown');
    updatecontainer.removeClass('updates_pulldown_active');
    }
  
  // CLOSE MENU ON MY PROFILE
  var menucontainer = jQuery(".more_tab");

    if (!menucontainer.is(e.target) // if the target of the click isn't the container...
        && menucontainer.has(e.target).length === 0) // ... nor a descendant of the container
    { 
     menucontainer.addClass('tab_closed');
     menucontainer.removeClass('tab_open');
    }
  
  // CLOSE MENU ON SETTINGS
  var menucontainer = jQuery(".core_mini_auth");

    if (!menucontainer.is(e.target) // if the target of the click isn't the container...
        && menucontainer.has(e.target).length === 0) // ... nor a descendant of the container
    { 
     jQuery('.privacy_options').css("display", "none");
    }
});

function showsignup(){
  var offset = jQuery('#why_join').offset();
  //jQuery(window).scrollTop(offset.top);
  jQuery('html, body').animate({scrollTop: offset.top}, 800);
}
 
</script>