<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<?php
if( $this->viewer()->getIdentity() ):
  echo $this->navigation()
    ->menu()
    ->setContainer($this->navigation)
    ->setPartial(null)
    ->setUlClass('navigation')
    ->render();
endif;
?>
	
<?php /*?>


<ul class="navigation ">
	<?php foreach($this->navigation as $nav){?>
    <li>
    	<a href="" class="<?php echo $nav->class;?>"><?php echo $nav->label;?></a>
    </li>
    <?php } ?>
    
</ul>



<?php */?>