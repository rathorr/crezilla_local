<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: terms.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style>
	.terms_statement
	{
		margin: 5px 5px 0px 10px;
	}
	.terms_statement span
	{
	    font-size: 16px;
	    font-weight: bold;
	}
	p
	{
		margin: 0 10px 10px;
    	font-size: 15px;
	}
	#global_header > div.layout_page_header > div > div.generic_layout_container.layout_core_menu_mini > div.device_on{
		display: none !important;
	}
.logout .layout_core_mobi_menu_mini #core_menu_mini_menu_mobile {
    display: none;
}
</style>
<h2><?php echo $this->translate('Terms of Service') ?></h2>
<div class="terms_statement" >
<p>
  This agreement was written in English. To the extent any translated version of this agreement conflicts with the English version, the English version controls.  Please note that Section 16 contains certain changes to the general terms for users outside the United States.</p>

<span>Statement of Rights and Responsibilities</span>
<p>This Statement of Rights and Responsibilities ("Statement," "Terms," or "SRR") derives from the Crezilla Principles, and is our terms of service that governs our relationship with users and others who interact with Crezilla, as well as Crezilla brands, products and services, which we call the “Crezilla Services” or “Services”. By using or accessing the Crezilla Services, you agree to this Statement, as updated from time to time in accordance with Section 13 below. Additionally, you will find resources at the end of this document that help you understand how Crezilla works.

<p>Because Crezilla provides a wide range of Services, we may ask you to review and accept supplemental terms that apply to your interaction with a specific app, product, or service. To the extent those supplemental terms conflict with this SRR, the supplemental terms associated with the app, product, or service govern with respect to your use of such app, product or service to the extent of the conflict.</p>
<span>1.	Privacy </span>

<p>Your privacy is very important to us. We designed our Data Policy to make important disclosures about how you can use Crezilla to share with others and how we collect and can use your content and information. We encourage you to read the Data Policy, and to use it to help you make informed decisions. </p>
 
<span>2.	Sharing Your Content and Information</span>

<p>You own all of the content and information you post on Crezilla, and you can control how it is shared through your privacy settings. In addition:</p>
<p>1.	For content that is covered by intellectual property rights, like photos and videos (IP content), you specifically give us the following permission, subject to your privacy settings: you grant us a non-exclusive, transferable, sub-licensable, royalty-free, worldwide license to use any IP content that you post on or in connection with Crezilla (IP License). This IP License ends when you delete your IP content or your account unless your content has been shared with others, and they have not deleted it.</p>
<p>2.	When you delete IP content, it is deleted in a manner similar to emptying the recycle bin on a computer. However, you understand that removed content may persist in backup copies for a reasonable period of time (but will not be available to others).</p>
<p>3.	When you use an application, the application may ask for your permission to access your content and information as well as content and information that others have shared with you.  We require applications to respect your privacy, and your agreement with that application will control how the application can use, store, and transfer that content and information.  </p>
<p>4.	When you publish content or information using the Public setting, it means that you are allowing everyone, including people off of Crezilla, to access and use that information, and to associate it with you (i.e., your name and profile picture).</p>
<p>5.	We always appreciate your feedback or other suggestions about Crezilla, but you understand that we may use your feedback or suggestions without any obligation to compensate you for them (just as you have no obligation to offer them).</p>
 
<span>3.	Safety</span>

<p>We do our best to keep Crezilla safe, but we cannot guarantee it. We need your help to keep Crezilla safe, which includes the following commitments by you:</p>
<p>1.	You will not post unauthorized commercial communications (such as spam) on Crezilla.</p>
<p>2.	You will not collect users' content or information, or otherwise access Crezilla, using automated means (such as harvesting bots, robots, spiders, or scrapers) without our prior permission.</p>
<p>3.	You will not engage in unlawful multi-level marketing, such as a pyramid scheme, on Crezilla.</p>
<p>4.	You will not upload viruses or other malicious code.</p>
<p>5.	You will not solicit login information or access an account belonging to someone else.
<p>6.	You will not bully, intimidate, or harass any user.</p>
<p>7.	You will not post content that: is hate speech, threatening, or pornographic; incites violence; or contains nudity or graphic or gratuitous violence.</p>
<p>8.	You will not develop or operate a third-party application containing alcohol-related, dating or other mature content (including advertisements) without appropriate age-based restrictions.</p>
<p>9.	You will not use Crezilla to do anything unlawful, misleading, malicious, or discriminatory.</p>
<p>10.	You will not do anything that could disable, overburden, or impair the proper working or appearance of Crezilla, such as a denial of service attack or interference with page rendering or other Crezilla functionality.</p>
<p>11.	You will not facilitate or encourage any violations of this Statement or our policies.</p>
 
<span>4.	Registration and Account Security</span>

<p>Crezilla users provide their real names and information, and we need your help to keep it that way. Here are some commitments you make to us relating to registering and maintaining the security of your account:</p>
<p>1.	You will not provide any false personal information on Crezilla, or create an account for anyone other than yourself without permission.</p>
<p>2.	You will not create more than one personal account.</p>
<p>3.	If we disable your account, you will not create another one without our permission.</p>
<p>4.	You will not use your personal timeline primarily for your own commercial gain, and will use a Crezilla Page for such purposes.</p>
<p>5.	You will not use Crezilla if you are under 13.</p>
<p>6.	You will not use Crezilla if you are a convicted sex offender.</p>
<p>7.	You will keep your contact information accurate and up-to-date.</p>
<p>8.	You will not share your password (or in the case of developers, your secret key), let anyone else access your account, or do anything else that might jeopardize the security of your account.</p>
<p>9.	You will not transfer your account (including any Page or application you administer) to anyone without first getting our written permission.</p>
<p>10.	If you select a username or similar identifier for your account or Page, we reserve the right to remove or reclaim it if we believe it is appropriate (such as when a trademark owner complains about a username that does not closely relate to a user's actual name).</p>
 
<span>5.	Protecting Other People's Rights</span>

<p>We respect other people's rights, and expect you to do the same.</p>
<p>1.	You will not post content or take any action on Crezilla that infringes or violates someone else's rights or otherwise violates the law.</p>
<p>2.	We can remove any content or information you post on Crezilla if we believe that it violates this Statement or our policies.</p>
<p>3.	If we remove your content for infringing someone else's copyright, and you believe we removed it by mistake, we will provide you with an opportunity to appeal.</p>
<p>4.	If you repeatedly infringe other people's intellectual property rights, we will disable your account when appropriate.</p>
<p>5.	You will not use our copyrights or Trademarks or any confusingly similar marks, except as expressly permitted by our Brand Usage Guidelines or with our prior written permission.</p>
<p>6.	If you collect information from users, you will: obtain their consent, make it clear you (and not Crezilla) are the one collecting their information, and post a privacy policy explaining what information you collect and how you will use it.</p>
<p>7.	You will not post anyone's identification documents or sensitive financial information on Crezilla.</p>
<p>8.	You will not tag users or send email invitations to non-users without their consent. Crezilla offers social reporting tools to enable users to provide feedback about tagging.</p>
 
<span>6.	Mobile and Other Devices</span>
<p>1.	We currently provide our mobile website for free, but please be aware that your carrier's normal rates and fees, such as text messaging and data charges, will still apply.</p>
<p>2.	In the event you change or deactivate your mobile telephone number, you will update your account information on Crezilla within 48 hours to ensure that your messages are not sent to the person who acquires your old number.</p>
<p>3.	You provide consent and all rights necessary to enable users to sync (including through an application) their devices with any information that is visible to them on Crezilla.</p>
 
<span>7.	About Advertisements and Other Commercial Content Served or Enhanced by Crezilla.</span>

<p>Our goal is to deliver advertising and other commercial or sponsored content that is valuable to our users and advertisers. In order to help us do that, you agree to the following:</p>
<p>1.	You give us permission to use your name, profile picture, content, and information in connection with commercial, sponsored, or related content (such as a brand you like) served or enhanced by us. This means, for example, that you permit a business or other entity to pay us to display your name and/or profile picture with your content or information, without any compensation to you. If you have selected a specific audience for your content or information, we will respect your choice when we use it.</p>
<p>2.	We do not give your content or information to advertisers without your consent.</p>
<p>3.	You understand that we may not always identify paid services and communications as such.</p>
 
<span>8.	Special Provisions Applicable to Advertisers </span>

<p>If you use our self-service advertising creation interfaces for creation, submission and/or delivery of any advertising or other commercial or sponsored activity or content (collectively, the “Self-Serve Ad Interfaces”), you agree to our Self-Serve Ad Terms. In addition, your advertising or other commercial or sponsored activity or content placed on Crezilla or our publisher network will comply with our Advertising Policies.</p>
<span>9.	Special Provisions Applicable to Pages</span>

<p>If you create or administer a Page on Crezilla, or run a promotion or an offer from your Page, you agree to our Pages Terms.</p>
 
<span>10.	Special Provisions Applicable to Software</span>
<p>1.	If you download or use our software, such as a stand-alone software product, an app, or a browser plugin, you agree that from time to time, the software may download and install upgrades, updates and additional features from us in order to improve, enhance, and further develop the software.</p>
<p>2.	You will not modify, create derivative works of, decompile, or otherwise attempt to extract source code from us, unless you are expressly permitted to do so under an open source license, or we give you express written permission.</p>

<span>11.	Amendments</span>
<p>1.	We’ll notify you before we make changes to these terms and give you the opportunity to review and comment on the revised terms before continuing to use our Services.</p>
<p>2.	If we make changes to policies, guidelines or other terms referenced in or incorporated by this Statement, we may provide notice on the Site Governance Page.</p>
<p>3.	Your continued use of the Crezilla Services, following notice of the changes to our terms, policies or guidelines, constitutes your acceptance of our amended terms, policies or guidelines.</p>
 
<span>12.	Termination</span>

<p>If you violate the letter or spirit of this Statement, or otherwise create risk or possible legal exposure for us, we can stop providing all or part of Crezilla to you. We will notify you by email or at the next time you attempt to access your account. You may also delete your account or disable your application at any time. In all such cases, this Statement shall terminate, but the following provisions will still apply: 2.2, 2.4, 3-5, 6.3, and 13-18. </p>
 
<span>13.	Disputes</span>
<p>1.	You will resolve any claim, cause of action or dispute (claim) you have with us arising out of or relating to this Statement or Crezilla exclusively in the courts of New Delhi, India and you agree to submit to the personal jurisdiction of such courts for the purpose of litigating all such claims. The laws of India will govern this Statement, as well as any claim that might arise between you and us, without regard to conflict of law provisions.</p>
<p>2.	If anyone brings a claim against us related to your actions, content or information on Crezilla, you will indemnify and hold us harmless from and against all damages, losses, and expenses of any kind (including reasonable legal fees and costs) related to such claim. Although we provide rules for user conduct, we do not control or direct users' actions on Crezilla and are not responsible for the content or information users transmit or share on Crezilla. We are not responsible for any offensive, inappropriate, obscene, unlawful or otherwise objectionable content or information you may encounter on Crezilla. We are not responsible for the conduct, whether online or offline, of any user of Crezilla.</p>
<p>3.	WE TRY TO KEEP CREZILLA UP, BUG-FREE, AND SAFE, BUT YOU USE IT AT YOUR OWN RISK. WE ARE PROVIDING CREZILLA AS IS WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. WE DO NOT GUARANTEE THAT CREZILLA WILL ALWAYS BE SAFE, SECURE OR ERROR-FREE OR THAT CREZILLA WILL ALWAYS FUNCTION WITHOUT DISRUPTIONS, DELAYS OR IMPERFECTIONS. CREZILLA IS NOT RESPONSIBLE FOR THE ACTIONS, CONTENT, INFORMATION, OR DATA OF THIRD PARTIES, AND YOU RELEASE US, OUR DIRECTORS, OFFICERS, EMPLOYEES, AND AGENTS FROM ANY CLAIMS AND DAMAGES, KNOWN AND UNKNOWN, ARISING OUT OF OR IN ANY WAY CONNECTED WITH ANY CLAIM YOU HAVE AGAINST ANY SUCH THIRD PARTIES. IF YOU ARE A CALIFORNIA RESIDENT, YOU WAIVE CALIFORNIA CIVIL CODE §1542, WHICH SAYS: A GENERAL RELEASE DOES NOT EXTEND TO CLAIMS WHICH THE CREDITOR DOES NOT KNOW OR SUSPECT TO EXIST IN HIS OR HER FAVOR AT THE TIME OF EXECUTING THE RELEASE, WHICH IF KNOWN BY HIM OR HER MUST HAVE MATERIALLY AFFECTED HIS OR HER SETTLEMENT WITH THE DEBTOR. WE WILL NOT BE LIABLE TO YOU FOR ANY LOST PROFITS OR OTHER CONSEQUENTIAL, SPECIAL, INDIRECT, OR INCIDENTAL DAMAGES ARISING OUT OF OR IN CONNECTION WITH THIS STATEMENT OR CREZILLA, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. OUR AGGREGATE LIABILITY ARISING OUT OF THIS STATEMENT OR CREZILLA WILL NOT EXCEED THE GREATER OF ONE HUNDRED DOLLARS ($100) OR THE AMOUNT YOU HAVE PAID US IN THE PAST TWELVE MONTHS. APPLICABLE LAW MAY NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY OR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU. IN SUCH CASES, CREZILLA'S LIABILITY WILL BE LIMITED TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW.</p>

<span>14.	Definitions</span>
<p>1.	By "Crezilla" or” Crezilla Services” we mean the features and services we make available, including through (a) our website at www.Crezilla.com and any other Crezilla branded or co-branded websites (including sub-domains, international versions, widgets, and mobile versions); (b) our Platform; (c) social plugins such as the Like button, the Share button and other similar offerings; and (d) other media, brands, products, services, software (such as a toolbar), devices, or networks now existing or later developed. Crezilla reserves the right to designate, in its sole discretion, that certain of our brands, products, or services are governed by separate terms and not this SRR.</p>
<p>2.	By "Platform" we mean a set of APIs and services (such as content) that enable others, including application developers and website operators, to retrieve data from Crezilla or provide data to us.</p>
<p>3.	By "information" we mean facts and other information about you, including actions taken by users and non-users who interact with Crezilla.</p>
<p>4.	By "content" we mean anything you or other users post, provide or share using Crezilla Services.</p>
<p>5.	By "data" or "user data" or "user's data" we mean any data, including a user's content or information that you or third parties can retrieve from Crezilla or provide to Crezilla through Platform.</p>
<p>6.	By "post" we mean post on Crezilla or otherwise make available by using Crezilla.</p>
<p>7.	By "use" we mean use, run, copy, publicly perform or display, distribute, modify, translate, and create derivative works of.</p>
<p>8.	By "application" we mean any application or website that uses or accesses Platform, as well as anything else that receives or has received data from us.  If you no longer access Platform but have not deleted all data from us, the term application will apply until you delete the data.</p>
<p>9.	By “Trademarks” we mean the list of trademarks provided here.</p> 
 
<span>15.	Other</span>
<p>1.	This Statement makes up the entire agreement between the parties regarding Crezilla, and supersedes any prior agreements.</p>
<p>2.	If any portion of this Statement is found to be unenforceable, the remaining portion will remain in full force and effect.</p>
<p>3.	If we fail to enforce any of this Statement, it will not be considered a waiver.</p>
<p>4.	Any amendment to or waiver of this Statement must be made in writing and signed by us.</p>
<p>5.	You will not transfer any of your rights or obligations under this Statement to anyone else without our consent.</p>
<p>6.	All of our rights and obligations under this Statement are freely assignable by us in connection with a merger, acquisition, or sale of assets, or by operation of law or otherwise.</p>
<p>7.	Nothing in this Statement shall prevent us from complying with the law.</p>
<p>8.	This Statement does not confer any third party beneficiary rights.</p>
<p>9.	We reserve all rights not expressly granted to you.</p>
<p>10.	You will comply with all applicable laws when using or accessing Crezilla.
</p>

</div>