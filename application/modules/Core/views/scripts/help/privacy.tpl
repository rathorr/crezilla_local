<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: privacy.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Alex
 */
?>
<style>
	.privacy_statement
	{
		margin: 5px 5px 0px 10px;
	}
	.privacy_statement span
	{

    font-size: 16px;
    font-weight: bold;
	}
	p
	{
		margin: 0 10px 10px;
    	font-size: 15px;
	}
	#global_header > div.layout_page_header > div > div.generic_layout_container.layout_core_menu_mini > div.device_on{
		display: none !important;
	}
.logout .layout_core_mobi_menu_mini #core_menu_mini_menu_mobile {
    display: none;
}

</style>
<h2><?php echo $this->translate('Privacy Statement') ?></h2>
<div class="privacy_statement">

<span>What kinds of information do we collect?</span>
<p>Depending on which Services you use, we collect different kinds of information from or about you. Things you do and information you provide. We collect the content and other information you provide when you use our Services, including when you sign up for an account, create or share, and message or communicate with others. This can include information in or about the content you provide, such as the location of a photo or the date a file was created. We also collect information about how you use our Services, such as the types of content you view or engage with or the frequency and duration of your activities. </p>

<span>Things others do and information they provide.</span>
<p>We also collect content and information that other people provide when they use our Services, including information about you, such as when they share a photo of you, send a message to you, or upload, sync or import your contact information.</p>

<span>Your networks and connections.</span>
<p>We collect information about the people and groups you are connected to and how you interact with them, such as the people you communicate with the most or the groups you like to share with. We also collect contact information you provide if you upload, sync or import this information (such as an address book) from a device.</p>
<span>Information about payments.</span>

<p>If you use our Services for purchases or financial transactions (like when you buy something on Crezilla, make a purchase in a game, or make a donation), we collect information about the purchase or transaction. This includes your payment information, such as your credit or debit card number and other card information, and other account and authentication information, as well as billing, shipping and contact details.</p>



<span>Device information.</span>
<p>We collect information from or about the computers, phones, or other devices where you install or access our Services, depending on the permissions you’ve granted. We may associate the information we collect from your different devices, which helps us provide consistent Services across your devices. Here are some examples of the device information we collect: 
•	Attributes such as the operating system, hardware version, device settings, file and software names and types, battery and signal strength, and device identifiers.
•	Device locations, including specific geographic locations, such as through GPS, Bluetooth, or WiFi signals.
•	Connection information such as the name of your mobile operator or ISP, browser type, language and time zone, mobile phone number and IP address.</p>

<span>Information from websites and apps that use our Services.</span>
<p>We collect information when you visit or use third-party websites and apps that use our Services (like when they offer our Like button or Crezilla Log In or use our measurement and advertising services). This includes information about the websites and apps you visit, your use of our Services on those websites and apps, as well as information the developer or publisher of the app or website provides to you or us.</p>

<span>Information from third-party partners.</span>
<p>We receive information about you and your activities on and off Crezilla from third-party partners, such as information from a partner when we jointly offer services or from an advertiser about your experiences or interactions with them.</p>

<span>Crezilla companies.</span>
<p>We receive information about you from companies that are owned or operated by Crezilla, in accordance with their terms and policies. </p>

<span>How do we use this information?</span>
<p>We use all of the information we have to help us provide and support our Services. </p>

<span>Here’s how:</span>
<span>Provide, improve and develop Services.</span>
<p>We are able to deliver our Services, personalize content, and make suggestions for you by using this information to understand how you use and interact with our Services and the people or things you’re connected to and interested in on and off our Services. </p>

<span>We also use information we have to provide shortcuts and suggestions to you.</span> 
<p>When we have location information, we use it to tailor our Services for you and others, like helping you to check-in and find local events or offers in your area or tell your friends that you are nearby. </p>

<p>We conduct surveys and research, test features in development, and analyze the information we have to evaluate and improve products and services, develop new products or features, and conduct audits and troubleshooting activities.
Communicate with you.</p>

<p>We use your information to send you marketing communications, communicate with you about our Services and let you know about our policies and terms. We also use your information to respond to you when you contact us.</p>
<span>Show and measure ads and services.</span>

<p>We use the information we have to improve our advertising and measurement systems so we can show you relevant ads on and off our Services and measure the effectiveness and reach of ads and services.Learn more about advertising on our Services and how you can controlhow information about you is used to personalize the ads you see.</p>

<span>Promote safety and security.</span>
<p>We use the information we have to help verify accounts and activity, and to promote safety and security on and off of our Services, such as by investigating suspicious activity or violations of our terms or policies. We work hard to protect your account using teams of engineers, automated systems, and advanced technology such as encryption and machine learning. We also offer easy-to-use security tools that add an extra layer of security to your account. </p>

<p>We use cookies and similar technologies to provide and support our Services and each of the uses outlined and described in this section of our policy. </p>

<span>How is this information shared?</span>
<p>Sharing On Our Services
People use our Services to connect and share with others. We make this possible by sharing your information in the following ways:
People you share and communicate with.
When you share and communicate using our Services, you choose the audience who can see what you share. For example, when you post on Crezilla, you select the audience for the post, such as a customized group of individuals, all of your Friends, or members of a Group. Likewise, when you use Messenger, you also choose the people you send photos to or message. </p>

<p>Public information is any information you share with a public audience, as well as information in your Public Profile, or content you share on a Crezilla Page or another public forum. Public information is available to anyone on or off our Services and can be seen or accessed through online search engines, APIs, and offline media, such as on TV. </p>

<p>In some cases, people you share and communicate with may download or re-share this content with others on and off our Services. When you comment on another person’s post or like their content on Crezilla, that person decides the audience who can see your comment or like. If their audience is public, your comment will also be public.</p>

<span>People that see content others share about you.</span>
<p>Other people may use our Services to share content about you with the audience they choose. For example, people may share a photo of you, mention or tag you at a location in a post, or share information about you that you shared with them. If you have concerns with someone’s post, social reporting is a way for people to quickly and easily ask for help from someone they trust. </p>

<span>Apps, websites and third-party integrations on or using our Services.</span>
<p>When you use third-party apps, websites or other services that use, or are integrated with, our Services, they may receive information about what you post or share. For example, when you play a game with your Crezilla friends or use the Crezilla Comment or Share button on a website, the game developer or website may get information about your activities in the game or receive a comment or link that you share from their website on Crezilla. In addition, when you download or use such third-party services, they can access your Public Profile, which includes your username or user ID, your age range and country/language, your list of friends, as well as any information that you share with them. Information collected by these apps, websites or integrated services is subject to their own terms and policies. </p>

<span>Sharing within Crezilla companies.</span>
<p>We share information we have about you within the family of companies that are part of Crezilla. Learn more about our companies.</p>

<span>New owner.</span>
<p>If the ownership or control of all or part of our Services or their assets changes, we may transfer your information to the new owner. </p>

<span>Sharing With Third-Party Partners and Customers</span>
<p>We work with third party companies who help us provide and improve our Services or who use advertising or related products, which makes it possible to operate our companies and provide free services to people around the world. </p>

<span>Here are the types of third parties we can share information with about you:</span>
<p>Advertising, Measurement and Analytics Services (Non-Personally Identifiable Information Only). We want our advertising to be as relevant and interesting as the other information you find on our Services. With this in mind, we use all of the information we have about you to show you relevant ads. We do not share information that personally identifies you (personally identifiable information is information like name or email address that can by itself be used to contact you or identifies who you are) with advertising, measurement or analytics partners unless you give us permission. We may provide these partners with information about the reach and effectiveness of their advertising without providing information that personally identifies you, or if we have aggregated the information so that it does not personally identify you. For example, we may tell an advertiser how its ads performed, or how many people viewed their ads or installed an app after seeing an ad, or provide non-personally identifying demographic information (such as 25 year old female, in Madrid, who likes software engineering) to these partners to help them understand their audience or customers, but only after the advertiser has agreed to abide by our advertiser guidelines. </p>

<span>Vendors, service providers and other partners.</span>
<p>We transfer information to vendors, service providers, and other partners who globally support our business, such as providing technical infrastructure services, analyzing how our Services are used, measuring the effectiveness of ads and services, providing customer service, facilitating payments, or conducting academic research and surveys. These partners must adhere to strict confidentiality obligations in a way that is consistent with this Data Policy and the agreements we enter into with them.
</p>
</div>