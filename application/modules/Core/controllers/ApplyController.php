<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ReportController.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Core_ApplyController extends Core_Controller_Action_Standard
{
  public function init()
  {
    $this->_helper->requireUser();
    $this->_helper->requireSubject();
  }

  public function createAction()
  {
		$viewer = Engine_Api::_()->user()->getViewer();
		if( !$viewer->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
  
		$id = $this->_getParam('id'); 
		$viewer = $viewer->getIdentity();
   
		$table = Engine_Api::_()->getDbtable('applys', 'classified');	
		//$select = $table->select();
		$applyname = $table->info('name'); 

		$select = $table->select("$applyname.*")
				->where("$applyname.classified_id = $id", 1)
				->where("$applyname.user_id = $viewer", 1);
				
		$result = $table->fetchAll($select);
		
		if($result->count() > 0){
			 $this->view->status_message = "You have already applied for this job.";
			 return $view;
		}

		$arr = array
		 (
		   'user_id'=>$viewer,
		   'classified_id'=>$id,
		 );
			 
		$table->insert($arr); 
		$this->view->status_message = "You have successfully applied for the job.";
		return $view;


  }
  
  
  //GET THE LIST OF ALL APPLICATNTS FOR THIS JOB
  public function listAction()
  {
		$viewer = Engine_Api::_()->user()->getViewer();
		if( !$viewer->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
  
		$id = $this->_getParam('id');
		
		$viewer = $viewer->getIdentity();
   
		$table = Engine_Api::_()->getDbtable('applys', 'classified');	
		//$select = $table->select();
		$applyname = $table->info('name'); 

		$select = $table->select("$applyname.*")
				->where("$applyname.classified_id = $id", 1);
				
		$result = $table->fetchAll($select);
		
		if($result->count() > 0){
			 $this->view->reslist = $result;
			 return $view;
		}
		else{
			$this->view->reslist = "No applicants found.";
			 return $view;
		}

		
  }
}