<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: IndexController.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */

class Home_IndexController extends Core_Controller_Action_Standard
{
  public function indexAction()
  {
    if ($this->_getParam('test')){
      Engine_Api::_()->user()->setViewer( new User_Model_User(array()) );
    }

    if( Engine_Api::_()->user()->getViewer()->getIdentity() )
    {
      return $this->_helper->redirector->gotoRoute(array('action' => 'home'), 'user_general', true);
    }

    // check public settings
    if( !Engine_Api::_()->getApi('settings', 'core')->core_general_portal &&
      !$this->_helper->requireUser()->isValid() ) {
      return;
    }

    $view = $this->view;
    $baseUrl = $view->layout()->staticBaseUrl;

    // Core functions
    $view->headScript()->appendFile( $baseUrl. 'application/modules/Home/externals/scripts/core.js');

    $examples = Engine_Api::_()->home()->getExamples();
    $settings = Engine_Api::_()->getDbTable('settings', 'core');

	// Params 
    $example = $settings->getSetting('home.example');
    $background = $settings->getSetting('home.background');
    if (empty($background) && !empty($examples[$example])) {
      $background = $examples[$example];
    }
    $background = $baseUrl . $background;
    $opacity = (int)$settings->getSetting('home.opacity')/100;

	  // Set background
	  $styles = <<<CONTENT
  #global_page_home-index-index {
    background-image: url({$background}) !important;
  }
  #global_page_home-index-index .layout_right .generic_layout_container,
  #global_page_home-index-index .layout_left .generic_layout_container,
  #global_page_home-index-index .layout_middle .generic_layout_container {
    background: rgba(0,0,0, {$opacity});
  }	
CONTENT;

	$view->headStyle()->appendStyle($styles);


    // Revert from mobile version
    $layout = Zend_Layout::startMvc();
    $layout->setViewBasePath(APPLICATION_PATH . "/application/modules/Core/layouts", 'Core_Layout_View')
        ->setViewSuffix('tpl');

    // Render
    $this->_helper->content
      //->setNoRender()
      ->setEnabled()
    ;
  }

}
