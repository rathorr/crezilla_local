<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: AdminSettingsController.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */

class Home_AdminSettingsController extends Core_Controller_Action_Admin
{

  public function indexAction()
  {
    $settings = Engine_Api::_()->getDbTable('settings', 'core');

    // Get form
    $this->view->form = $form = new Home_Form_Settings();

    $this->_addRemoveLinks($settings, $form);

    $form->populate((array)$settings->getSetting('home'));

    if (!$this->getRequest()->isPost()) {
      return;
    }
    if (!$form->isValid($this->getRequest()->getPost())) {
      return;
    }

    $values = $form->getValues();

    foreach ($values as $key => $item){
      if ($item === null){
        unset($values[$key]);
      }
    }
    $settings->setSetting('home', $values);
	
	if ($form->background->getValue() !== null) {
      $fileName = $form->background->getFileName();
      $extension = ltrim(strrchr(basename($fileName), '.'), '.');
      $path = 'public/admin/home_background.' . $extension;
      @copy($fileName, APPLICATION_PATH . DIRECTORY_SEPARATOR . $path);
      $settings->setSetting('home.background', $path);
    }
    if ($form->logo->getValue() !== null) {
      $fileName = $form->logo->getFileName();
      $extension = ltrim(strrchr(basename($fileName), '.'), '.');
      $path = 'public/admin/home_logo.' . $extension;
      @copy($fileName, APPLICATION_PATH . DIRECTORY_SEPARATOR . $path);
      $settings->setSetting('home.logo', $path);
    }

	

    if ($values['header']){
      $this->_addWidget('home.menu-main', -1); // First
    } else {
      $this->_removeWidget('home.menu-main');
    }
    if ($values['footer']){
      $this->_addWidget('home.menu-footer', 99); // Last
    } else {
      $this->_removeWidget('home.menu-footer');
    }
    if ($values['members']){
      $this->_addWidget('home.popular-members', 0); // Second
    } else {
      $this->_removeWidget('home.popular-members');
    }
    if ($values['signup']){
      $this->_addWidget('home.signup', 99);
    } else {
      $this->_removeWidget('home.signup');
    }

    $form->addNotice('Changes has been saved');

    // Refresh
    $this->_addRemoveLinks($settings, $form);

    // Clear Cache
    try {
      /**
       * @var $cache Zend_Cache_Core
       */
      $cache = Engine_Content::getInstance()->getCache();
      $cache->clean();
    } catch (Exception $e) {

    }

  }
  
  protected function _addRemoveLinks($settings, $form)
  {
    if ($settings->getSetting('home.background')){
      $link = $this->view->url(array('module' => 'home', 'controller' => 'settings', 'action' => 'remove-background'));
      $form->getElement('background')->getDecorator('Description')->setEscape(false);
      $form->getElement('background')->setOptions(array(
        'description' => '<a href="' . $link . '" class="smoothbox">' . $this->view->translate('Remove') . '</a>'
      ));
    }
    if ($settings->getSetting('home.logo')){
      $link = $this->view->url(array('module' => 'home', 'controller' => 'settings', 'action' => 'remove-logo'));
      $form->getElement('logo')->getDecorator('Description')->setEscape(false);
      $form->getElement('logo')->setOptions(array(
        'description' => '<a href="' . $link . '" class="smoothbox">' . $this->view->translate('Remove') . '</a>'
      ));
    }
  }

  protected function _addWidget($name, $order)
  {
    $page = Engine_Api::_()->getDbTable('pages', 'core')->fetchRow(array(
      'name = ?' => 'home_index_index'
    ));
    $table = Engine_Api::_()->getDbTable('content', 'core');
    $middle = $table->fetchRow(array(
      'name = ?' => 'middle',
      'page_id = ?' => $page->page_id
    ));
    if (!$middle) {
      throw new Exception('Middle block is not exists');
    }
    $table = Engine_Api::_()->getDbTable('content', 'core');

    $widget = $table->fetchRow(array(
      'name = ?' => $name,
      'page_id = ?' => $page->page_id
    ));
    if (!$widget) {
      $widget = $table->createRow(array(
        'page_id' => $page->page_id,
        'type' => 'widget',
        'name' => $name,
        'parent_content_id' => $middle->content_id,
        'order' => $order
      ))->save();
    }
  }

  protected function _removeWidget($name)
  {
    $page = Engine_Api::_()->getDbTable('pages', 'core')->fetchRow(array(
      'name = ?' => 'home_index_index'
    ));
    Engine_Api::_()->getDbTable('content', 'core')->delete(array(
      'name = ?' => $name,
      'page_id = ?' => $page->page_id
    ));
  }

  public function removeLogoAction()
  {
    $settings = Engine_Api::_()->getDbTable('settings', 'core');
    $this->view->style = $settings->setSetting('home.logo', 0);

    $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => true,
      'parentRefresh' => true,
      'messages' => array(Zend_Registry::get('Zend_Translate')->_('Your photo has been removed.'))
    ));
  }

  public function removeBackgroundAction()
  {
    $settings = Engine_Api::_()->getDbTable('settings', 'core');
    $this->view->style = $settings->setSetting('home.background', 0);

    $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => true,
      'parentRefresh' => true,
      'messages' => array(Zend_Registry::get('Zend_Translate')->_('Your photo has been removed.'))
    ));

  }

}