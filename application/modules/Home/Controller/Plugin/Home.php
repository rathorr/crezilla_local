<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: Home.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */

class Home_Controller_Plugin_Home extends Zend_Controller_Plugin_Abstract
{
  public function preDispatch(Zend_Controller_Request_Abstract $request)
  {
    if (($request->getModuleName() == 'core' || $request->getModuleName() == 'mobi')
      && $request->getControllerName() == 'index'
      && $request->getActionName() == 'index'
    ) {
      // if our touch is active
      if (Engine_Api::_()->getDbTable('modules', 'core')->isModuleEnabled('apptouch') && Engine_Api::_()->apptouch()->isApptouchMode()){
        return ;
      }
      if (Engine_Api::_()->getDbTable('settings', 'core')->getSetting('home.enabled')) {
        $request->setModuleName('home');
      }
    }
  }

}