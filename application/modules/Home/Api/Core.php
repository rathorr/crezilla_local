<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: Core.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */

class Home_Api_Core extends Core_Api_Abstract
{
  public function getExamples()
  {
    return array(
      'city' => 'application/modules/Home/externals/images/examples/city.jpg',
      'guitarist' => 'application/modules/Home/externals/images/examples/guitarist.jpg',
    );
  }
}