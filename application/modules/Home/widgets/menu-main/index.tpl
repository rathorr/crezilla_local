<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    $Id: index.tpl 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */
?>

<a href="javascript:void(0)"
   class="home-icon-menu"
   onclick="$$('.layout_home_menu_main > ul')[0].toggle()"></a>

<script type="text/javascript">
  window.addEvent('resize', function () {
    var width = $$('body')[0].getWidth();
    if (width > 767) {
      $$('.layout_home_menu_main > ul')[0].setStyle('display', 'block');
    } else {
      $$('.layout_home_menu_main > ul')[0].setStyle('display', 'none');
    }
  });
</script>

<?php
  echo $this->navigation()
    ->menu()
    ->setContainer($this->navigation)
    ->setPartial(null)
    ->setUlClass('navigation')
    ->render();
?>