<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: Controller.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */

class Home_Widget_MenuMainController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    $this->view->navigation = $navigation = Engine_Api::_()
      ->getApi('menus', 'core')
      ->getNavigation('core_main');
    
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    $require_check = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.browse', 1);
    if(!$require_check && !$viewer->getIdentity()){
      $navigation->removePage($navigation->findOneBy('route','user_general'));
    }
  }

  public function getCacheKey()
  {
    //return Engine_Api::_()->user()->getViewer()->getIdentity();
  }
}