<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    $Id: index.tpl 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */
?>
<script type="text/javascript">
  en4.core.runonce.add(function () {
    //new Tips($$('.popular_tips'));
  });
</script>

<ul class="home-popular-members">
  <?php foreach ($this->paginator as $user): ?>
    <li>
      <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon'), array(
        'class' => 'popular_tips',
        'title' => $user->getTitle()
      )) ?>
    </li>
  <?php endforeach; ?>
</ul>
