<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: Controller.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */
  
class Home_Widget_PopularMembersController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    // Should we consider views or comments popular?
    $popularType = $this->_getParam('popularType', 'member');
    if( !in_array($popularType, array('view', 'member')) ) {
      $popularType = 'member';
    }
    $this->view->popularType = $popularType;
    $this->view->popularCol = $popularCol = $popularType . '_count';

    // Get paginator
    $table = Engine_Api::_()->getDbtable('users', 'user');
    $select = $table->select()
      ->where('search = ?', 1)
      ->where('photo_id > 0')
      ->where('enabled = ?', 1)
      ->where($popularCol . ' >= ?', 0)
      ->order($popularCol . ' DESC')
    ;
    $this->view->paginator = $paginator = Zend_Paginator::factory($select);

	$settings = Engine_Api::_()->getDbTable('settings', 'core');
	
    // Set item count per page and current page number
    $paginator->setItemCountPerPage($settings->getSetting('home.memberscount', 50));
    $paginator->setCurrentPageNumber($this->_getParam('page', 1));

    // Do not render if nothing to show
    if( $paginator->getTotalItemCount() <= 0 ) {
      return $this->setNoRender();
    }
  }

  public function getCacheKey()
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    $translate = Zend_Registry::get('Zend_Translate');
    return $viewer->getIdentity() . $translate->getLocale();
  }

  public function getCacheSpecificLifetime()
  {
    return 120;
  }
}