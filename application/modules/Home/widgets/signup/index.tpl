<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    $Id: index.tpl 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */
?>

<style type="text/css">
  @keyframes scaleDown {
    to { opacity: 0; transform: scale(.9); }
  }
  @keyframes scaleUp {
    from { opacity: 0; transform: scale(.9); }
  }
  @-webkit-keyframes scaleDown {
    to { opacity: 0; -webkit-transform: scale(.9); }
  }
  @-webkit-keyframes scaleUp {
    from { opacity: 0; -webkit-transform: scale(.9); }
  }
</style>

<script type="text/javascript">
  function signupForm(hide)
  {
    $$('body')
        .removeClass('scaleDown')
        .removeClass('scaleUp');

    $$('body').addClass('scaleDown'); // make effect
    setTimeout(function () {
      $$('body').removeClass('scaleDown');
      var all_widgets = $$('.layout_right, .layout_middle > div, layout_left');
      var the_form = $$('.layout_home_signup');
      if (hide){
        all_widgets.show();
        the_form.hide();
      } else {
        all_widgets.hide();
        the_form.show();
      }
      $$('body').addClass('scaleUp'); // make effect
      homeCenter();
    }, 1000);
  }
  en4.core.runonce.add(function (){
    $$('a[href='+en4.core.baseUrl+'signup]').addEvent('click', function (e){
      e.stop();
      signupForm();
    });
  });

</script>

<a href="javascript:void(0);" onclick="signupForm(true)" class="home-sigup-close"></a>
<?php echo $this->form->getForm()->render();?>