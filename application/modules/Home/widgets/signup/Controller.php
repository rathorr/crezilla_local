<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: Controller.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */

class Home_Widget_SignupController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    $table = Engine_Api::_()->getDbtable('signup', 'user');
    $select = $table->select()
      ->where('enable = ?', 1)
      ->order('order ASC');

    $row = $table->fetchRow($select);
    if (!$row) {
      return $this->setNoRender();
    }
    $this->view->form = new $row->class;
  }

  public function getCacheKey()
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    $translate = Zend_Registry::get('Zend_Translate');
    return $viewer->getIdentity() . $translate->getLocale();
  }

  public function getCacheSpecificLifetime()
  {
    return 120;
  }

}