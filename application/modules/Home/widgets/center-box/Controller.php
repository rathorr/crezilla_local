<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: Controller.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */

class Home_Widget_CenterBoxController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	  
	// If the user is logged in, they can't chk this page
    if( Engine_Api::_()->user()->getViewer()->getIdentity() ) {
		$redirector = new Zend_Controller_Action_Helper_Redirector(); 
		$redirector->gotoUrl('members/home');
		die;
    }
    $examples = Engine_Api::_()->home()->getExamples();
	
    $settings = Engine_Api::_()->getDbTable('settings', 'core');
	//echo '<pre>';print_r($settings);echo '</pre>';

    $this->view->title = $settings->getSetting('home.title');
	$this->view->aligntitle = $settings->getSetting('home.aligntitle', 'left');
    $this->view->description = $settings->getSetting('home.body');
    $this->view->logo = $settings->getSetting('home.logo');
	
  }

  public function getCacheKey()
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    $translate = Zend_Registry::get('Zend_Translate');
    return $viewer->getIdentity() . $translate->getLocale();
  }

  public function getCacheSpecificLifetime()
  {
    return 120;
  }

}