<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    $Id: index.tpl 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */
?>
<style>
.step {
	width: 100%;
	position: relative;
}
.step:not(.active) {
	opacity: 1;
	filter: alpha(opacity=99);
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(opacity=99)";
}
.step:not(.active) a.jms-link{
	opacity: 1;
	margin-top: 40px;
}
#global_content{width:100% !important;}
.jms-slideshow{ margin:0 !important;}

  #global_wrapper
  {
      padding-top: 0px !important;
  }
</style>
<script src="https://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<?php echo $this->headLink()->appendStylesheet($this->baseUrl().'/public/js/demo.css') ?>
<?php echo $this->headLink()->appendStylesheet($this->baseUrl().'/public/js/style.css') ?>

<?php echo $this->headLink()->appendStylesheet($this->baseUrl().'/public/custom/home-page.css') ?>
<?php //echo $this->headScript()->appendFile($this->baseUrl().'/application/modules/Photoviewer/externals/scripts/jquery-1.9.0.min.js'); ?>

<?php //echo $this->headScript()->appendFile($this->baseUrl().'/public/js/jmpress.js'); ?>
<?php //echo $this->headScript()->appendFile($this->baseUrl().'/public/js/jquery.jmslideshow.js'); ?>
<?php  //echo $this->headScript()->appendFile($this->baseUrl().'/public/js/jquery.bxslider.min.js'); ?>

<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300,100' rel='stylesheet' type='text/css'>

  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  
<!-- new layout section start  -->

<div class="main-head-home">


<img src="<?php echo $this->baseUrl().'/public/custom/images/ipro-img.jpg'?>" class="main-index-img" alt="" />

<div class="home-header"><a href="<?php echo $this->baseUrl();?>"><img src="<?php echo $this->baseUrl().'/public/admin/iproducer-logo-index.png'?>" /> </a>




<nav class="navbar navbar-inverse nologinmenu">
  <div class="">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
     
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="#whyjoin">WHY JOIN</a></li>
        <li id="desk"><a>HIGHLIGHTS</a></li>
        <li><a class="br0" href="#howitworks">HOW IT WORKS</a></li>
        <li><a class="hl-menu" href="<?php echo $this->baseUrl().'/signup' ?>"> Join</a></li>
        <li><a class="hl-menu" href="<?php echo $this->baseUrl().'/login' ?>"> Log In</a></li>
      </ul>
      
    </div>
  </div>
</nav>
  
 
</div>
<div class="sub-tit-main-head">
SEARCH. ENGAGE. CREATE.
<div class="clear"></div> 
<!-- <a href="" data-toggle="modal" data-target="#myModal">Join</a> <a href="<?php echo $this->baseUrl().'/login' ?>">Log in</a> -->
<a href="<?php echo $this->baseUrl().'/signup' ?>">Join</a> <a href="<?php echo $this->baseUrl().'/login' ?>">Log in</a>

</div>
<div class="about-text">
<h1>ABOUT US</h1>
CREZILLA is a global creative network, built on a web and mobile 
platform, that helps professional and amateur content creators 
discover fabulous on and off screen talent, the smartest 
professionals, and the best service providers, allowing them to put 
together productions of any scale or budget quickly and easily.
</div>



</div>

<div class="index-why-join" id="whyjoin">
<h1>WHY JOIN?</h1>
<div class="col-md-4">
<img src="<?php echo $this->baseUrl().'/public/custom/images/content-creators.jpg'?>" alt="" />
<strong>CONTENT CREATORS</strong>
<span>Put project teams together quickly and 
discover new talent and service providers.</span>
<img src="<?php echo $this->baseUrl().'/public/custom/images/more-link-img.jpg'?>" alt="" />
</div>
<div class="col-md-4">
<img src="<?php echo $this->baseUrl().'/public/custom/images/talent.jpg'?>" alt="" />
<strong>TALENT</strong>
<span>Showcase your talent, expand your network 
letting the biggest content creators from 
around the world find you.</span>
<img src="<?php echo $this->baseUrl().'/public/custom/images/more-link-img.jpg'?>" alt="" />
</div>
<div class="col-md-4">
<img src="<?php echo $this->baseUrl().'/public/custom/images/service-provider.jpg'?>" alt="" />
<strong>SERVICE PROVIDERS</strong>
<span>Expand your client network and quote 
for projects with the biggest content 
creators from around the world.</span>

<img src="<?php echo $this->baseUrl().'/public/custom/images/more-link-img.jpg'?>" alt="" />
</div>
<div class="clear"></div>
</div>

<!-- <div class="heiglights-section" id="highlights">
<div class="heiglights-section-left">
<div class="text-highlight"><h2>HIGHLIGHTS</h2>
We want CREZILLA to be easy for you to use and fun! So 
we've built it around a social interface with lots of familiar 
tools and tricks. Whether you're a content creator, talent, 
service provider or just  someone who wants to get a sense 
of the global creative scene, this is the place to be.

<div class="clear"></div>
<br/><br/>
<a data-toggle="modal" data-target="#myModal" class="blc-btn">JOIN</a><a href="<?php echo $this->baseUrl().'/login' ?>" class="org-btn">LOG IN</a>
 
</div>
</div>
<div class="heiglights-section-right">

<div class="heighlight-box one">
<div class="heighlight-icon">
<img src="<?php echo $this->baseUrl().'/public/custom/images/social_icon.jpg'?>" /></div> 
<div class="heighlight-text"><strong>SOCIAL</strong>
Join our global creative 
community and expand 
your network.
</div>
</div>

<div class="heighlight-box two">
<div class="heighlight-icon"><img src="<?php echo $this->baseUrl().'/public/custom/images/showcase_icon.jpg'?>" /></div>
<div class="heighlight-text"><strong>SHOWCASE</strong>
Showcase your organization, 
talent, experience or skill with 
our portfolio tools.
</div></div>

<div class="heighlight-box three">
<div class="heighlight-icon">
<img src="<?php echo $this->baseUrl().'/public/custom/images/network_icon.jpg'?>" /></div>
<div class="heighlight-text"><strong>NETWORK</strong>
Our on ground social events 
create superb industry 
networking opportunities.
</div></div>

<div class="heighlight-box four">
<div class="heighlight-icon"><img src="<?php echo $this->baseUrl().'/public/custom/images/opportunities_icon.jpg'?>" /></div>
<div class="heighlight-text"><strong>OPPORTUNITIES</strong>
Apply for advertised work 
opportunities or simply let new 
clients discover you
</div></div>

</div>

<div class="clear"></div>

</div> -->

<div class="clear"></div>

<div class="heiglights-section" id="highlights">
<div class="heiglights-section2">
<div class="heiglights-section-left2">
<div class="text-highlight"><h2 style="color: #3dc2bb;">HIGHLIGHTS</h2></div>
<div class="heighlight-box one">
<div class="heighlight-icon">
<img src="<?php echo $this->baseUrl().'/public/custom/images/information_icon.jpg'?>" /></div> 
<div class="heighlight-text"><strong>INFORMATION</strong>
Up to date and accurate info, allows 
our search engines to find you easily, 
matching you with the best 
opportunities.
</div>
</div>

<div class="heighlight-box two">
<div class="heighlight-icon"><img src="<?php echo $this->baseUrl().'/public/custom/images/picture_icon.jpg'?>" /></div>
<div class="heighlight-text"><strong>PICTURES</strong>
Good quality pictures and 
videos bring your profile alive 
and attract more attention
</div></div>
<div class="clear"></div>
<div class="heighlight-box three">
<div class="heighlight-icon">
<img src="<?php echo $this->baseUrl().'/public/custom/images/security.jpg'?>" /></div>
<div class="heighlight-text"><strong>SECURITY</strong>
Do not share your mobile or email 
address with strangers. CREZILLA never 
shares member contact information. Use 
our messenger function to reach out 
directly.
</div></div>

<div class="heighlight-box four">
<div class="heighlight-icon"><img src="<?php echo $this->baseUrl().'/public/custom/images/invite_icon.jpg'?>" /></div>
<div class="heighlight-text"><strong>INVITE</strong>
Invite friends to join you on 
Crezilla. It's a fun place to be!
</div></div>
</div>


<div class="heiglights-section-right2" id="howitworks" >

<div class="text-highlight"><h2>HOW IT WORKS</h2>
You need to create an account to join the CREZILLA community. Creating an 
account is FREE and takes just a few minutes. You can also use our social 
media sign in to speed things up. In order to have a meaningful experience, 
follow the best practice advice below.
<div class="clear"></div>
<br/><br/>

</div>
</div>



<div class="clear"></div>
</div>
</div>
<!-- for Mobile -->
<div class="heiglights-section3">

<div class="heiglights-section-right2" id="howitworks" >

<div class="text-highlight"><h2>HOW IT WORKS</h2>
You need to create an account to join the CREZILLA community. Creating an 
account is FREE and takes just a few minutes. You can also use our social 
media sign in to speed things up. In order to have a meaningful experience, 
follow the best practice advice below.
<div class="clear"></div>
<br/><br/>

</div>
</div>
</div>
<div class="heiglights-section4">
<div class="heiglights-section-left2" id="highlightsm">
<div class="text-highlight"><h2 style="color: #3dc2bb;">HIGHLIGHTS</h2></div>
<div class="heighlight-box one">
<div class="heighlight-icon">
<img src="<?php echo $this->baseUrl().'/public/custom/images/information_icon.jpg'?>" /></div> 
<div class="heighlight-text"><strong>INFORMATION</strong>
Up to date and accurate info, allows 
our search engines to find you easily, 
matching you with the best 
opportunities.
</div>
</div>

<div class="heighlight-box two">
<div class="heighlight-icon"><img src="<?php echo $this->baseUrl().'/public/custom/images/picture_icon.jpg'?>" /></div>
<div class="heighlight-text"><strong>PICTURES</strong>
Good quality pictures and 
videos bring your profile alive 
and attract more attention
</div></div>
<div class="clear"></div>
<div class="heighlight-box three">
<div class="heighlight-icon">
<img src="<?php echo $this->baseUrl().'/public/custom/images/security.jpg'?>" /></div>
<div class="heighlight-text"><strong>SECURITY</strong>
Do not share your mobile or email 
address with strangers. CREZILLA never 
shares member contact information. Use 
our messenger function to reach out 
directly.
</div></div>

<div class="heighlight-box four">
<div class="heighlight-icon"><img src="<?php echo $this->baseUrl().'/public/custom/images/invite_icon.jpg'?>" /></div>
<div class="heighlight-text"><strong>INVITE</strong>
Invite friends to join you on 
Crezilla. It's a fun place to be!
</div></div>
</div>
<div class="clear"></div>

</div>
<!-- end for mobile -->
<div class="clear"></div>

<!-- new layout section start -->

<!-- slider section start -->
<!-- slider section end -->

<!-- lower section start -->



<!-- lower section end -->




