<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    $Id: index.tpl 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */
?>

<?php
  $this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Home/externals/colorpicker/jscolor.js');
?>

<style>
  #header-label, #footer-label, #members-label, #signup-label, #enabled-label, #customize-label {
    display: none;
  }
  #color-element img {
    vertical-align: top;
    margin-top: 3px;
  }
  .colorpicker-container td {
    padding:2px;
  }
  .colorpicker-container table table tr {
    display: none;
  }
  .colorpicker-container table table tr:first-child {
    display: block;
  }
</style>

<script type="text/javascript">
  function openDemo()
  {
    var url = '<?php echo $this->url(array('module' => 'home', 'test' => 1), 'default', true);?>';
    window.open(url,'_blank'); return false;
  }
/*
  jscolor.dir = '<?php echo $this->layout()->staticBaseUrl . 'application/modules/Home/externals/colorpicker/';?>';

  en4.core.runonce.add(function (){
    $('color').addEvent('change', function (){
      var color = ((this.color.rgb[0]*100).toFixed(0)) + ','+((this.color.rgb[1]*100).toFixed(0))+ ','+((this.color.rgb[2]*100).toFixed(0));
      $('colorrgb').set('value', color);
    });
  });
*/

</script>

<div class='settings'>
  <?php echo $this->form->render();?>
 </div>