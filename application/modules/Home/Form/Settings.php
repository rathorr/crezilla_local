<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: Settings.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */

class Home_Form_Settings extends Engine_Form
{
  public function init()
  {
    $this->setTitle('Global settings');

    $this
      ->setAttrib('enctype', 'multipart/form-data');

    $this->addElement('Checkbox', 'enabled', array(
      'label' => 'Enabled the landing page',
    ));

    $this->addElement('Select', 'example', array(
      'label' => 'Example',
      'multiOptions' => array(
        'city' => 'Night city',
        'guitarist' => 'Guitarist'
      )
    ));

    $this->addElement('File', 'background', array(
      'label' => 'Choose own background',
      'destination' => APPLICATION_PATH . '/public/temporary/',
      'multiFile' => 1,
      'validators' => array(
        /*array('Count', false, 1),*/
        // array('Size', false, 612000),
        array('Extension', false, 'jpg,jpeg,png,gif'),
      ),
      'description' => 'Recommended 1280x960'
    ));

    $this->addElement('File', 'logo', array(
      'label' => 'Choose new logo',
      'destination' => APPLICATION_PATH . '/public/temporary/',
      'multiFile' => 1,
      'validators' => array(
        /*array('Count', false, 1),*/
        // array('Size', false, 612000),
        array('Extension', false, 'jpg,jpeg,png,gif'),
      )
    ));

    $this->addElement('Text', 'title', array(
      'label' => 'Title',
      'allowEmpty' => false,
      'required' => true,
      'filters' => array(
        new Engine_Filter_Censor(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '255'))
      ),
      'autofocus' => 'autofocus',
    ));
	
	$this->addElement('Select', 'aligntitle', array(
	  'label' => 'Align Title',
	  'multiOptions' => array(
	    'left' => 'Left',
		'center' => 'Center',
		'right' => 'Right'
	  )
	));

    $upload_url = "";
/*    if (Engine_Api::_()->authorization()->isAllowed('album', $user, 'create')) {
      $upload_url = Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'upload-photo'), 'blog_general', true);
    }*/

    $this->addElement('TinyMce', 'body', array(
      'disableLoadDefaultDecorators' => true,
      'required' => true,
      'allowEmpty' => false,
      'decorators' => array(
        'ViewHelper'
      ),
      'editorOptions' => array(
        /*'upload_url' => $upload_url,*/
        'html' => (bool)1,
      ),
      'filters' => array(
        new Engine_Filter_Censor(),
        //new Engine_Filter_Html(array('AllowedTags' => $allowed_html))
      ),
    ));

    $this->addElement('Select', 'opacity', array(
      'label' => 'Widgets Transparent',
      'multiOptions' => array(
        100 => '0%',
        90 => '10%',
        80 => '20%',
        70 => '30%',
        60 => '40%',
        50 => '50%',
        40 => '60%',
        30 => '70%',
        20 => '80%',
        10 => '90%',
        1 => '100%'
      ),
      'value' => 60
    ));
	
	$page = Engine_Api::_()->getDbTable('pages', 'core')->fetchRow(array(
	  'name = ?' => 'home_index_index'
	));
	$link = '';
	if ($page){
      $link = $this->getView()->url(array(
		'module' => 'core', 
		'controller' => 'content', 
		'action' => 'index', 
		'page' => $page->getIdentity()
	  ), 'admin_default', true);
	}
	
	$this->addElement('Dummy', 'customize', array(
	  'content' => '<a href="'.$link.'" target="_blank">'.$this->getView()->translate('Customize Layout').'</a>'
	));
	
    $this->addElement('Checkbox', 'header', array(
      'label' => 'Display website header'
    ));
    $this->addElement('Checkbox', 'footer', array(
      'label' => 'Display website footer'
    ));
    $this->addElement('Checkbox', 'members', array(
      'label' => 'Display popular members'
    ));
	$this->addElement('Text', 'memberscount', array(
      'label' => 'Members Count',
	  'value' => '50',
	  'placeholder' => '50'
    ));
    $this->addElement('Checkbox', 'signup', array(
      'label' => 'Use quick signup form'
    ));

/*    $this->addElement('Text', 'color', array(
      'label' => 'Color',
      'class' => 'color'
    ));
    $this->addElement('Hidden', 'colorrgb', array(
      'label' => 'Color',
    ));*/

    $this->addElement('Button', 'done', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper'
      ),
    ));

    $this->addElement('Button', 'demo', array(
      'label' => 'See demo',
      'link' => true,
      'prependText' => ' or ',
      'onclick' => 'openDemo()',
      'style' => 'background: #314957;border: #314957 1px solid;',
      'decorators' => array(
        'ViewHelper'
      ),
    ));

    $this->addDisplayGroup(array('done', 'demo'), 'buttons');

  }
}