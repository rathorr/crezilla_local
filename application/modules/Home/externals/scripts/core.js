
// Center By Top
function homeCenter() {
  var el = $('global_content_simple');
  var result = ($$('body')[0].getHeight() / 2) - (el.getHeight() / 2) - 40; // -40px of padding-top
  if (result > 0) {
    el.setStyle('margin-top', result);
  }
}
window.addEvent('domready', function () {
  $$('html')[0].removeAttribute('id');
  $$('html')[0].setStyle('background', 'none');
  homeCenter();
});

window.addEvent('resize', function () {
  homeCenter();
});