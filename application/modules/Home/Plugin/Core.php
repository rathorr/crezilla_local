<?php

class Home_Plugin_Core {

  public function onRenderLayoutDefaultSimple($event)
  {
    $view = $event->getPayload();
    if( !($view instanceof Zend_View_Interface) ) {
      return;
    }
    if (Engine_Api::_()->getDbTable('settings', 'core')->getSetting('home.enabled')) {
      $view->headMeta()
          ->appendName('viewport', 'width=device-width, initial-scale=1.0');

    }
  }

  public function onRenderLayoutMobileDefaultSimple($event)
  {
    $view = $event->getPayload();
    if( !($view instanceof Zend_View_Interface) ) {
      return;
    }
    if (Engine_Api::_()->getDbTable('settings', 'core')->getSetting('home.enabled')) {
      $view->headMeta()
          ->appendName('viewport', 'width=device-width, initial-scale=1.0');
    }
  }
}