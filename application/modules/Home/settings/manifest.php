<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: manifest.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */ 
return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'home',
    'version' => '4.5.0p6',
    'path' => 'application/modules/Home',
    'title' => 'Custom Landing Page',
    'description' => 'Custom Landing Page',
    'author' => '<a href="http://luxervice.com" title="Luxervice LLC" target="_blank">Luxervice LLC/MisterWizard</a>',
    'meta' =>
    array (
      'title' => 'Custom Landing Page',
      'description' => 'Custom Landing Page',
      'author' => '<a href="http://luxervice.com" title="Luxervice LLC" target="_blank">Luxervice LLC/MisterWizard</a>',
    ),
    'callback' =>
    array (
      'path' => 'application/modules/Home/settings/install.php',
      'class' => 'Home_Installer',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Home',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/home.csv',
    ),
  ),
  // Hooks ---------------------------------------------------------------------
  'hooks' => array(
    array(
      'event' => 'onRenderLayoutDefaultSimple',
      'resource' => 'Home_Plugin_Core',
    ),
    array(
      'event' => 'onRenderLayoutMobileDefaultSimple',
      'resource' => 'Home_Plugin_Core',
    ),
  ),
  // Routes --------------------------------------------------------------------
); ?>