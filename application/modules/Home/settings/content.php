<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    File Info: content.php 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */
return array(
  array(
    'title' => 'Center Box',
    'description' => 'Title, description and logo',
    'category' => 'Custom Landing Page',
    'type' => 'widget',
    'name' => 'home.center-box',
    'defaultParams' => array(
    )
  ),
  array(
    'title' => 'Login',
    'description' => 'Login Form',
    'category' => 'Custom Landing Page',
    'type' => 'widget',
    'name' => 'home.login',
    'defaultParams' => array(
    )
  ),
  array(
    'title' => 'Popular Members',
    'description' => 'Shows thumbnails of popular members',
    'category' => 'Custom Landing Page',
    'type' => 'widget',
    'name' => 'home.popular-members',
    'defaultParams' => array(
    )
  ),
  array(
    'title' => 'Signup',
    'description' => 'Signup Form (opens when click on "join")',
    'category' => 'Custom Landing Page',
    'type' => 'widget',
    'name' => 'home.signup',
    'defaultParams' => array(
    )
  ),
  array(
    'title' => 'Header Menu',
    'description' => 'Standard Main menu',
    'category' => 'Custom Landing Page',
    'type' => 'widget',
    'name' => 'home.menu-main',
    'defaultParams' => array(
    )
  ),
  array(
    'title' => 'Footer Menu',
    'description' => 'Standard Footer Menu',
    'category' => 'Custom Landing Page',
    'type' => 'widget',
    'name' => 'home.menu-footer',
    'defaultParams' => array(
    )
  ),
) ?>