<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: profile.tpl 9984 2013-03-20 00:00:04Z john $
 * @author     John
 */
?>

<style type="text/css">

    .layout_main .layout_middle {
        width: 100% !important;
       background-color: #fff !important;
    }
    
</style>

<div class="layout_page_messages_messages_classifiedcompose">
<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_messages_menu">

</div>

<div class="generic_layout_container layout_core_content">
<style>
/*Compose Message section start*/
 #compose-menu{display:none !important;}
 
 body#global_page_messages-messages-classifiedcompose .login_my_wrapper #global_content .layout_top, body#global_page_messages-messages-classifiedcompose .login_my_wrapper #global_content .layout_main{border:0px; background-color:#fff;}
body#global_page_messages-messages-classifiedcompose .generic_layout_container.layout_main{padding:0px;}
body#global_page_messages-messages-classifiedcompose .login_my_wrapper #global_content .layout_main .layout_middle{width:100%;}
body#global_page_messages-messages-classifiedcompose #global_content h2 {color: #666; font-family: robotoregular;font-size: 22px;font-weight: 700;margin: 0;padding: 10px 20px;text-transform: capitalize;}
body#global_page_messages-messages-classifiedcompose .generic_layout_container.layout_core_content {clear: both; padding: 0 20px;}
body#global_page_messages-messages-classifiedcompose .generic_layout_container > h3{background: #189ee6 none repeat scroll 0 0; border-radius: 3px; border-top: 0 none; color: #fff; text-shadow: none; width: 130px; text-align: center;}
body#global_page_messages-messages-classifiedcompose form#messages_compose{ background-color:#FFFFFF !important;}
body#global_page_messages-messages-classifiedcompose form#messages_compose .form-wrapper .form-element {width: 100%;}
body#global_page_messages-messages-classifiedcompose form#messages_compose .form-wrapper .form-element input {width: 99%; max-width:100%; border-radius:3px; border:1px solid #dbdbdb; background-color:#fafafa; padding:6px;}
body#global_page_messages-messages-classifiedcompose form#messages_compose .form-wrapper .form-element textarea {width: 64.5% !important; max-width:100%; border-radius:3px; border:1px solid #dbdbdb; background-color:#fafafa; padding:6px;}
body#global_page_messages-messages-classifiedcompose form#messages_compose .form-wrapper .form-element label{color:#5a5a5a !important; font-size:14px !important; padding: 6px 0 !important;}

body#global_page_messages-messages-classifiedcompose .generic_layout_container div > div .form-elements #to-element{width:100%;}
body#global_page_messages-messages-classifiedcompose form#messages_compose .form-elements #body-wrapper #body-element textarea#body{width: 100% !important;}

body#global_page_messages-messages-classifiedcompose form#messages_compose .form-elements #submit_compose-wrapper{margin-left:0px !important;}

body#global_page_messages-messages-classifiedcompose form#messages_compose .com_form_error_validatin + label.overTxtLabel {
    color: #fff !important;
}
 body#global_page_messages-messages-classifiedcompose #body-wrapper #body-label{display:block;}
 
 body#global_page_messages-messages-classifiedcompose #submit_form-element button#submit_form {
    background-color: #3faee8;
    border-radius: 5px;
    padding: 10px 50px;
}
body#global_page_messages-messages-classifiedcompose #submit_form-element button#submit_form:hover  {background-color:#34a3dd;}

body#global_page_messages-messages-classifiedcompose .compose-container {
    border: 1px solid #dbdbdb;
    border-radius: 3px;
    width: 100%; padding: 0;}
  
  body#global_page_messages-messages-classifiedcompose .compose-container .compose-content {
    background-color: #fafafa;
    width: 100%;
    display: none !important;}

 /*Compose Message section end*/

.generic_layout_container .layout_core_content{
  background-color: #e9eaed;     min-height: 600px;     padding: 2% 4% !important;     width: 100%;
}
.tag{padding-left:10px !important;}
body#global_page_messages-messages-classifiedcompose form#messages_compose .form-elements #body-wrapper #body-element textarea#body{ display:block !important;}
</style>




<script type="text/javascript" src="/public/js/jquery.min.js"></script>

  <script type="text/javascript">
  var jQuery  = $.noConflict();
  </script>
<script type="text/javascript">

   function validatecompose(){
   console.log('clicked');
  //var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
  if(jQuery.trim(jQuery("#toValues").val()) == ''){
      //$("#signinerror").html("Email is required.");
      //$("#signinerror").show();
      console.log('toValues');
      jQuery("#to").focus();
      jQuery("#to").addClass("com_form_error_validatin");
      //$("#signinerror").fadeOut(7000);
      return false;
  }
  //var message = jQuery.trim(jQuery(".compose-content").text());
  var body = jQuery.trim(jQuery("#body").val());

  if(body == ''){
    console.log('content');
    console.log('body' +body);
      //$("#signinerror").html("Password is required.");
      //$("#signinerror").show();
      jQuery(".compose-content2").focus();
      jQuery(".compose-content2").addClass("com_form_error_validatin");
      //$("#signinerror").fadeOut(7000);
      return false;
  }
  jQuery('#messages_compose').submit();
    
  }
</script>
<style>

.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
.global_form > div > div > h3 + p {
  max-width:100%;
}
</style>

<form id="messages_compose" enctype="application/x-www-form-urlencoded" class="global_form" action="/messages/compose" method="post">
<div>
<div>
<h3>Compose Message</h3>
<p class="form-description">Create your new message with the form below to contact for Classifed you have choosen.</p>
<div class="form-elements">
<div id="to-wrapper" class="form-wrapper">
  <div id="to-label" class="form-label">
  <label for="to" class="optional">Send To</label>
  </div>
  <div id="to-element" class="form-element">
  <input type="text" name="to" id="to" readonly="readonly" value="<?php echo $name=($this->Clsowner_name ?: '');?>" placeholder="name" autocomplete="off">
  </div>
</div>
<div id="toValues-wrapper" class="form-wrapper" style="height: auto;">
  <div id="toValues-label" class="form-label">
  <label for="toValues" class="required">Send To</label>
  </div>
  <div id="toValues-element" class="form-element">
  <input type="hidden" name="toValues" value="<?php echo $uid=($this->Clsowner_id ?: '');?>" id="toValues">
  </div>
</div>
<div id="title-wrapper" class="form-wrapper">
  <div id="title-label" class="form-label">
  <label for="title" class="optional">Subject</label>
  </div>
  <div id="title-element" class="form-element">
  <input type="text" readonly="readonly" placeholder="Enter Subject here" name="title" id="title" value="<?php echo $Classified_title=($this->title ?: '');?>">
  </div>
</div>
<div id="body-wrapper" class="form-wrapper">
  <div id="body-label" class="form-label">
  <label for="body" class="optional">Message</label>
  </div>
  <div id="body-element" class="form-element">
  <div id="compose-container" class="compose-container">
  <div class="compose-content" contenteditable="true" style="display: block;">
  <br></div>
  <textarea name="body" id="body" cols="45" rows="6" class="compose-content2 compose-textarea" placeholder="Enter message here" style="display: none;"></textarea>
  </div>
  </div>
</div>
  <div id="submit_compose-wrapper" class="form-wrapper">
  <div id="submit_compose-label" class="form-label">&nbsp;</div>
  <div id="submit_compose-element" class="form-element">
  <button name="submit_compose" id="submit_compose" type="button" onclick="validatecompose()">Send Message</button>
  </div>
  </div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<script type="text/javascript">
var is_image = '<?php echo $this->photoDisplay ?>';
jQuery(document).ready(function(){
       jQuery('.loading_image').hide();
		/*jQuery.fancybox({
            'width': '500px',
            'height': '200px',
            //'autoScale': true,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'type': 'iframe',
            'href': '<?php echo $this->baseUrl()."/members/edit/pathapproval";?>'
        });*/
	});
jQuery('#submitclassified').click(function(){

	if(jQuery("#name").val()=="")
	{
		alert("Please fill the Classified Title!");
		jQuery("#name").focus();
		return false;
	}
	if(jQuery("#cads_body").val()=="")
	{
		alert("Please fill the Classified Description!");
		jQuery("#name").focus();
		return false;
	}
       jQuery('.loading_image').show();
	
    });
jQuery("form").bind("keypress", function (e) {
    if (e.keyCode == 13) {
       // $("#btnSearch").attr('value');
        //add more buttons here
        console.log('enter');
        return false;
    }
});
 
</script>

