<style>
/*Compose Message section start*/
 #compose-menu{display:none !important;}
 
 body#global_page_messages-messages-compose .login_my_wrapper #global_content .layout_top, body#global_page_messages-messages-compose .login_my_wrapper #global_content .layout_main{border:0px; background-color:#fff;}
body#global_page_messages-messages-compose .generic_layout_container.layout_main{padding:0px;}
body#global_page_messages-messages-compose .login_my_wrapper #global_content .layout_main .layout_middle{width:100%;}
body#global_page_messages-messages-compose #global_content h2 {color: #666; font-family: robotoregular;font-size: 22px;font-weight: 700;margin: 0;padding: 10px 20px;text-transform: capitalize;}
body#global_page_messages-messages-compose .generic_layout_container.layout_core_content {clear: both; padding: 0 20px;}
body#global_page_messages-messages-compose .generic_layout_container > h3{background: #189ee6 none repeat scroll 0 0; border-radius: 3px; border-top: 0 none; color: #fff; text-shadow: none; width: 130px; text-align: center;}
body#global_page_messages-messages-compose form#messages_compose{ background-color:#FFFFFF !important;}
body#global_page_messages-messages-compose form#messages_compose .form-wrapper .form-element {width: 100%;}
body#global_page_messages-messages-compose form#messages_compose .form-wrapper .form-element input {width: 99%; max-width:100%; border-radius:3px; border:1px solid #dbdbdb; background-color:#fafafa; padding:6px;}
body#global_page_messages-messages-compose form#messages_compose .form-wrapper .form-element textarea {width: 64.5% !important; max-width:100%; border-radius:3px; border:1px solid #dbdbdb; background-color:#fafafa; padding:6px;}
body#global_page_messages-messages-compose form#messages_compose .form-wrapper .form-element label{color:#5a5a5a !important; font-size:14px !important; padding: 6px 0 !important;}

body#global_page_messages-messages-compose .generic_layout_container div > div .form-elements #to-element{width:100%;}
body#global_page_messages-messages-compose form#messages_compose .form-elements #body-wrapper #body-element textarea#body{width: 100% !important;}

body#global_page_messages-messages-compose form#messages_compose .form-elements #submit_compose-wrapper{margin-left:0px !important;}

body#global_page_messages-messages-compose form#messages_compose .com_form_error_validatin + label.overTxtLabel {
    color: #fff !important;
}
 body#global_page_messages-messages-compose #body-wrapper #body-label{display:block;}
 
 body#global_page_messages-messages-compose #submit_form-element button#submit_form {
    background-color: #3faee8;
    border-radius: 5px;
    padding: 10px 50px;
}
body#global_page_messages-messages-compose #submit_form-element button#submit_form:hover  {background-color:#34a3dd;}

body#global_page_messages-messages-compose .compose-container {
    border: 1px solid #dbdbdb;
    border-radius: 3px;
    width: 100%; padding: 0;}
  
  body#global_page_messages-messages-compose .compose-container .compose-content {
    background-color: #fafafa;
    width: 100%;
    display: none !important;}

 /*Compose Message section end*/

.generic_layout_container .layout_core_content{
  background-color: #e9eaed;     min-height: 600px;     padding: 2% 4% !important;     width: 100%;
}
.tag{padding-left:10px !important;}
body#global_page_messages-messages-compose form#messages_compose .form-elements #body-wrapper #body-element textarea#body{ display:block !important;}
</style>
<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Messages
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: compose.tpl 10224 2014-05-15 18:45:45Z lucas $
 * @author     John
 */
?>

<?php
  if (APPLICATION_ENV == 'production')
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.min.js');
  else
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');
?>

<script type="text/javascript">

  // Populate data
  var maxRecipients = <?php echo sprintf("%d", $this->maxRecipients) ?> || 10;
  var to = {
    id : false,
    type : false,
    guid : false,
    title : false
  };
  var isPopulated = false;

  <?php if( !empty($this->isPopulated) && !empty($this->toObject) ): ?>
    isPopulated = true;
    to = {
      id : <?php echo sprintf("%d", $this->toObject->getIdentity()) ?>,
      type : '<?php echo $this->toObject->getType() ?>',
      guid : '<?php echo $this->toObject->getGuid() ?>',
      title : '<?php echo $this->string()->escapeJavascript($this->toObject->getTitle()) ?>'
    };
  <?php endif; ?>
  
  function removeFromToValue(id) {
    // code to change the values in the hidden field to have updated values
    // when recipients are removed.
    var toValues = $('toValues').value;
    var toValueArray = toValues.split(",");
    var toValueIndex = "";

    var checkMulti = id.search(/,/);

    // check if we are removing multiple recipients
    if (checkMulti!=-1){
      var recipientsArray = id.split(",");
      for (var i = 0; i < recipientsArray.length; i++){
        removeToValue(recipientsArray[i], toValueArray);
      }
    }
    else{
      removeToValue(id, toValueArray);
    }

    // hide the wrapper for usernames if it is empty
    if ($('toValues').value==""){
      $('toValues-wrapper').setStyle('height', '0');
    }

    $('to').disabled = false;
  }

  function removeToValue(id, toValueArray){
    for (var i = 0; i < toValueArray.length; i++){
      if (toValueArray[i]==id) toValueIndex =i;
    }

    toValueArray.splice(toValueIndex, 1);
    $('toValues').value = toValueArray.join();
  }

  en4.core.runonce.add(function() {
    if( !isPopulated ) { // NOT POPULATED
      new Autocompleter.Request.JSON('to', '<?php echo $this->url(array('module' => 'user', 'controller' => 'friends', 'action' => 'suggest','message' => true), 'default', true) ?>', {
        'minLength': 1,
        'delay' : 250,
        'selectMode': 'pick',
        'autocompleteType': 'message',
        'multiple': false,
        'className': 'message-autosuggest',
        'filterSubset' : true,
        'tokenFormat' : 'object',
        'tokenValueKey' : 'label',
    'keycommand':'none',
        'injectChoice': function(token){
          if(token.type == 'user'){
            var choice = new Element('li', {
              'class': 'autocompleter-choices',
              'html': token.photo,
              'id':token.label
            });
            new Element('div', {
              'html': this.markQueryValue(token.label),
              'class': 'autocompleter-choice'
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
          else {
            var choice = new Element('li', {
              'class': 'autocompleter-choices friendlist',
              'id':token.label
            });
            new Element('div', {
              'html': this.markQueryValue(token.label),
              'class': 'autocompleter-choice'
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
            
        },
        onPush : function(){
          if( $('toValues').value.split(',').length >= maxRecipients ){
            $('to').disabled = true;
          }
        }
      });
      
      new Composer.OverText($('to'), {
        'textOverride' : '',
    /*'textOverride' : '<?php echo $this->translate('Recipients') ?>',*/
        'element' : 'label',
        'isPlainText' : true,
        'positionOptions' : {
          position: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
          edge: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
          offset: {
            x: ( en4.orientation == 'rtl' ? -4 : 4 ),
            y: 2
          }
        }
      });

    } else { // POPULATED

      var myElement = new Element("span", {
        'id' : 'tospan' + to.id,
        'class' : 'frnd tag tag_' + to.type,
        'html' :  to.title /* + ' <a href="javascript:void(0);" ' +
                  'onclick="this.parentNode.destroy();removeFromToValue("' + toID + '");">x</a>"' */
      });
      $('to-element').appendChild(myElement);
      $('to-wrapper').setStyle('height', 'auto');

      // Hide to input?
      $('to').setStyle('display', 'none');
      $('toValues-wrapper').setStyle('display', 'none');
    }
  });
  


</script>

<?php
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/mdetect/mdetect' . ( APPLICATION_ENV != 'development' ? '.min' : '' ) . '.js')
      ->appendFile($this->layout()->staticBaseUrl . 'application/modules/Core/externals/scripts/composer.js');
?>
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/jquery.min.js'?>"></script>

  <script type="text/javascript">
  var jQuery  = $.noConflict();
  </script>
<script type="text/javascript">
  var composeInstance;
  en4.core.runonce.add(function() {
    var tel = new Element('div', {
      'id' : 'compose-tray',
      'styles' : {
        'display' : 'none'
      }
    }).inject($('submit'), 'before');

    var mel = new Element('div', {
      'id' : 'compose-menu'
    }).inject($('submit'), 'after');

    // @todo integrate this into the composer
    if ( '<?php 
         $id = Engine_Api::_()->user()->getViewer()->level_id;
         echo Engine_Api::_()->getDbtable('permissions', 'authorization')->getAllowed('messages', $id, 'editor');
         ?>' == 'plaintext' ) {
      if( !Browser.Engine.trident && !DetectMobileQuick() && !DetectIpad() ) {
        composeInstance = new Composer('body', {
          overText : false,
          menuElement : mel,
          trayElement: tel,
          baseHref : '<?php echo $this->baseUrl() ?>',
          hideSubmitOnBlur : false,
          allowEmptyWithAttachment : false,
          submitElement: 'submit',
          type: 'message'
        });
      }
    }
  });
  
   function validatecompose(){
   console.log('clicked');
  //var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
  if(jQuery.trim(jQuery("#toValues").val()) == ''){
      //$("#signinerror").html("Email is required.");
      //$("#signinerror").show();
      console.log('toValues');
      jQuery("#to").focus();
      jQuery("#to").addClass("com_form_error_validatin");
      //$("#signinerror").fadeOut(7000);
      return false;
  }
  //var message = jQuery.trim(jQuery(".compose-content").text());
  var body = jQuery.trim(jQuery("#body").val());

  if(body == ''){
    console.log('content');
    console.log('body' +body);
      //$("#signinerror").html("Password is required.");
      //$("#signinerror").show();
      jQuery(".compose-content2").focus();
      jQuery(".compose-content2").addClass("com_form_error_validatin");
      //$("#signinerror").fadeOut(7000);
      return false;
  }
  jQuery('#messages_compose').submit();
    
  }
</script>
<style>

.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
.global_form > div > div > h3 + p {
  max-width:100%;
}
</style>
<?php foreach( $this->composePartials as $partial ): ?>
  <?php echo $this->partial($partial[0], $partial[1]) ?>
<?php endforeach; ?>

<?php echo $this->form->render($this) ?>