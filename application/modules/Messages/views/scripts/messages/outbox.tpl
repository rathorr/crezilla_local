<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Messages
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: outbox.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style>
#message_search {
    background-color: #2ab9b7;
    background-image: url("<?php echo $this->baseUrl();?>/public/custom/images/message_search_icon1.png");
    background-position: 92px 7px;
    background-repeat: no-repeat;
    border: 0 none;
    border-radius: 3px;
    color: #fff;
    font-family: robotoregular;
    padding: 4px;
    text-align: left;
    background-position: center center; padding: 4px 15px;
}
.generic_layout_container .layout_core_content{
	background-color: #e9eaed;     min-height: 600px;     padding: 2% 4% !important;     width: 100%;
}
</style>
<div>
  <?php echo $this->translate(array('%2$s message sent', '%2$s messages sent', $this->unread),
                              $this->locale()->toNumber($this->unread),
                              $this->locale()->toNumber($this->paginator->getTotalItemCount())) ?>
  <?php if( count($this->paginator) ): ?>
    <a href="javascript:void(0)" id="checkall"><?php echo $this->translate('Check All') ?></a>
  <?php endif; ?>
  
  
  <form action="messages/outboxsearch" method="GET" class="outbox-search inbox-search-form">
    <input style="background-color: #fafafa;border: 1px solid #dbdbdb; border-radius: 3px;" class="inbox-search" type="text" placeholder="<?php echo $this->translate('Search') ?>" name="query" value="<?php echo htmlspecialchars(!empty($this->queryStr) ? $this->queryStr : '', ENT_QUOTES, 'UTF-8') ?>"  />
     <input type="submit" id="message_search" value=""/>
  </form>
  
  
  
</div>
<br />
<?php if( count($this->paginator) ): ?>
  <div class="messages_list">
    <ul>
      <?php foreach( $this->paginator as $conversation ):
        $message = $conversation->getOutboxMessage($this->viewer());
        $recipient = $conversation->getRecipientInfo($this->viewer());
        $resource = "";
        $sender   = "";
        if( $conversation->hasResource() &&
                  ($resource = $conversation->getResource()) ) {
          $sender = $resource;
        } else if( $conversation->recipients > 1 ) {
          $sender = $this->viewer();
        } else {
          foreach( $conversation->getRecipients() as $tmpUser ) {
            if( $tmpUser->getIdentity() != $this->viewer()->getIdentity() ) {
              $sender = $tmpUser;
              break;
            }
          }
        }
        if( (!isset($sender) || !$sender) ){
          if( $this->viewer()->getIdentity() !== $conversation->user_id ){
            $sender = Engine_Api::_()->user()->getUser($conversation->user_id);
          } else {
            $sender = $this->viewer();
          }
        }
        if( !isset($sender) || !$sender ) {
          //continue;
          $sender = new User_Model_User(array());
        }
        ?>
        <li<?php if( !$recipient->inbox_read ): ?> class='messages_list_new'<?php endif; ?> id="message_conversation_<?php echo $conversation->getIdentity() ?>">
          <div class="messages_list_checkbox">
            <input class="checkbox" type="checkbox" value="<?php echo $conversation->getIdentity() ?>" />
          </div>
          <div class="messages_list_photo">
            <?php echo $this->htmlLink($sender->getHref(), $this->itemPhoto($sender, 'thumb.icon')) ?>
          </div>
          <div class="messages_list_from">
            <p class="messages_list_from_name">
              <?php if( !empty($resource) ): ?>
                <?php echo $resource->toString() ?>
              <?php elseif( $conversation->recipients == 1 ): ?>
                <?php echo $this->htmlLink($sender->getHref(), $sender->getTitle()) ?>
              <?php else: ?>
                <?php echo $this->translate(array('%s person', '%s people', $conversation->recipients),
                    $this->locale()->toNumber($conversation->recipients)) ?>
              <?php endif; ?>
            </p>
            <p class="messages_list_from_date">
              <?php echo $this->timestamp($message->date) ?>
            </p>
          </div>
          <div class="messages_list_info">
            <p class="messages_list_info_title">
              <?php
                // ... scary
                ( (isset($message) && '' != ($title = trim($message->getTitle()))) ||
                  (isset($conversation) && '' != ($title = trim($conversation->getTitle()))) ||
                  $title = '<em>' . $this->translate('(No Subject)') . '</em>' );
              ?>
               <?php echo $this->htmlLink($conversation->getHref(), substr($title,0,80)) ?>
            </p>
            <p class="messages_list_info_body">
               <?php echo substr(html_entity_decode($message->body),0,250).'..'; ?>
            </p>
          </div>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>

  <br />
  <button id="delete"><?php echo $this->translate('Delete Selected');?></button>
  <script type="text/javascript">
    $$('.messages_list').enableLinks();
  <!--
   $('checkall').addEvent('click', function() {
      var hasUnchecked = false;
      $$('.messages_list input[type="checkbox"]').each(function(el) {
        if( !el.get('checked') ) {
          hasUnchecked = true;
        }
      });
      $$('.messages_list input[type="checkbox"]').set('checked', hasUnchecked);
    });
  $('delete').addEvent('click', function(){
    var selected_ids = new Array();
    $$('div.messages_list input[type=checkbox]').each(function(cBox) {
      if (cBox.checked)
        selected_ids[ selected_ids.length ] = cBox.value;
    });
    var sb_url = '<?php echo $this->url(array('action'=>'delete'), 'messages_general', true) ?>?place=outbox&message_ids='+selected_ids.join(',');
    if (selected_ids.length > 0)
      Smoothbox.open(sb_url);
  });
  //-->
  </script>
  <br />
  <br />

<?php else: ?>
  <p><?php echo $this->translate(array('You have %s sent message total', 'You have %s sent messages total', $this->paginator->getTotalItemCount()), $this->locale()->toNumber($this->paginator->getTotalItemCount())) ?></p>
  <br />
  <div class="tip">
    <span>
      <?php echo $this->translate('Tip: %1$sClick here%2$s to send your first message!', "<a href='".$this->url(array('action' => 'compose'), 'messages_general')."'>", '</a>'); ?>
    </span>
  </div>
<?php endif; ?>

<?php echo $this->paginationControl($this->paginator); ?>
