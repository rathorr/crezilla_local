<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Messages
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Menus.php 9864 2013-02-12 03:05:01Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Messages
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Messages_Plugin_Menus
{
  // core_mini
  
  public function onMenuInitialize_CoreMiniMessages($row)
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    if( !$viewer->getIdentity() )
    {
      return false;
    }

    // Get permission setting
    $permission = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'messages', 'create');
    if( Authorization_Api_Core::LEVEL_DISALLOW === $permission )
    {
      return false;
    }

    $message_count = Engine_Api::_()->messages()->getUnreadMessageCount($viewer);
    $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl() . '/';

    return array(
      'label' => '<span class="'.( $message_count ? 'msg_update' : 'no_count' ).'" >'.( $message_count ?  $message_count  : '0' ).'</span><span class="message_icon" '.( $message_count ? 'style="color:#fff;"' : '' ).'></span>',
      'route' => 'messages_general',
      'params' => array(
        'action' => 'inbox'
      )
    );
  }



  // user_profile

  public function onMenuInitialize_UserProfileMessage($row)
  {
    // Not logged in
    $viewer = Engine_Api::_()->user()->getViewer();
    $subject = Engine_Api::_()->core()->getSubject();
	
    if( !$viewer->getIdentity() || $viewer->getGuid(false) === $subject->getGuid(false) ) {
      return false;
    }

    // Get setting?
    $permission = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'messages', 'create');
    if( Authorization_Api_Core::LEVEL_DISALLOW === $permission )
    {
      return false;
    }
    $messageAuth = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'messages', 'auth');
    if( $messageAuth == 'none' ) {
      return false;
    } else if( $messageAuth == 'friends' ) {
      // Get data
      $direction = (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('user.friends.direction', 1);
      if( !$direction ) {
        //one way
        $friendship_status = $viewer->membership()->getRow($subject);
      }
      else $friendship_status = $subject->membership()->getRow($viewer);
		
		
		
      if( !$friendship_status || $friendship_status->active == 0 ) {
        return false;
      }
    }
	
	
	//$is_friend	=	Engine_Api::_()->getDbtable('friendship', 'user')->isFriend($viewer->getIdentity(), $subject->getIdentity());
	//if($subject->membership()->isMember($viewer)){echo 'if';}else{echo 'else';}die;
    if(($subject->msg_setting == 2 && !$subject->membership()->isMember($viewer)) || $subject->msg_setting == 3){
		return false;	
	}
	
		return array(
		  'label' => "Send Message",
		  'icon' => 'application/modules/Messages/externals/images/send.png',
		  'route' => 'messages_general',
		  'class' => 'smoothbox',
		  'params' => array(
			'action' => 'sendmsg',
			'to' => $subject->getIdentity(),
			'format' => 'smoothbox',
		  ),
		);
  }
}
