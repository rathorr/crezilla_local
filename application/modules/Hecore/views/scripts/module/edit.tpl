<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hecore
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: edit.tpl 2010-07-02 17:53 ermek $
 * @author     Ermek
 */
?>

<div class="global_form_popup settings form_license_key_error">

  <div style="padding:20px;">
    <h3><?php echo $this->translate('Sorry, but this module can\t be edited with the present version of Hire-Experts Core.')?></h3>
  </div>

</div>