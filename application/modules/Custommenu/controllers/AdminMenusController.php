<?php

class Custommenu_AdminMenusController extends Core_Controller_Action_Admin {

    const CHECK_ACCESS_METHOD = 'Custommenu_Plugin_Menus::checkAccess';
        
    protected $_menus;


    public function init() {
        $menusTable = Engine_Api::_()->getDbtable('menus', 'core');
        $menusSelect = $menusTable->select();
        $this->view->menus = $this->_menus = $menusTable->fetchAll($menusSelect);

        $this->_enabledModuleNames = Engine_Api::_()->getDbtable('modules', 'core')->getEnabledModuleNames();
    }

    public function indexAction() {
        $this->view->name = $name = $this->_getParam('name', 'core_main');

        $menus = $this->_menus;

        // Check if selected menu is in list
        $selectedMenu = $menus->getRowMatching('name', $name);

        if (null === $selectedMenu) {
            return $this->_forward('success', 'utility', 'core', array(
                        'parentRedirect' => Zend_Controller_Front::getInstance()->getBaseUrl() . '/admin/custommenu/menus/index',
                        'messages' => $this->view->translate('Menu doesn\'t exist'),
                        'name' => 'core-main'
                    ));
        }
        $this->view->selectedMenu = $selectedMenu;

        $menuList = array();

        foreach ($menus as $menu) {
            $menuList[$menu->name] = $this->view->translate($menu->title);
        }

        $this->view->menuList = $menuList;

        $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');

        $menuItemsSelect = $menuItemsTable
                ->select()
                ->where('menu = ?', $name)
                ->order('order');

        if (!empty($this->_enabledModuleNames)) {
            $menuItemsSelect->where('module IN(?)', $this->_enabledModuleNames);
        }

        $this->view->menuItems = $menuItems = $menuItemsTable->fetchAll($menuItemsSelect);

        // Get subitems
        $childItems = array();

        if (!empty($menuItems)) {

            foreach ($menuItems as $item) {

                if (!is_null($item->submenu)) {
                    $children = $menuItemsTable
                            ->select()
                            ->where('menu = ?', $item->submenu)
                            ->order('order');

                    $childItems[$item->name] = $menuItemsTable->fetchAll($children);
                }
            }
        }


        $this->view->childItems = $childItems;
    }

    /**
     * Create menu item
     * @return type
     * @throws Core_Model_Exception
     */   
    public function createAction() {
        $this->view->name = $name = $this->_getParam('name', 'core_main');

        // Get list of menus
        $menus = $this->_menus;

        // Check if selected menu is in list
        $selectedMenu = $menus->getRowMatching('name', $name);
                        
        if (null === $selectedMenu) {
            throw new Core_Model_Exception('Invalid menu name');
        }
        $this->view->selectedMenu = $selectedMenu;

        // Get form
        $this->view->form = $form = new Custommenu_Form_Admin_Item(); 
        
        // Rename default form title 
        $form->setTitle('Create Menu Item');
        $form->submit->setLabel('Create Menu Item');
        
        // Check stuff
        if (!$this->getRequest()->isPost()) {
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        // Save
        $values = $form->getValues();
        $label = $values['label'];
        unset($values['label']);

        $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');

        $menuItem = $menuItemsTable->createRow();
        $menuItem->label = $label;
        $menuItem->params = $values;
        $menuItem->menu = $name;
        $menuItem->module = 'core'; // Need to do this to prevent it from being hidden
        $menuItem->plugin = self::CHECK_ACCESS_METHOD;
        $menuItem->submenu = '';
        $menuItem->custom = 1;        

        $menuItem->name = 'custommenu_' . sprintf('%s', uniqid());
        $menuItem->save();

        $this->view->status = true;
        $this->view->form = null;
    }
    
    /**
     * Edit menu item
     * @return type
     * @throws Core_Model_Exception
     */
    public function editAction() {                   
        $this->view->name = $name = $this->_getParam('name');
                                
        // Get menu item
        $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');
        $menuItemsSelect = $menuItemsTable->select()->where('name = ?', $name);
        
        if (!empty($this->_enabledModuleNames)) {
            $menuItemsSelect->where('module IN(?)', $this->_enabledModuleNames);
        }
        
        $this->view->menuItem = $menuItem = $menuItemsTable->fetchRow($menuItemsSelect);
                         
        $this->view->form = $form = new Custommenu_Form_Admin_Item();        
                  
        if (!empty($menuItem->params['itemtype'])) {
            $form->setSpecials();               
        }
        
        if (!$menuItem) {            
            return $this->_forward('success', 'utility', 'core', array(
                'parentRefresh' => true,
                'messages'      => $this->view->translate('Menu item doesn\'t exist. Enable plugin.')
            ));
        }
 
        // Make safe
        $menuItemData = $menuItem->toArray();
                                               
        if (isset($menuItemData['params']) && is_array($menuItemData['params'])) {
            $menuItemData = array_merge($menuItemData, $menuItemData['params']);
        }
        if (!$menuItem->custom) {
            $form->removeElement('uri');
        }
        unset($menuItemData['params']);
               
        // Check stuff
        if (!$this->getRequest()->isPost()) {                        
            $form->populate($menuItemData);
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }
   
        $values = $form->getValues(); 
        
        // Save
        $menuItem->label = $values['label'];
        $menuItem->enabled = !empty($values['enabled']);
        
        unset($values['label']);
        unset($values['enabled']);
        
        if (empty($menuItem->params)) {
            $menuItem->params = array_merge($menuItem->params, $values);            
        } else {
            $tempParams = array();
            foreach ($values as $key => $val) {
                $tempParams[$key] = $val;
            }    
            $menuItem->params = array_merge($menuItem->params, $tempParams);
            
        }
        
        if (!empty($values['target'])) {
            $menuItem->params = array_merge($menuItem->params, array('target' => $values['target']));
        } else if (isset($menuItem->params['target'])) {
            // Remove the target
            $tempParams = array();
            foreach ($menuItem->params as $key => $item) {
                if ($key != 'target') {
                    $tempParams[$key] = $item;
                }
            }
            $menuItem->params = $tempParams;
        }
        
        if (!empty($values['icon'])) {
            $menuItem->params = array_merge($menuItem->params, array('icon' => $values['icon']));
        } else if (isset($menuItem->params['icon'])) {
            // Remove the target
            $tempParams = array();
            foreach ($menuItem->params as $key => $item) {
                if ($key != 'icon') {
                    $tempParams[$key] = $item;
                }
            }
            $menuItem->params = $tempParams;
        }
        
        unset($values['uri']);            
        unset($values['icon']);
        unset($values['target']);
                        
        $menuItem->save();

        $this->view->status = true;
        $this->view->form = null;
        
        return $this->_forward('success', 'utility', 'core', array(
            'parentRefresh' => true,
            'messages'      => $this->view->translate('Menu item successfully updated')
        ));
    }    
    
    /**
     * Reordering 
     * @return void
     */
    public function orderAction() {               
                
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if (!$this->getRequest()->isPost()) {
            return;
        }
                        
        $menuName = $this->getRequest()->getParam('menu');

        $parentName = $this->getRequest()->getParam('parent');
        
        // when move item back to the 1-st level
        $goBackId = $this->getRequest()->getParam('goBackId'); 
        
        $table = Engine_Api::_()->getDbtable('menuItems', 'core');
        
        if ($parentName) {
            $parentName  = str_replace('admin_menus_item_', '', $parentName);
            $subMenuName = "{$parentName}_submenu";
            $childName   = str_replace('admin_menus_item_', '', $this->getRequest()->getParam('child'));

            // parents                                       
            $menuitem = $table->fetchRow(
                $table->select()
                      ->where('menu = ?', $menuName)
                      ->where('name = ?', $parentName)
            );

            $menuitem->submenu = $subMenuName;
            $menuitem->save();
            
            // children
            $menuitem = $table->fetchRow(
                $table->select()                      
                      ->where('name = ?', $childName)                      
            );
            
            $menuitem->menu = $subMenuName;
            $menuitem->save();
                        
            $this->reorder($subMenuName, $table);
            $this->reorder($menuName, $table); 
            
        } elseif (!is_null($goBackId)) { 
            $goBackId = str_replace('admin_menus_item_', '', $goBackId);
            $menuitem = $table->fetchRow(
                $table->select()                      
                      ->where('name = ?', $goBackId)                      
            );
            
            $menuitem->menu = $menuName;
            $menuitem->save();
        }    
                    
        $this->reorder($menuName, $table);                        
        return;
    }
        
    /**
     * Help function to reoder items
     */
    private function reorder($menuName, $table) {
        $menuitems = $table->fetchAll($table->select()->where('menu = ?', $menuName));

        foreach ($menuitems as $menuitem) {

            $order = $this->getRequest()->getParam('admin_menus_item_' . $menuitem->name);

            if (!$order) {
                $order = 999;
            }

            $menuitem->order = $order;
            $menuitem->save();                                    
        }
    }
    
    /*
     * Delete item and subitems
     */       
    public function deleteAction() {        

        $this->view->name = $name = $this->_getParam('name');
        
        if ($this->getRequest()->isPost()) {
            $table = Engine_Api::_()->getDbtable('menuItems', 'core');
            $db = $table->getAdapter();

            $itemParent = $table->fetchRow(
                $table->select()
                    ->where('name = ?', $name)
            );

            if (is_null($itemParent)) {
                return $this->_forward('success', 'utility', 'core', array(
                    'parentRefresh' => true,
                    'messages'      => $this->view->translate('Menu item doesn\'t exist')
                ));
            }

            $menuName = "{$name}_submenu"; 

            $itemsChildren = $table->fetchAll(
                $table->select()
                      ->where('menu = ?', $menuName)        
            );
            
            $db->beginTransaction();

            try {
                $itemParent->delete();
                // if exist a submenu of current item                
                if ($itemParent->submenu) {
                    foreach ($itemsChildren as $child) {
                        $child->delete();
                    }
                }
                
                $db->commit();

                return $this->_forward('success', 'utility', 'core', array(
                    'parentRefresh' => true,
                    'messages'      => $this->view->translate('Menu item successfully deleted')
                ));
                
            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }
        }
    }

    public function deleteMenuAction() {
        $name = $this->_getParam('name');

        if ($this->getRequest()->isPost()) {
            $menuTable = Engine_Api::_()->getDbtable('menus', 'core');
            $db = $menuTable->getAdapter();

            $menuRow = $menuTable->fetchRow(
                $menuTable->select()
                    ->where('name = ?', $name)
            );
                                    
            if (is_null($menuRow)) {
                return $this->_forward('success', 'utility', 'core', array(
                    'parentRefresh' => true,
                    'messages'      => $this->view->translate('Menu doesn\'t exist')
                ));
            }

            $subMenu = "{$name}_submenu";

            $itemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');

            $itemParent = $itemsTable->fetchRow(
                $itemsTable->select()
                    ->where('menu = ?', $name)
            );

            $itemsChildren = $itemsTable->fetchAll(
                $itemsTable->select()
                    ->where('submenu = ?', $subMenu)
            );

            $db->beginTransaction();

            try {
                $menuRow->delete();

                if (!is_null($itemParent)) {
                    $itemParent->delete();
                }

                if (!is_null($itemsChildren)) {
                    foreach ($itemsChildren as $child) {
                        $child->delete();
                    }
                }

                $db->commit();

                return $this->_forward('success', 'utility', 'core', array(
                    'parentRedirect' => Zend_Controller_Front::getInstance()->getBaseUrl() . '/admin/custommenu/menus/index',
                    'messages'       => $this->view->translate('Menu successfully deleted'),
                    'name'           => 'core-main'
                ));

            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }
        }

    }
    
    /**
     * Add special menu items like avatar, profile, updates etc...
     */
    public function addSpecialItemAction() {
        $this->view->name = $name = $this->_getParam('name', 'core_main');

        // Get list of menus
        $menus = $this->_menus;

        // Check if selected menu is in list
        $selectedMenu = $menus->getRowMatching('name', $name);
                        
        if (null === $selectedMenu) {
            throw new Core_Model_Exception('Invalid menu name');
        }
        $this->view->selectedMenu = $selectedMenu;

        // Get form
        $this->view->form = $form = new Custommenu_Form_Admin_Item(); 
        
        // Changing default form params
        $form->setSpecials();
        $form->submit->setLabel('Create Menu Item');
        
        // Check stuff
        if (!$this->getRequest()->isPost()) {
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        // Save
        $values = $form->getValues();
        //$label = $values['label'];
        //unset($values['label']);

        $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');

        // Check if Search || Updates items are present in current menu
        $menuItems = $menuItemsTable->fetchAll(
            $menuItemsTable->select()
                ->where('menu = ?', $name)
                ->where('custom = ?', 1)
                ->order('order')
        );

        foreach ($menuItems as $item) {
            if (is_null($item->params['itemtype'])) {
                continue;
            }

            if (($values['itemtype'] == 4 && $item->params['itemtype'] == $values['itemtype']) || ($values['itemtype'] == 5 && $item->params['itemtype'] == $values['itemtype'])) {
                return $this->_forward('success', 'utility', 'core', array(
                    'parentRefresh' => true,
                    'messages'      => $this->view->translate('There is already such item in menu')
                ));
            }
        }
        // End of check

        $menuItem = $menuItemsTable->createRow();
        $menuItem->label = '';
        $menuItem->params = $values;
        $menuItem->menu = $name;
        $menuItem->module = 'core'; // Need to do this to prevent it from being hidden
        $menuItem->plugin = self::CHECK_ACCESS_METHOD;
        $menuItem->submenu = '';
        $menuItem->custom = 1;
        $menuItem->save();

        $menuItem->name = 'custommenu_' . sprintf('%s', uniqid());
        $menuItem->save();

        $this->view->status = true;
        $this->view->form = null;
    }
    
}