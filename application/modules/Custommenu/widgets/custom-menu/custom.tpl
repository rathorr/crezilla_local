<div class="layout_user_home_links">
    <div class="quicklinks">
        <?php
        $menuClass = '';
        if ($this->ulClass) {
            $menuClass = "class=\"{$this->ulClass}\"";
        }
        ?>    
        <ul <?php echo $menuClass; ?>>
            <?php foreach ($this->navigation as $link): ?>
                <li class="<?php echo $link->getClass(); ?>">
                    <?php if ($link->get('itemtype') == 'search'): ?>                    
                        <form id="global_search_form" action="<?php echo $this->url(array('controller' => 'search'), 'default', true) ?>" method="get">
                            <input type='text' class='text suggested' name='query' id='global_search_field' size='20' maxlength='100' alt='<?php echo $this->translate('Search') ?>' />
                        </form>

                    <?php endif; ?>

                    <?php if ($this->viewer->getIdentity() && $link->get('itemtype') == 'updates') : ?>                    
                        <span onclick="toggleUpdatesPulldown(event, this, '4');" style="display: inline-block;" class="updates_pulldown">
                            <div class="pulldown_contents_wrapper">
                                <div class="pulldown_contents">
                                    <ul class="notifications_menu" id="notifications_menu">
                                        <div class="notifications_loading" id="notifications_loading">
                                            <img src='<?php echo $this->layout()->staticBaseUrl ?>application/modules/Core/externals/images/loading.gif' style='float:left; margin-right: 5px;' />
                                            <?php echo $this->translate("Loading ...") ?>
                                        </div>
                                    </ul>
                                </div>
                                <div class="pulldown_options">
                                    <?php
                                    echo $this->htmlLink(array('route' => 'default', 'module' => 'activity', 'controller' => 'notifications'), $this->translate('View All Updates'), array('id' => 'notifications_viewall_link'))
                                    ?>
                                    <?php
                                    echo $this->htmlLink('javascript:void(0);', $this->translate('Mark All Read'), array(
                                        'id' => 'notifications_markread_link',
                                    ))
                                    ?>
                                </div>
                            </div>
                            <a href="javascript:void(0);" id="updates_toggle" <?php if ($this->notificationCount): ?> class="new_updates"<?php endif; ?>><?php echo $this->translate(array('%s Update', '%s Updates', $this->notificationCount), $this->locale()->toNumber($this->notificationCount)) ?></a>
                        </span>

                        <script type='text/javascript'>
                            var notificationUpdater;

                            en4.core.runonce.add(function(){
                                if($('global_search_field')){
                                    new OverText($('global_search_field'), {
                                        poll: true,
                                        pollInterval: 500,
                                        positionOptions: {
                                            position: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
                                            edge: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
                                            offset: {
                                                x: ( en4.orientation == 'rtl' ? -4 : 4 ),
                                                y: 2
                                            }
                                        }
                                    });
                                }

                                if($('notifications_markread_link')){
                                    $('notifications_markread_link').addEvent('click', function() {
                                        en4.activity.hideNotifications('<?php echo $this->string()->escapeJavascript($this->translate("0 Updates")); ?>');
                                    });
                                }

        <?php if ($this->updateSettings && $this->viewer->getIdentity()): ?>
                    notificationUpdater = new NotificationUpdateHandler({
                        'delay' : <?php echo $this->updateSettings; ?>
                    });
                    notificationUpdater.start();
                    window._notificationUpdater = notificationUpdater;
        <?php endif; ?>
            });

            var toggleUpdatesPulldown = function(event, element, user_id) {
                if( element.className=='updates_pulldown' ) {
                    element.className= 'updates_pulldown_active';
                    showNotifications();
                } else {
                    element.className='updates_pulldown';
                }
            }

            var showNotifications = function() {
                en4.activity.updateNotifications();
                new Request.HTML({
                    'url' : en4.core.baseUrl + 'activity/notifications/pulldown',
                    'data' : {
                        'format' : 'html',
                        'page' : 1
                    },
                    'onComplete' : function(responseTree, responseElements, responseHTML, responseJavaScript) {
                        if( responseHTML ) {
                            // hide loading icon
                            if($('notifications_loading')) $('notifications_loading').setStyle('display', 'none');

                            $('notifications_menu').innerHTML = responseHTML;
                            $('notifications_menu').addEvent('click', function(event){
                                event.stop(); //Prevents the browser from following the link.

                                var current_link = event.target;
                                var notification_li = $(current_link).getParent('li');

                                // if this is true, then the user clicked on the li element itself
                                if( notification_li.id == 'core_menu_mini_menu_update' ) {
                                    notification_li = current_link;
                                }

                                var forward_link;
                                if( current_link.get('href') ) {
                                    forward_link = current_link.get('href');
                                } else{
                                    forward_link = $(current_link).getElements('a:last-child').get('href');
                                }

                                if( notification_li.get('class') == 'notifications_unread' ){
                                    notification_li.removeClass('notifications_unread');
                                    en4.core.request.send(new Request.JSON({
                                        url : en4.core.baseUrl + 'activity/notifications/markread',
                                        data : {
                                            format     : 'json',
                                            'actionid' : notification_li.get('value')
                                        },
                                        onSuccess : function() {
                                            window.location = forward_link;
                                        }
                                    }));
                                } else {
                                    window.location = forward_link;
                                }
                            });
                        } else {
                            $('notifications_loading').innerHTML = '<?php echo $this->string()->escapeJavascript($this->translate("You have no new updates.")); ?>';
                        }
                    }
                }).send();
            };
                        </script>

                    <?php endif; ?>

                    <?php
                    if ($this->viewer->getIdentity() && ($link->get('itemtype') == 'avatar' || $link->get('itemtype') == 'avatar-profile' || $link->get('itemtype') == 'profile')) {
                        echo $this->htmlLink($link->getHref(), $this->translate($link->getLabel()), array(
                            'class' => 'buttonlink' . ( $link->getClass() ? ' ' . $link->getClass() : '' ),
                            'style' => 'background-image: url(' . $link->get('icon') . ');',
                            'target' => $link->get('target'),
                        ));
                    }
                    ?>

                    <?php
                    if (!$link->get('itemtype')) {
                        $icon = $link->get('icon');
                        if (!empty($icon)) {
                            $icon = "<img src=\"{$link->get('icon')}\" alt=\"\" />";
                        }
                        echo $this->htmlLink($link->getHref(), $icon . $this->translate($link->getLabel()), array(
                            'class' => 'buttonlink' . ( $link->getClass() ? ' ' . $link->getClass() : '' ),
                            'target' => $link->get('target'),
                        ));
                    }
                    ?>

                    <?php if ($link->getPages()) : ?>
                        <ul class="custommenu-nav-subitem">
                            <?php foreach ($link->getPages() as $subLink) : ?>
                                <li class="<?php echo $subLink->getClass(); ?>">
                                    <?php if ($subLink->get('itemtype') == 'search'): ?>                                    
                                        <form id="global_search_form" action="<?php echo $this->url(array('controller' => 'search'), 'default', true) ?>" method="get">
                                            <input type='text' class='text suggested' name='query' id='global_search_field' size='20' maxlength='100' alt='<?php echo $this->translate('Search') ?>' />
                                        </form>
                                    <?php endif; ?>

                                    <?php if ($this->viewer->getIdentity() && $subLink->get('itemtype') == 'updates') : ?>                                    
                                        <span onclick="toggleUpdatesPulldown(event, this, '4');" style="display: inline-block;" class="updates_pulldown">
                                            <div class="pulldown_contents_wrapper">
                                                <div class="pulldown_contents">
                                                    <ul class="notifications_menu" id="notifications_menu">
                                                        <div class="notifications_loading" id="notifications_loading">
                                                            <img src='<?php echo $this->layout()->staticBaseUrl ?>application/modules/Core/externals/images/loading.gif' style='float:left; margin-right: 5px;' />
                                                            <?php echo $this->translate("Loading ...") ?>
                                                        </div>
                                                    </ul>
                                                </div>
                                                <div class="pulldown_options">
                                                    <?php
                                                    echo $this->htmlLink(array('route' => 'default', 'module' => 'activity', 'controller' => 'notifications'), $this->translate('View All Updates'), array('id' => 'notifications_viewall_link'))
                                                    ?>
                                                    <?php
                                                    echo $this->htmlLink('javascript:void(0);', $this->translate('Mark All Read'), array(
                                                        'id' => 'notifications_markread_link',
                                                    ))
                                                    ?>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0);" id="updates_toggle" <?php if ($this->notificationCount): ?> class="new_updates"<?php endif; ?>><?php echo $this->translate(array('%s Update', '%s Updates', $this->notificationCount), $this->locale()->toNumber($this->notificationCount)) ?></a>
                                        </span>

                                        <script type='text/javascript'>
                                            var notificationUpdater;

                                            en4.core.runonce.add(function(){
                                                if($('global_search_field')){
                                                    new OverText($('global_search_field'), {
                                                        poll: true,
                                                        pollInterval: 500,
                                                        positionOptions: {
                                                            position: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
                                                            edge: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
                                                            offset: {
                                                                x: ( en4.orientation == 'rtl' ? -4 : 4 ),
                                                                y: 2
                                                            }
                                                        }
                                                    });
                                                }

                                                if($('notifications_markread_link')){
                                                    $('notifications_markread_link').addEvent('click', function() {
                                                        en4.activity.hideNotifications('<?php echo $this->string()->escapeJavascript($this->translate("0 Updates")); ?>');
                                                    });
                                                }

                <?php if ($this->updateSettings && $this->viewer->getIdentity()): ?>
                            notificationUpdater = new NotificationUpdateHandler({
                                'delay' : <?php echo $this->updateSettings; ?>
                            });
                            notificationUpdater.start();
                            window._notificationUpdater = notificationUpdater;
                <?php endif; ?>
                    });

                    var toggleUpdatesPulldown = function(event, element, user_id) {
                        if( element.className=='updates_pulldown' ) {
                            element.className= 'updates_pulldown_active';
                            showNotifications();
                        } else {
                            element.className='updates_pulldown';
                        }
                    }

                    var showNotifications = function() {
                        en4.activity.updateNotifications();
                        new Request.HTML({
                            'url' : en4.core.baseUrl + 'activity/notifications/pulldown',
                            'data' : {
                                'format' : 'html',
                                'page' : 1
                            },
                            'onComplete' : function(responseTree, responseElements, responseHTML, responseJavaScript) {
                                if( responseHTML ) {
                                    // hide loading icon
                                    if($('notifications_loading')) $('notifications_loading').setStyle('display', 'none');

                                    $('notifications_menu').innerHTML = responseHTML;
                                    $('notifications_menu').addEvent('click', function(event){
                                        event.stop(); //Prevents the browser from following the link.

                                        var current_link = event.target;
                                        var notification_li = $(current_link).getParent('li');

                                        // if this is true, then the user clicked on the li element itself
                                        if( notification_li.id == 'core_menu_mini_menu_update' ) {
                                            notification_li = current_link;
                                        }

                                        var forward_link;
                                        if( current_link.get('href') ) {
                                            forward_link = current_link.get('href');
                                        } else{
                                            forward_link = $(current_link).getElements('a:last-child').get('href');
                                        }

                                        if( notification_li.get('class') == 'notifications_unread' ){
                                            notification_li.removeClass('notifications_unread');
                                            en4.core.request.send(new Request.JSON({
                                                url : en4.core.baseUrl + 'activity/notifications/markread',
                                                data : {
                                                    format     : 'json',
                                                    'actionid' : notification_li.get('value')
                                                },
                                                onSuccess : function() {
                                                    window.location = forward_link;
                                                }
                                            }));
                                        } else {
                                            window.location = forward_link;
                                        }
                                    });
                                } else {
                                    $('notifications_loading').innerHTML = '<?php echo $this->string()->escapeJavascript($this->translate("You have no new updates.")); ?>';
                                }
                            }
                        }).send();
                    };
                                        </script>
                                    <?php endif; ?>

                                    <?php
                                    if ($this->viewer->getIdentity() && ($subLink->get('itemtype') == 'avatar' || $subLink->get('itemtype') == 'avatar-profile' || $subLink->get('itemtype') == 'profile')) {
                                        echo $this->htmlLink($subLink->getHref(), $this->translate($subLink->getLabel()), array(
                                            'class' => 'buttonlink' . ( $subLink->getClass() ? ' ' . $subLink->getClass() : '' ),
                                            'style' => 'background-image: url(' . $subLink->get('icon') . ');',
                                            'target' => $subLink->get('target'),
                                        ));
                                    }
                                    ?>

                                    <?php
                                    if (!$subLink->get('itemtype')) {
                                        $icon = $subLink->get('icon');
                                        if (!empty($icon)) {
                                            $icon = "<img src=\"{$link->get('icon')}\" alt=\"\" />";
                                        }
                                        echo $this->htmlLink($subLink->getHref(), $icon . $this->translate($subLink->getLabel()), array(
                                            'class' => 'buttonlink' . ( $subLink->getClass() ? ' ' . $subLink->getClass() : '' ),
                                            'target' => $subLink->get('target'),
                                        ));
                                    }
                                    ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                    <?php endif; ?>
                </li>

            <?php endforeach; ?>

        </ul>

    </div>
</div>
