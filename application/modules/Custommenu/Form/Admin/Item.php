<?php

class Custommenu_Form_Admin_Item extends Core_Form_Admin_Menu_ItemEdit {

    protected $_showRoles;

    public function __construct($options = true) {
        $this->_showRoles = $options;
        parent::__construct();        
    }
    
    public function init() {
        parent::init();

        if ($this->_showRoles) {

            $this->addElement('Cancel', 'check', array(// Select/Unselect roles trigger
                'label'   => 'Unselect All',
                'link'    => true,
                'href'    => '',
                'onclick' => "var text = $(this).get('text');
                var inputs = $('roletypes-element').getElements('input');
                var do_check;

                if (text == 'Unselect All') {
                    for (i = 1; i < inputs.length; i++) {
                        if (inputs[i].checked) {
                            do_check = false;
                        }
                    }
                    $(this).set('text', 'Select All');
                } else if (text == 'Select All') {
                    for (i = 1; i < inputs.length; i++) {
                        if (inputs[i].checked == false) {
                            do_check = true;
                        }
                    }
                    $(this).set('text', 'Unselect All');
                }
                $$('.user-role-type').set('checked', do_check);
                ",
            ));
            $this->check->removeDecorator('label');           
            
            if (count($this->getUserRoles())) {
                                   
                $roles = $this->createElement('multiCheckbox', 'roletypes');
                $roles->addMultiOptions($this->getUserRoles())
                      ->setValue(array_keys($this->getUserRoles()))
                      ->setAttrib('class', 'user-role-type')
                      ->setAttrib('checked', 'checked');

                
                $this->addElement($roles);
                $this->roletypes->removeDecorator('label');

            }
            
            $this->addDisplayGroup(array('check', 'roletypes'), 'roles', array('legend' => 'Show For:'));            
        }

        $this->getDisplayGroup('buttons')->setOrder(8);
    }

    /**
     * Get user roles
     * @return array
     */
    private function getUserRoles() {
        $roleTable = Engine_Api::_()->getDbtable('levels', 'authorization');

        return $roleTable->getLevelsAssoc();
    }
    
    /**
     * Set Types Of Special Menu Items     
     */
    public function setSpecials() {
        $this->setTitle('Create Special Menu Item');
        $this->addElement('select', 'itemtype', array(
            'multiOptions' => array(
                '1' => 'Avatar', 
                '2' => 'Avatar & Profile Link', 
                '3' => 'Profile Link', 
                '4' => 'Updates',
                '5' => 'Search',
            )
        ));
        $this->itemtype->setLabel('Type')->setOrder(-1);
        $this->removeElement('uri');
        $this->removeElement('icon');
        $this->removeElement('label');
    }
    

}