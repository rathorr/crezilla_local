<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'custommenu',
    'version' => '4.3.0',
    'path' => 'application/modules/Custommenu',
    'title' => 'Custom Menu',
    'description' => 'Displays custom navigation menu with drop-down features.',
    'author' => 'WebHive Team',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Custommenu',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/custommenu.csv',
    ),
  ),
); ?>