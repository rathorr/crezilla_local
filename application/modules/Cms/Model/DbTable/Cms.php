<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Classifieds.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Cms_Model_DbTable_Cms extends Engine_Db_Table
{
  protected $_rowClass = "Cms_Model_Cms";

  /**
   * Gets a paginator for classifieds
   *
   * @param Core_Model_Item_Abstract $user The user to get the messages for
   * @return Zend_Paginator
   */
  public function getCmsPaginator($params = array(),
      $customParams = null)
  {
    $paginator = Zend_Paginator::factory($this->getCmsSelect($params, $customParams));
    if( !empty($params['page']) ) {
      $paginator->setCurrentPageNumber($params['page']);
    }
    if( !empty($params['limit']) ) {
      $paginator->setItemCountPerPage($params['limit']);
    }
    return $paginator;
  }

  /**
   * Gets a select object for the user's cms entries
   *
   * @param Core_Model_Item_Abstract $user The user to get the messages for
   * @return Zend_Db_Table_Select
   */
  public function getCmsSelect($params = array(), $customParams = null)
  {
    $tableName = $this->info('name');
    

    $select = $this->select()
        ->from($this)
        ->order(!empty($params['orderby']) ? $tableName . '.' . $params['orderby'] . ' DESC'
                  : $tableName . '.creation_date DESC' );

    return $select;
  }
  
  public function getCmsByUrl($url){
	$select	=	$this->select()
			->from ( $this) 
			->where('url = ?', $url)
			;
	$result = $select->query()->fetchAll();
		if($result){
			return $result[0];	
		}else{
			return FALSE;	
		}
		
	
  }
 
}
