<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: AdminManageController.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Cms_AdminManageController extends Core_Controller_Action_Admin
{
  public function indexAction()
  {
    $page=$this->_getParam('page',1);
    $this->view->paginator = Engine_Api::_()->getItemTable('cms')->getCmsPaginator(array(
      'orderby' => 'cms_id',
    ));
    $this->view->paginator->setItemCountPerPage(25);
    $this->view->paginator->setCurrentPageNumber($page);
  }
  
  public function editAction(){
	  $cms = Engine_Api::_()->getItem('cms', (int) $this->_getParam('cms_id'));
	 
	  // Make form
      $this->view->form = $form = new Cms_Form_Edit();
	  
	  if( !$this->getRequest()->isPost() )
		{
		  $formData	=	$cms->toArray();
		  
		  $form->populate($formData);
		  
		  $this->view->status = false;
		  $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
		  return;
		} 

    if( !$form->isValid($this->getRequest()->getPost()) )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid data');
      return;
    }

    // Process
    $db = $cms->getTable()->getAdapter();
    $db->beginTransaction();

    try
    {
      $values = $form->getValues();
      $cms->setFromArray($values);
      $cms->save();

      $db->commit();
    }
    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }

        return $this->_helper->redirector->gotoRoute(array('action' => 'index'), 'cms_general', true);
  
  }


}