<?php

class Cms_IndexController extends Core_Controller_Action_Standard
{
  public function indexAction()
  {
    $this->view->cms = Engine_Api::_()->getDbtable('cms', 'cms')->getCmsByUrl($this->_getParam('url'));
	$this->view->url = $this->_getParam('url');
  }
}
