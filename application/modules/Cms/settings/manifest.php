<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'cms',
    'version' => '4.0.0',
    'path' => 'application/modules/Cms',
    'title' => 'Content Management System',
    'description' => '',
    'author' => '',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Cms',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/cms.csv',
    ),
  ),
  // Items ---------------------------------------------------------------------
  'items' => array(
    'cms'
  ),
  'routes' => array(
    'cms_specific' => array(
      'route' => 'admin/cms/:action/:cms_id/*',
      'defaults' => array(
        'module' => 'cms',
        'controller' => 'admin-manage',
        'action' => 'edit'
      ),
      'reqs' => array(
        'action' => '(edit)',
      ),
    ),
	'cms_general' => array(
      'route' => 'admin/cms/:action',
      'defaults' => array(
        'module' => 'cms',
        'controller' => 'admin-manage',
        'action' => 'index'
      ),
      'reqs' => array(
        'action' => '(index)',
      ),
    ),
	'cms_desc' => array(
      'route' => 'cms/:url',
      'defaults' => array(
        'module' => 'cms',
        'controller' => 'index',
        'action' => 'index'
      )
    ),
  )
); ?>