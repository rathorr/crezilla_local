<?php
/**
 * Luxervice
 *
 * @category   Application_Extensions
 * @package    Home
 * @copyright  Copyright Luxervice LLC
 * @license    http://www.luxervice.com/license/
 * @version    $Id: index.tpl 01.07.13 09:31 michael $
 * @author     Michael <michael@luxervice.com>
 */
?>

<style>
.step {
	width: 100%;
	position: relative;
}
.step:not(.active) {
	opacity: 1;
	filter: alpha(opacity=99);
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(opacity=99)";
}
.step:not(.active) a.jms-link{
	opacity: 1;
	margin-top: 40px;
}
#global_content{width:100% !important;}
.jms-slideshow{ margin:0 !important;}
#global_header > div.layout_page_header > div > div.generic_layout_container.layout_core_menu_mini > div.device_on
{
	display: none;
}
.logout .layout_core_mobi_menu_mini #core_menu_mini_menu_mobile {
    display: none;
}

</style>
<?php echo $this->headLink()->appendStylesheet($this->baseUrl().'/public/custom/custom.css') ?>
<?php echo $this->headLink()->appendStylesheet($this->baseUrl().'/public/custom/media.css') ?>
<!-- lower section start -->

<div class="lower_container">
	<section id="connect">
	
	<div class="conct_cont">
		<div class="col-md-12 text-center" style="margin-bottom:20px;">
			<h1 class="heading"><?php echo $this->cms['title'];?></h1>
			<span class="title_footer"></span>
			
		</div>
		
		<div class="col-md-12 text-center" style="clear:both; margin-bottom:20px;">
		<?php echo $this->cms['body'];?>
		</div>
	</div><!-- connect container end -->
	<div  <?php if($this->cms['url'] =='privacy'){
		echo 'style="width:100%; display:block"';
		echo $this->content()->renderWidget('core.contact') ;}
		else if($this->cms['url'] =='faq'){
		echo 'style="width:100%; display:block"';
		echo $this->content()->renderWidget('core.faqs');
		}
		else{echo 'style="width:100%; display:none"';}
		?>>
		<?php ?>
	 </div>
	</section>
	

</div>

<!-- lower section end -->

