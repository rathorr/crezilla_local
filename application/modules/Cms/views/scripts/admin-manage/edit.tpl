<h2><?php echo $this->translate("Edit CMS") ?></h2>
<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: edit.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>
<?php 
$this->headScript()
    ->appendFile($this->layout()->staticBaseUrl . 'externals/jquery/jquery-1.10.2.js');
?>


<script type="application/javascript">


$(document).on('submit', '#add_edu_form', function(){
		if($.trim($('#institute_name').val()) == ''){
			jQuery('#institute_name').focus();
			jQuery('#institute_name').addClass("com_form_error_validatin");
			//alert('Please enter institute name.');
			return false;	
		}
		if($.trim($('#start_date').val()) == '0'){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Please select start date.');
			return false;	
		}
		if($.trim($('#end_date').val()) == '0'){
			jQuery('#end_date').focus();
			jQuery('#end_date').addClass("com_form_error_validatin");
			//alert('Please select end date.');
			return false;	
		}
		if($('#start_date').val() > $('#end_date').val()){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Start date can not be greater than end date.');
			$('#start_date').val('0');
			$('#end_date').val('0');
			return false;	
		}
		if($.trim($('#category_id').val()) == 0){
			jQuery('#category_id').focus();
			jQuery('#category_id').addClass("com_form_error_validatin");
			//alert('Please select category.');
			return false;
		}
		if($.trim($('#category_id').val()) == 'other' && $.trim($('#other_category').val()) == ''){
			jQuery('#other_category').focus();
			jQuery('#other_category').addClass("com_form_error_validatin");
			//alert('Please enter category.');
			return false;
		}
		if($.trim($('#field_of_study').val()) == 0){
			jQuery('#field_of_study').focus();
			jQuery('#field_of_study').addClass("com_form_error_validatin");
			//alert('Please enter field of study.');
			return false;
		}
});

</script>

<?php
  echo $this->form->render();
?>
