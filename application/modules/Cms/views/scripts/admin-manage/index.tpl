<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */
?>

<script type="text/javascript">

function multiDelete()
{
  return confirm("<?php echo $this->translate('Are you sure you want to delete the selected classified listings?');?>");
}

function selectAll()
{
  var i;
  var multidelete_form = $('multidelete_form');
  var inputs = multidelete_form.elements;
  for (i = 1; i < inputs.length; i++) {
    if (!inputs[i].disabled) {
      inputs[i].checked = inputs[0].checked;
    }
  }
}
</script>

<h2><?php echo $this->translate("CMS") ?></h2>

	
<?php if( count($this->paginator) ): ?>
<form id='multidelete_form' method="post" action="<?php echo $this->url();?>" onSubmit="return multiDelete()">
<table class='admin_table'>
  <thead>
    <tr>
      <th class='admin_table_short'>Sr No.</th>
      <th><?php echo $this->translate("Title") ?></th>
      <th><?php echo $this->translate("Last Modified") ?></th>
      <th><?php echo $this->translate("Options") ?></th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1;foreach ($this->paginator as $item): ?>
      <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $item->getTitle() ?></td>
        <td><?php echo date('M d, Y', strtotime($item->modified_date)); ?></td>
        <td>
          <a href="<?php echo $this->baseUrl().'/admin/cms/edit/'.$item->cms_id;?>">
            <?php echo $this->translate("edit") ?>
          </a>
          |
          <a href="<?php echo $this->baseUrl().'/cms/'.$item->url;?>" target="_blank">
            <?php echo $this->translate("view") ?>
          </a>
        </td>
      </tr>
    <?php $i++; endforeach; ?>
  </tbody>
</table>

<br />

</form>

<br/>
<div>
  <?php echo $this->paginationControl($this->paginator); ?>
</div>

<?php else:?>
  <div class="tip">
    <span>
      <?php echo $this->translate("There are no cms pagess listed yet.") ?>
    </span>
  </div>
<?php endif; ?>
