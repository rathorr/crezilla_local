<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Edit.php 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Cms_Form_Edit extends Engine_Form
{
  
  public function init()
  {
    $user = Engine_Api::_()->user()->getViewer();

    $user_level = Engine_Api::_()->user()->getViewer()->level_id;
    $this->setAttrib('id', 'edit_cms_form')
      ->setAttrib('name', 'cms_edit');
    
	
    $this->addElement('Text', 'title', array(
      'label' => 'Title',
	  'placeholder'	=>	'Enter Title',
      'required' => true,
      'notEmpty' => true,
      'validators' => array(
        'NotEmpty',
      ),
      'filters' => array(
        new Engine_Filter_Censor(),
        //new Engine_Filter_HtmlSpecialChars(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '255'))
      )
    ));
    $this->title->getValidator('NotEmpty')->setMessage("Please enter title.");
	
	
    // Init descriptions
	$allowed_html = Engine_Api::_()->authorization()->getPermission($user_level, 'blog', 'auth_html');
    $this->addElement('TinyMce', 'body', array(
      'label' => 'Description',
      /*'disableLoadDefaultDecorators' => true,*/
	  'placeholder'	=>	"Enter description",
      'allowEmpty' => false,
      /*'decorators' => array(
        'ViewHelper'
      ),*/
      'editorOptions' => array(
        'upload_url' => $upload_url,
        'html' => (bool) $allowed_html,
      ),
      'filters' => array(
        new Engine_Filter_Censor(),
        new Engine_Filter_Html(array('AllowedTags'=>$allowed_html))),
    )); 
    

    // Submit or succumb!
    $this->addElement('Button', 'submit', array(
      'label' => 'Save',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    
    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'decorators' => array(
        'ViewHelper'
      )
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
  
}
