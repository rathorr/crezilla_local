<?php

class Pdf_IndexController extends Core_Controller_Action_Standard
{
  public function indexAction()
  {
  	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit');
    if( count($this->view->navigation) == 1 ) {
      $this->view->navigation = null;
    }
    $viewer = Engine_Api::_()->user()->getViewer();
    if( !$this->_helper->requireUser()->isValid() ) return;    
    $this->view->page = $page = $this->_getParam('page');
    $this->view->paginator = Engine_Api::_()->getItemTable('pdf')->getPdfPaginator(array(
      'orderby' => 'modified_date',
	  'owner'=>$viewer
    ));
    $this->view->paginator->setItemCountPerPage(10);
    $this->view->paginator->setCurrentPageNumber($page);
	
	/*$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit', array('params' => $params)); */
  }
  
   public function deletepdfAction()
	{
	
	if($this->getRequest()->isPost()){
		$this->_helper->layout->disableLayout();
		$currnet_user 			= Engine_Api::_()->user()->getViewer()->getIdentity();
	   	$pdf_id  = $_POST['pdf_id'];
 	   	$deleteFile=Engine_Api::_()->getDbTable('pdfs', 'pdf')->deletePdfFile($pdf_id,$currnet_user);
		
		return $this->_forward('success', 'utility', 'core', array(
				'smoothboxClose' => true,
				'parentRefresh'  => true,
				'messages' => array('Your pdf file is deleted.')
			  ));
	}else{
		$this->view->pdf_id  = $pdf_id  = (int) $this->_getParam('pdf_id', null);
		$page = $this->view->page;
	}
 	}
	
	public function deletepdffileAction()
	{
		
 	   $currnet_user 			= Engine_Api::_()->user()->getViewer()->getIdentity();
	   $pdf_id  = $_POST['pdf_id'];
 	   $deleteFile=Engine_Api::_()->getDbTable('pdfs', 'pdf')->deletePdfFile($pdf_id,$currnet_user);
 	   echo "success::deleted";
	   die();	
	 
	}
  
  public function createpdfAction()
	{
 	//$this->view->pdf_id  = $pdf_id  = (int) $this->_getParam('pdf_id', null); 	
	
		if($_FILES["pdf_file"]["name"]){
			$this->_helper->layout->disableLayout();
			$uploads_dir = 'public/pdf';
			$tmp_name = $_FILES["pdf_file"]["tmp_name"];
			$name = $_FILES["pdf_file"]["name"];
			$file_title=$_POST['title'];
			$ext=end(explode(".",$name));
			$currnet_user 			= Engine_Api::_()->user()->getViewer()->getIdentity();
			$newname=$currnet_user."_".time().".".$ext;
			
			move_uploaded_file($tmp_name, "$uploads_dir/$newname");
			$saveFile=Engine_Api::_()->getDbTable('pdfs', 'pdf')->savePdfFile($currnet_user,$file_title,$newname);
			
			return $this->_forward('success', 'utility', 'core', array(
				'smoothboxClose' => true,
				'parentRefresh'  => true,
				'messages' => array('Your pdf is uploaded successfully.')
			  ));
		}else{
			$page = $this->view->page;
		}
	}
	
	public function uploadpdffileAction()
	{
	//$this->_helper->layout->disableLayout();	
	
	 
	}
  
  
  
}
