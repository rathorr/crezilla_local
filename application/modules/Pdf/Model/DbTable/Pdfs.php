<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Classifieds.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Pdf_Model_DbTable_Pdfs extends Engine_Db_Table
{
  protected $_rowClass = "Pdf_Model_Pdf";

  /**
   * Gets a paginator for classifieds
   *
   * @param Core_Model_Item_Abstract $user The user to get the messages for
   * @return Zend_Paginator
   */
 public function getPdfPaginator($options = array())
  {
    return Zend_Paginator::factory($this->getPdfSelect($options));
  }

  /**
   * Gets a select object for the user's Pdf entries
   *
   * @param Core_Model_Item_Abstract $user The user to get the messages for
   * @return Zend_Db_Table_Select
   */
  public function getPdfSelect($options = array())
  {
    $select = $this->select();
    
	if(!empty($options['owner'])){
      $select->where('owner_id = ?', $options['owner']->getIdentity());
	}
        $select->order('modified_date DESC')
        ;

    if( !empty($options['search']) && is_numeric($options['search']) ) {
      $select->where('search = ?', $options['search']);
    }

    return $select;
  }
  
  public function getPdfByUrl($url){
	$select	=	$this->select()
			->from ( $this) 
			->where('url = ?', $url)
			;
	$result = $select->query()->fetchAll();
		if($result){
			return $result[0];	
		}else{
			return FALSE;	
		}
		
	
  }
  
  
  public function getPageFile($page_id  )
	{
		 
				 
						
	   $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		  $query = "select  pdf_file,modified_date from engine4_pdf_pdfs
 		         where  pdf_id='".$page_id."'  ";
				  
		$stmt = $db->query($query);
        $result = $stmt->fetchAll();
		 
 		 $filename= $result;
 		 return $filename;
		 
 	}
	

	public function deletePdfFile($pdf_id,$user_id)
	{
	   $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		$query = "delete from engine4_pdf_pdfs
 		            where  pdf_id='".$pdf_id."'
					and owner_id='".$user_id."'  ";
				  
		$stmt = $db->query($query);
        $result = $stmt->fetchAll();
		 
 		 $filename= $result;
 		 return $filename;
		 
 	}
	
	public function savePdfFile($currnet_user,$file_title,$newname)
	{
						
	   $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		$query = "insert into  engine4_pdf_pdfs
 		          set  title='".addslashes($file_title)."',
				  pdf_file	='".addslashes($newname)."',
				  owner_id='".$currnet_user."',
				  creation_date='now()'";
			  
		$stmt = $db->query($query);
  		 return  true;
		 
 	}	
 
}
