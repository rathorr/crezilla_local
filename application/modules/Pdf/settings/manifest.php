<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'pdf',
    'version' => '4.0.0',
    'path' => 'application/modules/Pdf',
    'title' => 'pdf',
    'description' => 'pdf',
    'author' => 'Vivek',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Pdf',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/pdf.csv',
    ),
  ),
  // Items ---------------------------------------------------------------------
  'items' => array(
    'pdf'
  ),
  
  
  'routes' => array(
      
    'page_autosubmit' => array(
      'route' => 'delete-pdf/:action/:pdf_id/*',
      'defaults' => array(
        'module' => 'pdf',
        'controller' => 'index',
    		'action' => 'deletepdf',
    		'pdf_id' => 0
      )
    ),
	
	
	'page_autosubmit2' => array(
      'route' => 'delete-pdf-file/:action/:pdf_id/*',
      'defaults' => array(
        'module' => 'pdf',
        'controller' => 'index',
    		'action' => 'deletepdffile',
    		'pdf_id' => 0
      )
    ),
	
	'page_autosubmit3' => array(
      'route' => 'create-pdf/:action/*',
      'defaults' => array(
        'module' => 'pdf',
        'controller' => 'index',
    		'action' => 'createpdf'
       )
    ),
	
	'page_autosubmit4' => array(
      'route' => 'upload-pdf-file/:action/*',
      'defaults' => array(
        'module' => 'pdf',
        'controller' => 'index',
    		'action' => 'uploadpdffile'

      )
    ),
     

     
  ),
); ?>