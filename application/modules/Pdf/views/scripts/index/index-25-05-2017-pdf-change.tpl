<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: profile.tpl 9984 2013-03-20 00:00:04Z john $
 * @author     John
 */
?>

<style>


.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:auto;
  width:100%;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}


ul.form-errors > li, ul.form-notices > li {
 float:none;
}

.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}

</style>

<div class="headline">
  <h2>
     
      <?php echo $this->translate('Manage Pdf');?>
    
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
        echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
		
    ?>
  </div>
</div>

<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle" style="width:100%; background-color: #fff;
    padding: 1% 2%;">
<div class="generic_layout_container layout_core_content">

<script type="text/javascript">
//&lt;![CDATA[
  //window.addEvent('domready', function() {
//    $('sort').addEvent('change', function(){
//      $(this).getParent('form').submit();
//    });
//
//    var category_id = $('category_id');
//    if( category_id != null ){
//      category_id.addEvent('change', function(){
//        $(this).getParent('form').submit();
//      });
//    }
//  })
//]]&gt;
</script>

   <h2 style="border-bottom:none; background-color:#fff;font-size:14px;" class="heading"><a href="<?php echo $this->baseUrl().'/albums/manage';?>" class="wp_init">Album</a></h2>
   <h2 style="border-bottom: 2px solid #fb933c; font-size:14px;" class="heading"><a href="<?php echo $this->baseUrl().'/pdf/';?>" class="wp_init">Pdf</a></h2>
   
   <h2 class="heading" style="border-bottom:none; background-color:#fff;font-size:14px;"><a href="<?php echo $this->baseUrl().'/videos/manage';?>">Video/Audio</a></h2>
   <!--SKIP Start-->
   
   
   
   
<p class="skip_div abs_d2" style="margin-top:10px;">
<!-- <a href="<?php echo $this->baseUrl().'/videos/manage';?>" class="skip_text skip_bt2 wp_init">Next</a> -->
</p>
<h2 class="create_pdf"  style="text-align:right; float:right;"><a class="buttonlink smoothbox icon_photos_delete wp_init" href="<?php echo $this->baseUrl();?>/create-pdf/createpdf/format/smoothbox">Post PDF</a></h2>


<!--SKIP End-->
    <?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
      <ul class="albums_manage">
              <?php foreach( $this->paginator as $album ): 
              $rec= Engine_Api::_()->getDbTable('pdfs', 'pdf')->getPageFile($album->pdf_id);
              $file=$rec[0]['pdf_file'];
              $modified_date=$rec[0]['modified_date'];
              ?>
              <li>
          <div class="albums_manage_photo">
            <a href="<?php echo $this->baseUrl();?>/public/pdf/<?php echo $file;?>" target="_blank" class="wp_init"><img class="thumb_normal item_photo_album  thumb_normal" style="width:75px; height:75px;" alt="" src="<?php echo $this->baseUrl();?>/public/PDF-icon.png"></a>          </div>
          <div class="albums_manage_options">
                                      <a class="buttonlink smoothbox icon_photos_delete wp_init" href="<?php echo $this->baseUrl();?>/delete-pdf/deletepdf/<?php echo $album->pdf_id;?>/format/smoothbox">Delete PDF</a>          </div>
          <div class="albums_manage_info">
            <h3><a href="<?php echo $this->baseUrl();?>/public/pdf/<?php echo $file;?>" class="wp_init"><?php echo $album->getTitle(); ?></a></h3>
            <div class="albums_manage_info_photos">
              <?php echo date("M d, Y",strtotime($modified_date));?>            </div>
            <div class="albums_manage_info_desc">
                          </div>
          </div>
        </li>
        <?php endforeach; ?>
               
               
               
                </ul>
   <?php else: ?>
   <div class="tip">
      <span id="no-album">
        <?php echo $this->translate('You do not have any pdfs yet.');?>
         
      </span>
    </div>	
  <?php endif; ?>

<script type="text/javascript">

  $$('.custom_951').getParent().addClass('active');
</script>
</div>
</div>
</div>


<style type="text/css">
.pages{ float:right;}
</style>