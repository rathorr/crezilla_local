<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: profile.tpl 9984 2013-03-20 00:00:04Z john $
 * @author     John
 */
?>

<style>
.pages{ float:right;}
.tabs {
  position: absolute;
    padding: 0px !important;
    width: 20%;
    background-color: #fff !important;
    border-right: 1px solid #ccc; 
    float: left;
}
.tab3 {
    position: relative;
    left: 25%;
    background-color: #fff;
    width: 75%;
    border-left: 1px solid #ccc;
    padding-top: 1px;
}
.layout_middle
{
  background-color: #E9EAED !important;
  width: 100% !important;
}
.toptitle
{
  margin: 0 0 10px 0 !important;
  padding: 0px 0px !important;
}
#global_content > div > div > div > div > div.tabs > ul > li
{
  width: 100%;
}
.heading
  {
    background-color:#fff;
    font-size:15px !important; 
    padding: 10px 10px 0px 10px !important;
    border-bottom: none;
  }
   .heading > a:hover
  {
    border-bottom: 1px solid #fb933c;
    text-decoration: none !important;
    color: #fb933c !important;
  }
  .container {
    padding-right: 0px;
    padding-left: 0px;
}
  .chennal_video_list_area{
  float: left;
  width:  100%;
 /* border-left: 1px solid #e5e5e5;*/   
}
.chennal_video_list_area ._chennal_video_list_head {
    margin-bottom: 15px;
}
.chennal_video_list_area ._chennal_video_list_head ._chennal_video_list_head_content {
    float: left;
    padding-left: 10px;
    padding-right: 10px;
}
._add_chennal_video {
    float: right;
    margin-right: 10px;
    color: #000 !important;
    padding: 0px 10px;
    border: 1px solid #000;
    text-transform: uppercase !important;
    margin-top: 8px;
    height: 30px;
    line-height: 30px;
}
._chennal_video_list_head ._chennal_video_list_head_content {
    font-size: 18px;
    padding-top: 10px;
    padding-bottom: 10px;
    color: #333;
}
._chennal_video_list_head {
    float: left;
    width: 100%;
    background: white;
    border-bottom: 1px solid #e5e5e5;
}
.chennal_video_list_item {
    margin-bottom: 20px;
    padding-left: 10px;
    padding-right: 10px;
}
.chennal_video_list_item_area {
    background: #fff;
    float: left;
    width: 100%;
    border: 1px solid #ccc;
}
.chennal_video_img {
    position: relative;
}
._chennal_video_action {
    position: absolute;
    right: 5px;
    top: 5px;
    width: 80%;
    z-index: 10;
}
.edit_chennal_video {
    opacity: 0;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
.edit_chennal_video i {
    background: #000;
    color: #fff;
    width: 20px;
    height: 20px;
    line-height: 20px;
    text-align: center;
    -webkit-border-radius: 2px 2px 0px 0px;
    -moz-border-radius: 2px 2px 0px 0px;
    border-radius: 2px 2px 0px 0px;
    display: inline-block;
    float: right;
    cursor: pointer;
}
._chennal_video_action_btns {
    clear: both;
    background: #000;
    /* padding: 2px 8px; */
    padding: 2px 0px 0px 2px;
    width: 100%;
    float: right;
    opacity: 0;
    pointer-events: none;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
._chennal_video_action a {
    color: #fff;
}
._chennal_video_action_btns a {
    display: block;
    padding: 2px;
}
._chennal_video_list_item_title {
    white-space: nowrap;
    width: 135px;
    overflow: hidden;
    text-overflow: ellipsis;
    padding-top: 5px;
}
._chennal_video_list_item_title, ._chennal_video_list_item_subtile {
    padding-left: 10px;
}
._chennal_video_list_item_title a {
    color: #333;
    font-size: 14px;
}
._chennal_video_list_item_title, ._chennal_video_list_item_subtile {
    padding-left: 10px;
}
.chennal_video_list_item_area img {
    width: 100%;
}
.img-responsive {
    min-height: 160px;
    max-height: 160px;
}
.chennal_video_img:hover .video_overlay,
.chennal_video_img:hover .edit_chennal_video{
  opacity: 1;
}
.edit_chennal_video:hover ._chennal_video_action_btns{
  opacity: 1;
  pointer-events: initial;
}
._chennal_video_action_btns a:hover {
    text-decoration: none !important;
    color: #fb933c;
}
#global_content > div > div > div > div > div.tab3 > div > div > div > div > div > div > div.tip > span > a
{
  margin-left: 10px;
    margin-top: -5px;
}
 #global_content > div > div > div > div > div.tab3 > div > div > div > div > div > div > div.tip > span
 {
  padding-left: 30px !important;
    padding-top: 10px !important;
    padding-bottom: 10px !important;
    padding-right: 1px !important;
    background-position: 6px 10px !important;
 } 
 @media (max-width: 768px)
{
  .tabs2 {
    width: 100% !important;
  }
  .tabs {
    position: relative;
    border-bottom: 1px solid #e8e8e8;
    width: 100%;
    float: none;
  }
  .tab3 {
    left: 0%;
    width: 100%;
    border-left: 0px solid #ccc;
}
.heading {
    width: auto !important;
    padding: 0px 10px 10px 10px !important;
}
div.tabs > ul > li
{
     width: auto !important;
    float: left;
} 
}
</style>

<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
<h2 class="toptitle" style="border-bottom: 1px solid #ccc;">
    <?php echo $this->translate('Manage PDFs') ?>
  </h2>
  <div class="tabs2"> 
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>    
<div class="tab3">
<h2 class="heading"><a href="<?php echo $this->baseUrl().'/albums/manage';?>">Album</a></h2>
<h2 class="heading"><a style="border-bottom: 1px solid #fb933c; color: #fb933c;" href="<?php echo $this->baseUrl().'/pdf';?>">Pdf</a></h2>
<h2 class="heading"><a href="<?php echo $this->baseUrl().'/videos/manage';?>">Video/Audio</a></h2>
<p style="border-bottom: 1px solid #ccc;font-size: 0px;">&nbsp;</p>
<div class="container">
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 _chennal_video_list_section">
      <div class=" chennal_video_list _manage_pages_list">
        <div class="row">
          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
            <div class="chennal_video_list_area">
            <div class="_chennal_video_list_head">
              <div class="_chennal_video_list_head_content">PDFs</div>
              <a href="/create-pdf/createpdf/format/smoothbox" class="_add_chennal_video smoothbox"><i class="fa fa-plus"></i> New PDF</a>
            </div>

         <?php if( $this->paginator->getTotalItemCount() > 0 ){
          foreach( $this->paginator as $album ){
          $rec= Engine_Api::_()->getDbTable('pdfs', 'pdf')->getPageFile($album->pdf_id);
          $file=$rec[0]['pdf_file'];
          $modified_date=$rec[0]['modified_date'];
          ?>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 chennal_video_list_item">
                <div class="chennal_video_list_item_area">              
                  <div class="chennal_video_img"><div class="_chennal_video_action">
                    <div class="edit_chennal_video" href="javascript:void(0);" title=""><i class="fa fa-pencil"></i>
                    <div class="_chennal_video_action_btns">
                      <a class="smoothbox" href="<?php echo $this->baseUrl();?>/delete-pdf/deletepdf/<?php echo $album->pdf_id;?>/format/smoothbox">Delete PDF</a> 
                    </div>
                    </div>
                  </div><!-- <img src="img/video-1.jpg" class="img-responsive"> -->
                   <a href="<?php echo $this->baseUrl();?>/public/pdf/<?php echo $file;?>" target="_blank" class="wp_init"><img class="thumb_normal item_photo_album  thumb_normal" alt="" src="<?php echo $this->baseUrl();?>/public/PDF-icon.png"></a>
                  </div>
                  <div class="_chennal_video_list_item_title">
                  <a href="<?php echo $this->baseUrl();?>/public/pdf/<?php echo $file;?>" class="wp_init"><?php echo $album->getTitle(); ?></a>
                  </div>
                  <div class="_chennal_video_list_item_subtile">
                  <?php echo date("M d, Y",strtotime($modified_date));?>                 
                  </div>
                </div>
              </div>
                <?php } if( $this->paginator->count() > 1 ){ ?>
                <br />
                <?php echo $this->paginationControl($this->paginator, null, null); ?>
              <?php } ?>
              <?php } else {?>
                <div class="tip">
                 <span>
                <?php echo $this->translate("You do not have any PDF yet.");?>
                <a href="/create-pdf/createpdf/format/smoothbox" class="_add_chennal_video smoothbox"><i class="fa fa-plus"></i> Create a new PDF</a>
                  </span>
                </div>
              <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
      
  </div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
  $$('.custom_951').getParent().addClass('active');
</script>