<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: edit.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>

<div class="headline">
  <h2>
    <?php echo $this->translate('Photo Albums');?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>
</div>

<?php
  echo $this->form->render();
?>

<script type="text/javascript">
  $$('.core_main_album').getParent().addClass('active');
  $$('.custom_305').getParent().addClass('active');
  $$('.custom_317').getParent().addClass('active');
  $$('.custom_328').getParent().addClass('active');
  function validateeditalbum(){
	
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#title").val()) == ''){
		jQuery("#title").focus();
		jQuery("#title").addClass("com_form_error_validatin");
		return false;
	}
	
	jQuery('#albums_edit').submit();
	
}
</script>
