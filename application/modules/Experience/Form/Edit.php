<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Edit.php 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Experience_Form_Edit extends Engine_Form
{
  
  public function init()
  {
    $user = Engine_Api::_()->user()->getViewer();

    $user_level = Engine_Api::_()->user()->getViewer()->level_id;
    $this->setTitle('Edit Experience Deatils')
		->setAttrib('id', 'edit_exp_form')
      ->setAttrib('name', 'experience_edit');
    
	
	 // Init role
    $roleTable = Engine_Api::_()->getItemTable('role');
    $roles = $roleTable->select()
        ->from($roleTable, array('title', 'role_id'))
        ->where('row_status = ?', '1')
		->where('parent_id = ?', '0')
        ->query()
        ->fetchAll();
   // print_r($roles);
    $rolesOptions = array('' => 'Select Role');
    foreach( $roles as $role ) {
      $rolesOptions[$role['role_id']] = $role['title'];
    }
	
	// prepare roles
    $this->addElement('Select', 'role_id', array(
      'label' => 'Role',
      'multiOptions' => $rolesOptions,
	  'RegisterInArrayValidator' => false
    ));
	
	// prepare start dadte
    $this->addElement('Text', 'start_date', array(
      'label' => 'Start From',
	  'placeholder'	=>	'Select Start Date',
	  'class' => 'datepicker',
      'required'   => true,
    ));
	
	// prepare end_date
    $this->addElement('Text', 'end_date', array(
      'label' => 'End Date',
	  'placeholder'	=>	'Select End Date',
	  'class' => 'datepicker',
      'required'   => true,
    ));

    
	
    // Init descriptions
    $this->addElement('Textarea', 'description', array(
      'label' => 'Description',
	  'placeholder'	=>	'Enter Description',
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
        //new Engine_Filter_HtmlSpecialChars(),
        new Engine_Filter_EnableLinks(),
      ),
    ));

    // Submit or succumb!
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Experience',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    
    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'decorators' => array(
        'ViewHelper'
      )
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
  
}
