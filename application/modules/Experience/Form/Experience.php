<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Album.php 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Experience_Form_Experience extends Engine_Form
{
  public function init()
  {
    $user_level = Engine_Api::_()->user()->getViewer()->level_id;
    $user = Engine_Api::_()->user()->getViewer();

    // Init form
    $this
	->setTitle('Experience')
      ->setAttrib('id', 'add_exp_form')
      ->setAttrib('name', 'experience_form')
      ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()))
      ;
	  
	  // Init designation
    $roleTable = Engine_Api::_()->getItemTable('role');
    $roles = $roleTable->select()
        ->from($roleTable, array('title', 'role_id'))
        ->where('row_status = ?', '1')
		->where('parent_id = ?', '0')
        ->query()
        ->fetchAll();
   // print_r($roles);
    $rolesOptions = array('' => 'Select Role');
    foreach( $roles as $role ) {
      $rolesOptions[$role['role_id']] = $role['title'];
    }
	
	// prepare roles
    $this->addElement('Select', 'role_id', array(
      'label' => 'Designation',
      'multiOptions' => $rolesOptions,
    ));
	
    
	// prepare start dadte
    $this->addElement('Text', 'start_date', array(
      'label' => 'Start Date',
	  'placeholder'	=>	'Select Start Date',
	  'class' => 'datepicker',
      'required'   => true,
    ));
	
	// prepare end_date
    $this->addElement('Text', 'end_date', array(
      'label' => 'End Date',
	  'placeholder'	=>	'Select End Date',
	  'class' => 'datepicker',
      'required'   => true,
    ));
  
    // Init descriptions
    $this->addElement('Textarea', 'description', array(
      'label' => 'Description',
	  'placeholder'	=>	'Enter Description',
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
        //new Engine_Filter_HtmlSpecialChars(),
        new Engine_Filter_EnableLinks(),
      ),
    ));
    

   $this->addElement('Button', 'submit', array(
      'label' => 'Save Experience',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    
    $this->addElement('Button', 'cancel', array(
      'label' => 'cancel',
	  'id'		=>	'cancel_button',
      'link' => true,
      'prependText' => ' or ',
      'decorators' => array(
        'ViewHelper'
      )
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
	
  }

  public function saveExpValues($values, $user_id){
	  
		$values['owner_id']		=	$user_id;
		$values['start_date']	=	date('Y-m-d', strtotime($values['start_date']));
		$values['end_date']		=	date('Y-m-d', strtotime($values['end_date']));
		$values['owner_type']	=	'user';
		$values['created_on']	=	date('Y-m-d', time());
		$values['row_status']	=	'1';
		$experience = Engine_Api::_()->getDbtable('experiences', 'experience')->createRow();
		$experience->setFromArray($values);
		$experience->save();
	}
	
}
