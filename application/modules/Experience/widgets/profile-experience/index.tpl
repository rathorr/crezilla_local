<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Experience
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>
<?php 
  $this->headLink()
    ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/custom/custom.css');
?>
<script type="text/javascript">
  en4.core.runonce.add(function(){

    <?php if( !$this->renderOne ): ?>
    var anchor = $('profile_experience').getParent();
    $('profile_experience_previous').style.display = '<?php echo ( $this->paginator->getCurrentPageNumber() == 1 ? 'none' : '' ) ?>';
    $('profile_experience_next').style.display = '<?php echo ( $this->paginator->count() == $this->paginator->getCurrentPageNumber() ? 'none' : '' ) ?>';

    $('profile_experience_previous').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() - 1) ?>
        }
      }), {
        'element' : anchor
      })
    });

    $('profile_experience_next').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() + 1) ?>
        }
      }), {
        'element' : anchor
      })
    });
    <?php endif; ?>
  });
</script>
<ul id="profile_experience" class="thumbs">
  <?php
     $i=1;
     foreach( $this->paginator as $exp ):?>
    <div class="experience_section" id="experience_<?php echo $exp->experience_id; ?>">
          
          <div class="edu_listing">
    	<span><?php echo $i;?></span>
    	<div class="title"><span><?php echo $exp->title;?></span></div>
        <div class="activities">
        	<label>Time Period:&nbsp;</label><span><?php echo date('m/d/Y', strtotime($exp->start_date)).'&nbsp; - &nbsp;'.date('m/d/Y', strtotime($exp->end_date));?></span>
        </div>
        <?php if($this->cat_id == '10'){?>
        <div class="activities">
        	<label>Type Of Actor :&nbsp;</label><span><?php echo $exp->actor_type;?></span>
         </div>
        <?php } ?>
        <div class="activities">
        	<label>Channel :&nbsp;</label>
            <span>
                	<?php if($exp->channel == 'other'){
                    	echo $exp->other_channel;
                    }elseif($exp->channel == '0'){
                        echo 'No channel listed';
                    }else {
                        echo $exp->channel;
                    }?>
            </span>
       </div>
       
        <div class="activities">
        <label>Company :&nbsp;</label><span><?php echo $exp->company ? $exp->company : 'No company listed.';?></span>
        </div>
        
         <div class="activities">
        <label>Project :&nbsp;</label><span><?php echo $exp->project ? $exp->project : 'No project.';?></span>
        </div>
        
        <div class="activities">
        <label>Role :&nbsp;</label><span><?php echo $exp->role_cat_name ? $exp->role_name.' ('.$exp->role_cat_name.')' : 'No role specified.';?></span>
        </div>
        
         <div class="activities">
        <label>Specialization :&nbsp;</label><span><?php echo $exp->specialization ? $exp->specialization : 'No specialization.';?></span>
        </div>
        
        <div class="activities">
         <label>Description :&nbsp;</label><span><?php echo $exp->description ? $exp->description : 'No description';?></span>
        </div>
    </div>
    </div>
  <?php $i++; endforeach;?>
</ul>

<div>
  <div id="profile_experience_previous" class="paginator_previous">
    <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Previous'), array(
      'onclick' => '',
      'class' => 'buttonlink icon_previous'
    )); ?>
  </div>
  <div id="profile_experience_next" class="paginator_next">
    <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Next'), array(
      'onclick' => '',
      'class' => 'buttonlink_right icon_next'
    )); ?>
  </div>
</div>
