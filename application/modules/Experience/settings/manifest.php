<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'experience',
    'version' => '4.8.0',
    'path' => 'application/modules/Experience',
    'title' => 'Experience',
    'description' => 'Experience',
    'author' => '',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Experience',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/experience.csv',
    ),
  ),
  // Items ---------------------------------------------------------------------
  'items' => array(
    'experience',
	'role',
	'clients',
	'awards',
  'projects'
  ),
  // Routes --------------------------------------------------------------------
  'routes' => array(
    'experience_specific' => array(
      'route' => 'experience/:action/:experience_id/*',
      'defaults' => array(
        'module' => 'experience',
        'controller' => 'index',
        'action' => 'edit'
      ),
      'reqs' => array(
        'action' => '(delete|edit)',
      ),
    ),
	'experience_general' => array(
      'route' => 'experience/:action',
      'defaults' => array(
        'module' => 'experience',
        'controller' => 'index',
        'action' => 'index'
      ),
      'reqs' => array(
        'action' => '(index|add)',
      ),
    ),
	'experience_subcat' => array(
      'route' => 'experience/:action/:cat_id',
      'defaults' => array(
        'module' => 'experience',
        'controller' => 'index',
        'action' => 'getsubcat'
      ),
      'reqs' => array(
        'action' => '(getsubcat)',
      ),
    ),
  ),
); ?>