<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: content.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
return array(
  array(
    'title' => 'Profile Experience',
    'description' => 'Displays a member\'s experience on their profile.',
    'category' => 'Experience',
    'type' => 'widget',
    'name' => 'experience.profile-experience',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Experience',
      'titleCount' => true,
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
 
) ?>