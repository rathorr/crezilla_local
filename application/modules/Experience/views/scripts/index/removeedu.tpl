<?php $education	=	$this->educations;?>
<?php if($education){
	foreach($education as $edu){?>
   		<div class="edu_box" id="edu_box_<?php echo $edu->education_id;?>">
        	<span class="edit_input_list" onclick="edit_edu('<?php echo $edu->education_id;?>')">Edit</span>
        	<div class="fields_data_center">
            <span class="field_data_wrapper">
			<span class="field_data_label"><label>Qualification</label></span>
			<span class="inst_data" id="qua_<?php echo $edu->education_id;?>"><?php echo $edu->qualification;?></span>
			</span>
            <?php if($edu->year || $edu->institute){?>
            	
                	<?php if($edu->institute){?>
					<span class="field_data_wrapper">
                   <span class="field_data_label"> <label>Institute</label></span>
                        <span class="inst inst_data" id="ins_<?php echo $edu->education_id;?>">
                            <?php echo $edu->institute;?>
                        </span>
					</span>
					
                    <?php }
				
                     if($edu->year){?>
					 <span class="field_data_wrapper">
                     	<span class="field_data_label"><label>Year</label></span>
                        <span class="year inst_data" id="year_<?php echo $edu->education_id;?>">
                            <?php echo $edu->year;?>
                        </span>
					</span>
					
                    <?php } ?>
                
            <?php } ?>
			</div>
             <a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('edu').'/id/'.base64_encode($edu->education_id);?>" class="edu_rem smoothbox" style="color:#969696 !important;">Delete</a>
            <!--<span class="edu_rem" onclick="remove_edu('<?php echo $edu->education_id;?>')">Remove</span>-->
        </div>
<?php }
}?>