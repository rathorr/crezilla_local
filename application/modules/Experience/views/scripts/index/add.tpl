<div class="headline">
  <h2>
    <?php echo $this->translate('Edit My Profile');?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>
</div>
<div class="experience-section-top" style="padding:0 12px">
<h2 class="heading">Add More Experience</h2>
<a href="experience" id="add_exp" class="add_more icon_classified_new">Go Back</a>
</div>

<?php 
 $this->headScript()
    ->appendFile($this->layout()->staticBaseUrl . 'externals/jquery/jquery-ui.min.js');
 $this->headLink()
    ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/jquery/jquery-ui.css');
   $this->headLink()
    ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/custom/custom.css');
    ?>

  
<div id="exp-form">
	<?php //echo $this->form->render($this) ?>
    <form method="post" action="<?php echo $this->baseUrl();?>/experience/add" class="global_form" enctype="application/x-www-form-urlencoded" id="add_exp_form">
    <div>
    	<div>
    		<h3>Experience</h3>
                <div class="form-elements">
                <div class="form-wrapper" id="role_id-wrapper">
                <div class="form-label" id="role_id-label">
                <label class="optional" for="role_id">Designation</label></div>
                <div class="form-element" id="role_id-element">
                <select id="role_id" name="role_id">
                <option value="">Select Role</option>
                    <?php 
                    foreach($this->rolesOptions as $role){?>
                    	<option value="<?php echo $role['role_id']; ?>"><?php echo $role['title'];?></option>
                    <?php } ?>
                </select>
                </div>
                </div>
                <div class="form-wrapper" id="start_date-wrapper">
                <div class="form-label" id="start_date-label">
                <label class="required" for="start_date">Start Date</label></div>
                <div class="form-element" id="start_date-element">
                <input type="text" class="datepicker" placeholder="Select Start Date" value="" id="start_date" name="start_date"></div></div>
                <div class="form-wrapper" id="end_date-wrapper">
                <div class="form-label" id="end_date-label">
                <label class="required" for="end_date">End Date</label></div>
                <div class="form-element" id="end_date-element">
                <input type="text" class="datepicker" placeholder="Select End Date" value="" id="end_date" name="end_date"></div></div>
                <div class="form-wrapper" id="description-wrapper">
                <div class="form-label" id="description-label">
                <label class="optional" for="description">Description</label></div>
                <div class="form-element" id="description-element">
                <textarea placeholder="Enter Description" rows="6" cols="45" id="description" name="description"></textarea></div></div>
                
                <div class="form-wrapper key-client-add" id="client-wrapper">
                    <div class="form-label" id="client-label">
                        <label class="optional" for="client">Client(s):</label>
                    </div>
                    <div class="main_client_div">
              		  <div class="form-element" id="client-element">
                    <input type="text" id="client" name="client[]">
                  </div>
                    </div>
                <span id="add_more_client">Add More+</span>
               </div>
                
                <div class="form-wrapper key-award-add" id="award-wrapper">
                    <div class="form-label" id="award-label">
                        <label class="optional" for="award">Award(s):</label>
                    </div>
               		<div class="main_award_div">
                     <div class="form-element" id="award-element">
                    <input type="text" id="award" name="award[]">
                  </div>
                  </div>
                <span id="add_more_award">Add More+</span>
               </div>
   
                <div id="buttons-wrapper" class="form-wrapper">
                <fieldset id="fieldset-buttons">
                
                <button type="submit" id="submit" name="submit">Save Experience</button>
                
                <button prependtext=" or " link="1" type="button" id="cancel_button" name="cancel">cancel</button>
                </fieldset>
                </div>
                </div>
                </div>
                </div>
                </form>
    
    
</div>
<script type="application/javascript">
jQuery(document).ready(function(){
	jQuery('.user_edit_experience').parent().addClass('active');
	jQuery('.custom_315').parent().addClass('active');
	jQuery('.custom_304').parent().addClass('active');
	jQuery('.custom_922').parent().addClass('active');
	
	jQuery('#start_date').datepicker({ dateFormat: 'dd/mm/yy',changeMonth: true,
        changeYear: true, yearRange: '1970:2016' });
	jQuery('#end_date').datepicker({ dateFormat: 'dd/mm/yy',changeMonth: true,
        changeYear: true, yearRange: '1970:2016' });
	
});

jQuery(document).on('click','.delete_doc',function(){
	jQuery(this).parent().remove();	
});

jQuery('#add_more_client').on('click',function(){
	
jQuery('.main_client_div').append('<div class="form-element add-more-client" id="client-element"><input type="text" id="client" name="client[]"><span class="delete_doc">x</span></div>');	
});

jQuery('#add_more_award').on('click',function(){
	
jQuery('.main_award_div').append('<div class="form-element add-more-award " id="award-element"><input type="text" id="award" name="award[]"><span class="delete_doc">x</span></div>');	
});

jQuery(document).on('click', '#add_exp',function(){
	jQuery('#exp-form').show();
});

jQuery(document).on('click', '#cancel_button',function(){
	window.location.href="<?php echo $this->baseUrl().'/experience'?>";
});

/*jQuery(document).on('change', '#start_date, #end_date', function(){
	if((jQuery('#start_date').val() != '') && (jQuery('#end_date').val() != '')){
		if(jQuery('#start_date').val() > jQuery('#end_date').val()){
			//alert('Start date can not be greater than end date.');
			jQuery('#start_date').val('');
			jQuery('#end_date').val('');
			return false;	
		}	
	}
});*/


jQuery(document).on('submit', '#add_exp_form', function(){
		jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
		if(jQuery.trim(jQuery('#role_id').val()) == ''){
			jQuery('#role_id').focus();
			jQuery('#role_id').addClass("com_form_error_validatin");
			//alert('Please enter title.');
			return false;	
		}
		
		if(jQuery.trim(jQuery('#start_date').val()) == ''){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Please select start date.');
			return false;	
		}
		if(jQuery.trim(jQuery('#end_date').val()) == ''){
			jQuery('#end_date').focus();
			jQuery('#end_date').addClass("com_form_error_validatin");
			//alert('Please select end date.');
			return false;	
		}
		if(parseInt(fn_DateCompare(jQuery.trim(jQuery('#start_date').val()), jQuery.trim(jQuery('#end_date').val()))) == 1){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Start date can not be greater than end date.');
			jQuery('#start_date').val('');
			jQuery('#end_date').val('');
			return false;
		}
		
});


function fn_DateCompare(DateA, DateB) {
  var a = new Date(DateA.replace(/-/g, '/'));
  var b = new Date(DateB.replace(/-/g, '/'));

  var msDateA = Date.UTC(a.getFullYear(), a.getMonth()+1, a.getDate());
  var msDateB = Date.UTC(b.getFullYear(), b.getMonth()+1, b.getDate());

  if (parseFloat(msDateA) < parseFloat(msDateB))
	return -1;  // less than
  else if (parseFloat(msDateA) == parseFloat(msDateB))
	return 0;  // equal
else if (parseFloat(msDateA) > parseFloat(msDateB))
	return 1;  // greater than
else
	return null;
}  // error

</script>
 
