 <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
 <style type="text/css">
*
{
  font-family: 'Roboto', sans-serif !important;
}
.layout_main .layout_middle {
    width: 100% !important;
     background-color: #E9EAED !important;
}
.skip_div.abs_d {
    margin-top: 7px;
}
.middle_section_exp .form-elements .form-element textarea, .summary {
    background-color: #fff;
    border: 1px solid #dbdbdb;
    border-radius: 2px;
    padding: 2%;
    width: 100%;
    max-width: 100%;
    min-height:50px !important;
}
#autoSuggestionsList > li {
     background: none repeat scroll 0 0 #FFF;
     border-bottom: 1px solid #E3E3E3;
     list-style: none outside none;
     padding: 3px 15px 3px 15px;
     text-align: left;
}
.auto_list {
     border: 1px solid #E3E3E3;
     border-radius: 5px 5px 5px 5px;
     cursor:pointer;
     z-index: 999 !important;
     }
  

 #autoSuggestionsList > li:hover,#autoSuggestionsList > li.selected {
    background-color: #ccc;
}

#autoSuggestionsListp > li {
 background: none repeat scroll 0 0 #FFF;
 border-bottom: 1px solid #E3E3E3;
 list-style: none outside none;
 padding: 3px 15px 3px 15px;
 text-align: left;
}
.auto_listp {
 border: 1px solid #E3E3E3;
 border-radius: 5px 5px 5px 5px;
 cursor:pointer;
 z-index: 999 !important;
 }
  

#autoSuggestionsListp > li:hover,#autoSuggestionsListp > li.selected {
    background-color: #ccc;
}

#global_content > div > div.generic_layout_container.layout_main > div > div > div.tabs2 {
    width: 73%;
    background-color: #fff !important;
}

.tabs {
    position: absolute;
    padding: 0px !important;
    width: 20%;
    background-color: #fff !important;
    float: left;
}

#global_content > div > div.generic_layout_container.layout_main > div > div > div > div.tabs > ul > li {
    width: 100%;
    padding-top: 5px !important;
} 
.tab3 {
    position: relative;
    left: 25%;
    background-color: #fff;
    width: 75%;
    border-left: 1px solid #ccc;
    padding-top: 1px;
}

#qua-wrapper,#year-wrapper,.shortrap
{
    width: 49.5% !important;
    display: inline-block;
    padding-left: 10px;
}
.shortrap
{
    width: 49% !important;
    display: inline-block;
    padding-left: 10px;
}
.global_form > div > div {
    padding: 0 0 0 0 !important;
}
#ins-wrapper,#workexp-wrapper,.shortrap,#role_id-wrapper,#clnt-wrapper,#past-wrapper,
#desig_id-wrapper, #exp_pro-wrapper, #clint-wrapper, #year-wrapper, #pastp-wrapper,
#awd-wrapper, #awd_pro-wrapper, #year-wrapper1, #psum-wrapper
{
    padding-left: 10px;
}
#ins-element, #workexp-element,#role_id-element,
#clnt-element,#past-element,#desig_id-element, #exp_pro-element , #psum-element
{
    max-width: 98.5%;
}

.middle_section_exp .form-elements .form-element {
width: 100% !important;
max-width: 98.5%;
}
.middle_section_exp .exp_buttons {
text-align: left;
padding-left: 10px;
}
.cancel,
.middle_section_exp .exp_buttons fieldset button.cancel:hover
{
    background-color: #ccc !important;
    height: 35px;
    width: 100px;
    border-radius: 0px !important;
    margin-left: 10px !important;
    text-transform: capitalize;
}
html #TB_window
{
    width: 25%!important;
    left: 35%!important;
    max-height: 125px;
}

button#submit
{
    width: 30%;
}
#global_page_experience-index-index #global_page_experience-index-remove #fieldset-buttons > button#submit
{
    width: 50%;
} 
.psum
{
    height: auto !important;
    line-height: 20px !important;
}
div#autoSuggestionsList{
    position: absolute;
    width: 98.5%;
    max-height: 250px;
    overflow: auto;
}

div#role_id-element {
    position: absolute;
    width:100% !important;
    padding-left: 10px;
    z-index: 1;
    overflow: visible;
}
div#desig_id-element {
    position: inherit;
    width:100% !important;    
    padding-left: 10px;
    z-index: 1;
    overflow: visible;
}
div#role_id-wrapper {
    position: relative;

}
div#desig_id-wrapper {
    position: relative;
    padding-left: 0px;
}
.exp_form form#experience_form div#year-wrapper
{
    margin-top: 50px;
}
</style>

<div class="layout_page_experience_index_index">
<div class="generic_layout_container layout_top">
</div>

<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
<h2>Professional </h2>
	<!--SKIP Start-->
<div class="tabs2"> 
    <div class="tabs">
            <?php
              // Render the menu
                echo $this->navigation()
                ->menu()
                ->setContainer($this->navigation)
                ->render();
           ?>
     </div>
 <?php 
 $this->headScript()
    ->appendFile($this->layout()->staticBaseUrl . 'externals/jquery/jquery-1.10.2.js');
 $this->headScript()
    ->appendFile($this->layout()->staticBaseUrl . 'externals/jquery/jquery-ui.min.js');
 $this->headLink()
    ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/jquery/jquery-ui.css');
   $this->headLink()
    ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/custom/custom.css');
    
$experiences	=	$this->experiences;
?>

<div class="tab3"> 
 <div class="poss"><h3>Education<a href="javascript:void(0);" id="add_edu" class="add_details custm_mk_a" title="Add Education">Add</a></h3>
  
 </div>
 
 <div class="middle_section_exp">
 <div class="edu_form form_center" <?php if(isset($this->educations[0])){echo 'style="display:none;"';}else{'style="display:block;"';}?>>
 <form class="global_form" action="" method="post" id="education_form">
 <input type="hidden" id="edu_id" class="edu_id" />
    <div>
        <div>
                <div class="form-elements">
                <div id="qua-wrapper" class="form-wrapper">
                <div id="qua-element" class="form-element">
                 <input type="text" name="qualification" class="qualification" value="" placeholder="Enter qualification">
                </div>
                </div>

                <div id="year-wrapper" class="form-wrapper">
                <div id="year-element" class="form-element">
                    <input type="text" name="year" class="year" id="edu_year" value="" placeholder="Enter year">
                </div>
                </div>

                <div id="ins-wrapper" class="form-wrapper">
                <div id="ins-element" class="form-element">
                    <input type="text" name="institute" class="institute" value="" placeholder="Enter institute">
                </div>
                </div>
                <div class="form-wrapper exp_buttons" id="buttons-wrapper">
                <fieldset id="fieldset-buttons">
                <button name="save_edu" class="save" onclick="save_education()" type="button">Save</button>
                <button name="cancel" class="cancel" id="add_edu" type="button" link="1" prependtext=" or ">Cancel</button>
                </fieldset>
                </div>
                </div>
                </div>
                </div>
                </form>
 
 </div>
 <div class="edu_list edit_list">
    <?php
    $education  =   $this->educations;
    if(isset($education[0])){
     $icheck=0;
    foreach($this->educations as $edu){ 
    $icheck++; ?>
        <div class="edu_box" id="edu_box_<?php echo $edu->education_id;?>">
            <span class="<?php if ($icheck <=1){ echo 'edit_input_list';} else {echo 'edit_input_list2';}?>" onclick="edit_edu('<?php echo $edu->education_id;?>')">Edit</span>
            <div class="fields_data_center">
            <span class="field_data_wrapper shortrap">
            <span class="inst_data" id="qua_<?php echo $edu->education_id;?>"><?php echo $edu->qualification;?></span>
            </span>
            <?php if($edu->year || $edu->institute){?>
                
                <?php if($edu->year){?>
                     <span class="field_data_wrapper shortrap">
                      <span class="year inst_data" id="year_<?php echo $edu->education_id;?>">
                            <?php echo $edu->year;?>
                        </span>
                    </span>
                    
                    <?php } 
                    if($edu->institute){?>
                    <span class="field_data_wrapper">
                       <span class="inst inst_data" id="ins_<?php echo $edu->education_id;?>">
                            <?php echo $edu->institute;?>
                        </span>
                    </span>
                    
                    <?php }?>
                
            <?php } ?>
            </div>
            
            <a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('edu').'/id/'.base64_encode($edu->education_id);?>" class="<?php if ($icheck <=1){ echo 'edu_rem';} else {echo 'workexp_rem';}?> smoothbox">Delete</a>
        </div>
<?php }
}?>
 </div>
 </div>

 <div class="poss"><h3>Work Experience (Years)<a href="javascript:void(0);" id="add_workexp" class="add_details custm_mk_a" title="Add Work Experience" <?php if(isset($this->workexps[0])){echo 'style="display:none;"';}else{'style="display:block;"';}?>>Add</a></h3>
  
 </div>

        <div class="middle_section_exp">
        <div class="workexp_form form_center" <?php if(isset($this->workexps[0])){echo 'style="display:none;"';}else{'style="display:block;"';}?>>
        <form class="global_form" action="" method="post" id="workexp_form">
        <input type="hidden" id="workexp_id" class="workexp_id" />
        <div>
            <div>
                    <div class="form-elements">
                    <div id="workexp-wrapper" class="form-wrapper">
                    <div id="workexp-element" class="form-element">
                     <input type="text" name="workexp" id="workexp" class="workexp" maxlength="2" value="" placeholder="Enter work experience (e.g. 1 or 2 or 10) numeric only" onkeypress="return isNumberKey(event)">
                    </div>
                    </div>
                   
                    <div class="form-wrapper exp_buttons" id="buttons-wrapper">
                    <fieldset id="fieldset-buttons">
                    <button name="save_workexp" class="save" onclick="save_workexperience()" type="button">Save</button>
                    <button name="cancel" class="cancel" id="add_workexp" type="button" link="1" prependtext=" or ">Cancel</button>
                    </fieldset>
                    </div>
                    </div>
                    </div>
                    </div>
                    </form>

        </div>

        <div class="workexp_list edit_list">
        <?php
        $workexp  =   $this->workexps;
        if(isset($workexp[0])){
        foreach($this->workexps as $workexp){?>
            <div class="workexp_box" id="workexp_box_<?php echo $workexp->workexps_id;?>">
                <span class="edit_input_list2" onclick="edit_workexp('<?php echo $workexp->workexps_id;?>')">Edit</span>
                <div class="fields_data_center">
                <?php if($workexp->workexp){?>
                          <span class="field_data_wrapper">
               
                <span class="inst_data" id="workexp_<?php echo $workexp->workexps_id;?>"><?php echo $workexp->workexp;?></span>
                </span>
                <?php }?>
               
                </div>
               
                <a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('workexp').'/id/'.base64_encode($workexp->workexps_id);?>" class="workexp_rem smoothbox" >Delete</a>
            </div>
        <?php }
        }?>
        </div>
        </div>

        <div class="poss"><h3>Employment History<a href="javascript:void(0);" id="add_exp" class="add_details custm_mk_a"  title="Add Experience">Add</a></h3>
 
 </div>
 
  <div class="middle_section_exp">
 <div class="exp_form form_center"  <?php if(isset($this->experiences[0])){echo 'style="display:none;"';}else{'style="display:block;"';}?>>
 <?php if(isset($this->experiences[0])){$style= 'style="display:inline-block;"';}else{$style= 'style="display:none;"';}?>
 <form class="global_form" action="" method="post" id="experience_form">
 <input type="hidden" id="exp_id" class="exp_id" />
    <div>
        <div>
                <div class="form-elements">
                <div class="form-wrapper" id="role_id-wrapper">                
                <div class="form-element" id="role_id-element">
                <input type="text" name="role_id" class="role_id" id="role_id" autocomplete="off" value="" placeholder="Enter Role">
                <input type="hidden" name="role_idh" class="role_idh" id="role_idh" value="">
                <div id="autoSuggestionsList"></div>
                </div>
                </div>

                  
                 <div id="year-wrapper" class="form-wrapper">
                
                <div id="year-element" class="form-element">
                   <input type="text" name="expyearfrom" class="expyearfrom" value="" placeholder="Enter From year">
                </div>
                </div>
                <div id="year-wrapper" class="form-wrapper">
                
                <div id="year-element" class="form-element">
                    <input type="text" name="expyearto" class="expyearto" value="Ongoing" placeholder="Enter To year">
                </div>
                </div>

                <div id="clnt-wrapper" class="form-wrapper">
                <div id="clnt-element" class="form-element">
                    <input type="text" name="client" class="client" value="" placeholder="Enter company/client">
                </div>
                </div>
                
               
                
                <div id="past-wrapper" class="form-wrapper">
                <div id="past-element" class="form-element">
                    <textarea name="pastexp" class="pastexp" maxlength="300"  placeholder="Enter past experience description"></textarea>
                </div>
                </div>
                
                 <div class="form-wrapper  exp_buttons" id="buttons-wrapper">
                <fieldset id="fieldset-buttons">
                <button name="save_exp" class="save" onclick="save_experience()" type="button">Save</button>
                <button name="cancel" class="cancel" id="add_exp" type="button" link="1" prependtext=" or " <?php echo $style;?>>Cancel</button>
                </fieldset>
                </div>
                </div>
                </div>
                </div>
                </form>
 
 </div>
 
 <div class="exp_list edit_list">
    <?php
    $experience =   $this->experiences;
    if(isset($experience[0])){
    $icheck=0;
    foreach($this->experiences as $exp){
    $icheck++;?>
        <div class="exp_box" id="exp_box_<?php echo $exp->experience_id;?>">
            <span class="<?php if ($icheck <=1){ echo 'edit_input_list';} else {echo 'edit_input_list2';}?>" onclick="edit_exp('<?php echo $exp->experience_id;?>')">Edit</span>
            <div class="fields_data_center">
                <span class="field_data_wrapper">
    <span class="inst_data" id="rol_<?php echo $exp->experience_id;?>"><?php echo $exp->role_name;?></span>
    <input type="hidden" name="role_idh" class="role_idh" id="role_idh" value="<?php echo $exp->role_id;?>">

                </span>
                
                    <?php if($exp->fromyear){?>
                      <span class="field_data_wrapper shortrap">
                     <span class="year inst_data" id="expyearfrom_<?php echo $exp->experience_id;?>">
                            <?php echo $exp->fromyear;?>
                        </span>
                        </span>
                    <span class="field_data_wrapper shortrap">     
                    <span class="year inst_data" id="expyearto_<?php echo $exp->experience_id;?>">
                            <?php echo $exp->toyear;?>
                        </span>
                        
                    </span> 
                                
                    <?php } if($exp->client){?>
                  
                    <span class="field_data_wrapper">
                        <span class="inst_data" id="clnt_<?php echo $exp->experience_id;?>">
                            <?php echo $exp->client;?>
                        </span>
                    </span> 
                    <?php } if($exp->pastexp){?>
                    <span class="field_data_wrapper">
                        <p class="inst_data" id="pastexp_<?php echo nl2br($exp->experience_id);?>">
                            <?php echo $exp->pastexp;?>
                        </p>
                    </span> 
                    <?php }?>
            </div>
            <a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('exp').'/id/'.base64_encode($exp->experience_id);?>" class="<?php if ($icheck <=1){ echo 'edu_rem';} else {echo 'workexp_rem';}?> smoothbox">Delete</a>
        </div>
<?php }
}?>
 </div>
 </div>
 <div class="poss"><h3>Project History<a href="javascript:void(0);" id="add_project" class="add_details custm_mk_a"  title="Add Project">Add</a></h3>
 
 </div>
 
  <div class="middle_section_exp">
 <div class="pro_form form_center"  <?php if(isset($this->projects[0])){echo 'style="display:none;"';}else{'style="display:block;"';}?>>
 <?php if(isset($this->projects[0])){$style= 'style="display:inline-block;"';}else{$style= 'style="display:none;"';}?>
 <form class="global_form" action="" method="post" id="project_form">
 <input type="hidden" id="project_id" class="project_id" />
    <div>
    	<div>
                <div class="form-elements">
                <div class="form-wrapper" id="desig_id-wrapper">
                <div class="form-element" id="desig_id-element">
                <input type="text" name="desig_id" class="desig_id" id="desig_id" autocomplete="off" value="" placeholder="Enter Designation">
                <input type="hidden" name="desig_idh" class="desig_idh" id="desig_idh" value="">
                <div id="autoSuggestionsListp"></div>
                </div>
                </div>
                
                <div id="exp_pro-wrapper" class="form-wrapper">
                <div id="exp_pro-element" class="form-element">
                	<input type="text" name="exppro" class="exppro" value="" placeholder="Enter project">
                </div>
                </div>
                
                <div id="clint-wrapper" class="form-wrapper">
                <div id="clint-element" class="form-element">
                	<input type="text" name="clientp" class="clientp" value="" placeholder="Enter company/client">
                </div>
                </div>
                
                <div id="year-wrapper1" class="form-wrapper">
                <div id="year-element" class="form-element">
                	<input type="text" name="expyear" class="expyear" value="" placeholder="Enter year">
                </div>
                </div>
                
                <div id="pastp-wrapper" class="form-wrapper">
                <div id="pastp-element" class="form-element">
                	<textarea name="pastpexp" class="pastpexp" maxlength="300" placeholder="Enter project description"></textarea>
                </div>
                </div>
                
                 <div class="form-wrapper  exp_buttons" id="buttons-wrapper">
                <fieldset id="fieldset-buttons">
                <button name="save_pro" class="save" onclick="save_project()" type="button">Save</button>
                <button name="cancel" class="cancel" id="add_project" type="button" link="1" prependtext=" or " <?php echo $style;?>>Cancel</button>
                </fieldset>
                </div>
                </div>
                </div>
                </div>
                </form>
 
 </div>
 
 <div class="pro_list edit_list">
 	<?php
    $project	=	$this->projects;
    if(isset($project[0])){
    $icheck=0;
	foreach($this->projects as $pro){
    $icheck++;?>
   		<div class="pro_box" id="pro_box_<?php echo $pro->projects_id;?>">
        	<span class="<?php if ($icheck <=1){ echo 'edit_input_list';} else {echo 'edit_input_list2';}?>" onclick="edit_pro('<?php echo $pro->projects_id;?>')">Edit</span>
			<div class="fields_data_center">
				<span class="field_data_wrapper">
	<span class="inst_data" id="desig_<?php echo $pro->projects_id;?>"><?php echo $pro->desig_name;?></span>
    <input type="hidden" name="desig_idh" class="desig_idh" id="desig_idh" value="<?php echo $pro->desig_id;?>">
				</span>
				<?php if($pro->project || $pro->client || $pro->year){?>
            	
                	<?php  if($pro->project){?>
					<span class="field_data_wrapper">
                        <span class="pro inst_data" id="exppro_<?php echo $pro->projects_id;?>">
                            <?php echo $pro->project;?>
                        </span>
					</span>	
                    <?php }
                     if($pro->clientp){?>
					 <span class="field_data_wrapper">
                        <span class="inst_data" id="clintp_<?php echo $pro->projects_id;?>">
                            <?php echo $pro->clientp;?>
                        </span>
					</span>	
			
                    <?php } if($pro->year){?>
                    <span class="field_data_wrapper">
						<span class="year inst_data" id="expyear_<?php echo $pro->projects_id;?>">
                            <?php echo $pro->year;?>
                        </span>
					</span>	
                    
                    <?php } if($pro->pastpexp){?>
                    <span class="field_data_wrapper">
						<p class="inst_data" id="pastpexp_<?php echo nl2br($pro->projects_id);?>">
                            <?php echo $pro->pastpexp;?>
                        </p>
					</span>	
                    <?php }?>
                
				<?php } ?>
			</div>
            <a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('project').'/id/'.base64_encode($pro->projects_id);?>" class="<?php if ($icheck <=1){ echo 'edu_rem';} else {echo 'workexp_rem';}?> smoothbox">Delete</a>
        </div>
<?php }
}?>
 </div>
 </div>
 
 <div class="poss"><h3>Awards<a href="javascript:void(0);" id="add_awd" class="add_details custm_mk_a"  title="Add Awards">Add</a></h3>
  
 </div>
 
 <div class="middle_section_exp">

 <div class="awd_form form_center"   <?php if(isset($this->awards[0])){echo 'style="display:none;"';}else{'style="display:block;"';}?>>
 <form class="global_form" action="" method="post" id="award_form">
 <input type="hidden" id="awd_id" class="awd_id" />
    <div>
    	<div>
                <div class="form-elements">
                 <div id="awd-wrapper" class="form-wrapper">
                <div id="awd_yr-element" class="form-element">
                 <input type="text" name="award_year" class="award_year" value="" placeholder="Enter year">
                </div>
                </div>
                <div id="awd-wrapper" class="form-wrapper">
                <div id="awd-element" class="form-element">
                 <input type="text" name="award" class="award" value="" placeholder="Enter award">
                </div>
                </div>

                <div id="awd-wrapper" class="form-wrapper">
                <div id="awd_cat-element" class="form-element">
                 <input type="text" name="project" class="project" value="" placeholder="Enter project">
                </div>
                </div>
                <div id="awd-wrapper" class="form-wrapper">
                <div id="awd_cat-element" class="form-element">
                 <input type="text" name="award_cat" class="award_cat" value="" placeholder="Enter category">
                </div>
                </div>
                
                
                <div id="awd_pro-wrapper" class="form-wrapper">
                <div id="awd_pro-element" class="form-element">
                <select id="position_id" name="position_id" class="position_id">
                    <option value="">Select Position</option>
                    <option value="Winner">Winner</option>
                    <option value="Runners UP">Runners UP</option>
                    <option value="Nominated">Nominated</option>
                </select>
                </div>
                </div>
                
                            
                 <div class="form-wrapper  exp_buttons" id="buttons-wrapper">
                <fieldset id="fieldset-buttons">
                <button name="save_awd" class="save" onclick="save_award()" type="button">Save</button>
                <button name="cancel" class="cancel" id="add_awd" type="button" link="1" prependtext=" or ">Cancel</button>
                </fieldset>
                </div>
                </div>
                </div>
                </div>
                </form>
 
 </div>
 
 <div class="awd_list edit_list">
 	<?php
    $award	=	$this->awards;
    if(isset($award[0])){
    $icheck=0;
	foreach($this->awards as $awd){
    $icheck++;?>
   		<div class="adw_box" id="awd_box_<?php echo $awd->award_id;?>">
			
				<span class="<?php if ($icheck <=1){ echo 'edit_input_list';} else {echo 'edit_input_list2';}?>" onclick="edit_awd('<?php echo $awd->award_id;?>')">Edit</span>
			<div class="fields_data_center">
              <?php if($awd->year){?>
            <span class="field_data_wrapper">
                <span class="inst_data" id="awrd_yr_<?php echo $awd->award_id;?>"><?php echo $awd->year;?></span>
            </span>
            <?php } ?>
                <span class="field_data_wrapper">
                <span class="inst_data" id="awrd_<?php echo $awd->award_id;?>"><?php echo $awd->award;?></span>
            </span> 
            <span class="field_data_wrapper">   
                <span class="inst_data" id="pro_<?php echo $awd->award_id;?>"><?php echo $awd->project;?></span>
            </span>
            <span class="field_data_wrapper">
				<span class="inst_data" id="awrd_cat_<?php echo $awd->award_id;?>"><?php echo $awd->category;?></span>
			</span>
            
			<span class="field_data_wrapper">	
				<span class="inst_data" id="pos_<?php echo $awd->award_id;?>"><?php echo $awd->position;?></span>
			</span>
            
          
			</div>	
			<a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('awd').'/id/'.base64_encode($awd->award_id);?>" class="<?php if ($icheck <=1){ echo 'edu_rem';} else {echo 'workexp_rem';}?> smoothbox">Delete</a>
		</div>
<?php }
}?>
 </div>
 
 </div>
 
  <div class="poss"><h3>Professional Memberships<a href="javascript:void(0);" id="add_mem" class="add_details custm_mk_a" title="Add Memberships">Add</a></h3>
  
 </div>
 
 <div class="middle_section_exp">
 <div class="mem_form form_center" <?php if(isset($this->unionmemberships[0])){echo 'style="display:none;"';}else{'style="display:block;"';}?>>
 <form class="global_form" action="" method="post" id="membership_form">
 <input type="hidden" id="mem_id" class="mem_id" />
    <div>
        <div>
                <div class="form-elements">
                <div id="mem-wrapper" class="form-wrapper">
                <div id="mem-element" class="form-element">
                 <input type="text" name="unionmembership" class="unionmembership" value="" placeholder="Enter membership">
                </div>
                </div>
               
                <div class="form-wrapper exp_buttons" id="buttons-wrapper">
                <fieldset id="fieldset-buttons">
                <button name="save_mem" class="save" onclick="save_membership()" type="button">Save</button>
                <button name="cancel" class="cancel" id="add_mem" type="button" link="1" prependtext=" or ">Cancel</button>
                </fieldset>
                </div>
                </div>
                </div>
                </div>
                </form>
 
 </div>
 
 <div class="mem_list edit_list">
    <?php
    $membership  =   $this->unionmemberships;
    if(isset($membership[0])){
    $icheck=0;
    foreach($this->unionmemberships as $mem){ $icheck++;?>
        <div class="mem_box" id="mem_box_<?php echo $mem->unionmemberships_id;?>">
            <span class="<?php if ($icheck <=1){ echo 'edit_input_list';} else {echo 'edit_input_list2';}?>" onclick="edit_mem('<?php echo $mem->unionmemberships_id;?>')">Edit</span>
            <div class="fields_data_center">
            <?php if($mem->unionmembership){?>
                      <span class="field_data_wrapper">
            <span class="inst_data" id="mem_<?php echo $mem->unionmemberships_id;?>"><?php echo $mem->unionmembership;?></span>
            </span>
            <?php }?>
           
            </div>
           
            <a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('mem').'/id/'.base64_encode($mem->unionmemberships_id);?>" class="<?php if ($icheck <=1){ echo 'edu_rem';} else {echo 'workexp_rem';}?> smoothbox">Delete</a>
        </div>
<?php }
}?>
 </div>
 </div>
   <div class="poss"><h3>Professional Summary<a href="javascript:void(0);" id="add_psum" class="add_details custm_mk_a" title="Add Professional Summary" <?php if(isset($this->professionalsummarys[0])){echo 'style="display:none;"';}else{'style="display:block;"';}?>>Add</a></h3>
  
 </div>
 
 <div class="middle_section_exp">
 <div class="psum_form form_center" <?php if(isset($this->professionalsummarys[0])){echo 'style="display:none;"';}else{'style="display:block;"';}?>>
 <form class="global_form" action="" method="post" id="summary_form">
 <input type="hidden" id="psum_id" class="psum_id" />
    <div>
        <div>
                <div class="form-elements">
                <div id="psum-wrapper" class="form-wrapper">
                <div id="psum-element" class="form-element">
                  <textarea name="summary" class="summary" placeholder="Enter Professional Summary"></textarea>
                </div>
                </div>
               
                <div class="form-wrapper exp_buttons" id="buttons-wrapper">
                <fieldset id="fieldset-buttons">
                <button name="save_psum" class="save" onclick="save_summary()" type="button">Save</button>
                <button name="cancel" class="cancel" id="add_psum" type="button" link="1" prependtext=" or ">Cancel</button>
                </fieldset>
                </div>
                </div>
                </div>
                </div>
                </form>
 
 </div>
 
 <div class="psum_list edit_list">
    <?php
    $professionalsummary  =   $this->professionalsummarys;
    if(isset($professionalsummary[0])){ 
    foreach($this->professionalsummarys as $sum){?>
        <div class="psum_box" id="psum_box_<?php echo $sum->professionalsummarys_id;?>">
            <span class="edit_input_list2" onclick="edit_psum('<?php echo $sum->professionalsummarys_id;?>')">Edit</span>
            <div class="fields_data_center">
            <?php if($sum->summary){?>
                      <span class="field_data_wrapper">
            <span class="inst_data psum" id="psum_<?php echo $sum->professionalsummarys_id;?>"><?php echo $sum->summary;?></span>
            </span>
            <?php }?>
           
            </div>
           
            <a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('professionalsummary').'/id/'.base64_encode($sum->professionalsummarys_id);?>" class="workexp_rem smoothbox">Delete</a>
        </div>
<?php }
}?>
 </div>
 </div>
</div>
</div>
</div>
</div>
</div>
</div>
 <link href="<?php echo $this->baseUrl().'/public/custom/jquery.alerts.css?'.time() ?>" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/jquery.alerts.js';?>"></script>
<script src="https://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="application/javascript">
jQuery(document).on('click', '#add_edu', function(){
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	jQuery('.edu_id').val('');
	jQuery('.edu_form').slideToggle();
	jQuery("#education_form")[0].reset();
	jQuery('.edu_box').show();	
});

jQuery(document).on('click', '#add_mem', function(){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    jQuery('.mem_id').val('');
    jQuery('.mem_form').slideToggle();
    jQuery("#membership_form")[0].reset();
    jQuery('.mem_box').show();  
});

jQuery(document).on('click', '#add_workexp', function(){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    jQuery('.workexp_id').val('');
    jQuery('.workexp_form').slideToggle();
    jQuery("#workexp_form")[0].reset();
    jQuery('.workexp_box').show();  
});



jQuery(document).on('click', '#add_exp', function(){
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	jQuery("#role_id").find('option:selected').removeAttr("selected");
	jQuery('#role_id option[value=""]').attr('selected','selected');
	//jQuery('select[name="role_id"]').find('option:contains("Select")').attr("selected",true);
	jQuery('.exp_id').val('');
	jQuery('.exp_form').slideToggle();
	jQuery("#experience_form")[0].reset();
	jQuery('.exp_box').show();	
});


jQuery(document).on('click', '#add_awd', function(){
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	jQuery('.awd_id').val('');
	jQuery('.awd_form').slideToggle();
	jQuery("#award_form")[0].reset();
	jQuery('.adw_box').show();	
});

jQuery(document).on('click', '#add_project', function(){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    jQuery("#project_id").find('option:selected').removeAttr("selected");
    jQuery('#project_id option[value=""]').attr('selected','selected');
    //jQuery('select[name="role_id"]').find('option:contains("Select")').attr("selected",true);
    jQuery('.project_id').val('');
    jQuery('.pro_form').slideToggle();
    jQuery("#project_form")[0].reset();
    jQuery('.pro_box').show();  
});
jQuery(document).on('click', '#add_psum', function(){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    jQuery('.psum_id').val('');
    jQuery('.psum_form').slideToggle();
    jQuery("#summary_form")[0].reset();
    jQuery('.psum_box').show();  
});
jQuery(document).ready(function(){
	jQuery('.user_edit_experience').parent().addClass('active');
	jQuery('.custom_931').parent().addClass('active');
});

function save_education(){
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery('.qualification').val()) == ''){
			jQuery(".qualification").focus();
			jQuery(".qualification").addClass("com_form_error_validatin");	
			return false;
	}
   // return false;
	jQuery('.loading_image').show();
	jQuery.post('<?php echo $this->baseUrl()."/experience/index/saveedu";?>',{qua:jQuery.trim(jQuery('.qualification').val()),ins:jQuery.trim(jQuery('.institute').val()), year:jQuery.trim(jQuery('#edu_year').val()),edu_id:jQuery.trim(jQuery('.edu_id').val())}, function(resp){
		jQuery('.edu_list').html(jQuery.trim(resp));
		jQuery('.edu_form').slideToggle();
		jQuery('.loading_image').hide();
		Smoothbox.bind(jQuery('.edu_box'));
		return false;
	});	
}

function save_membership(){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    if(jQuery.trim(jQuery('.unionmembership').val()) == ''){
            jQuery(".unionmembership").focus();
            jQuery(".unionmembership").addClass("com_form_error_validatin");  
            return false;
    }
   
    jQuery('.loading_image').show();
    jQuery.post('<?php echo $this->baseUrl()."/experience/index/savemem";?>',{mem:jQuery.trim(jQuery('.unionmembership').val()),mem_id:jQuery.trim(jQuery('.mem_id').val())}, function(resp){
        jQuery('.mem_list').html(jQuery.trim(resp));
        jQuery('.mem_form').slideToggle();
        jQuery('.loading_image').hide();
        Smoothbox.bind(jQuery('.mem_box'));
        return false;
    }); 
}
function save_summary(){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    if(jQuery.trim(jQuery('.summary').val()) == ''){
            jQuery(".summary").focus();
            jQuery(".summary").addClass("com_form_error_validatin");  
            return false;
    }
   
    jQuery('.loading_image').show();
    jQuery.post('<?php echo $this->baseUrl()."/experience/index/savesum";?>',{sum:jQuery.trim(jQuery('.summary').val()),sum_id:jQuery.trim(jQuery('.psum_id').val())}, function(resp){
        jQuery('.psum_list').html(jQuery.trim(resp));
        jQuery('.psum_form').slideToggle();
        jQuery('.loading_image').hide();
        jQuery('a#add_psum').hide();
        Smoothbox.bind(jQuery('.psum_box'));
        return false;
    }); 
}

function save_workexperience(){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    if(jQuery.trim(jQuery('.workexp').val()) == ''){
            jQuery(".workexp").focus();
            jQuery(".workexp").addClass("com_form_error_validatin");  
            return false;
    }
   
    jQuery('.loading_image').show();
    jQuery.post('<?php echo $this->baseUrl()."/experience/index/saveworkexp";?>',{workexp:jQuery.trim(jQuery('.workexp').val()),workexp_id:jQuery.trim(jQuery('.workexp_id').val())}, function(resp){
        jQuery('.workexp_list').html(jQuery.trim(resp));
        jQuery('.workexp_form').slideToggle();
        jQuery('.loading_image').hide();
        jQuery('a#add_workexp').hide();

        Smoothbox.bind(jQuery('.workexp_box'));
        return false;
    }); 
}

function save_award(){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
     if(jQuery.trim(jQuery('.award').val()) == ''){
            jQuery(".award").focus();
            jQuery(".award").addClass("com_form_error_validatin");  
            return false;
    }
    if(jQuery.trim(jQuery('.award_cat').val()) == ''){
            jQuery(".award_cat").focus();
            jQuery(".award_cat").addClass("com_form_error_validatin");  
            return false;
    }   
    
    if(jQuery.trim(jQuery('.position_id').val()) == ''){
            jQuery(".position_id").focus();
            jQuery(".position_id").addClass("com_form_error_validatin");
            return false;   
    }
    jQuery('.loading_image').show();
    jQuery.post('<?php echo $this->baseUrl()."/experience/index/saveawd";?>',{cat:jQuery.trim(jQuery('.award_cat').val()),year:jQuery.trim(jQuery('.award_year').val()),awd:jQuery.trim(jQuery('.award').val()),pro:jQuery.trim(jQuery('.project').val()),pos:jQuery.trim(jQuery('.position_id').val()), awd_id:jQuery.trim(jQuery('.awd_id').val())}, function(resp){
        jQuery('.awd_list').html(jQuery.trim(resp));
        jQuery('.awd_form').slideToggle();
        jQuery('.loading_image').hide();
        Smoothbox.bind(jQuery('.awd_box'));
        return false;
    }); 
}
function save_experience(){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    if(jQuery.trim(jQuery('.role_id').val()) == ''){
            jQuery(".role_id").focus();
            jQuery(".role_id").addClass("com_form_error_validatin");    
            return false;
    }
    
    if(jQuery.trim(jQuery('.expyearfrom').val()) == ''){
            jQuery(".expyearfrom").focus();
            jQuery(".expyearfrom").addClass("com_form_error_validatin");    
            return false;
    }
    
    if(jQuery.trim(jQuery('.client').val()) == ''){
            jQuery(".client").focus();
            jQuery(".client").addClass("com_form_error_validatin"); 
            return false;
    }
    
    jQuery('.loading_image').show();
    jQuery.post('<?php echo $this->baseUrl()."/experience/index/saveexp";?>',{role_id:jQuery.trim(jQuery('#role_idh').val()),fromyear:jQuery.trim(jQuery('.expyearfrom').val()),toyear:jQuery.trim(jQuery('.expyearto').val()),pastexp:jQuery.trim(jQuery('.pastexp').val()),client:jQuery.trim(jQuery('.client').val()),project:jQuery.trim(jQuery('.exp_project').val()), exp_id:jQuery.trim(jQuery('.exp_id').val())}, function(resp){
        jQuery('.exp_list').html(jQuery.trim(resp));
        jQuery('.exp_form').slideToggle();
        jQuery('.loading_image').hide();
        Smoothbox.bind(jQuery('.exp_box'));
        return false;
    }); 
}

function save_project(){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    if(jQuery.trim(jQuery('.desig_id').val()) == ''){
            jQuery(".desig_id").focus();
            jQuery(".desig_id").addClass("com_form_error_validatin");    
            return false;
    }
    
    if(jQuery.trim(jQuery('.exppro').val()) == ''){
            jQuery(".exppro").focus();
            jQuery(".exppro").addClass("com_form_error_validatin");    
            return false;
    }
    
    if(jQuery.trim(jQuery('.clientp').val()) == ''){
            jQuery(".clientp").focus();
            jQuery(".clientp").addClass("com_form_error_validatin"); 
            return false;
    }
    
    jQuery('.loading_image').show();
    jQuery.post('<?php echo $this->baseUrl()."/experience/index/savepro";?>',{desig_id:jQuery.trim(jQuery('#desig_idh').val()),year:jQuery.trim(jQuery('.expyear').val()),pastpexp:jQuery.trim(jQuery('.pastpexp').val()),clientp:jQuery.trim(jQuery('.clientp').val()),project:jQuery.trim(jQuery('.exppro').val()), pro_id:jQuery.trim(jQuery('.project_id').val())}, function(resp){
        jQuery('.pro_list').html(jQuery.trim(resp));
        jQuery('.pro_form').slideToggle();
        jQuery('.loading_image').hide();
        Smoothbox.bind(jQuery('.pro_box'));
        return false;
    }); 
}
function edit_edu(edu_id){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	jQuery('.edu_box').show();
	jQuery('.qualification').val(jQuery.trim(jQuery('#qua_'+edu_id).html()));
	jQuery('.institute').val(jQuery.trim(jQuery('#ins_'+edu_id).html()));
	jQuery('.year').val(jQuery.trim(jQuery('#year_'+edu_id).html()));
	jQuery('.edu_id').val(edu_id);
	jQuery('.edu_form').slideDown();
	jQuery('#edu_box_'+edu_id).hide();	
}
function edit_mem(mem_id){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    jQuery('.mem_box').show();
    jQuery('.unionmembership').val(jQuery.trim(jQuery('#mem_'+mem_id).html()));
    jQuery('.mem_id').val(mem_id);
    jQuery('.mem_form').slideDown();
    jQuery('#mem_box_'+mem_id).hide();  
}
function edit_psum(sum_id){
    console.log('sum_id'+sum_id);
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    jQuery('.psum_box').show();
    jQuery('.summary').val(jQuery.trim(jQuery('#psum_'+sum_id).html()));
    jQuery('.psum_id').val(sum_id);
    jQuery('.psum_form').slideDown();
    jQuery('#psum_box_'+sum_id).hide();
    jQuery('a#add_psum').hide();  
}

function edit_workexp(workexp_id){
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    jQuery('.workexp_box').show();
    jQuery('.workexp').val(jQuery.trim(jQuery('#workexp_'+workexp_id).html()));
    jQuery('.workexp_id').val(workexp_id);
    jQuery('.workexp_form').slideDown();
    jQuery('#workexp_box_'+workexp_id).hide();
    jQuery('a#add_workexp').hide();  
}

function edit_awd(awd_id){
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	jQuery('.adw_box').show();
	jQuery('.award_cat').val(jQuery.trim(jQuery('#awrd_cat_'+awd_id).html()));
	jQuery('.award_year').val(jQuery.trim(jQuery('#awrd_yr_'+awd_id).html()));
	jQuery('.award').val(jQuery.trim(jQuery('#awrd_'+awd_id).html()));
	jQuery('.project').val(jQuery.trim(jQuery('#pro_'+awd_id).html()));
    jQuery('.position_id').val(jQuery.trim(jQuery('#pos_'+awd_id).html()));
	jQuery('.awd_id').val(awd_id);
	jQuery('.awd_form').slideDown();
	jQuery('#awd_box_'+awd_id).hide();	
}


function strip_tags( str ) {
    str=str.toString();
    return str.replace(/<\/?[^>]+>/gi, '');
}

function edit_exp(exp_id){
	var pastexp	=	jQuery('#pastexp_'+exp_id).html();
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	jQuery('.exp_box').show();
    jQuery('.role_id').val(jQuery.trim(jQuery('#rol_'+exp_id).html()));
    //jQuery('.exp_project').val(jQuery.trim(jQuery('#exppro_'+exp_id).html()));
	jQuery('.expyearfrom').val(jQuery.trim(jQuery('#expyearfrom_'+exp_id).html()));
    jQuery('.expyearto').val(jQuery.trim(jQuery('#expyearto_'+exp_id).html()));
	jQuery('.pastexp').val(jQuery.trim(strip_tags(jQuery('#pastexp_'+exp_id).html())));
	jQuery('.client').val(jQuery.trim(jQuery('#clnt_'+exp_id).html()));
	jQuery('.exp_id').val(exp_id);
	jQuery('.exp_form').slideDown();
	jQuery('#exp_box_'+exp_id).hide();	
}

function edit_pro(pro_id){
    //var pastexp =   jQuery('#pastpexp_'+pro_id).html();
    jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
    jQuery('.pro_box').show();
    jQuery('.desig_id').val(jQuery.trim(jQuery('#desig_'+pro_id).html()));
    jQuery('.exppro').val(jQuery.trim(jQuery('#exppro_'+pro_id).html()));
    jQuery('.expyear').val(jQuery.trim(jQuery('#expyear_'+pro_id).html()));
    jQuery('.pastpexp').val(jQuery.trim(strip_tags(jQuery('#pastpexp_'+pro_id).html())));
    jQuery('.clientp').val(jQuery.trim(jQuery('#clintp_'+pro_id).html()));
    jQuery('.project_id').val(pro_id);
    jQuery('.pro_form').slideDown();
    jQuery('#pro_box_'+pro_id).hide();  
}

     jQuery(document).on('keyup','#role_id', function(e) {
         console.log(jQuery(this).val().length);
         var inputString = jQuery(this).val();
         console.log(inputString.length);
        if(inputString.length < 1) {
                jQuery('#autoSuggestionsList').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/experience/index/getallroles";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                   // data = jQuery.parseJSON(data);
                    //data = JSON.parse(data);
                      if(data)
                        {
                            jQuery('#autoSuggestionsList').show();
                            jQuery('#autoSuggestionsList').addClass('auto_list');
                            jQuery('#autoSuggestionsList').html(data);
                        }
                        else
                        {
                            jQuery('#autoSuggestionsList').hide();
                        }
                          
                           
                     });
                 
             }
            
            
    });
      jQuery(document).on('click','#autoSuggestionsList li',function(){ 
            var el=jQuery(this).text();
            var elid=jQuery(this).attr('id');
            console.log("elid"+elid);
           jQuery('#role_id').val(el);
           jQuery('#role_idh').val(elid);
           // alert('hello'+el);
          // $('#search-column #submit-search').click();
        setTimeout("jQuery('#autoSuggestionsList').hide();", 200);
       });

      jQuery(document).on('keyup','#desig_id', function(e) {
         console.log(jQuery(this).val().length);
         var inputString = jQuery(this).val();
         console.log(inputString.length);
        if(inputString.length < 1) {
                jQuery('#autoSuggestionsListp').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/experience/index/getallroles";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                   // data = jQuery.parseJSON(data);
                    //data = JSON.parse(data);
                      if(data)
                        {
                            jQuery('#autoSuggestionsListp').show();
                            jQuery('#autoSuggestionsListp').addClass('auto_listp');
                            jQuery('#autoSuggestionsListp').html(data);
                        }
                        else
                        {
                            jQuery('#autoSuggestionsListp').hide();
                        }
                          
                           
                     });
                 
             }
            
            
    });
      jQuery(document).on('click','#autoSuggestionsListp li',function(){ 
            var el=jQuery(this).text();
            var elid=jQuery(this).attr('id');
            console.log("elid"+elid);
           jQuery('#desig_id').val(el);
           jQuery('#desig_idh').val(elid);
           // alert('hello'+el);
          // $('#search-column #submit-search').click();
        setTimeout("jQuery('#autoSuggestionsListp').hide();", 200);
       });
      function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)){
                return false;
            }
            return true;
        }
</script>
<script >
jQuery(document).ready(function(){
  var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
if( isMobile.any() )
{
 jQuery('a#add_edu, a#add_exp, a#add_project, a#add_awd, a#add_mem,a#add_psum, a#add_workexp, a.edu_rem, a.workexp_rem, span.edit_input_list, span.edit_input_list2').text(' ');
}
 
    });
    </script>