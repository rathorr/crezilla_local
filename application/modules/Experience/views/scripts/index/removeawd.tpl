<?php $award	=	$this->awards;?>
<?php if($award){
	foreach($award as $awd){?>
   		<div class="adw_box" id="awd_box_<?php echo $awd->award_id;?>">
			
				<span class="edit_input_list" onclick="edit_awd('<?php echo $awd->award_id;?>')">edit</span>
			<div class="fields_data_center">
            <span class="field_data_wrapper">
				<span class="field_data_label"><label>Category</label></span>
				<span class="inst_data" id="awrd_cat_<?php echo $awd->award_id;?>"><?php echo $awd->category;?></span>
			</span>
            
			<span class="field_data_wrapper">
				<span class="field_data_label"><label>Award</label></span>
				<span class="inst_data" id="awrd_<?php echo $awd->award_id;?>"><?php echo $awd->award;?></span>
			</span>	
				
			<span class="field_data_wrapper">	
				<span class="field_data_label"><label>Project</label></span>
				<span class="inst_data" id="pro_<?php echo $awd->award_id;?>"><?php echo $awd->project;?></span>
			</span>
            
            <?php if($awd->year){?>
            <span class="field_data_wrapper">
				<span class="field_data_label"><label>Year</label></span>
				<span class="inst_data" id="awrd_yr_<?php echo $awd->award_id;?>"><?php echo $awd->year;?></span>
			</span>
            <?php } ?>
			</div>	
            <a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('awd').'/id/'.base64_encode($awd->award_id);?>" class="awd_rem edu_rem smoothbox" style="color:#969696 !important;">Delete</a>
				<!--<span class="awd_rem edu_rem" onclick="remove_awd('<?php echo $awd->award_id;?>')">Remove</span>-->
			
		</div>
<?php }
}?>