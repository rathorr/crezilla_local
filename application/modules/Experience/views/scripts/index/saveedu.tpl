   <?php
    $education  =   $this->educations;
    if(isset($education[0])){
     $icheck=0;
    foreach($this->educations as $edu){ 
    $icheck++; ?>
        <div class="edu_box" id="edu_box_<?php echo $edu->education_id;?>">
            <span class="<?php if ($icheck <=1){ echo 'edit_input_list';} else {echo 'edit_input_list2';}?>" onclick="edit_edu('<?php echo $edu->education_id;?>')">Edit</span>
            <div class="fields_data_center">
            <span class="field_data_wrapper shortrap">
            <span class="inst_data" id="qua_<?php echo $edu->education_id;?>"><?php echo $edu->qualification;?></span>
            </span>
            <?php if($edu->year || $edu->institute){?>
                
                <?php if($edu->year){?>
                     <span class="field_data_wrapper shortrap">
                      <span class="year inst_data" id="year_<?php echo $edu->education_id;?>">
                            <?php echo $edu->year;?>
                        </span>
                    </span>
                    
                    <?php } 
                    if($edu->institute){?>
                    <span class="field_data_wrapper">
                       <span class="inst inst_data" id="ins_<?php echo $edu->education_id;?>">
                            <?php echo $edu->institute;?>
                        </span>
                    </span>
                    
                    <?php }?>
                
            <?php } ?>
            </div>
            
            <a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('edu').'/id/'.base64_encode($edu->education_id);?>" class="<?php if ($icheck <=1){ echo 'edu_rem';} else {echo 'workexp_rem';}?> smoothbox">Delete</a>
        </div>
<?php }
}?>