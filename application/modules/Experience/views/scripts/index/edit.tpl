<div class="headline">
  <h2>
    <?php echo $this->translate('Edit My Profile');?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>
</div><?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: edit.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>
<?php 
 $this->headScript()
    ->appendFile($this->layout()->staticBaseUrl . 'externals/jquery/jquery-ui.min.js');
 $this->headLink()
    ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/jquery/jquery-ui.css');
?>
<div class="experience-section-top" style="padding:0 12px">

  <h2 class="heading">
    <?php echo $this->translate('Edit Experience Details');?>
  </h2>

</div>
<?php $experience	=	$this->experience;?>
<script type="application/javascript">
jQuery(document).on('click', '#cancel_button',function(){
	window.location.href="<?php echo $this->baseUrl().'/experience'?>";
});	
var cat_id	=	'<?php echo $this->cat_id;?>';
jQuery(document).ready(function(){
	
	jQuery('.user_edit_experience').parent().addClass('active');
	jQuery('.custom_315').parent().addClass('active');
	jQuery('.custom_304').parent().addClass('active');
	jQuery('.custom_922').parent().addClass('active');
		
	
	var role_id	=	"<?php echo $this->experience['role_id']?>";
	
	jQuery('#start_date').datepicker({ dateFormat: 'dd/mm/yy',changeMonth: true,
        changeYear: true, yearRange: '1970:2016' });
	jQuery('#end_date').datepicker({ dateFormat: 'dd/mm/yy',changeMonth: true,
        changeYear: true, yearRange: '1970:2016' });
		
	jQuery('.delete_client_record').on('click', function(){
	var org_client_ids	=	jQuery('#remove_client_ids').val();
	var client_id		=	jQuery(this).parent().attr('id');
	jQuery('#remove_client_ids').val(org_client_ids+client_id+',');
	jQuery(this).parent().remove();
	
	});
	
	jQuery('.delete_award_record').on('click', function(){
		var org_award_ids	=	jQuery('#remove_award_ids').val();
		var award_id		=	jQuery(this).parent().attr('id');
		jQuery('#remove_award_ids').val(org_award_ids+award_id+',');
		jQuery(this).parent().remove();
		
	});
	
	jQuery(document).on('click','.delete_doc',function(){
		jQuery(this).parent().remove();	
	});
	
	jQuery('#add_more_client').on('click',function(){
	
jQuery('.main_client_div').append('<div class="form-element add-more-client" id="client-element"><input type="text" id="client" name="client[]"><input type="hidden" name="client_id[]" value=""  /><span class="delete_doc">x</span></div>');	
	});	
	
	jQuery('#add_more_award').on('click',function(){
		
	jQuery('.main_award_div').append('<div class="form-element add-more-award " id="award-element"><input type="text" id="award" name="award[]"><input type="hidden" name="award_id[]" value="" /><span class="delete_doc">x</span></div>');	
	});
	
});

jQuery(document).on('change', '#start_date, #end_date', function(){
	if(parseInt(fn_DateCompare(jQuery.trim(jQuery('#start_date').val()), jQuery.trim(jQuery('#end_date').val()))) == 1){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Start date can not be greater than end date.');
			jQuery('#start_date').val('');
			jQuery('#end_date').val('');
			return false;
		}
});

function fn_DateCompare(DateA, DateB) {
  var a = new Date(DateA.replace(/-/g, '/'));
  var b = new Date(DateB.replace(/-/g, '/'));

  var msDateA = Date.UTC(a.getFullYear(), a.getMonth()+1, a.getDate());
  var msDateB = Date.UTC(b.getFullYear(), b.getMonth()+1, b.getDate());

  if (parseFloat(msDateA) < parseFloat(msDateB))
	return -1;  // less than
  else if (parseFloat(msDateA) == parseFloat(msDateB))
	return 0;  // equal
else if (parseFloat(msDateA) > parseFloat(msDateB))
	return 1;  // greater than
else
	return null;}  // error


jQuery(document).on('submit', '#edit_exp_form', function(){
		if(jQuery.trim(jQuery('#role_id').val()) == ''){
			jQuery('#role_id').focus();
			jQuery('#role_id').addClass("com_form_error_validatin");
			//alert('Please enter title.');
			return false;	
		}
		
		if(jQuery.trim(jQuery('#start_date').val()) == ''){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Please select start date.');
			return false;	
		}
		if(jQuery.trim(jQuery('#end_date').val()) == ''){
			jQuery('#end_date').focus();
			jQuery('#end_date').addClass("com_form_error_validatin");
			//alert('Please select end date.');
			return false;	
		}
		if(parseInt(fn_DateCompare(jQuery.trim(jQuery('#start_date').val()), jQuery.trim(jQuery('#end_date').val()))) == 1){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Start date can not be greater than end date.');
			jQuery('#start_date').val('');
			jQuery('#end_date').val('');
			return false;
		}
		});


</script>

<form method="post" action="<?php echo $this->baseUrl();?>/experience/edit/<?php echo $experience['experience_id'];?>" class="global_form" enctype="application/x-www-form-urlencoded" id="edit_exp_form">
<input type="hidden" name="experience_id" value="<?php echo $experience['experience_id'];?>" />
    <div>
    	<div>
    <h3>Experience</h3>
        <div class="form-elements">
        <div class="form-wrapper" id="role_id-wrapper">
        <div class="form-label" id="role_id-label">
        <label class="optional" for="role_id">Designation</label></div>
        <div class="form-element" id="role_id-element">
        <select id="role_id" name="role_id">
        <option value="">Select Role</option>
            <?php 
            foreach($this->rolesOptions as $role){?>
                <option value="<?php echo $role['role_id'];?>" <?php if($role["role_id"] == $experience["role_id"]){echo "selected='selected'";}?>><?php echo $role['title'];?></option>
            <?php } ?>
        </select>
        </div>
        </div>
        <div class="form-wrapper" id="start_date-wrapper">
        <div class="form-label" id="start_date-label">
        <label class="required" for="start_date">Start Date</label></div>
        <div class="form-element" id="start_date-element">
        <input type="text" class="datepicker" placeholder="Select Start Date" value="<?php echo date('d/m/Y', strtotime($experience["start_date"]));?>" id="start_date" name="start_date"></div></div>
        <div class="form-wrapper" id="end_date-wrapper">
        <div class="form-label" id="end_date-label">
        <label class="required" for="end_date">End Date</label></div>
        <div class="form-element" id="end_date-element">
        <input type="text" class="datepicker" placeholder="Select End Date" value="<?php echo date('d/m/Y', strtotime($experience["end_date"]));?>" id="end_date" name="end_date"></div></div>
        <div class="form-wrapper" id="description-wrapper">
        <div class="form-label" id="description-label">
        <label class="optional" for="description">Description</label></div>
        <div class="form-element" id="description-element">
        <textarea placeholder="Enter Description" rows="6" cols="45" id="description" name="description"><?php echo $experience["description"];?></textarea></div></div>
        
         <div class="form-wrapper key-client-add" id="client-wrapper">
    	<div class="form-label" id="client-label">
        	<label class="optional" for="client">Client(s):</label>
        </div>
        <?php $clients	=	$this->clients;?>
	<div class="form-element main_client_div" id="client-element">
		
        <div class="form-element add-more-client" id="client-element">
        <input type="text" id="client" name="client[]" value="<?php echo ($clients[0])?$clients[0]['client_name']:'';?>">
        <input type="hidden" name="client_id[]" value="<?php echo ($clients[0])?$clients[0]['client_id']:'';?>" /> 
        </div>
        
        <?php 
        if($this->clients){$i=1;
        	foreach($this->clients as $client){
            	if($i>1){?>
               		 <div class="form-element add-more-client" id="<?php echo $client['client_id'];?>">
                     <input type="text" id="client" name="client[]" value="<?php echo $client['client_name'];?>">
                     <input type="hidden" name="client_id[]" value="<?php echo $client['client_id'];?>"  />
                     <span class="delete_client_record edit_delete_project_record"> x</span>
                     </div>
               <?php  }
            $i++;}
        }
        ?>
        <input type="hidden" name="remove_client_ids" value="" id="remove_client_ids"/>
      </div>
    <span id="add_more_client">Add More+</span>
   </div>
   
   		<div class="form-wrapper key-award-add" id="award-wrapper">
    	<div class="form-label" id="award-label">
        	<label class="optional" for="award">Award(s):</label>
        </div>
        <?php $awards	=	$this->awards;?>
	<div class="form-element main_award_div" id="award-element">
		
        <div class="form-element add-more-award" id="award-element">
        <input type="text" id="award" name="award[]" value="<?php echo ($awards[0])?$awards[0]['award_name']:'';?>">
        <input type="hidden" name="award_id[]" value="<?php echo ($awards[0])?$awards[0]['award_id']:'';?>" /> 
        </div>
        
        <?php 
        if($this->awards){$i=1;
        	foreach($this->awards as $award){
            	if($i>1){?>
               		 <div class="form-element add-more-award" id="<?php echo $award['award_id'];?>">
                     <input type="text" id="award" name="award[]" value="<?php echo $award['award_name'];?>">
                     <input type="hidden" name="award_id[]" value="<?php echo $award['award_id'];?>"  />
                     <span class="delete_award_record edit_delete_award_record"> x</span>
                     </div>
               <?php  }
            $i++;}
        }
        ?>
        <input type="hidden" name="remove_award_ids" value="" id="remove_award_ids"/>
      </div>
    <span id="add_more_award">Add More+</span>
   </div>
        <div id="buttons-wrapper" class="form-wrapper">
        <fieldset id="fieldset-buttons">
        
        <button type="submit" id="submit" name="submit">Save Experience</button>
        
        <button prependtext=" or " link="1" type="button" id="cancel_button" name="cancel">cancel</button>
        </fieldset>
        </div>
        </div>
        </div>
        </div>
        </form>
