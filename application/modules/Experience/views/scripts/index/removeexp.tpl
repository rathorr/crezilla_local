<?php $experience	=	$this->experiences;?>
<?php if(isset($experience[0])){
	foreach($experience as $exp){?>
   		<div class="exp_box" id="exp_box_<?php echo $exp->experience_id;?>">
        	<span class="edit_input_list" onclick="edit_exp('<?php echo $exp->experience_id;?>')">edit</span>
			<div class="fields_data_center">
				<span class="field_data_wrapper">
				<span class="field_data_label"><label>Role</label></span>
				<span class="inst_data" id="rol_<?php echo $exp->experience_id;?>"><?php echo $exp->role_name;?></span>
				</span>
				<?php if($exp->project || $exp->client || $exp->year){?>
            	
                	<?php if($exp->year){?>
                    <span class="field_data_wrapper">
					<span class="field_data_label"> <label>Year</label></span>
                     
						<span class="year inst_data" id="expyear_<?php echo $exp->experience_id;?>">
                            <?php echo $exp->year;?>
                        </span>
					</span>	
                    <?php } if($exp->project){?>
					<span class="field_data_wrapper">
                    	<span class="field_data_label"><label>Project</label></span>
                        <span class="pro inst_data" id="exppro_<?php echo $exp->experience_id;?>">
                            <?php echo $exp->project;?>
                        </span>
					</span>	
                    <?php }
                     if($exp->client){?>
					 <span class="field_data_wrapper">
                    <span class="field_data_label"> <label>Client</label></span>
                        <span class="inst_data" id="clnt_<?php echo $exp->experience_id;?>">
                            <?php echo $exp->client;?>
                        </span>
					</span>	
			
                    <?php } ?>
                
				<?php } ?>
			</div>
            <!--<span class="exp_rem edu_rem" onclick="remove_exp('<?php echo $exp->experience_id;?>')">Remove</span>-->
            <a href="<?php echo $this->baseUrl().'/experience/index/remove/type/'.base64_encode('exp').'/id/'.base64_encode($exp->experience_id);?>" class="exp_rem edu_rem smoothbox" style="color:#969696 !important;">Delete</a>
        </div>
<?php }
}?>