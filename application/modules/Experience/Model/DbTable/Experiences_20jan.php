<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Experience_Model_DbTable_Experiences extends Engine_Db_Table
{
  protected $_rowClass = 'Experience_Model_Experience';

  public function getExperienceSelect($options = array())
  {
    $select = $this->select();
    if( !empty($options['owner']) && 
        $options['owner'] instanceof Core_Model_Item_Abstract ) {
      $select
	  	->setIntegrityCheck ( false )
		->from ( array('t' => 'engine4_experience_experiences')) 
		->joinLeft ( array ('p' => 'engine4_experience_roles'), 't.role_id = p.role_id', array('title as role_name'))
		->joinLeft ( array ('p2' => 'engine4_experience_roles'), 't.role_subcat_id = p2.role_id', array('title as role_cat_name'))
        ->where('t.owner_type = ?', $options['owner']->getType())
        ->where('t.owner_id = ?', $options['owner']->getIdentity())
		->where('t.row_status = ?', 1)
        ->order('t.modified_date DESC')
        ;
		
    }
	
    if( !empty($options['search']) && is_numeric($options['search']) ) {
      $select->where('search = ?', $options['search']);
    }

    return $select;
  }

  public function getExperiencePaginator($options = array())
  {
    return Zend_Paginator::factory($this->getExperienceSelect($options));
  }

}