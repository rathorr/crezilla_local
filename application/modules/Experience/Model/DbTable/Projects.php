<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Experience_Model_DbTable_Projects extends Engine_Db_Table
{
  protected $_rowClass = 'Experience_Model_Projects';

  public function getProjectnames($ownerid){
    $select = $this->select("project")
               ->setIntegrityCheck(false)
              ->where('owner_id=?', $ownerid);
     $result  = $this->fetchAll($select);
    return $result;
 }
 
  public function getDesignationPro($ownerid){
    $select = $this->select()
    			->from ('engine4_experience_projects as t')	
               ->setIntegrityCheck(false)
              ->where('owner_id=?', $ownerid)
              ->joinLeft("engine4_classified_dasignatedsubcategories as t2", "t2.dasignatedsubcategory_id = t.desig_id", array('dasignatedsubcategory_name as desig_name'));
     $result  = $this->fetchAll($select);
    
    return $result;
 }
 
  public function getProjectsPaginator($options = array())
  {
    return Zend_Paginator::factory($this->getExperienceSelect($options));
  }

}