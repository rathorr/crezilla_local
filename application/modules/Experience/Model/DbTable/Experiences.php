<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Experience_Model_DbTable_Experiences extends Engine_Db_Table
{
  protected $_rowClass = 'Experience_Model_Experience';

  public function getExperienceSelect($options = array())
  {
    $select = $this->select();
    if( !empty($options['owner']) && 
        $options['owner'] instanceof Core_Model_Item_Abstract ) {
      $select
	  	->setIntegrityCheck ( false )
		->from ( array('t' => 'engine4_experience_experiences')) 
		->joinLeft ( array ('p' => 'engine4_experience_roles'), 't.role_id = p.role_id', array('title as role_name'))
        ->where('t.owner_type = ?', $options['owner']->getType())
        ->where('t.owner_id = ?', $options['owner']->getIdentity())
		->where('t.row_status = ?', 1)
        ->order('t.modified_date DESC')
        ;
		
    }
	
    if( !empty($options['search']) && is_numeric($options['search']) ) {
      $select->where('search = ?', $options['search']);
    }

    return $select;
  }

   public function getDesignationExp($ownerid){
    $select = $this->select()
                ->from ('engine4_experience_experiences as t') 
               ->setIntegrityCheck(false)
              ->where('owner_id=?', $ownerid)
              ->joinLeft("engine4_classified_dasignatedsubcategories as t2", "t2.dasignatedsubcategory_id = t.role_id", array('dasignatedsubcategory_name as desig_name'))
              ->order("t.experience_id DESC");
     $result  = $this->fetchAll($select);
   return $result;
 }
 
  public function getExperiencePaginator($options = array())
  {
    return Zend_Paginator::factory($this->getExperienceSelect($options));
  }

}