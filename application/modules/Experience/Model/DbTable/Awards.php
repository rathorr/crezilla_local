<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Transactions.php 27.07.11 15:15 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class Experience_Model_DbTable_Awards extends Engine_Db_Table
{
  protected $_rowClass = 'Experience_Model_Award';
	
	public function getExperienceAwards($exp_id)
	{
		//echo $page_id;die;
		if (!$exp_id){
			return false;
		}
		
		$select = $this->select()
      	->where('experience_id = ?',$exp_id)
		->where('is_deleted = ?',0)
      	;
		
		$result	=	 $this->fetchAll($select);
		$maindata=	array();
		if($result){
			foreach($result as $res){
				$data['award_id']	=	$res['award_id'];
				$data['award_name']=	$res['award_name'];
				$data['experience_id']	=	$res['experience_id'];
				$data['is_deleted']	=	$res['is_deleted'];
				$data['created_on']	=	$res['created_on'];	
				$maindata[]	=	$data;
			}	
		}
		return $maindata;
		
	}

	public function getawardlist($userid)
	{
		$select = $this->select()
				->where("owner_id = ?",$userid)
				->where("row_status = ?", 1)
				->order("creation_date desc");
	return $result	= $this->fetchAll($select);
	}
	
}