<?php

class Experience_IndexController extends Core_Controller_Action_Standard
{
  public function init()
  {
    //if( !$this->_helper->requireAuth()->setAuthParams('experience', null, 'view')->isValid() ) return;
    
     if( 0 !== ($experience_id = (int) $this->_getParam('experience_id')) &&
        null !== ($experience = Engine_Api::_()->getItem('experience', $experience_id)) )
    {
      Engine_Api::_()->core()->setSubject($experience);
    }
	
	 // Set up navigation
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	 if(strtolower($profile_type) == 'talent'){
		 $menu_type	=	'user_edit';
	 }else if(strtolower($profile_type) == 'agency'){
		$menu_type	=	'custom_33'; 
	 }else{
		$menu_type	=	'custom_31'; 
		
		if( isset($is_company->value) && $is_company->value !="" )
		{
			//echo $is_company->value;
			$menu_type	=	'custom_34'; 
		}
		
	 }
	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit');
  }
  
  public function indexAction()
  {
  	/*$abc='Associate Director';
  	$subdesigid = Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getsubdesigIDByname($abc);
	  		print_r($subdesigid);
	  		die();*/
	$viewer = Engine_Api::_()->user()->getViewer()->getIdentity();
	
	if( !$viewer ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
	
	$exptable = Engine_Api::_()->getDbTable('experiences', 'experience');
    $expName = $exptable->info('name');
	
	/*$roletable = Engine_Api::_()->getDbTable('roles', 'experience');
    $roleName = $roletable->info('name');*/

    $roletable = Engine_Api::_()->getDbTable('dasignatedsubcategories', 'classified');
    $roleName = $roletable->info('name');
	
	$edutable = Engine_Api::_()->getDbTable('educations', 'education');
    $eduName = $edutable->info('name');
    $memtable = Engine_Api::_()->getDbTable('unionmemberships', 'user');
    $memName = $memtable->info('name'); 
	
	//$table = Engine_Api::_()->getItemTable('education');
    $eduselect = $edutable->select("$eduName.*")
					->where("$eduName.owner_id = $viewer", 1)
					->where("$eduName.row_status = 1", 1)
					->order("$eduName.creation_date desc");
	// get the data
	$this->view->educations = $edutable->fetchAll($eduselect);

	$memselect = $memtable->select("$memName.*")
					->where("$memName.owner_id = $viewer", 1)
					->where("$memName.row_status = 1", 1)
					->order("$memName.creation_date desc");
	// get the data
	$this->view->unionmemberships = $memtable->fetchAll($memselect);
	
	$workexptable = Engine_Api::_()->getDbTable('workexps', 'user');
    $workexpName = $workexptable->info('name'); 
		$workexpselect = $workexptable->select("$workexpName.*")
					->where("$workexpName.owner_id = $viewer", 1)
					->where("$workexpName.row_status = 1", 1)
					->order("$workexpName.creation_date desc");
	// get the data
	$this->view->workexps = $workexptable->fetchAll($workexpselect);
	
	
    /*$expselect = $exptable->select("$expName.*")
						->setIntegrityCheck(false)
						->joinLeft($roleName, "$roleName.role_id = $expName.role_id", array('title as role_name'))
                        ->where("$expName.owner_id = $viewer", 1)
						->where("$expName.row_status = 1", 1)
                        ->order("$expName.creation_date DESC");*/
     $expselect = $exptable->select("$expName.*")
						->setIntegrityCheck(false)
						->joinLeft($roleName, "$roleName.dasignatedsubcategory_id = $expName.role_id", array('dasignatedsubcategory_name as role_name','dasignatedsubcategory_id as role_id'))
                        ->where("$expName.owner_id = $viewer", 1)
						->where("$expName.row_status = 1", 1)
                        ->order("$expName.creation_date DESC");
	// get the data
	$this->view->experiences =$exptable->fetchAll($expselect);
	
	$protable = Engine_Api::_()->getDbTable('projects', 'experience');
    	$proName = $protable->info('name');
	
		$proselect = $protable->select("$proName.*")
						->setIntegrityCheck(false)
						->joinLeft($roleName, "$roleName.dasignatedsubcategory_id = $proName.desig_id", array('dasignatedsubcategory_name as desig_name','dasignatedsubcategory_id as desig_id'))
                        ->where("$proName.owner_id = $viewer", 1)
						->where("$proName.row_status = 1", 1)
                        ->order("$proName.creation_date DESC");
		// get the data
	$this->view->projects =$protable->fetchAll($proselect);

	$awdtable = Engine_Api::_()->getDbTable('awards', 'experience');
    $awdName = $awdtable->info('name'); 
	
	//$table = Engine_Api::_()->getItemTable('education');
    $awdselect = $awdtable->select("$awdName.*")
					->where("$awdName.owner_id = $viewer", 1)
					->where("$awdName.row_status = 1", 1)
					->order("$awdName.creation_date desc");
	// get the data
	$this->view->awards = $awdtable->fetchAll($awdselect);

	$sumtable = Engine_Api::_()->getDbTable('professionalsummarys', 'user');
		$sumName = $sumtable->info('name'); 
		
		$sumselect = $sumtable->select("$sumName.*")
						->where("$sumName.owner_id = $viewer", 1)
						->where("$sumName.row_status = 1", 1)
						->order("$sumName.creation_date desc");
		// get the data
		$this->view->professionalsummarys = $sumtable->fetchAll($sumselect);
  }
  
  public function removeAction(){
  	$viewer	=	Engine_Api::_()->user()->getViewer()->getIdentity();
		$type = base64_decode($this->_getParam('type'));  
		if($this->getRequest()->isPost()){
			if($type == 'edu'){
				$delitem	=	'education';
				$this->_helper->layout()->disableLayout(); 
				$edu_id	=	base64_decode($this->_getParam('id'));
				$edutable = Engine_Api::_()->getDbTable('educations', 'education');
				$condition = array(
			    'education_id = ?' => $edu_id,
			    'owner_id = ?' => $viewer
				);
				
				$edutable->delete($condition);
			}elseif($type == 'exp'){
				$delitem	=	'experience';
				$this->_helper->layout()->disableLayout(); 
				$exp_id	=	base64_decode($this->_getParam('id'));
				$experiencetable = Engine_Api::_()->getDbTable('experiences', 'experience');
				$condition = array(
			    'experience_id = ?' => $exp_id,
			    'owner_id = ?' => $viewer
				);
				
				$experiencetable->delete($condition);
			}elseif($type == 'awd'){
				$delitem	=	'award';
				$this->_helper->layout()->disableLayout(); 
				$awd_id	=	base64_decode($this->_getParam('id'));
	   			$awardstable = Engine_Api::_()->getDbTable('awards', 'experience');
				$condition = array(
			    'award_id = ?' => $awd_id,
			    'owner_id = ?' => $viewer
				);
				
				$awardstable->delete($condition);	
			}elseif($type == 'mem'){
				$delitem	=	'unionmembership';
				$this->_helper->layout()->disableLayout(); 
				$mem_id	=	base64_decode($this->_getParam('id'));
				$unionmembershiptable = Engine_Api::_()->getDbTable('unionmemberships', 'user');
				$condition = array(
			    'unionmemberships_id = ?' => $mem_id,
			    'owner_id = ?' => $viewer
				);
				
				$unionmembershiptable->delete($condition);
			}elseif($type == 'workexp'){
				$delitem	=	'workexp';
				$this->_helper->layout()->disableLayout(); 
				$workexp_id	=	base64_decode($this->_getParam('id'));
				$workexptable = Engine_Api::_()->getDbTable('workexps', 'user');
				$condition = array(
			    'workexps_id = ?' => $workexp_id,
			    'owner_id = ?' => $viewer
				);
				
				$workexptable->delete($condition);
				
			}elseif($type == 'project'){
				$delitem	=	'project';
				$this->_helper->layout()->disableLayout(); 
				$project_id	=	base64_decode($this->_getParam('id'));
				$projecttable = Engine_Api::_()->getDbTable('projects', 'experience');
				$condition = array(
			    'projects_id = ?' => $project_id,
			    'owner_id = ?' => $viewer
				);
				
				$projecttable->delete($condition);
				
			}elseif($type == 'professionalsummary'){
				$delitem	=	'professionalsummary';
				$this->_helper->layout()->disableLayout(); 
				$professionalsummary_id	=	base64_decode($this->_getParam('id'));
				$professionalsummarytable = Engine_Api::_()->getDbTable('professionalsummarys', 'user');
				$condition = array(
			    'professionalsummarys_id = ?' => $professionalsummary_id,
			    'owner_id = ?' => $viewer
				);
				
				$professionalsummarytable->delete($condition);
				
			}
			
			$this->_forward('success', 'utility', 'core', array(
			  'smoothboxClose' => true,
			  'parentRefresh' => true,
			  'messages' => array(Zend_Registry::get('Zend_Translate')->_("Your $delitem has been deleted."))
			));
		}else{
			$this->view->type = $this->_getParam('type');
			$this->view->id = $this->_getParam('id');
		}
  }
  public function saveeduAction(){
	  	$viewer	=	Engine_Api::_()->user()->getViewer()->getIdentity();
	  	$this->_helper->layout()->disableLayout(); 
		$values['qualification']=	$_POST['qua'];
		$values['institute']	=	$_POST['ins'];
		$values['year']			=	$_POST['year'];
		$values['edu_id']		=	$_POST['edu_id'];
		$values['owner_id']		=	$viewer;
		if($values['edu_id']){
			$edu = Engine_Api::_()->getItem('education', $values['edu_id']);
		}else{
			$edu = Engine_Api::_()->getDbtable('educations', 'education')->createRow();
		}
		$edu->setFromArray($values);
		$edu->save();
		
		$edutable = Engine_Api::_()->getDbTable('educations', 'education');
		$eduName = $edutable->info('name'); 
		
		$eduselect = $edutable->select("$eduName.*")
						->where("$eduName.owner_id = $viewer", 1)
						->where("$eduName.row_status = 1", 1)
						->order("$eduName.creation_date desc");
		// get the data
		$this->view->educations = $edutable->fetchAll($eduselect);
  }
  

   public function savememAction(){
	  	$viewer	=	Engine_Api::_()->user()->getViewer()->getIdentity();
	  	$this->_helper->layout()->disableLayout(); 
		$values['unionmembership']=	$_POST['mem'];
		$values['unionmemberships_id']		=	$_POST['mem_id'];
		$values['owner_id']		=	$viewer;
		if($values['unionmemberships_id']){
			$edu = Engine_Api::_()->getItem('unionmembership', $values['unionmemberships_id']);
		}else{
			$edu = Engine_Api::_()->getDbtable('unionmemberships', 'user')->createRow();
		}
		$edu->setFromArray($values);
		$edu->save();
		
		$memtable = Engine_Api::_()->getDbTable('unionmemberships', 'user');
		$memName = $memtable->info('name'); 
		
		$memselect = $memtable->select("$memName.*")
						->where("$memName.owner_id = $viewer", 1)
						->where("$memName.row_status = 1", 1)
						->order("$memName.creation_date desc");
		// get the data
		$this->view->unionmemberships = $memtable->fetchAll($memselect);
  }

   public function saveworkexpAction(){
	  	$viewer	=	Engine_Api::_()->user()->getViewer()->getIdentity();
	  	$this->_helper->layout()->disableLayout(); 
		$values['workexp']=	$_POST['workexp'];
		$values['workexps_id']		=	$_POST['workexp_id'];
		$values['owner_id']		=	$viewer;
		if($values['workexps_id']){
			$workexp = Engine_Api::_()->getItem('workexp', $values['workexps_id']);
		}else{
			$workexp = Engine_Api::_()->getDbtable('workexps', 'user')->createRow();
		}
		$workexp->setFromArray($values);
		$workexp->save();
		
		$workexptable = Engine_Api::_()->getDbTable('workexps', 'user');
		$workexpName = $workexptable->info('name'); 
		
		$workexpselect = $workexptable->select("$workexpName.*")
						->where("$workexpName.owner_id = $viewer", 1)
						->where("$workexpName.row_status = 1", 1)
						->order("$workexpName.creation_date desc");
		// get the data
		$this->view->workexps = $workexptable->fetchAll($workexpselect);
  }


  public function saveexpAction(){
	  	$viewer	=	Engine_Api::_()->user()->getViewer()->getIdentity();
	  	$this->_helper->layout()->disableLayout(); 
	  	
		$values['role_id']		=	$_POST['role_id'];
		$values['client']		=	$_POST['client'];
		$values['project']		=	$_POST['project'];
		$values['fromyear']		=	$_POST['fromyear'];
		$values['toyear']		=	$_POST['toyear'];
		
		$pastexp=$_POST['pastexp'];
		$textToStore = nl2br(htmlentities($pastexp, ENT_QUOTES, 'UTF-8'));
		$values['pastexp']		=	$textToStore;
		$values['exp_id']		=	$_POST['exp_id'];
		$values['owner_id']		=	$viewer;
		if($values['exp_id']){
			$exp = Engine_Api::_()->getItem('experience', $values['exp_id']);
		}else{
			$exp = Engine_Api::_()->getDbtable('experiences', 'experience')->createRow();
		}
		$exp->setFromArray($values);
		$exp->save();
		
		$exptable = Engine_Api::_()->getDbTable('experiences', 'experience');
    	$expName = $exptable->info('name');
	
		/*$roletable = Engine_Api::_()->getDbTable('roles', 'experience');
    	$roleName = $roletable->info('name');*/
    	$roletable = Engine_Api::_()->getDbTable('dasignatedsubcategories', 'classified');
    	$roleName = $roletable->info('name');
		
		$expselect = $exptable->select("$expName.*")
						->setIntegrityCheck(false)
						->joinLeft($roleName, "$roleName.dasignatedsubcategory_id = $expName.role_id", array('dasignatedsubcategory_name as role_name','dasignatedsubcategory_id as role_id'))
                        ->where("$expName.owner_id = $viewer", 1)
						->where("$expName.row_status = 1", 1)
                        ->order("$expName.creation_date DESC");

			// get the data
	$this->view->experiences =$exptable->fetchAll($expselect);
  }
  
    public function saveproAction(){
	  	$viewer	=	Engine_Api::_()->user()->getViewer()->getIdentity();
	  	$this->_helper->layout()->disableLayout(); 
	  	
		$values['desig_id']		=	$_POST['desig_id'];
		$values['clientp']		=	$_POST['clientp'];
		$values['project']		=	$_POST['project'];
		$values['year']			=	$_POST['year'];
		$pastpexp				=	$_POST['pastpexp'];
		$textToStore = nl2br(htmlentities($pastpexp, ENT_QUOTES, 'UTF-8'));
		$values['pastpexp']		=	$textToStore;
		$values['pro_id']		=	$_POST['pro_id'];
		$values['owner_id']		=	$viewer;
		if($values['pro_id']){
			$pro = Engine_Api::_()->getItem('projects', $values['pro_id']);
		}else{
			$pro = Engine_Api::_()->getDbtable('projects', 'experience')->createRow();
		}
		//echo '<pre>';print_r($values);die();
		$pro->setFromArray($values);
		$pro->save();
		
		
    	$roletable = Engine_Api::_()->getDbTable('dasignatedsubcategories', 'classified');
    	$roleName = $roletable->info('name');

		$protable = Engine_Api::_()->getDbTable('projects', 'experience');
    	$proName = $protable->info('name');
	
		$proselect = $protable->select("$proName.*")
						->setIntegrityCheck(false)
						->joinLeft($roleName, "$roleName.dasignatedsubcategory_id = $proName.desig_id", array('dasignatedsubcategory_name as desig_name','dasignatedsubcategory_id as desig_id'))
                        ->where("$proName.owner_id = $viewer", 1)
						->where("$proName.row_status = 1", 1)
                        ->order("$proName.creation_date DESC");

			// get the data
	$this->view->projects =$protable->fetchAll($proselect);
  }
  public function saveawdAction(){
	  	$viewer	=	Engine_Api::_()->user()->getViewer()->getIdentity();
	  	$this->_helper->layout()->disableLayout();
		$values['category']	=	$_POST['cat'];
		$values['year']		=	$_POST['year']; 
		$values['award']	=	$_POST['awd'];
		$values['project']	=	$_POST['pro'];
		$values['position']	=	$_POST['pos'];
		$values['awd_id']	=	$_POST['awd_id'];
		$values['owner_id']	=	$viewer;
		if($values['awd_id']){
			$awd = Engine_Api::_()->getItem('awards', $values['awd_id']);
		}else{
			$awd = Engine_Api::_()->getDbtable('awards', 'experience')->createRow();
		}
		$awd->setFromArray($values);
		$awd->save();
		
		$awdtable = Engine_Api::_()->getDbTable('awards', 'experience');
		$awdName = $awdtable->info('name'); 
		
		//$table = Engine_Api::_()->getItemTable('education');
		$awdselect = $awdtable->select("$awdName.*")
						->where("$awdName.owner_id = $viewer", 1)
						->where("$awdName.row_status = 1", 1)
						->order("$awdName.creation_date desc");
		// get the data
		$this->view->awards = $awdtable->fetchAll($awdselect);
  }
   public function savesumAction(){
	  	$viewer	=	Engine_Api::_()->user()->getViewer()->getIdentity();
	  	$this->_helper->layout()->disableLayout(); 
		$values['summary']=	$_POST['sum'];
		$values['professionalsummarys_id']	=	$_POST['sum_id'];
		$values['owner_id']		=	$viewer;
		if($values['professionalsummarys_id']){
			$sum = Engine_Api::_()->getItem('professionalsummarys', $values['professionalsummarys_id']);
		}else{
			$sum = Engine_Api::_()->getDbtable('professionalsummarys', 'user')->createRow();
		}
		$sum->setFromArray($values);
		$sum->save();
		
		$sumtable = Engine_Api::_()->getDbTable('professionalsummarys', 'user');
		$sumName = $sumtable->info('name'); 
		
		$sumselect = $sumtable->select("$sumName.*")
						->where("$sumName.owner_id = $viewer", 1)
						->where("$sumName.row_status = 1", 1)
						->order("$sumName.creation_date desc");
		// get the data
		$this->view->professionalsummarys = $sumtable->fetchAll($sumselect);
  }

/*  public function addAction()
  {
	$viewer = Engine_Api::_()->user()->getViewer()->getIdentity();
	
	if( !$viewer ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
	
	
	$table = Engine_Api::_()->getDbtable('users', 'user');
	
	$roleTable = Engine_Api::_()->getItemTable('role');
    $roles = $roleTable->select()
        ->from($roleTable, array('title', 'role_id'))
        ->where('row_status = ?', '1')
        ->query()
        ->fetchAll();
   // print_r($roles);
    foreach( $roles as $role ) {
      $rolesOption['title'] = $role['title'];
	  $rolesOption['role_id'] = $role['role_id'];
	  $rolesOptions[]	=	$rolesOption;
    }
	$this->view->rolesOptions	=	$rolesOptions;
	$this->view->form = $form = new Experience_Form_Experience();
	
	if($this->getRequest()->getPost()){
		$values	=	$this->getRequest()->getPost();
		$values['owner_id']		=	$viewer;
		$values['start_date']	=	date('Y-m-d', strtotime($values['start_date']));
		$values['end_date']		=	date('Y-m-d', strtotime($values['end_date']));
		$values['owner_type']	=	'user';
		$values['created_on']	=	date('Y-m-d', time());
		$values['row_status']	=	'1';
		$experience = Engine_Api::_()->getDbtable('experiences', 'experience')->createRow();
		$experience->setFromArray($values);
		$experience->save();
		
		// SAVE CLIENTS
		  if(!empty($values['client'])){
				$tableclient = Engine_Api::_()->getDbTable('clients', 'experience');
				foreach($values['client'] as $clint){
					$client = $tableclient->createRow();
					$client->client_name		=	$clint;
					$client->experience_id		=	$experience->experience_id;
					$client->created_on			=	date('Y-m-d H:i:s', time());
					$client->save();	
				}  
		  }
		  
		  // SAVE AWARDS
		  if(!empty($values['award'])){
				$tableaward = Engine_Api::_()->getDbTable('awards', 'experience');
				foreach($values['award'] as $awrd){
					$award = $tableaward->createRow();
					$award->award_name	=	$awrd;
					$award->experience_id		=	$experience->experience_id;
					$award->created_on	=	date('Y-m-d H:i:s', time());
					$award->save();	
				}  
		  }
		
		
		//$experience = $form->saveExpValues($this->getRequest()->getPost(), $viewer);	
		return $this->_helper->redirector->gotoRoute(array('action' => 'index'), 'experience_general', true);
		die();
	}
	
	$this->view->experiences = 	"";
	
  }*/
  
  
  
 /* public function deleteAction() {
    $viewer = Engine_Api::_()->user()->getViewer();
    $experience = Engine_Api::_()->getItem('experience', $this->getRequest()->getParam('experience_id'));
    //if( !$this->_helper->requireAuth()->setAuthParams($experience, null, 'delete')->isValid()) return;

    // In smoothbox
    $this->_helper->layout->setLayout('default-simple');
    
    $this->view->form = $form = new Experience_Form_Delete();

    if( !$experience )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_("Experience doesn't exists or not authorized to delete");
      return;
    }

    if( !$this->getRequest()->isPost() )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
      return;
    }

    $db = $experience->getTable()->getAdapter();
    $db->beginTransaction();

    try
    {
		
	  $experience['row_status'] = 2;
	  $experience->save();
      $db->commit();
    }

    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }

    $this->view->status = true;
    $this->view->message = Zend_Registry::get('Zend_Translate')->_('Experience has been deleted.');
    return $this->_forward('success' ,'utility', 'core', array(
      'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'index'), 'experience_general', true),
      'messages' => Array($this->view->message)
    ));
  }*/
  
 /*  public function editAction(){
   
   
   $viewer = Engine_Api::_()->user()->getViewer()->getIdentity();
	
	if( !$viewer ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
    // Prepare data
	
	$roleTable = Engine_Api::_()->getItemTable('role');
    $roles = $roleTable->select()
        ->from($roleTable, array('title', 'role_id'))
        ->where('row_status = ?', '1')
        ->query()
        ->fetchAll();
   // print_r($roles);
    foreach( $roles as $role ) {
      $rolesOption['title'] = $role['title'];
	  $rolesOption['role_id'] = $role['role_id'];
	  $rolesOptions[]	=	$rolesOption;
    }
	$this->view->rolesOptions	=	$rolesOptions;
	
	$table = Engine_Api::_()->getDbtable('users', 'user');
	
    $this->view->experience = $experience = Engine_Api::_()->core()->getSubject();

    // Make form
    //$this->view->form = $form = new Experience_Form_Edit();
    
		if($this->getRequest()->isPost() )
		{
		  $values = $_POST;
		  $values['start_date']	=	date('Y-m-d', strtotime($values['start_date']));
		  $values['end_date']	=	date('Y-m-d', strtotime($values['end_date']));
		  $experience->setFromArray($values);
		  $experience->save();
		 
		   // ADD OR REMOVE CLIENTS
		  if($values['remove_client_ids']){
			  $remove_client_ids	=	explode(',',$values['remove_client_ids']);
			  foreach($remove_client_ids as $clnt){
				  if($clnt){
					$clint	=	Engine_Api::_()->getItem('clients', $clnt);
					$clint->is_deleted	=	1;
					$clint->save();
				  }
			  }
		  }
		  
		  
		  if(!empty($values['client'])){
				$i=0;
				foreach($values['client'] as $clint){
					if($clint){
						if($values["client_id"][$i]){
							$client	=	Engine_Api::_()->getItem('clients', $values["client_id"][$i]);
							$client->client_name	=	$clint;
							$client->experience_id		=	$experience->experience_id;
							$client->created_on	=	date('Y-m-d H:i:s', time());
							$client->save();
							
						}else{
							$tableclient = Engine_Api::_()->getDbTable('clients', 'experience');
							$client = $tableclient->createRow();
							$client->client_name	=	$clint;
							$client->experience_id		=	$experience->experience_id;
							$client->created_on	=	date('Y-m-d H:i:s', time());
							$client->save();		
						}
					}
				$i++;
				}  
		  }
		   
		  // ADD OR REMOVE AWARDS
		  if($values['remove_award_ids']){
			  $remove_award_ids	=	explode(',',$values['remove_award_ids']);
			  foreach($remove_award_ids as $awrd){
				  if($awrd){
					$awrd	=	Engine_Api::_()->getItem('awards', $awrd);
					$awrd->is_deleted	=	1;
					$awrd->save();
				  }
			  }
		  }
	  
	  if(!empty($values['award'])){
			$i=0;
			foreach($values['award'] as $awrd){
				if($awrd){
					if($values["award_id"][$i]){
						$award	=	Engine_Api::_()->getItem('awards', $values["award_id"][$i]);
						$award->award_name	=	$awrd;
						$award->experience_id		=	$experience->experience_id;
						$award->created_on	=	date('Y-m-d H:i:s', time());
						$award->save();
						
					}else{
						$tableaaward = Engine_Api::_()->getDbTable('awards', 'page');
						$award = $tableaaward->createRow();
						$award->award_name	=	$awrd;
						$award->experience_id		=	$experience->experience_id;
						$award->created_on	=	date('Y-m-d H:i:s', time());
						$award->save();		
					}
				}
			$i++;
			}  
	  }
		  
		  return $this->_helper->redirector->gotoRoute(array('action' => 'index'), 'experience_general', true);
		} 
		
    $this->view->clients	=	Engine_Api::_()->getDbTable('clients', 'experience')->getExperienceClients($experience->experience_id);
	
	$this->view->awards		=	Engine_Api::_()->getDbTable('awards', 'experience')->getExperienceAwards($experience->experience_id);
   }*/
   
 /*  public function getsubcatAction(){
	   $this->view->catid	=	$this->_request->getParam('cat_id');
	   $this->_helper->layout()->disableLayout(); 
	   
	   $roleTable = Engine_Api::_()->getItemTable('role');
    	$roles = $roleTable->select()
        ->from($roleTable, array('title', 'role_id'))
        ->where('row_status = ?', '1')
		->where('parent_id = ?', $this->_request->getParam('cat_id'))
        ->query()
        ->fetchAll();
		
		$this->view->roles	=	$roles;
   }*/

  public function getallrolesAction(){
   		  $search = $_POST['queryString'];

        $search = trim(preg_replace('!\s+!', ' ', $search));
        // echo $search;
   		 // die();
       if($search !='') {
	        $subdesig = Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getsearchByname($search);
			//print_r($subdesig);
			//die();
			//$this->view->subdesig	=	$subdesig;

         //$lis= "<li class='selected'></li>";
	        $lis= "";

        foreach($subdesig as $row){
        	$lis.= "<li id='".$row["dasignatedsubcategory_id"]."'>".$row["dasignatedsubcategory_name"]."</li>";
      		/*$data[] = array(
		          'id'    => $row["dasignatedsubcategory_id"],
		          'name'  => $row["dasignatedsubcategory_name"],
		        );*/
		       // $lis.="<li>".$row["dasignatedsubcategory_name"]."</li>";
        	}
        	$this->_helper->json($lis);
        }
   }
}
