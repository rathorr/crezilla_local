<?php

class Experience_IndexController extends Core_Controller_Action_Standard
{
  public function init()
  {
    //if( !$this->_helper->requireAuth()->setAuthParams('experience', null, 'view')->isValid() ) return;
    
     if( 0 !== ($experience_id = (int) $this->_getParam('experience_id')) &&
        null !== ($experience = Engine_Api::_()->getItem('experience', $experience_id)) )
    {
      Engine_Api::_()->core()->setSubject($experience);
    }
	
	 // Set up navigation
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	 if(strtolower($profile_type) == 'talent'){
		 $menu_type	=	'user_edit';
	 }else if(strtolower($profile_type) == 'agency'){
		$menu_type	=	'custom_33'; 
	 }else{
		$menu_type	=	'custom_31'; 
		
		if( isset($is_company->value) && $is_company->value !="" )
		{
			//echo $is_company->value;
			$menu_type	=	'custom_34'; 
		}
		
	 }
	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation($menu_type);
  }
  
  public function indexAction()
  {
	$viewer = Engine_Api::_()->user()->getViewer()->getIdentity();
	
	if( !$viewer ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
	
	/*$this->view->navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('user_edit');*/
	
	$table = Engine_Api::_()->getDbtable('users', 'user');
	$userdata = $table->getUserCategory($viewer);
	
	$this->view->form = $form = new Experience_Form_Experience();
	
	if($this->getRequest()->getPost()){
		$experience = $form->saveExpValues($this->getRequest()->getPost(), $viewer, $userdata->value, $userdata->label);	
	}
	
	$table = Engine_Api::_()->getDbTable('experiences', 'experience');
    $expName = $table->info('name');
	
	$roletable = Engine_Api::_()->getDbTable('roles', 'experience');
    $roleName = $roletable->info('name');
	
	$roletable2 = Engine_Api::_()->getDbTable('roles', 'experience');
    $roleName2 = $roletable2->info('name'); 
	
	//$table = Engine_Api::_()->getItemTable('experience');
      $select = $table->select("$expName.*")
						->setIntegrityCheck(false)
						->joinLeft($roleName, "$roleName.role_id = $expName.role_id", array('title as role_name'))
						->joinLeft($roleName2, "engine4_experience_roles_2.role_id = $expName.role_subcat_id", array('title as role_cat_name'))
                        ->where("$expName.owner_id = $viewer", 1)
						->where("$expName.row_status = 1", 1)
                        ->order("$expName.creation_date DESC");
	// get the data
	$result = $table->fetchAll($select);
	//echo '<pre>'; print_r($result); die;
	
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	if(isset($is_company->value))
	$this->view->is_company  = $is_company->value;



	$this->view->experiences = 	$result;
	$this->view->cat_id		 =	$userdata->value;
  }
  
  
  
  
  public function addAction()
  {
	$viewer = Engine_Api::_()->user()->getViewer()->getIdentity();
	
	if( !$viewer ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
	
	/*$this->view->navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('user_edit');*/
	
	$table = Engine_Api::_()->getDbtable('users', 'user');
	$userdata = $table->getUserCategory($viewer);
	
	$this->view->form = $form = new Experience_Form_Experience();
	
	if($this->getRequest()->getPost()){
		$experience = $form->saveExpValues($this->getRequest()->getPost(), $viewer, $userdata->value, $userdata->label);	
		return $this->_helper->redirector->gotoRoute(array('action' => 'index'), 'experience_general', true);
		die();
	}
	
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	if(isset($is_company->value))
	$this->view->is_company  = $is_company->value;

	$this->view->experiences = 	"";
	$this->view->cat_id		 =	$userdata->value;
	
  }
  
  
  
  public function deleteAction() {
    $viewer = Engine_Api::_()->user()->getViewer();
    $experience = Engine_Api::_()->getItem('experience', $this->getRequest()->getParam('experience_id'));
    //if( !$this->_helper->requireAuth()->setAuthParams($experience, null, 'delete')->isValid()) return;

    // In smoothbox
    $this->_helper->layout->setLayout('default-simple');
    
    $this->view->form = $form = new Experience_Form_Delete();

    if( !$experience )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_("Experience doesn't exists or not authorized to delete");
      return;
    }

    if( !$this->getRequest()->isPost() )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
      return;
    }

    $db = $experience->getTable()->getAdapter();
    $db->beginTransaction();

    try
    {
	  $experience['row_status'] = 2;
	  $experience->save();
      //experience->delete();
      $db->commit();
    }

    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }

    $this->view->status = true;
    $this->view->message = Zend_Registry::get('Zend_Translate')->_('Experience has been deleted.');
    return $this->_forward('success' ,'utility', 'core', array(
      'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'index'), 'experience_general', true),
      'messages' => Array($this->view->message)
    ));
  }
  
   public function editAction(){
   
   /*$this->view->navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('user_edit');*/
	  
   $viewer = Engine_Api::_()->user()->getViewer()->getIdentity();
	
	if( !$viewer ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
    // Prepare data
	
	$table = Engine_Api::_()->getDbtable('users', 'user');
	$userdata = $table->getUserCategory($viewer);
	
	$this->view->cat_id		 =	$userdata->value;
	
    $this->view->experience = $experience = Engine_Api::_()->core()->getSubject();

	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	if(isset($is_company->value))
	$this->view->is_company  = $is_company->value;


    // Make form
    $this->view->form = $form = new Experience_Form_Edit();
    
    if( !$this->getRequest()->isPost() )
    {
	  $formData	=	$experience->toArray();
	  
	  $formData['other_channel'] 	= ($formData['channel'] == 'other') ? $formData['other_channel'] : '';
	  
      $form->populate($formData);
      
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
      return;
    } 

    if( !$form->isValid($this->getRequest()->getPost()) )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid data');
      return;
    }

    // Process
    $db = $experience->getTable()->getAdapter();
    $db->beginTransaction();

    try
    {
      $values = $form->getValues();
	  //echo '<pre>'; print_r($values); echo '</pre>';
      $experience->setFromArray($values);
      $experience->save();

      $db->commit();
    }
    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }

        return $this->_helper->redirector->gotoRoute(array('action' => 'index'), 'experience_general', true);
  
   }
   
   public function getsubcatAction(){
	   $this->view->catid	=	$this->_request->getParam('cat_id');
	   $this->_helper->layout()->disableLayout(); 
	   
	   $roleTable = Engine_Api::_()->getItemTable('role');
    	$roles = $roleTable->select()
        ->from($roleTable, array('title', 'role_id'))
        ->where('row_status = ?', '1')
		->where('parent_id = ?', $this->_request->getParam('cat_id'))
        ->query()
        ->fetchAll();
		
		$this->view->roles	=	$roles;
   }
}
