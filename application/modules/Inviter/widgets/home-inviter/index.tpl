<?php $settings = Engine_Api::_()->getDbTable('settings', 'core'); $key = $settings->__get('inviter.linkedin.consumer.key', 0); ?>
<script type="text/javascript" src="//platform.linkedin.com/in.js">
api_key: <?php echo $key; ?>
</script>



<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Quiz
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: view.tpl 2010-07-02 17:53 ermek $
 * @author     Ermek
 */
?>
<?php if ($this->success) : ?>
  <script type="text/javascript">
    en4.core.runonce.add(function () {
      var success = '<?php echo $this->success; ?>';
      var message = '<?php echo $this->message; ?>';
      if (success == '2')
        he_show_message(message, '', 5000);
      if (success == '1')
        he_show_message(message, 'error', 5000);
    });
  </script>
<?php endif; ?>

<div class='inviter_layout_middle'>


  <div class='inviter-forms-conteiner global_form'>
    <div>
      <div>
        <?php if ($this->count > 0): ?>
          <div class='inviter-form-cont inviter-form-bg' id='inviter-importer-conteiner'>
            <div id='inviter-importer-title' class="inviter-tab-title inviter-import-title"
                 onclick="if ($(this).hasClass('inviter-form-title')){tab_slider('importer');}"
                 onmouseover="if ($(this).hasClass('inviter-form-title')){$('inviter-importer-conteiner').addClass('inviter-form-hover')}"
                 onmouseout="if ($(this).hasClass('inviter-form-title')){$('inviter-importer-conteiner').removeClass('inviter-form-hover')}">
              <h3 style="padding: 20px;"><?php echo $this->translate('INVITER_Import Your Contacts') ?></h3>
            </div>
            <div class='inviter-form' id='inviter-importer-form'> <?php echo $this->form->render($this) ?> </div>
          </div>
        <?php else: ?>
          <div class="tip">
            <span><?php echo $this->translate('INVITER_No providers'); ?></span>
          </div>
        <?php endif; ?>

        <?php if ($this->viewer->getIdentity()): ?>
          <div class='inviter-form-conteiner inviter-form-cont' id='inviter-uploader-conteiner' style="display:none;">
            <div id='inviter-uploader-title' class='inviter-tab-title inviter-upload-title inviter-form-title'
                 onclick="if ($(this).hasClass('inviter-form-title')){tab_slider('uploader');}"
                 onmouseover="if ($(this).hasClass('inviter-form-title')){$('inviter-uploader-conteiner').addClass('inviter-form-hover')}"
                 onmouseout="if ($(this).hasClass('inviter-form-title')){$('inviter-uploader-conteiner').removeClass('inviter-form-hover')}">
              <h3 style="padding: 20px;"><?php echo $this->translate('INVITER_Upload Your Contacts') ?></h3>
            </div>
            <div class='inviter-form'
                 id='inviter-uploader-form'> <?php echo $this->form_upload->render($this) ?> </div>
          </div>
          <div class='inviter-form-conteiner inviter-form-cont' id='inviter-writer-conteiner'>
            <div id='inviter-writer-title' class='inviter-tab-title inviter-write-title inviter-form-title'
                 onclick="if ($(this).hasClass('inviter-form-title')){tab_slider('writer');}"
                 onmouseover="if ($(this).hasClass('inviter-form-title')){$('inviter-writer-conteiner').addClass('inviter-form-hover')}"
                 onmouseout="if ($(this).hasClass('inviter-form-title')){$('inviter-writer-conteiner').removeClass('inviter-form-hover')}">
              <h3 style="padding: 20px;"><?php echo $this->translate('INVITER_Write Your Contacts') ?></h3>
            </div>
            <div class='inviter-form'
                 id='inviter-writer-form'> <?php echo $this->form_write->render($this) ?> </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>

<div id='default_provider_list' style='display:none;'></div>

<style>
#global_content h2 {
    color: #666;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 700;
    margin: 0;
    padding: 10px 20px;
    text-transform: capitalize;
}
.global_form > div {
  width:100%;
}
.global_form > div > div {
  width:100%;
}
select {
  height:auto;
  width:100%;
}
div.field-privacy-selector > span.caret {
  display:none;
}
ul.form-errors > li, ul.form-notices > li {
 float:none;
}

.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
.layout_main .layout_middle {
    background-color: #fff !important;
    padding: 1% 2%;
    width: 100%;
}
.login_my_wrapper .layout_inviter_referral_link{border-radius: 5px;}
 .layout_inviter_referral_link{clear: both;overflow: auto;width: 100%;}
</style>
