<script type='text/javascript'>
suggest.current_suggests = <?php echo Zend_Json::encode($this->current_suggests); ?>;
</script>

<div class='generic_layout_container layout_user_list_popular' style="display:none;">

<ul id='suggest_list'>
  <?php foreach ($this->suggests as $user): ?>

  <li class="suggest_friend_widget" >
    <table width="100%" id="suggest_<?php echo $user->getIdentity() ?>"><tr>
      <td width="48px" style="padding: 0px 3px 2px 0px;" valign="top">
        <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon')) ?>
      </td>
     
      <td valign="top">
        <b><?php echo $this->htmlLink($user->getHref(), $user->getTitle()) ?></b>
        <br/>
        
        <a href="javascript:suggest.show_mutual_friends(<?php echo $user->getIdentity() ?>)" style="font-size: 10px">
          <?php echo $this->translate(array('%s mutual friend', '%s mutual friends', count($this->current_suggests[$user->getIdentity()]['mutual_friends'])),count($this->current_suggests[$user->getIdentity()]['mutual_friends']));?>
        </a>
        <br/>
        
        <a class="smoothbox buttonlink smoothbox icon_friend_add" style="font-size: 12px" href="<?php echo $this->url(array('action' => 'add', 'controller' => 'friends', 'module' => 'members'), 'default')?>/user_id/<?php echo $user->getIdentity(); ?>">
          <?php echo $this->translate('INVITER_Add as friend');?>
        </a>
      </td>
      
      <td width="5px" valign="top">
        <div class="nonefriend nonfriend_delete" 
             onmouseover="$(this).addClass('nonfriend_delete_hover')"
             onmouseout="$(this).removeClass('nonfriend_delete_hover')"
             onclick="suggest.remove_suggest(<?php echo $user->getIdentity() ?>, true)"
        >&nbsp;</div>
      </td>
    </tr></table>
  </li>
  <?php endforeach; ?>
</ul>
</div>






<div class="col-md-12 col-sm-12 col-xs-12 index-right-job-posted">
<div class="col-md-7 col-sm-5 col-xs-6 index-right-text-lfet"><span style="font-size: 15px; color: rgb(76, 76, 76); font-weight: 600;">People you may know?</span></div>
<div class="col-md-5 col-sm-5 col-xs-6 classified_header text-right"><a class="fmp" href="members">Find more</a></div>
</div>


<div class="container col-md-12 col-sm-12 col-xs-12">
		  <?php foreach( $this->suggests as $user ): 
  			//$user_id = $arr_item['user_id'];
			//$number_mutual = $arr_item['number_mutual'];
  			//$user = Engine_Api::_() -> getItem('user', $user_id); 
          ?>
          
          <div class="col-md-2 col-sm-2 col-xs-3 nopadding">
			<div class="col-md-12 peopl_conect" style="padding:5px 0px;">
				
				  <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon', ''), array('class' => 'newestmembers_thumb')) ?></div>	
				  
				  <div class="col-md-12" style=" ">
					  <div class='newestmembers_info feed_item_photo' style="margin-right: 0px;">
						<div class='newestmembers_name feed_item_body text-center' >
						  <?php echo $this->htmlLink($user->getHref(), $this->string()->chunk($this->string()->truncate($user->getTitle(), 11), 10)) ?>
							
						</div>
				</div>
				  </div>
				  
                    <!-- add friend button -->
                <div class="col-md-12 text-center" style="padding:1px 0 8px;">
                            <a class="button_blue smoothbox" style=" display: inline-block;" href="<?php echo $this->baseUrl().'/members/friends/add/user_id/'.$user_id;?>">Connect</a>
                </div>
				  
            </div>
          
          <?php endforeach;?>
</div>
