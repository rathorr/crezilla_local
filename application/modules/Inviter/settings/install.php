<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Friends Inviter
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: install.php 2010-07-02 19:52 mirlan $
 * @author     Mirlan
 */

/**
 * @category   Application_Extensions
 * @package    Friends Inviter
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */
class Inviter_Installer extends Engine_Package_Installer_Module {

    public function onPreInstall() {
        parent::onPreInstall();

        $db = $this->getDb();
        $translate = Zend_Registry::get('Zend_Translate');

        $select = $db->select()
                ->from('engine4_core_modules')
                ->where('name = ?', 'hecore')
                ->where('enabled = ?', 1);

        $hecore = $db->fetchRow($select);

        if (!$hecore) {
            $error_message = $translate->_('Error! This plugin requires Hire-Experts Core module. It is free module and can be downloaded from Hire-Experts.com');
            return $this->_error($error_message);
        }

        if (version_compare($hecore['version'], '4.1.1') < 0) {
            $error_message = $translate->_('This plugin requires Hire-Experts Core Module. We found that you has old version of Core module, please download latest version of Hire-Experts Core Module and install. Note: Core module is free.');
            return $this->_error($error_message);
        }

        $operation = $this->_databaseOperationType;
        $module_name = $this->getOperation()->getTargetPackage()->getName();
        $package = $this->_operation->getPrimaryPackage();

        $licenseKey = strtoupper(substr(md5(md5($package->getName()) . md5($_SERVER['HTTP_HOST'])), 0, 16));

        $select = $db->select()
                ->from('engine4_hecore_modules')
                ->where('name = ?', $module_name);

        $module = $db->fetchRow($select);

        if ($operation == 'install') {

            if ($module && $module['installed']) {
                return;
            }

            $db = Engine_Db_Table::getDefaultAdapter();

            $db->query("CREATE TABLE IF NOT EXISTS `engine4_inviter_invites` ( 
			`invite_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, 
			`user_id` INT(11) UNSIGNED NULL DEFAULT NULL, 
			`sender` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', 
			`recipient` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', 
			`code` VARCHAR(255) NOT NULL COLLATE 'latin1_general_ci', 
			`sent_date` DATETIME NOT NULL, 
			`message` TEXT NOT NULL COLLATE 'utf8_unicode_ci', 
			`new_user_id` INT(11) UNSIGNED NOT NULL DEFAULT '0', 
			`provider` VARCHAR(20) NOT NULL COLLATE 'utf8_bin', 
			`recipient_name` VARCHAR(100) NOT NULL COLLATE 'utf8_bin', 
			`referred_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', 
			PRIMARY KEY (`invite_id`), 
			INDEX `user_id` (`user_id`), 
			INDEX `recipient` (`recipient`) 
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;");

            $db->query("CREATE TABLE IF NOT EXISTS `engine4_inviter_statistics` ( 
			`type` VARCHAR(20) NOT NULL DEFAULT '0', 
			`date` DATETIME NOT NULL, 
			`value` BIGINT(20) NOT NULL DEFAULT '0', 
			PRIMARY KEY (`type`, `date`) 
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;");

            $db->query("CREATE TABLE IF NOT EXISTS `engine4_inviter_nonefriends` ( 
			`user_id` INT(11) NOT NULL, 
			`nonefriend_ids` TEXT NULL, 
			UNIQUE INDEX `user_id` (`user_id`) 
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;");

            $db->query("CREATE TABLE IF NOT EXISTS `engine4_inviter_providers` ( 
		  `provider_id` int(11) NOT NULL AUTO_INCREMENT, 
		  `provider_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, 
		  `provider_logo` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, 
		  `provider_default` tinyint(4) NOT NULL DEFAULT '0', 
		  `provider_enabled` tinyint(1) NOT NULL DEFAULT '1', 
		  `isopeninviter` tinyint(1) NOT NULL DEFAULT '1', 
		  PRIMARY KEY (`provider_id`), 
		  UNIQUE KEY `id` (`provider_id`), 
		  UNIQUE KEY `title` (`provider_title`), 
		  UNIQUE KEY `logo` (`provider_logo`), 
		  UNIQUE KEY `provider_title` (`provider_title`), 
		  UNIQUE KEY `provider_logo` (`provider_logo`) 
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=104 ;");

            $db->query("INSERT IGNORE INTO `engine4_inviter_providers` (`provider_id`, `provider_title`, `provider_logo`, `provider_default`, `provider_enabled`, `isopeninviter`) VALUES 
		(1, 'Abv', 'abv_logo.png', 0, 0, 1), 
		(2, 'AOL', 'aol_logo.png', 0, 0, 1), 
		(3, 'Apropo', 'apropo_logo.png', 0, 0, 1), 
		(4, 'Atlas', 'atlas_logo.png', 0, 0, 1), 
		(5, 'Aussiemail', 'aussiemail_logo.png', 0, 0, 1), 
		(6, 'Azet', 'azet_logo.png', 0, 0, 1), 
		(7, 'Badoo', 'badoo_logo.png', 0, 0, 1), 
		(8, 'Bebo', 'bebo_logo.png', 0, 0, 1), 
		(9, 'Bigstring', 'bigstring_logo.png', 0, 0, 1), 
		(10, 'Bookcrossing', 'bookcrossing_logo.png', 0, 0, 1), 
		(11, 'Bordermail', 'bordermail_logo.png', 0, 0, 1), 
		(12, 'Brazencareerist', 'brazencareerist_logo.png', 0, 0, 1), 
		(13, 'Canoe', 'canoe_logo.png', 0, 0, 1), 
		(14, 'Care2', 'care2_logo.png', 0, 0, 1), 
		(15, 'Clevergo', 'clevergo_logo.png', 0, 0, 1), 
		(16, 'Cyworld', 'cyworld_logo.png', 0, 0, 1), 
		(17, 'Doramail', 'doramail_logo.png', 0, 0, 1), 
		(18, 'Eons', 'eons_logo.png', 0, 0, 1), 
		(19, 'Evite', 'evite_logo.png', 0, 0, 1), 
		(20, 'Facebook', 'facebook_logo.png', 1, 1, 0), 
		(21, 'Faces', 'faces_logo.png', 0, 0, 1), 
		(22, 'Famiva', 'famiva_logo.png', 0, 0, 1), 
		(23, 'FastMail', 'fastmail_logo.png', 0, 0, 1), 
		(24, 'Fdcareer', 'fdcareer_logo.png', 0, 0, 1), 
		(25, 'Flickr', 'flickr_logo.png', 0, 0, 1), 
		(26, 'Flingr', 'flingr_logo.png', 0, 0, 1), 
		(27, 'Flixster', 'flixster_logo.png', 0, 0, 1), 
		(28, 'Fm5', 'fm5_logo.png', 0, 0, 1), 
		(29, 'Freemail', 'freemail_logo.png', 0, 0, 1), 
		(30, 'Friendfeed', 'friendfeed_logo.png', 0, 0, 1), 
		(31, 'Friendster', 'friendster_logo.png', 0, 0, 1), 
		(32, 'Gawab', 'gawab_logo.png', 0, 0, 1), 
		(33, 'GMail', 'gmail_logo.png', 1, 1, 0), 
		(34, 'GMX.net', 'gmx_net_logo.png', 0, 0, 1), 
		(35, 'Grafitti', 'graffiti_logo.png', 0, 0, 1), 
		(36, 'Hi5', 'hi5_logo.png', 0, 0, 1), 
		(37, 'Live/Hotmail', 'hotmail_logo.png', 1, 1, 0), 
		(38, 'Hushmail', 'hushmail_logo.png', 0, 0, 1), 
		(39, 'Hyves', 'hyves_logo.png', 0, 0, 1), 
		(40, 'Inbox.com', 'inbox_logo.png', 0, 0, 1), 
		(41, 'IndiaTimes', 'indiatimes_logo.png', 0, 0, 1), 
		(42, 'India', 'india_logo.png', 0, 0, 1), 
		(43, 'Inet', 'inet_logo.png', 0, 0, 1), 
		(44, 'Interia', 'interia_logo.png', 0, 0, 1), 
		(45, 'KataMail', 'katamail_logo.png', 0, 0, 1), 
		(46, 'Kids', 'kids_logo.png', 0, 0, 1), 
		(47, 'Kincafe', 'kincafe_logo.png', 0, 0, 1), 
		(48, 'Konnects', 'konnects_logo.png', 0, 0, 1), 
		(49, 'Koolro', 'koolro_logo.png', 0, 0, 1), 
		(50, 'Last.fm', 'lastfm_logo.png', 1, 1, 0), 
		(51, 'Libero', 'libero_logo.png', 0, 0, 1), 
		(52, 'LinkedIn', 'linkedin_logo.png', 1, 1, 0), 
		(53, 'Livejournal', 'livejournal_logo.png', 0, 0, 1), 
		(54, 'Lovento', 'lovento_logo.png', 0, 0, 1), 
		(55, 'Lycos', 'lycos_logo.png', 0, 0, 1), 
		(56, 'Mail2World', 'mail2world_logo.png', 0, 0, 1), 
		(57, 'Mail.com', 'mail_com_logo.png', 0, 0, 1), 
		(58, 'Mail.in', 'mail_in_logo.png', 0, 0, 1), 
		(59, 'Mail.ru', 'mail_ru_logo.png', 1, 1, 0), 
		(60, 'Meinvz', 'meinvz_logo.png', 0, 0, 1), 
		(61, 'Meta', 'meta_logo.png', 0, 0, 1), 
		(62, 'Mevio', 'mevio_logo.png', 0, 0, 1), 
		(63, 'Motortopia', 'motortopia_logo.png', 0, 0, 1), 
		(64, 'MSN', 'msn_logo.png', 1, 1, 0), 
		(65, 'Multiply', 'multiply_logo.png', 0, 0, 1), 
		(66, 'Mycatspace', 'mycatspace_logo.png', 0, 0, 1), 
		(67, 'Mydogspace', 'mydogspace_logo.png', 0, 0, 1), 
		(68, 'Mynet.com', 'mynet_logo.png', 0, 0, 1), 
		(69, 'MySpace', 'myspace_logo.png', 0, 0, 1), 
		(70, 'Netaddress', 'netaddress_logo.png', 0, 0, 1), 
		(71, 'NetLog', 'netlog_logo.png', 0, 0, 1), 
		(72, 'Ning', 'ning_logo.png', 0, 0, 1), 
		(73, 'Nz11', 'nz11_logo.png', 0, 0, 1), 
		(74, 'O2', 'o2_logo.png', 0, 0, 1), 
		(75, 'OperaMail', 'operamail_logo.png', 0, 0, 1), 
		(76, 'Orkut', 'orkut_logo.png', 1, 1, 0), 
		(77, 'Perfspot', 'perfspot_logo.png', 0, 0, 1), 
		(78, 'Plaxo', 'plaxo_logo.png', 0, 0, 1), 
		(79, 'Plazes', 'plazes_logo.png', 0, 0, 1), 
		(80, 'Plurk', 'plurk_logo.png', 0, 0, 1), 
		(81, 'Pochta', 'pochta_logo.png', 0, 0, 1), 
		(82, 'Popstarmail', 'popstarmail_logo.png', 0, 0, 1), 
		(83, 'Rambler', 'rambler_logo.png', 0, 0, 1), 
		(84, 'Rediff', 'rediff_logo.png', 0, 0, 1), 
		(85, 'Sapo.pt', 'sapo_logo.png', 0, 0, 1), 
		(86, 'Skyrock', 'skyrock_logo.png', 0, 0, 1), 
		(87, 'Tagged', 'tagged_logo.png', 0, 0, 1), 
		(88, 'Techemail', 'techemail_logo.png', 0, 0, 1), 
		(89, 'Terra', 'terra_logo.png', 0, 0, 1), 
		(90, 'Twitter', 'twitter_logo.png', 1, 1, 0), 
		(91, 'Uk2', 'uk2_logo.png', 0, 0, 1), 
		(92, 'Vimeo', 'vimeo_logo.png', 0, 0, 1), 
		(93, 'Virgilio', 'virgilio_logo.png', 0, 0, 1), 
		(94, 'Vkontakte', 'vkontakte_logo.png', 0, 0, 1), 
		(95, 'Walla', 'walla_logo.png', 0, 0, 1), 
		(96, 'Web.de', 'web_de_logo.png', 0, 0, 1), 
		(97, 'Wp.pt', 'wpl_logo.png', 0, 0, 1), 
		(98, 'Xanga', 'xanga_logo.png', 0, 0, 1), 
		(99, 'Xing', 'xing_logo.png', 0, 0, 1), 
		(100, 'Xuqa', 'xuqa_logo.png', 0, 0, 1), 
		(101, 'Yahoo!', 'yahoo_logo.png', 1, 1, 0), 
		(102, 'Yandex', 'yandex_logo.png', 0, 0, 1), 
		(103, 'Zapakmail', 'zapak_logo.png', 0, 0, 1), 
		(104, 'Foursquare', 'foursquare_logo.png', 1, 1, 0);");

            $db->query("INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES 
		('core_main_inviter', 'inviter', 'Inviter', 'Inviter_Plugin_Core', '{\"route\":\"inviter_general\",\"module\":\"inviter\",\"controller\":\"index\",\"action\":\"index\"}', 'core_main', '', 2),
		('core_sitemap_inviter', 'inviter', 'Friends Inviter', 'Inviter_Plugin_Core', '{\"route\":\"inviter_general\",\"module\":\"inviter\",\"controller\":\"index\",\"action\":\"index\"}', 'core_sitemap', '', 2),
		('inviter_admin_main_stats', 'inviter', 'View Statistics', '', '{\"route\":\"admin_default\",\"module\":\"inviter\",\"controller\":\"stats\",\"action\":\"index\"}', 'inviter_admin_main', '', 1),
		('inviter_admin_main_charts', 'inviter', 'View Charts', '', '{\"route\":\"admin_default\",\"module\":\"inviter\",\"controller\":\"stats\",\"action\":\"chart\"}', 'inviter_admin_main', '', 2),
		('inviter_admin_main_level', 'inviter', 'Level Settings', '', '{\"route\":\"admin_default\",\"module\":\"inviter\",\"controller\":\"settings\",\"action\":\"level\"}', 'inviter_admin_main', '', 3),
		('inviter_profile_invite', 'inviter', 'Invite Friends', '', '{\"route\":\"inviter_general\",\"module\":\"inviter\",\"controller\":\"index\",\"action\":\"index\"}', 'inviter_profile', '', 1),
		('inviter_profile_invitation', 'inviter', 'Invitations', '', '{\"route\":\"inviter_invitations\",\"module\":\"inviter\",\"controller\":\"invitiations\", \"action\":\"index\"}', 'inviter_profile', '', 2),
		('inviter_profile_referral', 'inviter', 'My Referrals', '', '{\"route\":\"inviter_referrals\",\"module\":\"inviter\",\"controller\":\"referrals\", \"action\":\"index\"}', 'inviter_profile', '', 3),
		('core_admin_main_plugins_inviter', 'inviter', 'HE - Friends Inviter', '', '{\"route\":\"admin_default\",\"module\":\"inviter\",\"controller\":\"stats\"}', 'core_admin_main_plugins', '', 888),
		('inviter_facebook', 'inviter', 'Facebook', 'Inviter_Plugin_Core', '{\"route\":\"inviter_facebook\",\"module\":\"inviter\",\"controller\":\"facebook\", \"action\":\"index\"}', 'inviter_profile', '', 4),
		('user_profile_introduce', 'inviter', 'INVITER_Profile Introduction', 'Inviter_Plugin_Core', '{\"route\":\"default\",\"module\":\"inviter\",\"controller\":\"introduce\", \"action\": \"edit\"}', 'user_edit', '', 1),
		('inviter_admin_main_prov_settings', 'inviter', 'INVITER_Providers Settings', '', '{\"route\":\"admin_default\",\"module\":\"inviter\",\"controller\":\"settings\",\"action\":\"providers-settings\"}', 'inviter_admin_main', '', 5), 
		('inviter_admin_main_providers', 'inviter', 'INVITER_Manage Providers', '', '{\"route\":\"admin_default\",\"module\":\"inviter\",\"controller\":\"settings\",\"action\":\"providers\"}', 'inviter_admin_main', '', 6);");

            $db->query("INSERT IGNORE INTO `engine4_core_mailtemplates` (`type`,`module`, `vars`) VALUES 
		('inviter','inviter', '[from],[to],[message],[link],[code],[from_email]');");

            $db->query("INSERT IGNORE INTO `engine4_inviter_statistics`(`type`, `date`, `value`) VALUES 
		('inviter.referreds', '29.12.1899', '0'), 
		('inviter.sents', '29.12.1899', '0');");

            $db->query("DELETE FROM `engine4_user_signup` WHERE `class` = 'Inviter_Plugin_Signup_Invite';");

            $db->query("INSERT IGNORE INTO `engine4_user_signup` (`class`, `order`, `enable`) VALUES 
		('Inviter_Plugin_Signup_Invite', 4, 1);");

            $db->query("INSERT IGNORE INTO `engine4_authorization_permissions` (`level_id`, `type`, `name`, `value`, `params`) VALUES  
		(4, 'inviter', 'use', 1, NULL), 
		(3, 'inviter', 'use', 1, NULL), 
		(1, 'inviter', 'use', 1, NULL), 
		(2, 'inviter', 'use', 1, NULL), 
		(5, 'inviter', 'use', 0, NULL);");

            $db->query("CREATE TABLE IF NOT EXISTS `engine4_inviter_codes` ( 
		  `code_id` int(11) NOT NULL AUTO_INCREMENT, 
		  `user_id` int(11) NOT NULL, 
		  `code` text NOT NULL, 
		  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		  PRIMARY KEY (`code_id`) 
		)  
		COLLATE='utf8_unicode_ci' 
		ENGINE=InnoDB;");

            $db->query("CREATE TABLE IF NOT EXISTS `engine4_inviter_introductions` ( 
		  `introduction_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, 
		  `user_id` INT(10) UNSIGNED NOT NULL, 
		  `body` TEXT NULL COLLATE 'utf8_unicode_ci', 
		  `creation_date` DATETIME NULL DEFAULT NULL, 
		  `displayed_date` DATETIME NULL DEFAULT NULL, 
		  `more_friends_date` DATETIME NULL DEFAULT NULL, 
		  `publish` TINYINT(1) UNSIGNED NULL DEFAULT '0', 
		  `exclude_ids` TEXT NULL COLLATE 'utf8_unicode_ci', 
		  UNIQUE INDEX `introduction_id` (`introduction_id`), 
		  UNIQUE INDEX `user_id` (`user_id`) 
		) 
		COLLATE='utf8_unicode_ci' 
		ENGINE=InnoDB;");

            $db->query("CREATE TABLE IF NOT EXISTS `engine4_inviter_tokens` ( 
		  `token_id` int(10) NOT NULL AUTO_INCREMENT, 
		  `user_id` int(11) NOT NULL DEFAULT '0', 
		  `object_id` varchar(255) NOT NULL DEFAULT '', 
		  `object_name` varchar(255) DEFAULT NULL, 
		  `provider` varchar(255) DEFAULT NULL, 
		  `oauth_token` TEXT NULL, 
		  `oauth_token_secret` TEXT NULL, 
		  `creation_date` datetime DEFAULT NULL, 
		  `expiration_date` datetime DEFAULT NULL, 
		  `active` tinyint(1) NOT NULL DEFAULT '1', 
		  PRIMARY KEY (`token_id`,`object_id`) 
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

            $sql = <<<CONTENT
INSERT IGNORE INTO  `engine4_core_pages` (`name`,     `displayname`,     `title`,     `description`, `provides`, `view_count`) VALUES 
('inviter_index_index', 'Friends Inviter', 'Friends Inviter', 'Frends Inviter main page', 'no-subject', 0); 
CONTENT;

            $db->query($sql);
            $page_id = $db->lastInsertId();

            $sql = <<<CONTENT
INSERT IGNORE INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`) VALUES 
($page_id, 'container', 'main', NULL, 1); 
CONTENT;

            $db->query($sql);
            $main_content_id = $db->lastInsertId();

            $sql = <<<CONTENT
INSERT IGNORE INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`) VALUES 
($page_id, 'container', 'middle', $main_content_id, 6); 
CONTENT;

            $db->query($sql);
            $middle_content_id = $db->lastInsertId();

            $sql = <<<CONTENT
INSERT IGNORE INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`) VALUES 
($page_id, 'widget', 'inviter.home-inviter', $middle_content_id, 3); 
CONTENT;

            $db->query($sql);

            $sql = <<<CONTENT
INSERT IGNORE INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`) VALUES 
($page_id, 'container', 'right', $main_content_id, 2); 
CONTENT;

            $db->query($sql);
            $right_content_id = $db->lastInsertId();

            $sql = <<<CONTENT
INSERT IGNORE INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`) VALUES 
($page_id, 'widget', 'inviter.referral-link', $right_content_id, 4); 
CONTENT;

            $db->query($sql);

            $sql = <<<CONTENT
INSERT IGNORE INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`) VALUES 
($page_id, 'widget', 'inviter.list-suggest', $right_content_id, 5); 
CONTENT;

            $db->query($sql);

            $sql = <<<CONTENT
SELECT TRUE FROM `engine4_core_modules` WHERE `name` = 'page' LIMIT 1 
CONTENT;

            if (null != $db->fetchRow($sql)) {
                $db->query("INSERT IGNORE INTO `engine4_page_modules` (`name`, `widget`, `order`, `params`, `informed`) VALUES 
		('inviter', 'inviter.page-inviter', '0','{\"title\":\"Inviter\", \"titleCount\":true}' ,'1');");
            }

            $db->query("INSERT IGNORE INTO `engine4_core_mailtemplates` (`type`,`module`, `vars`) VALUES 
		('page_inviter','inviter', '[from],[to],[message],[link],[page_title],[from_email]');");

            $db->query("INSERT INTO `engine4_hecore_modules` (`name`, `version`, `key`, `installed`, `modified_stamp`) VALUES ('" . $package->getName() . "', '" . $package->getVersion() . "', '" . $licenseKey . "', 1, " . time() . ")");
        } else { //$operation = upgrade|refresh
            $db = Engine_Db_Table::getDefaultAdapter();

            $db->query("UPDATE `engine4_hecore_modules` SET `version` = '" . $package->getVersion() . "', modified_stamp = " . time() . " WHERE `name` = '" . $package->getName() . "';");
        }
    }

}