<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: content.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
return array(
  array(
    'title' => 'Profile Education',
    'description' => 'Displays a member\'s education on their profile.',
    'category' => 'Education',
    'type' => 'widget',
    'name' => 'education.profile-education',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Education',
      'titleCount' => true,
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
 
) ?>