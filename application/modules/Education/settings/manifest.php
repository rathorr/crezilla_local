<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'education',
    'version' => '4.0.0',
    'path' => 'application/modules/Education',
    'title' => 'education',
    'description' => '',
    'author' => '',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Education',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/education.csv',
    ),
  ),
  // Items ---------------------------------------------------------------------
  'items' => array(
    'category',
	'education'
  ),
  // Routes --------------------------------------------------------------------
  'routes' => array(
    'education_specific' => array(
      'route' => 'education/:action/:education_id/*',
      'defaults' => array(
        'module' => 'education',
        'controller' => 'index',
        'action' => 'edit'
      ),
      'reqs' => array(
        'action' => '(delete|edit)',
      ),
    ),
	'education_general' => array(
      'route' => 'education/:action',
      'defaults' => array(
        'module' => 'education',
        'controller' => 'index',
        'action' => 'index'
      ),
      'reqs' => array(
        'action' => '(index|add)',
      ),
    ),
  ),
); ?>