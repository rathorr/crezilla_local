<?php 
 $this->headScript()
    ->appendFile($this->layout()->staticBaseUrl . 'externals/jquery/jquery-ui.min.js');
 $this->headLink()
    ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/jquery/jquery-ui.css');
   $this->headLink();
   ?>
<div class="headline">
  <h2>
    <?php echo $this->translate('Edit My Profile');?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>
</div>
<div style="padding:0px 12px;">
<h2 class="heading">Add Education</h2></div>


<script type="application/javascript">
jQuery(document).ready(function(){
	jQuery('.user_edit_education').parent().addClass('active');
	jQuery('.custom_314').parent().addClass('active');
	jQuery('.custom_922').parent().addClass('active');
	
jQuery('#start_date').datepicker({ dateFormat: 'dd/mm/yy',changeMonth: true,
        changeYear: true, yearRange: '1970:2016' });
	jQuery('#end_date').datepicker({ dateFormat: 'dd/mm/yy',changeMonth: true,
        changeYear: true , yearRange: '1970:2016'});
});


jQuery(document).on('click', '#cancel_button',function(){
	window.location.href='<?php echo $this->baseUrl();?>/education';
});

/*jQuery(document).on('change', '#start_date, #end_date', function(){
	if((jQuery('#start_date').val() != '0') && (jQuery('#end_date').val() != '0')){
		if(jQuery('#start_date').val() > jQuery('#end_date').val()){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Start date can not be greater than end date.');
			jQuery('#start_date').val('0');
			jQuery('#end_date').val('0');
			return false;	
		}	
	}
});*/
function ShowHideOther(){
	if(jQuery('#category_id').val() == 'other'){
		jQuery('#other_category').show();	
	}else{
		jQuery('#other_category').hide();
	}
}

jQuery(document).on('submit', '#add_edu_form', function(){
		if(jQuery.trim(jQuery('#institute_name').val()) == ''){
			jQuery('#institute_name').focus();
			jQuery('#institute_name').addClass("com_form_error_validatin");
			//alert('Please enter institute name.');
			return false;	
		}
		if(jQuery.trim(jQuery('#start_date').val()) == '0'){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Please select start date.');
			return false;	
		}
		if(jQuery.trim(jQuery('#end_date').val()) == '0'){
			jQuery('#end_date').focus();
			jQuery('#end_date').addClass("com_form_error_validatin");
			//alert('Please select end date.');
			return false;	
		}
		if(parseInt(fn_DateCompare(jQuery.trim(jQuery('#start_date').val()), jQuery.trim(jQuery('#end_date').val()))) == 1){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Start date can not be greater than end date.');
			jQuery('#start_date').val('');
			jQuery('#end_date').val('');
			return false;
		}
		/*if(jQuery('#start_date').val() > jQuery('#end_date').val()){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Start date can not be greater than end date.');
			jQuery('#start_date').val('0');
			jQuery('#end_date').val('0');
			return false;	
		}*/
		if(jQuery.trim(jQuery('#category_id').val()) == 0){
			jQuery('#category_id').focus();
			jQuery('#category_id').addClass("com_form_error_validatin");
			//alert('Please select category.');
			return false;
		}
		if(jQuery.trim(jQuery('#category_id').val()) == 'other' && jQuery.trim(jQuery('#other_category').val()) == ''){
			jQuery('#other_category').focus();
			jQuery('#other_category').addClass("com_form_error_validatin");
			//alert('Please enter category.');
			return false;
		}
		if(jQuery.trim(jQuery('#field_of_study').val()) == 0){
			jQuery('#field_of_study').focus();
			jQuery('#field_of_study').addClass("com_form_error_validatin");
			//alert('Please enter field of study.');
			return false;
		}
		window.location.href="<?php echo $this->baseUrl().'/education';?>";
});

function fn_DateCompare(DateA, DateB) {
  var a = new Date(DateA.replace(/-/g, '/'));
  var b = new Date(DateB.replace(/-/g, '/'));

  var msDateA = Date.UTC(a.getFullYear(), a.getMonth()+1, a.getDate());
  var msDateB = Date.UTC(b.getFullYear(), b.getMonth()+1, b.getDate());

  if (parseFloat(msDateA) < parseFloat(msDateB))
	return -1;  // less than
  else if (parseFloat(msDateA) == parseFloat(msDateB))
	return 0;  // equal
else if (parseFloat(msDateA) > parseFloat(msDateB))
	return 1;  // greater than
else
	return null;
}  // error

</script>

<?php echo $this->form->render($this) ?>

