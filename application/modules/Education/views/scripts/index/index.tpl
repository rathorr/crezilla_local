<div class="headline">
  <h2>
    <?php echo $this->translate('Edit My Profile');?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>
</div>
<div class="education-section-top" style="padding:0 12px">
<h2 class="heading" style="border-bottom: 2px solid #398bcc; font-size:14px;"><a href="<?php echo $this->baseUrl().'/education';?>">Education</a></h2> <h2 class="heading" style="border-bottom:none; background-color:#fff;font-size:14px;"><a href="<?php echo $this->baseUrl().'/experience';?>">Experience</a></h2>
<a  class="add_more icon_classified_new" href="<?php echo $this->baseUrl().'/education/add';?>">Add More Education</a></div>
<?php 
$educations	=	$this->educations;
if(isset($educations[0])){$i=1;
	foreach($educations as $edu){?>
    <div class="education_section" id="education_<?php echo $edu->education_id; ?>">
          <div class="album_options">
            <?php echo $this->htmlLink(array('route' => 'education_specific', 'action' => 'edit', 'education_id' => $edu->education_id), $this->translate('Edit Settings'), array(
              'class' => 'buttonlink icon_photos_settings'
            )) ?>
            <?php echo $this->htmlLink(array('route' => 'education_specific', 'action' => 'delete', 'education_id' => $edu->education_id, 'format' => 'smoothbox'), $this->translate('Delete education'), array(
              'class' => 'buttonlink smoothbox icon_photos_delete'
            )) ?>
          </div>
    	<div class="edu_listing">
        <span class="sr-list"><?php echo $i;?></span>
    	<div class="title">
		<span class="exp-title"><?php echo $edu->institute_name;?></span>
		</div>
		<div class="activities">
        <label>Degree:&nbsp;</label><span><?php echo ($edu->category_id == 'other') ? $edu->other_category : $edu->category_name;?>,<?php echo $edu->field_of_study;?></span>
		</div>
		
		<div class="activities">
        <label>Time Period:&nbsp;</label><span><?php echo date('m/d/Y', strtotime($edu->start_date)).'&nbsp; - &nbsp;'.date('m/d/Y', strtotime($edu->end_date));?></span></div>
        <div class="activities">
        <label>Activities: &nbsp;</label><span><?php echo $edu->activities ? $edu->activities : 'No activity';?></span>
        </div>
        
        <div class="activities">
         <label>Description: &nbsp;</label><span><?php echo $edu->description ? $edu->description : 'No description';?></span>
        </div>
    </div>
    </div>
         
<?php $i++;}}else {?>

<div class="tip">
      <span id="no-education">You do not have any education listing yet. </span>
  </div>

<?php }?>

<script type="text/javascript">
jQuery('.user_edit_education').parent().addClass('active');
jQuery('.custom_922').parent().addClass('active');
</script>
