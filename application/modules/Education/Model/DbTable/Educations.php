<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Education_Model_DbTable_Educations extends Engine_Db_Table
{
  protected $_rowClass = 'Education_Model_Education';

  public function getEducationSelect($options = array())
  {
    $select = $this->select();
    if( !empty($options['owner']) && 
        $options['owner'] instanceof Core_Model_Item_Abstract ) {
      $select
	  	->setIntegrityCheck ( false )
        ->from ( array('t' => 'engine4_education_educations'))      
        ->joinLeft ( array ('p' => 'engine4_education_categories'), 't.category_id = p.category_id', array('category_name'))  
        ->where('t.owner_type = ?', $options['owner']->getType())
        ->where('t.owner_id = ?', $options['owner']->getIdentity())
        ->order('t.education_id DESC')
        ;
    }

    if( !empty($options['search']) && is_numeric($options['search']) ) {
      $select->where('t.search = ?', $options['search']);
    }

    return $select;
  }

  public function getEducationPaginator($options = array())
  {
    return Zend_Paginator::factory($this->getEducationSelect($options));
  }

  public function geteducationlist($userid)
  {
    $select = $this->select()
        ->where("owner_id = ?",$userid)
        ->where("row_status = ?", 1)
        ->order("creation_date desc");
  return $result  = $this->fetchAll($select);
  }

}