<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Album.php 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Education_Form_Education extends Engine_Form
{
  public function init()
  {
    $user_level = Engine_Api::_()->user()->getViewer()->level_id;
    $user = Engine_Api::_()->user()->getViewer();

    // Init form
    $this
      ->setTitle('Education')
      ->setAttrib('id', 'add_edu_form')
      ->setAttrib('name', 'education_form')
      ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()))
      ;
    
    // Init album
    $degreeTable = Engine_Api::_()->getItemTable('category');
    $degrees = $degreeTable->select()
        ->from($degreeTable, array('category_name', 'category_id'))
        ->where('row_status = ?', '1')
        ->query()
        ->fetchAll();
   // print_r($degrees);
    $degreesOptions = array('0' => '-');
    foreach( $degrees as $degree ) {
      $degreesOptions[$degree['category_id']] = $degree['category_name'];
    }
	
	$degreesOptions['other'] = 'Other';

    // Init name
    $this->addElement('Text', 'institute_name', array(
      'label' => 'Institute',
	  'placeholder'	=>	'Enter Institute Name',
      'maxlength' => '255',
	  'required'   => true,
      'filters' => array(
        //new Engine_Filter_HtmlSpecialChars(),
        'StripTags',
        new Engine_Filter_Censor(),
        new Engine_Filter_StringLength(array('max' => '255')),
      )
    ));
	
	$dateOptions = array('0' => '-');
	for($j=1;$j<=50;$j++){
		$yr	=	1960+$j;
		$dateOptions[$yr] = $yr;
	}
	
	
	// prepare start dadte
    //$this->addElement('Select', 'start_date', array(
//      'label' => 'Start From',
//      'multiOptions' => $dateOptions,
//    ));

	$this->addElement('Text', 'start_date', array(
      'label' => 'Start From',
	  'placeholder'	=>	'Select Start Date',
	  'class' => 'datepicker',
      'required'   => true,
    ));
	
	// prepare end_date
    /*$this->addElement('Select', 'end_date', array(
      'label' => 'End',
      'multiOptions' => $dateOptions,
    ));*/
	
	$this->addElement('Text', 'end_date', array(
      'label' => 'End Date',
	  'placeholder'	=>	'Select End Date',
	  'class' => 'datepicker',
      'required'   => true,
    ));

    // prepare degree
    $this->addElement('Select', 'category_id', array(
      'label' => 'Category',
      'multiOptions' => $degreesOptions,
      'onchange' => "ShowHideOther()",
    ));
	
	
	// if others selected as degree
	$this->addElement('Text', 'other_category', array(
      'label' => '',
      'maxlength' => '255',
      'id' => 'other_category',
	   'placeholder'	=>	'Enter Category',
	  'style'    => array('display:none;'),
      'filters' => array(
        //new Engine_Filter_HtmlSpecialChars(),
        'StripTags',
        new Engine_Filter_Censor(),
        new Engine_Filter_StringLength(array('max' => '255')),
      )
    ));
	
	 // Init field of study
    $this->addElement('Text', 'field_of_study', array(
      'label' => 'Field Of Study',
	  'placeholder'	=>	'Enter Field of Study',
      'maxlength' => '255',
	  'required'   => true,
      'filters' => array(
        //new Engine_Filter_HtmlSpecialChars(),
        'StripTags',
        new Engine_Filter_Censor(),
        new Engine_Filter_StringLength(array('max' => '255')),
      )
    ));
	
	 // Init Activities
    $this->addElement('Textarea', 'activities', array(
      'label' => 'Activities and Societies',
	  'placeholder'	=>	'Enter Activities and Societies',
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
        //new Engine_Filter_HtmlSpecialChars(),
        new Engine_Filter_EnableLinks(),
      ),
    ));

    // Init descriptions
    $this->addElement('Textarea', 'description', array(
      'label' => 'Description',
	  'placeholder'	=>	'Enter Description',
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
        //new Engine_Filter_HtmlSpecialChars(),
        new Engine_Filter_EnableLinks(),
      ),
    ));
    

    /*// Init submit
    $this->addElement('Button', 'submit', array(
      'label' => 'Save',
      'type' => 'submit',
    ));
	
	// Init Cancel
	$this->addElement('Button', 'cancel', array(
      'label' => 'Cancel',
	  'id'		=>	'cancel_button',
      'type' => 'button',
    ));*/
	
	// Submit or succumb!
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Education',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    
    $this->addElement('Button', 'cancel', array(
      'label' => 'cancel',
	  'id'		=>	'cancel_button',
      'link' => true,
      'prependText' => ' or ',
      'decorators' => array(
        'ViewHelper'
      )
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
	
  }

  public function saveEduValues($values, $user_id){
		$values['owner_id']		=	$user_id;
		$values['owner_type']	=	'user';
		$values['start_date']	=	date('Y-m-d', strtotime($values['start_date']));
		$values['end_date']		=	date('Y-m-d', strtotime($values['end_date']));
		$values['created_on']	=	date('Y-m-d', time());
		$values['row_status']	=	'1';
		//echo '<pre>'; print_r($values); die;
		
		$education = Engine_Api::_()->getDbtable('educations', 'education')->createRow();
		$education->setFromArray($values);
		$education->save();
	}
	
}
