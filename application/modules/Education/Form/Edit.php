<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Edit.php 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Education_Form_Edit extends Engine_Form
{
  
  public function init()
  {
    $user = Engine_Api::_()->user()->getViewer();

    $user_level = Engine_Api::_()->user()->getViewer()->level_id;
    $this->setTitle('Edit Education Deatils')
		->setAttrib('id', 'edit_edu_form')
      ->setAttrib('name', 'education_edit');
    
	$degreeTable = Engine_Api::_()->getItemTable('category');
    $degrees = $degreeTable->select()
        ->from($degreeTable, array('category_name', 'category_id'))
        ->where('row_status = ?', '1')
        ->query()
        ->fetchAll();
   // print_r($degrees);
    $degreesOptions = array('0' => '-');
    foreach( $degrees as $degree ) {
      $degreesOptions[$degree['category_id']] = $degree['category_name'];
    }
	
	$degreesOptions['other'] = 'Other';
	
	
    $this->addElement('Text', 'institute_name', array(
      'label' => 'Institute',
	  'placeholder'	=>	'Enter Institute Name',
      'required' => true,
      'notEmpty' => true,
      'validators' => array(
        'NotEmpty',
      ),
      'filters' => array(
        new Engine_Filter_Censor(),
        //new Engine_Filter_HtmlSpecialChars(),
        'StripTags',
        new Engine_Filter_StringLength(array('max' => '255'))
      )
    ));
    $this->institute_name->getValidator('NotEmpty')->setMessage("Please enter institute name.");
	
	$dateOptions = array('0' => '-');
	for($j=1;$j<=50;$j++){
		$yr	=	1960+$j;
		$dateOptions[$yr] = $yr;
	}
	
	
	// prepare start dadte
    /*$this->addElement('Select', 'start_date', array(
      'label' => 'Start From',
      'multiOptions' => $dateOptions,
    ));*/
	
	$this->addElement('Text', 'start_date', array(
      'label' => 'Start From',
	  'placeholder'	=>	'Select Start Date',
	  'class' => 'datepicker',
      'required'   => true,
    ));
	
	// prepare end_date
    /*$this->addElement('Select', 'end_date', array(
      'label' => 'End',
      'multiOptions' => $dateOptions,
    ));*/
	
	$this->addElement('Text', 'end_date', array(
      'label' => 'End Date',
	  'placeholder'	=>	'Select End Date',
	  'class' => 'datepicker',
      'required'   => true,
    ));

    // prepare degree
    $this->addElement('Select', 'category_id', array(
      'label' => 'Category',
      'multiOptions' => $degreesOptions,
      'onchange' => "ShowHideOther()",
    ));
	
	
	// if others selected as degree
	$this->addElement('Text', 'other_category', array(
      'label' => '',
      'maxlength' => '255',
	  'placeholder'	=>	'Enter Category',
      'id' => 'other_category',
      'filters' => array(
        //new Engine_Filter_HtmlSpecialChars(),
        'StripTags',
        new Engine_Filter_Censor(),
        new Engine_Filter_StringLength(array('max' => '255')),
      )
    ));
	
    
     // Init field of study
    $this->addElement('Text', 'field_of_study', array(
      'label' => 'Field Of Study',
	  'placeholder'	=>	'Enter Field of Study',
      'maxlength' => '255',
	  'required'   => true,
      'filters' => array(
        //new Engine_Filter_HtmlSpecialChars(),
        'StripTags',
        new Engine_Filter_Censor(),
        new Engine_Filter_StringLength(array('max' => '255')),
      )
    ));
	
	 // Init Activities
    $this->addElement('Textarea', 'activities', array(
      'label' => 'Activities and Societies',
	  'placeholder'	=>	'Enter Activities and Societies',
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
        //new Engine_Filter_HtmlSpecialChars(),
        new Engine_Filter_EnableLinks(),
      ),
    ));

    // Init descriptions
    $this->addElement('Textarea', 'description', array(
      'label' => 'Description',
	  'placeholder'	=>	'Enter Description',
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
        //new Engine_Filter_HtmlSpecialChars(),
        new Engine_Filter_EnableLinks(),
      ),
    ));
    

    // Submit or succumb!
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Education',
      'type' => 'submit',
      'decorators' => array(
        'ViewHelper'
      )
    ));
    
    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'decorators' => array(
        'ViewHelper'
      )
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
  
}
