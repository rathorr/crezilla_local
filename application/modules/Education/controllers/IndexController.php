<?php

class Education_IndexController extends Core_Controller_Action_Standard
{
	
 public function init()
  {
    //if( !$this->_helper->requireAuth()->setAuthParams('education', null, 'view')->isValid() ) return;
    
     if( 0 !== ($education_id = (int) $this->_getParam('education_id')) &&
        null !== ($education = Engine_Api::_()->getItem('education', $education_id)) )
    {
      Engine_Api::_()->core()->setSubject($education);
    }
	
	 // Set up navigation
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	 if(strtolower($profile_type) == 'talent'){
		 $menu_type	=	'user_edit';
	 }else if(strtolower($profile_type) == 'agency'){
		$menu_type	=	'custom_33'; 
	 }else{
		$menu_type	=	'custom_31'; 
		if( isset($is_company->value) && $is_company->value !="" )
		{
			//echo $is_company->value;
			$menu_type	=	'custom_34'; 
		}
		
	 }
	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit');
	
  }
  
  public function indexAction()
  {	 
	 $viewer	=	Engine_Api::_()->user()->getViewer();
	 if( !$viewer->getIdentity() ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
	
/*	$this->view->navigation = Engine_Api::_()->getApi('menus', 'core')
	->getNavigation('user_edit');*/
	
	$catTable = Engine_Api::_()->getDbTable('categories', 'education');
    $catName = $catTable->info('name'); 
	
	$table = Engine_Api::_()->getDbTable('educations', 'education');
    $eduName = $table->info('name'); 
	
	//$table = Engine_Api::_()->getItemTable('education');
    $select = $table->select("$eduName.*")
						->setIntegrityCheck(false)
                        ->where("$eduName.owner_id = $viewer->user_id", 1)
						->where("$eduName.row_status = 1", 1)
       					->joinLeft($catName, "$catName.category_id = $eduName.category_id", array('category_name'))
                        ->order("$eduName.creation_date desc");
	// get the data
	$result = $table->fetchAll($select);
	$this->view->educations = $result;
	
  }
  
  
  
  public function addAction(){
			 
	 $viewer	=	Engine_Api::_()->user()->getViewer();
	 if( !$viewer->getIdentity() ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
	
	/*$this->view->navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('user_edit');*/
	
	$this->view->form = $form = new Education_Form_Education();
	
	if($this->getRequest()->getPost()){
		$education = $form->saveEduValues($this->getRequest()->getPost(), $viewer->user_id);	 
		return $this->_helper->redirector->gotoRoute(array('module' => 'education',
'controller' => 'index', 'action' => 'index'));
	}
	
  }
  
   public function deleteAction() {
    $viewer = Engine_Api::_()->user()->getViewer();
    $education = Engine_Api::_()->getItem('education', $this->getRequest()->getParam('education_id'));
    //if( !$this->_helper->requireAuth()->setAuthParams($education, null, 'delete')->isValid()) return;

    // In smoothbox
    $this->_helper->layout->setLayout('default-simple');
    
    $this->view->form = $form = new Education_Form_Delete();

    if( !$education )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_("Education doesn't exists or not authorized to delete");
      return;
    }

    if( !$this->getRequest()->isPost() )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
      return;
    }

    $db = $education->getTable()->getAdapter();
    $db->beginTransaction();

    try
    {
	  $education['row_status'] = 2;
	  $education->save();
      //education->delete();
      $db->commit();
    }

    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }

    $this->view->status = true;
    $this->view->message = Zend_Registry::get('Zend_Translate')->_('Education has been deleted.');
    return $this->_forward('success' ,'utility', 'core', array(
      'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'index'), 'education_general', true),
      'messages' => Array($this->view->message)
    ));
  }
  
   public function editAction(){   
	  
    //if( !$this->_helper->requireUser()->isValid() ) return;
    //if( !$this->_helper->requireSubject('album')->isValid() ) return;
    //if( !$this->_helper->requireAuth()->setAuthParams(null, null, 'edit')->isValid() ) return;

    // Prepare data
    $this->view->education = $education = Engine_Api::_()->core()->getSubject();
	
	/*$this->view->navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('user_edit');*/

    // Make form
    $this->view->form = $form = new Education_Form_Edit();
    
    if( !$this->getRequest()->isPost() )
    {
	  $formData	=	$education->toArray();
	  
	  $formData['category_id'] 		= ($formData['category_id'] == 0) ? 'other' : $formData['category_id'];
	  $formData['other_category'] 	= ($formData['category_id'] == 0) ? $formData['other_category'] : '';
	  
	  $formData['start_date']	=	date('d/m/Y', strtotime($formData['start_date']));
	  $formData['end_date']		=	date('d/m/Y', strtotime($formData['end_date']));
	  
      $form->populate($formData);
      
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
      return;
    } 

    if( !$form->isValid($this->getRequest()->getPost()) )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid data');
      return;
    }

    // Process
    $db = $education->getTable()->getAdapter();
    $db->beginTransaction();

    try
    {
      $values = $form->getValues();
	  $values['start_date']	=	date('Y-m-d', strtotime($values['start_date']));
	  $values['end_date']	=	date('Y-m-d', strtotime($values['end_date']));
	  $education->setFromArray($values);
      $education->save();

      $db->commit();
    }
    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }

        return $this->_helper->redirector->gotoRoute(array('action' => 'index'), 'education_general', true);
  
   }
}
