<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>
<?php 
  $this->headLink()
    ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/custom/custom.css');
?>
<script type="text/javascript">
  en4.core.runonce.add(function(){

    <?php if( !$this->renderOne ): ?>
    var anchor = $('profile_educations').getParent();
    $('profile_education_previous').style.display = '<?php echo ( $this->paginator->getCurrentPageNumber() == 1 ? 'none' : '' ) ?>';
    $('profile_education_next').style.display = '<?php echo ( $this->paginator->count() == $this->paginator->getCurrentPageNumber() ? 'none' : '' ) ?>';

    $('profile_education_previous').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() - 1) ?>
        }
      }), {
        'element' : anchor
      })
    });

    $('profile_education_next').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() + 1) ?>
        }
      }), {
        'element' : anchor
      })
    });
    <?php endif; ?>
  });
</script>
<ul id="profile_educations" class="thumbs">
  <?php
     $i=1;
     foreach( $this->paginator as $edu ):?>
    <div class="education_section" id="education_<?php echo $edu->education_id; ?>">
          
    	<div class="edu_listing">
        <span><?php echo $i;?></span>
    	<div class="title"><?php echo $edu->institute_name;?></div>
        <div class="degree"><?php echo ($edu->category_id == 'other') ? $edu->other_category : $edu->category_name;?>,<?php echo $edu->field_of_study;?></div>
        <div class="start-end-date"><?php echo $edu->start_date.'-'.$edu->end_date?></div>
        <div class="activities">
        <label>Activities: </label><span><?php echo $edu->activities ? $edu->activities : 'No activity';?></span>
        </div>
        
        <div class="activities">
         <label>Description: </label><span><?php echo $edu->description ? $edu->description : 'No description';?></span>
        </div>
    </div>
    </div>
  <?php $i++; endforeach;?>
</ul>

<div>
  <div id="profile_education_previous" class="paginator_previous">
    <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Previous'), array(
      'onclick' => '',
      'class' => 'buttonlink icon_previous'
    )); ?>
  </div>
  <div id="profile_education_next" class="paginator_next">
    <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Next'), array(
      'onclick' => '',
      'class' => 'buttonlink_right icon_next'
    )); ?>
  </div>
</div>
