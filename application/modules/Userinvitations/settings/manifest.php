<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'Userinvitations',
    'version' => '4.0.0',
    'path' => 'application/modules/Userinvitations',
    'title' => 'Userinvitations',
    'description' => 'Will show pin post on the use wall',
    'author' => '',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Userinvitations',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/Userinvitations.csv',
    ),
  ),
  // Hooks ---------------------------------------------------------------------
  'hooks' => array(
    array(
      'event' => 'onItemDeleteBefore',
      'resource' => 'Userinvitations_Plugin_Core',
    ),
  ),
  // Items ---------------------------------------------------------------------
  'items' => array(
    'Userinvitations'
  ),

    // Routes --------------------------------------------------------------------
  'routes' => array(
    // Admin - Send Mail
    
    'user_sendmail' => array(
      'route' => 'admin/Userinvitations/manage/:action/:id',
      'defaults' => array(
        'module' => 'Userinvitations',
        'controller' => 'AdminManage',
        'action' => 'sendmail'
    
      ),
      'reqs' => array(
        'action' => 'sendmail|'
    
      )
    ),
    // added on 13-09-2016

     /*    'manage_invites'=> array(
      'route' => 'members/manageinvites',
      'defaults' => array(
        'module' => 'Userinvitations',
        'controller' => 'manageinvites',
        'action' => 'username',
    
      ),
      'reqs' => array(
      'action' => 'index|username',    
      )
    ),*/
        'manage_invites'=> array(
      'route' => 'members/manageinvites',
      'defaults' => array(
        'module' => 'Userinvitations',
        'controller' => 'manageinvites',
        'action' => 'index',
    
      ),
      'reqs' => array(
      'action' => 'index|',    
      )
    ),
    'manage_usercodes'=> array(
      'route' => 'members/manageinvites/addMoreCodes',
      'defaults' => array(
        'module' => 'Userinvitations',
        'controller' => 'manageinvites',
        'action' => 'addMoreCodes',
    
      ),
      'reqs' => array(
      'action' => 'addMoreCodes|',    
      )
    ),
     // added on 13-09-2016 End
 )
); ?>