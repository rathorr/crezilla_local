<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Create.php 9837 2012-11-29 01:12:35Z matthew $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Userinvitations_Form_Admin_Create extends Engine_Form
{
	
  public function init()
  {
    $this->setTitle('Send Invitation')
      ->setDescription('Please compose a new Invitation below.')
      ->setAttrib('id', 'pinposts_create')
	  ->setAttrib('class', 'global_form');     
   
      
    $this->addElement('Text', 'name', array(
      'label' => 'Name*',
      'required' => false,	  
	  'placeholder'	=>	'Enter Name',
      'allowEmpty' => false,
    ));
    
    // Add title
    $this->addElement('Text', 'email', array(
      'label' => 'Email*',
	  'placeholder'	=>	'Enter email',
      'required' => true,
      'allowEmpty' => false,
    ));
	
	$string=uniqid();
	
	
	// Add code
    $this->addElement('Text', 'code-display', array(
      'label' => 'Code*',
	  'placeholder'	=>	'Enter code',	  
      'disabled' => 'disabled',
      'value'	=>	$string,
      'required' => false,
      
    ));
	
	
	// Add code
    $this->addElement('Hidden', 'code', array(
      'label' => 'code*',
	  'placeholder'	=>	'Enter code',	        
      'value'	=>	$string,
      'required' => false,
      
    ));
		
	
    $this->addElement('Button', 'submit_post', array(
      'label' => 'Edit',
      'type' => 'button',
      'onclick'=> 'chkdate()',
	  'ignore' => true,
      'decorators' => array('ViewHelper')
    ));


        // Buttons
    $this->addElement('Button', 'submit_post', array(
      'label' => 'Send',
      'type' => 'button',
      'onclick'=> 'chkdate()',
	  'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'ignore' => true,
      'link' => true,
      'href' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('module' => 'Userinvitations', 'controller' => 'manage', 'action' => 'index'), 'admin_default', true),
      'prependText' => Zend_Registry::get('Zend_Translate')->_(' or '),
      'decorators' => array(
        'ViewHelper',
      ),
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
}