<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Edit.php 9837 2012-11-29 01:12:35Z matthew $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Userinvitations_Form_Admin_Edit extends Engine_Form
{
  public function init()
  {
    $this->setTitle('Edit User Invitation')
      ->setDescription('Please modifiy your pinpost below.')
	  ->setAttrib('id', 'userinvitations_edit');    
	  
	  
    $this->addElement('Text', 'name', array(
      'label' => 'Name*',
      'required' => true,	  
	  'placeholder'	=>	'Enter name',
      'allowEmpty' => false,
    ));
    
    // Add title
    $this->addElement('Text', 'email', array(
      'label' => 'email*',
	  'placeholder'	=>	'Enter email',
      'required' => true,
      'allowEmpty' => false,
    ));
	
	
	// Add title
    $this->addElement('Text', 'code', array(
      'label' => 'code*',
	  'placeholder'	=>	'Enter code',
      'required' => true,
      'allowEmpty' => false,
    ));
	

    $this->addElement('Button', 'submit_post', array(
      'label' => 'Edit',
      'type' => 'button',
      'onclick'=> 'chkdate()',
	  'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'ignore' => true,
      'link' => true,
      'href' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('module' => 'Userinvitations', 'controller' => 'manage', 'action' => 'index'), 'admin_default', true),
      'prependText' => Zend_Registry::get('Zend_Translate')->_(' or '),
      'decorators' => array(
        'ViewHelper',
      ),
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
}