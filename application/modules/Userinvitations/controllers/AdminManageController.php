<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: AdminManageController.php 9837 2012-11-29 01:12:35Z matthew $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Userinvitations_AdminManageController extends Core_Controller_Action_Admin
{
  public function indexAction()
  {

    $this->view->formFilter = $formFilter = new Userinvitations_Form_Admin_Filter();
    $page = $this->_getParam('page', 1);

    if( $formFilter->isValid($this->_getAllParams()) ) {
      $values = $formFilter->getValues();
      $paginator = Engine_Api::_()->userinvitations()->getPaginator($values);
	  
      if ($values['orderby'] && $values['orderby_direction'] != 'DESC') {
        $this->view->orderby = $values['orderby'];
      }
    } else {
      $paginator = Engine_Api::_()->userinvitations()->getPaginator();
    }
    
    $this->view->paginator = $paginator->setCurrentPageNumber( $page );
  }
  
  
  public function createAction()
  {
  
    $this->view->form = $form = new Userinvitations_Form_Admin_Create();	
	

    if( $this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost()) ) {
		$params = $form->getValues();
	
	
		if($params['email']!=""){
		
			$email=$params['email'];		
			$users = Engine_Api::_()->getDbTable('users', 'user');
			$invitName = $users->info('name'); 
			$selectInv = $users->select("user_id")		
						->where("email = '$email'", 1);					
						$selectInv = $users->fetchRow($selectInv);		
			if(isset($selectInv['user_id'])){						
				$this->view->error ='Email already exits';
				 //return $this->_helper->redirector->gotoRoute(array('action' => 'create'));
			}else{
			
				// send mail save invitations 
				 $invitations = Engine_Api::_()->getDbtable('userinvitations', 'userinvitations')->createRow();
				 $params_send['name'] = $params['name'];
				 $params_send['email'] = $params['email'];
				 $params_send['code'] = $params['code'];
				 $params_send['status'] = 1;
				 $params_send['send_on'] = date('Y-m-d h:i:s');		
				
				$invitations->setFromArray($params_send);
				$invitations->save(); 			
				$inviteUrl = Zend_Controller_Front::getInstance()->getRouter()->assemble(array(
						  'module' => '',
						  'controller' => 'signup',
						), 'default', true)
					  . '?'
					  . http_build_query(array('code' => $params_send['code']));
				$message='';
				$message = str_replace('%invite_url%', $inviteUrl, $message);	  
		  
				// get admin email 
				$users_table = Engine_Api::_()->getDbtable('users', 'user');
				$users_select = $users_table->select()
					->where('level_id = ?', 1)
					->where('enabled >= ?', 1);
				$super_admin = $users_table->fetchRow($users_select);     
				// Send mail
				$mailType =  'invite_code';
				$mailParams = array(
				  'host' => $_SERVER['HTTP_HOST'],
				  'email' => $params_send['email'],
				  'date' => time(),
				  'sender_email' => $super_admin->email,
				  //'sender_title' => $super_admin->displayname,
				  // 'sender_link' => $user->getHref(),
				  // 'sender_photo' => $user->getPhotoUrl('thumb.icon'),
				  'message' => $message,
				  'object_link' => $inviteUrl,
				  'code' => $params_send['code'],
				);			
				$send = Engine_Api::_()->getApi('mail', 'core')->sendSystem($params_send['email'], $mailType,$mailParams);  
				if($send){
					
					return $this->_helper->redirector->gotoRoute(array('action' => 'index'));	
				}else{
					$this->view->error ='An error occurred while sending mail.';
				}
			
			}				
		
		}
		
	
    }
  }

  public function editAction()
  {
  
    $id = $this->_getParam('id', null);
    $pinpost = Engine_Api::_()->getItem('Userinvitations', $id);
    
    // $pinpost['start_date']	= date('Y-m-d', strtotime($pinpost['start_date']));
	// $pinpost['end_date']	= date('Y-m-d', strtotime($pinpost['end_date']));
    $this->view->form = $form = new Userinvitations_Form_Admin_Edit();
    $form->populate($pinpost->toArray());

    // Save values
    if( $this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost()) ) {
      $params = $form->getValues();
      // $params['start_date']	= date('Y-m-d', strtotime($params['start_date']));
	  // $params['end_date']	= date('Y-m-d', strtotime($params['end_date']));
      $pinpost->setFromArray($params);
      $pinpost->save();
      return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
    }
  }

  public function deleteAction()
  {
    //$this->view->form = $form = new Announcement_Form_Admin_Edit();
    $this->view->id = $id = $this->_getParam('id', null);
    $pinpost = Engine_Api::_()->getItem('Userinvitations', $id);

    // Save values
    if( $this->getRequest()->isPost() )
    {
      $pinpost->delete();
      return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
    }
  }

  public function deleteselectedAction()
  {
    //$this->view->form = $form = new Announcement_Form_Admin_Edit();
    $this->view->ids = $ids = $this->_getParam('ids', null);
    $confirm = $this->_getParam('confirm', false);
    $this->view->count = count(explode(",", $ids));

    //$announcement = Engine_Api::_()->getItem('announcement', $id);

    // Save values
    if( $this->getRequest()->isPost() && $confirm == true )
    {
      $ids_array = explode(",", $ids);
      foreach( $ids_array as $id ){
        $pinpost = Engine_Api::_()->getItem('Userinvitations', $id);
        if( $pinpost ) $pinpost->delete();
      }
      
      $this->_helper->redirector->gotoRoute(array('action' => 'index'));
    }
  }
  
  
  public function sendmailAction(){
  
    $res['status']=1;
	$res['msg']='';	
   
   	if($_POST['email']!=""){
		$email= $_POST['email'];
		$users = Engine_Api::_()->getDbTable('users', 'user');
		$invitName = $users->info('name'); 
		 $selectInv = $users->select("user_id")
		 ->setIntegrityCheck(false)
					->where("email = '$email'", 1);					
					$selectInv = $users->fetchRow($selectInv);
		 
		if(isset($selectInv['user_id'])){
			$res['status']=1;
			$res['msg']='Email already exits.';				
		}else{		
			$res['status']=0;
			$res['msg']='';	
		
			 $id = $this->_getParam('id', null);
			 $invitations = Engine_Api::_()->getItem('Userinvitations', $id);
			 $params['name'] = $_POST['name'];
			 $params['email'] = $_POST['email'];
			 $params['status'] = 1;
			 $params['send_on'] = date('Y-m-d h:i:s');			
			$invitations->setFromArray($params);
			$invitations->save(); 			
			$inviteUrl = Zend_Controller_Front::getInstance()->getRouter()->assemble(array(
					  'module' => '',
					  'controller' => 'signup',
					), 'default', true)
				  . '?'
				  . http_build_query(array('code' => $invitations['code']));
			$message='';
			$message = str_replace('%invite_url%', $inviteUrl, $message);	  
		  
		 // get admin email 
		  $users_table = Engine_Api::_()->getDbtable('users', 'user');
		  $users_select = $users_table->select()
				->where('level_id = ?', 1)
				->where('enabled >= ?', 1);
		  $super_admin = $users_table->fetchRow($users_select);     
			// Send mail
			$mailType =  'invite_code';
			$mailParams = array(
			  'host' => $_SERVER['HTTP_HOST'],
			  'email' => $invitations['email'],
			  'date' => time(),
			  'sender_email' => $super_admin->email,
			  'sender_title' => $super_admin->displayname,
			  // 'sender_link' => $user->getHref(),
			  // 'sender_photo' => $user->getPhotoUrl('thumb.icon'),
			  'message' => $message,
			  'object_link' => $inviteUrl,
			  'code' => $invitations['code'],
			);
			
			$send = Engine_Api::_()->getApi('mail', 'core')->sendSystem($invitations['email'], $mailType,$mailParams);
			
			if($send){
				echo json_encode($res);
				die;
			}else{
				$res['status']=1;
				$res['msg']='There are some issue in sending mail.';
				echo json_encode($res);
				die;			
			}
			
			//$this->_helper->redirector->gotoRoute(array('action' => 'index'));
			
		}
		
	}	
	echo json_encode($res);
	die;
  
 }
  
 
}