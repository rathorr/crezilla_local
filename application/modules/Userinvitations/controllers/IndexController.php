<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: IndexController.php 9893 2013-02-14 00:00:53Z shaun $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Userinvitations_IndexController extends Core_Controller_Action_Standard
{
  public function init()
  {
  
  }
  
  
  // NONE USER SPECIFIC METHODS
  public function indexAction()
  {
   
  }


  // USER SPECIFIC METHODS
  public function manageAction()
  {  
    if( !$this->_helper->requireUser()->isValid() ) return;	
		$uid=Engine_Api::_()->user()->getViewer()->getIdentity();
		
		$invitations = Engine_Api::_()->getDbTable('userinvitations', 'userinvitations');
		$invitName = $invitations->info('name'); 
		$selectInv = $invitations->select("email")
		->where("sender_id = '$uid'", 1);
		//->where("code = '$code'", 1);
		 $selectInv = $invitations->fetchAll($selectInv);		
		$viewer = Engine_Api::_()->user()->getViewer();
		$this->view->user_id = $viewer->getIdentity();
		$projects = $categories = Engine_Api::_()->getDbtable('parenttypes', 'classified')->getListAssoc($viewer);
	
		$posted_jobs = array();
		$i = 0;
		if(count($selectInv) > 0)
		{
				foreach($projects as $project)
				{
			
				$values['project'] 						= $project['project_id']; 
				$posted_jobs[$i]['project_name'] 		= stripslashes($project['parent_name']);
				$posted_jobs[$i]['project_desc'] 		= $project['parent_desc'];
				$posted_jobs[$i]['project_id'] 			= $project['parent_id'];
				$posted_jobs[$i]['permission'] 			= $project['permission'];
				
				$posted_jobs[$i]['owner_id'] 			= $project['owner_id'];
				$posted_jobs[$i]['user_id'] 			= $project['user_id'];
				$i++;
				}
			}
		$this->view->projects = $selectInv;
		
	
  }
  
   public function sendmailAction(){
   
   	$res['status']=1;
	$res['msg']='';
	
	
   
   	if($_POST['email']!=""){	
	
		$email=$_POST['email'];		
		$users = Engine_Api::_()->getDbTable('users', 'user');
		$invitName = $users->info('name'); 
		 $selectInv = $users->select("user_id")		
					->where("email = '$email'", 1);					
		 $selectInv = $users->fetchRow($selectInv);
	
		if(isset($selectInv['user_id'])){
			$res['status']=1;
			$res['msg']='Email already exits.';				
		}else{		
			$res['status']=0;
			$res['msg']='we have send email successfully';	
		
			 $id = $this->_getParam('id', null);
			 $invitations = Engine_Api::_()->getItem('Userinvitations', $id);
			 $params['name'] = $_POST['name'];
			 $params['email'] = $_POST['email'];
			 $params['status'] = 1;
			 $params['send_on'] = date('Y-m-d h:i:s');			
			$invitations->setFromArray($params);
			$invitations->save(); 			
			$inviteUrl = Zend_Controller_Front::getInstance()->getRouter()->assemble(array(
					  'module' => '',
					  'controller' => 'signup',
					), 'default', true)
				  . '?'
				  . http_build_query(array('code' => $invitations['code']));
			$message='';
			$message = str_replace('%invite_url%', $inviteUrl, $message);	  
		  
		 // get admin email 
		  // $users_table = Engine_Api::_()->getDbtable('users', 'user');
		  // $users_select = $users_table->select()
				// ->where('level_id = ?', 1)
				// ->where('enabled >= ?', 1);
		  // $super_admin = $users_table->fetchRow($users_select);     
		  
		  
		  $user_sender = Engine_Api::_()->user()->getUser(Engine_Api::_()->user()->getViewer()->getIdentity());
		  $user_sender_email = explode('@',$user_sender->email);
			// Send mail
			$mailType =  'invite_code';
			$mailParams = array(
			  'host' => $_SERVER['HTTP_HOST'],
			  'email' => $invitations['email'],
			  'date' => time(),
			  'sender_email' => $user_sender->email,
			  'sender_title' => $user_sender->displayname,
			  // 'sender_link' => $user->getHref(),
			  // 'sender_photo' => $user->getPhotoUrl('thumb.icon'),
			  'message' => $message,
			  'object_link' => $inviteUrl,
			  'code' => $invitations['code'],
			);	
			
			Engine_Api::_()->getApi('mail', 'core')->sendSystem($invitations['email'], $mailType,$mailParams);
			//$this->_helper->redirector->gotoRoute(array('action' => 'index'));
			
		}
		
	}	
	echo json_encode($res);
	die;
 }

  public function createAction()
  {
  
  }

  public function deleteAction()
  {
  
  }
  
  public function closeAction()
  {
  
  }
  
}

