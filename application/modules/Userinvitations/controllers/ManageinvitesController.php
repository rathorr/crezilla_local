<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: SignupController.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Userinvitations_ManageinvitesController extends Core_Controller_Action_Standard
{
  public function init()
  {
  }
  
  public function indexAction($type=NULL)
  {
    //$search_info  = Engine_Api::_()->getDbtable('users', 'user')->getAllUserInfo();
    //$table = Engine_Api::_()->getDbtable('users', 'user');
   // $select = $table->select();
    $query=Engine_Db_Table::getDefaultAdapter()->select()
            ->from('engine4_users as t1')
            ->distinct()
            ->where('t1.user_id != 1')
            ->where('t1.displayname != " "')
            ->joinLeft("engine4_userinvitations_userinvitations as t2", "t2.sender_id = t1.user_id", null);

   $userDetails= $query->query()->fetchAll();
   $this->view->userDetailsArray=$userDetails;  
  } 
  public function userinfoAction($type=NULL)
  {
    $topuserid=Engine_Api::_()->user()->getViewer()->getIdentity();
    if($topuserid == 1)
    {

       $userid = $_POST['userid'];
       //echo $userid;
       //die();
      $query=Engine_Db_Table::getDefaultAdapter()->select()
            ->from("engine4_users")
            ->where("user_id = $userid",1)
            ->where("displayname != ''",1);

  $fetchuserDetails= $query->query()->fetchAll();
  $res['name']=ucwords(strtolower($fetchuserDetails[0]['displayname']));
  $res['email']=$fetchuserDetails[0]['email'];
  $res['usrid']=$fetchuserDetails[0]['user_id'];

  $qtotalcodes=Engine_Db_Table::getDefaultAdapter()->select()
            ->from("engine4_userinvitations_userinvitations")
            ->where("sender_id = $userid ",1);

  $qtotalcodes= $qtotalcodes->query()->fetchAll();
 // die();
  $res['totalcode']=count($qtotalcodes);
  //$this->view->$qtotalcodes= count($qtotalcodes);
  $qsendon=Engine_Db_Table::getDefaultAdapter()->select()
            ->from("engine4_userinvitations_userinvitations")
            ->where("sender_id = $userid ",1)
            ->where("send_on != '0000-00-00 00:00:00'");

  $qsendon= count($qsendon->query()->fetchAll());
  $res['totalsent']=$qsendon;
  //$this->view->$qsendon= $qsendon;
  $res['totalpend']= $res['totalcode']-$res['totalsent'];


  /*$invitations = Engine_Api::_()->getDbTable('userinvitations', 'userinvitations');
    $invitName = $invitations->info('name'); 
     $selectemail = $invitations->select("sender_id")
          ->setIntegrityCheck(false)          
          ->where("sender_id = '$sender_id'", 1);
     $selectemail = $invitations->fetchRow($selectemail);
     $res['email'] =$selectemail['email'];

   
    $invitations = Engine_Api::_()->getDbTable('userinvitations', 'userinvitations');
    $invitName = $invitations->info('name'); 
     $selectemail = $invitations->select("name,email")
          ->setIntegrityCheck(false)          
          ->where("code = '$code'", 1);
     $selectemail = $invitations->fetchRow($selectemail);
     $res['email'] =$selectemail['email'];
     $name= explode(' ', $selectemail['name']);
     $res['first_name'] =$name[0];
     $res['last_name'] =$name[1];    
  */
  echo json_encode($res);   
  die;  

    }
    else
    {
      echo  $topuserid;
    }
   // die();
  /*$userid=Engine_Api::_()->user()->getViewer()->getIdentity();
$this->view->userfirstname=Engine_Api::_()->getDbtable('users', 'user')->getUserFirstName($userid); 
   $this->view->username=Engine_Api::_()->getDbtable('users', 'user')->getUserFullName($userid);*/
    } 

public function addmorecodesAction($type=NULL)
  {
    $topuserid=Engine_Api::_()->user()->getViewer()->getIdentity();
    if($topuserid == 1)
    {
      $noofcode=$_POST['noofcode'];
      $usr_ide=$_POST['usr_ide'];
    for($i=1;$i <=$noofcode;$i++){          
          $params_for['status'] = 0;
          $params_for['created_on'] = date('Y-m-d h:i:s');      
          $params_for['sender_id']  = $usr_ide;
          $params_for['code'] = $string=uniqid();
          $invitSave = Engine_Api::_()->getDbtable('userinvitations', 'userinvitations')->createRow();
          $invitSave->setFromArray($params_for);
          $invitSave->save();
        }
        $resp['status']=1;
    echo json_encode($resp);  
    die;
  }
  }
}