<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcements
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>

<ul id="pinpost-feed" class="pndiv">
  <?php foreach( $this->pinposts as $item ): ?>
    <li class="activity_post_items intsec">  

	<div class="feed_item_photo">
		<a href="javascript:void(0);" style="display:inline-block; vertical-align: top;">
			<img class="thumb_icon item_photo_page  thumb_icon" alt="Crezilla" src="<?php echo $this->baseUrl();?>/public/custom/images/ipro-welcome-logo.jpg" alt="welcome logo">
		</a>
		<span style="display:inline-block;" class="status_user">
			<a class="feed_item_username welctitle" href="javascript:void(0);">
			<?php echo $item->title;?>
			</a>
			<span class="timestamp" style="display:block;margin-top: -5px;">
			<?php echo $this->timestamp($item->creation_date);?>
			</span> 
		</span>

		 
	</div>
    
        <!-- end add some actions --> 
    <div class="feed_item_body">
            <span class="feed_item_posted">
                    
          <!--// End comment -->
          <span class="feed_item_statustext welcomebrif"><?php echo $item->body;?></span>
          <span style="font-weight: bold; font-family: arial ! important; color: rgb(66, 66, 66) ! important; font-size: 18px;">
          	To grow your network. <a href="<?php echo $this->baseUrl().'/members';?>" style="text-decoration:underline;">ADD CONNECTIONS</a> and <a href="<?php echo $this->baseUrl().'/invite';?>" style="text-decoration:underline;">INVITE a CONNECTION</a>.
          </span>         
        
      </span>      
       <!-- End of Comment Likes -->
    </div>
  </li>
  <?php endforeach; ?>
</ul>


