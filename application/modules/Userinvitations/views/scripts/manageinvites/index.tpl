<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: add.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>
<style>
	#id_tableDis
	{
		text-align: center !important;
	}
	#userId_code{
		text-align: center !important;

	}
	#name_code{
		text-align: center !important;
	}
	#accordion > table > tbody > tr > th{
		width: 10%;
		text-align: center !important;
	}
	#accordion > table > tbody > tr > td{
		text-align: center !important;
	}
</style>
<div class="layout_page_user_job_index">
<div class="generic_layout_container layout_top">
	<div class="generic_layout_container layout_middle">
		<div class="generic_layout_container layout_classified_browse_menu">
				<div class="headline">
				  <h2>Manage Invites  </h2>
				</div>
		</div>
	</div>
</div>
<div class="generic_layout_container layout_main">
<div class="Invitations_middle">	
<div class="generic_layout_container layout_core_content">
  	<div id="invite-wrapper" class="form-wrapper-heading">
		
		<?php //echo $this->userid;
		/*$userinfoArray = $this->userDetailsArray;
		echo '<pre>';
    print_r($userinfoArray);
    die();
    $userinfoArray = $this->userDetailsArray;
    foreach ($userinfoArray as $key => $uvalue) {
			  echo $uvalue['user_id'];
			  echo $uvalue['displayname'];
			  }*/
    ?>
    <form id="user_form_changepass" enctype="application/x-www-form-urlencoded" class="change_pass_form" action="/Userinvitations/manageinvites/userinfo" method="post">
    	<span  class="field_container">Select a User</span>&nbsp;&nbsp;
		<select id="user_id" name="user_id" >
			<option selected="selected"  value="">--User Name--</option>
			 <?php 
			$userinfoArray = $this->userDetailsArray;
			$usrid=$this->userid;
			foreach ($userinfoArray as $key => $uvalue){
				  $u_id = $uvalue['user_id'];
				  $u_name= ucwords(strtolower($uvalue['displayname']));
				  ?>
			<option value="<?php echo $u_id; ?>"<?php echo ($u_id =="$usrid"?"selected='selected'":""); ?>><?php echo $u_name;?></option>		
			<?php  } ?> 
		</select>
	<input id="get_vals" name="get_vals" value="Get Details" type="button" />
	</form>
	</div>
	<div class="generic_layout_container layout_core_content"  id='id_tableDis'>
	<?php
	/*if(totalcodes != 0)
	{
	$totalcodes= $this->$qtotalcodes;
	$totalsend=$this->$qsendon;
	$name= $this->$fetchuserDetails[0]['displayname'];
	$email=$this->$fetchuserDetails[0]['email']; */
	?>
    <div id="accordion" style="margin-top:12px">	
	  <table class="manage_inc" border="1" align="center">
      <tbody>
      <tr>
      	<th scope="col">User Id</th>
      	<th scope="col">Email</th>
        <th scope="col">Dispaly Name</th>
        <th scope="col">Code Assigned</th>
        <th scope="col">Code Sent</th>
        <th scope="col">Pending</th>
        <th scope="col">Add</th>
        <th scope="col">Action</th>
      </tr>
	 
	 <!--  <tr>	  
		<td><?php echo $email;?></td>
		<td><?php echo ucwords(strtolower($name)); ?></td>
      	<td><?php echo $email;?></td>
      	<td><?php echo $email;?></td>
      	<td><?php echo $email;?></td>      
        <td><input type="text" id="name_code" name="name_code" value=""></td>
        <td><input type="button" name="add_more_code" id="add_more_code" value="Add"></td>
      </tr>	  -->
       <tr>	  
		<td><input type="text" readonly id="userId_code" name="userId_code" value=""></td>
		<td><span id="uemail"></span></td>
		<td><span id="uname"></span></td>
      	<td><span id="totalcode"></span></td>
      	<td><span id="totalsent"></span></td>
      	<td><span id="totalpend"></span></td>      
        <td><input type="text" id="name_code" name="name_code" value=""></td>
        <td><input type="button" name="add_more_code" id="add_more_code" value="Assign More"></td>
      </tr>	 
	  </tbody>
	  </table>	</div>
	  <?php //}?>
</div>
</div>
</div>
</div>
<script>
jQuery(document).ready(function(){
jQuery('#id_tableDis').hide();
});
	jQuery('#get_vals').click(function(){
		jQuery.post("<?php echo $this->baseUrl();?>/Userinvitations/manageinvites/userinfo",{userid:jQuery.trim(jQuery("#user_id").val())}, function(resp){
			console.log(resp);
		var data=JSON.parse(resp);
		if(data){	
			console.log('data'+data);
			//jQuery('#id_tableDis').css('display:block !important;');
			jQuery('#id_tableDis').show();
			jQuery("#uemail").text(data.email);
			jQuery("#uname").text(data.name);
			jQuery("#totalcode").text(data.totalcode);
			jQuery("#totalsent").text(data.totalsent);
			jQuery("#totalpend").text(data.totalpend);
			jQuery("#userId_code").val(data.usrid);
		}
		else
		{
			jQuery('#id_tableDis').hide();
		}
	});

	});
		jQuery('#add_more_code').click(function(){
			var noofcode =jQuery.trim(jQuery("#name_code").val());
			var usr_ide = jQuery.trim(jQuery("#userId_code").val());
		jQuery.post("<?php echo $this->baseUrl();?>/Userinvitations/manageinvites/addmorecodes",{noofcode:noofcode,usr_ide:usr_ide}, function(resp){
			console.log(resp);
		var data=JSON.parse(resp);
		if(data){	
			console.log('data'+data);
			if(data.status==1)
			{
				alert('Codes successfully Added!');
				location.reload();
			}
			//return false;
		}
		else
		{
			jQuery('#id_tableDis').hide();
		}
	});

	});
</script>