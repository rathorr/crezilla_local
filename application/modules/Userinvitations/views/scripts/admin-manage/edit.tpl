<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: edit.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>
<div class="settings">
  <?php echo $this->form->render($this) ?>
</div>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<?php 
 $this->headLink()
    ->appendStylesheet($this->baseUrl() . '/externals/jquery/jquery-ui.css');
   $this->headLink();
   ?>
<style type="text/css">
.com_form_error_validatin {
    background-color: #dc2324 !important;
    border: 0 none !important;
    color: #fff !important;
}
</style>

<script type="application/javascript">
jQuery(document).ready(function(){
	
jQuery('#start_date').datepicker({ dateFormat: 'dd-mm-yy',changeMonth: true,
        changeYear: true, minDate: 0 });
jQuery('#end_date').datepicker({ dateFormat: 'dd-mm-yy',changeMonth: true,
        changeYear: true, minDate: 0});
});

function chkdate(){
	jQuery('.com_form_error_validatin').addClass("com_form_error_validatin");
	if(jQuery.trim(jQuery('#title').val()) == ''){
			jQuery('#title').focus();
			jQuery('#title').addClass("com_form_error_validatin");
			//alert('Please select category.');
			return false;
		}
	if(jQuery.trim(jQuery('#start_date').val()) == ''){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Please select category.');
			return false;
		}
	if(jQuery.trim(jQuery('#end_date').val()) == ''){
			jQuery('#end_date').focus();
			jQuery('#end_date').addClass("com_form_error_validatin");
			//alert('Please select category.');
			return false;
		}
	if(parseInt(fn_DateCompare(jQuery.trim(jQuery('#start_date').val()), jQuery.trim(jQuery('#end_date').val()))) == 1){
			jQuery('#end_date').focus();
			jQuery('#end_date').addClass("com_form_error_validatin");
			//alert('Start date can not be greater than end date.');
			jQuery('#end_date').val('');
			return false;
		}
		
	jQuery('#pinposts_edit').submit();
}

function fn_DateCompare(DateA, DateB) {
  var a = new Date(DateA.replace(/-/g, '/'));
  var b = new Date(DateB.replace(/-/g, '/'));

  var msDateA = Date.UTC(a.getFullYear(), a.getMonth()+1, a.getDate());
  var msDateB = Date.UTC(b.getFullYear(), b.getMonth()+1, b.getDate());

  if (parseFloat(msDateA) < parseFloat(msDateB))
	return -1;  // less than
  else if (parseFloat(msDateA) == parseFloat(msDateB))
	return 0;  // equal
else if (parseFloat(msDateA) > parseFloat(msDateB))
	return 1;  // greater than
else
	return null;
}  // error

</script>