<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: create.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>

<div class="settings">
	
	<div id="he_contacts_message" style="display:block;" ><div class="msg"><?php echo $this->error; ?></div></div>
  <?php echo $this->form->render($this) ?>
</div>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<?php 
 $this->headLink()
    ->appendStylesheet($this->baseUrl() . '/externals/jquery/jquery-ui.css');
   $this->headLink();
   ?>
<style type="text/css">
.com_form_error_validatin {
    background-color: #dc2324 !important;
    border: 0 none !important;
    color: #fff !important;
}

.msg { color:#FF0000 !important}

</style>

<script type="application/javascript">
jQuery(document).ready(function(){
	jQuery('#codedisplay').val(jQuery('#code').val());
	
});

		function chkdate(){
				//alert('ddd');
			jQuery('.com_form_error_validatin').addClass("com_form_error_validatin");
			
			if(jQuery.trim(jQuery('#name').val()) == ''){
					jQuery('#name').focus();
					jQuery('#name').addClass("com_form_error_validatin");					
					return false;
				}
			
			if(jQuery.trim(jQuery('#email').val()) == ''){
					jQuery('#email').focus();
					jQuery('#email').addClass("com_form_error_validatin");
					//alert('Please select category.');
					return false;
				}
			
				
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;	
	
			if(jQuery.trim(jQuery('#email').val()) == '' || regex.test(jQuery('#email').val())==false){
					jQuery('#email').focus();
					jQuery('#email').addClass("com_form_error_validatin");			
					return false;
			}	
			
			jQuery('#pinposts_create').submit();
		}

// error
//});

</script>