<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 10051 2013-06-11 23:36:56Z jung $
 * @author     Sami
 */
?>
<script type="text/javascript">

  en4.core.runonce.add(function() {
    $$('th.admin_table_short input[type=checkbox]').addEvent('click', function(event) {
      var el = $(event.target);
      $$('input[type=checkbox]').set('checked', el.get('checked'));
    });
  });

  var changeOrder =function(orderby, direction){
    $('orderby').value = orderby;
    $('orderby_direction').value = direction;
    $('filter_form').submit();
  }

  var delectSelected =function(){
    var checkboxes = $$('input[type=checkbox]');
    var selecteditems = [];

    checkboxes.each(function(item, index){
      var checked = item.get('checked');
      var value = item.get('value');
      if (checked == true && value != 'on'){
        selecteditems.push(value);
      }
    });

    $('ids').value = selecteditems;
    $('delete_selected').submit();
  }
  
  jQuery(document).on('click','.active_post',function(){
	jQuery('.active_post').each(function(){
			jQuery(this).removeAttr('checked');	
	});
	
	jQuery(this).prop('checked',true);
	jQuery.post('<?php echo $this->baseUrl();?>/admin/pinpost/manage/changepoststatus',{row_id:jQuery.trim(jQuery(this).val())},function(resp){});
	
});

  jQuery(document).on('click','a.send-mail',function(){
	//alert('dd');
	//alert(jQuery(this).attr('id'));

	var id=jQuery(this).attr('id');
	
	
	jQuery('#name_'+id).removeClass("com_form_error_validatin");	
	jQuery('#email_'+id).removeClass("com_form_error_validatin");	
	
	// if(jQuery.trim(jQuery('#name_'+id).val()) == ''){
			// jQuery('#name_'+id).focus();
			// jQuery('#name_'+id).addClass("com_form_error_validatin");			
			// return false;
	// }
	
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;	
	
	if(jQuery.trim(jQuery('#email_'+id).val()) == '' || regex.test(jQuery('#email_'+id).val())==false){
			jQuery('#email_'+id).focus();
			jQuery('#email_'+id).addClass("com_form_error_validatin");			
			return false;
	}
	
	var name = jQuery('#name_'+id).val();
	var email = jQuery('#email_'+id).val();	
	
	jQuery.post('<?php echo $this->baseUrl();?>/admin/Userinvitations/manage/sendmail/id/'+id,{
	row_id:id,
	name:name,
	email:email,
	},function(resp){
		//alert(resp);
	
		var data = JSON.parse(resp);
		if(data.status==0){		
			//window.location.href = '<?php echo $this->baseUrl();?>/Userinvitations/index/manage';	
			//window.location.href = '<?php echo $this->baseUrl();?>/admin/Userinvitations/manage';
			alert('we have send email successfully.');
			return false;			
		}else{
			alert(data.msg);
		return false;
		
		}
	
		
		});
	
	});
</script>

<h2><?php echo $this->translate('Manage Invitations') ?></h2>
<?php echo $this->formFilter->render($this) ?>
<br />
<div>
  <?php echo $this->htmlLink(array('action' => 'create', 'reset' => false), 
    $this->translate("Post New Invitation"),
    array(
      'class' => 'buttonlink',
      'style' => 'background-image: url(' . $this->layout()->staticBaseUrl . 'application/modules/Pinpost/externals/images/admin/add.png);')) ?>
  <?php if($this->paginator->getTotalItemCount()!=0): ?>
    <?php echo $this->translate('%d Total Invitations', $this->paginator->getTotalItemCount()) ?>
  <?php endif;?>
  <?php echo $this->paginationControl($this->paginator); ?>
</div>
<br />
<?php if( count($this->paginator) ): ?>
  <table class='admin_table'>
    <thead>
      <tr>
        <th style="width: 1%;" class="admin_table_short"><input type='checkbox' class='checkbox'></th>
		
        <th style="width: 1%;"><a href="javascript:void(0);" onclick="javascript:changeOrder('userinvitations_id', '<?php if($this->orderby == 'userinvitations_id') echo "DESC"; else echo "ASC"; ?>');">
          <?php echo $this->translate("ID") ?>
        </a></th>
		<th style="width: 5%;" class="admin_table_short">Code</th>
        <th style="width: 15%;"><a href="javascript:void(0);" onclick="javascript:changeOrder('name', '<?php if($this->orderby == 'name') echo "DESC"; else echo "ASC"; ?>');">
          Name
        </a></th>

        <th style="width: 15%;"><a href="javascript:void(0);" onclick="javascript:changeOrder('email', '<?php if($this->orderby == 'email') echo "DESC"; else echo "ASC"; ?>');">
          Email
        </a></th>
        <th style="width:5%;">Status</th>
        <th style="width:5%;">Sender</th>
        <th style="width:15%;"><a href="javascript:void(0);" onclick="javascript:changeOrder('send_on', '<?php if($this->orderby == 'send_on') echo "DESC"; else echo "ASC"; ?>');">          
		  Send on
        </a></th>
        <th style="width: 15%;">
          <?php echo $this->translate("Options") ?>
        </th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($this->paginator as $item): 
		  // echo "<pre>";
		  // print_r($item);
		  // echo "</pre>";
		  
		  ?>	   
      <tr>	 
	  
	  <?php if($item->level_id!="" || $item->sender_id!=0){ ?>
	  
			<td>#</td>
			<td><?php echo $item->userinvitations_id ?></td>		
			<td class="admin_table_bold"><?php echo $item->code ?> </td>
			<td class="admin_table_bold"><?php echo $item->displayname ?> </td>
			<td><?php echo $item->email ?></td>			
			<td><?php if($item->status==1){echo 'Send';}else{echo "Not send";} ?></td>
			<td><?php echo ($item->sender_id==0)?'Admin':'User'; ?></td>
			<td><?php echo ($item->send_on!='0000-00-00 00:00:00')? date('F d, Y', strtotime($item->send_on)):'N/A'; ?></td>
			<td class="admin_table_options"> 
				<?php echo ($item->sender_id!=0 && $item->level_id!="" )?'Registered User':''; ?>
				<?php echo ($item->sender_id==0 && $item->level_id!="" )?'Registered User':''; ?>
				<?php echo ($item->sender_id!=0 && $item->level_id=="" )?'User Invitation':''; ?>
				</td>
	  <?php }else{ ?>
	  
			<td><input type='checkbox' class='checkbox' value="<?php echo $item->userinvitations_id?>"></td>
			<td><?php echo $item->userinvitations_id ?></td>	
			<td class="admin_table_bold"><?php echo $item->code ?> </td>
			<td class="admin_table_bold"><input type="text" name="name" id="name_<?php echo  $item->userinvitations_id; ?>" value="<?php echo $item->name ?>" /> </td>
			<td><input type="text" name="email" id="email_<?php echo  $item->userinvitations_id; ?>" value="<?php echo $item->email ?>" /></td>
			<td><?php if($item->status==1){echo 'Send';}else{echo "Not send";} ?></td>
			<td><?php echo ($item->sender_id==0)?'Admin':'User'; ?></td>
			<td><?php echo ($item->send_on!='0000-00-00 00:00:00')? date('F d, Y', strtotime($item->send_on)):'N/A';  ?></td>
			<td class="admin_table_options">
			  <?php //echo $this->htmlLink(array('action' => 'edit', 'id' => $item->getIdentity(), 'reset' => false),		$this->translate('edit')) ?>
				
			  <?php echo $this->htmlLink(
				array('action' => 'delete', 'id' => $item->getIdentity(), 'reset' => false),
				$this->translate('delete')) ?>
				| <a href="javascript:;" class="send-mail" id="<?php echo  $item->userinvitations_id; ?>" >send-mail</a>
			</td>
			
	  <?php } ?>
		
      </tr>
	  
      <?php endforeach; ?>
    </tbody>
  </table>

<br/>
<div class='buttons'>
  <button onclick="javascript:delectSelected();" type='submit'>
    <?php echo $this->translate("Delete Selected") ?>
  </button>
</div>
<form id='delete_selected' method='post' action='<?php echo $this->url(array('action' =>'deleteselected')) ?>'>
  <input type="hidden" id="ids" name="ids" value=""/>
</form>
<?php else:?>
  <div class="tip">
    <span>
      <?php echo $this->translate("There are currently no User Invitations.") ?>
    </span>
  </div>
<?php endif; ?>