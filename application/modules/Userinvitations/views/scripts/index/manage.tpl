<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: manage.tpl 9987 2013-03-20 00:58:10Z john $
 * @author     Jung
 */ 
 $this->headScript()	
    ->appendFile($this->baseUrl() . '/application/modules/User/externals/js/min.js')
	->appendFile($this->baseUrl() . '/application/modules/User/externals/assets/js/jquery-ui.min.js')
	   
?>
<script type="text/javascript">
	
	
	function chkdate(){
	jQuery('.com_form_error_validatin').addClass("com_form_error_validatin");
	if(jQuery.trim(jQuery('#title').val()) == ''){
			jQuery('#title').focus();
			jQuery('#title').addClass("com_form_error_validatin");
			//alert('Please select category.');
			return false;
		}
	if(jQuery.trim(jQuery('#start_date').val()) == ''){
			jQuery('#start_date').focus();
			jQuery('#start_date').addClass("com_form_error_validatin");
			//alert('Please select category.');
			return false;
		}
	if(jQuery.trim(jQuery('#end_date').val()) == ''){
			jQuery('#end_date').focus();
			jQuery('#end_date').addClass("com_form_error_validatin");
			//alert('Please select category.');
			return false;
		}
	if(parseInt(fn_DateCompare(jQuery.trim(jQuery('#start_date').val()), jQuery.trim(jQuery('#end_date').val()))) == 1){
			jQuery('#end_date').focus();
			jQuery('#end_date').addClass("com_form_error_validatin");
			//alert('Start date can not be greater than end date.');
			jQuery('#end_date').val('');
			return false;
		}
		
	jQuery('#pinposts_edit').submit();
}

	
  jQuery(document).on('click','a.send-mail',function(){
	
	
	var id=jQuery(this).attr('id');
	jQuery('#name_'+id).removeClass("com_form_error_validatin");	
	jQuery('#email_'+id).removeClass("com_form_error_validatin");	
	
	// if(jQuery.trim(jQuery('#name_'+id).val()) == ''){
			// jQuery('#name_'+id).focus();
			// jQuery('#name_'+id).addClass("com_form_error_validatin");			
			// return false;
	// }
	
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;	
	
	if(jQuery.trim(jQuery('#email_'+id).val()) == '' || regex.test(jQuery('#email_'+id).val())==false){
			jQuery('#email_'+id).focus();
			jQuery('#email_'+id).addClass("com_form_error_validatin");			
			return false;
	}
	
	var name = jQuery('#name_'+id).val();
	var email = jQuery('#email_'+id).val();
	
	jQuery.post('<?php echo $this->baseUrl();?>/Userinvitations/index/sendmail/id/'+id,{
	row_id:id,
	name:name,
	email:email,
	},function(resp){	
		
		var data = JSON.parse(resp);
		if(data.status==0){		
			//window.location.href = '<?php echo $this->baseUrl();?>/Userinvitations/index/manage';	
			alert('We have send your e-mail successfully.');
			window.location.href = '<?php echo $this->baseUrl();?>/Userinvitations/index/manage';	
			return false;
			
		}else{
			alert(data.msg);
		return false;
		
		}
	
	
		
		});
	
	});
</script>
<div class="layout_page_user_job_index">
<div class="generic_layout_container layout_top">
	<div class="generic_layout_container layout_middle">
		<div class="generic_layout_container layout_classified_browse_menu">
				<div class="headline">
				  <h2>Manage User Invitations  </h2>
				</div>
		</div>
	</div>
</div>
<div class="generic_layout_container layout_main">
<div class="Userinvitations_middle">	
<div class="generic_layout_container layout_core_content">
  <?php if (($this->current_count >= $this->quota) && !empty($this->quota)):?>
    <div class="tip">
      <span>
        <?php echo $this->translate('You have already created the maximum number of listings allowed. If you would like to create a new listing, please delete an old one first.');?>
      </span>
    </div>
    <br/>
  <?php endif; ?>
  <?php if( count($this->projects) > 0 ): 
    echo '<div id="accordion" style="margin-top:12px">'; 
	?>
	
	  <table class="manage_jp">
      <tr>
      	<th scope="col">Code</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Sent On</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
      </tr>
	<?php

   foreach($this->projects as $project):
	
  ?> 
  <?php if( count($project) > 0 ){ ?>  
  
      <?php if($project['user_id']!=""){ ?>
	  
	  <tr>	  
      	<td><?php	echo $project['code'];    ?></td>
      	<td> <?php	echo $project['name'];    ?></td>
      	<td><?php	echo $project['email'];    ?></td>
      	<td>		
		<?php echo ($project['send_on']!='0000-00-00 00:00:00')? date('F d, Y', strtotime($project['send_on'])):'N/A'; ?>
		</td>
      	<td><?php	echo ($project['status']==1)?'Sent':'Not Sent';    ?></td>      
        <td>           
            Registered User
         </td>
      </tr>	 
	  <?php }else{ ?>
	  
	  
	  <tr>	  
		 <td><?php	echo $project['code'];    ?></td>
		 <td> <input type="text" id="name_<?php echo $project['userinvitations_id']; ?>" name="name_<?php echo $project['userinvitations_id']; ?>" value="<?php echo $project['name']; ?>" /> </td>
      	<td><input type="text" id="email_<?php echo $project['userinvitations_id']; ?>"  name="email_<?php echo $project['userinvitations_id']; ?>" value="<?php echo $project['email']; ?>" /> </td>
      	<td><?php echo ($project['send_on']!='0000-00-00 00:00:00')? date('F d, Y', strtotime($project['send_on'])):'N/A'; ?></td>
      	<td><?php	echo ($project['status']==1)?'Sent':'Not Sent';     ?></td>      
        <td>           
            <a href="javascript:;" class="send-mail" id="<?php echo $project['userinvitations_id'];  ?>" ><?php	echo ($project['status']==1)?'Re-Send':'Send-mail';?></a>
         </td>
      </tr>	 
	  
	  
	  <?php } ?>
	  
	  
	 <?php } else { ?>
	 <p>No Invitations  </p>
	 <?php } ?>  
        <?php endforeach; 
		
		echo '</table>	';
        echo '</div>';
        else:
        echo '<div class="tip">
     			 	<span id="no-experience">No Invitations. </span>
 				 </div>';
		endif; ?>
</div>
</div>
</div>
</div>