<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Core.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Userinvitations_Api_Core extends Core_Api_Abstract
{
  public function getPaginator($params = array())
  {
    return Zend_Paginator::factory($this->getSelect($params));
  }

  public function getSelect($params = array())
  {
    $table = Engine_Api::_()->getDbtable('userinvitations', 'userinvitations');
	$tName = $table->info('name');
	$usertable = Engine_Api::_()->getDbTable('users', 'user');
    $uName = $usertable->info('name');
	// $user2table = Engine_Api::_()->getDbTable('users', 'user');
    // $u2Name = $user2table->info('name');
	
    $select = $table->select()
	  ->setIntegrityCheck(false)
	  ->from($tName) 
	  ->joinLeft($uName, "$uName.user_id = $tName.user_id", array("displayname","level_id"))
	  //->joinLeft($u2Name, "$u2Name.user_id = $tName.sender_id", array("displayname as sender"))
      ->order( !empty($params['orderby']) ? $params['orderby'].' '.$params['orderby_direction'] : 'userinvitations_id DESC' );

    $select->limit(10);

    return $select;
  }
}