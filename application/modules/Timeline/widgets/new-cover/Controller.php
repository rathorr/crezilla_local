<?php
/**
 * user, page - взять готовое
 * groups, events, offers - хранить cover photo в фотках самих плагинов
 * rest - создавать альбом type_cover_photos
 *
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Timeline
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Controller.php 2012-02-01 16:58:20 mt.uulu $
 * @author     Mirlan
 */

/**
 * @category   Application_Extensions
 * @package    Timeline
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */
class Timeline_Widget_NewCoverController extends Engine_Content_Widget_Abstract
{
    public function indexAction()
    {

        if (!Engine_Api::_()->core()->hasSubject()) {
            return $this->setNoRender();
        }

        $element = $this->getElement();
        $element->clearDecorators()
            //->addDecorator('Children', array('placement' => 'APPEND'))
            ->addDecorator('Container');

        $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->subject = $subject = Engine_Api::_()->core()->getSubject();
        
        if (Engine_Api::_()->getDbTable('modules', 'core')->isModuleEnabled('like')) {
            $this->view->likeEnabled = $likeEnabled = (bool)(Engine_Api::_()->like()->isAllowed($subject));
            $this->view->liked = Engine_Api::_()->like()->isLike($subject);
        }
		
		
		
        // Set up element
        $element = $this->getElement();
        $element->clearDecorators();
		
		

        $action_id = (int)Zend_Controller_Front::getInstance()->getRequest()->getParam('action_id');
        $activeTab = $action_id ? 'activity.feed' : $this->_getParam('tab');

        if (empty($activeTab)) {
            $activeTab = Zend_Controller_Front::getInstance()->getRequest()->getParam('tab');
        }

        $tabs = Engine_Api::_()->timeline()->getWidgetTabs($element, $activeTab);

        $this->view->activeTab = $activeTab;
        $this->view->tabs = $tabs;
        $this->view->max = $this->_getParam('max');


        $settings = Engine_Api::_()->getDbTable('settings', 'core');
        $this->view->item_id = $id = $subject->getIdentity();
        $this->view->item_type = $type = $subject->getType();
		
		
		$friends = Engine_Api::_()->getDbtable('friendship', 'user')->pageFollowed(Engine_Api::_()->user()->getViewer()->getIdentity(),$id);
		
		 $friendsFollowing = Engine_Api::_()->getDbtable('friendship', 'user')->pageFollowingTotal($id);
		 
 		$this->view->followTotal =$friendsFollowing[0]['totalfollowing'];
		$this->view->follow=0;
 		 if(count($friends)>0)
		 {
			 $this->view->follow = 1;
			 
		 }
		
		 


      if(Engine_Api::_()->getDbTable('modules', 'core')->isModuleEnabled('hegift')){

          $viewer = Engine_Api::_()->user()->getViewer();

            if( !Engine_Api::_()->core()->hasSubject('user') ) {
              $subject = $viewer;
            } else {
              $subject = Engine_Api::_()->core()->getSubject('user');
            }

            $this->view->user = $subject;
			

            /**
             * @var $settings User_Model_DbTable_Settings
             * @var $recipient Hegift_Model_Recipient
             */

            $settings = Engine_Api::_()->getDbTable('settings', 'user');
            $this->view->recipient_id = $recipient_id = $settings->getSetting($subject, 'active_gift');
            $this->view->recipient = $recipient = Engine_Api::_()->getItem('hegift_recipient', $recipient_id);

            if ($recipient !== null) {
              $this->view->from = Engine_Api::_()->getItem('user', $recipient->subject_id);
              $this->view->message = nl2br($recipient->message);
              $this->view->privacy = $recipient->getPrivacyForUser($viewer);
              $this->view->gift = Engine_Api::_()->getItem('gift', $recipient->gift_id);
            }

            $this->view->storage = Engine_Api::_()->storage();
      }
	  
	  
	 

        /*$setting_position = Engine_Api::_()->timeline()->getCoverPhotoPositionSetting($id, $type);

        $position = $settings->getSetting($setting_position);
        $position = ($position) ? unserialize($position) : array('top' => 0, 'left' => 0);
        $this->view->position = json_encode($position);*/


        /*if ($subject->getType() == 'user') {
            $table = Engine_Api::_()->getDbTable('settings', 'core');
            $table2 = Engine_Api::_()->getDbTable('settings', 'hecore');
            $oldPos1 = $table2->getSetting($subject, 'timeline-cover-position');
            $oldPos2 = $table2->getSetting($subject, 'timeline-born-position');

            if (isset($subject->cover_id) && $subject->cover_id) {
                $photo_id = $subject->cover_id;

                $photo = Engine_Api::_()->getItem('album_photo', $photo_id);
                if ($photo) {
                    $cover_parent = 'subject';
                    if ($photo->getType() == 'pagealbumphoto') {
                        $cover_parent = 'pagealbum';
                    }
                    if ($photo->getType() == 'album_photo') {
                        $cover_parent = 'album';
                    }
                    $setting_name = Engine_Api::_()->timeline()->getCoverPhotoSetting($subject->getIdentity(), $subject->getType(), 'cover');
                    $cover_parent_setting_name = Engine_Api::_()->timeline()->getCoverParentSetting($subject->getIdentity(), $subject->getType(), 'cover');
                    $position_setting_name = Engine_Api::_()->timeline()->getCoverPhotoPositionSetting($subject->getIdentity(), $subject->getType(), 'cover');
                    $table->setSetting($cover_parent_setting_name, $cover_parent);
                    $table->setSetting($setting_name, $photo_id);
                    $table->setSetting($position_setting_name, serialize($oldPos1));
                }

                $subject->cover_id = 0;
                $subject->save();
            }

            if (isset($subject->born_id) && $subject->born_id) {
                $photo_id = $subject->born_id;

                $photo = Engine_Api::_()->getItem('album_photo', $photo_id);

                if ($photo) {

                    $cover_parent = 'subject';
                    if ($photo->getType() == 'pagealbumphoto') {
                        $cover_parent = 'pagealbum';
                    }
                    if ($photo->getType() == 'album_photo') {
                        $cover_parent = 'album';
                    }
                    $setting_name = Engine_Api::_()->timeline()->getCoverPhotoSetting($subject->getIdentity(), $subject->getType(), 'born');
                    $cover_parent_setting_name = Engine_Api::_()->timeline()->getCoverParentSetting($subject->getIdentity(), $subject->getType(), 'born');
                    $position_setting_name = Engine_Api::_()->timeline()->getCoverPhotoPositionSetting($subject->getIdentity(), $subject->getType(), 'born');
                    $table->setSetting($cover_parent_setting_name, $cover_parent);
                    $table->setSetting($setting_name, $photo_id);
                    $table->setSetting($position_setting_name, serialize($oldPos2));
                }

                $subject->born_id = 0;
                $subject->save();
            }
        }*/


        try {

            $this->view->profilePhoto = Engine_Api::_()->timeline()->getProfilePhoto($subject, $type, $id);
        } catch(Exception $e) {

        }
        try {

            $this->view->coverPhoto = Engine_Api::_()->timeline()->getTimelinePhoto($id, $type, 'cover');
        } catch(Exception $e) {

        }

        /*$albumPhoto = Engine_Api::_()->timeline()->getTimelinePhotoObject($id, $type, 'cover');

        if (is_object($albumPhoto)) {
            $albumPhoto = $albumPhoto->getHref();
        }
        if (!strlen(trim($albumPhoto))) {
            $albumPhoto = 'javascript://';
        }

        $settings = Engine_Api::_()->getDbTable('settings', 'core');

        if(!$settings->getSetting('photoviewer.enable')) {
            $albumPhoto = 'javascript://';
        }
        $this->view->albumPhoto = $albumPhoto;
        $this->view->coverPhoto = Engine_Api::_()->timeline()->getTimelinePhoto($id, $type, 'cover');

        if (!$this->view->coverPhoto) {
            $this->view->coverScreen = true;
            $this->view->coverPhoto = 'application/modules/Timeline/externals/images/default_cover.jpg';
            $this->view->position = json_encode(array('top' => 0, 'left' => 0));
        }*/

        $this->view->isAlbumEnabled = Engine_Api::_()->getDbTable('modules', 'core')->isModuleEnabled('album');

        if($viewer->getIdentity()) {
            if ($type == 'page') {
                $this->view->canEdit = ($subject->user_id == $viewer->getIdentity() || $viewer->level_id < 4);
            } else {
                $this->view->canEdit = $subject->authorization()->isAllowed($viewer, 'edit');
            }
        }


        $this->view->profile_options = Engine_Api::_()->timeline()->getProfileOptions($type);

        $check_photoviewer = Engine_Api::_()->getDbTable('modules', 'core');
        $select = $check_photoviewer->select()
            ->where('name = ?', 'photoviewer')
            ->where('enabled = ?', 1);
        $viewer_photo = $check_photoviewer->fetchRow($select);

        if ($viewer_photo->enabled == 1) {
            $this->view->photoviewer = 1;
        } else {
            $this->view->photoviewer = 0;
        }

        $this->view->allowFromAlbums = array('user', 'page');

        $settings = Engine_Api::_()->getApi('settings', 'core');
        $this->view->menuitems = $settings->__get('timeline.menuitems', 20);

        switch ($type) {
            case 'user':
                if ($subject->status) {
                    $this->view->subjectAdditionalInfo = $this->view->viewMore($subject->status, 60);
                    if ($subject->getIdentity() == $viewer->getidentity()) {
                        $this->view->subjectAdditionalInfo .= '<a class="profile_status_clear" href="javascript:void(0);" onclick="document.tl_cover.clearStatus();">(' . $this->view->translate('clear') . ')</a>';
                    }
                }
                break;
            case
            'page':
                $this->view->subjectAdditionalInfo = false;
                $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);
                foreach($fieldStructure as $s) {
                    if($s->option_id) {
                        $opt = $s->getOption();
                        $this->view->subjectAdditionalInfo = $opt->label;
                        break;
                    }
                }
                break;
            default:
                $this->view->subjectAdditionalInfo = false;
        }

    }
}
