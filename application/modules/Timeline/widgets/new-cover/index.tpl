<?php
$this->headScript()->appendFile($this->baseUrl() . '/application/modules/Timeline/externals/scripts/cover.js');
?>
<style type="text/css">
.timeline-profile-tabs .cover-edit{display:block !important;}
</style>
<?php if ($this->canEdit): ?>
    <script type="text/javascript">
        document.tl_cover = new TimelineCover();
        document.tl_cover.setOptions({
            element_id: 'cover-container-img',
            save_button: 'tl-cover-save-button',
            edit_buttons: 'tl-cover-edit-group',
            loader_id: 'tl-cover-loader',
            is_allowed: true,
            cover_url: '<?php echo $this->url(array('action' => 'get', 'item_type' => $this->item_type, 'item_id' => $this->subject()->getIdentity()), 'timeline_photo', true); ?>',
            position_url: '<?php echo $this->url(array('action' => 'position', 'type'=>'cover', 'item_type' => $this->item_type, 'item_id' => $this->subject()->getIdentity()), 'timeline_photo', true); ?>',
            imgSrc: '<?php echo $this->coverPhoto['photoSrc']; ?>'
        });
        try {
            document.tl_cover.position = JSON.parse('<?php echo $this->coverPhoto['position']; ?>');
        } catch(e) {
            console.log(e);
        }

        en4.core.runonce.add(function () {
            document.tl_cover.init();
            document.tl_cover.options.cover_width = document.tl_cover.get().getParent().getWidth();
        });


    </script>
<?php endif; ?>

<div class="timeline-profile-tabs">
<div class="profile-container he-thumbnail">
<div class="cover-container">

    <!--<a href="<?php echo ($this->coverPhoto['photoHref']); ?>" id="cover-container" class="cover">
        <img id="cover-container-img" src="<?php echo $this->coverPhoto['photoSrc']; ?>"/>
    </a>-->
    
    <a href="javascript:void(0);" id="cover-container" class="cover" style="cursor:default;">
       <span style="background-image:url(<?php echo $this->coverPhoto['photoSrc']; ?>);" id="cover-container-img" class="wall_background"></span>
        <!--<img id="cover-container-img" src="<?php echo $this->coverPhoto['photoSrc']; ?>"/>-->
    </a>

    <?php if ($this->canEdit): ?>
        <div class="cover-edit he-button-group">
            <a id="tl-cover-save-button" class="he-btn wp_init" type="button" style="display: none;">
                Save Position
            </a>

            <div id="tl-cover-edit-group" class="he-btn-group" style="display: inline-block;">
                <!--<a data-toggle="dropdown" class="he-btn he-dropdown-toggle wp_init" type="button">
                    <?php echo $this->translate('TIMELINE_Edit Cover'); ?> <i class="he-caret"></i>
                </a>-->
                <?php echo $this->htmlLink(array(
                                'route' => 'timeline_photo',
                                'action' => 'upload',
                                'item_type' => $this->item_type,
                                'type' => 'cover',
                                'item_id' => $this->subject()->getIdentity(),
                                'reset' => true
                            ),
                            $this->translate('TIMELINE_Edit Cover'),
                            array(
                                'class' => 'cover-albums smoothbox he-btn he-dropdown-toggle wp_init',
                            )); ?>
                <!--<ul class="he-dropdown-menu">
                    <?php if (in_array($this->item_type, $this->allowFromAlbums)): ?>
                        <?php if ($this->isAlbumEnabled): ?>
                            <li><?php echo $this->htmlLink(array(
                                        'route' => 'timeline_photo',
                                        'item_type' => $this->item_type,
                                        'item_id' => $this->subject()->getIdentity(),
                                        'type' => 'cover',
                                        'reset' => true
                                    ),
                                    $this->translate('TIMELINE_Choose from Photos...'),
                                    array(
                                        'class' => 'cover-albums smoothbox',
                                    )); ?>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>
                    <li>
                        <?php echo $this->htmlLink(array(
                                'route' => 'timeline_photo',
                                'action' => 'upload',
                                'item_type' => $this->item_type,
                                'type' => 'cover',
                                'item_id' => $this->subject()->getIdentity(),
                                'reset' => true
                            ),
                            $this->translate('TIMELINE_Upload Photo...'),
                            array(
                                'class' => 'cover-albums smoothbox',
                            )); ?>
                    </li>
                    <?php if (!$this->coverScreen): ?>
                        <li><?php echo $this->htmlLink(
                                'javascript:document.tl_cover.reposition.start()',
                                $this->translate('TIMELINE_Reposition...'),
                                array('class' => 'cover-reposition')); ?>
                        </li>
                        <li><?php echo $this->htmlLink(array(
                                    'route' => 'timeline_photo',
                                    'action' => 'remove',
                                    'type' => 'cover',
                                    'item_type' => $this->item_type,
                                    'item_id' => $this->subject()->getIdentity(),
                                ),
                                $this->translate('TIMELINE_Remove...'), array(
                                    'class' => 'cover-remove smoothbox')); ?>
                        </li>
                    <?php endif; ?>
                </ul>-->
            </div>
        </div>
    <?php endif; ?>
    <?php

    ?>
    
    <?php $img	=	 ($this->subject()->getPhotoUrl()!='')?$this->subject()->getPhotoUrl():$this->baseUrl().'/application/modules/Page/externals/images/nophoto_page_thumb_normal.png';?>
    <a href="<?php echo $this->subject()->getHref(); ?>" id="profile-photo"
       class="profile-photo he-thumbnail"
       style="background-image: url('<?php echo $img;?>'); background-position: 50% 50%; background-size: cover;">
     

        <?php if($this->item_type == 'page'): ?>
            <?php if( $this->subject()->featured ) :?>
                <div class="page_featured" >
                    <span><?php echo $this->translate('Featured')?></span>
                </div>
            <?php endif;?>
            <?php if( $this->subject()->sponsored ) :?>
                <div class="sponsored_page"><?php echo $this->translate('SPONSORED')?></div>
            <?php endif;?>
        <?php endif;?>

    </a>
   <h2><?php echo $this->subject()->getTitle(); ?></h2>


    <div class="title-n-options"> <!--Title And option Buttons Container-->
        <div class="title-n-category"> <!--Title & Category-->
           

            <!--<?php if($this->subjectAdditionalInfo): ?>
                <div class="subject-additional-info" id="subject-additional-info">
                    <?php echo $this->subjectAdditionalInfo; ?>
                </div>
            <?php endif; ?>-->

            <?php if ($this->item_type != 'page'): ?>
                <div class="cover-rate">
                    <?php echo $this->content()->renderWidget('rate.widget-rate'); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="option-buttons">
        
          <?php if ($this->follow==1): ?>
        <span class="following_txt"> Following: <b><?php echo $this->followTotal;?></b>   </span>
        <a class="smoothbox show_friends proj_share" href="<?php echo $this->baseUrl()."/classified/index/get-unfollow-page/action_id/".$this->subject->getIdentity();?>">Unfollow </a>
        
           <?php else: ?>
        <span  class="following_txt"> Following: <b><?php echo $this->followTotal;?></b>   </span>
        <a class="smoothbox show_friends proj_share" href="<?php echo $this->baseUrl()."/classified/index/get-follow-page/action_id/".$this->subject->getIdentity();?>">Follow </a>
          <?php endif; ?>
            <?php if ($this->likeEnabled): ?>
                <?php if ($this->liked): ?>
                    <a class="he-btn he-btn-unlike"
                       data-like="0"
                       onclick="document.tl_core.likeSubject(this, '<?php echo $this->subject->getIdentity(); ?>', '<?php echo $this->subject->getType(); ?>');">
                        <span class="he-glyphicon he-glyphicon-thumbs-down"></span>
                        <span id="tl-like-btn-text"><?php echo $this->translate('like_Unlike'); ?></span>
                    </a>
                <?php else: ?>
                    <a class="he-btn he-btn-like"
                       data-like="1"
                       onclick="document.tl_core.likeSubject(this, '<?php echo $this->subject->getIdentity(); ?>', '<?php echo $this->subject->getType(); ?>');">
                        <span class="he-glyphicon he-glyphicon-thumbs-up"></span>
                        <span id="tl-like-btn-text"><?php echo $this->translate('like_Like'); ?></span>
                    </a>
                <?php endif; ?>
            <?php endif; ?>
            
            <a class="he-btn smoothbox menu_page_profile page_profile_share wp_init" href="/IPRODUCER/activity/index/share/type/page/id/381/format/smoothbox">Share Page</a>
            
        </div>
    </div>
</div>

<div class="tab-buttons"><!--Tab Button Cantainer-->
    <ul class="he-nav he-nav-tabs"> <!--Tabs-->
        <li id='time_line_widget' style="display: none">
            <a href="#tab-timeline">
             <?php echo   $this->translate('Recent Updates')?>
            </a>
        </li>
        <?php $cnt = 0; $last = 0;
        $isPage = ($this->item_type == 'page') ? true : false;
        foreach ($this->tabs as $tab): ?>
            <?php if(!$tab['childCount'] && !$isPage && $tab['childCount'] != -2) continue; $last++;?>
            <?php $tabCnt = $tab['childCount']; ?>
            <?php if ($cnt < $this->menuitems): ?>
                <li <?php if ($cnt == 0) {
                    echo 'class="he-active"';
                } ?>>
                    <a id="tab-<?php echo $tab['id']; ?>" href="#tab-<?php echo $tab['id']; ?>" <?php if ($cnt == 0) {
                        echo 'class="he-active"';
                        echo 'id="timeLine-active"';
                    } ?>>
                        <?php echo $this->translate($tab['title']) ?> <?php echo ($tabCnt && $tabCnt > 0) ? ' (' . $tabCnt . ')' : ''; ?>
                    </a>
                </li>
                <?php $cnt++; endif; ?>
        <?php endforeach; ?>

        <?php if ($last > $cnt): ?>
            <li class="he-dropdown">
                <a data-toggle="dropdown" class="he-dropdown-toggle" id="myTabDrop1"
                   href="javascript://">More
                    <b class="he-caret"></b></a>
                <ul aria-labelledby="myTabDrop1" role="menu" class="he-dropdown-menu">
                    <?php for ($i = $cnt; $i < count($this->tabs); $i++): ?>
                        <?php $tab = $this->tabs[$i]; ?>
                        <?php if(!$tab['childCount'] && !$isPage && $tab['childCount'] != -2) continue;?>
                        <?php $tabCnt = $tab['childCount']; ?>
                        <li><a data-toggle="tab" tabindex="-1"
                               href="#tab-<?php echo $tab['id']; ?>">
                                <?php echo $this->translate($tab['title']) ?> <?php echo ($tabCnt && $tabCnt > 0) ? ' (' . $tabCnt . ')' : ''; ?>
                            </a></li>
                    <?php endfor; ?>
                </ul>
            </li>
        <?php endif; ?>
    </ul>
</div>
</div>
<div class="he-tab-content">
    <?php $cnt = 0;
    foreach ($this->tabs as $tab): ?>
        <div id="#tab-<?php echo $tab['id']; ?>"
             class="he-tab-pane <?php if ($cnt == 0) echo ' active_tabInTimeLine' ?>"
            <?php if ($cnt == 0) echo 'style="display:block" '; else echo 'style="display:none"'; ?> >
            <div>
                <?php echo $tab['content']; ?>
            </div>
        </div>
        <?php $cnt++; endforeach; ?>
    <div id="#tab-timeline" class="he-tab-pane he-row he-show-grid">

    </div>
</div>

</div>
