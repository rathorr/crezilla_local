<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: AdminManageController.php 9837 2012-11-29 01:12:35Z matthew $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Pinpost_AdminManageController extends Core_Controller_Action_Admin
{
  public function indexAction()
  {
    $this->view->formFilter = $formFilter = new Pinpost_Form_Admin_Filter();
    $page = $this->_getParam('page', 1);

    if( $formFilter->isValid($this->_getAllParams()) ) {
      $values = $formFilter->getValues();
      $paginator = Engine_Api::_()->pinpost()->getPaginator($values);
      if ($values['orderby'] && $values['orderby_direction'] != 'DESC') {
        $this->view->orderby = $values['orderby'];
      }
    } else {
      $paginator = Engine_Api::_()->pinpost()->getPaginator();
    }
    
    $this->view->paginator = $paginator->setCurrentPageNumber( $page );
  }
  
  
  public function createAction()
  {
    $this->view->form = $form = new Pinpost_Form_Admin_Create();

    if( $this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost()) ) {
      $params = $form->getValues();
      $params['user_id'] 	= Engine_Api::_()->user()->getViewer()->getIdentity();
      $params['start_date']	= date('Y-m-d', strtotime($params['start_date']));
	  $params['end_date']	= date('Y-m-d', strtotime($params['end_date']));
      $pinpost = Engine_Api::_()->getDbtable('pinposts', 'pinpost')->createRow();
      $pinpost->setFromArray($params);
      $pinpost->save();

      return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
    }
  }

  public function editAction()
  {
    $id = $this->_getParam('id', null);
    $pinpost = Engine_Api::_()->getItem('pinpost', $id);
        
    //echo '<pre>';
    //var_dump($announcement['networks']);
    //echo '</pre>';
    //die('The End');
    
    $pinpost['start_date']	= date('Y-m-d', strtotime($pinpost['start_date']));
	$pinpost['end_date']	= date('Y-m-d', strtotime($pinpost['end_date']));
    $this->view->form = $form = new Pinpost_Form_Admin_Edit();
    $form->populate($pinpost->toArray());

    // Save values
    if( $this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost()) ) {
      $params = $form->getValues();
      $params['start_date']	= date('Y-m-d', strtotime($params['start_date']));
	  $params['end_date']	= date('Y-m-d', strtotime($params['end_date']));
      $pinpost->setFromArray($params);
      $pinpost->save();
      return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
    }
  }

  public function deleteAction()
  {
    //$this->view->form = $form = new Announcement_Form_Admin_Edit();
    $this->view->id = $id = $this->_getParam('id', null);
    $pinpost = Engine_Api::_()->getItem('pinpost', $id);

    // Save values
    if( $this->getRequest()->isPost() )
    {
      $pinpost->delete();
      return $this->_helper->redirector->gotoRoute(array('action' => 'index'));
    }
  }

  public function deleteselectedAction()
  {
    //$this->view->form = $form = new Announcement_Form_Admin_Edit();
    $this->view->ids = $ids = $this->_getParam('ids', null);
    $confirm = $this->_getParam('confirm', false);
    $this->view->count = count(explode(",", $ids));

    //$announcement = Engine_Api::_()->getItem('announcement', $id);

    // Save values
    if( $this->getRequest()->isPost() && $confirm == true )
    {
      $ids_array = explode(",", $ids);
      foreach( $ids_array as $id ){
        $pinpost = Engine_Api::_()->getItem('pinpost', $id);
        if( $pinpost ) $pinpost->delete();
      }
      
      $this->_helper->redirector->gotoRoute(array('action' => 'index'));
    }
  }
  
  public function changepoststatusAction(){
	  	$this->_helper->layout()->disableLayout(); 
		$row_id		=	$_POST['row_id'];
		
		$table = Engine_Api::_()->getDbtable('pinposts', 'pinpost');
	 	 foreach( $table->fetchAll($table->select()) as $pinpost )
	 	 {
			$pinpost->active = 0;
			$pinpost->save();
	  	}
		  
		$pinpost = Engine_Api::_()->getItem('pinpost', $row_id);
		$pinpost->active = 1;
		$pinpost->save();
		die;
	}
}