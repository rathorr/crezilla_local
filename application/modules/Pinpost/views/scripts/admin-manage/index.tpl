<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 10051 2013-06-11 23:36:56Z jung $
 * @author     Sami
 */
?>
<script type="text/javascript">

  en4.core.runonce.add(function() {
    $$('th.admin_table_short input[type=checkbox]').addEvent('click', function(event) {
      var el = $(event.target);
      $$('input[type=checkbox]').set('checked', el.get('checked'));
    });
  });

  var changeOrder =function(orderby, direction){
    $('orderby').value = orderby;
    $('orderby_direction').value = direction;
    $('filter_form').submit();
  }

  var delectSelected =function(){
    var checkboxes = $$('input[type=checkbox]');
    var selecteditems = [];

    checkboxes.each(function(item, index){
      var checked = item.get('checked');
      var value = item.get('value');
      if (checked == true && value != 'on'){
        selecteditems.push(value);
      }
    });

    $('ids').value = selecteditems;
    $('delete_selected').submit();
  }
  
  jQuery(document).on('click','.active_post',function(){
	jQuery('.active_post').each(function(){
			jQuery(this).removeAttr('checked');	
	});
	
	jQuery(this).prop('checked',true);
	jQuery.post('<?php echo $this->baseUrl();?>/admin/pinpost/manage/changepoststatus',{row_id:jQuery.trim(jQuery(this).val())},function(resp){});
	
});

</script>

<h2><?php echo $this->translate('Manage Pinposts') ?></h2>



<?php echo $this->formFilter->render($this) ?>

<br />

<div>
  <?php echo $this->htmlLink(array('action' => 'create', 'reset' => false), 
    $this->translate("Post New Pinpost"),
    array(
      'class' => 'buttonlink',
      'style' => 'background-image: url(' . $this->layout()->staticBaseUrl . 'application/modules/Pinpost/externals/images/admin/add.png);')) ?>
  <?php if($this->paginator->getTotalItemCount()!=0): ?>
    <?php echo $this->translate('%d pinposts total', $this->paginator->getTotalItemCount()) ?>
  <?php endif;?>
  <?php echo $this->paginationControl($this->paginator); ?>
</div>

<br />

<?php if( count($this->paginator) ): ?>
  <table class='admin_table'>
    <thead>
      <tr>
        <th style="width: 1%;" class="admin_table_short"><input type='checkbox' class='checkbox'></th>
        <th style="width: 1%;"><a href="javascript:void(0);" onclick="javascript:changeOrder('pinpost_id', '<?php if($this->orderby == 'pinpost_id') echo "DESC"; else echo "ASC"; ?>');">
          <?php echo $this->translate("ID") ?>
        </a></th>
        <th style="width: 50%;"><a href="javascript:void(0);" onclick="javascript:changeOrder('title', '<?php if($this->orderby == 'title') echo "DESC"; else echo "ASC"; ?>');">
          <?php echo $this->translate("Title") ?>
        </a></th>
        <th style="width: 10%;"><a href="javascript:void(0);" onclick="javascript:changeOrder('start_date', '<?php if($this->orderby == 'start_date') echo "DESC"; else echo "ASC"; ?>');">
          <?php echo $this->translate("Start Date") ?>
        </a></th>
        <th style="width: 10%;"><a href="javascript:void(0);" onclick="javascript:changeOrder('end_date', '<?php if($this->orderby == 'end_date') echo "DESC"; else echo "ASC"; ?>');">
          <?php echo $this->translate("End Date") ?>
        </a></th>
        <th style="width: 10%;"><?php echo $this->translate("Active") ?></th>
        <th style="width: 15%;"><a href="javascript:void(0);" onclick="javascript:changeOrder('creation_date', '<?php if($this->orderby == 'creation_date') echo "DESC"; else echo "ASC"; ?>');">
          <?php echo $this->translate("Date") ?>
        </a></th>
        <th style="width: 15%;">
          <?php echo $this->translate("Options") ?>
        </th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($this->paginator as $item): ?>
      <tr>
        <td><input type='checkbox' class='checkbox' value="<?php echo $item->pinpost_id?>"></td>
        <td><?php echo $item->pinpost_id ?></td>
        <td class="admin_table_bold"><?php echo $item->title ?></td>
       <td><?php echo date('m/d/Y', strtotime($item->start_date)); ?></td>
       <td><?php echo date('m/d/Y', strtotime($item->end_date)); ?></td>
       
       <td><input type="checkbox" value="<?php echo $item->pinpost_id;?>" class="active_post"   <?php if($item->active==1){echo 'checked="checked"';} ?>/></td>
        
        <td><?php echo $this->locale()->toDateTime( $item->creation_date ) ?></td>
        <td class="admin_table_options">
          <?php echo $this->htmlLink(
            array('action' => 'edit', 'id' => $item->getIdentity(), 'reset' => false),
            $this->translate('edit')) ?> |
          <?php echo $this->htmlLink(
            array('action' => 'delete', 'id' => $item->getIdentity(), 'reset' => false),
            $this->translate('delete')) ?>
        </td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<br/>
<div class='buttons'>
  <button onclick="javascript:delectSelected();" type='submit'>
    <?php echo $this->translate("Delete Selected") ?>
  </button>
</div>

<form id='delete_selected' method='post' action='<?php echo $this->url(array('action' =>'deleteselected')) ?>'>
  <input type="hidden" id="ids" name="ids" value=""/>
</form>

<?php else:?>

  <div class="tip">
    <span>
      <?php echo $this->translate("There are currently no pinposts.") ?>
    </span>
  </div>

<?php endif; ?>
