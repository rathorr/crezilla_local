<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Create.php 9837 2012-11-29 01:12:35Z matthew $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Pinpost_Form_Admin_Create extends Engine_Form
{
  public function init()
  {
    $this->setTitle('Create Pinpost')
      ->setDescription('Please compose your new pinpost below.')
      ->setAttrib('id', 'pinposts_create')
	  ->setAttrib('class', 'global_form');     
   
    // Add title
    $this->addElement('Text', 'title', array(
      'label' => 'Heading*',
      'required' => true,
	  'placeholder'	=>	'Enter Heading',
      'allowEmpty' => false,
    ));
	
	$this->addElement('Text', 'start_date', array(
      'label' => 'Start Date*',
      'required' => true,
	  'class'	=>	'datepicker',
	  'placeholder'	=>	'Enter Start Date',
      'allowEmpty' => false,
    ));
	
	$this->addElement('Text', 'end_date', array(
      'label' => 'End Date*',
      'required' => true,
	  'class'	=>	'datepicker',
	  'placeholder'	=>	'Enter End Date',
      'allowEmpty' => false,
    ));
    
    $this->addElement('Textarea', 'body', array(
      'label' => 'Body*',
      'required' => true,
	  'maxlength'	=>	'500',
      'editorOptions' => array(
        'bbcode' => true,
        'html' => true,
      ),
      'allowEmpty' => false,        
    ));

        // Buttons
    $this->addElement('Button', 'submit_post', array(
      'label' => 'Post',
      'type' => 'button',
      'onclick'=> 'chkdate()',
	  'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'ignore' => true,
      'link' => true,
      'href' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('module' => 'pinpost', 'controller' => 'manage', 'action' => 'index'), 'admin_default', true),
      'prependText' => Zend_Registry::get('Zend_Translate')->_(' or '),
      'decorators' => array(
        'ViewHelper',
      ),
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
}