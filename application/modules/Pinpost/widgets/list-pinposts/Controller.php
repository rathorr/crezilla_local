<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 10139 2014-03-11 13:49:08Z andres $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Announcement
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Pinpost_Widget_ListPinpostsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
     
    // Get Pinposts
    // Get paginator
    $table = Engine_Api::_()->getDbtable('pinposts', 'pinpost');
    $pinpost_select = $table->select()->order('creation_date DESC');
	$pinpost_select = $table->select()->where('active = ? ','1');
	$pinpost_select = $table->select()->where('UNIX_TIMESTAMP(start_date) <= ? ',time());
	$pinpost_select = $table->select()->where('UNIX_TIMESTAMP(end_date) >= ? ',time());
    $pinpost_query = $table->fetchAll($pinpost_select);
    
    $pinpost_keep_list = array();
    $pinpost_count = count($pinpost_query);
    for($i = 0; $i < $pinpost_count; $i++) {
		array_push($pinpost_keep_list, $pinpost_query[$i]);
    }
    
    $paginator = Zend_Paginator::factory($pinpost_keep_list);
    
    // Set item count per page and current page number
    $paginator->setItemCountPerPage($this->_getParam('itemCountPerPage', 1));
    $paginator->setCurrentPageNumber($this->_getParam('page', 1));   

    // Hide if nothing to show
    if( $paginator->getTotalItemCount() <= 0 ) {
      return $this->setNoRender();
    }
    
    $this->view->pinposts = $paginator;
  }
}
