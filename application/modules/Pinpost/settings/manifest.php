<?php return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'pinpost',
    'version' => '4.0.0',
    'path' => 'application/modules/Pinpost',
    'title' => 'Pinpost',
    'description' => 'Will show pin post on the use wall',
    'author' => '',
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Pinpost',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/pinpost.csv',
    ),
  ),
  // Hooks ---------------------------------------------------------------------
  'hooks' => array(
    array(
      'event' => 'onItemDeleteBefore',
      'resource' => 'Pinpost_Plugin_Core',
    ),
  ),
  // Items ---------------------------------------------------------------------
  'items' => array(
    'pinpost'
  ),
); ?>