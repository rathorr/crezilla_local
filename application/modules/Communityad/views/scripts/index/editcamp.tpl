<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions 
 * @package    Communityad
 * @copyright  Copyright 2009-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: editcamp.tpl  2011-02-16 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>

<style>

#global_content h2 {
    color: #3faee8;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 700;
    margin: 0;
    padding: 10px 20px;
    text-transform: capitalize;
}
.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:auto;
  width:100%;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}


ul.form-errors > li, ul.form-notices > li {
 float:none;
}

.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
h2.create_pdf
{margin:6px !important;}
h2.create_pdf a
{padding:10px !important;}
</style>

<div class="headline">
  <h2>
     
      <?php echo $this->translate('Advertising');?>
    
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
        echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
		
    ?>
  </div>
</div>

<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle" style="width:100%; background-color: #fff;
    padding: 1% 2%;">
<div class="generic_layout_container layout_core_content">
<form method="post" class="global_form">
  <div>
    <div>
      <h3><?php echo $this->translate("Edit Campaign Title") ?></h3>
      <br />
      <p>
        <input type="hidden" name="confirm" value="<?php echo $this->adcampaign_id ?>"/>

        <input type="text" name="name" maxlength="100" value= '<?php echo $this->camp_title ?>' style="background-color: #fafafa;  border: 1px solid #d8d8d8; border-radius: 5px; height: 36px;
    padding: 6px;width: 99%;" />
        <br /><br />

        <button type='submit' class="submit"><?php echo $this->translate("Save Title") ?></button>
        <?php echo Zend_Registry::get('Zend_Translate')->_(' or ') ?>
        <a href='<?php echo $this->url(array(), 'communityad_campaigns', true) ?>'>
          <?php echo $this->translate("cancel") ?></a>
      </p>
    </div>
  </div>
</form>
<?php if (@$this->closeSmoothbox): ?>
  <script type="text/javascript">
    TB_close();
  </script>            
<?php endif; ?>

</div>
</div>
</div>