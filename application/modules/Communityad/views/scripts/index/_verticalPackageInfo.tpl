<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sitepage
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: _verticalPackageInfo.tpl 2015-05-11 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<?php
$request = Zend_Controller_Front::getInstance()->getRequest();
$controller = $request->getControllerName();
$action = $request->getActionName();
?>

<li class="seaocore_package_vertical">
    <div class="fleft">
        <?php if (in_array('price', $packageInfoArray)): ?>
            <div class="contentblock_left_text highlightleft"><b><?php echo $this->translate("Price"); ?></b></div>
        <?php endif; ?>
        <?php if (in_array('quantity', $packageInfoArray)): ?>
            <div class="contentblock_left_text"><b><?php echo $this->translate("Quantity"); ?></b></div>
        <?php endif; ?>    
        <?php if (in_array('featured', $packageInfoArray)): ?>
            <div class="contentblock_left_text"><b><?php echo $this->translate("Featured"); ?></b></div>
        <?php endif; ?>
        <?php if (in_array('sponsored', $packageInfoArray)): ?>
            <div class="contentblock_left_text"><b><?php echo $this->translate("Sponsored"); ?></b></div>
        <?php endif; ?>
        <?php if (in_array('targeting', $packageInfoArray)): ?>
            <div class="contentblock_left_text"><b><?php echo $this->translate("Targeting"); ?></b></div>
        <?php endif; ?>
        <?php if (in_array('youcanadvertise', $packageInfoArray)): ?>
            <div class="contentblock_left_text"><b><?php echo $this->translate("You can advertise"); ?></b></div><?php endif; ?>

        <?php if (in_array('description', $packageInfoArray)): ?>
            <div class="contentblock_left_text"><b><?php echo $this->translate("Description"); ?></b></div>
        <?php endif; ?>
        <?php if (in_array('price', $packageInfoArray)): ?>
            <div class="contentblock_left_text highlightleft"><b><?php echo $this->translate("Price"); ?></b></div>
        <?php endif; ?>
    </div>
    <div class="paidEvents scroll-pane" id="paidSeaocorePanel" style="overflow-x: hidden; overflow-y: hidden; ">
        <div class=" " id ="scrollbar_before"></div>
        <div id="scroll-areas-main" >
            <div id="list-scroll-areas" style=" float:left;overflow:hidden;"> 
                <div class="scroll-content" id="scroll-content" style="margin-left: 0px;width:100%; display:table;">
                    <?php foreach ($this->paginator as $item): ?>
                        <div class="contentblock_right_inner">
                            <div class="contentblock_right_inner_heading o_hidden">
                                <a href="javascript:void(0);" onclick="Smoothbox.open('<?php echo $this->url(array('module' => 'communityad', 'controller' => 'index', 'action' => 'packge-detail', 'id' => $item->package_id), 'default') ?>')"  ><?php echo $this->translate(ucfirst($item->title)); ?></a>
                            </div>
                            <div class="contentblock_right_text">
                                <div class="contentblock_right_inner_btn">
                                    <?php if ($controller == 'package' && $action == 'update-package'): ?>

                                    <?php else: ?>
                                        <?php if (!empty($this->type_id) && !empty($this->type)) : ?>
                                            <a class="seaocore_buttonlink" href='<?php echo $this->url(array('id' => $item->package_id, 'type' => $this->type, 'type_id' => $this->type_id), 'communityad_create', true) ?>' ><?php echo $this->translate("COMMUNITYAD_PACKAGE_CREATE_BUTTON_" . strtoupper($item->type)); ?> &raquo;</a>
                                        <?php else : ?>
                                            <a class="seaocore_buttonlink" href='<?php echo $this->url(array('id' => $item->package_id), 'communityad_create', true) ?>' ><?php echo $this->translate("COMMUNITYAD_PACKAGE_CREATE_BUTTON_" . strtoupper($item->type)); ?> &raquo;</a>  
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php if (in_array('price', $packageInfoArray)): ?>
                                <div class="contentblock_right_text highlightright"><b> <?php
                                        if (!$item->isFree()):echo $this->locale()->toCurrency($item->price, $currency);
                                        else: echo $this->translate('FREE');
                                        endif;
                                        ?></b> 
                                </div>
                            <?php endif; ?>
                            <?php if (in_array('quantity', $packageInfoArray)): ?>
                                <div class="contentblock_right_text"> <?php
                                    switch ($item->price_model):
                                        case "Pay/view":
                                            if ($item->model_detail != -1): echo $this->translate(array('%s View', '%s Views', $item->model_detail), $this->locale()->toNumber($item->model_detail));
                                            else: echo $this->translate('UNLIMITED Views');
                                            endif;

                                            break;

                                        case "Pay/click":
                                            if ($item->model_detail != -1): echo $this->translate(array('%s Click', '%s Clicks', $item->model_detail), $this->locale()->toNumber($item->model_detail));
                                            else: echo $this->translate('UNLIMITED Clicks');
                                            endif;

                                            break;

                                        case "Pay/period":
                                            if ($item->model_detail != -1): echo $this->translate(array('%s Day', '%s Days', $item->model_detail), $this->locale()->toNumber($item->model_detail));
                                            else: echo $this->translate('UNLIMITED  Days');
                                            endif;

                                            break;
                                    endswitch;
                                    ?></div>
                            <?php endif; ?>
                            <?php if ($item->type == 'default'): ?> 
                                <?php if (in_array('featured', $packageInfoArray)): ?>
                                    <div class="contentblock_right_text">     
                                        <?php if ($item->featured == 1): ?>
                                            <img src="<?php echo $this->layout()->staticBaseUrl ?>application/modules/Communityad/externals/images/tick.png">
                                        <?php else: ?>
                                            <img src="<?php echo $this->layout()->staticBaseUrl ?>application/modules/Communityad/externals/images/cross.png">
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="contentblock_right_text"> <img src="<?php echo $this->layout()->staticBaseUrl ?>application/modules/Communityad/externals/images/cross.png"></div>
                            <?php endif; ?>
                            <?php if (in_array('sponsored', $packageInfoArray)): ?>
                                <div class="contentblock_right_text">     
                                    <?php if ($item->sponsored == 1): ?>
                                        <img src="<?php echo $this->layout()->staticBaseUrl ?>application/modules/Communityad/externals/images/tick.png">
                                    <?php else: ?>
                                        <img src="<?php echo $this->layout()->staticBaseUrl ?>application/modules/Communityad/externals/images/cross.png">
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>

                            <?php if (in_array('targeting', $packageInfoArray)): ?>
                                <div class="contentblock_right_text">     
                                    <?php if ($item->network == 1): ?>
                                        <img src="<?php echo $this->layout()->staticBaseUrl ?>application/modules/Communityad/externals/images/tick.png">
                                    <?php else: ?>
                                        <img src="<?php echo $this->layout()->staticBaseUrl ?>application/modules/Communityad/externals/images/cross.png">
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>

                            <?php if (in_array('youcanadvertise', $packageInfoArray)): ?> 
                                <?php
                                $canAdvertise = explode(",", $item->urloption);
                                if (in_array("website", $canAdvertise)) {
                                    $canAdvertise[array_search("website", $canAdvertise)] = $this->translate('Custom Ad');
                                }

                                foreach ($canAdvertise as $key => $value):
                                    if (strstr($value, "sitereview")) {
                                        $isReviewPluginEnabled = Engine_Api::_()->getDbtable('modules', 'communityad')->getModuleInfo("sitereview");
                                        if (!empty($isReviewPluginEnabled)) {
                                            $sitereviewExplode = explode("_", $value);
                                            $getAdsMod = Engine_Api::_()->getItem("communityad_module", $sitereviewExplode[1]);
                                            $modTemTitle = strtolower($getAdsMod->module_title);
                                            $modTemTitle = ucfirst($modTemTitle);
                                            $canAdvertise[$key] = $modTemTitle;
                                        } else {
                                            unset($canAdvertise[$key]);
                                        }
                                    } else {
                                        if ($value != 'Custom Ad') {
                                            $getInfo = Engine_Api::_()->getDbtable('modules', 'communityad')->getModuleInfo($value);
                                            if (!empty($getInfo)) {
                                                $canAdvertise[$key] = $this->translate($getInfo['module_title']);
                                            } else {
                                                unset($canAdvertise[$key]);
                                            }
                                        } else {
                                            $canAdvertise[$key] = ucfirst($value);
                                        }
                                    }
                                endforeach;
                                $canAdStr = implode(", ", $canAdvertise);
                                ?>
                                <div class="contentblock_right_text" style="overflow: auto;">     
                                    <?php echo $canAdStr; ?>
                                </div>
                            <?php endif; ?>

                            <?php if (in_array('description', $packageInfoArray)): ?>      
                                <div class="contentblock_right_text contentblock_description">
                                    <?php if (empty($this->detailPackage)): ?>
                                        <?php echo $this->viewMore($this->translate($item->desc), 425); ?>
                                    <?php else: ?>
                                        <?php echo $this->translate($item->desc); ?>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>

                            <?php if (in_array('price', $packageInfoArray)): ?>    
                                <div class="contentblock_right_text highlightright">
                                    <b>
                                        <?php
                                        if ($item->price > 0):
                                            echo $this->locale()->toCurrency($item->price, $currency);
                                        else:
                                            echo $this->translate('FREE');
                                        endif;
                                        ?>
                                    </b> 
                                </div>
                            <?php endif; ?>

                            <div class="contentblock_right_text">
                                <div class="contentblock_right_inner_btn">
                                    <?php if ($controller == 'package' && $action == 'update-package'): ?>

                                    <?php else: ?>
                                        <?php if (!empty($this->type_id) && !empty($this->type)) : ?>
                                            <a class="seaocore_buttonlink" href='<?php echo $this->url(array('id' => $item->package_id, 'type' => $this->type, 'type_id' => $this->type_id), 'communityad_create', true) ?>' ><?php echo $this->translate("COMMUNITYAD_PACKAGE_CREATE_BUTTON_" . strtoupper($item->type)); ?> &raquo;</a>
                                        <?php else : ?>
                                            <a class="seaocore_buttonlink" href='<?php echo $this->url(array('id' => $item->package_id), 'communityad_create', true) ?>' ><?php echo $this->translate("COMMUNITYAD_PACKAGE_CREATE_BUTTON_" . strtoupper($item->type)); ?> &raquo;</a>  
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="scrollbarArea" id ="scrollbar_after"></div>
    </div>
</li>

<script type="text/javascript" >

    var totalLsit = <?php echo $this->paginator->getTotalItemCount(); ?>;
    en4.core.runonce.add(function () {
        resetContent();
        (function () {
            $('list-scroll-areas').setStyle('height', $('scroll-content').offsetHeight + 'px');
            $('list-scroll-areas').setStyle('width', $('paidSeaocorePanel').offsetWidth + 'px');
            
            scrollBarContentArea = new SEAOMooHorizontalScrollBar('scroll-areas-main', 'list-scroll-areas', {
                'arrows': false,
                'horizontalScroll': true,
                'horizontalScrollElement': 'scrollbar_after',
                'horizontalScrollBefore': true,
                'horizontalScrollBeforeElement': 'scrollbar_before'
            });
        }).delay(700);
    });

    var resetContent = function () {
        var width = ($('paidSeaocorePanel').offsetWidth / totalLsit);
        width = width - 2;
        if (width < 200)
            width = 200;
        width++;
        var numberOfItem = ($('paidSeaocorePanel').offsetWidth / width);
        var numberOfItemFloor = Math.floor(numberOfItem);
        var extra = (width * (numberOfItem - numberOfItemFloor) / numberOfItemFloor);
        width = width + extra;
        $('scroll-content').setStyle('width', (width * totalLsit) + 'px');
        $('scroll-content').getElements('.contentblock_right_inner').each(function (el) {
            el.setStyle('width', width - 1 + 'px');

        });
    }; 
</script>