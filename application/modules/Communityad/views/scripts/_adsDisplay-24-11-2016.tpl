<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions 
 * @package    Communityad
 * @copyright  Copyright 2009-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: _communityad-pages.tpl 2011-02-16 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<style type="text/css">
ul.footer-nav.adv_footer_menu li
{display:inline-block;}
ul.footer-nav.adv_footer_menu li a
{color:#666 !important; font-size:15px; }
ul.footer-nav.adv_footer_menu li a:hover
{color:#666 !important;}

</style>
<?php
echo '<pre>';
$is_identity = $this->viewer()->getIdentity();
$adcancel_enable = Engine_Api::_()->getApi('settings', 'core')->getSetting('adcancel.enable', 1);
$adBlockWidth = Engine_Api::_()->getApi('settings', 'core')->getSetting('ad.block.width', 150);
$k=1;
foreach ($this->communityads_array as $community_ad):
  $div_id = $this->identity . $community_ad['userad_id'];
  $encode_adId = Engine_Api::_()->communityad()->getDecodeToEncode('' . $community_ad['userad_id'] . '');
  if (!empty($community_ad['resource_type']) && !empty($community_ad['resource_id'])):
    $resource_url = Engine_Api::_()->communityad()->resourceUrl($community_ad['resource_type'], $community_ad['resource_id']);
  endif;
  ?>
  <?php Engine_Api::_()->communityad()->adViewCount($community_ad['userad_id'], $community_ad['campaign_id']); ?>
  <?php //endif;     ?>
  <?php if($k==1){echo "<div class='third_ad'>";}?>
  <div class="caab_list">
    <!-- DIV: Which show when click on cross of advertisment. -->
    <div id= "cmad_ad_cancel_<?php echo $div_id; ?>" style="display:none; width:<?php echo $adBlockWidth; ?>px;" class="cmadrem">
      <div class="cmadrem_rl">
        <?php echo '<a class="" title="' . $this->translate('Cancel reporting this ad') . '" href="javascript:void(0);" onclick="adUndo(' . $div_id . ', \'cmad\');">' . $this->translate('Undo') . '</a>'; ?>
      </div>
      <div class="cmadrem_con">
        <?php echo $this->translate("Do you want to report this? Why didn't you like it?"); ?>
        <form>
          <?php $ads_id = $encode_adId; ?>
          <div><input type="radio" name="adAction" value="0" onclick="adSave('Misleading', '<?php echo $ads_id; ?>', <?php echo $div_id; ?>, 'cmad')"/><?php echo $this->translate('Misleading'); ?></div>
          <div><input type="radio" name="adAction" value="1" onclick="adSave('Offensive', '<?php echo $ads_id; ?>', <?php echo $div_id; ?>, 'cmad')"/><?php echo $this->translate('Offensive'); ?></div>
          <div><input type="radio" name="adAction" value="2" onclick="adSave('Inappropriate', '<?php echo $ads_id; ?>', <?php echo $div_id; ?>, 'cmad')"/><?php echo $this->translate('Inappropriate'); ?></div>
          <div><input type="radio" name="adAction" value="3" onclick="adSave('Licensed Material', '<?php echo $ads_id; ?>', <?php echo $div_id; ?>, 'cmad')"/><?php echo $this->translate('Licensed Material'); ?></div>
          <div><input type="radio" name="adAction" value="4" onclick="otherAdCannel(4, '<?php echo $div_id; ?>', 'cmad')" id="cmad_other_<?php echo $div_id; ?>"/><?php echo $this->translate('Other'); ?></div>

          <div>
            <textarea name="cmad_other_text_<?php echo $div_id; ?>" onclick="this.value = ''" onblur="if (this.value == '')
                this.value = '<?php echo $this->string()->escapeJavascript($this->translate('Specify your reason here..')) ?>';"  id="cmad_other_text_<?php echo $div_id; ?>" style="display:none;" /><?php echo $this->translate('Specify your reason here..') ?></textarea>
          </div>

          <div>
            <?php echo '<a href="javascript:void(0);" onclick="adSave(\'Other\', \'' . $ads_id . '\', ' . $div_id . ', \'cmad\')" id="cmad_other_button_' . $div_id . '"  style="display:none" class="cmadrem_button">' . $this->translate('Report') . '</a>'; ?>
          </div>
        </form>
      </div>	
    </div>

    <!-- DIV:  Which default show, This div contain the all information about advertisment. -->
    <div class="cmaddis" id="cmad_ad_<?php echo $div_id; ?>" style="width:<?php echo $adBlockWidth ?>px;">
      <div class="cmaddis_close">
        <?php
        if (!empty($is_identity) && !empty($adcancel_enable)) {
          echo '<a class="" title="' . $this->translate('Report this ad') . '" href="javascript:void(0);" onclick="adCancel(' . $div_id . ', \'cmad\');"></a>';
        }
        ?>
      </div>
      <div class="cmadaddis" style="width:<?php echo $adBlockWidth ?>px;">
               
		
        <!--image code start here for both-->
        <div class="cmaddis_image">
          <a href="<?php echo $this->url(array('adId' => $encode_adId), 'communityad_adredirect', true) ?>"  
		  <?php echo $set_target ?>>
		 
		  <?php echo $this->itemPhoto($community_ad, '', '') ?>
		  </a>
        </div>
		
		 <!--title code start here-->
		<div class="cmaddis_title">
          <?php
          // Title if has existence on site then "_blank" not work else work.
          if (!empty($community_ad['resource_type']) && !empty($community_ad['resource_id'])) {
            $set_target = '';
          } else {
            $set_target = 'target="_blank"';
          }
          echo '<a href="' . $this->url(array('adId' => $encode_adId), 'communityad_adredirect', true) . '" ' . $set_target . ' >' . ucfirst($community_ad['cads_title']) . "</a>";
          ?>
        </div>
		
        <!--description code start here for both-->
        <div class="cmaddis_body">	
          <a href="<?php echo $this->url(array('adId' => $encode_adId), 'communityad_adredirect', true) ?>"  <?php echo $set_target ?>><?php echo $community_ad['cads_body'] ?></a>

        </div>
        <!--description code end here for both-->
      </div>	

    </div>
	<?php if($k==1){
	$url = explode('/',$_SERVER['REQUEST_URI']);
	if($url[count($url)-1]=='home' || $url[count($url)-1]=='casting' || $url[count($url)-1]=='services' || $url[count($url)-2]=='profile' || ($url[count($url)-2]=='job' && $url[count($url)-1]=='search')){
	?>
	<div class="allads">
   
    <a href="<?php echo $this->baseUrl().'/members/subscribe';?>">
  <img src="<?php echo $this->baseUrl();?>/public/custom/images/subscribe-to-newsletter-button.png" style="width: 91%;">
  </a>
	<ul class="footer-nav adv_footer_menu">
		
		<li class=""><a href="<?php echo $this->baseUrl().'/cms/about';?>">About Us</a></li>
		<li class=""><a href="<?php echo $this->baseUrl().'/ads/campaigns';?>">Advertise</a></li>
		<li class=""><a href="<?php echo $this->baseUrl().'/cms/privacy';?>">Contact Us</a></li>
		<li class=""><a href="<?php echo $this->baseUrl().'/cms/faq';?>">FAQ's</a></li>
		
	</ul>
	
    
    <ul class="adv-footer-nav adv-footer-copy">
		 <li><p> &copy; <span style="text-transform: uppercase;"> CREZILLA</span> Copyright 2016 </p></li>
		<li><a href="https://www.facebook.com/Crezilla-1256684177699324/" target="_blank"><img src="<?php echo $this->baseUrl().'/public/custom/images/inner-fb.png'  ?>"></a></li>
        <li><a href=" https://www.instagram.com/crezilla_community/" target="_blank"><img src="<?php echo $this->baseUrl().'/public/custom/images/inner-insta.png'  ?>"></a></li>
        <li><a href="https://twitter.com/crezilla" target="_blank"><img src="<?php echo $this->baseUrl().'/public/custom/images/inner-twit.png'  ?>"></a></li>
        <li><a href="" target="_blank"><img src="<?php echo $this->baseUrl().'/public/custom/images/inner-link.png'  ?>"></a></li>
   </ul>
  
	
    
    
    </div>
	<?php } 
	}?>
  </div>	
 <?php if($k==1){?>
 <div class="head create_div" style="display:none;"><a href="<?php echo $this->baseUrl();?>/ads/package" style="float:left;">Create an Ad</a><div style="float:right;"><a href="<?php echo $this->baseUrl();?>/ads/adboard">More Ads</a></div>
</div>			  
 <?php } 
  if($k==1){echo "</div>";} ?>
<?php 

if(sizeof($this->communityads_array)<3){?>
<!-- 	
<div class="allads">
   
    <a href="<?php echo $this->baseUrl().'/members/subscribe';?>">
  <img src="<?php echo $this->baseUrl();?>/public/custom/images/subscribe-to-newsletter-button.png" style="width: 91%;">
  </a>
	<ul class="footer-nav adv_footer_menu">
		
		<li class=""><a href="<?php echo $this->baseUrl().'/cms/about';?>">About Us</a></li>
		<li class=""><a href="<?php echo $this->baseUrl().'/ads/campaigns';?>">Advertise</a></li>
		<li class=""><a href="<?php echo $this->baseUrl().'/cms/privacy';?>">Contact Us</a></li>
		<li class=""><a href="<?php echo $this->baseUrl().'/cms/faq';?>">FAQ's</a></li>
		
	</ul>
	<p style="font-size:14px; color:#666 !important;text-transform: lowercase;"> &copy; CREZILLA Copyright 2015 </p>
	</div> -->
<?php }
$k++; endforeach; ?>