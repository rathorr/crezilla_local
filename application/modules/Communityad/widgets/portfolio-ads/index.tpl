<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
   <style type="text/css">
   ._community_ads_items
   {
    min-height: 262px;
  }
  ._adimg
  {
    max-height: 102px;
    min-height: 102px;
    object-fit: cover;
  }
  p._addesc
  {
    font-size: 12px !important;
    min-height: 80px;
    max-height: 80px;
  }
  ._pp_area_bottom2
  {
    font-family: 'roboto';
    text-transform: uppercase;
    background-color: #fff;
    color: #fb933c;
    margin-bottom: 10px;
  }
  ._pp_area_bottom2 >a,
  ._pp_area_bottom2 >a:hover
  {
    color: #fb933c;
  }
  ._pp_area
  {
    margin-bottom: 0px;
  }
 </style>
<!-- Eighth Block -->
<div class="_ps_common_heading">SPONSERED ADS</div>
<div class="_pp_area _ps_pas ads_slicker">
<?php foreach ($this->communityads_array as $community_ad){
 $div_id = $this->identity . $community_ad['userad_id'];
  $encode_adId = Engine_Api::_()->communityad()->getDecodeToEncode('' . $community_ad['userad_id'] . '');
  if (!empty($community_ad['resource_type']) && !empty($community_ad['resource_id'])){
    $resource_url = Engine_Api::_()->communityad()->resourceUrl($community_ad['resource_type'], $community_ad['resource_id']);
  }
  $views=Engine_Api::_()->communityad()->adViewCount($community_ad['userad_id'], $community_ad['campaign_id']);

  // URL $community_ad['cads_url'];
  ?>

  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 _community_ads_items">
    <div class="_community_ads_img" id="<?php echo $div_id; ?>"><a href="<?php echo $this->url(array('adId' => $encode_adId), 'communityad_adredirect', true) ?>" target="_blank">
    <img src="<?php echo Engine_Api::_()->getItem('storage_file', $community_ad['photo_id'])->storage_path;?>" alt="" class="_adimg"></a></div>
    <div class="_community_ads_desc">
    <span><a class="_community_ads_title _clear" href="<?php echo $this->url(array('adId' => $encode_adId), 'communityad_adredirect', true) ?>" title="" target="_blank"><?php echo ucfirst($community_ad['cads_title']);?></a></span>
    <!-- <span><a class="_community_ads_web_link _clear" href="<?php echo $this->url(array('adId' => $encode_adId), 'communityad_adredirect', true) ?>" title="" target="_blank">www.shopify.com</a></span> -->
      <p class="_clear _addesc"><?php echo $community_ad['cads_body']; ?></p>      
    </div>
  </div>   
  <?php }?> 
</div>
<div class="col-xs-12 _pp_area_bottom2 text-center">
      <a href="<?php echo $this->baseUrl();?>/ads/package" style="float:left;">Create an Ad</a>
      <a href="<?php echo $this->baseUrl();?>/ads/adboard" style="float:right;">More Ads</a> 
  </div>

<!-- End Eighth Block -->
<script type="text/javascript" src="application/modules/User/externals/portfolio/bootstrap.min.js"></script>
<script type="text/javascript" src="application/modules/User/externals/portfolio/slick.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    
    jQuery('.ads_slicker').slick({
      slidesToShow: 2,
        slidesToScroll: 1
    });
  });
</script>