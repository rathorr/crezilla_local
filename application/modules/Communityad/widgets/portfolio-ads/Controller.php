<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Communityad
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class Communityad_Widget_PortfolioAdsController extends Engine_Content_Widget_Abstract {
	
	public function indexAction(){

		// Don't render this if not logged in
	    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	    if( !$viewer->getIdentity() ) {
	      return $this->setNoRender();
	    }

		$limit = Engine_Api::_()->getApi('settings', 'core')->ad_board_limit;
		$params = array();
		$params['lim'] = $limit;
		// FEATCH ADS
		$fetch_community_ads = Engine_Api::_()->communityad()->getAdvertisement($params);
		if (!empty($fetch_community_ads)) {
			$this->view->communityads_array = $fetch_community_ads;
		} else {
			$this->view->noResult = 1;
		}
	}    	   
}