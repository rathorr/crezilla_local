<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions 
 * @package    Communityad
 * @copyright  Copyright 2009-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: Controller.php  2011-02-16 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
class Communityad_Widget_TopAdController extends Engine_Content_Widget_Abstract {

  public function indexAction() {
	  $packageIds = $this->_getParam('packageIds', array());
      if ($packageIds) {
        $packages = Engine_Api::_()->getItemtable('package')->getEnabledPackageList('default');
        $packageIds = array_intersect($packageIds, array_keys($packages));
        if ($packageIds) {
          $params['packageIds'] = $packageIds;
        }
      }
	 
   $this->view->communityads_array = $fetch_community_ads = Engine_Api::_()->communityad()->getTopAdvertisement($params);
   
   $this->view->viewer_object = $viewer_object = Engine_Api::_()->user()->getViewer();
      $this->view->user_id = $viewer_object->getIdentity();
  }

}

?>