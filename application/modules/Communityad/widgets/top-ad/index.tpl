<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions 
 * @package    Communityad
 * @copyright  Copyright 2009-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: _communityad-pages.tpl 2011-02-16 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<?php
$is_identity = $this->viewer()->getIdentity();
$adcancel_enable = Engine_Api::_()->getApi('settings', 'core')->getSetting('adcancel.enable', 1);
$adBlockWidth = Engine_Api::_()->getApi('settings', 'core')->getSetting('ad.block.width', 150);
$community_ad	=	$this->communityads_array[0];
  $div_id = $this->identity . $community_ad['userad_id'];
  $encode_adId = Engine_Api::_()->communityad()->getDecodeToEncode('' . $community_ad['userad_id'] . '');
  if (!empty($community_ad['resource_type']) && !empty($community_ad['resource_id'])):
    $resource_url = Engine_Api::_()->communityad()->resourceUrl($community_ad['resource_type'], $community_ad['resource_id']);
  endif;
  ?>
  <?php Engine_Api::_()->communityad()->adViewCount($community_ad['userad_id'], $community_ad['campaign_id']); ?>
  <?php //endif;     ?>
  <div class="caab_list top_ads" style="width:100%;">
    <!-- DIV: Which show when click on cross of advertisment. -->
    <div id= "cmad_ad_cancel_<?php echo $div_id; ?>" style="display:none; class="cmadrem">
      <div class="cmadrem_rl top_ads">
        <?php echo '<a class="" title="' . $this->translate('Cancel reporting this ad') . '" href="javascript:void(0);" onclick="adUndo(' . $div_id . ', \'cmad\');">' . $this->translate('Undo') . '</a>'; ?>
      </div>
      <div class="cmadrem_con top_ads">
        <?php echo $this->translate("Do you want to report this? Why didn't you like it?"); ?>
        <form>
          <?php $ads_id = $encode_adId; ?>
          <div><input type="radio" name="adAction" value="0" onclick="adSave('Misleading', '<?php echo $ads_id; ?>', <?php echo $div_id; ?>, 'cmad')"/><?php echo $this->translate('Misleading'); ?></div>
          <div><input type="radio" name="adAction" value="1" onclick="adSave('Offensive', '<?php echo $ads_id; ?>', <?php echo $div_id; ?>, 'cmad')"/><?php echo $this->translate('Offensive'); ?></div>
          <div><input type="radio" name="adAction" value="2" onclick="adSave('Inappropriate', '<?php echo $ads_id; ?>', <?php echo $div_id; ?>, 'cmad')"/><?php echo $this->translate('Inappropriate'); ?></div>
          <div><input type="radio" name="adAction" value="3" onclick="adSave('Licensed Material', '<?php echo $ads_id; ?>', <?php echo $div_id; ?>, 'cmad')"/><?php echo $this->translate('Licensed Material'); ?></div>
          <div><input type="radio" name="adAction" value="4" onclick="otherAdCannel(4, '<?php echo $div_id; ?>', 'cmad')" id="cmad_other_<?php echo $div_id; ?>"/><?php echo $this->translate('Other'); ?></div>

          <div>
            <textarea name="cmad_other_text_<?php echo $div_id; ?>" onclick="this.value = ''" onblur="if (this.value == '')
                this.value = '<?php echo $this->string()->escapeJavascript($this->translate('Specify your reason here..')) ?>';"  id="cmad_other_text_<?php echo $div_id; ?>" style="display:none;" /><?php echo $this->translate('Specify your reason here..') ?></textarea>
          </div>

          <div>
            <?php echo '<a href="javascript:void(0);" onclick="adSave(\'Other\', \'' . $ads_id . '\', ' . $div_id . ', \'cmad\')" id="cmad_other_button_' . $div_id . '"  style="display:none" class="cmadrem_button">' . $this->translate('Report') . '</a>'; ?>
          </div>
        </form>
      </div>	
    </div>

    <!-- DIV:  Which default show, This div contain the all information about advertisment. -->
    <div class="cmaddis top_ads" id="cmad_ad_<?php echo $div_id; ?>">
      <div class="cmaddis_close top_ads">
        <?php
        if (!empty($is_identity) && !empty($adcancel_enable)) {
          echo '<a class="" title="' . $this->translate('Report this ad') . '" href="javascript:void(0);" onclick="adCancel(' . $div_id . ', \'cmad\');"></a>';
        }
        ?>
      </div>
      <div class="cmadaddis top_ads">
        
        <!--image code start here for both-->
        <div class="cmaddis_image top_ads">
          <a href="<?php echo $this->url(array('adId' => $encode_adId), 'communityad_adredirect', true) ?>"  <?php echo $set_target ?>><?php echo $this->itemPhoto($community_ad, '', '') ?></a>
        </div>

      </div>	
    
    </div>
  </div>	

