<style>
.layout_right .cmaddis .cmadaddis {
    padding-bottom: 0px !important;
}

.layout_communityad_ads .cmad_ad_clm .cmad_block_wrp .caab_list {
    border-bottom: 1px solid #eee;
    padding-bottom: 5px;}
	
	.cmaddis .cmaddis_body a {
    color: #666;
    font-size: 11px;
}

.cmaddis .cmaddis_title a{font-size:11px !important;}
.cmaddis .cmaddis_image img{ width:100% !important; }
.fxd_header{background: #fff none repeat scroll 0 0 !important; box-shadow: 0 1px 2px 0 rgba(1, 1, 1, 0.3);}
</style>

<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions 
 * @package    Communityad
 * @copyright  Copyright 2009-2011 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    $Id: index.tpl  2011-02-16 9:40:21Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */
?>
<?php if (!empty($this->loaded_by_ajax)) : ?>
<?php $content_id= $this->identity?$this->identity: ($this->widgetId?$this->widgetId:rand(1000000000, 9999999999))?>
  <div id="content_<?php echo $content_id ?>"></div>
  <script type="text/javascript">
    en4.core.runonce.add(function() {
      en4.communityad.sendReq($("content_<?php echo $content_id ?>").getParent('.layout_communityad_ads'),'<?php echo $this->identity ?>','<?php echo $this->adboardPage ? 1 :0 ?>',<?php echo $this->params ? $this->jsonInline($this->params):"{}" ?>);
    });
  </script>
<?php endif; ?>
  <?php if(!$this->identity): $this->identity= rand(1000000000, 9999999999); endif;?>
<?php if (!empty($this->showContent)): ?>

  <div class="cmad_ad_clm">
    <div>
      <?php if ($this->showType == 'all'): ?>
      
      <div class="cmad_block_wrp">
        <?php
        include APPLICATION_PATH . '/application/modules/Communityad/views/scripts/_adsDisplay.tpl';
        ?>
      </div>
        <?php if(!$this->adboardPage): ?>
        <div class="head">
          <?php if (Engine_Api::_()->communityad()->enableCreateLink()) : ?>
            <?php echo '<a href="' . $this->url(array(), 'communityad_listpackage', true) . '" style="float:left;">' . $this->translate('Create an Ad') . '</a>'; ?>
          <?php endif; ?>
          <?php
          $is_show_adboard = Engine_Api::_()->getApi('settings', 'core')->getSetting('show.adboard', 1);
          if (!empty($is_show_adboard)):
            echo '<div style="float:right;"><a href="' . $this->url(array(), 'communityad_display', true) . '">' . $this->translate('More Ads') . '</a></div>';
          endif;
          ?>
        </div>
      <?php endif; ?>
      <?php else: ?>
        <div class="cmad_bloack_top">
          <b><?php echo $this->showType === 'sponsored' ? $this->translate('Sponsored') : $this->translate('Featured') ?></b>
          <?php if (!$this->adboardPage && Engine_Api::_()->communityad()->enableCreateLink()) : ?>
            <?php echo '<a href="' . $this->url(array(), 'communityad_listpackage', true) . '">' . $this->translate('Create an Ad') . '</a>'; ?>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      
      <?php if ($this->showType !== 'all'): ?>
        <?php if (!$this->adboardPage && Engine_Api::_()->getApi('settings', 'core')->getSetting('show.adboard', 1)): ?>
          <div class="cmaddis_more"><a href="<?php echo $this->url(array(), 'communityad_display', true) ?>"><?php echo $this->translate('More Ads') ?></a></div>
          <?php endif; ?>
        <?php endif; ?>
    </div>
  </div>
<?php endif; ?>
<style type="text/css">
.login_my_wrapper #global_content > div > div > div.layout_right > .generic_layout_container.layout_communityad_ads {
    clear: both;
    margin-bottom: 0;
}

.login_my_wrapper #global_content .layout_main .layout_right
{position:relative;}

/*.absolute_header{position: absolute; z-index: 999;}*/


</style>
<script type="text/javascript">

jQuery(document).ready(function(){
	
	jQuery('.layout_communityad_ads').addClass('absolute_header');
	jQuery('.layout_communityad_ads').attr('id','masthead');
	
});
if(typeof(jQuery('.third_ad').offset()) !='undefined'){
	var topv = jQuery('.third_ad').offset().top;
jQuery(window).scroll(function() {
    
    var windscroll = jQuery(window).scrollTop();
 	
    if (windscroll > topv) {
			jQuery('.create_div').show();
            jQuery(".third_ad").addClass("fxd_header");
           jQuery(".third_ad").removeClass("absolute_header");
    }
    else
    {
		jQuery('.create_div').hide();
        jQuery(".third_ad").removeClass("fxd_header");
        jQuery(".third_ad").addClass("absolute_header");
    }
   
});
}
 
</script>