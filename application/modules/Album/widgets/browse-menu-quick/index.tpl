<!-- <p class="skip_div abs_d2">
<a href="<?php echo $this->baseUrl().'/pdf';?>" class="skip_text skip_bt2 wp_init">Next</a>
</p> -->
<?php if( count($this->quickNavigation) > 0 ): ?>
  <div class="quicklinks">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->quickNavigation)
        ->render();
    ?>
  </div>
<?php endif; ?>

