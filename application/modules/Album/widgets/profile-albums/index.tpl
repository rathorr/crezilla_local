<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>
<link rel="stylesheet" href="<?php echo $this->baseUrl().'/application/modules/Album/externals/styles/prettyPhoto.css';?>" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $this->baseUrl().'/application/modules/Album/externals/scripts/jquery.prettyPhoto.js'?>"></script>

<script type="text/javascript">
  en4.core.runonce.add(function(){

    <?php if( !$this->renderOne ): ?>
    var anchor = $('profile_albums').getParent();
    $('profile_albums_previous').style.display = '<?php echo ( $this->paginator->getCurrentPageNumber() == 1 ? 'none' : '' ) ?>';
    $('profile_albums_next').style.display = '<?php echo ( $this->paginator->count() == $this->paginator->getCurrentPageNumber() ? 'none' : '' ) ?>';

    $('profile_albums_previous').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() - 1) ?>
        }
      }), {
        'element' : anchor
      })
    });

    $('profile_albums_next').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() + 1) ?>
        }
      }), {
        'element' : anchor
      })
    });
    <?php endif; ?>
  });
</script>


  <?php $i=0;foreach( $this->paginator as $album ):
     // echo $album->photo_id; die;
       
   ?>
 
  <ul id="profile_albums" class="thumbs gallery<?php echo $i;?>">
    <li>
      <a class="thumbs_photo img_hov_grey_active" href="<?php echo $this->baseUrl().'/albums/photo/view/album_id/'.$album->getIdentity().'/photo_id/'.$album->photo_id; ?>" rel="prettyPhoto">
       <span class="img_hov_grey">
       <p class="thumbs_info">
        <span class="thumbs_title">
          <?php echo $this->string()->chunk($this->string()->truncate($album->getTitle(), 45), 10); ?>
        </span>
        <span class="photo_count">
        <?php echo $this->translate(array('%s photo', '%s photos', $album->count()),$this->locale()->toNumber($album->count())) ?>
        </span>
      </p>
       </span>
       <span style="background-image: url(<?php echo $album->getPhotoUrl('thumb.main'); ?>); background-size: cover; background-position: 50% 50%; width:100%;"></span>
     
       
      </a>
      <?php 
      $data=Engine_Api::_()->getDbtable('photos', 'album')->getAlbumPhotos($album->album_id, $this->subject->getIdentity());
       if($data){
       	foreach($data as $img){
            if($img['photo_id'] != $album->photo_id){
                echo '</li><li style="display:none;">';?>
                <a class="thumbs_photo" href="<?php echo $this->baseUrl().'/albums/photo/view/album_id/'.$album->getIdentity().'/photo_id/'.$album->photo_id; ?>" rel="prettyPhoto[gallery1]">
              		<img src="<?php echo $this->baseUrl().'/'.$img['storage_path']; ?>" style="width:160px !important; height:180px !important;"/>
               
              </a>
        	
        <?php }
         }
       }
     ?>
     
    </li>
     
    </ul>
  <?php $i++;endforeach;?>


