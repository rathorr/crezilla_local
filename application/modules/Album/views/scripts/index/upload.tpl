<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: upload.tpl 9987 2013-03-20 00:58:10Z john $
 * @author     Sami
 */
?>
<style>
#global_content > div > div.generic_layout_container.layout_top > div > div > div > div.tabs
{
 display: none;
}
#global_content > div > div.generic_layout_container.layout_top > div > div > div.headline
{
  padding-left: 10px;
}
#global_content > div > div.generic_layout_container.layout_top > div > div > div > h2
{
  margin: 0 0 10px 16px !important;
  border-bottom: 1px solid #ccc;
  padding-left: 0px !important;
}
.global_form div.form-label { display:none;}
</style>
<script type="text/javascript">
  var updateTextFields = function()
  {
    var fieldToggleGroup = ['#title-wrapper', '#category_id-wrapper', '#description-wrapper', '#search-wrapper',
                            '#auth_view-wrapper',  '#auth_comment-wrapper', '#auth_tag-wrapper'];
        fieldToggleGroup = $$(fieldToggleGroup.join(','))
    if ($('album').get('value') == 0) {
      fieldToggleGroup.show();
    } else {
      fieldToggleGroup.hide();
    }
  }
  en4.core.runonce.add(updateTextFields);

</script>

<?php echo $this->form->render($this) ?>


<script type="text/javascript">
jQuery(document).ready(function(){
  jQuery('.album_main_manage').parent().addClass('active');
  $$('.custom_305').getParent().addClass('active');
  $$('.custom_328').getParent().addClass('active');
});
  function validatecreatealbum(){
	
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#album").val()) == '0' && jQuery.trim(jQuery("#title").val()) == ''){
		jQuery("#title").focus();
		jQuery("#title").addClass("com_form_error_validatin");
		return false;
	}
	
	jQuery('#form-upload').submit();
	
}
</script>
