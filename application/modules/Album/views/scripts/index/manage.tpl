<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: manage.tpl 10217 2014-05-15 13:41:15Z lucas $
 * @author     Sami
 */
?>
<style type="text/css"> 
.pages{ float:right;}
.tabs {
  position: absolute;
    padding: 0px !important;
    width: 20%;
    background-color: #fff !important;
    border-right: 1px solid #ccc; 
    float: left;
}
.tab3 {
    position: relative;
    left: 25%;
    background-color: #fff;
    width: 75%;
    border-left: 1px solid #ccc;
    padding-top: 1px;
}
#global_content > div > div.generic_layout_container.layout_main > div > div > div > div.tabs > ul > li{
  width: 100%;}
  .heading
  {
    background-color:#fff;
    font-size:15px !important; padding: 10px 10px 0px 10px !important;
    border-bottom: none;
  }
   .heading > a:hover
  {
    border-bottom: 1px solid #fb933c;
    text-decoration: none !important;
    color: #fb933c !important;
  }
  img{
  max-width: 100%;
}
._community_ads, .chennal_video_list_section {
    padding: 0 15px!important;
}
.chennal_video_list_head{
  float: left;
    width: 100%;
    padding-bottom: 8px;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 30px;
}
.chennal_video_list_head_content {
    font-size: 18px;
    text-transform: uppercase;
}
.footer_section {
    padding: 40px 0;
    background: #e8e8e8;
    float: left;
    width: 100%;
    margin-top: 100px;
}
.footer-links a{
  text-decoration: none;
  color: #1f1f1f;
  font-size: 14px;
  text-transform: uppercase;   
}
.footer-links ul{
  margin: 0px;
  padding:0px;
  list-style: none;
}
.footer-links ul li{
  display: block;
  margin-bottom: 8px;
}
.footer_head{
  font-size: 14px;
  color: #1f1f1f;
  text-transform: uppercase;
  padding-bottom: 8px; 
}
.footer-social-content a {
    text-decoration: none;
    display: inline-block;
    margin-right: 10px;
    -webkit-transition: 0.5s ease-in-out;
    -moz-transition: 0.5s ease-in-out;
    -ms-transition: 0.5s ease-in-out;
    -o-transition: 0.5s ease-in-out;
    transition: 0.5s ease-in-out;
}
.footer-social-content a:hover{
  opacity: 0.5;
}
.footer-newsletter{padding-right: 0px;}
.footer-newsletter-content input.newsletter-input{
  height: 46px;
  line-height: 46px;
  border: 1px solid #000000;
  padding:0 20px;
  background: #fff;
  color: #1f1f1f;
  font-size:14px;
  text-transform: uppercase; 
  width: 65%;
  margin-right: -5px;
}
.footer-newsletter-content input.newsletter-submit{
  height: 47px;
  line-height: 46px;
  border: 1px solid #000000;
  padding:0 20px;
  background: #000;
  color: #fff;
  font-size:14px;
  text-transform: uppercase; 
  width: 35%;
  
}
.copyright_section {
    background: #d4d4d4;
    float: left;
    width: 100%;
    padding: 15px 0 10px;
}
.copyright_section span{text-transform: uppercase;}
.chennal_video_list_item_area{
  background: #fff;
  float: left;
  width: 100%;
  border:1px solid #ccc;
}
.chennal_video_list_item_area img{
  width: 100%;
}


._chennal_video_list_item_title a {
    color: #333;
    font-size: 18px;
}
._chennal_video_list_item_title a:hover
{
  /*border-bottom: 1px solid #fb933c;*/
    text-decoration: none !important;
    color: #fb933c;
}

._chennal_video_list_item_subtitle{
  font-size: 14px;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
  word-wrap: ;
  white-space: nowrap;
}
._chennal_video_list_item_action{
  float: left;
  width: 100%;
}
._chennal_video_list_item_action a{
  text-decoration: none;
  color: #cacaca; 
}
._chennal_video_list_item_review {
  font-size:14px;
  color: #ffc100; 
  float: left;
  width: 100%;
  padding-top: 20px;
}
._chennal_video_list_item_review .fa-star{
  cursor: pointer;
}
.padrgtnone{padding-right: 0px;}
.padlftnone{padding-left: 0px;}
._addfolderr_icn {
    width: 13px;
    height: 13px;
    display: inline-block;
    background: url(img/add_folder_icn.png) no-repeat center center;
    background-size: cover;
    margin-right: 0;
    margin-left: 8px;
}
.chennal_video_list_item {
  margin-bottom: 20px;
  padding-left: 10px;
  padding-right: 10px;
}
.chennal_video_list_search{
  float: left;
  width: 100%;
  padding-bottom: 20px;
}
.chennal_video_list_search input.search-input{
  border-radius: 0;
  box-shadow: none;
  margin-bottom: 10px;
}
.chennal_video_list_search input.search-submit{
  background: #ff9e20;
  color: #fff;
  border-radius: 0;
  box-shadow: none;
  -webkit-transition: 0.5s ease-in-out;
  -moz-transition: 0.5s ease-in-out;
  -ms-transition: 0.5s ease-in-out;
  -o-transition: 0.5s ease-in-out;
  transition: 0.5s ease-in-out;
}
.chennal_video_list_search input.search-submit:hover{
  background: #1f1f1f;
}
._chennal_video_list_head,
._community_ads_area {
    background: white;
}
/*.chennal_video_list,*/
._chennal_video_list_head{
  float: left;
    width: 100%;
    background: white;
    border-bottom: 1px solid #e5e5e5;
}
._chennal_video_list_head ._chennal_video_list_head_content{
  font-size: 18px;
    padding-top: 10px;
    padding-bottom: 10px;
    color: #333;
}
._video_sidebar{
  padding-top: 10px;
  font-family: 'Lato';
  font-size: 14px; 
}
._video_sidebar ul{
  padding:0;
  margin: 0;
  list-style: none; 
}
._video_sidebar ul li{
  display: block;
  height: 30px;
  line-height: 30px;
  padding: 0 30px;
  border-left: 3px solid transparent;
  margin-bottom: 8px;
}
._video_sidebar ul li a{
  display: block;
  text-transform: none;
  color: #000;
}
._video_sidebar ul li.active{
  border-left: 3px solid #f55731;
}
._video_sidebar ul li.active a{
  color: #f55731;
}
.chennal_video_list_area{
  float: left;
  width:  100%;
 /* border-left: 1px solid #e5e5e5;*/   
}
.chennal_video_list_area ._chennal_video_list_head{
  margin-bottom: 15px;
}
.chennal_video_list_area ._chennal_video_list_head ._chennal_video_list_head_content{
  float: left;
  padding-left:10px;
  padding-right:10px;
}
._add_chennal_video {
    float: right;
    margin-right: 10px;
    color: #000 !important;
    padding: 0px 10px;
    border: 1px solid #000;
    text-transform: uppercase !important;
    margin-top: 8px;
    height: 30px;
    line-height: 30px;
}
.chennal_video_img{
  position: relative;
}
.video_overlay{
  position: absolute;
  width: 40px;
    height: 40px;
    top: 50%;
    left: 50%;
    margin: -20px 0 0 -20px;
  cursor: pointer;
  background:url('application/modules/Video/externals/images/play_icn.png') no-repeat center center;
  opacity: 0;
  -webkit-transition: 0.3s ease-in-out;
  -moz-transition: 0.3s ease-in-out;
  -ms-transition: 0.3s ease-in-out;
  -o-transition: 0.3s ease-in-out;
  transition: 0.3s ease-in-out;
}
.chennal_video_img:hover .video_overlay,
.chennal_video_img:hover .edit_chennal_video{
  opacity: 1;
}
._chennal_video_action {
    position: absolute;
    right: 5px;
    top: 5px;
    width:  80%;
    z-index: 10;
}
.edit_chennal_video{
  opacity: 0;
  -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
.edit_chennal_video i{
  background: #fff;
  width: 20px;
  height: 20px;
  line-height: 20px;
  text-align: center;
  -webkit-border-radius: 2px 2px 0px 0px;
  -moz-border-radius: 2px 2px 0px 0px;
  border-radius: 2px 2px 0px 0px;
  display: inline-block;
    float: right;
    cursor: pointer;
}
._chennal_video_action_btns {
    clear: both;
    background: #fff;
    /*padding: 2px 8px;*/
    padding: 2px 0px 0px 2px;
    width: 100%;
    float: right;
    opacity:0;
    pointer-events: none;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
.edit_chennal_video:hover ._chennal_video_action_btns{
  opacity: 1;
  pointer-events: initial;
}
._chennal_video_action_btns a{
  display: block;
  padding:2px; 
}
._chennal_video_action_btns a:hover
{
  text-decoration: none !important;
    color: #fb933c;
}
._chennal_video_action a {
    color: #000;
}
._community_ads_head{
  padding: 10px 0;
}
._community_ads_head a{
  color: #000;
}
._community_ads_head a._createads{
  float: right;
  color: #f55731;
}
@media(max-width:1200px){
  ._video_sidebar ul li{
    padding-right: 15px;
    padding-left: 15px;
  }
}
@media (max-width: 1024px){
  .footer-newsletter,
  .footer-social-links {padding-left:0px; clear:both; margin-top: 20px;}

}
@media (max-width: 768px){
  .toptitle
  {
    margin: 0 0px !important;
    padding: 5px 0px !important;
     margin-bottom: 5px !important; 
  }
._chennal_video_list_item_title
{
  width: 95% !important;
}
  .footer-social-links{
    clear: none;
    margin-top: 0px;
  }
  .tabs2
  {
    width: 100% !important;
  }
  .tabs {
    position: relative;
    border-bottom: 1px solid #e8e8e8;
    width: 100%;
    float: none;
}
.tab3 {
    left: 0%;
    width: 100%;
    border-left: 0px solid #ccc;
}
.heading
{
  width: auto !important;
  padding: 0px 10px 10px 10px !important;
}
.chennal_video_list_item {
    /*border-bottom: 1px solid #ccc;*/
}
}
.container {
    padding-right: 0px;
    padding-left: 0px;
    }
.time_overlay
{
  position: absolute;
  right: 10px;
  bottom: 10px;
  color: #fff;
}
.img-responsive
{
  min-height:160px;
  max-height:160px;
}
._chennal_video_list_item_title,
._chennal_video_list_item_subtile
{
  padding-left: 10px;
}
._chennal_video_list_item_title
{
  white-space: nowrap;
  width: 190px;
  overflow: hidden;
  text-overflow: ellipsis;
  padding-top: 5px;
}
</style>
<h2 class="toptitle" style="border-bottom: 1px solid #ccc;">
    <?php echo $this->translate('Manage Albums') ?>
  </h2>
  <div class="tabs2"> 
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>    
<div class="tab3">
<h2 class="heading"><a style="border-bottom: 1px solid #fb933c; color: #fb933c;" href="<?php echo $this->baseUrl().'/albums/manage';?>">Album</a></h2>
<h2 class="heading"><a href="<?php echo $this->baseUrl().'/pdf';?>">Pdf</a></h2>
<h2 class="heading"><a href="<?php echo $this->baseUrl().'/videos/manage';?>">Video/Audio</a></h2>
<p style="border-bottom: 1px solid #ccc;font-size: 0px;">&nbsp;</p>
<div class="container">
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 _chennal_video_list_section">
      <div class=" chennal_video_list _manage_pages_list">
        <div class="row">
          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
            <div class="chennal_video_list_area">
            <div class="_chennal_video_list_head">
              <div class="_chennal_video_list_head_content">Albums</div>
              <a href="/albums/upload" class="_add_chennal_video"><i class="fa fa-plus"></i> New Album</a>
            </div>

         <?php if( $this->paginator->getTotalItemCount() > 0 ){
          foreach( $this->paginator as $album ){?>
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 chennal_video_list_item">
                <div class="chennal_video_list_item_area">              
                  <div class="chennal_video_img"><div class="_chennal_video_action">
                    <div class="edit_chennal_video" href="javascript:void(0);" title=""><i class="fa fa-pencil"></i>
                    <div class="_chennal_video_action_btns">
                      <!-- <a href="javascript:void(0);" title="">edit video</a> -->
                      <!-- <a href="javascript:void(0);" title="">delete video</a> -->
                      <?php echo $this->htmlLink(array('route' => 'album_general', 'action' => 'upload', 'album_id' => $album->album_id), $this->translate('Add Photos'), array(
                      'class' => ''
                      ))?>
                      <?php echo $this->htmlLink(array('route' => 'album_specific', 'action' => 'editphotos', 'album_id' => $album->album_id), $this->translate('Manage Photos'), array(
                        'class' => ''
                      )) ?>
                      <?php echo $this->htmlLink(array('route' => 'album_specific', 'action' => 'edit', 'album_id' => $album->album_id), $this->translate('Edit Album Title'), array(
                        'class' => ''
                      )) ?>
                      <?php echo $this->htmlLink(array('route' => 'album_specific', 'action' => 'delete', 'album_id' => $album->album_id, 'format' => 'smoothbox'), $this->translate('Delete Album'), array(
                        'class' => 'smoothbox'
                      )) ?>
                    </div>
                    </div>
                  </div><!-- <img src="img/video-1.jpg" class="img-responsive"> -->
                   <?php
                    if( $album->photo_id ) {?>
                    <a href="<?php echo $this->baseUrl().'/albums/photo/view/album_id/'.$album->getIdentity().'/photo_id/'.$album->photo_id; ?>" rel="prettyPhoto[gallery1]">
                  <?php echo $this->itemPhoto($album, 'img-responsive');?>
                   </a>
                 <?php } else {?>
                    <img class="img-responsive" src="application/modules/Album/externals/images/empty.png">
                  <?php  } ?>
                  </div>
                  <div class="_chennal_video_list_item_title">
                  <a href="/albums/editphotos/<?php echo $album->album_id; ?>" title=""><?php echo $album->getTitle();?></a>
                  </div>
                  <div class="_chennal_video_list_item_subtile">
                  <?php 
                  echo ($album->count() < 1 ? "0" : $album->count())." " .($album->count() > 1 ? "photos" : "photo");?>                    
                  </div>
                  <div class="_chennal_video_list_item_subtile">
                  <?php echo $album->getDescription() ?>
                  </div>
                </div>
              </div>
                <?php } if( $this->paginator->count() > 1 ){ ?>
                <br />
                <?php echo $this->paginationControl($this->paginator, null, null); ?>
              <?php } ?>
              <?php } else {?>
                <div class="tip">
                 <span>
                <?php echo $this->translate("You do not have any albums yet.");?>
                <?php if( $this->canCreate ){?>
                  <?php $create = $this->translate("Be the first to create one!");?>
                   <a href="albumns/upload">Upload</a> 
                  <?php } ?>
                  </span>
                </div>

              <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
      
  </div>
</div>
</div>
<script type="text/javascript">
  $$(".core_main_album").getParent().addClass("active");
  $$(".custom_305").getParent().addClass("active");
  $$(".custom_317").getParent().addClass("active");
  $$(".custom_328").getParent().addClass("active");
  $$(".custom_951").getParent().addClass("active");
</script>