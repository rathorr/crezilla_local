<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: editphotos.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>



<script type="text/javascript">
    var SortablesInstance;

    en4.core.runonce.add(function() {
      $$('.thumbs_nocaptions > li').addClass('sortable');
      SortablesInstance = new Sortables($$('.thumbs_nocaptions'), {
        clone: true,
        constrain: true,
        //handle: 'span',
        onComplete: function(e) {
          var ids = [];
          $$('.thumbs_nocaptions > li').each(function(el) {
            ids.push(el.get('id').match(/\d+/)[0]);
          });
          //console.log(ids);

          // Send request
          var url = '<?php echo $this->url(array('action' => 'order')) ?>';
          var request = new Request.JSON({
            'url' : url,
            'data' : {
              format : 'json',
              order : ids
            }
          });
          request.send();
        }
      });
    });
  </script>

<style>
#global_content > div.headline
{
  padding-left: 10px;
}
#global_content > div.headline > h2
{
  margin: 0 0 10px 16px !important;
  border-bottom: 1px solid #ccc;
  padding-left: 0px !important;
}
#global_content h2 {
    color: #3faee8;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 700;
    margin: 0;
    padding: 10px 20px;
    text-transform: capitalize;
}
.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:auto;
  width:100%;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}


ul.form-errors > li, ul.form-notices > li {
 float:none;
}

.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
h2.create_pdf
{margin:6px !important;}
h2.create_pdf a
{padding:10px !important;}
input[type="checkbox"] {
    border: medium none;
    display: block;
    float: left;
    margin: 2px 4px 0 0;
    padding: 0;
    width: auto;
}
.pages{ float:right;}
</style>

<div class="headline">
  <h2>
     
      <?php echo $this->translate('Photo Albums');?>
    
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
        echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
		
    ?>
  </div>
</div>

<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle" style="width:100%; background-color: #fff;
    padding: 1% 2%;">
<div class="generic_layout_container layout_core_content">

<h2>
  <?php echo $this->htmlLink($this->album->getHref(), $this->album->getTitle()) ?>
  (<?php echo $this->translate(array('%s photo', '%s photos', $this->album->count()),$this->locale()->toNumber($this->album->count())) ?>)
  
</h2>

<form action="<?php echo $this->escape($this->form->getAction()) ?>" method="<?php echo $this->escape($this->form->getMethod()) ?>" enctype="multipart/form-data" id="form-upload">
    <input type="hidden" name="album_id"  value="<?php echo $this->album->getIdentity();?>"/>
  <?php echo $this->form->album_id; ?>
  <ul class='albums_editphotos thumbs thumbs_nocaptions'>
    <?php foreach( $this->paginator as $photo ): ?>
      <li id="thumbs-photo-<?php echo $photo->photo_id ?>">
        <div class="albums_editphotos_photo">
          <?php echo $this->itemPhoto($photo, 'thumb.normal')  ?>
        </div>
        <div class="albums_editphotos_info">
          <?php
            $key = $photo->getGuid();
            echo $this->form->getSubForm($key)->render($this);
          ?>
          <div class="albums_editphotos_cover">
            <input type="radio" name="cover" value="<?php echo $photo->getIdentity() ?>" <?php if( $this->album->photo_id == $photo->getIdentity() ): ?> checked="checked"<?php endif; ?> />
          </div>
          <div class="albums_editphotos_label">
            <label><?php echo $this->translate('Album Cover');?></label>
        </div>
        </div>
      </li>
    <?php endforeach; ?>
  </ul>
  <?php echo $this->form->submit->render(); ?>
 <!-- <a href="<?php echo $this->baseUrl().'/albums/view/'.$this->album->album_id;?>" class="back_button">Back</a>-->
   <a href="<?php echo $this->baseUrl().'/albums/manage';?>" class="back_button">Back</a>
</form>


<?php if( $this->paginator->count() > 0 ): ?>
  <br />
  <?php echo $this->paginationControl($this->paginator); ?>
<?php endif; ?>

<script type="text/javascript">
jQuery(document).ready(function(){
  jQuery('.album_main_manage').parent().addClass('active');
  $$('.custom_951').getParent().addClass('active');
});

</script>

  

</div>
</div>
</div>