<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: edit.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>

<div class="headline">
  <h2>
    <?php echo $this->translate('Photo Albums');?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>
</div>



<style>
#global_content > div.headline
{
  padding-left: 10px;
}
#global_content > div.headline > h2
{
  margin: 0 0 10px 16px !important;
  border-bottom: 1px solid #ccc;
  padding-left: 0px !important;
}
#global_content h2 {
    color: #3faee8;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 700;
    margin: 0;
    padding: 10px 20px;
    text-transform: capitalize;
}
.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:auto;
  width:100%;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}


ul.form-errors > li, ul.form-notices > li {
 float:none;
}

.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
h2.create_pdf
{margin:6px !important;}
h2.create_pdf a
{padding:10px !important;}
</style>



<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
<?php
  echo $this->form->render();
?>

<script type="text/javascript">
  $$('.core_main_album').getParent().addClass('active');
  $$('.custom_305').getParent().addClass('active');
  $$('.custom_317').getParent().addClass('active');
  $$('.custom_951').getParent().addClass('active');
  function validateeditalbum(){
	
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#title").val()) == ''){
		jQuery("#title").focus();
		jQuery("#title").addClass("com_form_error_validatin");
		return false;
	}
	
	jQuery('#albums_edit').submit();
	
}
</script>
</div>
</div>
</div>
