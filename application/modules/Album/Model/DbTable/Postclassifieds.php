<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Photos.php 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Album_Model_DbTable_Postclassifieds extends Engine_Db_Table
{
  protected $_rowClass = 'Album_Model_Postclassified';
  
  public function getPostclassifiedsSelect(array $params)
  {
    $select = $this->select();
    
    if( !empty($params['album']) && $params['album'] instanceof Album_Model_Album ) {
      $select->where('album_id = ?', $params['album']->getIdentity());
    } else if( !empty($params['album_id']) && is_numeric($params['album_id']) ) {
      $select->where('album_id = ?', $params['album_id']);
    }
    
    if( !isset($params['order']) ) {
      $select->order('order ASC');
    } else if( is_string($params['order']) ) {
      $select->order($params['order']);
    }
	
    return $select;
  }
  
  public function getPostclassifiedPaginator(array $params)
  {
    return Zend_Paginator::factory($this->getPhotoSelect($params));
  }
  
  public function getPostclassifieds($classified_id, $user_id){
	  $select	=	$this->select();
	  
	 
	  $select
	  	->setIntegrityCheck ( false )
        ->from ( array('p' => 'engine4_album_postclassifieds'))      
        ->joinLeft ( array ('f' => 'engine4_storage_files'), 'f.file_id = p.file_id', array('f.storage_path'))  
        ->where('p.classified_id = ?', $classified_id)
        ->where('p.owner_id = ?', $user_id)
        ->order('p.photo_id ASC')
        ;
	  
    $album = $this->fetchAll($select); 
	return $album; 
  }
  
  public function getPostclassifiedStoragePathByFileId($file_id){
	 $select	=	$this->select();
	  
	  $select
	  	->setIntegrityCheck ( false )
        ->from ( array('p' => 'engine4_album_postclassifieds'))      
        ->joinLeft ( array ('f' => 'engine4_storage_files'), 'f.file_id = p.file_id', array('f.storage_path'))  
        ->where('f.file_id = ?', $file_id)
        ;
    $album = $this->fetchRow($select); 
	return $album->storage_path; 
  }

  /* public function getmyClassifieds($user_id,$limit){
    $select = $this->select();
    $select
      ->setIntegrityCheck ( false )
        ->from ( array('p' => 'engine4_album_postclassifieds'))   */   
        /*->joinLeft ( array ('f' => 'engine4_storage_files'), 'f.parent_file_id = p.file_id', array('f.storage_path','p.title','p.description'))  
        ->where('f.type = ?', 'thumb.normal')*/
       /* ->joinLeft ( array ('f' => 'engine4_storage_files'), 'f.file_id = p.file_id', array('f.storage_path','p.title','p.description'))  
        ->where('p.owner_id = ?', $user_id)
        ->order('p.postclassified_id DESC')
        ->limit($limit)
        ;
    
    $album = $this->fetchAll($select); 
  return $album; 
  }*/

  public function getmyClassifieds($user_id,$limit){
    $select = $this->select();
    $select
      ->setIntegrityCheck ( false )
        ->from ( array('p' => 'engine4_album_postclassifieds'), array('p.postclassified_id','p.title','p.description','p.url','p.owner_id','creation_date')) 
        ->where('p.owner_id = ?', $user_id)
        ->order('p.postclassified_id DESC')
        ->limit($limit)
        ;
    
    $album = $this->fetchAll($select); 
  return $album; 
  }
  public function getallClassifieds($user_id,$limit){
    $select = $this->select();
    $select
        ->setIntegrityCheck ( false )
        ->from ( array('p' => 'engine4_album_postclassifieds'), array('p.postclassified_id','p.title','p.description','p.url','p.owner_id','creation_date'))
        //->where("p.owner_id != $user_id")
        ->order('p.postclassified_id DESC')
        ->limit($limit)
        ;
    
    $album = $this->fetchAll($select); 
  return $album; 
  }

}
