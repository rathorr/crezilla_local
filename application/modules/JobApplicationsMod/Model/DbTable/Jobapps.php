<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Categories.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Jobapplicationsmod_Model_DbTable_Jobapps extends Engine_Db_Table
{
  protected $_rowClass = 'Jobapplicationsmod_Model_Jobapp';
  
  public function getCategoriesAssoc()
  {
    $data = array();
    $stmt = $this->select()
        ->from($this)
        ->query()
        ;
    foreach( $stmt->fetchAll() as $category ) {
      $data[$category['app_id']] = $category['user_id'];
    }
    return $data;
  }
}