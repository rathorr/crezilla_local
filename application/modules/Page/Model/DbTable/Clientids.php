<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Transactions.php 27.07.11 15:15 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class Page_Model_DbTable_Clientids extends Engine_Db_Table
{
  protected $_rowClass = 'Page_Model_Clientids';
  public function userClientcode($code){
    $select = $this->select()
         ->where('clientcode=?', $code)
       ->order('clientids_id ASC');
     $result  = $this->fetchRow($select);
   return $result;
 }
   public function getClientcode($pageid){
    //echo $userid;
    $select = $this->select("clientcode")
               ->setIntegrityCheck(false)
              ->where('page_id=?', $pageid);
     $result  = $this->fetchRow($select)->clientcode;
    return $result;

 }

}