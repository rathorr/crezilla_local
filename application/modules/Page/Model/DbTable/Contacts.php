<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Transactions.php 27.07.11 15:15 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class Page_Model_DbTable_Contacts extends Engine_Db_Table
{
  protected $_rowClass = 'Page_Model_Contact';
	
	public function getPageAddresses($page_id)
	{
		//echo $page_id;die;
		if (!$page_id){
			return false;
		}
		
		$select = $this->select()
      	->where('page_id = ?',$page_id)
		->where('is_deleted = ?',0)
      	;
		
		$result	=	 $this->fetchAll($select);
		$maindata=	array();
		if($result){
			foreach($result as $res){
				$data['address_id']		=	$res['contact_id'];
				$data['address']		=	$res['address'];
				$data['phone_number']	=	$res['phone_number'];
				$data['zip_code']		=	$res['zip_code'];
				$data['country']		=	$res['country'];
				$data['page_id']		=	$res['page_id'];
				$data['is_deleted']		=	$res['is_deleted'];
				$data['created_on']		=	$res['created_on'];
				$data['modified_date']	=	$res['modified_date'];	
				$maindata[]	=	$data;
			}	
		}
		return $maindata;
		
	}
	
}