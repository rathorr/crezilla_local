<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ListItems.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Page_Model_DbTable_Dasignatedcategories extends Engine_Db_Table
{
  protected $_rowClass = 'Page_Model_Dasignatedcategories';
  
public function pageDasignatedcategories($page_id){
	  $select	= $this->select()
	       ->where('page_id=?', $page_id)
		   ->where('is_deleted=?', 0)
		   ->order('dasignatedcategories_id ASC');
     $result	=	$this->fetchAll($select);
	 
	 return $result;
 }
 
  public function getpagedcbydcprimaryid($dc_primary_id, $page_id){
	  $select	= $this->select()
	       ->where('page_id=?', $page_id)
		   ->where('dc_primary_id=?', $dc_primary_id)
		   ->where('is_deleted=?', 0)
		   ->order('dasignatedcategories_id DESC');
     $result	=	$this->fetchAll($select);
	 return ($result)?$result[0]['dasignatedcategories_id']:false;
	 
	 //return $result;
 }
 public function deletedesignationbyid($dcid,$page_id)
 {
 	$condition = array(
    'dc_primary_id = ?' => $dcid,
    'page_id = ?' => $page_id
	);
	/*$condition = array(
    'dc_primary_id = ' . $dcid,
    'user_id = ' . $user_id
	);*/
	//print_r($condition);
	$result = $this->delete($condition);
	return $result;
 }

}