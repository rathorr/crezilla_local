<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Transactions.php 27.07.11 15:15 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class Page_Model_DbTable_Awards extends Engine_Db_Table
{
  protected $_rowClass = 'Page_Model_Award';
	
	public function getPageAwards($page_id)
	{
		//echo $page_id;die;
		if (!$page_id){
			return false;
		}
		
		$select = $this->select()
      	->where('page_id = ?',$page_id)
		->where('is_deleted = ?',0)
      	;
		
		$result	=	 $this->fetchAll($select);
		$maindata=	array();
		if($result){
			foreach($result as $res){
				$data['award_id']	=	$res['award_id'];
				$data['award_name']=	$res['award_name'];
				$data['page_id']	=	$res['page_id'];
				$data['is_deleted']	=	$res['is_deleted'];
				$data['created_on']	=	$res['created_on'];	
				$maindata[]	=	$data;
			}	
		}
		return $maindata;
		
	}
	
}