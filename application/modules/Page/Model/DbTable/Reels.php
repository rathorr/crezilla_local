<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Transactions.php 27.07.11 15:15 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class Page_Model_DbTable_Reels extends Engine_Db_Table
{
  protected $_rowClass = 'Page_Model_Reel';
	
	public function getPageReels($page_id)
	{
		if (!$page_id){
			return false;
		}
		
		$select = $this->select()
      	->where('page_id = ?',$page_id)
		->where('is_deleted = ?',0)
      	;
		
		$result	=	 $this->fetchAll($select);
		$maindata=	array();
		if($result){
			foreach($result as $res){
				$data['reel_id']	=	$res['reel_id'];
				$data['reel_path']	=	$res['reel_path'];
				$data['format']		=	$res['format'];
				$data['page_id']	=	$res['page_id'];
				$data['is_deleted']	=	$res['is_deleted'];
				$data['created_on']	=	$res['created_on'];	
				$maindata[]	=	$data;
			}	
		}
		return $maindata;
	}
	
	public function getPageImageReels($page_id)
	{
		if (!$page_id){
			return false;
		}
		
		$select = $this->select()
      	->where('page_id = ?',$page_id)
		->where('is_deleted = ?',0)
		->where('format = ?', 2)
      	;
		
		$result	=	 $this->fetchAll($select);
		$maindata=	array();
		if($result){
			foreach($result as $res){
				$data['reel_id']	=	$res['reel_id'];
				$data['reel_path']	=	$res['reel_path'];
				$data['format']		=	$res['format'];
				$data['page_id']	=	$res['page_id'];
				$data['is_deleted']	=	$res['is_deleted'];
				$data['created_on']	=	$res['created_on'];	
				$maindata[]	=	$data;
			}	
		}
		return $maindata;
	}
	
	public function getPagePdfReels($page_id)
	{
		if (!$page_id){
			return false;
		}
		
		$select = $this->select()
      	->where('page_id = ?',$page_id)
		->where('is_deleted = ?',0)
		->where('format = ?', 1)
      	;
		
		$result	=	 $this->fetchAll($select);
		$maindata=	array();
		if($result){
			foreach($result as $res){
				$data['reel_id']	=	$res['reel_id'];
				$data['reel_path']	=	$res['reel_path'];
				$data['format']		=	$res['format'];
				$data['page_id']	=	$res['page_id'];
				$data['is_deleted']	=	$res['is_deleted'];
				$data['created_on']	=	$res['created_on'];	
				$maindata[]	=	$data;
			}	
		}
		return $maindata;
	}
	
	public function getPageLinkReels($page_id)
	{
		if (!$page_id){
			return false;
		}
		
		$select = $this->select()
      	->where('page_id = ?',$page_id)
		->where('is_deleted = ?',0)
		->where('format = ?', 3)
      	;
		
		$result	=	 $this->fetchAll($select);
		$maindata=	array();
		if($result){
			foreach($result as $res){
				$data['reel_id']	=	$res['reel_id'];
				$data['reel_path']	=	$res['reel_path'];
				$data['format']		=	$res['format'];
				$data['page_id']	=	$res['page_id'];
				$data['is_deleted']	=	$res['is_deleted'];
				$data['created_on']	=	$res['created_on'];	
				$maindata[]	=	$data;
			}	
		}
		return $maindata;
	}
}