<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Transactions.php 27.07.11 15:15 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class Page_Model_DbTable_Clients extends Engine_Db_Table
{
  protected $_rowClass = 'Page_Model_Client';
	
	public function getPageClients($page_id)
	{
		//echo $page_id;die;
		if (!$page_id){
			return false;
		}
		
		$select = $this->select()
      	->where('page_id = ?',$page_id)
		->where('is_deleted = ?',0)
      	;
		
		$result	=	 $this->fetchAll($select);
		$maindata=	array();
		if($result){
			foreach($result as $res){
				$data['client_id']	=	$res['client_id'];
				$data['client_name']=	$res['client_name'];
				$data['page_id']	=	$res['page_id'];
				$data['is_deleted']	=	$res['is_deleted'];
				$data['created_on']	=	$res['created_on'];	
				$maindata[]	=	$data;
			}	
		}
		return $maindata;
		
	}
	
	public function getPageOwnerInfo($page_id)
	{
		//echo $page_id;die;
		if (!$page_id){
			return false;
		}
		
		  $query ="select u.email,u.displayname from engine4_users u  left join engine4_page_pages  p
				 on u.user_id=p.parent_id 
		         where  p.page_id='".$page_id."'  ";
				 
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
 
		
	}
	
}