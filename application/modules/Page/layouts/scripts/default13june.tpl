<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.net/license/
 * @version    $Id: default.tpl 9089 2011-07-21 23:12:11Z john $
 * @author     John
 */
?>

<?php
$wall = '';
if (Engine_Api::_()->getDbTable('modules' ,'hecore')->isModuleEnabled('wall')){
  $wall = $this->partial('_header.tpl', 'wall');
}
?>

<?php echo $this->doctype()->__toString() ?>
<?php $locale = $this->locale()->getLocale()->__toString(); $orientation = ( $this->layout()->orientation == 'right-to-left' ? 'rtl' : 'ltr' ); ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $locale ?>" lang="<?php echo $locale ?>" dir="<?php echo $orientation ?>">
<head>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,200,900' rel='stylesheet' type='text/css'>
  <base href="<?php echo rtrim((constant('_ENGINE_SSL') ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $this->baseUrl(), '/'). '/' ?>" />


  <?php // ALLOW HOOKS INTO META ?>
  <?php echo $this->hooks('onRenderLayoutDefault', $this) ?>


  <?php // TITLE/META ?>
  <?php
  $counter = (int) $this->layout()->counter;

  $request = Zend_Controller_Front::getInstance()->getRequest();
  $this->headTitle()
    ->setSeparator(' - ');
  $pageTitleKey = 'pagetitle-' . $request->getModuleName() . '-' . $request->getActionName()
    . '-' . $request->getControllerName();
  $pageTitle = $this->translate($pageTitleKey);
  if( $pageTitle && $pageTitle != $pageTitleKey ) {
    $this
      ->headTitle($pageTitle, Zend_View_Helper_Placeholder_Container_Abstract::PREPEND);
  }
  $this
    ->headTitle($this->translate($this->layout()->siteinfo['title']), Zend_View_Helper_Placeholder_Container_Abstract::PREPEND)
  ;
  $this->headMeta()
    ->appendHttpEquiv('Content-Type', 'text/html; charset=UTF-8')
    ->appendHttpEquiv('Content-Language', 'en-US');

  // Make description and keywords
  $description = '';
  $keywords = '';

  $description .= ' ' .$this->layout()->siteinfo['description'];
  $keywords = $this->layout()->siteinfo['keywords'];

  if($this->subject() && $this->subject()->getIdentity()){
    // $this->headTitle($this->subject()->title);

    // $description .= ' ' .$this->subject()->getDescription();
    if (!empty($keywords)) $keywords .= ',';
    $keywords .= $this->subject()->getKeywords(',');
  }

  $this->headMeta()->appendName('description', trim($description));
  $this->headMeta()->appendName('keywords', trim($keywords));
  // Get body identity
  if( isset($this->layout()->siteinfo['identity']) ) {
    $identity = $this->layout()->siteinfo['identity'];
  } else {
    $identity = $request->getModuleName() . '-' .
      $request->getControllerName() . '-' .
      $request->getActionName();
  }
  ?>
  <?php echo $this->hooks("onRenderLayoutDefaultSeo", $this) ?>
  <?php echo $this->headTitle()->toString()."\n" ?>
  <?php echo $this->headMeta()->toString()."\n" ?>
  <?php
  if(!is_null($this->page)) {
    // for SEO by Kirill
    // Open Graph

    if( strpos($this->subject()->getPhotoUrl('thumb.normal'), 'http://') === false && strpos($this->subject()->getPhotoUrl('thumb.normal'), 'https://') === false)
      $host_url = (_ENGINE_SSL ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
    elseif( strpos($this->subject()->getPhotoUrl('thumb.normal'), 'http://') === 0 || strpos($this->subject()->getPhotoUrl('thumb.normal'), 'https://') === 0  )
      $host_url = '';

    $settings = Engine_Api::_()->getDbTable('settings', 'core');
    $app_id = $settings->getSetting('core.facebook.appid', false);
    if($app_id) echo '<meta property="fb:app_id" content="' . $app_id  . '"/>' . "\n";
    echo '<meta property="og:title" content="'.$this->subject()->title.'" />'."\n";
    echo '<meta property="og:type" content="website" />'."\n";
    echo '<meta property="og:url" content="'.$this->subject()->getHref().'" />'."\n";
    echo '<meta property="og:image" content="'. $host_url . $this->subject()->getPhotoUrl('thumb.normal') . '" />'."\n";
    echo '<meta property="og:description" content="'.strip_tags($this->subject()->description).'" />'."\n";
    // Open Graph
    // Google
    echo '<meta itemprop="name" content="'.$this->subject()->title.'" />'."\n";
    echo '<meta itemprop="description" content="'.strip_tags($this->subject()->description).'" />'."\n";
    echo '<meta itemprop="image" content="'. $host_url . $this->subject()->getPhotoUrl('thumb.normal') . '" />'."\n";
    // Google

    // for SEO by Kirill
  }
  ?>
  <?php // LINK/STYLES ?>
  <?php
  $this->headLink(array(
      'rel' => 'favicon',
      'href' => ( isset($this->layout()->favicon)
        ? $this->layout()->staticBaseUrl . $this->layout()->favicon
        : '/favicon.ico' ),
      'type' => 'image/x-icon'),
    'PREPEND');
  $themes = array();
  if( !empty($this->layout()->themes) ) {
    $themes = $this->layout()->themes;
  } else {
    $themes = array('default');
  }
  foreach( $themes as $theme ) {
    $this->headLink()
      ->prependStylesheet($this->baseUrl().'/application/css.php?request=application/themes/'.$theme.'/theme.css')
      ->prependStylesheet(rtrim($this->baseUrl(), '/') . '/public/bootstrap/bootstrap.css')
     // ->prependStylesheet(rtrim($this->baseUrl(), '/') . '/public/custom/custom.css')
      //->prependStylesheet(rtrim($this->baseUrl(), '/') . '/public/custom/custom2.css')
      ->prependStylesheet(rtrim($this->baseUrl(), '/') . '/public/custom/device_media.css')
      ->prependStylesheet(rtrim($this->baseUrl(), '/') . '/public/custom/style/ihover.css');
    if( $orientation == 'rtl' ) {
      // @todo add include for rtl
    }
  }
  // Process
  foreach( $this->headLink()->getContainer() as $dat ) {
    if( !empty($dat->href) ) {
      if( false === strpos($dat->href, '?') ) {
        $dat->href .= '?c=' . $counter;
      } else {
        $dat->href .= '&c=' . $counter;
      }
    }
  }
  ?>
  <?php echo $this->headLink()->toString()."\n" ?>
  <?php echo $this->headStyle()->toString()."\n" ?>

  <?php // TRANSLATE ?>
  <?php $this->headScript()->prependScript($this->headTranslate()->toString()) ?>

	<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/jquery.min.js'?>"></script>

  <script type="text/javascript">
  var jQuery	=	$.noConflict();
  </script>
  <?php // SCRIPTS ?>
  <script type="text/javascript">
    <?php echo $this->headScript()->captureStart(Zend_View_Helper_Placeholder_Container_Abstract::PREPEND) ?>

    en4.orientation = '<?php echo $orientation ?>';

    en4.core.language.setLocale('<?php echo $this->locale()->getLocale()->__toString() ?>');

    Date.setServerOffset('<?php echo date('D, j M Y G:i:s O', time()) ?>');

    en4.orientation = '<?php echo $orientation ?>';
    en4.core.environment = '<?php echo APPLICATION_ENV ?>';
    en4.core.language.setLocale('<?php echo $this->locale()->getLocale()->__toString() ?>');
    en4.core.loader = new Element('img', {src: 'application/modules/Core/externals/images/loading.gif'});

    en4.core.setBaseUrl('<?php echo $this->url(array(), 'default', true) ?>');
    en4.core.loader = new Element('img', {src: 'application/modules/Core/externals/images/loading.gif'});

    <?php if( $this->subject() ): ?>
    en4.core.staticBaseUrl = '<?php echo $this->escape($this->layout()->staticBaseUrl) ?>';
    en4.core.subject = {
      type : '<?php echo $this->subject()->getType(); ?>',
      id : <?php echo (int)$this->subject()->getIdentity(); ?>,
      guid : '<?php echo $this->subject()->getGuid(); ?>'
    };
      <?php endif; ?>
    <?php if( $this->viewer()->getIdentity() ): ?>
    en4.user.viewer = {
      type : '<?php echo $this->viewer()->getType(); ?>',
      id : <?php echo $this->viewer()->getIdentity(); ?>,
      guid : '<?php echo $this->viewer()->getGuid(); ?>'
    };
      <?php endif; ?>
    if( <?php echo ( Zend_Controller_Front::getInstance()->getRequest()->getParam('ajax', false) ? 'true' : 'false' ) ?> ) {
      en4.core.dloader.attach();
    }
    <?php echo $this->headScript()->captureEnd(Zend_View_Helper_Placeholder_Container_Abstract::PREPEND) ?>
  </script>
  <script type="text/JavaScript">
function adjustStyle(width) {
	var base_url = '<?php echo $this->baseUrl();?>';
			width = parseInt(width);
			if ((width >= 0) && (width <= 320)) {
				 
				jQuery("#size_stylesheet").attr("href", base_url+"/public/custom/screen_320.css");
				 
			} 
			if  ((width >= 321) && (width <= 480)) {
				jQuery("#size_stylesheet").attr("href", base_url+"/public/custom/screen_480.css");
			}
			
			if  ((width >= 481) && (width <= 640)) {
				jQuery("#size_stylesheet").attr("href", base_url+"/public/custom/screen_640.css");
			} 

			 if ((width >= 641) && (width <= 768)) {
				 
				jQuery("#size_stylesheet").attr("href", base_url+"/public/custom/screen_768.css");
			}
			
			 if ((width >= 769) && (width <= 980)) {
				 
				jQuery("#size_stylesheet").attr("href", base_url+"/public/custom/screen_980.css"); 
			}
			
			if (width > 980) {
				jQuery("#size_stylesheet").attr("href", base_url+"/public/custom/custom.css"); 
				jQuery("#size_stylesheet_other").attr("href", base_url+"/public/custom/custom2.css"); 
			}
}

jQuery(function() {
	adjustStyle(jQuery(this).width());
	jQuery(window).resize(function() {
		adjustStyle(jQuery(this).width());
	});
});

</script>
  <?php
  $this->headScript()
    ->appendFile($this->layout()->staticBaseUrl.'application/modules/Page/externals/scripts/activity.js')
    ->appendFile($this->layout()->staticBaseUrl.'application/modules/Page/externals/scripts/comment.js')
    ->prependFile($this->layout()->staticBaseUrl.'externals/smoothbox/smoothbox4.js')
    ->prependFile($this->layout()->staticBaseUrl.'application/modules/User/externals/scripts/core.js')
    ->prependFile($this->layout()->staticBaseUrl.'application/modules/Core/externals/scripts/core.js')
    ->prependFile($this->layout()->staticBaseUrl.'externals/chootools/chootools.js');

  $modulesTbl = Engine_Api::_()->getDbTable('modules', 'core');
  $coreItem = $modulesTbl->getModule('core')->toArray();

  //Activity
  if (version_compare($coreItem['version'], '4.2.9') < 0) {
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl.'application/modules/Page/externals/scripts/old_activity.js');
  } else {
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl.'application/modules/Page/externals/scripts/activity.js');
  }

  if (version_compare($coreItem['version'], '4.1.7') < 0) {
    $this->headScript()
      ->prependFile($this->layout()->staticBaseUrl.'externals/mootools/mootools-1.2.4.4-more-' . (APPLICATION_ENV == 'development' ? 'nc' : 'yc') . '.js')
      ->prependFile($this->layout()->staticBaseUrl.'externals/mootools/mootools-1.2.4-core-' . (APPLICATION_ENV == 'development' ? 'nc' : 'yc') . '.js');
  } elseif(version_compare($coreItem['version'], '4.2.2') < 0) {
    $this->headScript()
      ->prependFile($this->layout()->staticBaseUrl.'externals/mootools/mootools-1.2.5.1-more-' . (APPLICATION_ENV == 'development' ? 'nc' : 'yc') . '.js')
      ->prependFile($this->layout()->staticBaseUrl.'externals/mootools/mootools-1.2.5-core-' . (APPLICATION_ENV == 'development' ? 'nc' : 'yc') . '.js');
  } else {
    $this->headScript()
      ->prependFile($this->layout()->staticBaseUrl.'externals/mootools/mootools-more-1.4.0.1-full-compat-' . (APPLICATION_ENV == 'development' ? 'nc' : 'yc') . '.js')
      ->prependFile($this->layout()->staticBaseUrl.'externals/mootools/mootools-core-1.4.5-full-compat-' . (APPLICATION_ENV == 'development' ? 'nc' : 'yc') . '.js');
  }

  // Process
  foreach( $this->headScript()->getContainer() as $dat ) {
    if( !empty($dat->attributes['src']) ) {
      if( false === strpos($dat->attributes['src'], '?') ) {
        $dat->attributes['src'] .= '?c=' . $counter;
      } else {
        $dat->attributes['src'] .= '&c=' . $counter;
      }
    }
  }
  $headIncludes = $this->layout()->headIncludes;
  ?>
  <?php echo $this->headScript()->toString()."\n" ?>
  <?php echo $headIncludes ?>

</head>

<style type="text/javascript">

</style>

<body id="global_page_<?php echo $request->getModuleName() . '-' . $request->getControllerName() . '-' . $request->getActionName() ?>" class="devices_media">
  <div class="page-search-results hidden" id="page-search-results"></div>
   <link id="size_stylesheet" rel="stylesheet" type="text/css" href="" ab=""/>
<link id="size_stylesheet_other" rel="stylesheet" type="text/css" href="" />
  <script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/js/slide.js"></script>
      
      <div id="slider" style="right:-342px;display:none;" >
		<!--<div id="sidebar"   onclick="open_panel()">-->
        <div id="sidebar" >
        	<a href="<?php echo $this->baseUrl().'/cms/faq';?>">
			<img src="public/custom/images/contact.png"/>
            </a>
		</div>
		<div id="header">
        	 <img class="help_close" src="public/custom/images/close-help-icon.png" onClick="close_panel()" style="cursor:pointer;"/>
			 <?php //echo $this->content()->renderWidget('core.contact') ?>
		 </div>
	</div>
	
	<script type="text/javascript">
window.onload = function() { 
        document.getElementById('slider').style.display = 'block'; 
};
</script>


	
  <script type="javascript/text">
  function showIt2() {
  document.getElementById("slider").style.visibility = "visible";
}setTimeout("showIt2()", 500000); // after 5 secs

</script>
  
  <div id="global_header" <?php if($this->viewer()->getIdentity()){echo 'class="login"';}else{echo 'class="logout"';} ?>>
    <?php
      echo $this->pageContent('header');
      echo $wall;
    ?>
  </div>
  <div id='global_wrapper' <?php if(Engine_Api::_()->user()->getViewer()->getIdentity()){echo 'class="login_my_wrapper"';}else{echo 'class="logout_my_wrapper"';} ?>>
    <div id='global_content'>
      <?php echo $this->layout()->content ?>
    </div>
  </div>
  <div id="global_footer">
    <?php
      echo $this->pageContent('footer');
    ?>
  </div>
  <div class="col-md-12 copyright_footer_bottom">
	&copy; copyrights 2015 IPRODUCER
  </div>
</body>
</html>
