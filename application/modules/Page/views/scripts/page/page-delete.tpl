<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/js/jquery.min.js"></script>
<script type="text/javascript">

 function remove_parent(obj)
  {
	   
		  var ajax_url = '<?php echo $this->baseUrl()."/page-shared/page-delete/".$this->page_id."";?>';
 		  var page_id = jQuery('#page_id').val();
		  var data_id = jQuery(obj).attr("data_id");
 		  jQuery.ajax({
				url: ajax_url,
				type: 'POST',
				data: { data_id: data_id, page_id: page_id },
				async: false,
				success: function(s){
 					
					var msg= s.split("::");
					if(s!='')
					{
						parent.location.reload();
					  
					}
					else
					{
						jQuery("#signinerror").html(msg[1]);
					}
					
				}
			});
	   
  
  }
  


</script>

<span id="global_content_simple">
    

<form name="share_form" id="share_form" class="global_form_popup">
<input type="hidden" name="page_id" id="page_id" value="<?php echo $this->page_id; ?>" />

<div><div><h3>Delete Page</h3>
<p class="form-description">Are you sure you want to delete this page? </p>
<div class="form-elements">
<div class="form-wrapper" id="buttons-wrapper"><div class="form-label" id="buttons-label">&nbsp;</div><div class="form-element" id="buttons-element"><div id="buttons-wrapper" class="form-wrapper"><fieldset id="fieldset-buttons">

<button type="button" onClick="remove_parent(this)" id="submit" name="submit">Delete</button>

 or <a onclick="parent.Smoothbox.close()" href="javascript:void(0);" type="button" id="cancel" name="cancel">cancel</a></fieldset></div></div></div></div></div></div></form>  </span>