<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: edit.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>

<?php
	$this->headScript()
		->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Observer.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.Local.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.Request.js');
?>
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/externals/tinymce/tinymce.min.js"></script>
 


<script type="text/javascript">

    he_word_completer.prepend_word = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$this->url(array('page_id' => ''), 'page_view').'/'; ?>";
    he_word_completer.title = "title";
    he_word_completer.url = "url";
    he_word_completer.completer = "page_url_placeholder";
    he_word_completer.default_value = "pagename";
    he_word_completer.ajax_url = "<?php echo $this->url(array('action' => 'validate'), 'page_ajax'); ?>";
    he_word_completer.init();
</script>



<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
      'topLevelId' => (int) @$this->topLevelId,
      'topLevelValue' => (int) @$this->topLevelValue
    ))
?>

<?php //echo $this->render('_page_options_menu.tpl'); ?>
<div class='layout_left' style="width: auto;">
  <?php //echo $this->render('_page_edit_tabs.tpl'); ?>
</div>

<div class='layout_middle'>
  <div class="page_basic_info_edit">
    <?php //echo $this->form->render($this); 
    	$pagevalues	=	$this->pagevalues;
    	$pageitem = Engine_Api::_()->getItem('page', $pagevalues['page_id']);
    ?>
      

    
  </div>
  
</div>

 
