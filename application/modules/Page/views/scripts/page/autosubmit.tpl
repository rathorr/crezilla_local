<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: edit.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>

<?php
	$this->headScript()
		->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Observer.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.Local.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.Request.js');
?>
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/externals/tinymce/tinymce.min.js"></script>
 


 



<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
      'topLevelId' => (int) @$this->topLevelId,
      'topLevelValue' => (int) @$this->topLevelValue
    ))
?>

<?php //echo $this->render('_page_options_menu.tpl'); ?>
<div class='layout_left' style="width: auto;">
  <?php //echo $this->render('_page_edit_tabs.tpl'); ?>
</div>

<div class='layout_middle'>
  <div class="page_basic_info_edit">
    <?php //echo $this->form->render($this); 
    	$pagevalues	=	$this->pagevalues;
    	$pageitem = Engine_Api::_()->getItem('page', $pagevalues['page_id']);
        $hostname=$_SERVER['HTTP_HOST'];
        $pageOwnerInfo	=	Engine_Api::_()->getDbTable('clients', 'page')->getPageOwnerInfo($pagevalues['page_id']);
        $pretrimmedrandom = md5(uniqid(mt_rand(),true));
          
          $trimmed =  $pretrimmedrandom;
        
	      $productname='Page Creation';
         
	      $displayname=$pageOwnerInfo[0]['displayname'];
        
	      $email=$pageOwnerInfo[0]['email'];
        
	   
	      $hash=hash('sha512', 'C0Dr8m|'.$trimmed.'|1.000|'.$productname.'|'.$displayname.'|'.$email.'|||||||||||3sf0jURk');
	 
    ?>
    <script>
 

 jQuery(document).ready(function(){

    document.getElementById('payu_form_submit').submit();
 });
</script>

 
 
    
   
	   
<form action="https://test.payu.in/_payment" method='post' id="payu_form_submit">
<input type='hidden' name='firstname' value="<?php echo $displayname;?>" />
<input type='hidden' name='lastname' value='' />
<input type='hidden' name='surl' value='http://<?php echo $hostname;?><?php echo $this->baseUrl();?>/page-pay/success/<?php echo $pagevalues["page_id"];?>' />
<input type='hidden' name='phone' value='9560616526' />
<input type='hidden' name='key' value='C0Dr8m' />
<input type='hidden' name='hash' value = '<?php echo $hash;?>' />
<input type='hidden' name='curl' value='http://<?php echo $hostname;?><?php echo $this->baseUrl();?>/page-pay/cancel/<?php echo $pagevalues["page_id"];?>' />
<input type='hidden' name='furl' value='http://<?php echo $hostname;?><?php echo $this->baseUrl();?>/page-pay/fail/<?php echo $pagevalues["page_id"];?>' />
<input type='hidden' name='txnid' value='<?php echo $trimmed;?>' />
<input type='hidden' name='productinfo' value='Page Creation' />
<input type='hidden' name='amount' value='1.000' />
<input type='hidden' name='email' value='<?php echo $email;?>' />

 
</form> 
    <div>
    <div>
    <div style="padding-top:10%;">
    <h4 style="text-align: center;">redirecting to payment gateway.........</h4>
	</div>
<div class="form-elements">
 
 

 
</div>
</div>
</div>
</div>
</div>
</form>
    
  </div>
  
</div>

 
