<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: edit.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style type="text/css">
body
{
  font-family: 'Roboto', sans-serif !important;
}
#global_content h2 {
    color: #3faee8;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 500;
    margin: 0;
    padding: 10px 20px;
    text-transform: capitalize;
}
.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
      height: 34px;
    line-height: 30px;
    width: 100%;
    border-radius: 0;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}


ul.form-errors > li, ul.form-notices > li {
 float:none;
}

.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
h2.create_pdf
{margin:6px !important;}
h2.create_pdf a
{padding:10px !important;}

#mulitplefileuploader span b{display:none;}
.ajax-file-upload-error{float: left; margin-left: 148px; color: #FF0000; font-weight: bold;}
.fancybox-custom .fancybox-skin {box-shadow: 0 0 50px #222;}
#edit_page_form
{
  width: 50%;
  margin-left: 25%;
}
.global_form div.form-label {
     width: 100%; 
    /* text-align: left !important; */
    /* padding: 4px 15px 0 2px; */
     margin-bottom: 10px; 
    /* overflow: hidden; */
     float:none; 
    /* clear: left; */
    /* font-size: .9em; */
     font-weight: 500; 
     color: #777; 
}
label
{
  font-family: 'Roboto', sans-serif !important;
  font-weight: 500 !important;
}
.SumoSelect {
  width: 100% !important;
}
.SumoSelect > .CaptionCont {
  border-radius: 0px !important;
  background-color: #fff !important;
  max-width: 100% !important;
}
.SumoSelect > .optWrapper {
  background: #fff !important;
}

@media screen and (max-width :767px)
{ 
  #edit_page_form {
    width: 100%;
    margin-left: 0%; 
}
.addSkills {
  background-position: 40px 7px;
  width: 56px;
  right: 0px;
  }
  .ajax-upload-dragdrop
  {
    width: 30% !important;
  }
  .ajax-file-upload 
  {
    padding: 6px 24px !important;
  }
  .global_form .form-elements input#photo,.global_form .form-elements input#cover
  {
    width: 10% !important;
  }
}
	</style>
<?php
	$this->headScript()
		->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Observer.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.Local.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.Request.js');
?>

<script type="text/javascript">

    he_word_completer.prepend_word = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$this->url(array('page_id' => ''), 'page_view').'/'; ?>";
    he_word_completer.title = "title";
    he_word_completer.url = "url";
    he_word_completer.completer = "page_url_placeholder";
    he_word_completer.default_value = "pagename";
    he_word_completer.ajax_url = "<?php echo $this->url(array('action' => 'validate'), 'page_ajax'); ?>";
    he_word_completer.init();
</script>


 
<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
      'topLevelId' => (int) @$this->topLevelId,
      'topLevelValue' => (int) @$this->topLevelValue
    ))
?>

<div class="headline">

<?php echo $this->render('_page_options_menu.tpl'); ?>
</div>

<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle" style="width:100%; background-color: #fff;
    padding: 1% 2%;">
<div class="generic_layout_container layout_core_content">


<div class='layout_middle' style="width:100%;">
  <div class="page_basic_info_edit">
    <?php 
    	$pagevalues	=	$this->pagevalues;
    	$pageitem = Engine_Api::_()->getItem('page', $pagevalues['page_id']);
        $categoryList = Engine_Api::_()->getDbtable('pages', 'page')->getPageCategory();
    ?>
    <form method="post" action="<?php echo $this->baseUrl();?>/page-team/edit/<?php echo $pagevalues['page_id'];?>" class="global_form" onsubmit="return validateeditpage();" id="edit_page_form" enctype="multipart/form-data">
   <div>
    <div>
    

<div class="form-elements">
<h2>Edit Information</h2>
<div class="form-wrapper" id="title-wrapper">
<div class="form-label" id="title-label">
<label class="required" for="title">Title *</label>
</div>
<div class="form-element" id="title-element">
<input type="text" value="<?php echo $pagevalues['displayname'];?>" id="title" name="title">
</div>
</div>


<div class="form-wrapper" id="url-wrapper">
<div class="form-label" id="url-label">
<label class="required" for="url">URL *</label>
</div>
<div class="form-element" id="url-element">
<input type="text" value="<?php echo $pagevalues['url'];?>" id="url" name="url" title="<?php echo $pagevalues['page_id'];?>">
<p class="description">You can set custom destination to make it easier for people to find your Page. The URL may only contain alphanumeric characters and dashes. The full url will be <span id="page_url_placeholder"><?php echo $this->baseUrl();?>/page/pagename</span>
</p>
</div>
</div>

<div class="form-wrapper" id="website-wrapper">
<div class="form-label" id="website-label">
<label class="optional" for="website">Category</label>
</div>
<div class="form-element" id="website-element">
<select name="category_id" id="category_id">
<?php
foreach($categoryList as $key => $val)
{
?>
<option value="<?php echo $val['option_id'];?>" <?php if($pagevalues['category_id']==$val['option_id']){?> selected="selected" <?php }?>><?php echo $val['label'];?></option>
<?php
}
?>
</select>
</div>
</div>

<div class="form-wrapper" id="photo-wrapper">
    	<div class="form-label" id="photo-label">
        	<label class="optional" for="photo">Profile Picture</label>
        </div>
	<div class="form-element" id="photo-element">
	<input type="hidden" id="MAX_FILE_SIZE" value="786432000" name="MAX_FILE_SIZE">
		<input type="file" id="photo" name="photo">
        <?php //echo $this->itemPhoto($pageitem, 'thumb.normal', $pageitem->getTitle());?>
        <?php 
        	$companypic	=	Engine_Api::_()->getDbtable('users', 'user')->getUserPageProfilePicPath($pagevalues['user_id'], $pagevalues['photo_id'], $pagevalues['page_id']);
        	$companypic	=	($companypic=='')?'application/modules/Page/externals/images/nophoto_page_thumb_profile.png':$companypic;
        ?>
        <a style=" border: 0 none;display: block;
    padding-bottom: 100%;position: relative;width: 100%;" class="feed_item_thumb" href="javascript:void(0);" ><span style="background-image: url(<?php echo $companypic; ?>); background-position: center center;background-repeat: no-repeat;background-size: cover;border: 1px solid #ccc;box-sizing: border-box;display: block;
    height: 100%;left: 0;position: absolute;top: 0;width: 100%;"></span></a>
      </div>
   </div>

<div class="form-wrapper" id="photo-wrapper">
    	<div class="form-label" id="photo-label">
        	<label class="optional" for="photo">Cover Picture</label>
        </div>
	<div class="form-element" id="photo-element">
		<input type="file" id="cover" name="cover">
       <!-- <img src="<?php echo $this->coverPhoto['photoSrc']; ?>" />-->
       
       <a style=" border: 0 none;display: block;
    padding-bottom: 100%;position: relative;width: 100%;" class="feed_item_thumb" href="javascript:void(0);" ><span style="background-image: url(<?php echo $this->coverPhoto['photoSrc']; ?>); background-position: center center;background-repeat: no-repeat;background-size: cover;border: 1px solid #ccc;box-sizing: border-box;display: block;
    height: 100%;left: 0;position: absolute;top: 0;width: 100%;"></span></a>
        
      </div>
   </div>

<div class="form-wrapper" id="description-wrapper">
<div class="form-label" id="description-label">
<label class="optional" for="description">About</label>
</div>
<div class="form-element" id="description-element">
<textarea id="description" name="description"  aria-hidden="true"><?php echo $pagevalues['description'];?></textarea></div>
</div>


<div class="form-wrapper" id="services-wrapper">
<div class="form-label" id="services-label">
<label class="optional" for="services">Description of Services</label>
</div>
<div class="form-element" id="services-element">
<textarea id="services" name="services" style="width: 553px;"  aria-hidden="true"><?php echo $pagevalues['services'];?></textarea></div>
</div>
<!-- Designation Start -->
<div class="form-wrapper" id="role-wrapper">
      <div class="form-label" id="role-label" style="display:block !important;">
          <label class="optional" for="role">Categories:</label>
         </div>
  <div class="form-element" id="role-element">
 <input type="hidden" name="desig_ids" id="desig_ids" value="">
<select class="field_container designations" id="designation"  multiple="multiple">
  <?php if($this->dasignatedsubcategories){
      foreach($this->dasignatedsubcategories  as $key=>$val){?>
        <option  value="<?php echo $key;?>" <?php if(in_array($key, $this->userdc)){echo 'selected="selected"';}?>><?php echo $val?></option>
      <?php }
   }?>
</select>
  </div>
    </div>
    
<!-- Designation end-->
<div class="form-wrapper" id="website-wrapper">
<div class="form-label" id="website-label">
<label class="optional" for="website">Website</label>
</div>
<div class="form-element" id="website-element">
<input type="text" value="<?php echo $pagevalues['website'];?>" id="website" name="website">
</div>
</div>

<?php $address	=	$this->addresses;?>
<div class="form-wrapper" id="address-wrapper">
<div class="form-label" id="address-label">
<label class="optional" for="address">Address</label>
</div>
<div class="form-element" id="address-element">
<input type="text" value="<?php echo ($address[0])?$address[0]['address']:'';?>" class="address" name="address[]" placeholder="Enter Address">
</div>
<div class="merge_address"><!-- merge start-->
<div class="form-element" id="phone-element">
<input type="text" value="<?php echo ($address[0])?$address[0]['phone_number']:'';?>" class="phone_number" name="phone_number[]" placeholder="Enter Phone Number">
</div>

<div class="form-element" id="zip-element">
<input type="text" value="<?php echo ($address[0])?$address[0]['zip_code']:'';?>" class="zip_code" name="zip_code[]" placeholder="Enter Pin Code">
</div>

<div class="form-element" id="country-element">
<input type="text" value="<?php echo ($address[0])?$address[0]['country']:'';?>" class="country" name="country[]" placeholder="Enter Country">
</div>
</div><!-- merge end-->
<input type="hidden" name="add_id[]" value="<?php echo ($address[0])?$address[0]['address_id']:'';?>" />
<span class="add_more_address addSkills" title="Add More" style="background-color:transparent;">Add</span>


<?php 
if($address) {$i=1;
    foreach($address as $addr){
       if($i>1) {?>
       <div id="<?php echo $addr['address_id'];?>" class="add_field">
       <div id="address-label" class="form-label">
		<label for="address" class="optional"></label>
		</div>
       <div class="form-element" id="address-element">
        <input type="text" value="<?php echo $addr['address'];?>" class="address" name="address[]" placeholder="Enter Address">
        </div>
        <div class="merge_address">
        <div class="form-element" id="phone-element">
        <input type="text" value="<?php echo $addr['phone_number'];?>" class="phone_number" name="phone_number[]" placeholder="Enter Phone Number">
        </div>
        
        <div class="form-element" id="zip-element">
        <input type="text" value="<?php echo $addr['zip_code'];?>" class="zip_code" name="zip_code[]" placeholder="Enter Pin Code">
        </div>
        
        <div class="form-element" id="country-element">
        <input type="text" value="<?php echo $addr['country'];?>" class="country" name="country[]" placeholder="Enter Country">
        </div>
        </div>
	<input type="hidden" name="add_id[]" value="<?php echo $addr['address_id']?>" />
     <span class="delete_address_record" style="cursor:pointer;">X</span> 
     </div> 
  <?php } 
  $i++;}
}?>
<input type="hidden" name="remove_address_ids" value="" id="remove_address_ids"/>

</div>


   <div class="form-wrapper key-client-add" id="client-wrapper">
    	<div class="form-label" id="client-label">
        	<label class="optional" for="client">Key Client(s):</label>
        </div>
        <?php $clients	=	$this->key_clients;?>
	<div class="form-element main_client_div" id="client-element">
		
        <div class="form-element add-more-client client-element" id="client-element">
        <input type="text" id="client" name="client[]" value="<?php echo ($clients[0])?$clients[0]['client_name']:'';?>">
        <input type="hidden" name="client_id[]" value="<?php echo ($clients[0])?$clients[0]['client_id']:'';?>" /> 
        </div>
        
        <?php 
        if($this->key_clients){$i=1;
        	foreach($this->key_clients as $client){
            	if($i>1){?>
               		 <div class="form-element add-more-client client-element" id="<?php echo $client['client_id'];?>">
                     <input type="text" id="client" name="client[]" value="<?php echo $client['client_name'];?>">
                     <input type="hidden" name="client_id[]" value="<?php echo $client['client_id'];?>"  />
                     <span class="delete_client_record edit_delete_project_record"> x</span>
                     </div>
               <?php  }
            $i++;}
        }
        ?>
        <input type="hidden" name="remove_client_ids" value="" id="remove_client_ids"/>
      </div>
    <span id="add_more_client" class="addSkills" title="Add More" style="background-color:transparent;">Add</span>
   </div>
   
   <div class="form-wrapper key-project-add" id="project-wrapper">
    	<div class="form-label" id="project-label">
        	<label class="optional" for="project">Key Project(s):</label>
        </div>
        <?php $projects	=	$this->key_projects;?>
	<div class="form-element main_project_div" id="project-element">
		
        <div class="form-element add-more-projec project-element" id="project-element">
        <input type="text" id="project" name="project[]" value="<?php echo ($projects[0])?$projects[0]['project_name']:'';?>">
        <input type="hidden" name="project_id[]" value="<?php echo ($projects[0])?$projects[0]['project_id']:'';?>" /> 
        </div>
        
        <?php 
        if($this->key_projects){$i=1;
        	foreach($this->key_projects as $project){
            	if($i>1){?>
               		 <div class="form-element add-more-projec project-element" id="<?php echo $project['project_id'];?>">
                     <input type="text" id="project" name="project[]" value="<?php echo $project['project_name'];?>">
                     <input type="hidden" name="project_id[]" value="<?php echo $project['project_id'];?>"  />
                     <span class="delete_project_record edit_delete_project_record"> x</span>
                     </div>
               <?php  }
            $i++;}
        }
        ?>
        <input type="hidden" name="remove_project_ids" value="" id="remove_project_ids"/>
      </div>
    <span id="add_more_project" class="addSkills" title="Add More" style="background-color:transparent;">Add</span>
   </div>
   
   <div class="form-wrapper key-award-add" id="award-wrapper">
    	<div class="form-label" id="award-label">
        	<label class="optional" for="award">Key Award(s):</label>
        </div>
        <?php $awards	=	$this->key_awards;?>
	<div class="form-element main_award_div" id="award-element">
		
        <div class="form-element add-more-awar award-element" id="award-element">
        <input type="text" id="award" name="award[]" value="<?php echo ($awards[0])?$awards[0]['award_name']:'';?>">
        <input type="hidden" name="award_id[]" value="<?php echo ($awards[0])?$awards[0]['award_id']:'';?>" /> 
        </div>
        
        <?php 
        if($this->key_awards){$i=1;
        	foreach($this->key_awards as $award){
            	if($i>1){?>
               		 <div class="form-element add-more-awar award-element" id="<?php echo $award['award_id'];?>">
                     <input type="text" id="award" name="award[]" value="<?php echo $award['award_name'];?>">
                     <input type="hidden" name="award_id[]" value="<?php echo $award['award_id'];?>"  />
                     <span class="delete_award_record edit_delete_award_record"> x</span>
                     </div>
               <?php  }
            $i++;}
        }
        ?>
        <input type="hidden" name="remove_award_ids" value="" id="remove_award_ids"/>
      </div>
    <span id="add_more_award" class="addSkills" title="Add More" style="background-color:transparent;">Add</span>
   </div>
   
   
   <div class="form-wrapper key-reel" id="reel-wrapper">
    	<div class="form-label" id="reel-label">
        	<label class="optional" for="project">Add Showreel</label>
        </div>
	</div>
   
   <div class="form-wrapper key-award-add" id="showlink-wrapper">
    	<div class="form-label" id="showlink-label">
        	<label class="optional" for="showlink">Video/Audio Link(s):</label>
        </div>
        <?php $links	=	$this->key_linkreels;?>
	<div class="form-element main_showlink_div" id="showlink-element">
		
        <div class="form-element add-more-showlin" id="showlink-element">
        <input type="text" id="showlink" name="showlink[]" value="<?php echo ($links[0])?$links[0]['reel_path']:'';?>" <?php if(!$links[0]['reel_path']){echo "style='max-width:100% !important;';";}?>>
        <?php if($links[0]['reel_path']){?>
        	<a class="fancybox-media" href="<?php echo $links[0]['reel_path'];?>" title="View"></a>
        <?php } ?>
        <input type="hidden" name="showlink_id[]" value="<?php echo ($links[0])?$links[0]['reel_id']:'';?>" /> 
        </div>
        
        <?php 
        if($this->key_linkreels){$i=1;
        	foreach($this->key_linkreels as $showlink){
            	if($i>1){?>
               		 <div class="form-element add-more-showlin" id="<?php echo $showlink['reel_id'];?>">
                     <input type="text" id="showlink" name="showlink[]" value="<?php echo $showlink['reel_path'];?>">
                     <a class="fancybox-media" href="<?php echo $showlink['reel_path'];?>" title="View"></a>
                     <input type="hidden" name="showlink_id[]" value="<?php echo $showlink['reel_id'];?>"  />
                     <span class="delete_showlink_record edit_delete_showlink_record"> x</span>
                     </div>
               <?php  }
            $i++;}
        }
        ?>
        <input type="hidden" name="remove_showlink_ids" value="" id="remove_showlink_ids"/>
      </div>
    <span id="add_more_showlink" title="Add More" class="addSkills" style="background-color:transparent;">Add</span>
   </div>
   
   <div class="form-wrapper key-reel" id="reel-wrapper">
    	<div class="form-label" id="reel-label">
        	<label class="optional" for="project">Photo/Pdf(s):</label>
        </div>
        
         <div>
            <div id="mulitplefileuploader">Upload</div>
            <div id="status"></div>
        </div>
	<div class="form-element" id="reel-element">
    <!-- IMAGES -->
   <ul id="key_reels" class="videos_browse img_reels">
      <?php $i=1;foreach( $this->key_imagereels as $reel ):?>
      
      <li class="video" style="float: left;margin-right: 20px;max-width: 300px;overflow: hidden;" id="<?php echo $reel['reel_id'];?>">
         <a class="fancybox" href="<?php echo $this->baseUrl().'/public/page/reels/'.$reel['reel_path'];?>" data-fancybox-group="gallery">
         	<img src="<?php echo $this->baseUrl().'/public/page/reels/'.$reel['reel_path'];?>" alt="" />
         </a>
    	<a href="javascript:void(0);" class="delete_reel_record">X</a> 
        </li>
      <?php $i++; endforeach; ?>
      <input type="hidden" name="remove_reel_ids" value="" id="remove_reel_ids"/>
    </ul>
    
    <!-- PDF LINKS -->
    <ul id="key_reels" class="videos_browse pdf_reels">
      <?php $i=1;foreach( $this->key_pdfreels as $reel1 ):?>
      
      <li class="video" style="float: left;margin-right: 20px;max-width: 300px;overflow: hidden;" id="<?php echo $reel['reel_id'];?>">
         <a href="<?php echo $this->baseUrl().'/public/page/reels/'.$reel1['reel_path'];?>" target="_blank">
         <img src="<?php echo $this->baseUrl().'/public/custom/images/PDF-icon.png';?>" alt="" style="width:75px; height:75px;" />
         </a>
         
       <a href="javascript:void(0);" class="delete_reel_record">X</a> 
        </li>
      <?php $i++; endforeach; ?>
    </ul>
    
    </div>
   
   
   
<div class="form-wrapper" id="buttons-wrapper">
<div class="form-label" id="buttons-label">&nbsp;</div>
<div class="form-element" id="buttons-element">
<input type="hidden" name="file_name" value="" id="file_name" />
<button type="submit" id="execute" name="execute" class="save-changes">Save Changes</button>

<a href="<?php echo $this->baseUrl();?>/page/<?php echo $pagevalues['url'];?>" type="button" id="cancel" name="cancel">cancel</a>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
    
  </div>
  
</div>
<link href="<?php echo $this->baseUrl();?>/public/uploadify/uploadfile.css" rel="stylesheet">
<script src="<?php echo $this->baseUrl();?>/public/uploadify/jquery.uploadfile.min.js"></script>
<script src="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.js"></script>
<link href="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox-media.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.css" media="screen" />
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('.fancybox').fancybox();
	
	jQuery('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});
});

jQuery('.delete_address_record').on('click', function(){
	var org_address_ids	=	jQuery('#remove_address_ids').val();
	var address_id		=	jQuery(this).parent().attr('id');
	jQuery('#remove_address_ids').val(org_address_ids+address_id+',');
	jQuery(this).parent().remove();
	
});

jQuery('.delete_client_record').on('click', function(){
	var org_client_ids	=	jQuery('#remove_client_ids').val();
	var client_id		=	jQuery(this).parent().attr('id');
	jQuery('#remove_client_ids').val(org_client_ids+client_id+',');
	jQuery(this).parent().remove();
	
});

jQuery('.delete_project_record').on('click', function(){
	var org_project_ids	=	jQuery('#remove_project_ids').val();
	var project_id		=	jQuery(this).parent().attr('id');
	jQuery('#remove_project_ids').val(org_project_ids+project_id+',');
	jQuery(this).parent().remove();
	
});

jQuery('.delete_award_record').on('click', function(){
	var org_award_ids	=	jQuery('#remove_award_ids').val();
	var award_id		=	jQuery(this).parent().attr('id');
	jQuery('#remove_award_ids').val(org_award_ids+award_id+',');
	jQuery(this).parent().remove();
	
});

jQuery('.delete_showlink_record').on('click', function(){
	var org_showlink_ids	=	jQuery('#remove_showlink_ids').val();
	var showlink_id		=	jQuery(this).parent().attr('id');
	jQuery('#remove_showlink_ids').val(org_showlink_ids+showlink_id+',');
	jQuery(this).parent().remove();
	
});

jQuery('.delete_reel_record').on('click', function(){
	var org_reel_ids	=	jQuery('#remove_reel_ids').val();
	var reel_id			=	jQuery(this).parent().attr('id');
	jQuery('#remove_reel_ids').val(org_reel_ids+reel_id+',');
	jQuery(this).parent().remove();
	
});



jQuery('.add_more_address').on('click', function(){
	var text	=	'<div class="new_add"><div class="form-element" id="address-element"><input type="text" value="" class="address" name="address[]" placeholder="Enter Address"></div><div class="form-element" id="phone-element"><input type="text" value="" class="phone_number" name="phone_number[]" placeholder="Enter Phone Number"></div><div class="form-element" id="zip-element"><input type="text" value="" class="zip_code" name="zip_code[]" placeholder="Enter Pin Code"></div><div class="form-element" id="country-element"><input type="text" value="" class="country" name="country[]" placeholder="Enter Country"></div><input type="hidden" name="add_id[]" value="" /><span class="delete_add" style="cursor:pointer;">X</span></div>';
jQuery('#address-wrapper').append(text);	
});

jQuery(document).on('click','.delete_doc',function(){
	jQuery(this).parent().remove();	
});

jQuery(document).on('click','.delete_add',function(){
	jQuery(this).parent().remove();	
});

jQuery('#add_more_client').on('click',function(){
	
jQuery('.main_client_div').append('<div class="form-element add-more-clien client-element" id="client-element"><input type="text" id="client" name="client[]"><input type="hidden" name="client_id[]" value=""  /><span class="delete_doc">x</span></div>');	
});	

jQuery('#add_more_project').on('click',function(){
	
jQuery('.main_project_div').append('<div class="form-element add-more-projec project-element" id="project-element"><input type="text" id="project" name="project[]"><input type="hidden" name="project_id[]" value="" /><span class="delete_doc">x</span></div>');	
});

jQuery('#add_more_award').on('click',function(){
	
jQuery('.main_award_div').append('<div class="form-element add-more-awar award-element" id="award-element"><input type="text" id="award" name="award[]"><input type="hidden" name="award_id[]" value="" /><span class="delete_doc">x</span></div>');	
});

jQuery('#add_more_showlink').on('click',function(){
	
jQuery('.main_showlink_div').append('<div class="form-element add-more-showlin " id="showlink-element"><input type="text" id="showlink" name="showlink[]"><input type="hidden" name="showlink_id[]" value="" /><span class="delete_doc edit_delete_showlink_record">x</span></div>');	
});

function validateeditpage(){
	 
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#title").val()) == ''){
			//$("#signinerror").html("Email is required.");
			//$("#signinerror").show();
			jQuery("#title").focus();
			jQuery("#title").addClass("com_form_error_validatin");
			//$("#signinerror").fadeOut(7000);
			return false;
	}
	if(jQuery.trim(jQuery.trim(jQuery("#url").val())) == '' || jQuery("#url").hasClass("error")==true){
 		jQuery("#url").focus();
		jQuery("#url").addClass("com_form_error_validatin");
		//$('#email').focus();
		//$('#email').addClass('error');
		return false;
  	 }
	//return false;
	//jQuery('#create_page_form').submit();	  
  	
}
</script>
<?php 
if($pageitem['user_id'] == Engine_Api::_()->user()->getViewer()->getIdentity()){?>
<script type="text/javascript">
    	  jQuery(document).ready(function(){
			  var page_id	=	"<?php echo $pageitem['page_id'] ?>";
			  jQuery('ul#pagenames .active').removeClass('active');
			  jQuery('.page_active_'+page_id).addClass('active');
				//var data	=	'<img src="'+jQuery('#pic').val()+'"/>';
				var img		=	'<?php echo $this->itemPhoto($pageitem,"thumb.icon");?>';
				jQuery('.profile_icon span').html(img+'<img src="<?php echo $this->baseUrl();?>/public/custom/images/profile-dd-arrow.png">');  
			});
</script>	
<?php } ?>



<!--<link rel="stylesheet" href="<?php echo $this->baseUrl().'/public/html5lightbox/jquery.js';?>" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/html5lightbox/html5lightbox.js'?>"></script>
-->
<script type="text/javascript">
jQuery(document).ready(function()
{
   jQuery('.designations').SumoSelect({selectAll: false,placeholder: 'Select Only 5 Designations'});
setTimeout(function(){ 
     jQuery('#desig_ids').val(jQuery('#designation').val());
  }, 1000);

 var settings = {
	url: "<?php echo $this->baseUrl();?>/page-ajax/addreel",
	method: "POST",
	allowedTypes:"jpg,png,jpeg,pdf",
	fileName: "myfile",
	multiple: true,
	onSuccess:function(files,data,xhr)
	{
		var string=jQuery('#file_name').val();
  		jQuery("#file_name").val(data);
		if(string!="")
		{
 		var res = "".concat(string,"||",data);
		jQuery("#file_name").val(res);
		}
		else
		{
			jQuery("#file_name").val();	
		}
 		//jQuery("#status").html("<font color='green'>Upload is success</font>");
		
	},
	onError: function(files,status,errMsg)
	{		
		jQuery("#status").html("<font color='red'>Upload is Failed</font>");
	}
}
jQuery("#mulitplefileuploader").uploadFile(settings);


});
jQuery(document).on('click', '#designation', function(){
  jQuery('#desig_ids').val(jQuery(this).val());
});
</script>



</div>
</div>
</div>