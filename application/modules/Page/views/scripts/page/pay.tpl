<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: edit.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>

 
 
 


 



<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
      'topLevelId' => (int) @$this->topLevelId,
      'topLevelValue' => (int) @$this->topLevelValue
    ))
?>

<?php //echo $this->render('_page_options_menu.tpl'); ?>
<div class='layout_left' style="width: auto;">
  <?php //echo $this->render('_page_edit_tabs.tpl'); ?>
</div>

<div class='layout_middle'>
  <div class="page_basic_info_edit">
    <?php //echo $this->form->render($this); 
    	$pagevalues	=	$this->pagevalues;
    	$pageitem = Engine_Api::_()->getItem('page', $pagevalues['page_id']);
    ?>
    <form method="post" action="<?php echo $this->baseUrl();?>/page-pay/autosubmit/<?php echo $pagevalues['page_id'];?>" class="global_form"   id="edit_page_form" enctype="multipart/form-data">
    
    <input type="hidden" name="page_id_pay" value="<?php echo $pagevalues['page_id'];?>"  />
    <div>
    <div>
    <div style="padding-top:10%;">
    <h4>To Publish your page you have to pay Rs.10 per page.</h4>
    </div>

<div class="form-elements">
 <div align="center" style="text-align:center;"><button type="submit" id="execute" name="execute" class="save-changes">Proceed to pay</button></div>
  
</div>
</div>
</div>
</div>
</div>
</form>
    
  </div>
  
</div>

 
