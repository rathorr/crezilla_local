<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: edit.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>

<?php
	$this->headScript()
		->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Observer.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.Local.js')
    ->appendFile($this->layout()->staticBaseUrl.'externals/autocompleter/Autocompleter.Request.js');
?>
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/externals/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.tiny",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>


 



<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
      'topLevelId' => (int) @$this->topLevelId,
      'topLevelValue' => (int) @$this->topLevelValue
    ))
?>

<?php //echo $this->render('_page_options_menu.tpl'); ?>
<div class='layout_left' style="width: auto;">
  <?php //echo $this->render('_page_edit_tabs.tpl'); ?>
</div>
 

<?php //echo $this->form->render($this); 
    	$pagevalues	=	$this->pagevalues;
     	$pageitem = Engine_Api::_()->getItem('page', $pagevalues['page_id']);
?>

 
 <form method="post" action="<?php echo $this->baseUrl();?>/page-pay/autosubmit/<?php echo $pagevalues['page_id'];?>" class="global_form"   id="edit_page_form" enctype="multipart/form-data">
<div class='layout_middle'>
  <div class="page_basic_info_edit">
     
     

 
 
    
   
	   
  
    <div>
    <div>
    <div style="padding-top:10%;">
    <h4 style="color:#030;text-align: center;">Your transaction was successfull.</h4>
	</div>
<div class="form-elements">
 
  

 
</div>
</div>
</div>
</div>
</div>
</form>
    
  </div>
  
</div>

<script type="text/javascript">
jQuery('.delete_client_record').on('click', function(){
	var org_client_ids	=	jQuery('#remove_client_ids').val();
	var client_id		=	jQuery(this).parent().attr('id');
	jQuery('#remove_client_ids').val(org_client_ids+client_id+',');
	jQuery(this).parent().remove();
	
});

jQuery('.delete_project_record').on('click', function(){
	var org_project_ids	=	jQuery('#remove_project_ids').val();
	var project_id		=	jQuery(this).parent().attr('id');
	jQuery('#remove_project_ids').val(org_project_ids+project_id+',');
	jQuery(this).parent().remove();
	
});

jQuery('.delete_document_record').on('click', function(){
	var org_document_ids	=	jQuery('#remove_document_ids').val();
	var document_id			=	jQuery(this).parent().attr('id');
	jQuery('#remove_document_ids').val(org_document_ids+document_id+',');
	jQuery(this).parent().remove();
	
});

jQuery(document).on('change','.document_file',function(){
	jQuery(this).prev().val('Document');
});

jQuery(document).on('change','.document_name',function(){
	if(jQuery(this).val()==''){
		jQuery(this).val('Document');
	}
});

jQuery('#add_more_document').on('click',function(){
	
jQuery(this).before('<div class="form-element" id="document-element"><input type="text" name="document_name[]" class="document_name"><input type="file"  name="document[]" class="document_file"><span class="delete_doc">Remove(-)</span></div>');	
});	
jQuery(document).on('click','.delete_doc',function(){
	jQuery(this).parent().remove();	
});

jQuery('#add_more_client').on('click',function(){
	
jQuery(this).before('<div class="form-element" id="client-element"><input type="text" id="client" name="client[]"><span class="delete_doc">Remove(-)</span></div>');	
});	

jQuery('#add_more_project').on('click',function(){
	
jQuery(this).before('<div class="form-element" id="project-element"><input type="text" id="project" name="project[]"><span class="delete_doc">Remove(-)</span></div>');	
});

function validateeditpage(){
	 
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#title").val()) == ''){
			//$("#signinerror").html("Email is required.");
			//$("#signinerror").show();
			jQuery("#title").focus();
			jQuery("#title").addClass("com_form_error_validatin");
			//$("#signinerror").fadeOut(7000);
			return false;
	}
	if(jQuery.trim(jQuery.trim(jQuery("#url").val())) == '' || jQuery("#url").hasClass("error")==true){
 		jQuery("#url").focus();
		jQuery("#url").addClass("com_form_error_validatin");
		//$('#email').focus();
		//$('#email').addClass('error');
		return false;
  	 }
	//return false;
	//jQuery('#create_page_form').submit();	  
  	
}
</script>
