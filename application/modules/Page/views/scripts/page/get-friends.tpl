<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: create.tpl 10110 2013-10-31 02:04:11Z andres $
 * @author     Jung
 */
?>

<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/jquery.min.js'?>"></script>

  <script type="text/javascript">
  var jQuery	=	$.noConflict();
  </script>

<script type="text/javascript">

  function validatevlassified(){
		
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#title").val()) == ''){
			jQuery("#title").focus();
			jQuery("#title").addClass("com_form_error_validatin");
			return false;
	}
	
	if(jQuery.trim(jQuery(".field_18:checked").length) < 1){
			jQuery("#signinerror").html("Please select talent type.");
			jQuery("#signinerror").show();
			jQuery("#signinerror").fadeOut(7000);
			jQuery(window).scrollTop(0);
			return false;
	}
	
	if(jQuery.trim(jQuery(".field_21:checked").length) < 1){
			jQuery("#signinerror").html("Please select gender.");
			jQuery("#signinerror").show();
			jQuery("#signinerror").fadeOut(7000);
			jQuery(window).scrollTop(0);
			return false;
	}
	
	if(jQuery.trim(jQuery(".field_13:checked").length) < 1){
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Please select age range.");
			jQuery("#signinerror").fadeOut(7000);
			jQuery(window).scrollTop(0);
			return false;
	}
	if(jQuery.trim(jQuery(".field_14").val()) == ''){
			jQuery(".field_14").focus();
			jQuery(".field_14").addClass("com_form_error_validatin");
			return false;
	}
	if(jQuery.trim(jQuery(".field_15:checked").length) < 1){
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Please select payment type.");
			jQuery("#signinerror").fadeOut(7000);
			jQuery(window).scrollTop(0);
			return false;
	}
	if(jQuery.trim(jQuery(".field_17:checked").length) < 1){
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Please select experience level.");
			jQuery("#signinerror").fadeOut(7000);
			jQuery(window).scrollTop(0);
			return false;
	}
	
	
	jQuery("#classifieds_create").submit();
	  
   
  }


  /***************** Add Edit functionality *********************/
  function submit_form()
  {
   var ajax_url = '<?php echo $this->baseUrl()."/classified/index/add-parent";?>';
   var form_data = jQuery("#parent_form").serialize();
	jQuery.ajax({
		url: ajax_url,
        type: 'POST',
        data: form_data,
        async: false,
        success: function(s){
        	var msg= s.split("::");
			if(jQuery.trim(msg[0]) == 'success')
			{
				alert(msg[1]);
				jQuery("#parent_name").val(); 
				jQuery("#parent_id").val(); 
				jQuery("#form_div").hide(); 
				jQuery("#overlay").hide(); 
				location.reload();
			  
			}
			else
			{
				jQuery("#signinerror").html(msg[1]);
			}
			
        }
	});
  }
  
function submit_share_form()
{
  var friend_val = jQuery('.tag').length;
  if(friend_val == "0" )
 {
 	alert("Please select a friend to share the page");
 	return false;
 }
 else
 {
  	AJAXPost('share_form');
 }
 
 
}

function AJAXPost(formId) {
    var elem   = document.getElementById(formId).elements;
    var url    = '<?php echo $this->baseUrl()."/classified/index/share-page";?>';;        
    var params = "";
    var value;
     for (var i = 0; i < elem.length; i++) {
		if(elem[i].type !='radio')
		{
			if (elem[i].tagName == "SELECT") {
				value = elem[i].options[elem[i].selectedIndex].value;
			} else {
				value = elem[i].value;                
			}
			params += elem[i].name + "=" + encodeURIComponent(value) + "&";
		}
    }
	
	var form = document.getElementById(formId);
	 
	 for(var i = 0; i < form.permission.length; i++)  
	 {
 
		  if(form.permission[i].checked)
		  {
                       var valueSelected = form.permission[i].value;
		  }
 
	  }
	  
	 params += "permission=" + encodeURIComponent(valueSelected) + "&"; 

	
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { 
        // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.open("POST",url,false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("Content-length", params.length);
    xmlhttp.setRequestHeader("Connection", "close");
    xmlhttp.send(params);
	alert(xmlhttp.responseText);
 	parent.location.reload();
    return xmlhttp.responseText;
}

  
</script>

<?php
  if (APPLICATION_ENV == 'production')
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.min.js');
  else
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');
?>

<script type="text/javascript">

  // Populate data
  var maxRecipients = <?php echo sprintf("%d", $this->maxRecipients) ?> || 10;
  var to = {
    id : false,
    type : false,
    guid : false,
    title : false
  };
  var isPopulated = false;

  <?php if( !empty($this->isPopulated) && !empty($this->toObject) ): ?>
    isPopulated = true;
    to = {
      id : <?php echo sprintf("%d", $this->toObject->getIdentity()) ?>,
      type : '<?php echo $this->toObject->getType() ?>',
      guid : '<?php echo $this->toObject->getGuid() ?>',
      title : '<?php echo $this->string()->escapeJavascript($this->toObject->getTitle()) ?>'
    };
  <?php endif; ?>
  
  function removeFromToValue(id) {
    // code to change the values in the hidden field to have updated values
    // when recipients are removed.
    var toValues = $('toValues').value;
    var toValueArray = toValues.split(",");
    var toValueIndex = "";

    var checkMulti = id.search(/,/);

    // check if we are removing multiple recipients
    if (checkMulti!=-1){
      var recipientsArray = id.split(",");
      for (var i = 0; i < recipientsArray.length; i++){
        removeToValue(recipientsArray[i], toValueArray);
      }
    }
    else{
      removeToValue(id, toValueArray);
    }

    // hide the wrapper for usernames if it is empty
    if ($('toValues').value==""){
      $('toValues-wrapper').setStyle('height', '0');
    }

    $('to').disabled = false;
  }

  function removeToValue(id, toValueArray){
    for (var i = 0; i < toValueArray.length; i++){
      if (toValueArray[i]==id) toValueIndex =i;
    }

    toValueArray.splice(toValueIndex, 1);
    $('toValues').value = toValueArray.join();
  }

  en4.core.runonce.add(function() {
    if( !isPopulated ) { // NOT POPULATED
       new Autocompleter.Request.JSON('to', '<?php echo $this->url(array('module' => 'classified', 'controller' => 'index', 'action' => 'suggest','message' => true), 'default', true) ?>', {
        'minLength': 1,
        'delay' : 250,
        'selectMode': 'pick',
        'autocompleteType': 'message',
        'multiple': false,
        'className': 'message-autosuggest',
        'filterSubset' : true,
        'tokenFormat' : 'object',
        'tokenValueKey' : 'label',
		'keycommand':'none',
        'injectChoice': function(token){
          if(token.type == 'user'){
            var choice = new Element('li', {
              'class': 'autocompleter-choices',
              'html': token.photo,
              'id':token.label
            });
            new Element('div', {
              'html': this.markQueryValue(token.label),
              'class': 'autocompleter-choice'
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
          else {
            var choice = new Element('li', {
              'class': 'autocompleter-choices friendlist',
              'id':token.label
            });
            new Element('div', {
              'html': this.markQueryValue(token.label),
              'class': 'autocompleter-choice'
            }).inject(choice);
            this.addChoiceEvents(choice).inject(this.choices);
            choice.store('autocompleteChoice', token);
          }
            
        },
        onPush : function(){
          if( $('toValues').value.split(',').length >= maxRecipients ){
            $('to').disabled = true;
          }
        }
      });
      
      new Composer.OverText($('to'), {
        'textOverride' : '',
		/*'textOverride' : '<?php echo $this->translate('Start typing...') ?>',*/
        'element' : 'label',
        'isPlainText' : true,
        'positionOptions' : {
          position: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
          edge: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
          offset: {
            x: ( en4.orientation == 'rtl' ? -4 : 4 ),
            y: 2
          }
        }
      });

    } else { // POPULATED

      var myElement = new Element("span", {
        'id' : 'tospan' + to.id,
        'class' : 'tag tag_' + to.type,
        'html' :  to.title /* + ' <a href="javascript:void(0);" ' +
                  'onclick="this.parentNode.destroy();removeFromToValue("' + toID + '");">x</a>"' */
      });
      //$('to-element').appendChild(myElement);
      $('to-wrapper').setStyle('height', 'auto');

      // Hide to input?
      $('to').setStyle('display', 'none');
      $('toValues-wrapper').setStyle('display', 'none');
    }
  });
  


</script>

<div class="headline">
  <h2>Share Page with your friends
  </h2>
 
</div>
<?php
$shared_list = array();
foreach($this->shared_list as $slist)
{
$shared_list[] = $slist['user_id'];
}


$page_id=preg_replace("/[^0-9]/","",$this->page_id);
?>

<div id="signinerror" style=" display:none; color:#ff0000; position: sticky; left: 293px;"></div>

<!--<form name="share_form" id="share_form">
<input type="hidden" name="page_id" value="<?php echo $page_id; ?>" />
<table>
 <tr>
 <td>Select Friends</td>
 <td>
 
 <input type="text" name="to" id="to" value="" placeholder="Start typing.." autocomplete="off">
 <div id="toValues-wrapper" class="form-wrapper">
 <div id="toValues-label" class="form-label">
 <label for="toValues" class="required">&nbsp;</label>
 </div>
<div id="toValues-element" class="form-element">
<input type="hidden" name="toValues" value="" id="toValues">
</div>
</div>
</td>
 </tr>
 <tr>
 <td>Permission</td>
 <td> 
 <label>View and Edit</label>
 <input type="radio" name="permission" checked="checked"   value="2" />
 </td>
 </tr>
 <tr>
 <td colspan="2"> <input type="button" onClick="submit_share_form()" name="share" value="Share" class="share_project"/></td>
 </tr>
</table>
</form>-->
	
	
	



<form name="share_form" id="share_form">
<input type="hidden" name="page_id" value="<?php echo $page_id; ?>" />

<div class="col-md-12">
	<div class="col-sm-3 col-xs-12">
	Select Friends
	</div>
	<div class="col-sm-6 col-xs-12">
	 <input type="text" name="to" id="to" value="" placeholder="Start typing.." autocomplete="off" class="viewedit-input">
 <div id="toValues-wrapper" class="form-wrapper">
 <div id="toValues-label" class="form-label"><label for="toValues" class="required">&nbsp;</label></div>
<div id="toValues-element" class="form-element">
<input type="hidden" name="toValues" value="" id="toValues"></div></div>
 <?php 

	
if( count($this->friends) > 0 ): ?>
  <!--<select class="classifieds_browse" name="user_friend[]" multiselect id="user_friend">
  <option value="">Select Friend</option>
    <?php 
foreach( $this->friends as $friend ):
	?>
      <option value="<?php echo $friend['friend_id']; ?>" <?php echo in_array($friend['friend_id'], $shared_list)?'selected':'';?> ><?php echo $friend['displayname']; ?></option>
    <?php endforeach; ?>
  </select>-->
 <?php endif; 
 

 ?>
	</div>
</div>
<div class="col-md-12" style=" margin-bottom: 14px;">
	<div class="col-sm-3 col-xs-12">Permission</div>

	<div class="col-sm-6 col-xs-12">
		 
		<label>View and Edit</label>
		<input type="radio" name="permission" checked value="2" />
	</div>
</div>

<div class="col-md-12">
	<div class="col-sm-3"><p>&nbsp;</p></div>
	<div class="col-sm-6">
		<input type="button" onClick="submit_share_form()" name="share" value="Share" class="share_project"/>
	</div>
</div>

 
</form>
	

 
 <style>
 

  .col-xs-12 { width:100%;}
 
 
 
  .col-sm-3
 {
 width:30%;float:left; padding:0 1%;
 }
 .col-sm-6 {
    width: 60%; float:left;
}





 .col-md-12
 {
 width:100%;float:left;
 }
 
 
 ul.message-autosuggest
 {width: 222px !important;}
    .black_overlay{
        display: none;
        position: absolute;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index:1001;
        -moz-opacity: 0.8;
        opacity:.80;
        filter: alpha(opacity=80);
    }
    .white_content {
        display: none;
        position: absolute;
        top: 25%;
        left: 25%;
        width: 50%;
        height: 50%;
        padding: 16px;
        border: 16px solid orange;
        background-color: white;
        z-index:1002;
        overflow: auto;
    }
	.tag {
    display: block;
	}
	body#global_page_page-page-get-friends #share_form input.share_project {
    background-color: #fb933c !important;
    border: 0 none !important;
    border-radius: 5px !important;
    color: #fff !important;
    font-weight: 700 !important;
    padding: 8px 12px !important;
}

body#global_page_page-index-manage #TB_iframeContent {
    min-height: 140px !important;
}
</style>

