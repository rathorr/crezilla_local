<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: create.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>

<?php 
	$this->headScript()
		->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
    ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
    ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
    ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');
?>

<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
 <link href="<?php echo $this->baseUrl();?>/public/uploadify/uploadfile.css" rel="stylesheet">
<script src="<?php echo $this->baseUrl();?>/public/uploadify/jquery.uploadfile.min.js"></script>
<script src="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.js"></script>
<link href="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.css" rel="stylesheet" />


<style>
body
{
  font-family: 'Roboto', sans-serif !important;
}

#global_content h2 {
    color: #3faee8;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 700;
    margin: 0;
    padding: 10px 20px;
    text-transform: capitalize;
}
.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:34px;
  line-height:30px;
  width:100%;
  border-radius: 0;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}


ul.form-errors > li, ul.form-notices > li {
 float:none;
}

.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
h2.create_pdf
{margin:6px !important;}
h2.create_pdf a
{padding:10px !important;}
body#global_page_page-index-create #designation {
    height: 100% !important;
  width:357px !important;
}
.SumoSelect > .optWrapper > .options {
      max-height: 160px !important;
    }

 #mulitplefileuploader span b{display:none;}
.ajax-file-upload-error{float: left; margin-left: 148px; color: #FF0000; font-weight: bold;}
#create_page_form
{
  width: 50%;
  margin-left: 25%;
}
#create_page_form > div > div
{
  text-align: center;
}

.global_form div.form-label {
     width: 100%; 
    /* text-align: left !important; */
    /* padding: 4px 15px 0 2px; */
     margin-bottom: 10px; 
    /* overflow: hidden; */
     float:none; 
    /* clear: left; */
    /* font-size: .9em; */
     font-weight: 500; 
     color: #777; 
}
label
{
  font-family: 'Roboto', sans-serif !important;
  font-weight: 500 !important;
}
.SumoSelect {
  width: 100%;
}
.SumoSelect > .CaptionCont {
  border-radius: 0px;
  background-color: #fff;
}
.SumoSelect > .optWrapper {
  background: #fff;
}

@media screen and (max-width :767px)
{ 
  #create_page_form {
    width: 100%;
    margin-left: 0%; 
}
.addSkills {
  background-position: 40px 7px;
  width: 56px;
  right: 0px;
  }
  .ajax-upload-dragdrop
  {
    width: 30% !important;
  }
  .ajax-file-upload 
  {
    padding: 6px 24px !important;
  }
  .global_form .form-elements input#photo,.global_form .form-elements input#cover
  {
    width: 10% !important;
  }
}
</style>

<!-- <div class="headline">
  <h2>
     
      <?php echo $this->translate('Create Page');?>
    
  </h2>

</div> -->

<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle" style="width:100%; background-color: #fff;
    padding: 1% 2%;">
<div class="generic_layout_container layout_core_content">
<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array())
?>

<script type="text/javascript" src="<?php echo $this->baseUrl();?>/externals/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.tiny",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>
<script type="text/javascript">
  he_word_completer.prepend_word = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$this->url(array('page_id' => ''), 'page_view').'/'; ?>";
  he_word_completer.title = "title";
  he_word_completer.url = "url";
  he_word_completer.completer = "page_url_placeholder";
  he_word_completer.default_value = "pagename";
  he_word_completer.ajax_url = "<?php echo $this->url(array('action' => 'validate'), 'page_ajax'); ?>";
  he_word_completer.init();
</script>


<?php
 // echo $this->form->render($this);
 
 $categoryList = Engine_Api::_()->getDbtable('pages', 'page')->getPageCategory();
  
 
?>


<form method="post" action="<?php echo $this->baseUrl().'/create-pages';?>" enctype="multipart/form-data" class="global_form" onsubmit="return validatepage()" id="create_page_form">
<div>
<div>


<div class="form-elements">
<h3>Create New Page</h3>
<div class="form-wrapper" id="title-wrapper">
	<div class="form-label" id="title-label" style="display:block !important;">
    	<label class="required" for="title">Title *</label>
    </div>
	<div class="form-element" id="title-element">
		<input type="text" value="" id="title" name="title" placeholder="Enter title">
    </div>
</div>
<div class="form-wrapper" id="url-wrapper">
	<div class="form-label" id="url-label" style="display:block !important;">
    	<label class="required" for="url">URL *</label>
     </div>
	<div class="form-element" id="url-element">
		<input type="text" value="" id="url" name="url" placeholder="Enter page url">
		<p class="description">You can set custom destination to make it easier for people to find your Page. The URL may only contain alphanumeric characters and dashes. The full url will be <span id="page_url_placeholder"><?php echo $this->baseUrl();?>/page/pagename</span></p>
    </div></div>
    
    <div class="form-wrapper" id="website-wrapper">
<div class="form-label" id="website-label" style="display:block !important;">
<label class="optional" for="website">Category</label>
</div>
<div class="form-element" id="website-element">
<select name="category_id" id="category_id">
<?php
foreach($categoryList as $key => $val)
{
?>
<option value="<?php echo $val['option_id'];?>"><?php echo $val['label'];?></option>
<?php
}
?>
</select>
</div>
</div>
	<div class="form-wrapper" id="photo-wrapper">
    	<div class="form-label" id="photo-label" style="display:block !important;">
        	<label class="optional" for="photo">Profile Picture</label>
        </div>
	<div class="form-element" id="photo-element">
	<input type="hidden" id="MAX_FILE_SIZE" value="786432000" name="MAX_FILE_SIZE">
		<input type="file" id="photo" name="photo">
      </div>
   </div>
   
   <div class="form-wrapper" id="photo-wrapper">
    	<div class="form-label" id="photo-label" style="display:block !important;">
        	<label class="optional" for="photo">Cover Picture</label>
        </div>
	<div class="form-element" id="photo-element">
		<input type="file" id="cover" name="cover">
      </div>
   </div>
   
	<div class="form-wrapper" id="description-wrapper">
    	<div class="form-label" id="description-label" style="display:block !important;">
        	<label class="optional" for="description">About:</label>
         </div>
	<div class="form-element" id="description-element">
	<textarea id="description" name="description" style="width: 553px;"  aria-hidden="true"></textarea>
	</div>
    </div>
    
    <div class="form-wrapper" id="services-wrapper">
    	<div class="form-label" id="services-label" style="display:block !important;">
        	<label class="optional" for="services">Description of Services:</label>
         </div>
	<div class="form-element" id="services-element">
	<textarea id="services" name="services" style="width: 553px;"  aria-hidden="true"></textarea>
	</div>
    </div>
    <div class="form-wrapper" id="role-wrapper">
      <div class="form-label" id="role-label" style="display:block !important;">
          <label class="optional" for="role">Business Role:</label>
         </div>
  <div class="form-element" id="role-element">
 <input type="hidden" name="desig_ids" id="desig_ids" value="">
<select class="field_container designations" id="designation"  multiple="multiple">
  <?php if($this->dasignatedsubcategories){
      foreach($this->dasignatedsubcategories  as $key=>$val){?>
        <option  value="<?php echo $key;?>" <?php if(in_array($key, $this->userdc)){echo 'selected="selected"';}?>><?php echo $val?></option>
      <?php }
   }?>
</select>
  </div>
    </div>
    
<!-- Designation end-->

<div class="form-wrapper" id="website-wrapper">
<div class="form-label" id="website-label" style="display:block !important;">
<label class="optional" for="website">Website</label>
</div>
<div class="form-element" id="website-element">
<input type="text" value="" id="website" name="website">
</div>
</div>


<div class="form-wrapper" id="address-wrapper">
<div class="form-label" id="address-label" style="display:block !important;">
<label class="optional" for="address">Address</label>
</div>
<div class="form-element" id="address-element">
<input type="text" value="" class="address" name="address[]" placeholder="Enter Address">
</div>
<div class="merge_address"><!-- merge start-->
<div class="form-element" id="phone-element">
<input type="text" value="" class="phone_number" name="phone_number[]" placeholder="Enter Phone Number">
</div>

<div class="form-element" id="zip-element">
<input type="text" value="" class="zip_code" name="zip_code[]" placeholder="Enter Pin Code">
</div>

<div class="form-element" id="country-element">
<input type="text" value="" class="country" name="country[]" placeholder="Enter Country">
</div>
</div><!-- merge end-->


<span class="add_more_address addSkills" title="Add More">Add</span>
</div>


   <div class="form-wrapper key-client-add" id="client-wrapper">
    	<div class="form-label" id="client-label" style="display:block !important;">
        	<label class="optional" for="client">Key Client(s):</label>
        </div>
        
	<div class="form-element" id="client-element">
		<input type="text" id="client" name="client[]">
      </div>
    <span id="add_more_client" title="Add More" class="addSkills" style="background-color:transparent;">Add</span>
   </div>
   
   
   <div class="form-wrapper key-project-add" id="project-wrapper">
    	<div class="form-label" id="project-label" style="display:block !important;">
        	<label class="optional" for="project">Key Project(s):</label>
        </div>
	<div class="form-element" id="project-element">
		<input type="text" id="project" name="project[]">
      </div>
    <span id="add_more_project" title="Add More" class="addSkills" style="background-color:transparent;">Add</span>
   </div>
   
   <div class="form-wrapper key-award-add" id="award-wrapper">
    	<div class="form-label" id="award-label" style="display:block !important;">
        	<label class="optional" for="award">Key Award(s):</label>
        </div>
	<div class="form-element" id="award-element">
		<input type="text" id="award" name="award[]">
      </div>
    <span id="add_more_award" title="Add More" class="addSkills" style="background-color:transparent;">Add</span>
   </div>
   
   <div class="form-label" id="reel-label" style="display:block !important;">
        	<label class="optional" for="project">Add Showreel:</label>
    </div>
   
   <div class="form-wrapper key-showlink-add" id="showlink-wrapper">
    	<div class="form-label" id="showlink-label" style="display:block !important;">
        	<label class="optional" for="showlink">Video/Audio Link(s):</label>
        </div>
	<div class="form-element" id="showlink-element">
		<input type="text" id="showlink" name="showlink[]">
      </div>
    <span id="add_more_showlink" title="Add More" class="addSkills" style="background-color:transparent;">Add</span>
   </div>
   
   
   
   
   
   
   
   <div class="form-label" id="reel-label" style="display:block !important;">
        	<label class="optional" for="project">Photo/Pdf(s):</label>
    </div>
        
        
        <div class="form-element">
		 <div id="mulitplefileuploader">Upload</div>
        
        <div id="status"></div>
      </div>
      
 
   
</div>

<div class="form-wrapper" id="buttons-wrapper">
<div class="form-label" id="buttons-label">&nbsp;</div>
<div class="form-element" id="buttons-element">
<input type="hidden" name="file_name" value="" id="file_name" />
<button type="submit" id="execute" class="save-changes" name="execute">Save</button>
 <a href="<?php echo $this->baseUrl();?>/members/home" type="button" id="cancel" name="cancel">cancel</a>
 </div>
</div>
</div>
</div>

</form>

<script type="text/javascript">
jQuery(document).on('change','.document_file',function(){
	if(jQuery(this).val()==''){
		jQuery(this).prev().val('Document');
	}
});

jQuery(document).on('change','.document_name',function(){
	if(jQuery(this).val()==''){
		jQuery(this).val('Document');
	}
});

jQuery('.add_more_address').on('click', function(){
	var text	=	'<div class="new_add"><div class="form-element" id="address-element"><input type="text" value="" class="address" name="address[]" placeholder="Enter Address"></div><div class="form-element" id="phone-element"><input type="text" value="" class="phone_number" name="phone_number[]" placeholder="Enter Phone Number"></div><div class="form-element" id="zip-element"><input type="text" value="" class="zip_code" name="zip_code[]" placeholder="Enter Pin Code"></div><div class="form-element" id="country-element"><input type="text" value="" class="country" name="country[]" placeholder="Enter Country"></div><span class="delete_add" style="cursor:pointer;">X</span></div>';
jQuery('#address-wrapper').append(text);	
});

jQuery('#add_more_document').on('click',function(){
	
jQuery(this).before('<div class="form-element add-more-doc" id="document-element"><input type="text" name="document_name[]" class="document_name"><input type="file"  name="document[]" class="document_file"><span class="delete_doc">x</span></div>');	
});	
jQuery(document).on('click','.delete_doc, .delete_add',function(){
	jQuery(this).parent().remove();	
});

jQuery('#add_more_client').on('click',function(){
	
jQuery(this).before('<div class="form-element add-more-client" id="client-element"><input type="text" id="client" name="client[]"><span class="delete_doc">x</span></div>');	
});	

jQuery('#add_more_project').on('click',function(){
	
jQuery(this).before('<div class="form-element add-more-project " id="project-element"><input type="text" id="project" name="project[]"><span class="delete_doc">x</span></div>');	
});

jQuery('#add_more_award').on('click',function(){
	
jQuery(this).before('<div class="form-element add-more-award " id="award-element"><input type="text" id="award" name="award[]"><span class="delete_doc">x</span></div>');	
});

jQuery('#add_more_showlink').on('click',function(){
	
jQuery(this).before('<div class="form-element add-more-showlink " id="showlink-element"><input type="text" id="showlink" name="showlink[]"><span class="delete_doc mk_edit_delete_showlink_record">x</span></div>');	
});

function validatepage(){
	 
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#title").val()) == ''){
			//$("#signinerror").html("Email is required.");
			//$("#signinerror").show();
			jQuery("#title").focus();
			jQuery("#title").addClass("com_form_error_validatin");
			//$("#signinerror").fadeOut(7000);
			return false;
	}
	if(jQuery.trim(jQuery.trim(jQuery("#url").val())) == '' || jQuery("#url").hasClass("error")==true){
 		jQuery("#url").focus();
		jQuery("#url").addClass("com_form_error_validatin");
		//$('#email').focus();
		//$('#email').addClass('error');
		return false;
  	 }
	//return false;
	//jQuery('#create_page_form').submit();	  
  	
}

</script>

<script type="text/javascript">
jQuery(document).ready(function()
{
  jQuery('.designations').SumoSelect({selectAll: false,placeholder: 'Select Only 5 Designations'});
setTimeout(function(){ 
     jQuery('#desig_ids').val(jQuery('#designation').val());
  }, 1000);

 var settings = {
	url: "<?php echo $this->baseUrl();?>/page-ajax/addreel",
	method: "POST",
	allowedTypes:"jpg,png,jpeg,pdf",
	fileName: "myfile",
	multiple: true,
	onSuccess:function(files,data,xhr)
	{
		var string=jQuery('#file_name').val();
  		jQuery("#file_name").val(data);
		if(string!="")
		{
 		var res = "".concat(string,"||",data);
		jQuery("#file_name").val(res);
		}
		else
		{
			jQuery("#file_name").val();	
		}
 		//jQuery("#status").html("<font color='green'>Upload is success</font>");
		
	},
	onError: function(files,status,errMsg)
	{		
		jQuery("#status").html("<font color='red'>Upload is Failed</font>");
	}
}
jQuery("#mulitplefileuploader").uploadFile(settings);


});
jQuery(document).on('click', '#designation', function(){
  jQuery('#desig_ids').val(jQuery(this).val());
});
</script>


</div>
</div>
</div>



