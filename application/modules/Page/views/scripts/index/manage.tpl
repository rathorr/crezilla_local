<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: manage.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>
<!-- <div class="headline">
  <h2>Manage Pages </h2>
</div> -->

<!-- PROFILE FORM START -->
 
<style>

#global_content h2 {
    color: #3faee8;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 700;
    margin: 0;
    padding: 10px 20px;
    text-transform: capitalize;
}
.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:auto;
  width:100%;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}


ul.form-errors > li, ul.form-notices > li {
 float:none;
}

.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
._manage_pages_section, ._community_ads, .member_list_section {
    padding: 0 15px!important;
}
._manage_pages_head, .member_list_head{
  float: left;
    width: 100%;
    padding-bottom: 8px;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 30px;
}
._manage_pages_head_content, .member_list_head_content {
    font-size: 18px;
    text-transform: uppercase;
}
._manage_pages_list_item_area{
  float: left;
  width: 100%;
  background: #fff;
}
.padnone{
  padding: 0px;
}
._manage_pages_list_item_block {
    background: #fff;
    padding: 15px 15px;
    float: left;
    border-bottom: 1px solid #e5e5e5 !important;
}
._manage_pages_list_item_block:last-of-type, ._manage_pages_list_item_block:nth-last-of-type(2) {
    border-bottom: none;
}
._manage_pages_list_item_block:nth-child(even) {
    border-left: 1px solid #e5e5e5;
}

._item_block_content {
    float: left;
    width: 100%;
}
._manage_pages_item_img {
    float: left;
    height: 105px;
    max-width: 100px;
}
._manage_pages_item_img img{
  width: 100%;
  max-height: 105px;
}
._manage_pages_item_content {
    width: calc(100% - 105px);
    float: right;
    padding: 0 9px;
}
  
._manage_pages_title a {
    color: #333;
    font-size: 18px;
}
._manage_pages_date{
  font-size: 14px;
  color: #999999;
}
._manage_pages_details {
    font-size: 14px;
    color: #999;
}
._manage_pages_desc {
    font-size: 14px;
    color: #333;
}
._item_block_footer {
    float: left;
    width: 100%;
}
._manage_pages_action {
  float: left;
  width: 100%;
    margin-top: 30px;
}
._manage_pages_action a._manage_pages_action_btn {
    text-decoration: none;
    color: #333;
    text-transform: uppercase;
    border: 1px solid #333;
    width: 85px;
    height: 30px;
    display: inline-block;
    text-align: center;
    line-height: 30px;
    margin-right: 10px;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    -ms-transition: 0.3s ease-in-out;
    -o-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
._manage_pages_action a._manage_pages_action_btn:last-child{margin-right: 0px;}
._manage_pages_action a._manage_pages_action_btn:hover {
    color: #fa933c;
    border: 1px solid #fa933c;
}

.footer_section {
    padding: 40px 0;
    background: #e8e8e8;
    float: left;
    width: 100%;
    margin-top: 100px;
}
.footer-links a{
  text-decoration: none;
  color: #1f1f1f;
  font-size: 14px;
  text-transform: uppercase;   
}
.footer-links ul{
  margin: 0px;
  padding:0px;
  list-style: none;
}
.footer-links ul li{
  display: block;
  margin-bottom: 8px;
}
.footer_head{
  font-size: 14px;
  color: #1f1f1f;
  text-transform: uppercase;
  padding-bottom: 8px; 
}
.footer-social-content a {
    text-decoration: none;
    display: inline-block;
    margin-right: 10px;
    -webkit-transition: 0.5s ease-in-out;
    -moz-transition: 0.5s ease-in-out;
    -ms-transition: 0.5s ease-in-out;
    -o-transition: 0.5s ease-in-out;
    transition: 0.5s ease-in-out;
}
.footer-social-content a:hover{
  opacity: 0.5;
}
.footer-newsletter{padding-right: 0px;}
.footer-newsletter-content input.newsletter-input{
  height: 46px;
  line-height: 46px;
  border: 1px solid #000000;
  padding:0 20px;
  background: #fff;
  color: #1f1f1f;
  font-size:14px;
  text-transform: uppercase; 
  width: 65%;
  margin-right: -5px;
}
.footer-newsletter-content input.newsletter-submit{
  height: 47px;
  line-height: 46px;
  border: 1px solid #000000;
  padding:0 20px;
  background: #000;
  color: #fff;
  font-size:14px;
  text-transform: uppercase; 
  width: 35%;
  
}
.copyright_section {
    background: #d4d4d4;
    float: left;
    width: 100%;
    padding: 15px 0 10px;
}
.copyright_section span{text-transform: uppercase;}
.member_list_item_area{
  background: #fff;
  padding:5px 5px 8px;
  float: left;
  width: 100%;
}

._member_list_item_title a {
    color: #333;
    font-size: 18px;
}
._member_list_item_subtitle{
  font-size: 14px;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
  word-wrap: ;
  white-space: nowrap;
}
._member_list_item_action{
  float: left;
  width: 100%;
}
._member_list_item_action a{
  text-decoration: none;
  color: #cacaca; 
}
._member_list_item_review {
  font-size:14px;
  color: #ffc100; 
  float: left;
  width: 100%;
  padding-top: 20px;
}
._member_list_item_review .fa-star{
  cursor: pointer;
}
.padrgtnone{padding-right: 0px;}
.padlftnone{padding-left: 0px;}
._addfolderr_icn {
    width: 13px;
    height: 13px;
    display: inline-block;
    background: url(img/add_folder_icn.png) no-repeat center center;
    background-size: cover;
    margin-right: 0;
    margin-left: 8px;
}
.member_list_item {
    margin-bottom: 30px;
}
.member_list_search{
  float: left;
  width: 100%;
  padding-bottom: 20px;
}
.member_list_search input.search-input{
  border-radius: 0;
  box-shadow: none;
  margin-bottom: 10px;
}
.member_list_search input.search-submit{
  background: #ff9e20;
  color: #fff;
  border-radius: 0;
  box-shadow: none;
  -webkit-transition: 0.5s ease-in-out;
  -moz-transition: 0.5s ease-in-out;
  -ms-transition: 0.5s ease-in-out;
  -o-transition: 0.5s ease-in-out;
  transition: 0.5s ease-in-out;
}
.member_list_search input.search-submit:hover{
  background: #1f1f1f;
}
@media (max-width: 1024px){
  .footer-newsletter,
  .footer-social-links {padding-left:0px; clear:both; margin-top: 20px;}

}
@media (max-width: 768px){

  .footer-social-links{
    clear: none;
    margin-top: 0px;
  }
 ._posted_job_right ,._manage_pages_section{
    float: right;
    padding-left: 0px !important;
    padding-right: 0px !important;
  }
._manage_pages_action a._manage_pages_action_btn {
  margin-right: 5px !important;
}
#global_content > div > div > div > div > div._manage_pages_section > div._manage_pages_list > div > div > div > div > div._manage_pages_action > a._manage_pages_action_btn
{
  margin-top: 10px;
}
}
._posted_job_right {
    float: right;
    padding-left: 15px;
    padding-right: 15px;
}
._posted_job_right_actions {
    background: white;
    padding-top: 15px;
    margin-bottom: 25px;
}
._posted_job_right_actions_btn {
    float: left;
    width: 100%;
    background: #fa933c;
    text-align: center;
    color: #fff !important;
    font-size: 14px;
    height: 40px;
    line-height: 40px;
    margin-bottom: 15px;
    text-transform: uppercase !important;
}
</style>
<!-- PROFILE FORM END -->
<!-- PHOTOS FORM START -->
<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle" style="width:100%; background-color: #E9EAED;
    padding: 1% 2%;">
<div class="generic_layout_container layout_core_content">
  <div class="container">
  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 _posted_job_right">
      <div class="col-xs-12 _posted_job_right_actions">
        <a href="<?php echo $this->baseUrl().'/create-pages';?>" class="_posted_job_right_actions_btn">Create Page</a>
        
      </div>
    </div>  
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 _manage_pages_section">
      <div class="_manage_pages_head">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 _manage_pages_head_content">Manage Pages</div>
        </div>
      </div>
      <div class="_manage_pages_list">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 _manage_pages_list_item">
            <div class="_manage_pages_list_item_area">
            <?php if (count($this->paginator) == 0){ ?>           
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 _manage_pages_list_item_block">
                <div class="_item_block_content"> 
                  <div class="_manage_pages_item_content">
                    <div class="_manage_pages_title">
                    <div class="tip">
                    <span><?php echo $this->translate("There is no pages"); ?>, <?php echo $this->htmlLink($this->url(array(), 'page_create'), $this->translate('create page')); ?>.
                    </span>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } else{ 
            foreach( $this->paginator as $page ){
            $page_id = $page->getIdentity(); 
            $pageInfo = Engine_Api::_()->getDbtable('pages', 'page')->getPageInfo($page_id);
            $pageInfo=$pageInfo[0];
            $page = Engine_Api::_()->getItem('page', $page_id);
            //echo $page->creation_date;?>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 _manage_pages_list_item_block">
                <div class="_item_block_content">
                  <div class="_manage_pages_item_img"><img src="<?php echo $page->getPhotoUrl('thumb.normal'); ?>"></div>
                  <div class="_manage_pages_item_content">
                    <div class="_manage_pages_title"><a href="<?php echo $page->getHref(); ?>" title=""><?php echo $pageInfo['title']; ?></a></div>
                    <div class="_manage_pages_date"><?php echo date('d F Y',strtotime($page->creation_date)); ?></div>
                    <div class="_manage_pages_details">
                      <span class="_manage_pages_like"><?php if (!empty($this->page_likes[$page_id])) echo $this->page_likes[$page_id]; else echo 0; ?> <?php echo $this->translate('likes this')?></span>
                      <span>|</span>
                      <span class="_manage_pages_views"><?php echo $page->view_count ?> <?php echo $this->translate("views"); ?></span>
                    </div>
                    <div class="_manage_pages_desc">
                      <p><?php echo $page->getDescription(true, true, false, 50); ?></p>
                    </div>
                  </div>
                </div>
                <div class="_manage_pages_action">
                  <!-- <a href="javascript:void(0)" class="_manage_pages_action_btn _manage_pages_shareedit">Share Edit</a> 
                  <a href="javascript:void(0)" class="_manage_pages_action_btn _manage_pages_shared">Shared</a>-->
                  <!-- <a href="javascript:void(0)" class="_manage_pages_action_btn _manage_pages_edit">edit</a> -->
                  <?php
                  $shared_list = Engine_Api::_()->getDbtable('pages', 'page')->checkShared($page->getIdentity());
            
                  if($pageInfo['user_id']==$this->user_id)
                    {
                   ?>
                  <a class="smoothbox _manage_pages_action_btn _manage_pages_shareedit" href="<?php echo $this->baseUrl()."/page-share/get-friends/".$page->getIdentity();?>">Share Edit</a>
                  
                  <?php
                    }
                  if($shared_list[0]['totShared']>0)
                  {
                    if($pageInfo['user_id']==$this->user_id)
                    {
                  ?>
                  <a class="smoothbox _manage_pages_action_btn _manage_pages_shared" href="<?php echo $this->baseUrl()."/page-shared/get-friends-shared/".$page->getIdentity();?>">Shared</a>
                  <?php
                    }
                  }
                  ?>
                  <?php echo $this->htmlLink($this->url(array('action' => 'edit', 'page_id' => $page->getIdentity()), 'page_team'), $this->translate("Edit"), array("class" => "_manage_pages_action_btn _manage_pages_edit")); ?>

                  <?php if($pageInfo['user_id']==$this->user_id) {?> 
                  <a href="<?php echo $this->baseUrl()."/page-shared/page-delete/".$page->getIdentity();?>" class="smoothbox _manage_pages_action_btn _manage_pages_remove">delete</a>
                  <?php }?>
                </div>
              </div>
              <?php } }?>
            </div>
          </div>
        </div>
      </div>
    </div>
   <!--  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 _community_ads">
      <div class=" col-xs-12 _community_ads_area">
        <div class="_community_ads_head">
          <a class="_sponsored" href="#" title="">Sponsored</a>
          <a class="_createads" href="#" title="">Create Ad</a>
        </div>
        <div class="_community_ads_items">
          <div class="_community_ads_img"><a href=""><img src="img/add_1.jpg" alt="" /></a></div>
          <div class="_community_ads_desc">
          <span><a class="_community_ads_title _clear" href="#" title="">Build Your Store on FB!</a></span>
          <span><a class="_community_ads_web_link _clear" href="#" title="">www.shopify.com</a></span>
            <p class="_clear">Today isn't just another day.</p>
          </div>
        </div>
        <div class="_community_ads_items">
          <div class="_community_ads_img"><a href=""><img src="img/add_2.jpg" alt="" /></a></div>
          <div class="_community_ads_desc">
            <span><a class="_community_ads_title _clear" href="#" title="">Secure your Company Name</a></span>
            <span><a class="_community_ads_web_link _clear" href="#" title="">www.pypestream.com</a></span>
            <p class="_clear">Secure your Company Name for a New Era of Communication!</p>
          </div>
        </div>
        
      </div>
    </div>   -->
  </div>
  
</div>
</div>
</div>
