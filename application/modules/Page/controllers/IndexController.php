<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: IndexController.php 2010-08-31 16:05 idris $
 * @author     Idris
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class Page_IndexController extends Core_Controller_Action_Standard
{
	protected $_package_id;

  public function init()
  {
    $content = $this->_getParam('content', '');
    $content_id = $this->_getParam('content_id', 0);

    $request = $this->getRequest();

    $subject = null;
    if( !Engine_Api::_()->core()->hasSubject() ) {
      $page_url = $this->_getParam('page_id', 0);

      if( $page_url === 0 && $request->getActionName() == 'view') {
        return $this->_redirectCustom(array('route' => 'page_browse'));
      }

      if( null !== $page_url && $page_url !== 0) {
        $subject = Engine_Api::_()->page()->getPageByUrl($page_url);
        if( $subject && $subject->getIdentity() ) {
          $subject->setContentInfo($content, $content_id);
          Engine_Api::_()->core()->setSubject($subject);
        }
      }
    }

    if ($request->getActionName() == 'view' || $request->getActionName() == 'index') {
      if ($subject && $subject->isTimeline()) {
        if (Engine_Api::_()->getDbTable('modules', 'hecore')->isModuleEnabled('timeline')) {
          $request->setActionName('timeline-view');
        }
      }
    }
  }
  
 

  public function createAction()
  {
	  $profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	  
    $settings = Engine_Api::_()->getDbTable('settings', 'core');
    $viewer = Engine_Api::_()->user()->getViewer();
	$table = Engine_Api::_()->getDbTable('pages', 'page');

    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('page_main');
     $dasignatedsubcategories = Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getsuppliersDesignations();
  $deoptions  = array();
    if($dasignatedsubcategories){
      foreach($dasignatedsubcategories as $desig){
        $deoptions[$desig['dasignatedsubcategory_id']]  = $desig['dasignatedsubcategory_name']; 
      }
    }
    asort($deoptions, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);

  $this->view->dasignatedsubcategories = $deoptions;  

    if( !$this->getRequest()->isPost() ) {
      return;
    }

    if(!($values = $this->getRequest()->getPost())) {
      //$form->populate($values);
      return;
    }
 

    if ($table->checkUrl($values['url'])) {
    	//$form->populate($values);
      //$form->addError(Zend_Registry::get('Zend_Translate')->_('This URL is already taken by other page.'));
      return ;
    }
    $values['url'] = strtolower(trim($values['url']));
    $values['url'] = preg_replace('/[^a-z0-9-]/', '-', $values['url']);
    $values['url'] = preg_replace('/-+/', "-", $values['url']);

    $db = $table->getAdapter();
    $db->beginTransaction();

    $page = $table->createRow();	

    try
    {
		 
	  $categoryId=$_POST['category_id'];
      $values['user_id'] = $viewer->getIdentity();
      $values['parent_type'] = $this->getRequest()->getParam('parent_type', 'user');
      $values['parent_id'] =  $this->getRequest()->getParam('subject_id', $viewer->getIdentity());

      $page->setFromArray($values);
      //$page->set_id = $values['category'];
      //$page->displayname = $page->title;
      //$page->name = $page->url;
	  $page->displayname = $values['title'];
      $page->name = $values['url'];
      $page->save();

      $page->membership()->addMember($viewer)->setUserApproved($viewer)->setResourceApproved($viewer)->setUserTypeAdmin($viewer);
      $page->setAdmin($viewer);
      $page->getTeamList()->add($viewer);

      if( !empty($_FILES['photo']['name']) ) {
		  
        $page->setPhoto($_FILES['photo']);
		 
      }
	  
	  if(!empty($_FILES['cover']['name'])){
		  
			$photo_id = $this->setCoverPhoto($_FILES['cover'], $page->page_id);
			$setting_name = Engine_Api::_()->timeline()->getCoverPhotoSetting($page->page_id, 'page', 'cover');
			$table = Engine_Api::_()->getDbTable('settings', 'core');
			$table->setSetting($setting_name, $photo_id);

			$iMain = Engine_Api::_()->getItem('storage_file', $photo_id);

			// Insert activity
			$activity_type = 'post_self';
			$body = '';

			$attachment = null;
			// Hooks to enable albums to work
			$event = Engine_Hooks_Dispatcher::_()
				->callEvent('onSubjectTimelinePhotoUpload', array(
					'subject' => $page,
					'file' => $iMain,
					'type' => 'cover'
				));

			$attachment = $event->getResponse();

			$cover_parent = 'album';

			if (!$attachment) {
				$attachment = $iMain;
				$cover_parent = 'storage';
			} else {
				if ($attachment->getType() == 'pagealbumphoto') {
					$cover_parent = 'pagealbum';
				}
			}

			$cover_parent_setting_name = Engine_Api::_()->timeline()->getCoverParentSetting($page->page_id, 'page', 'cover');
			$table->setSetting($setting_name, $attachment->getIdentity());
			$table->setSetting($cover_parent_setting_name, $cover_parent);
	}
	  
	  
	  // SAVE MEDIA REELS
		$files_name=$_POST['file_name'];
		$files_name=explode("||",$files_name);
		 
		 if(!empty($files_name)){
			foreach($files_name as $key=> $val)
			{
				if($val!="")
				{
					$file_extension = trim(strtolower(end(explode('.', $val))));
					if($file_extension =='pdf'){
						$format	=	'1';	
					}else{
						$format	=	'2';		
					}
					rename("public/temporary/".$val."", "public/page/reels/".$val."");
					$tablereel = Engine_Api::_()->getDbTable('reels', 'page');
					$reel = $tablereel->createRow();
					$reel->reel_path	=	$val;
					$reel->page_id		=	$page->page_id;
					$reel->format		=	$format;
					$reel->created_on	=	date('Y-m-d H:i:s', time());
					$reel->save();
				}
			}
		}
		
	  // SAVE LINK REELS
	  if(!empty($values['showlink'])){
			$tablelink = Engine_Api::_()->getDbTable('reels', 'page');
			foreach($values['showlink'] as $lnk){
				if($lnk){
					$link = $tablelink->createRow();
					$link->reel_path	=	$lnk;
					$link->format		=	3;
					$link->page_id		=	$page->page_id;
					$link->created_on	=	date('Y-m-d H:i:s', time());
					$link->save();	
				}
			}  
	  }	
	  
	  // SAVE PROJECTS
	  if(!empty($values['project'])){
			$tableproject = Engine_Api::_()->getDbTable('projects', 'page');
			foreach($values['project'] as $proj){
				if($proj){
					$project = $tableproject->createRow();
					$project->project_name	=	$proj;
					$project->page_id		=	$page->page_id;
					$project->created_on	=	date('Y-m-d H:i:s', time());
					$project->save();
				}
			}  
	  }
	  
	  // SAVE CLIENTS
	  if(!empty($values['client'])){
			$tableclient = Engine_Api::_()->getDbTable('clients', 'page');
			foreach($values['client'] as $clint){
				if($clint){
					$client = $tableclient->createRow();
					$client->client_name	=	$clint;
					$client->page_id		=	$page->page_id;
					$client->created_on		=	date('Y-m-d H:i:s', time());
					$client->save();	
				}  
			}
	  }
	  
	  // SAVE AWARDS
	  if(!empty($values['award'])){
			$tableaward = Engine_Api::_()->getDbTable('awards', 'page');
			foreach($values['award'] as $awrd){
				if($awrd){
					$award = $tableaward->createRow();
					$award->award_name	=	$awrd;
					$award->page_id		=	$page->page_id;
					$award->created_on	=	date('Y-m-d H:i:s', time());
					$award->save();
				}
			}  
	  }

	 

	  // SAVE ADDRESSES
		if(!empty($values['address'])){
			
			$i=0;
			foreach($values['address'] as $addr){
				$tableaddr = Engine_Api::_()->getDbTable('contacts', 'page');
				$address = $tableaddr->createRow();
				$address->address		=	$addr;
				$address->phone_number=	($values['phone_number'][$i])?$values['phone_number'][$i]:'';
				$address->zip_code	=	($values['zip_code'][$i])?$values['zip_code'][$i]:'';
				$address->country		=	($values['country'][$i])?$values['country'][$i]:'';
				$address->page_id		=	$page->page_id;
				$address->created_on	=	date('Y-m-d H:i:s', time());
				$address->created_by	=	$viewer->getIdentity();
				$address->modified_by	=	$viewer->getIdentity();
				$address->save();	
				
			$i++;
			}  
	  }
	     // SAVE Business Role
    $pagedesigs = Engine_Api::_()->getDbtable('dasignatedcategories', 'page')->pageDasignatedcategories($page->page_id);
    $desig_ids  = $values['desig_ids'];
    if($desig_ids){
    	$desig_ids=explode(',', $desig_ids);

      $pagedesoption = array();
      if($pagedesigs){
        foreach($pagedesigs as $des){
          $pagedesoption[$des['dasignatedcategories_id']]  = $des['dc_primary_id']; 
        }
      }
       if($pagedesoption){
              
        foreach($pagedesoption as $did){
         
          if($did && !in_array($did, $desig_ids)){
            $des_id  = Engine_Api::_()->getDbtable('dasignatedcategories', 'page')
        ->getpagedcbydcprimaryid($did, $page->page_id);
          if($des_id){              
              $page_des = Engine_Api::_()->getDbTable('dasignatedcategories', 'page');
              $condition = array(
                'dc_primary_id = ?' => $did,
                'page_id = ?' => $page->page_id
              );
              
              $page_des->delete($condition);
            }
          }
        } 
      }
      if($desig_ids){
      	//print_r($desig_ids);
       
        foreach($desig_ids as $dc){
        		//echo $dc;
          if(!in_array($dc, $pagedesoption) && $dc){
          		echo $dc; 
              $mdes = Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')
        ->getdscById($dc);
       /* print_r($mdes);
        die();*/
              $tabledes = Engine_Api::_()->getDbTable('dasignatedcategories', 'page');
              $dasignatedcategory = $tabledes->createRow();
              $dasignatedcategory->dasignatedcategory_title  = $mdes[0]['dasignatedsubcategory_name'];
              $dasignatedcategory->dc_primary_id  = $dc;
              $dasignatedcategory->created_on    = date('Y-m-d H:i:s', time());
              $dasignatedcategory->page_id     = $page->page_id;
              $dasignatedcategory->save(); 
          }
        } 
      }
     
    //die;  
    }
    	  		
	$page->createContent();

      // Add fields
      /*$customfieldform = $form->getSubForm('fields');
      $customfieldform->setItem($page);
      $customfieldform->saveValues();
      $customfieldform->removeElement('submit');*/

      $availableLabels = array(
        'everyone' => 'Everyone',
        'registered' => 'Registered Members',
        'likes' => 'Likes, Admins and Owner',
        'team' => 'Admins and Owner Only'
      );

      /**
       * @var $package Page_Model_Package
       * @var $authTb Authorization_Model_DbTable_Permissions
       */
      if ($settings->getSetting('page.package.enabled', 0)) {
        if ($subscription && $subscription->page_id == 0) {
          $package = $subscription->getPackage();
        } else {
          $package = Engine_Api::_()->getItemTable('page_package')->getDefaultPackage();
        }

        $page->package_id = $package->getIdentity();
        $page->featured = $package->featured;
        $page->sponsored = $package->sponsored;
        $page->approved = $package->autoapprove;
        $page->enabled = true;

        $view_options = array_intersect_key($availableLabels, array_flip($package->auth_view));
        $comment_options = array_intersect_key($availableLabels, array_flip($package->auth_comment));
        $posting_options = array_intersect_key($availableLabels, array_flip($package->auth_posting));
			} else {
        $authTb = Engine_Api::_()->authorization()->getAdapter('levels');
        $page->approved = (int) $authTb->getAllowed('page', $viewer, 'auto_approve');
        $page->featured = (int) $authTb->getAllowed('page', $viewer, 'featured');
        $page->sponsored = (int) $authTb->getAllowed('page', $viewer, 'sponsored');
        $page->enabled = 1;

        $view_options = (array) Engine_Api::_()->authorization()->getAdapter('levels')->getAllowed('page', $viewer, 'auth_view');
        $view_options = array_intersect_key($availableLabels, array_flip($view_options));

        $comment_options = (array) Engine_Api::_()->authorization()->getAdapter('levels')->getAllowed('page', $viewer, 'auth_comment');
        $comment_options = array_intersect_key($availableLabels, array_flip($comment_options));

        $posting_options = (array) Engine_Api::_()->authorization()->getAdapter('levels')->getAllowed('page', $viewer, 'auth_posting');
        $posting_options = array_intersect_key($availableLabels, array_flip($posting_options));
      }

      if ( $page->save() ) {
		//Create Client code
		
		$code = "CZ".str_pad(mt_rand(1,9999999),7,'0',STR_PAD_LEFT);
       $codestatus = Engine_Api::_()->getDbtable('clientids', 'page')->userClientcode($code);
        if(!$codestatus)
        {
           $clientiddata=array(
              'user_id' => $viewer->getIdentity(),
              'name'  => $values['url'],
              'page_id' => $page->page_id,
              'clientcode' => $code,
              'status' => 1,
              'created_on' => date('Y-m-d h:i:s')
           );
          $codeSave = Engine_Api::_()->getDbtable('clientids', 'page')->createRow();
          $codeSave->setFromArray($clientiddata);
          $codeSave->save();
        }

        $values = array(
          'auth_view' => key($view_options),
          'auth_comment' => key($comment_options),
          'auth_album_posting' => key($posting_options),
          'auth_blog_posting' => key($posting_options),
          'auth_disc_posting' => key($posting_options),
          'auth_doc_posting' => key($posting_options),
          'auth_event_posting' => key($posting_options),
          'auth_music_posting' => key($posting_options),
          'auth_video_posting' => key($posting_options)
        );
        $page->setPrivacy($values);
        if ($page->approved) {
          $activityApi = Engine_Api::_()->getDbtable('actions', 'activity');
          $action = $activityApi->addActivity($viewer, $page, 'page_create');
          if ($action) {
            $activityApi->attachActivity($action, $page);
          }
        }
      }

      if ($settings->getSetting('page.package.enabled', 0) && $subscription->page_id == 0) {
        $subscription->page_id = $page->page_id;
        $subscription->save();
      }

      $db->commit();
    } catch( Engine_Image_Exception $e ) {
      $db->rollBack();
      $form->addError(Zend_Registry::get('Zend_Translate')->_('The image you selected was too large.'));
      throw $e;
    }	catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }

	// CREATE TIMELIVE VIEW
	//$this->_createTimeline($page);
	//$page->timeline_converted = 1;
	$page->is_timeline = 0;
	$page->save();
 	$data = array( 'user_id' => $viewer->getIdentity(),
					'page_id' =>$page->page_id,
					'shared_by' => $viewer->getIdentity());
	Engine_Api::_()->getDbTable('pages', 'page')->savePageCategory($page->page_id,$categoryId);					
	Engine_Api::_()->getDbtable('pages', 'page')->setPagesList($data);
	
	$this->_redirectCustom(array('route' => 'page_team', 'action' => 'edit', 'page_id' => $page->page_id)); 
	/*
	### un-comment for enabling payment gateway.
	if($profile_type=='Agency')
	{
	  Engine_Api::_()->getDbTable('pages', 'page')->savePageAsDraft($page->page_id);
	 $this->_redirectCustom(array('route' => 'page_pay', 'action' => 'pay', 'page_id' => $page->page_id));
	}
	else
	{
	}
    $this->_redirectCustom(array('route' => 'page_team', 'action' => 'edit', 'page_id' => $page->page_id));*/
  }
  
  public function setCoverPhoto($photo = null, $page_id)
    {
        if ($photo instanceof Zend_Form_Element_File) {
            $file = $photo->getFileName();
            $fileName = $file;
        } else if ($photo instanceof Storage_Model_File) {
            $file = $photo->temporary();
            $fileName = $photo->name;
        } else if ($photo instanceof Core_Model_Item_Abstract && !empty($photo->file_id)) {
            $tmpRow = Engine_Api::_()->getItem('storage_file', $photo->file_id);
            $file = $tmpRow->temporary();
            $fileName = $tmpRow->name;
        } else if (is_array($photo) && !empty($photo['tmp_name'])) {
            $file = $photo['tmp_name'];
            $fileName = $photo['name'];
        } else if (is_string($photo) && file_exists($photo)) {
            $file = $photo;
            $fileName = $photo;
        } else {
            throw new User_Model_Exception('invalid argument passed to setTimelinePhoto');
        }

        if (!$fileName) {
            $fileName = $file;
        }

        $extension = ltrim(strrchr(basename($fileName), '.'), '.');
        $base = rtrim(substr(basename($fileName), 0, strrpos(basename($fileName), '.')), '.');
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary';
        // @TODO user_id - is null correct?
        $params = array(
            'parent_type' => 'cover',
            'parent_id' => $page_id,
            'user_id' => 0,
            'name' => basename($fileName),
        );

        /**
         * Save
         * @var $filesTable Storage_Model_DbTable_Files
         */
        $filesTable = Engine_Api::_()->getDbtable('files', 'storage');

        // Resize image (main)
        $mainPath = $path . DIRECTORY_SEPARATOR . $base . '_m.' . $extension;
        $image = Engine_Image::factory();
        $image->open($file)
//      ->resize(850, 315)
            ->write($mainPath)
            ->destroy();

        // Resize image (icon)
        $image = Engine_Image::factory();
        $image->open($file);

        // Store
        $iMain = $filesTable->createFile($mainPath, $params);

        // Remove temp files
        @unlink($mainPath);

        return $iMain->file_id;
    }
  
  	/* CUSTOM FUNCTION TO UPLOAD REELS*/
  	public function addreelAction()
	{
	$output_dir = "public/temporary/";
	 
	if(isset($_FILES["myfile"]))
		{
		$ret = array();
		
		$error =$_FILES["myfile"]["error"];
		{
		
		if(!is_array($_FILES["myfile"]['name'])) //single file
		{
			$fileName = $_FILES["myfile"]["name"];
			$fileExt=end(explode(".",$fileName));
			$unique_key = substr(md5(rand(0, 10000000)), 0, 7);
 			$fileName=time()."".$unique_key.".".$fileExt;
 			// echo '<pre>'; print_r($_FILES); echo '</pre>'; die;
			move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $fileName);
			 //echo "<br> Error: ".$_FILES["myfile"]["error"];
			$ret=$fileName;
		}
		else
		{
			$fileCount = count($_FILES["myfile"]['name']);
			  for($i=0; $i < $fileCount; $i++)
			  {
				$fileName = $_FILES["myfile"]["name"][$i];
				$fileName = $_FILES["myfile"]["name"][$i];
			    $fileExt=end(explode(".",$fileName));
				$unique_key = substr(md5(rand(0, 10000000)), 0, 7);
 			    $fileName=time()."".$unique_key.".".$fileExt;
				$ret=$fileName;
				move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName );
			  }
		
		}
		}
		echo $ret;
 		die();
 		}
		die;
 	}

	public function _createTimeline($page = null)
    {
        if (!$page) return false;
        $page_id = $page->getIdentity();
        $content_table = Engine_Api::_()->getDbtable('content', 'page');
        $pages_table = Engine_Api::_()->getDbtable('pages', 'page');

        $main = $pages_table->createContentItem(array('page_id' => $page_id, 'type' => 'container', 'order' => 2, 'name' => 'main', 'parent_content_id' => 0, 'is_timeline' => 1));
        $middle = $pages_table->createContentItem(array('page_id' => $page_id, 'type' => 'container', 'order' => 6, 'name' => 'middle', 'parent_content_id' => $main->content_id, 'is_timeline' => 1));

        $timeline_header = $pages_table->createContentItem(array('page_id' => $page_id, 'type' => 'widget', 'order' => 3, 'name' => 'timeline.new-cover', 'parent_content_id' => $middle->content_id, 'is_timeline' => 1));
        $timeline_content = $pages_table->createContentItem(array('page_id' => $page_id, 'type' => 'widget', 'order' => 4, 'name' => 'timeline.new-feed', 'parent_content_id' => $middle->content_id, 'is_timeline' => 1));
		

        //$core_tabs = $pages_table->createContentItem(array('page_id' => $page_id, 'type' => 'widget', 'order' => 5, 'name' => 'core.container-tabs', 'parent_content_id' => $middle->content_id, 'is_timeline' => 1));

        // search for core.container-tabs
        $select = $content_table->select()
            ->where('page_id = ?', $page->page_id)
            ->where('name = ?', 'core.container-tabs')
            ->where('is_timeline = ?', false)
            ->order('order ASC');

        $row = $content_table->fetchRow($select);

        if ($row) {
            $select = $content_table->select()
                ->where('page_id = ?', $page->page_id)
                ->where('parent_content_id = ?', $row->content_id)
                ->order('order ASC');

            $tabs = $content_table->fetchAll($select);
            if ($tabs) {
                foreach ($tabs as $tab) {
					if($tab['name']!='page.feed'){
						$tab = $tab->toArray();
						$tab['is_timeline'] = 1;
						$tab['parent_content_id'] = $timeline_header->content_id;
						unset($tab['content_id']);
	
						$tabs_content = $pages_table->createContentItem($tab);
					}
                }
            }
        }
		
		// search for right sidebar
        $select = $content_table->select()
            ->where('page_id = ?', $page->page_id)
            ->where('name = ?', 'right')
            ->where('is_timeline = ?', false)
            ->order('order ASC');

        $row = $content_table->fetchRow($select);

        if ($row) {
			
			 $pageselect = $content_table->select()
                ->where('page_id = ?', $page->page_id)
                ->where('parent_content_id = ?', $timeline_header->content_id)
                ->order('order ASC');


        	$pagerow = $content_table->fetchAll($pageselect);
		
            $select = $content_table->select()
                ->where('page_id = ?', $page->page_id)
                ->where('parent_content_id = ?', $row->content_id)
                ->order('order ASC');

            $tabs = $content_table->fetchAll($select);
            if ($tabs) {
                foreach ($tabs as $tab) {
                    $tab = $tab->toArray();
                    $tab['is_timeline'] = 1;
                    $tab['parent_content_id'] = $timeline_content->content_id;
                    unset($tab['content_id']);

                    $tabs_content = $pages_table->createContentItem($tab);
					// ADD WIDGETS ON PAGE INFO 
					//$tab = $tab->toArray();
					
					if ($pagerow) {
						foreach ($pagerow as $prow) {
							$tab['is_timeline'] = 1;
							$tab['parent_content_id'] = $prow->content_id;
							unset($tab['content_id']);
							$tabs_content = $pages_table->createContentItem($tab);
						}
					  }
                    /*$tab['is_timeline'] = 1;
                    $tab['parent_content_id'] = $pagerow->content_id;
                    unset($tab['content_id']);
					$tabs_content = $pages_table->createContentItem($tab);*/
                    
                }
            }
        }
		
        return true;
    }

  public function validateAction()
  {
  	if( !$this->_helper->requireUser()->isValid() || !$this->_helper->requireAuth()->setAuthParams('page', null, 'create')->checkRequire() ) {
  		return;
  	}

  	$url = $this->_getParam('url');
	$page_id = $this->_getParam('page_id');
	

  	 $url = strtolower(trim($url));
		$url = preg_replace('/[^a-z0-9-]/', '-', $url);
		$url = preg_replace('/-+/', "-", $url);

  	$table = Engine_Api::_()->getDbTable('pages', 'page');

    if( !strlen($url) ) {
      $this->view->success = 0;
      $this->view->message = Zend_Registry::get('Zend_Translate')->_("Url is Empty");
    } elseif ($table->checkUrl($url, $page_id)){
  		$this->view->success = 0;
  		$this->view->message = Zend_Registry::get('Zend_Translate')->_("Page with this url is already exists.");
        return;
  	}
    else{
  		$this->view->success = 1;
  		$this->view->message = Zend_Registry::get('Zend_Translate')->_("This url is free.");
  	}
    $pTable = Engine_Api::_()->getDbTable('settings', 'core');
    $setting = $pTable->fetchRow($pTable->select()->where('name=?','page.short.url' ));
    if($setting->value && $setting->value == 1){
      $subject = Engine_Api::_()->user()->getUser($url);
      if( $subject->getIdentity() )
      {
        $this->view->success = 0;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_("This url is already exists. Url used by user!");
          return;
      }
      $banUsername = Engine_Api::_()->getDbTable('bannedUsernames', 'core')->getUsernames();
      if(in_array($url,$banUsername)){
        $this->view->success = 0;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_("This url is already exists. Url banned!");
          return;
      }
      $banWords = Engine_Api::_()->getDbTable('bannedWords', 'core')->getWords();
      if(in_array($url,$banWords)){
        $this->view->success = 0;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_("This url is already exists. Url banned!");
          return;
      }
      $url_curl = rtrim((constant('_ENGINE_SSL') ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] ) . constant('_ENGINE_R_BASE').'/'.$url;
      $checkUrl = curl_init();
      curl_setopt($checkUrl, CURLOPT_URL,$url_curl );
      curl_setopt($checkUrl, CURLOPT_RETURNTRANSFER, 1);
      $r = curl_exec($checkUrl);

      $stat = curl_getinfo ( $checkUrl,CURLINFO_HTTP_CODE  );

      curl_close($checkUrl);

       

      if($stat>=400){
        $this->view->success = 1;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_("This url is free.");
      }
      else{
        $this->view->success = 0;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_("Url used by module.");
      }





    }

  	return;
  }
  public function validateurl($url = null)
  {

    if ($url == null) $valid['result'] = false;
    $ch = curl_init($url);
    $useragent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1";
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($ch,CURLOPT_VERBOSE,false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch,CURLOPT_SSLVERSION,3);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, FALSE);
    $data = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $valid['httpcode'] = $httpcode;
    show ($url . " is validated with httpcode " . $httpcode);
    show("Got code in validateurl: " . $valid[httpcode]);
    if ($httpcode >= 200 && $httpcode < 400) {
      $valid[result] = true;
    } else {
      $valid['result'] = false;
    }
    return $valid;
  }
  public function indexAction()
  {
    //if( !$this->_helper->requireAuth()->setAuthParams('page', null, 'view')->isValid() ) return;

    $viewer = Engine_Api::_()->user()->getViewer();
    $settings = Engine_Api::_()->getDbTable('settings', 'core');
    /*if ($settings->getSetting('page.public.mode', 0) == 0 && !$viewer->getIdentity()) {
      return $this->_forward('requireauth', 'error', 'core');
    }*/
	
	
	if (!$viewer->getIdentity()) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }else{
	  return $this->_helper->redirector->gotoRoute(array(), 'page_manage', true);	
	}
	

    // Render
    $this->_helper->content
        ->setNoRender()
        ->setEnabled()
        ;
	die;
  }

  public function manageAction()
  {
	  
  	if ( !$this->_helper->requireUser->isValid() ) return ;
	
	
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	 if(strtolower($profile_type) == 'talent'){
		 $menu_type	=	'user_edit';
	 }else if(strtolower($profile_type) == 'agency'){
		$menu_type	=	'custom_33'; 
	 }else {
		$menu_type	=	'custom_31'; 
	 }
		
	
	
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('user_edit');
    $viewer = $this->_helper->api()->user()->getViewer();
    $table = $this->_helper->api()->getDbtable('pages', 'page');
    $membershipTbl = $this->_helper->api()->getDbtable('membership', 'page');

    $select = $table
      ->select()->setIntegrityCheck(false)
      ->from(array('pages' => 'engine4_user_pages'))
	  ->join(array('page' => 'engine4_page_pages'), 'pages.user_id = page.user_id', array())
	  ->joinLeft(array('m' => 'engine4_page_membership'), "pages.page_id = m.resource_id AND m.type = 'ADMIN'", array())
      ->joinLeft(array('fv' => 'engine4_page_fields_values'), "fv.item_id = pages.page_id", array())
      ->joinLeft(array('fo' => 'engine4_page_fields_options'), "fo.option_id = fv.value", array('category' => 'label', 'category_id' => 'option_id'))
       ->where('pages.user_id = ?', $viewer->getIdentity())
	   ->order('page.creation_date DESC')
	   ->group("pages.page_id");
	  
	 
	
	
    $this->view->paginator = $paginator = Zend_Paginator::factory($select);
	 
    $this->view->owner = Engine_Api::_()->user()->getViewer();

    $settings = Engine_Api::_()->getDbTable('settings', 'core');
    $ipp = $settings->getSetting('page.browse_count', 15);

    $paginator->setItemCountPerPage($ipp);
    $paginator->setCurrentPageNumber($this->_getParam('page'));
	
    $page_ids = array();
    foreach ($paginator as $page) {
      $page_ids[] = $page->getIdentity();
	  
	   
    }
	
	 
	
	 
	$this->view->page_ids=$page_ids;
	$this->view->user_id=$viewer->getIdentity();
	$this->view->profile_type=strtolower($profile_type);
    $this->view->page_tags = Engine_Api::_()->page()->getPageTags($page_ids);
    $this->view->page_likes = Engine_Api::_()->like()->getLikesCount('page', $page_ids);
  }

  public function timelineViewAction()
  {
    /**
     * @var $pageObject Page_Model_Page
     */
    $pageObject = null;

    if (Engine_Api::_()->core()->hasSubject('page')) {
      $pageObject = Engine_Api::_()->core()->getSubject('page');
    }


    if (null == $pageObject) {
      $this->_forward('success', 'utility', 'core', array(
        'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'page_browse', true),
        'messages' => array($this->view->translate('Pages does not exists.'))
      ));
    }

    $viewer = $this->_helper->api()->user()->getViewer();

    if (!($pageObject->isOwner($viewer) || $pageObject->isEnabled())) {
      $this->_redirectCustom(array('route' => 'page_package_choose', 'page_id' => $pageObject->page_id));
    }

    if (!($pageObject->isOwner($viewer) || $pageObject->approved)) {
      $this->_redirectCustom(array('route' => 'page_browse'));
    }

    if (!$this->_helper->requireSubject()->isValid()) return 0;
    if (!$this->_helper->requireAuth()->setAuthParams($pageObject, $viewer, 'view')->isValid()) return 0;

    $pageObject->viewPage();
    $pageObject->description = stripslashes($pageObject->description);

    $path = Zend_Controller_Front::getInstance()->getControllerDirectory('page');
    $path = dirname($path) . '/layouts';

    $layout = Zend_Layout::startMvc();
    $layout->setViewBasePath($path);

    $content = Engine_Content::getInstance();
    $contentTable = Engine_Api::_()->getDbtable('pages', 'page');
    $content->setStorage($contentTable);

    $this->view->headTitle()->setAutoEscape(false);
    $this->view->headMeta()->setAutoEscape(false);
    $this->view->page = $pageObject;

    $tags = $pageObject->tags()->getTagMaps();
    $tagString = '';
    foreach ($tags as $tagmap) {
      if ($tagString !== '') $tagString .= ', ';
      $tagString .= $tagmap->getTag()->getTitle();
    }

    if (!empty($tagString)) {
      $this->view->headMeta()->appendName('keywords', $tagString);
    }

    if (!empty($pageObject->layout)) {
      $this->_helper->layout->setLayout($pageObject->layout);
    }

    $this->_helper->content->setContentName($pageObject->getIdentity())->setEnabled();
    return 0;
  }

	public function viewAction()
	{
		/**
		 * @var $pageObject Page_Model_Page
		 */
    $pageObject = null;
    $viewer = Engine_Api::_()->user()->getViewer();
    $settings = Engine_Api::_()->getDbTable('settings', 'core');
    if ($settings->getSetting('page.public.mode', 0) == 0 && !$viewer->getIdentity()) {
      return $this->_forward('requireauth', 'error', 'core');
    }

    if (Engine_Api::_()->core()->hasSubject('page')) {
      $pageObject = Engine_Api::_()->core()->getSubject('page');
    }

    if (null == $pageObject) {
      return $this->_forward('success', 'utility', 'core', array(
        'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'page_browse', true),
        'messages' => array($this->view->translate('Pages does not exists.'))
      ));
    }

    if( !$pageObject->isDefaultPackageEnabled() ) {
      if ( $pageObject->isOwner($viewer) ) {
        $this->_redirectCustom(array('route' => 'page_package_choose', 'page_id' => $pageObject->page_id));
      } else {
        $this->_redirectCustom(array('route' => 'page_browse'));
      }
    }

		if ( !$pageObject->isOwner($viewer) && !$pageObject->approved ) {
			$this->_redirectCustom(array('route' => 'page_browse'));
		}

		if( !$this->_helper->requireSubject()->isValid() ) return 0;
    //if( !$this->_helper->requireAuth()->setAuthParams($pageObject, $viewer, 'view')->isValid() ) return 0;

		if( !$pageObject->parent_id != $viewer->getIdentity()) {
			$data	=	array('viewer_id' => $viewer->getIdentity(), 'page_id' => $pageObject->getIdentity(), 'ip_addr'	=>	$_SERVER['REMOTE_ADDR']);
			$isAreadyView	=	Engine_Api::_()->getDbtable('views', 'page')->isViewed($data);
			if(!$isAreadyView){
			  $pageObject->viewPage();
			}
		}
		
		
		$pageObject->description = stripslashes($pageObject->description);

		$path = Zend_Controller_Front::getInstance()->getControllerDirectory('page');
    $path = dirname($path) . '/layouts';

		$layout = Zend_Layout::startMvc();
		$layout->setViewBasePath($path);

		$content = Engine_Content::getInstance();
		$contentTable = Engine_Api::_()->getDbtable('pages', 'page');
    $content->setStorage($contentTable);

    $this->view->headTitle()->setAutoEscape(false);
    $this->view->headMeta()->setAutoEscape(false);
    $this->view->page = $pageObject;

    $tags = $pageObject->tags()->getTagMaps();
    $tagString = '';
    foreach( $tags as $tagmap ) {
      if( $tagString !== '' ) $tagString .= ', ';
      $tagString .= $tagmap->getTag()->getTitle();
    }

    if( !empty($tagString) ) {
      $this->view->headMeta()->appendName('keywords', $tagString);
    }

    if( !empty($pageObject->layout) ) {
      $this->_helper->layout->setLayout($pageObject->layout);
    }

    $this->_helper->content->setContentName($pageObject->getIdentity())->setEnabled();

    if (!$pageObject->isAllowStyle()) {
      return 0;
    }
    // Get styles
    $table = Engine_Api::_()->getDbtable('styles', 'core');
    $select = $table->select()
      ->where('type = ?', $pageObject->getType())
      ->where('id = ?', $pageObject->getIdentity())
      ->limit();

    $row = $table->fetchRow($select);
    if (null !== $row && !empty($row->style)) {
      $this->view->headStyle()->appendStyle($row->style);
    }

    return 0;
	}

	public function largeMapAction()
	{
		$page_id = (int)$this->_getParam('page_id');

		if ($page_id === null){
			$this->view->result = 0;
			$this->view->message = Zend_Registry::get('Zend_Translate')->_("Undefined page.");
			return ;
		}

		$pageObject = Engine_Api::_()->getItem('page', $page_id);

		if(!$this->_helper->requireAuth()->setNoForward()->setAuthParams($pageObject, Engine_Api::_()->user()->getViewer(),'view')->checkRequire()){
			$this->view->result = 0;
      $this->view->message = Zend_Registry::get('Zend_Translate')->_("No rights");
      return;
		}

		$table = Engine_Api::_()->getDbTable('pages', 'page');

    $select = $table
      ->select()->setIntegrityCheck(false)
      ->from(array('page' => 'engine4_page_pages'))
      ->joinLeft(array('marker' => 'engine4_page_markers'), 'marker.page_id = page.page_id', array('marker_id', 'latitude', 'longitude'))
      ->where('page.page_id = ?', $page_id);

    $this->view->page = $page = $table->fetchRow($select);
    $markers = array();

    if ($page->marker_id > 0){
	    $markers[0] = array(
				'marker_id' => $page->marker_id,
				'lat' => $page->latitude,
				'lng' => $page->longitude,
				'pages_id' => $page->page_id,
				'pages_photo' => $page->getPhotoUrl('thumb.normal'),
				'title' => $page->getTitle(),
				'desc' => substr($page->getDescription(),0,200),
	      'url' => $page->getHref()
			);

			$this->view->markers = Zend_Json_Encoder::encode($markers);
			$this->view->bounds = Zend_Json_Encoder::encode(Engine_Api::_()->getApi('gmap', 'page')->getMapBounds($markers));
    }
	}

  public function printAction()
  {
    $page_id = (int)$this->_getParam('page_id', 0);
    $this->view->page = $page = Engine_Api::_()->getItem('page', $page_id);
    Engine_Api::_()->core()->setSubject($page);

    $this->view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
    $this->view->fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($page);

    /**
     * @var $table Page_Model_DbTable_Pages
     */
    $table = Engine_Api::_()->getDbTable('pages', 'page');
    $prefix = $table->getTablePrefix();
    $params = array('page_id' => $page_id);
    $select = $table->getSelect($params);
    $select->joinLeft($prefix.'page_markers', $prefix.'page_markers.page_id = '.$prefix.'page_pages.page_id', array('marker_id', 'longitude', 'latitude'));
    $page = $table->fetchRow($select);

    $markers = array();
    if ($page->marker_id > 0) {
      $markers[0] = array(
        'marker_id' => $page->marker_id,
        'lat' => $page->latitude,
        'lng' => $page->longitude,
        'pages_id' => $page->page_id,
        'pages_photo' => $page->getPhotoUrl('thumb.normal'),
        'title' => $page->getTitle(),
        'desc' => Engine_String::substr($page->getDescription(),0,200),
        'url' => $page->getHref()
      );

      $this->view->markers = Zend_Json_Encoder::encode($markers);
      $this->view->bounds = Zend_Json_Encoder::encode(Engine_Api::_()->getApi('gmap', 'page')->getMapBounds($markers));
    }
  }

  public function claimAction()
  {
    if ( !$this->_helper->requireUser->isValid() ) return ;

    $viewer = Engine_Api::_()->user()->getViewer();
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('page_main');

    $page_id = $this->getRequest()->getParam('id');
    $form = new Page_Form_ClaimPage();

    if(intval($page_id) > 0) {
      $row = Engine_Api::_()->getItemTable('page')->findRow($page_id);
      $form->getElement('title')->setDescription('');
      $form->getElement('title')->setValue($row->getTitle());
      $form->getElement('page_id')->setValue($page_id);
    }

    $this->view->form = $form;

    $translate = Zend_Registry::get('Zend_Translate');

    if( !$this->getRequest()->isPost() ) {
      return;
    }

    if( !$form->isValid($values = $this->getRequest()->getPost()) ) {
      $form->populate($values);
      return;
    }
    /**
     * @var $table Page_Model_DbTable_Claims
     **/
    $table = Engine_Api::_()->getDbTable('claims', 'page');

    $page_id = $values['page_id'];
    $claimed = $table->checkClaim($page_id);

    if ($claimed) {
      $status = $claimed->status;
      if ($status == 'pending') {
        $form->addError($translate->_('You have already claimed this page, your claim is under consideration of Administrator.'));
        return ;
      } elseif ($status == 'declined') {
        $form->addError($translate->_('You have already claimed this page, your claim has been declined by Administrator.'));
        return ;
      }
    }

    $page = Engine_Api::_()->getItem('page', $page_id);
    if ($page === null) {
      $form->addError($translate->_('This page not found, please choose page from drop down choice list.'));
      return;
    } elseif ($page->title != $values['title']) {
      $form->addError($translate->_('This page has not been found, please choose page from drop down choice list.'));
      return;
    }

    $db = $table->getAdapter();
    $db->beginTransaction();

    $claiming = $table->createRow();

    try {
      $values['user_id'] = $viewer->getIdentity();
      $claiming->setFromArray($values);
      $claiming->save();
      $db->commit();
      $values['page_id'] = 0;
      $values['title'] = '';
      $form->populate($values);
      $form->addNotice($translate->_('Your Notice successfully sent.'));
    } catch(Exception $e) {
      $db->rollBack();
      throw $e;
    }
  }

  public function suggestAction()
  {
    $pages = $this->getPagesByText($this->_getParam('text'), $this->_getParam('limit', 40));

    $data = array();
    $mode = $this->_getParam('struct');

    if( $mode == 'text' ) {
      foreach( $pages as $page ) {
        $data[] = $page->title;
      }
    } else {
      foreach( $pages as $page ) {
        $pagePhoto = $this->view->itemPhoto($page, 'thumb.icon');
        $data[] = array(
          'id' => $page->page_id,
          'label' => $page->title,
          'photo' => $pagePhoto
        );
      }
    }

    if( $this->_getParam('sendNow', true) ) {
      return $this->_helper->json($data);
    } else {
      $this->_helper->viewRenderer->setNoRender(true);
      $data = Zend_Json::encode($data);
      $this->getResponse()->setBody($data);
    }
  }

  public function termsAction()
  {
    if ( !$this->_helper->requireUser->isValid() ) return ;
    $this->_helper->layout->setLayout('default-simple');
  }

  private function getPagesByText($text = null, $limit = 10)
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    /**
     * @var $table Page_Model_DbTable_Pages
     **/
    $table = Engine_Api::_()->getDbtable('pages', 'page');
    $claimersTbl = Engine_Api::_()->getDbtable('settings', 'user');
    $select = $table->select()
      ->setIntegrityCheck(false)
      ->from(array('p' => $table->info('name')))
      ->joinLeft(array('c' => $claimersTbl->info('name')), 'p.user_id = c.user_id', array())
      ->where('c.name = ?', 'claimable_page_creator')
      ->where('p.user_id <> ?', $viewer->getIdentity())
      ->order('p.title ASC')
      ->limit($limit);

    if( $text ) {
      $select->where('p.title LIKE ?', '%'.$text.'%');
    }

    return $table->fetchAll($select);
  }

  private function _createDefaultContent()
  {
    $pageTable = Engine_Api::_()->getDbTable('pages', 'page');
    $page = "default";

    $pageObject = $pageTable->fetchRow($pageTable->select()->where('name = ?', $page)->orWhere('page_id = ?', $page));
		$contentTable = Engine_Api::_()->getDbtable('content', 'page');

    $contentDefault = $contentTable->fetchAll($contentTable->select()->where('page_id=?', $pageObject->getIdentity()));

    if(count($contentDefault) == 0) {
      $pageTable->createContentFirstTime($pageObject->getIdentity());
    }
  }

  public function browsereviewsAction()
  {
    //Render Layout Reviews
    $this->_helper->content
      ->setNoRender()
      ->setEnabled()
    ;
  }
  public function sendAction()
  {
    $form = new Page_Form_Send();
    if ($this->_request->isPost()) {
      // Don't render this if not authorized
      $viewer = Engine_Api::_()->user()->getViewer();
      $request = Zend_Controller_Front::getInstance()->getRequest();
      $page_id = $request->getParam('page_id');
      if( !Engine_Api::_()->core()->hasSubject() ) {
        if( !$page_id )
          return $this->setNoRender();
        else {
          $page = Engine_Api::_()->getItem('page', $page_id);
          Engine_Api::_()->core()->setSubject($page);
        }
      }
      $admins = $page->getTeam();
      $values = $form->isValid($this->_getAllParams());
      $values = $form->getValues();
      $convoTable = Engine_Api::_()->getDbTable('conversations', 'messages');
      $notificationTable = Engine_Api::_()->getDbTable('notifications', 'activity');
      if(empty($values['title']) ||empty($values['text']) ){

        $this->view->form = $form;
        return;
      }
      //$recipients = array();
      foreach ($admins as $admin) {
        if ($viewer->getIdentity() == $admin->user_id ) {
          continue ;
        }

        $recipient = $admin->user_id;
        $user = Engine_Api::_()->getItem('user', $admin->user_id);
        $notificationTable->addNotification($user, $viewer, $page, 'page_send_message');

        if (!empty($recipient)){
          $convoTable->send($viewer, $recipient,$values['title'],$values['text']);
        }
      }
      $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => true,
        'parentRefresh' => false,
        'messages' => array(Zend_Registry::get('Zend_Translate')->_('like_Your message successfully sent.'))
      ));

    }else{

      $this->view->form = $form;
    }

  }
}