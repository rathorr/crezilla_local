<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: PageController.php 2010-08-31 16:05 idris $
 * @author     Idris
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class Page_PageController extends Core_Controller_Action_Standard
{
	public function init()
  {
    if( !$this->_helper->requireUser()->isValid() ) return;
		$page_id = (int)$this->_getParam('page_id');


    $this->view->page = $page = Engine_Api::_()->getItem('page', $page_id);
    if( !$page ) {
		 
      $this->_redirectCustom(array('route' => 'page_browse'));
    }
    if (!Engine_Api::_()->core()->hasSubject()) {
      Engine_Api::_()->core()->setSubject($page);
    }
    $this->view->viewer = Engine_Api::_()->user()->getViewer();
		if ($page == null) {
			 
			$this->_redirectCustom(array('route' => 'page_browse'));
		}

		 

    /**
     *  @var $settings Core_Model_DbTable_Settings
     */
    $settings = Engine_Api::_()->getDbTable('settings', 'core');
    $viewer = Engine_Api::_()->user()->getViewer();
    $isOwner = $page->isOwner($viewer);

    if($this->_getParam('action') != 'delete' && !$page->isDefaultPackageEnabled() ) {
      if ( $isOwner ) {
        $this->_redirectCustom(array('route' => 'page_package_choose', 'page_id' => $page->page_id));
      } else {
        $this->_redirectCustom(array('route' => 'page_browse'));
      }
    }


    if ($settings->getSetting('page.package.enabled', 0)) {
      $this->view->packageEnabled = true;
      $this->view->isOwner = $isOwner;
      $this->view->package =  $package = $page->getPackage();
      $this->view->currency = $currency = Engine_Api::_()->getDbTable('settings', 'core')->getSetting('payment.currency', 'USD');
      if ( null != ($subscription = Engine_Api::_()->getItemTable('page_subscription')->getSubscription($page->getIdentity(), true))) {
        $this->view->subscription_expired = $subscription->expiration_date;
      }
      if( !$package ) {
        $package = Engine_Api::_()->getDbTable('packages', 'page')->getDefaultPackage();
      }
      $this->view->isDefaultPackage = $package->isDefault();
    }

    $this->view->isAllowLayout = $page->isAllowLayout();
    $this->view->isAllowPagecontact = $page->isAllowPagecontact();
    $this->view->isAllowPagefaq = $page->isAllowPagefaq();
    $this->view->isAllowStore = $page->isAllowStore();
    $this->view->isAllowDonation = $page->isAllowDonation();
    $this->view->isAllowStyle = $page->isAllowStyle();
    $this->view->isAllowInvite = Engine_Api::_()->getDbTable('modules', 'core')->isModuleEnabled('inviter');
    $this->view->isAllowedBadge = Engine_Api::_()->getDbTable('modules', 'core')->isModuleEnabled('hebadge');
    $this->view->action = $this->_getParam('action');

	}
	
  public function deleteAction()
  {
	 
		/**
		 * @var $page Page_Model_Page
		 */
  	$page_id = $this->_getParam('page_id');
	
 	$page = $this->view->page;
 		
  	$this->view->form = $form = new Page_Form_Delete();
  	
  	$form->setAction($this->view->url(array('action' => 'delete', 'page_id' => $page_id), 'page_team'));
  	$description = sprintf(Zend_Registry::get('Zend_Translate')
  	  ->_('PAGE_DELETE_DESC'), $this->view->htmlLink($page->getHref(), $page->getTitle()));
  	  
  	$form->setDescription($description);
  	
  	if (!$this->getRequest()->isPost()) {
  		return;
  	}
	$this->_helper->layout()->disableLayout(); 
	$db = Engine_Api::_()->getDbtable('pages', 'page')->getAdapter();
    $db->beginTransaction();
    
    try {
		Engine_Api::_()->getDbTable('pages', 'page')->deletePageAssignedUser($page_id);	
      if (null != ($subs = Engine_Api::_()->getItemTable('page_subscription')->getSubscription($page_id))) {
        $subs->delete();
		
      }

      $page->delete();
    	$db->commit();
    } catch (Exception $e) {
    	$db->rollBack();
    	throw $e;
    }
	
	$this->_forward('success', 'utility', 'core', array(
	 'parentRedirect' => $this->view->url(array('action' => 'index'), 'page_manage', true),
      'smoothboxClose' => true,
      'parentRefresh' => true,
      'messages' => array(Zend_Registry::get('Zend_Translate')->_('Your page has been deleted.'))
    ));
	
    
    //$this->_redirectCustom(array('route' => 'page_manage'));
  }
    
  public function deletePhotoAction()
  {
    // Get form
    $this->view->form = $form = new Page_Form_DeletePhoto();

    if (!$this->getRequest()->isPost() || !$form->isValid($this->getRequest()->getPost())) {
      return;
    }

    $page = Engine_Api::_()->core()->getSubject();
    $page->removePhotos();
    $page->photo_id = 0;
    $page->save();

    $this->view->status = true;
    $this->view->message = Zend_Registry::get('Zend_Translate')->_('Your photo has been deleted.');

    $this->_forward('success', 'utility', 'core', array(
      'parentRedirect' => $this->view->url(array('action' => 'edit-photo', 'page_id' => $page->getIdentity()), 'page_team', true),
      'smoothboxClose' => true,
      'parentRefresh' => false,
      'messages' => array(Zend_Registry::get('Zend_Translate')->_('Your photo has been deleted.'))
    ));
  }
  
  public function setCoverPhoto($photo = null, $page_id)
    {
        if ($photo instanceof Zend_Form_Element_File) {
            $file = $photo->getFileName();
            $fileName = $file;
        } else if ($photo instanceof Storage_Model_File) {
            $file = $photo->temporary();
            $fileName = $photo->name;
        } else if ($photo instanceof Core_Model_Item_Abstract && !empty($photo->file_id)) {
            $tmpRow = Engine_Api::_()->getItem('storage_file', $photo->file_id);
            $file = $tmpRow->temporary();
            $fileName = $tmpRow->name;
        } else if (is_array($photo) && !empty($photo['tmp_name'])) {
            $file = $photo['tmp_name'];
            $fileName = $photo['name'];
        } else if (is_string($photo) && file_exists($photo)) {
            $file = $photo;
            $fileName = $photo;
        } else {
            throw new User_Model_Exception('invalid argument passed to setTimelinePhoto');
        }

        if (!$fileName) {
            $fileName = $file;
        }

        $extension = ltrim(strrchr(basename($fileName), '.'), '.');
        $base = rtrim(substr(basename($fileName), 0, strrpos(basename($fileName), '.')), '.');
        $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary';
        // @TODO user_id - is null correct?
        $params = array(
            'parent_type' => 'cover',
            'parent_id' => $page_id,
            'user_id' => 0,
            'name' => basename($fileName),
        );

        /**
         * Save
         * @var $filesTable Storage_Model_DbTable_Files
         */
        $filesTable = Engine_Api::_()->getDbtable('files', 'storage');

        // Resize image (main)
        $mainPath = $path . DIRECTORY_SEPARATOR . $base . '_m.' . $extension;
        $image = Engine_Image::factory();
        $image->open($file)
//      ->resize(850, 315)
            ->write($mainPath)
            ->destroy();

        // Resize image (icon)
        $image = Engine_Image::factory();
        $image->open($file);

        // Store
        $iMain = $filesTable->createFile($mainPath, $params);

        // Remove temp files
        @unlink($mainPath);

        return $iMain->file_id;
    }
  
  public function setProfilePhoto($photo = null, $page_id)
    {
		if( $photo instanceof Zend_Form_Element_File ) {
		  $file = $photo->getFileName();
		} else if ($photo instanceof Core_Model_Item_Abstract && !empty($photo->file_id)) {
		  $tmpRow = Engine_Api::_()->getItem('storage_file', $photo->file_id);
	//            $file = $tmpRow->temporary();
		  $file = $tmpRow->name;
		} else if( is_array($photo) && !empty($photo['tmp_name']) ) {
		  $file = $photo['tmp_name'];
		} else if( is_string($photo) && file_exists($photo) ) {
		  $file = $photo;
		} else {
		  throw new Event_Model_Exception('invalid argument passed to setPhoto');
		}
	
		$name = basename($file);
		$path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary';
		$params = array(
		  'parent_id' => $page_id,
		  'parent_type'=>'page'
		);
	
		// Remove photos
		$this->removePhotos();
	
		// Save
		$storage = Engine_Api::_()->storage();
	
		// Resize image (main)
		$mainPath = $path . '/m_' . $name;
		$image = Engine_Image::factory();
		$image->open($file)
		//->resize(175, 200)
		  ->resize(300, 600)
		  ->write($path.'/m_'.$name)
		  ->destroy();
	
		// Resize image (normal)
		$normalPath = $path . '/in_' . $name;
		$image = Engine_Image::factory();
		$image->open($file)
		  ->resize(210, 240)
		  ->write($path.'/in_'.$name)
		  ->destroy();
	
		// Resize image (icon)
		$iconPath = $path . '/is_' . $name;
		$image = Engine_Image::factory();
		$image->open($file);
	
		$size = min($image->height, $image->width);
		$x = ($image->width - $size) / 2;
		$y = ($image->height - $size) / 2;
	
		$image->resample($x, $y, $size, $size, 48, 48)
		  ->write($path.'/is_'.$name)
		  ->destroy();
	
		// Store
		try {
		  $iMain = $storage->create($path.'/m_'.$name, $params);
		  $iIconNormal = $storage->create($path.'/in_'.$name, $params);
		  $iSquare = $storage->create($path.'/is_'.$name, $params);
	
		  $iMain->bridge($iIconNormal, 'thumb.normal');
		  $iMain->bridge($iSquare, 'thumb.icon');
		} catch( Exception $e ) {
		  // Remove temp files
		  @unlink($mainPath);
		  @unlink($normalPath);
		  @unlink($iconPath);
		  // Throw
		  if( $e->getCode() == Storage_Model_DbTable_Files::SPACE_LIMIT_REACHED_CODE ) {
			throw new Album_Model_Exception($e->getMessage(), $e->getCode());
		  } else {
			throw $e;
		  }
		}
	
		// Remove temp files
		@unlink($mainPath);
		@unlink($normalPath);
		@unlink($iconPath);
		@unlink($file);
	
		// Update row
		//$this->modified_date = date('Y-m-d H:i:s');
		return $this->photo_id = $iMain->file_id;
		//$this->save();
	
		return $this;
	  }
  public function removePhotos()
  {
    if (isset($this->photo_id) && $this->photo_id != 0){
      $storage = Engine_Api::_()->storage();
      $file = $storage->get($this->photo_id);
      if ($file !== null) $file->remove();
      $file = $storage->get($this->photo_id, 'thumb.normal');
      if ($file !== null) $file->remove();
      $file = $storage->get($this->photo_id, 'thumb.icon');
      if ($file !== null) $file->remove();
    }
  }
  
  public function editAction()
  {
	$page = $this->view->page;
	$subject = $this->view->page;
    $table = Engine_Api::_()->getDbTable('settings', 'core');
	$viewer = Engine_Api::_()->user()->getViewer();
    $coordinates = $this->_getParam('coordinates', false);
	
	///// Designation Categories start
   $dasignatedsubcategories = Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getsuppliersDesignations();
  $deoptions  = array();
    if($dasignatedsubcategories){
      foreach($dasignatedsubcategories as $desig){
        $deoptions[$desig['dasignatedsubcategory_id']]  = $desig['dasignatedsubcategory_name']; 
      }
    }
    asort($deoptions, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
    //print_r($deoptions);
   // die();
  $this->view->dasignatedsubcategories = $deoptions; 
  
  $userdcs  = Engine_Api::_()->getDbtable('dasignatedcategories', 'page')->pageDasignatedcategories($page->page_id);
  $this->view->userdcs=$userdcs;
  //echo '<pre>';print_r($userdcs); die();
  $udcoptions = array();
    if($userdcs){
      foreach($userdcs as $udsc){
        //echo $udsc['ind_primary_id'];
        $udcoptions[$udsc['dasignatedcategories_id']] = $udsc['dc_primary_id']; 
      }
    }
 
  $this->view->userdc = $udcoptions;
  //// Designation Categories end
	// LOAD PHOTO FORM
	$this->view->photoform = $form = new Page_Form_Photo();
	
    if (!$page->approved) {
      $page->search = $page->approved;
    }

	if( !empty($_FILES['photo']['name']) ) {
        $page->setPhoto($_FILES['photo']);
      }
	  
	if(!empty($_FILES['cover']['name'])){
			$photo_id = $this->setCoverPhoto($_FILES['cover'], $page->page_id);
			$setting_name = Engine_Api::_()->timeline()->getCoverPhotoSetting($page->page_id, 'page', 'cover');
			$table->setSetting($setting_name, $photo_id);

			$iMain = Engine_Api::_()->getItem('storage_file', $photo_id);

			// Insert activity
			$activity_type = 'post_self';
			$body = '';

			$attachment = null;
			// Hooks to enable albums to work
			$event = Engine_Hooks_Dispatcher::_()
				->callEvent('onSubjectTimelinePhotoUpload', array(
					'subject' => $page,
					'file' => $iMain,
					'type' => 'cover'
				));

			$attachment = $event->getResponse();

			$cover_parent = 'album';

			if (!$attachment) {
				$attachment = $iMain;
				$cover_parent = 'storage';
			} else {
				if ($attachment->getType() == 'pagealbumphoto') {
					$cover_parent = 'pagealbum';
				}
			}

			$cover_parent_setting_name = Engine_Api::_()->timeline()->getCoverParentSetting($page->page_id, 'page', 'cover');
			$table->setSetting($setting_name, $attachment->getIdentity());
			$table->setSetting($cover_parent_setting_name, $cover_parent);


	}
	
	
	
	if ($this->getRequest()->isPost()) {
		
	  $categoryId=$_POST['category_id'];
      $table = Engine_Api::_()->getDbTable('pages', 'page');
      $values = $this->getRequest()->getPost();
	  	
	  // ADD OR REMOVE MEDIA REELS
	  if($values){
		  $remove_reel_ids	=	explode(',',$values['remove_reel_ids']);
		  foreach($remove_reel_ids as $rel){
			  if($rel){
				$reel	=	Engine_Api::_()->getItem('reel', $rel);
				$reel->is_deleted	=	1;
				$reel->save();
			  }
		  }
	  }
	 
	  $files_name=$_POST['file_name'];
		$files_name=explode("||",$files_name);
		 
		  if(!empty($files_name)){
			 
			foreach($files_name as $key=> $val)
			{
				if($val!="")
				{
					$file_extension = trim(strtolower(end(explode('.', $val))));
					if($file_extension =='pdf'){
						$format	=	'1';	
					}else{
						$format	=	'2';		
					}
					rename("public/temporary/".$val."", "public/page/reels/".$val."");
					$tablereel = Engine_Api::_()->getDbTable('reels', 'page');
					$reel = $tablereel->createRow();
					$reel->reel_path	=	$val;
					$reel->format		=	$format;
					$reel->page_id		=	$page->page_id;
					$reel->created_on	=	date('Y-m-d H:i:s', time());
					//echo '<pre>'; print_r($document); echo '</pre>'; die;
					$reel->save();

				}
			}
		  }
		  
		// ADD OR REMOVE LINK REELS
	  if($values['remove_showlink_ids']){
		  $remove_showlink_ids	=	explode(',',$values['remove_showlink_ids']);
		  foreach($remove_showlink_ids as $link){
			  if($link){
				$link	=	Engine_Api::_()->getItem('reel', $link);
				$link->is_deleted	=	1;
				$link->save();
			  }
		  }
	  }
	  
	  if(!empty($values['showlink'])){
			$i=0;
			foreach($values['showlink'] as $lnk){
				if($lnk){
					if($values["showlink_id"][$i]){
						$link	=	Engine_Api::_()->getItem('reel', $values["showlink_id"][$i]);
						$link->reel_path	=	$lnk;
						$link->format		=	3;
						$link->page_id		=	$page->page_id;
						$link->created_on	=	date('Y-m-d H:i:s', time());
						$link->save();
						
					}else{
						$tablelink = Engine_Api::_()->getDbTable('reels', 'page');
						$link = $tablelink->createRow();
						$link->reel_path	=	$lnk;
						$link->format		=	3;
						$link->page_id		=	$page->page_id;
						$link->created_on	=	date('Y-m-d H:i:s', time());
						$link->save();
					}
				}
			$i++;
			}  
	  }
		  
		  
	  // ADD OR REMOVE ADDRESS
	  if($values['remove_address_ids']){
		  $remove_address_ids	=	explode(',',$values['remove_address_ids']);
		  foreach($remove_address_ids as $addr){
			  if($addr){
				$addr	=	Engine_Api::_()->getItem('contact', $addr);
				$addr->is_deleted	=	1;
				$addr->save();
			  }
		  }
	  }
	  if(!empty($values['address'])){
			$i=0;
			foreach($values['address'] as $adr){
				if($adr){
					if($values["add_id"][$i]){
						$address	=	Engine_Api::_()->getItem('contact', $values["add_id"][$i]);
						$address->address		=	$adr;
						$address->phone_number	=	($values['phone_number'][$i])?$values['phone_number'][$i]:'';
						$address->zip_code		=	($values['zip_code'][$i])?$values['zip_code'][$i]:'';
						$address->country		=	($values['country'][$i])?$values['country'][$i]:'';
						$address->page_id		=	$page->page_id;
						$address->created_on	=	date('Y-m-d H:i:s', time());
						$address->created_by	=	$viewer->getIdentity();
						$address->modified_by	=	$viewer->getIdentity();
						//echo '<pre>'; print_r($values); echo '</pre>'; die;
						$address->save();
						
					}else{
						$tableaddress = Engine_Api::_()->getDbTable('contacts', 'page');
						$address = $tableaddress->createRow();
						$address->address		=	$adr;
						$address->phone_number	=	($values['phone_number'][$i])?$values['phone_number'][$i]:'';
						$address->zip_code		=	($values['zip_code'][$i])?$values['zip_code'][$i]:'';
						$address->country		=	($values['country'][$i])?$values['country'][$i]:'';
						$address->page_id		=	$page->page_id;
						$address->created_on	=	date('Y-m-d H:i:s', time());
						$address->created_by	=	$viewer->getIdentity();
						$address->modified_by	=	$viewer->getIdentity();
						$address->save();		
					}
				}
			$i++;
			}  
	  }
	  
	  // ADD OR REMOVE PROJECTS
	  if($values['remove_project_ids']){
		  $remove_project_ids	=	explode(',',$values['remove_project_ids']);
		  foreach($remove_project_ids as $proj){
			  if($proj){
				$proj	=	Engine_Api::_()->getItem('project', $proj);
				$proj->is_deleted	=	1;
				$proj->save();
			  }
		  }
	  }
	  /*if(!empty($values['project'])){
			$tableproject = Engine_Api::_()->getDbTable('projects', 'page');
			foreach($values['project'] as $proj){
				if($proj){
					$project = $tableproject->createRow();
					$project->project_name	=	$proj;
					$project->page_id		=	$page->page_id;
					$project->created_on	=	date('Y-m-d H:i:s', time());
					$project->save();	
				}
			}  
	  }*/
	  
	  
	  if(!empty($values['project'])){
			$i=0;
			foreach($values['project'] as $proj){
				if($proj){
					if($values["project_id"][$i]){
						$project	=	Engine_Api::_()->getItem('project', $values["project_id"][$i]);
						$project->project_name	=	$proj;
						$project->page_id		=	$page->page_id;
						$project->created_on	=	date('Y-m-d H:i:s', time());
						$project->save();
						
					}else{
						$tableaproject = Engine_Api::_()->getDbTable('projects', 'page');
						$project = $tableaproject->createRow();
						$project->project_name	=	$proj;
						$project->page_id		=	$page->page_id;
						$project->created_on	=	date('Y-m-d H:i:s', time());
						$project->save();		
					}
				}
			$i++;
			}  
	  }
	  // ADD OR REMOVE CLIENTS
	  if($values['remove_client_ids']){
		  $remove_client_ids	=	explode(',',$values['remove_client_ids']);
		  foreach($remove_client_ids as $clnt){
			  if($clnt){
				$clint	=	Engine_Api::_()->getItem('client', $clnt);
				$clint->is_deleted	=	1;
				$clint->save();
			  }
		  }
	  }
	  
	 
	  if(!empty($values['client'])){
			$i=0;
			foreach($values['client'] as $clint){
				if($clint){
					if($values["client_id"][$i]){
						$client	=	Engine_Api::_()->getItem('client', $values["client_id"][$i]);
						$client->client_name	=	$clint;
						$client->page_id		=	$page->page_id;
						$client->created_on	=	date('Y-m-d H:i:s', time());
						$client->save();
						
					}else{
						$tableclient = Engine_Api::_()->getDbTable('clients', 'page');
						$client = $tableclient->createRow();
						$client->client_name	=	$clint;
						$client->page_id		=	$page->page_id;
						$client->created_on	=	date('Y-m-d H:i:s', time());
						$client->save();		
					}
				}
			$i++;
			}  
	  }
	  
	  // ADD OR REMOVE AWARDS
	  if($values['remove_award_ids']){
		  $remove_award_ids	=	explode(',',$values['remove_award_ids']);
		  foreach($remove_award_ids as $awrd){
			  if($awrd){
				$awrd	=	Engine_Api::_()->getItem('award', $awrd);
				$awrd->is_deleted	=	1;
				$awrd->save();
			  }
		  }
	  }
	  
	  if(!empty($values['award'])){
			$i=0;
			foreach($values['award'] as $awrd){
				if($awrd){
					if($values["award_id"][$i]){
						$award	=	Engine_Api::_()->getItem('award', $values["award_id"][$i]);
						$award->award_name	=	$awrd;
						$award->page_id		=	$page->page_id;
						$award->created_on	=	date('Y-m-d H:i:s', time());
						$award->save();
						
					}else{
						$tableaaward = Engine_Api::_()->getDbTable('awards', 'page');
						$award = $tableaaward->createRow();
						$award->award_name	=	$awrd;
						$award->page_id		=	$page->page_id;
						$award->created_on	=	date('Y-m-d H:i:s', time());
						$award->save();		
					}
				}
			$i++;
			}  
	  }
	  
	   // SAVE Business Role
    $pagedesigs = Engine_Api::_()->getDbtable('dasignatedcategories', 'page')->pageDasignatedcategories($page->page_id);
    $desig_ids  = $values['desig_ids'];
    if($desig_ids){
    	$desig_ids=explode(',', $desig_ids);

      $pagedesoption = array();
      if($pagedesigs){
        foreach($pagedesigs as $des){
          $pagedesoption[$des['dasignatedcategories_id']]  = $des['dc_primary_id']; 
        }
      }
       if($pagedesoption){
              
        foreach($pagedesoption as $did){
         
          if($did && !in_array($did, $desig_ids)){
            $des_id  = Engine_Api::_()->getDbtable('dasignatedcategories', 'page')
        ->getpagedcbydcprimaryid($did, $page->page_id);
          if($des_id){              
              $page_des = Engine_Api::_()->getDbTable('dasignatedcategories', 'page');
              $condition = array(
                'dc_primary_id = ?' => $did,
                'page_id = ?' => $page->page_id
              );
              
              $page_des->delete($condition);
            }
          }
        } 
      }
      if($desig_ids){
      	//print_r($desig_ids);
       
        foreach($desig_ids as $dc){
        		//echo $dc;
          if(!in_array($dc, $pagedesoption) && $dc){
          		echo $dc; 
              $mdes = Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')
        ->getdscById($dc);
       /* print_r($mdes);
        die();*/
              $tabledes = Engine_Api::_()->getDbTable('dasignatedcategories', 'page');
              $dasignatedcategory = $tabledes->createRow();
              $dasignatedcategory->dasignatedcategory_title  = $mdes[0]['dasignatedsubcategory_name'];
              $dasignatedcategory->dc_primary_id  = $dc;
              $dasignatedcategory->created_on    = date('Y-m-d H:i:s', time());
              $dasignatedcategory->page_id     = $page->page_id;
              $dasignatedcategory->save(); 
          }
        } 
      }
     
    //die;  
    }
    	  	
	  
	  
      if ($table->checkUrl($values['url'],$page->getIdentity())) {
          //$form->populate($values);
          //$form->addError(Zend_Registry::get('Zend_Translate')->_('This URL is already taken by other page.'));
          return ;
      }

      $values['url'] = strtolower(trim($values['url']));
      $values['url'] = preg_replace('/[^a-z0-9-]/', '-', $values['url']);
      $values['url'] = preg_replace('/-+/', "-", $values['url']);
    	//$db = Engine_Api::_()->getDbTable('pages', 'page')->getAdapter();
    	//$db->beginTransaction();

      /*$address = array($values['country'], $values['state'], $values['city'], $values['street']);

      if ($address[0] == '' && $address[1] == '' && $address[2] == '' && $address[3] == '' && !$coordinates) {
        $page->deleteMarker();
      } elseif ($page->isAddressChanged($address)) {
        if(!$address[0]) {
          unset($address[0]);
        }
        if(!$address[1]) {
          unset($address[1]);
        }
        if(!$address[2]) {
          unset($address[2]);
        }
        if(!$address[3]) {
          unset($address[3]);
        }
        $page->addMarkerByAddress($address);
      }*/

      if ($coordinates) {
        $coordinate_arr = explode(';', $coordinates);
        $pageMarker = $page->getMarker(true);
        $pageMarker->latitude = $coordinate_arr[0];
        $pageMarker->longitude = $coordinate_arr[1];
        $pageMarker->save();
      }

      /*$raw_tags = preg_split('/[,]+/', $values['tags']);
      $tags = array();
      foreach ($raw_tags as $tag) {
        $tag = trim(strip_tags($tag));
        if ($tag == "") {
          continue ;
        }
        $tags[] = $tag;
      }
      $page->tags()->setTagMaps($this->view->viewer, $tags);*/

      $misTypes = array('http//', 'htp://', 'http://');
      $values['website'] = str_replace($misTypes, '', trim($values['website']));
      if (function_exists('mb_convert_encoding')) {
        $values['description'] = mb_convert_encoding($values['description'], 'UTF-8');
        $values['title'] = mb_convert_encoding(strip_tags( $values['title'] ), 'UTF-8');
      } else {
        $values['title'] = Engine_String::strip_tags($values['title']);
      }

      $page->setFromArray($values);
      $page->displayname = $page->title;
      //$page->keywords = $values['tags'];
      //$page->set_id = $values['category'];
      $page->name = $values['url'];
      $page->modified_date = date('Y-m-d H:i:s');

      //$form->addNotice(Zend_Registry::get('Zend_Translate')->_('Changes were successfully saved.'));

      $page->save();
	  Engine_Api::_()->getDbTable('pages', 'page')->savePageCategory($page->page_id,$categoryId);
	  
	  /* REDIRECT TO VIEW PAGE */
	  $page2_url = $this->view->baseUrl().'/page/'.$values['url'];
	 header("Location: $page2_url");
  }
	//echo '1212'; die;
	$this->view->pagevalues	=	$page->toArray();
	
	$this->view->key_clients	=	Engine_Api::_()->getDbTable('clients', 'page')->getPageClients($page->page_id);
	
	$this->view->key_projects	=	Engine_Api::_()->getDbTable('projects', 'page')->getPageProjects($page->page_id);
	
	$this->view->key_awards		=	Engine_Api::_()->getDbTable('awards', 'page')->getPageAwards($page->page_id);
	
	$this->view->key_imagereels	=	Engine_Api::_()->getDbTable('reels', 'page')->getPageImageReels($page->page_id);
	
	$this->view->key_pdfreels	=	Engine_Api::_()->getDbTable('reels', 'page')->getPagePdfReels($page->page_id);
	
	$this->view->key_linkreels	=	Engine_Api::_()->getDbTable('reels', 'page')->getPageLinkReels($page->page_id);
	
	$this->view->addresses	=	Engine_Api::_()->getDbTable('contacts', 'page')->getPageAddresses($page->page_id);

	$this->view->coverPhoto = Engine_Api::_()->timeline()->getTimelinePhoto($page->page_id, 'page', 'cover');
	//$this->_redirectCustom(array('route' => 'page_team', 'action' => 'edit', 'page_id' => $page->page_id));

    //$pageMarker = Engine_Api::_()->getApi('gmap', 'page')->getPageMarker($page);
//    $markers = array($pageMarker);
//    $bounds = Engine_Api::_()->getApi('gmap', 'page')->getMapBounds($markers);
//    $markers = Zend_Json_Encoder::encode($markers);
//    $bounds = Zend_Json_Encoder::encode($bounds);
//    $form->addMapElement($mapJs, $markers, $bounds);
  }
   public function paymoneyAction()
   {
	 $page = $this->view->page;	
	   	$this->_redirectCustom(array('route' => 'page_team', 'action' => 'autosubmit', 'page_id' => $page->page_id));   
	}
   
	public function autosubmitAction()
	{
	
	$page = $this->view->page;
	
	$this->view->pagevalues	=	$page->toArray();
	}
	
	public function cancelAction()
	{
 	$page = $this->view->page;
 	
	$this->view->pagevalues	=	$page->toArray();
	}
	
	public function failAction()
	{
 	$page = $this->view->page;
 	$this->view->pagevalues	=	$page->toArray();
 	
	}
	
	
	public function successAction()
	{
  	$page = $this->view->page;
	if($_POST['mihpayid']!="")
	{
  			$orderExist=Engine_Api::_()->getDbTable('pages', 'page')->checkOrder($page->page_id);
  			if(sizeof($orderExist)>0)
			{

			}
			else
			{
			 $viewer = Engine_Api::_()->user()->getViewer();
			$user_id = $viewer->getIdentity();
			$txnid = $_POST['txnid'];
			$mihpayid = $_POST['mihpayid'];
			$transaction_amount = $_POST['amount'];
			
			Engine_Api::_()->getDbTable('pages', 'page')->saveOrder($user_id,$page->page_id,$txnid,$mihpayid,$transaction_amount);
			Engine_Api::_()->getDbTable('pages', 'page')->savePageUnDraft($page->page_id);	
			}
 	}
 	$this->view->pagevalues	=	$page->toArray();
	}
	public function payAction()
	{
 	$page = $this->view->page;
	$this->view->pagevalues	=	$page->toArray();
 	}
	
	
	public function getFriendsAction()
	{
	  
				$url=$_SERVER['REQUEST_URI'];
				$query_params=end(explode("/",$url));
	   
 				$page_id=	$query_params;
				$currnet_user 			= Engine_Api::_()->user()->getViewer()->getIdentity();
				$this->view->page_id  =$page_id;
 				$dateTime				= strtotime(date("d-m-Y"));
			
				$user_friends 			= Engine_Api::_()->getDbtable('friendship', 'user')->getUserFriendsList($currnet_user);
				
				
				
 				//$shared_list = Engine_Api::_()->getDbtable('friendship', 'user')->getprojectSharedList($project_id);
				//$this->view->shared_list = $shared_list;
				
				 
				 
				$this->view->friends = $user_friends;
				
			
	}
	  
	public function getFriendsSharedAction()
	{
		   
	  
	  			$url=$_SERVER['REQUEST_URI'];
				$query_params=end(explode("/",$url));
				$currnet_user 			= Engine_Api::_()->user()->getViewer()->getIdentity();
 				$dateTime				= strtotime(date("d-m-Y"));
 				
				
				$this->view->page_id  = $query_params ;
				$shared_list = Engine_Api::_()->getDbtable('pages', 'page')->getpageSharedListWithOther($query_params,$currnet_user);
				
				$this->view->shared_list = $shared_list;
				 
				
				 
				 
				
			
	}
	public function pageDeleteAction()
	{
		 
	  
		$url=$_SERVER['REQUEST_URI'];
		$query_params=end(explode("/",$url));
		
		
		
		/**
		* @var $page Page_Model_Page
		*/
		$page_id = (int)$query_params;
		
		$this->view->page_id  =$page_id;
		
  		
   	
   
  	
  	 
	if ($this->getRequest()->isPost()) { 
	
		$page_id = $this->_getParam('page_id');
		
		$page = $this->view->page;
 		
  		$form = new Page_Form_Delete();
  	
  		$form->setAction($this->view->url(array('action' => 'delete', 'page_id' => $page_id), 'page_team'));
  		$description = sprintf(Zend_Registry::get('Zend_Translate')
  	  ->_('PAGE_DELETE_DESC'), $this->view->htmlLink($page->getHref(), $page->getTitle()));
  	  
  		$form->setDescription($description);
  	
   

		$db = Engine_Api::_()->getDbtable('pages', 'page')->getAdapter();
		$db->beginTransaction();
    
    	try {
			Engine_Api::_()->getDbTable('pages', 'page')->deletePageAssignedUser($page_id);	
      	if (null != ($subs = Engine_Api::_()->getItemTable('page_subscription')->getSubscription($page_id))) 			{
        	$subs->delete();
		
      		}
		$jobs	=	Engine_Api::_()->getDbTable('classifieds', 'classified')->getJobsByPage($page_id);
		if($jobs){
			foreach($jobs as $job){
				$classified = Engine_Api::_()->getItem('classified', $job);
				$classified->delete();	
			}	
		}
				
		$page->delete();
		$db->commit();
		echo "success::alldone";
    		} 
		catch (Exception $e) {
    	$db->rollBack();
    	throw $e;
		}
	}
	
	
    
   
				
			
	}	
  public function badgesAction()
  {

  }

  public function privacyAction()
  {
    if (!$this->_helper->requireUser()->isValid()) return;

    /**
     * @var $page Page_Model_Page
     */
    $page = $this->view->page;
    $this->view->form = $form = new Page_Form_Privacy(array('page' => $page));

    $auth = Engine_Api::_()->authorization()->context;

    $roles = array('team', 'likes', 'registered', 'everyone');
    foreach ($roles as $roleString) {
      $role = $roleString;

      if( $role === 'team' ) {
        $role = $page->getTeamList();
      } elseif( $role === 'likes' ) {
        $role = $page->getLikesList();
      }

      if ( 1 === $auth->isAllowed($page, $role, 'view') && !empty($form->auth_view) ) {
        $form->auth_view->setValue($roleString);
      }
    }

    $roles = array('team', 'likes', 'registered');
    foreach ($roles as $roleString) {
      $role = $roleString;

      if( $role === 'team' ) {
        $role = $page->getTeamList();
      } elseif( $role === 'likes' ) {
        $role = $page->getLikesList();
      }

      if ( 1 === $auth->isAllowed($page, $role, 'comment') && !empty($form->auth_comment) ) {
        $form->auth_comment->setValue($roleString);
      }
    }

    $pageApi = Engine_Api::_()->page();
    $page_features = $page->getAllowedFeatures();

    if( $pageApi->isModuleExists('pagealbum') &&  in_array('pagealbum', $page_features) ) {
      $roles = array('team', 'likes', 'registered');
      foreach ($roles as $roleString) {
        $role = $roleString;

        if( $role === 'team' ) {
          $role = $page->getTeamList();
        } elseif( $role === 'likes' ) {
          $role = $page->getLikesList();
        }

        if ( 1 === $auth->isAllowed($page, $role, 'album_posting') && !empty($form->auth_album_posting) ) {
          $form->auth_album_posting->setValue($roleString);
        }
      }
    }

    if( $pageApi->isModuleExists('pageblog') &&  in_array('pageblog', $page_features) ) {
      $roles = array('team', 'likes', 'registered');
      foreach ($roles as $roleString) {
        $role = $roleString;

        if( $role === 'team' ) {
          $role = $page->getTeamList();
        } elseif( $role === 'likes' ) {
          $role = $page->getLikesList();
        }

        if ( 1 === $auth->isAllowed($page, $role, 'blog_posting') && !empty($form->auth_blog_posting) ) {
          $form->auth_blog_posting->setValue($roleString);
        }
      }
    }

    if( $pageApi->isModuleExists('pagediscussion') &&  in_array('pagediscussion', $page_features) ) {
      $roles = array('team', 'likes', 'registered');
      foreach ($roles as $roleString) {
        $role = $roleString;

        if( $role === 'team' ) {
          $role = $page->getTeamList();
        } elseif( $role === 'likes' ) {
          $role = $page->getLikesList();
        }

        if ( 1 === $auth->isAllowed($page, $role, 'disc_posting') && !empty($form->auth_disc_posting) ) {
          $form->auth_disc_posting->setValue($roleString);
        }
      }
    }

    if( $pageApi->isModuleExists('pagedocument') &&  in_array('pagedocument', $page_features) ) {
      $roles = array('team', 'likes', 'registered');
      foreach ($roles as $roleString) {
        $role = $roleString;

        if( $role === 'team' ) {
          $role = $page->getTeamList();
        } elseif( $role === 'likes' ) {
          $role = $page->getLikesList();
        }

        if ( 1 === $auth->isAllowed($page, $role, 'doc_posting') && !empty($form->auth_doc_posting) ) {
          $form->auth_doc_posting->setValue($roleString);
        }
      }
    }

    if( $pageApi->isModuleExists('pageevent') &&  in_array('pageevent', $page_features) ) {
      $roles = array('team', 'likes', 'registered');
      foreach ($roles as $roleString) {
        $role = $roleString;

        if( $role === 'team' ) {
          $role = $page->getTeamList();
        } elseif( $role === 'likes' ) {
          $role = $page->getLikesList();
        }

        if ( 1 === $auth->isAllowed($page, $role, 'event_posting') && !empty($form->auth_event_posting) ) {
          $form->auth_event_posting->setValue($roleString);
        }
      }
    }

    if( $pageApi->isModuleExists('pagemusic') &&  in_array('pagemusic', $page_features) ) {
      $roles = array('team', 'likes', 'registered');
      foreach ($roles as $roleString) {
        $role = $roleString;

        if( $role === 'team' ) {
          $role = $page->getTeamList();
        } elseif( $role === 'likes' ) {
          $role = $page->getLikesList();
        }

        if ( 1 === $auth->isAllowed($page, $role, 'music_posting') && !empty($form->auth_music_posting) ) {
          $form->auth_music_posting->setValue($roleString);
        }
      }
    }

    if( $pageApi->isModuleExists('pagevideo') &&  in_array('pagevideo', $page_features) ) {
      $roles = array('team', 'likes', 'registered');
      foreach ($roles as $roleString) {
        $role = $roleString;

        if( $role === 'team' ) {
          $role = $page->getTeamList();
        } elseif( $role === 'likes' ) {
          $role = $page->getLikesList();
        }

        if ( 1 === $auth->isAllowed($page, $role, 'video_posting') && !empty($form->auth_video_posting) ) {
          $form->auth_video_posting->setValue($roleString);
        }
      }
    }

    if( $pageApi->isModuleExists('store') &&  in_array('store', $page_features) ) {
      $roles = array('owner', 'team');
      foreach ($roles as $roleString) {
        $role = $roleString;

        if( $role === 'team' ) {
          $role = $page->getTeamList();
        }

        if ( 1 === $auth->isAllowed($page, $role, 'store_posting') && !empty($form->auth_store_posting) ) {
          $form->auth_store_posting->setValue($roleString);
        }
      }
    }
    if($pageApi->isModuleExists('donation') && in_array('donation', $page_features)){
      $roles = array('owner','team');
      foreach($roles as $roleString){
        $role = $roleString;

        if($role == 'team'){
          $role = $page->getTeamList();
        }

        if(1 === $auth->isAllowed($page, $role, 'charity_posting') && !empty($form->auth_charity_posting)){
          $form->auth_charity_posting->setValue($roleString);
        }

        if(1 === $auth->isAllowed($page, $role, 'project_posting') && !empty($form->auth_project_posting)){
          $form->auth_project_posting->setValue($roleString);
        }
      }
    }

    if (!$this->getRequest()->isPost()) {
      return ;
    }

    if (!$form->isValid($this->getRequest()->getPost())) {
      return ;
    }

    $db = Engine_Api::_()->getDbTable('pages', 'page')->getAdapter();
    $db->beginTransaction();

    try
    {
      $values = $form->getValues();
      $page->setPrivacy($values);
      $page->search = $values['search'];

      $form->addNotice(Zend_Registry::get('Zend_Translate')->_('Settings were successfully saved.'));

      $page->save();
      $db->commit();
    } catch (Exception $e) {
      $db->rollBack();
      throw $e;
    }

  }

  public function editPhotoAction()
  {
    if( !$this->_helper->requireUser()->isValid() ) return 0;

    /**
     * @var $page Page_Model_Page
     */
    $page = Engine_Api::_()->core()->getSubject();
    $this->view->form = $form = new Page_Form_Photo();

    if( empty($page->photo_id) ) {
      $form->removeElement('remove');
    }

    if (!$this->getRequest()->isPost()) {
      return 0;
    }

    if (!$form->isValid($this->getRequest()->getPost())) {
      return 0;
    }

    if( $form->Filedata->getValue() !== null ) {
      $db = Engine_Api::_()->getDbTable('pages', 'page')->getAdapter();
      $db->beginTransaction();

      try {
        $fileElement = $form->Filedata;

        $this->setProfilePhoto($fileElement, $page->getIdentity());

        $form->addNotice(Zend_Registry::get('Zend_Translate')->_('Image was successfully proccessed.'));

        $page->save();
        $db->commit();
      } catch( Engine_Image_Adapter_Exception $e ) {
        $db->rollBack();
        $form->addError(Zend_Registry::get('Zend_Translate')->_('The uploaded file is not supported or is corrupt.'));
      } catch( Exception $e ) {
        $db->rollBack();
        throw $e;
      }
    } else if( $form->getValue('coordinates') !== '' ) {
        $storage = Engine_Api::_()->storage();

        $iProfile = $storage->get($page->photo_id, 'thumb.profile');
        $iSquare = $storage->get($page->photo_id, 'thumb.icon');

        // Read into tmp file
        $pName = $iProfile->getStorageService()->temporary($iProfile);
        $iName = dirname($pName) . '/nis_' . basename($pName);

        list($x, $y, $w, $h) = explode(':', $form->getValue('coordinates'));

        $image = Engine_Image::factory();
        $image->open($pName)
          ->resample($x+.1, $y+.1, $w-.1, $h-.1, 48, 48)
          ->write($iName)
          ->destroy();

        $iSquare->store($iName);

        // Remove temp files
        @unlink($iName);
    }

    return $this->_helper->redirector->gotoRoute(array('action' => 'edit-photo', 'page_id' => $page->getIdentity()), 'page_team', true);
  }
  
	public function postNoteAction()
	{
		$note = $this->_getParam('note');
		$page = $this->view->page;

		$page->note = trim(strip_tags($note));
		$page->save();
		$this->view->note = nl2br($page->note);
		$this->view->result = 1;
		$this->view->message = "Ok!";
	}

  public function addFavoritesAction()
  {
    $page_id = (int)$this->_getParam('page_id');
    $favorites = $this->_getParam('favorites');
    $page = Engine_Api::_()->getItem('page', $page_id);

    if (!$page_id) {
      $this->view->message = $this->view->translate('Wrong parameter set.');
      $this->view->type = 'error';
			return ;
    }
    
		if (!$page) {
      $this->view->message = $this->view->translate('Pages does not exists.');
      $this->view->type = 'error';
			return ;
		}
    		
    $table = Engine_Api::_()->getDbTable('favorites', 'page');
    $db = $table->getAdapter();
    $db->beginTransaction();
    
    try {
      foreach ($favorites as $page_fav_id) {
        $table->insert(array(
          'page_id' => $page_id,
          'page_fav_id' => $page_fav_id
        ));
      }
      $db->commit();
      $this->view->message = $this->view->translate('Pages were successfully added.');
      $this->view->type = 'text';
    } catch (Exception $e) {
      $this->view->message = $this->view->translate($e->getMessage());
      $this->view->type = 'error';
      $db->rollBack();
      throw $e;
    }
    
  }

  public function deleteFavoritesAction()
  {
    $page_id = (int)$this->_getParam('page_id', 0);
    $favorites = $this->_getParam('favorites');
    $page = Engine_Api::_()->getItem('page', $page_id);

    if (!$page_id) {
      $this->view->message = $this->view->translate('Wrong parameter set.');
      $this->view->type = 'error';
			return ;
    }

		if (!$page) {
      $this->view->message = $this->view->translate('Pages does not exists.');
      $this->view->type = 'error';
			return ;
		}

		if (!$page->isAdmin()) {
      $this->view->message = $this->view->translate('You have no permission to do this.');
      $this->view->type = 'error';
  		return ;
  	}

    $table = Engine_Api::_()->getDbTable('favorites', 'page');
    $db = $table->getAdapter();
    $db->beginTransaction();

    try {
      foreach ($favorites as $page_fav_id) {
        $table->delete(array(
          'page_id = ?' => $page_fav_id,
          'page_fav_id = ?' => $page_id
        ));
      }
      $db->commit();
      $this->view->message = $this->view->translate('Pages were successfully deleted.');
      $this->view->type = 'text';
    } catch (Exception $e) {
      $this->view->message = $this->view->translate($e->getMessage());
      $this->view->type = 'error';
      $db->rollBack();
      throw $e;
    }

  }

  public function manageAdminsAction()
  {
    if( !$this->_helper->requireUser()->isValid() ) return;

    /**
     * @var $page Page_Model_Page
     */
    $page = $this->view->page;
    $this->view->is_super_admin = (bool) $page->isOwner($this->view->viewer);
    $this->view->admins = $page->getAdmins();
    $this->view->employers = $page->getEmployers();
  }

  public function appsAction()
  {
    $page = $this->view->page;
    $this->view->sub_menu = $this->_getParam('sub-menu', 'contact');

    if ($this->view->isAllowStore && $page->getStorePrivacy()) {
      $path = Engine_Api::_()->page()->getModuleDirectory('store');
      $this->view->addScriptPath($path . '/views/scripts');
      $this->view->isGatewayEnabled = (boolean)Engine_Api::_()->getDbTable('apis', 'store')->getEnabledGateways($page->getIdentity());
      $this->view->page_id = $page->getIdentity();
    }

    if($this->view->sub_menu == 'donation'){
      return $this->_helper->redirector->gotoRoute(array('controller' => 'page','action' => 'index', 'page_id' => $page->getIdentity()),'donation_extended',true);
    }
    if ($this->view->isAllowInvite) {
      $this->view->fb_settings = Engine_Api::_()->inviter()->getFacebookSettings($this->view, $page);
      $providers = Engine_Api::_()->inviter()->getProviders2(false, 15);
      $this->view->providers = Engine_Api::_()->inviter()->getIntegratedProviders();
      $this->view->count = count($providers);

      $session = new Zend_Session_Namespace('contacts');
      $session->unsetAll();

      $form = new Inviter_Form_Widget_PageImport();
      $form->page_id->setValue($page->getIdentity());

      if( !$page ) {
        $this->_redirect('browse-pages');
      }

      $this->view->form = $form;

      if ($this->view->viewer->getIdentity()) {
        $form_write = new Inviter_Form_Widget_PageWrite();
        $form_write->page_id->setValue($page->getIdentity());
        $this->view->form_write = $form_write;
      }
    }
  }
  
  
  

  public function getStartedAction()
  {

  }

  public function styleAction()
  {
    $page = $this->view->page;
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();

    if (!$this->view->isAllowStyle) {
      $this->_redirectCustom(array('route' => 'page_team', 'action'=>'edit', 'page_id'=>$page->getIdentity()));
    }

    // Get form
    $this->view->form = $form = new User_Form_Edit_Style();
    $form->getElement('style')->setDescription('Add your own CSS code above to give your page profile a more personalized look.');

    // Get current row
    $table = Engine_Api::_()->getDbtable('styles', 'core');
    $select = $table->select()
      ->where('type = ?', $page->getType())
      ->where('id = ?', $page->getIdentity())
      ->limit();

    $row = $table->fetchRow($select);

    // Not posting, populate
    if (!$this->getRequest()->isPost()) {
      $form->populate(array(
        'style' => (null === $row ? '' : $row->style)
      ));
      return;
    }

    // Whoops, form was not valid
    if (!$form->isValid($this->getRequest()->getPost())) {
      return;
    }

    // Cool! Process
    $style = $form->getValue('style');

    // Process
    $style = strip_tags($style);

    $forbiddenStuff = array(
      '-moz-binding',
      'expression',
      'javascript:',
      'behaviour:',
      'vbscript:',
      'mocha:',
      'livescript:',
    );

    $style = str_replace($forbiddenStuff, '', $style);

    // Save
    if (null == $row) {
      $row = $table->createRow();
      $row->type = $page->getType();
      $row->id = $page->getIdentity();
    }

    $row->style = $style;
    $row->save();

    $form->addNotice(Zend_Registry::get('Zend_Translate')->_('Your changes have been saved.'));
  }
  
  public function uploadPhotoAction()
  {
		
    if( !$this->_helper->requireUser()->isValid() ) return 0;

    /**
     * @var $page Page_Model_Page
     */
	 $this->view->photo_id = '';
    $page = Engine_Api::_()->core()->getSubject();
    $this->view->form = $form = new Page_Form_Photo();

    
    if (!$this->getRequest()->isPost()) {
      return 0;
    }

    if (!$form->isValid($this->getRequest()->getPost())) {
      return 0;
    }

    if( $form->Filedata->getValue() !== null ) {
      $db = Engine_Api::_()->getDbTable('pages', 'page')->getAdapter();
      $db->beginTransaction();

      try {
        $fileElement = $form->Filedata;

        $photo_id = $this->setProfilePhoto($fileElement, $page->getIdentity());

        $form->addNotice(Zend_Registry::get('Zend_Translate')->_('Image was successfully proccessed.'));
		$page->photo_id = $photo_id;
        $page->save();
		$this->view->photo_id = $page->photo_id;
        $db->commit();
      } catch( Engine_Image_Adapter_Exception $e ) {
        $db->rollBack();
        $form->addError(Zend_Registry::get('Zend_Translate')->_('The uploaded file is not supported or is corrupt.'));
      } catch( Exception $e ) {
        $db->rollBack();
        throw $e;
      }
    } else if( $form->getValue('coordinates') !== '' ) {
        $storage = Engine_Api::_()->storage();

        $iProfile = $storage->get($page->photo_id, 'thumb.profile');
        $iSquare = $storage->get($page->photo_id, 'thumb.icon');

        // Read into tmp file
        $pName = $iProfile->getStorageService()->temporary($iProfile);
        $iName = dirname($pName) . '/nis_' . basename($pName);

        list($x, $y, $w, $h) = explode(':', $form->getValue('coordinates'));

        $image = Engine_Image::factory();
        $image->open($pName)
          ->resample($x+.1, $y+.1, $w-.1, $h-.1, 48, 48)
          ->write($iName)
          ->destroy();

        $iSquare->store($iName);

        // Remove temp files
        @unlink($iName);
    }

    //return $this->_helper->redirector->gotoRoute(array('action' => 'edit-photo', 'page_id' => $page->getIdentity()), 'page_team', true);
    
  }
  
  public function getpagepicAction(){
	$this->_helper->content
        ->setNoRender()
        ->setEnabled()
        ; 
	$page_id =	$this->_getParam('page_id');
	$page	 = $this->view->page	=	Engine_Api::_()->getItem('page', $page_id);
	echo $companypic	=	Engine_Api::_()->getDbtable('users', 'user')->getUserPageProfilePicPath($page->parent_id, $page->photo_id, $page->getIdentity()); 
	die;
  }
}