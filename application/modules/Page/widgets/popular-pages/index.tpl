<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 2011-11-08 17:53 taalay $
 * @author     Taalay
 */
?>
<div class="col-md-12 col-sm-12 col-xs-12 index-right-job-posted">
<div class="col-md-7 col-sm-5 col-xs-8 index-right-text-lfet"><span style="font-size: 15px; color: rgb(76, 76, 76); font-weight: 600;">People also viewed</span></div>
<div class="col-md-5 col-sm-7 col-xs-4 classified_header text-right"><a class="fmp" href="members"><i class="fa fa-search" aria-hidden="true"></i> Find more</a></div>
</div>




<div class="container col-md-12 col-sm-12 col-xs-12">
		  <?php foreach( $this->pages as $page ): ?>
		  <div class="col-md-4 col-sm-3  col-xs-4 nopadding">
			<div class="col-md-12 peopl_conect" style="padding:5px 0px;">
				
				  <?php echo $this->htmlLink($page->getHref(), $this->itemPhoto($page, 'thumb.icon', ''), array('class' => 'newestmembers_thumb')) ?></div>	
				  
				  <div class="col-md-12">
					  <div class='newestmembers_info feed_item_photo' style="margin-right: 0px;">
						<div class='newestmembers_name feed_item_body text-center' >
						  <?php echo $this->htmlLink($page->getHref(), $this->string()->chunk($this->string()->truncate($page->getTitle(), 11), 10)) ?>
							
						</div>
				</div>
				  </div>
                  
               </div>
				
				<?php endforeach; ?>

</div>

