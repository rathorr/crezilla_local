<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Controller.php 2011-11-08 16:05 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class Page_Widget_PopularPagesController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	   
	 
    $cat_id = $this->_getParam('filter');
    $table = Engine_Api::_()->getDbTable('pages', 'page');
    $pageSlug= end(explode("/",$_SERVER['REQUEST_URI'])) ;
	$pageInfo = $table->getPageInfoByUrl($pageSlug);
    $settings = Engine_Api::_()->getApi('settings', 'core');
    $ipp = $settings->getSetting('page.popular_count', 6);
	
	$file = APPLICATION_PATH . '/application/settings/database.php';
		$options = include $file;
		$db = Zend_Db::factory($options['adapter'], $options['params']);
		$select = new Zend_Db_Select($db);
		$db->getConnection();
		$query2 = "select count(pp.page_id) as tot_sim_pages from engine4_page_pages pp 
 		 		   where pp.name <> 'header' AND pp.name <> 'footer'
				   and pp.approved=1 and pp.category_id =".$pageInfo[0]['category_id']."
				   and pp.page_id !=".$pageInfo[0]['page_id']." ";
				    
		   $stmt = $db->query($query2);
           $result2 = $stmt->fetchAll();
		 
		   if($result2[0]['tot_sim_pages']>0)
		   {
			$params = array(
			'cat_id' => $cat_id,
			'category_id' => $pageInfo[0]['category_id'],
			'page_id' => $pageInfo[0]['page_id'],
			'approved' => 1,
			'sort' => 'popular',
			'ipp' => $ipp,
			'p' => 1);
		   }
		   else
		   {
			 $params = array(
			'page_id' => $pageInfo[0]['page_id'],
			'approved' => 1,
			'sort' => 'popular',
			'ipp' => $ipp,
			'p' => 1);
			   
		   }
	 
	 
    $this->view->pages = $pages = $table->getPopularPaginator($params);
    //echo $pages->getTotalItemCount(); die;
    if (!$pages->getTotalItemCount()){
      return $this->setNoRender();
    }
  }
}