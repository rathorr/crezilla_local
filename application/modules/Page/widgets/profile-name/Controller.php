<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Page_Widget_ProfileNameController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    // Don't render this if not authorized
    $viewer = Engine_Api::_()->user()->getViewer();
    if( !Engine_Api::_()->core()->hasSubject() ) {
      return $this->setNoRender();
    }
	// Get subject and check auth
    $this->view->subject  = $subject = Engine_Api::_()->core()->getSubject('page');

    $this->view->is_owner  = $user= ($subject->getOwner()->isSelf($viewer))?true:false;

	 $view = $this->view;
   $clientcode = Engine_Api::_()->getDbtable('clientids', 'page')->getClientcode($subject->page_id);
    $this->view->clientcode = $clientcode;
  }
}