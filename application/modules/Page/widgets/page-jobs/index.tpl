<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 7244 2011-03-12 15:42:53 taalay $
 * @author     Taalay
 */
?>
 <link rel="stylesheet" href="<?php echo $this->baseUrl().'/public/html5lightbox/jquery.js';?>"type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/html5lightbox/html5lightbox.js'?>"></script>

<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>


<div id="">
<div class="content_feed_time_line">
    <div class="he-col-md-4 tl_left layout_left_timeline"  style="float:right !important;">

        <?php $cnt = 0;
        $array_symbols = array(
            '.',
            '-'
        );
        foreach ($this->tabs as $tab): ?>


                <div class="generic_layout_container layout_<?php echo str_ireplace($array_symbols, '_',$tab['name']); ?>">
                    <h3> <?php echo $tab['title']; ?></h3>
                    <?php echo $tab['content']; ?>
                </div>
        <?php endforeach; ?>

    </div>
    <div class="he-col-md-8 generic_layout_container layout_middle_timeline" id="timelinefeedtab" >
   <div class="profile_fields">
  <?php 
  if(!empty($this->key_jobs)){?>
  <table class="manage_jp table">
  <thead class="mk_tabl">
      <tr>
      	<th scope="col">No.</th>
        <th scope="col">Job Title</th>
        <th scope="col">Project</th>
        <th scope="col">Location</th>
        <!-- <th scope="col">Additional Information</th>
        <th scope="col">Created date</th> -->
        <th scope="col">Status</th>
      </tr>
	   </thead>
 <?php  $i=1;foreach( $this->key_jobs as $job ): 
 	$classified = Engine_Api::_()->getItem('classified', $job['classified_id']);
 ?>
 <tbody>
      <tr>
        <td><?php echo $i;?></td>
      	<td><a href="<?php echo $classified->getHref();?>"><?php echo $job['title'];?></a></td>
        <td><?php echo $job['project_name'];?></td>
        <td><?php echo Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedLocationById($job['classified_id']);?></td>
       <!--  <td><?php if($job['script']){?>
        <span>
        <?php echo $job['script_title'];?>
            <a href="<?php echo $this->baseUrl().'/public/classified/scripts/'.$job['script'];?>" target="_blank">
            <img src="<?php echo $this->baseUrl().'/public/custom/images/view_pdf.png'; ?>" title="View" alt="View"/>
            </a>
        </span>
        <?php }else{echo '-';}?></td>
        <td><?php echo date('F d, Y H:i a', strtotime($job['creation_date']));?></td> -->
        <td><?php echo ($job['closed'] )?'Closed':'Open'?></td>
        
      </tr>
      <?php $i++; endforeach;
      }else{
        echo '<tr><td colspan=6>No Jobs Posted</td></tr>';
      } ?>
	  </tbody>
    </table>

</div>
    </div>
</div>
</div>


<script type="text/javascript">
	$$(".layout_page_profile_fields .tip").destroy();
</script>

<div style="clear:both"></div>





