<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>



<?php $companypic	=	Engine_Api::_()->getDbtable('users', 'user')->getUserPageProfilePicPath($this->subject->parent_id, $this->subject->photo_id, $this->subject->getIdentity());
        $companypic	=	($companypic=='')?'application/modules/Page/externals/images/nophoto_page_thumb_profile.png':$companypic;
 ?>
<div id='profile_photo' style="background-image:url(<?php echo $companypic; ?>);" class="wall_background set_profile">
<?php if($this->viewer()->getIdentity() == $this->subject()->parent_id){?>
<span>
 	<a href="<?php echo $this->url(array('action' => 'upload-photo', 'page_id' => $this->subject()->getIdentity()), 'page_team', true)?>" class="smoothbox custom_oupload_camera_icon_small">
	<span>Update Profile Picture</span></a>
 </span>
 <?php } ?>
</div> 


<style type="text/css">
.thumb_profile.item_photo_user.thumb_profile, .thumb_profile.item_photo_page.thumb_profile{position:absolute;}
</style>

  <?php //echo $this->htmlLink( $this->subject()->getHref(), $this->itemPhoto($this->subject(), 'thumb.profile') ); ?>
  <?php //echo $this->itemPhoto($this->subject(), 'thumb.wallmain', "", array('id' => 'lassoImg'),'wall') ?>
 
	<!--<div class="icons" style="display:none;">
	<?php if ( $this->subject()->isStore()) : ?>
		<img class="page-icon" src="application/modules/Page/externals/images/store_mini.png" title="<?php echo $this->translate('STORE_Store'); ?>">
	<?php endif; ?>
	<?php echo $this->htmlImage("application/modules/Page/externals/images/featured".$this->subject()->featured.".png",
		$this->translate('PAGE_page_featured'.$this->subject()->featured),
		array(
		 'class'=>'page-icon',
			'title' => $this->translate('PAGE_page_featured'.$this->subject()->featured)
		)); ?>

	<?php echo $this->htmlImage("application/modules/Page/externals/images/sponsored".$this->subject()->sponsored.".png",
		$this->translate('PAGE_page_sponsored'.$this->subject()->sponsored),
		array(
		 'class'=>'page-icon',
			'title' => $this->translate('PAGE_page_sponsored'.$this->subject()->sponsored)
		)); ?>
	</div>
</div>-->

<script type="text/javascript">
function changeprofilepic(){
	jQuery.post('<?php echo $this->baseUrl()."/page-team/getpagepic/".$this->subject()->getIdentity();?>', function(resp){
			var pic	=	jQuery.trim(resp);
			if(pic == ''){
				pic = '<?php echo $this->baseUrl();?>/application/modules/Page/externals/images/nophoto_page_thumb_profile.png';	
			}
			jQuery('#profile_photo').css('background-image','url('+pic+')');
		});
}
</script>