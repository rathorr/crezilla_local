<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

?>

<?php
$this->headScript()->appendFile($this->baseUrl() . '/application/modules/Timeline/externals/scripts/profile_cover.js');
?>
<script type="text/javascript">
document.tl_cover = new TimelineCover();
        document.tl_cover.setOptions({
            element_id: 'profile_wallphoto',
            save_button: 'tl-cover-save-button',
            edit_buttons: 'tl-cover-edit-group',
            loader_id: 'tl-cover-loader',
            is_allowed: true,
            cover_url: '<?php echo $this->url(array('action' => 'get', 'item_type' => 'page', 'item_id' => $this->user->getIdentity()), 'timeline_photo', true); ?>',
            position_url: '<?php echo $this->url(array('action' => 'position', 'type'=>'cover', 'item_type' => 'page', 'item_id' => $this->user->getIdentity()), 'timeline_photo', true); ?>',
            imgSrc: '<?php echo $this->coverPhoto['photoSrc']; ?>'
        });
        try {
            document.tl_cover.position = JSON.parse('<?php echo $this->coverPhoto['position']; ?>');
        } catch(e) {
            console.log(e);
        }

        en4.core.runonce.add(function () {
            document.tl_cover.init();
            document.tl_cover.options.cover_width = document.tl_cover.get().getParent().getWidth();
        });
</script>

<div id='profile_wallphoto' style="background-image:url(<?php echo $this->coverPhoto['photoSrc']; ?>);" class="wall_background cover-container-img">
 <?php if($this->viewer()->getIdentity() == $this->user->parent_id){?>
 <span >
 	<a href="javascript:void(0);" class="custom_oupload_camera_icon"> 
    	<span>Update Cover Picture</span>
     </a>
	 <ul class="cover_option custom_cover_bx" style="display:none;">
    	<li>
        <a href="<?php echo $this->baseUrl().'/timeline/photo/upload/item_type/page/type/cover/item_id/'.$this->user->getIdentity();?>" class="smoothbox upload_photo">Upload Photo</a></li>
        <li><a href="<?php echo $this->baseUrl().'/timeline/photo/remove/type/cover/item_type/page/item_id/'.$this->user->getIdentity();?>" class="smoothbox remove_photo">Remove Photo</a></li>
    </ul>
 </span>
 <?php } ?>
</div>
<span class="wall_tp"></span>
<div class="user_views_followers">
<span class="views">
	<span class="view_count"><?php echo $this->locale()->toNumber($this->user->view_count);?>
    </span>
	<span class="view_title">views</span>
</span>

<span class="followers">
	<span class="view_count"><?php echo $this->followTotal;?>
    </span>
<span class="view_title">followers</span>
	
</span>

</div>

<div id="profile_options">
<a class="tft show_profile_options wp_init" href="javascript:void(0);">...
</a>
  
<ul style="display: none;">
      <?php if ($this->follow==1): ?>
      <li>
      	<a class="smoothbox show_friends proj_share unfollow_page" href="<?php echo $this->baseUrl().'/classified/index/get-unfollow-page/action_id/'.$this->user->getIdentity();?>">Unfollow </a>
      </li>
     <?php else: ?>
       <li>
           	<a class="smoothbox show_friends proj_share follow" href="<?php echo $this->baseUrl().'/classified/index/get-follow-page/action_id/'.$this->user->getIdentity();?>">Follow </a>
       </li>
      <?php endif;?>
      <?php if ($this->likeEnabled): ?>
        <?php if ($this->liked): ?>
        	<li>
            <a class="he-btn he-btn-unlike unlike"
               data-like="0"
               onclick="document.tl_core.likeSubject(this, '<?php echo $this->user->getIdentity(); ?>', '<?php echo $this->user->getType(); ?>');">
                <span id="tl-like-btn-text"><?php echo $this->translate('like_Unlike'); ?></span>
            </a>
            </li>
        <?php else: ?>
        <li>
            <a class="he-btn he-btn-like like"
               data-like="1"
               onclick="document.tl_core.likeSubject(this, '<?php echo $this->user->getIdentity(); ?>', '<?php echo $this->user->getType(); ?>');">
                <span id="tl-like-btn-text"><?php echo $this->translate('like_Like'); ?></span>
            </a>
            </li>
        <?php endif; ?>
    <?php endif; ?>  
      <li>
      <a class="he-btn smoothbox menu_page_profile page_profile_share share" href="<?php echo $this->baseUrl().'/activity/index/share/type/page/id/'.$this->user->getIdentity().'/format/smoothbox';?>">Share Page</a>
      </li>
      
  </ul>
  
  </div>
  
<script type="text/javascript">
  jQuery(document).ready(function(){
	var t	=	jQuery('#main_tabs').find('.active').html();
	
	if(jQuery.trim(t) == ''){
		setTimeout(function(){jQuery('#main_tabs').find('.tab_layout_page_profile_fields').find('a').click();}, 1000);
		
	}
});
jQuery(document).on('click','.show_profile_options, .tft',function(){
	jQuery('#profile_options ul').slideToggle();	
});

function changepic(){
	jQuery.post('<?php echo $this->baseUrl()."/members/profile/getuserpic/item_type/cover/id/".$this->user->getIdentity();?>', function(resp){
			var pic	=	jQuery.trim(resp);
			if(pic == ''){
				pic = '<?php echo $this->baseUrl();?>/application/modules/User/externals/images/nophoto_user_thumb_wallmain.png';	
			}
			jQuery('#profile_wallphoto').css('background-image','url('+pic+')');
			jQuery('.cover_option').css("display", "none");
			jQuery('.custom_oupload_camera_icon').css("background-color", "");
		});
}

jQuery(document).on('click', '#profile_wallphoto', function(){
	if(jQuery('.cover_option').css("display") =='none'){
		jQuery('.cover_option').css("display", "block");
		jQuery('.custom_oupload_camera_icon span').addClass("show_cover_icon");
		jQuery('.custom_oupload_camera_icon').css("background-color", "rgba(0,0,0,0.4)");	
	}else{
		jQuery('.cover_option').css("display", "none");
		jQuery('.custom_oupload_camera_icon span').removeClass("show_cover_icon");
		jQuery('.custom_oupload_camera_icon').css("background-color", "");
	}
});

</script>




