<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Page_Widget_ProfileWallphotoController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	 
    // Don't render this if not authorized
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    if( !Engine_Api::_()->core()->hasSubject() ) {
      return $this->setNoRender();
    }
	
    // Get subject and check auth
    $subject = Engine_Api::_()->core()->getSubject('page');

	$is_owner = false;
	if($subject->parent_id == $viewer->getIdentity()){
			$is_owner = true;  
	}

	$this->view->is_owner = $is_owner;
    $this->view->user = $subject;
	$this->view->coverPhoto = Engine_Api::_()->timeline()->getTimelinePhoto($subject->getIdentity(), 'page', 'cover');
	
	if (Engine_Api::_()->getDbTable('modules', 'core')->isModuleEnabled('like')) {
            $this->view->likeEnabled = $likeEnabled = (bool)(Engine_Api::_()->like()->isAllowed($subject));
            $this->view->liked = Engine_Api::_()->like()->isLike($subject);
    }
	
	$friendsFollowing = Engine_Api::_()->getDbtable('friendship', 'user')->pageFollowingTotal($subject->getIdentity());
	$friends = Engine_Api::_()->getDbtable('friendship', 'user')->pageFollowed(Engine_Api::_()->user()->getViewer()->getIdentity(),$subject->getIdentity());
 		$this->view->followTotal =$friendsFollowing[0]['totalfollowing'];
		$this->view->follow=0;
 		 if(count($friends)>0)
		 {
			 $this->view->follow = 1;
			 
		 }
  }
  
}