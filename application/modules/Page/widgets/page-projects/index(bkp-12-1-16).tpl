<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 7244 2011-03-12 15:42:53 taalay $
 * @author     Taalay
 */
?>
 <link rel="stylesheet" href="<?php echo $this->baseUrl().'/public/html5lightbox/jquery.js';?>"type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/html5lightbox/html5lightbox.js'?>"></script>

<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>


<div id="">
<div class="content_feed_time_line">
    <div class="he-col-md-4 tl_left layout_left_timeline"  style="float:right !important;">

        <?php $cnt = 0;
        $array_symbols = array(
            '.',
            '-'
        );
        foreach ($this->tabs as $tab): ?>


                <div class="generic_layout_container layout_<?php echo str_ireplace($array_symbols, '_',$tab['name']); ?>">
                    <h3> <?php echo $tab['title']; ?></h3>
                    <?php echo $tab['content']; ?>
                </div>
        <?php endforeach; ?>

    </div>
    <div class="he-col-md-8 generic_layout_container layout_middle_timeline" id="timelinefeedtab" >
   <div class="profile_fields">
 <ul>
  <?php 
  if(!empty($this->key_projs)){?>
 <?php  foreach( $this->key_projs as $proj ): ?>
      <li>
      	<h3><?php echo $proj['project_name'];?></h3>
        <p><?php echo $proj['project_desc'];?></p>
      
      </li>
      <?php endforeach;
      }else{
        echo '<li>No Projects</li>';
      } ?>
    </ul>

</div>
    </div>
</div>
</div>


<script type="text/javascript">
	$$(".layout_page_profile_fields .tip").destroy();
</script>







