<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Controller.php 7644 2011-03-12 15:38:39 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */
class Page_Widget_PageReelsController extends Engine_Content_Widget_Abstract
{
  protected $_childCount;
  
  public function indexAction()
  {
	$this->view->subject = $subject = Engine_Api::_()->core()->getSubject('page');
   $this->view->key_imagereels	=	Engine_Api::_()->getDbTable('reels', 'page')->getPageImageReels($subject->getIdentity());
	
	$this->view->key_pdfreels	=	Engine_Api::_()->getDbTable('reels', 'page')->getPagePdfReels($subject->getIdentity());
	
	$this->view->key_linkreels	=	Engine_Api::_()->getDbTable('reels', 'page')->getPageLinkReels($subject->getIdentity());
	
   $element = $this->getElement();
        $action_id = (int)Zend_Controller_Front::getInstance()->getRequest()->getParam('action_id');
        $activeTab = $action_id ? 'activity.feed' : $this->_getParam('tab');


        $subjectType = $subject->getType();
        $deprecated = array(
            'like.status',
            'page.profile-options',
            'page.profile-photo'
        );

        $tabs = array();
        $params = array();
        foreach ($element->getElements() as $child) {
            if (null === $activeTab) {
                $activeTab = $child->getIdentity();
            }
            $child->clearDecorators();

            $id = $child->getIdentity();
            $title = $child->getTitle();
            $name = $child->getName();

            if($subjectType == 'page' && in_array($name, $deprecated)) {
                continue;
            }

            $childCount = null;
            if (method_exists($child, 'getWidget') && method_exists($child->getWidget(), 'getChildCount')) {
                $childCount = $child->getWidget()->getChildCount();
            }
            if (!$title) $title = $name;

            $content = $child->render();

            if ($child->getNoRender()) {
                continue;
            }

            $tabs[] = array(
                'id' => $id,
                'name' => $name,
                'title' => $title,
                'childCount' => $childCount,
                'content' => $content . PHP_EOL
            );

            $params[$name]['title'] = ( is_array($child->params) && (string)(array_key_exists('title', $child->params))) ? $child->params['title'] : "TIMELINE_Application";

            if (method_exists($child, 'getWidget') && method_exists($child->getWidget(), 'getChildCount')) {
                $params[$name]['count'] = $child->getWidget()->getChildCount();
            }
        }

        $this->view->tabs = $tabs;
   
  }

}