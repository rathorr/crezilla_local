<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 7244 2011-03-12 15:42:53 taalay $
 * @author     Taalay
 */
?>
<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>

<style type="text/css">
body#global_page_page-index-timeline-view ul#profile_albums li {
    background: rgba(244, 244, 244, 0.6) none repeat scroll 0 0;
    border-radius: 5px !important;
    box-shadow: 0 1px 10px #999;
    float: none;
    height: auto;
    margin: 0 auto;
    overflow: hidden;
    padding: 0;
    text-align: center;
    width: 100%;
}
</style>
<div id="">
<div class="content_feed_time_line">
    <div class="he-col-md-4 tl_left layout_left_timeline"  style="float:right !important;">

        <?php $cnt = 0;
        $array_symbols = array(
            '.',
            '-'
        );
        foreach ($this->tabs as $tab): ?>


                <div class="generic_layout_container layout_<?php echo str_ireplace($array_symbols, '_',$tab['name']); ?>">
                    <h3> <?php echo $tab['title']; ?></h3>
                    <?php echo $tab['content']; ?>
                </div>
        <?php endforeach; ?>

    </div>
    <div class="he-col-md-8 generic_layout_container layout_middle_timeline" id="timelinefeedtab" >
   <div class=" ">
		<div id="vtabs3" style="min-height:158px;">
	<div>
		<ul>
        	<li><a href="#vtabs-content-p">Photos (<?php echo count($this->key_imagereels);?>)</a></li>
			<li><a href="#vtabs-content-q">Pdf (<?php echo count($this->key_pdfreels);?>)</a></li>
            <li><a href="#vtabs-content-r">Videos/Audios (<?php echo count($this->key_linkreels);?>)</a></li>
		</ul>
	</div>
	<div>
		<div id="#vtabs-content-p">
        	 <?php if( ($this->key_imagereels) > 0 ): ?>
              <?php $i=0;foreach( $this->key_imagereels as $img ): ?>
                <ul id="profile_albums" class="thumbs gallery<?php echo $i;?>" style="width:150px !important;">
                <li>
                  <a class="fancybox" href="<?php echo $this->baseUrl().'/public/page/reels/'.$img['reel_path'];?>" data-fancybox-group="gallery">
            		<span style="background-image: url(<?php echo $this->baseUrl().'/public/page/reels/'.$img['reel_path'];?>); background-size: cover; background-position: 50% 50%; width:100%; float:left; height:150px !important;"></span>
        		 </a>
                 
                </li>
                 
                </ul>
              <?php $i++;endforeach;?>
              <?php else: ?>
              <ul class="gallery">
              	<li>No Images</li>
              </ul>
              <?php endif;?>
		</div>
		<div id="#vtabs-content-q">
              <ul class="videos_browse">
            <?php if( count($this->key_pdfreels) > 0 ): ?>
                  <?php foreach( $this->key_pdfreels as $pdf ):?>
               <li style="float: left;margin-right: 20px;max-width: 300px; height:100px !important;overflow: hidden;">
                  <div class="">
                    <a href="<?php echo $this->baseUrl().'/public/page/reels/'.$pdf['reel_path'];?>" target="_blank" class="wp_init"><img class="thumb_normal item_photo_album  thumb_normal" style="width:75px; height:75px;" alt="" src="<?php echo $this->baseUrl();?>/public/PDF-icon.png"></a>          </div>
                </li>
                <?php endforeach; ?>
                       
           <?php else: ?>
           	<li>No Pdfs</li>
           <?php endif; ?>
           </ul>
		</div>
        
        <div id="#vtabs-content-r">
           
             <ul id="profile_videos" class="videos_browse">
               <?php if( count($this->key_linkreels) > 0 ): ?>
              <?php $i=1;foreach( $this->key_linkreels as $item ): 
              	
              ?>
                <li style="float: left;margin-right: 20px;max-width: 300px; height:100px !important;overflow: hidden;">
                  <div class="">
                    <a href="<?php echo $item['reel_path'];?>" target="_blank">
                    	<img class="thumb_normal item_photo_album  thumb_normal" style="width:75px; height:75px;" alt="" src="<?php echo $this->baseUrl();?>/public/custom/images/video.png">
                    </a>
                  </div>
                </li>
              <?php $i++; endforeach; ?>
              <?php else: ?>
              <li> No Videos</li>
              <?php endif;?>
            </ul>	
		</div>
        
	</div>
</div>
    
</div>
    </div>
</div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.css" media="screen" />
<script src="https://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox-media.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl();?>/public/vtabs/css/jquery-jvert-tabs-1.1.4.css" />
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/vtabs/js/jquery-jvert-tabs-1.1.4.js"></script>


<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('.fancybox').fancybox();
	jQuery("#vtabs3").jVertTabs();
	jQuery('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});
});

$$(".layout_page_profile_fields .tip").destroy();
</script>
<div style="clear:both"></div>






