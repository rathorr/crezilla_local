<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Controller.php 2010-08-31 16:05 idris $
 * @author     Idris
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class Page_Widget_ProfileFieldsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
  	$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
		$this->view->subject = $subject = Engine_Api::_()->core()->getSubject('page');

    if (!$subject->authorization()->isAllowed($viewer, 'view')) {
      return $this->setNoRender();
    }
	//echo $subject->getIdentity(); die;
	$this->view->key_clients	=	Engine_Api::_()->getDbTable('clients', 'page')->getPageClients($subject->getIdentity());
	
	$this->view->key_projects	=	Engine_Api::_()->getDbTable('projects', 'page')->getPageProjects($subject->getIdentity());
	
	$this->view->key_awards	=	Engine_Api::_()->getDbTable('awards', 'page')->getPageAwards($subject->getIdentity());
	
	$this->view->addresses	=	Engine_Api::_()->getDbTable('contacts', 'page')->getPageAddresses($subject->getIdentity());
	
		$view = $this->view;
    $view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
		
		$this->view->fieldStructure = $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);
		
		
		$element = $this->getElement();
        $action_id = (int)Zend_Controller_Front::getInstance()->getRequest()->getParam('action_id');
        $activeTab = $action_id ? 'activity.feed' : $this->_getParam('tab');


        $subjectType = $subject->getType();
        $deprecated = array(
            'like.status',
            'page.profile-options',
            'page.profile-photo'
        );

        $tabs = array();
        $params = array();
        foreach ($element->getElements() as $child) {
            if (null === $activeTab) {
                $activeTab = $child->getIdentity();
            }
            $child->clearDecorators();

            $id = $child->getIdentity();
            $title = $child->getTitle();
            $name = $child->getName();

            if($subjectType == 'page' && in_array($name, $deprecated)) {
                continue;
            }

            $childCount = null;
            if (method_exists($child, 'getWidget') && method_exists($child->getWidget(), 'getChildCount')) {
                $childCount = $child->getWidget()->getChildCount();
            }
            if (!$title) $title = $name;

            $content = $child->render();

            if ($child->getNoRender()) {
                continue;
            }

            $tabs[] = array(
                'id' => $id,
                'name' => $name,
                'title' => $title,
                'childCount' => $childCount,
                'content' => $content . PHP_EOL
            );

            $params[$name]['title'] = ( is_array($child->params) && (string)(array_key_exists('title', $child->params))) ? $child->params['title'] : "TIMELINE_Application";

            if (method_exists($child, 'getWidget') && method_exists($child->getWidget(), 'getChildCount')) {
                $params[$name]['count'] = $child->getWidget()->getChildCount();
            }
        }

        $this->view->tabs = $tabs;
  }
}