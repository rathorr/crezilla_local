<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 2010-08-31 17:53 idris $
 * @author     Idris
 */
?>
<style type="text/css">
.Pro_qua a {color: #3b5998 !important;text-transform: lowercase !important;}
</style>
<script type="text/javascript">
	$$(".layout_page_profile_fields .tip").destroy();
</script>

<script src="https://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl();?>/public/vtabs/css/jquery-jvert-tabs-1.1.4.css" />
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/vtabs/js/jquery-jvert-tabs-1.1.4.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){

	jQuery("#vtabs1").jVertTabs();
});
</script>
<?php if($this->subject->parent_id == $this->viewer->getIdentity() ):?>
<a href="<?php echo $this->baseUrl().'/page-team/edit/'.$this->subject->getIdentity();?>" class="update_info">Update Info</a>
<?php endif;?>
<div id="vtabs1">
	<div>
		<ul>
        	<li><a href="#vtabs-content-a">Contact and Basic Info</a></li>
            <li><a href="#vtabs-content-b">Detail About Me</a></li>
            <li><a href="#vtabs-content-c">Awards/Clients</a></li>
		</ul>
	</div>
	<div>
		<div id="#vtabs-content-a">
			<ul class="pf_info">
                <li class="abt-info">
               		<span class="comp_label"> Website </span>
                	<span class="Pro_qua"><?php echo $this->subject->getWebsite(); ?></span>
              </li>
              
              
               <li class="abt-info">
               	<span class="comp_label"> Address </span>
              <?php 
                $address	=	$this->addresses;
                if($address){
              
                    foreach($address as $addr){?>
                    
                    	<div class="edu_box">
                            <span  class="Pro_qua"><?php echo $addr['address'];?></span>
                             <span  class="inst pro_institute">
                                      <?php echo $addr['country'];?>
                              <?php 
                              if($addr['zip_code']){
                              	echo ', '.$addr['zip_code'];
                              }
                              ?> </span>
                            <span class="year pro_year">
                               <?php echo $addr['phone_number'];?>
                            </span>
                                                                                                                 </div>
                        
                   <?php }
                   
                }else{
                    echo '<span class="office_details">Nothing Mentioned</span>';
                }
                ?>
              
              </ul>
		</div>
        <div id="#vtabs-content-b">
			<ul class="abt-info">
              <li style="border-bottom: 1px solid rgb(233, 234, 237); padding-bottom: 5px;">
               <span class="comp_label">About Me</span>
                 <span class="comp_details">
                 <?php echo $this->subject->getDescription(false, false, false); ?></span>
              </li>
              
              <?php if($this->subject->services){?>
              <li>
              	<span class="comp_label">Services</span>
                <span class="comp_details"><?php echo $this->subject->services; ?></span>
              </li>
             <?php  }?>
              </ul>
		</div>
        <div id="#vtabs-content-c">
			<ul class="pf_info">
            		<li class="abt-info" style="border:none;">
               			<span class="comp_label" style="border-bottom:1px solid #e9eaed;"> Clients </span>
                            <?php $key_clients	=	$this->key_clients;
                            if(isset($key_clients[0])){
                           // echo '<div class="work_edu"><h4>Clients</h4></div>;
                            
                            foreach($this->key_clients as $clnt){?>
                                <div class="edu_box">
                                    <span class="Pro_qua">
                                    <?php echo $clnt['client_name'];?></span>
                                    
                                </div>
                        <?php }
                            }else{
                            	echo '<div class="edu_box">
                            <span class="Pro_qua">Nothing Mentioned</span>
                        </div>';
                            }?>
                         </li>
                <li class="abt-info" style="border:none;">
               			<span class="comp_label"  style="border-bottom:1px solid #e9eaed;"> Projects </span>
                    <?php
                    $key_projects	=	$this->key_projects;
                    if(isset($key_projects[0])){
                   
                    foreach($this->key_projects as $pro){?>
                        <div class="exp_box">
                            <span class="Pro_qua"><?php echo $pro['project_name'];?></span>
                        </div>
                    <?php }
                    }else{
                    	echo '<div class="exp_box">
                            <span class="Pro_qua">Nothing Mentioned</span>
                        </div>';
                    }?>
                 </li>
                 
                 <li class="abt-info" style="border:none;">
               			<span class="comp_label"  style="border-bottom:1px solid #e9eaed;"> Awards </span>
                 
                 
                    <?php
                    $key_awards	=	$this->key_awards;
                    if(isset($key_awards[0])){
                    foreach($this->key_awards as $awd){?>
                        <div class="adw_box">
                            <span class="Pro_qua"><?php echo $awd['award_name'];?></span>
                        </div>
                    <?php }
                    }else{
                    	echo '<div class="awd_box">
                            <span class="Pro_qua">Nothing Mentioned</span>
                        </div>';
                    }?>
                    </li>
                    </ul>
		</div>
		
	</div>
</div>
  





<div style="clear:both"></div>