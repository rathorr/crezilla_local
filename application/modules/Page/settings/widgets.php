<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: widgets.php 2010-08-31 16:05 idris $
 * @author     Idris
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

$multiOptions = array('friends' => 'Friends');
$multiComposerOptions = array('addLink' => 'Link');
$include = array('group', 'album', 'advalbum', 'advgroup');
$module_table = Engine_Api::_()->getDbTable('modules', 'core');
$module_name = $module_table->info('name');
$select = $module_table->select()
        ->from($module_name, array('name', 'title'))
        ->where($module_name . '.type =?', 'extra')
        ->where($module_name . '.name in(?)', $include)
        ->where($module_name . '.enabled =?', 1);

$contentModule = $select->query()->fetchAll();
$include[] = 'friends';
foreach ($contentModule as $module) {
    if ($module['name'] != 'album' && $module['name'] != 'advalbum')
        $multiOptions[$module['name']] = $module['title'];
    if ($module['name'] == 'album' || $module['name'] == 'advalbum')
        $multiComposerOptions['addPhoto'] = 'Photo';
}

$multiComposerOptions['addSmilies'] = 'Emoticons';

$others = array();
$page = array(
  array(
    'title' => 'HTML Block',
    'description' => 'Inserts any HTML of your choice.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'page.html-block',
    'special' => 1,
    'autoEdit' => true,
    'adminForm' => array(
      'elements' => array(
        array(
          'Text',
          'title',
          array(
            'label' => 'Title',
            'value' => 'HTML Block'
          ),
        ),
        array(
          'Textarea',
          'data',
          array(
            'label' => 'HTML'
        ),
        ),
      ),
    ),
  ),
  array(
    'title' => 'Page Rate',
    'description' => 'Displays the page\'s rate information.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'rate.widget-rate',
		'defaultParams' => array(
      'title' => 'Page Rate',
  		'titleCount' => false
    ),
  ),
  array(
    'title' => 'Page Activity Feed',
    'description' => 'Displays the page\'s activity feed(wall).',
    'category' => 'Tabs',
    'type' => 'widget',
    'name' => 'page.feed',
  	'defaultParams' => array(
      'title' => 'Updates',
  		'titleCount' => false
    ),
  ),
  array(
    'title' => 'Page Photo',
    'description' => 'Displays the page\'s photo(logo).',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'page.profile-photo',
		'defaultParams' => array(
      'title' => '',
  		'titleCount' => false
    ),
  ),
  array(
    'title' => 'Page Like Status',
    'description' => 'Displays the page\'s `likes` options.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'like.status'
  ),
  array(
    'title' => 'Page Map',
    'description' => 'Displays the page\'s location on Google Map.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'page.profile-map',
		'defaultParams' => array(
      'title' => 'Page Map',
  		'titleCount' => false
    ),
  ),
  array(
    'title' => 'Page Note',
    'description' => 'Displays the page\'s note - informative/welcome text, team members are allowed to edit this note.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'page.profile-note',
		'defaultParams' => array(
      'title' => 'Page Note',
  		'titleCount' => false
    ),
  ),
  array(
    'title' => 'Favorite Pages',
    'description' => 'Displays the page\'s favorite pages',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'page.favorite-pages',
		'defaultParams' => array(
      'title' => 'Favorite Pages',
  		'titleCount' => true
    ),
  ),
  array(
    'title' => 'Most Popular Pages',
    'description' => 'Displays most popular pages.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'page.popular-pages',
    'defaultParams' => array(
      'title' => 'Most Popular Pages',
      'titleCount' => false
    ),
  ),
  array(
    'title' => 'Page Team',
    'description' => 'Displays the page\'s team members.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'page.profile-admins',
    'isPaginated' => true,
  	'defaultParams' => array(
      'title' => 'Page Team',
  		'titleCount' => false
    ),
  ),

  array(
    'title' => 'Members Likes This',
    'description' => 'Displays members who liked the page.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'like.box',
		'defaultParams' => array(
      'title' => 'like_Like Club',
  		'titleCount' => true
    ),
  ),
  array(
    'title' => 'Page Options',
    'description' => 'Displays the page\'s options.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'page.profile-options',
		'defaultParams' => array(
      'title' => 'Page Options',
  		'titleCount' => false
    ),
  ),
  array(
    'title' => 'Page Info',
    'description' => 'Displays the page\'s detailed information.',
    'category' => 'Tabs',
    'type' => 'widget',
    'name' => 'page.profile-fields',
  	'defaultParams' => array(
      'title' => 'Info',
  		'titleCount' => true
    ),
  ),
  array(
    'title' => 'Show Reels',
    'description' => 'Displays page show reels',
    'category' => 'Page',
    'type' => 'widget',
    'name' => 'page.page-reels',
    'defaultParams' => array(
      'title' => 'Show Reels',
    )
  ),
  array(
    'title' => 'Show Jobs',
    'description' => 'Displays page related jobs',
    'category' => 'Page',
    'type' => 'widget',
    'name' => 'page.page-jobs',
    'defaultParams' => array(
      'title' => 'Show Jobs',
    )
  ),
  array(
    'title' => 'Show Projects',
    'description' => 'Displays page jobs related project',
    'category' => 'Page',
    'type' => 'widget',
    'name' => 'page.page-projects',
    'defaultParams' => array(
      'title' => 'Show Projects',
    )
  ),
	array(
		'title' => 'Search',
		'description' => 'Displays search box.',
		'category' => 'Widgets',
		'type' => 'widget',
		'name' => 'page.search',
		'defaultParams' => array(
      'title' => 'Search',
  		'titleCount' => false
    ),
  ),
	array(
		'title' => 'Tag Cloud',
		'description' => 'Displays tags cloud box.',
		'category' => 'Widgets',
		'type' => 'widget',
		'name' => 'page.tag-cloud',
		'defaultParams' => array(
      'title' => 'Tag Cloud',
  		'titleCount' => false
    ),
  ),
  array(
    'title' => 'Page Links',
    'description' => 'Displays the page\'s links.',
    'category' => 'Tabs',
    'type' => 'widget',
    'name' => 'page.page-links',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Links',
      'titleCount' => true,
    ),
    'requirements' => array(
      'subject',
    ),
  ),
  array(
    'title' => 'Statistics',
    'description' => 'Displays the page\'s likes, views. Statistics(graphic) - only team members are allowed to view.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'page.page-statistics',
    'defaultParams' => array(
      'title' => 'Statistics',
      'titleCount' => false
    ),
  ),
  array(
    'title' => 'Staff',
    'description' => 'Displays the page\'s staff list.',
    'category' => 'Tabs',
    'type' => 'widget',
    'name' => 'page.profile-staff',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Staff',
      'titleCount' => true
    ),
  ),
  array(
    'title' => 'Page Profile Check-Ins',
    'description' => 'Displays thumbnails of members who checked-in. Please put this widget on the right side.',
    'category' => 'Widgets',
    'type' => 'widget',
    'name' => 'page.page-profile-checkins',
    'defaultParams' => array(
      'title' => 'Check-Ins Profile Thumbnails',
      'titleCount' => false
    ),
  ),

  array(
    'title' => 'Check-Ins Tab',
    'description' => 'Displays the page\'s check-ins in detail in tab.',
    'category' => 'Tabs',
    'type' => 'widget',
    'name' => 'checkin.page-checkins',
    'defaultParams' => array(
      'title' => 'Check-Ins',
      'titleCount' => false
    ),
  ),

  array(
    'title' => 'Tab Container',
    'description' => 'Adds a container with a tab menu. Any other blocks you drop inside it will become tabs.',
    'category' => 'Tabs',
    'type' => 'widget',
    'name' => 'core.container-tabs',
    'special' => 1,
    'defaultParams' => array(
      'max' => 10
    ),
    'canHaveChildren' => true,
    'childAreaDescription' => 'Adds a container with a tab menu. Any other blocks you drop inside it will become tabs.',
    //'special' => 1,
    'adminForm' => array(
      'elements' => array(
        array(
          'Text',
          'title',
          array(
            'label' => 'Title',
          )
        ),
        array(
          'Select',
          'max',
          array(
            'label' => 'Max Tab Count',
            'description' => 'Show sub menu at x containers.',
            'default' => 10,
            'multiOptions' => array(
              0 => 0,
              1 => 1,
              2 => 2,
              3 => 3,
              4 => 4,
              5 => 5,
              6 => 6,
              7 => 7,
              8 => 8,
              9 => 9,
              10 => 10,
              11 => 11
            )
          )
        ),
      )
    ),
  ),

  array(
    'title' => 'Login or Signup',
    'description' => 'Displays a login form and a signup link for members that are not logged in.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.login-or-signup',
    'requirements' => array(
      'no-subject',
    ),
  ),
  array(
  'title' => 'Display Advertisements',
		'description' => 'Display advertisements on your site. Multiple settings available in the Edit Settings of this widget.',
		'category' => 'Community Ads',
		'type' => 'widget',
		'name' => 'communityad.ads',
        'autoEdit' => true,
        'defaultParams' => array(
            'loaded_by_ajax' => 1,
        ),
		'adminForm' => 'Communityad_Form_Admin_Widget_Ads'

    ),
	array(
    'title' => 'Page Name',
    'description' => 'Displays a page\'s name..',
    'category' => 'Page',
    'type' => 'widget',
    'name' => 'page.profile-name',
    'requirements' => array(
      'subject' => 'page',
    ),
  ),
  array(
    'title' => 'Page Wall Photo',
    'description' => 'Displays a page\'s wall photo.',
    'category' => 'Page',
    'type' => 'widget',
    'name' => 'page.profile-wallphoto',
    'requirements' => array(
      'viewer',
    ),
  ),
	array(
    'title' => 'Advanced Activity Page Feed',
    'description' => 'Displays the advanced activity feed.',
    'category' => 'Advanced Feed',
    'type' => 'widget',
    'name' => 'ynfeed.page-feed',
    'defaultParams' => array(
      'title' => 'What\'s New',
    ),
  'adminForm' => array(
      'elements' => array(
    ),
    )
  ),
   array(
        'title' => 'Advanced Comments & Replies',
        'description' => 'Enable users to comment and reply on the content being viewed. Displays all the comments and replies on the Content View page. This widget should be placed on Content View page.',
        'category' => 'Advanced Comments',
        'type' => 'widget',
        'name' => 'yncomment.comments',
        'autoEdit' => 'true',
        'defaultParams' => array(
            'title' => '',
            'taggingContent' => array("friends", "group", "event")
        ),
        'adminForm' => array(
            'elements' => array(
                array(
                    'MultiCheckbox',
                    'taggingContent',
                    array(
                        'label' => "Which Content Type do you want to tag in comments/replies? ('@' symbol is used)?",
                        'multiOptions' => $multiOptions,
                    )
                ),
                array(
                    'MultiCheckbox',
                    'showComposerOptions',
                    array(
                        'label' =>  "Which attachment type do you want to add into comments/replies?",
                        'multiOptions' => $multiComposerOptions,
                    )
                ),
                array(
                    'Radio',
                    'showAsNested',
                    array(
                        'label' => "Do you want to enable reply link on  a comment in this plugin's content? (If no, there is only one-level comment, user will not able to reply on the comment)",
                        'multiOptions' => array(
                            1 => 'Yes',
                            0 => 'No'
                        ),
                        'value' => 1
                    ),
                ),
                array(
                    'Radio',
                    'showAsLike',
                    array(
                        'label' => "Which option of Like/Dislike do you want to use?",
                        'multiOptions' => array(
                            1 => 'Like only',
                            0 => 'Both Like and Dislike'
                        ),
                        'value' => 1
                    ),
                ),
                array(
                    'Radio',
                    'showDislikeUsers',
                    array(
                        'label' => "Do you want to show the user's name who dislike the comments/replies? [Note: this setting will work only if both Like and Dislike setting is set]",
                        'multiOptions' => array(
                            1 => 'Yes',
                            0 => 'No'
                        ),
                        'value' => 1
                    ),
                ),
                array(
                    'Radio',
                    'showLikeWithoutIcon',
                    array(
                        'label' => "How do you want to display Like, Dislike for this plugin's content?",
                        'multiOptions' => array(
                            1 => 'Text only',
                            0 => 'Text with icon'
                        ),
                        'value' => 1
                    ),
                ),
                array(
                    'Radio',
                    'showLikeWithoutIconInReplies',
                    array(
                        'label' => "How do you want to display Like, Dislike for comments/replies?",
                        'multiOptions' => array(
                            1 => 'Text only',
                            2 => 'Icon only',
                            0 => 'Text with icon',
                            3 => 'Vote up/Vote down? [Note: this setting will work only if both Like and Dislike setting is set]'
                        ),
                        'value' => 1
                    ),
                ),
                array(
                    'Radio',
                    'commentsorder',
                    array(
                            'label' => 'Select the order in which comments should be displayed on your website.',
                            'multiOptions' => array(
                                    1 => 'Newer to older',
                                    0 => 'Older to newer'
                            ),
                            'value' => 1,
                    )
              ),
                array(
                    'Radio',
                    'loaded_by_ajax',
                    array(
                        'label' => 'Do you want this widget content  to be loaded by AJAX? (If yes, this action can improve webpage loading speed. If no, this action will load widget content along with page content)',
                        'multiOptions' => array(
                            1 => 'Yes',
                            0 => 'No'
                        ),
                        'value' => 1,
                    )
                ),
            ),
        ),
        'requirements' => array(
            'subject',
        ),
    ),
);

$modulesTbl = Engine_Api::_()->getDbTable('modules', 'core');
if ($modulesTbl->isModuleEnabled('offers')) {
  $others = array(
    array(
      'title' => 'Favorite Offer',
      'description' => 'Displays the page\'s favorite offer',
      'category' => 'Widgets',
      'type' => 'widget',
      'name' => 'offers.favorite-offer',
      'defaultParams' => array(
        'title' => 'Favorite Offer',
        'titleCount' => true
      )
    )
  );
}

return array_merge($page, $others);