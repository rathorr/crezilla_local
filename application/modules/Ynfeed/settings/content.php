<?php
return array(
  array(
    'title' => 'Advanced Activity Feed',
    'description' => 'Displays the advanced activity feed.',
    'category' => 'Advanced Feed',
    'type' => 'widget',
    'name' => 'ynfeed.feed',
    'defaultParams' => array(
      'title' => 'What\'s New',
    ),
  'adminForm' => array(
      'elements' => array(
    ),
    )
  ),
  array(
    'title' => 'Advanced Page Activity Feed',
    'description' => 'Displays the advanced page activity feed.',
    'category' => 'Advanced Feed',
    'type' => 'widget',
    'name' => 'ynfeed.page-feed',
    'defaultParams' => array(
      'title' => 'What\'s New',
    ),
  'adminForm' => array(
      'elements' => array(
    ),
    )
  ),
  array(
    'title' => 'Welcome Tab',
    'description' => 'Displays the welcome tab on home page.',
    'category' => 'Advanced Feed',
    'type' => 'widget',
    'name' => 'ynfeed.welcome-tab',
    'defaultParams' => array(
      'title' => 'Welcome',
    ),
  'adminForm' => array(
      'elements' => array(
    ),
    )
  )
) ?>