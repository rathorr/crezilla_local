<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Activity
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Attachments.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Activity
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Activity_Model_DbTable_Attachments extends Engine_Db_Table
{
  public function getAttachment($action_id){
	 $select = $this->select()
      ->where('action_id = ?', $action_id)
      ;
	  $result	=	$this->fetchAll($select);
	  
	  if($result){
		$result	=	$result[0];
		return $result; 
	  }
  }
}