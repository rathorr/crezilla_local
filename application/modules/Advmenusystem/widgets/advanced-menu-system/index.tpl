<script type="text/javascript">
var check_return_url = false;
var ynAdvMenu = {	
	updateAllElement: function()
	{
		var flag = 0;
		
		if(flag == 0)
		{
			ynAdvMenu.doAddSubMenu();
		}
	},
	disableAllSubMenu: function()
	{
		
		// disable all sub main menu
		<?php foreach($this->arr_sub_main as $value):?>
			ynAdvMenu.doDisableMenu('<?php echo $value;?>');
		<?php endforeach;?>
	},
	doDisableMenu: function(name)
	{
		var sub_menu_item = $$('.'+ name)[0];
		
		if(sub_menu_item !== null && sub_menu_item !== undefined)
		{
			sub_menu_item.parentNode.style.display = 'none';
		}
	},
	doAddSubMenu: function()
	{
		
		//Add sub menu for main menu 
		<?php foreach($this->arr_objectParent_main as $key => $html_json):?>
			ynAdvMenu.doAddEachSubMainMenu('<?php echo $key?>',<?php echo $html_json;?>);
		<?php endforeach;?>
	},
	doAddEachSubMainMenu: function(key, html)
	{
		var parent_menu_item = $$('.'+ key)[0];
		if(parent_menu_item !== null && parent_menu_item !== undefined)
		{
			var parent_item = parent_menu_item.parentNode;
			parent_item.className += ' ynadvmenu_downservicesMain active';
			parent_item.innerHTML = html;
		}
	},
	actionAdvancedMenuSystem: function()
	{
		ynAdvMenu.disableAllSubMenu();
		ynAdvMenu.updateAllElement();
	},
}
document.onload = ynAdvMenu.actionAdvancedMenuSystem(); 
</script>
