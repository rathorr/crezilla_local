<?php

class Advmenusystem_Api_Core {

	protected static $_log;
	/**
	 * get main logger
	 * @return Zend_Log
	 */
	public function getLog()
	{
		if (self::$_log == null)
		{
			self::$_log = new Zend_Log(new Zend_Log_Writer_Stream(APPLICATION_PATH . '/temporary/log/advmenusystem.txt'));
		}
		return self::$_log;
	}

	/**
	 * write log to temporary/ynevent.log
	 * @param string $message
	 * @param string $intro
	 * @param int   $type
	 * @return Zend_Log
	 */
	public function log($message, $intro = 'Loging Info', $type = null)
	{
		if (APPLICATION_ENV !== 'development')
		{
			return;
		}
		if ($type == null)
		{
			$type = Zend_Log::INFO;
		}
		if (!is_string($message))
		{
			$message = var_export($message, true);
		}
		return $this -> getLog() -> log(PHP_EOL . $intro . PHP_EOL . $message, $type);
	}
	public static function partialViewFullPath($partialTemplateFile) 
	 {
		$ds = DIRECTORY_SEPARATOR;
		return "application{$ds}modules{$ds}Advmenusystem{$ds}views{$ds}scripts{$ds}{$partialTemplateFile}";
  	 }

}
