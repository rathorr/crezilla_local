<?php
/**
 * Social Engine
 *
 * @category   Application_Extensions
 * @package    Advanced menu system
 */
class Advmenusystem_AdminStylesController extends Core_Controller_Action_Admin
{
  public function init()
  {
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('advmenusystem_admin_main', array(), 'advmenusystem_admin_main_styles');
  }
  public function indexAction()
  {
    $this->view->form = $form = new Advmenusystem_Form_Admin_Style();
	$values = Engine_Api::_()->getApi('settings', 'core')->getSetting('avdmenusystem.customcssobj', 0);
	$values = Zend_JSON::decode($values);
	if($values)
		$form->populate($values);
    if( $this->getRequest()->isPost() && $form->isValid($this->_getAllParams()) )
    {
	     $values = $form->getValues();
		 if(isset($_POST['submit']))
		 {
		     $str = $this->view->partial("_css.tpl");
			 $arr_keys = array_keys($values);
			 $arr_values = array_values($values);
			 $arr_keys = array_map(array($this ,'map'), $arr_keys);
			 $str = str_replace($arr_keys, $arr_values, $str);
			 Engine_Api::_()->getApi('settings', 'core')->setSetting('avdmenusystem.customcssobj', Zend_JSON::encode($values));
			 Engine_Api::_()->getApi('settings', 'core')->setSetting('avdmenusystem_customcss', $str);
		     $form->addNotice('Your changes have been saved.');
		 }
		else if(isset($_POST['clear']))
		{
			Engine_Api::_()->getApi('settings', 'core')->setSetting('avdmenusystem.customcssobj', '');
			Engine_Api::_()->getApi('settings', 'core')->setSetting('avdmenusystem_customcss', '');
			$form->populate(array('main_menu_sub_menu_background_color' => '79B4D4',
								'main_menu_sub_menu_link_hover_color'=> '',
								'main_menu_sub_menu_link_color'=> '',
								'main_menu_sub_menu_vertical_arrow'=>'center',
								'notification_padding_top'=>'0px'));
			$form->addNotice('You have set default styles.');

		}
    }
  }
  static public function map($a)
  {
  		return "[{$a}]";
  }
}
