<?php
class Advmenusystem_Form_Admin_Style extends Engine_Form
{
  public function init()
  {
  	    $view = Zend_Registry::get('Zend_View');
	    $view->headScript()->appendFile(Zend_Registry::get('StaticBaseUrl') . 'application/modules/Advmenusystem/externals/scripts/jscolor.js');
    $this
      ->setTitle('Style Settings')
      ->setDescription('These settings affect all theme in your community.')
	  ->setAttrib('style',"width: 600px");
	  
	//Change style for Notifications
	$this->addElement('dummy', 'head_notifications',array(
      'label'=>'Notification Icons',
     ));
	$this->addElement('Text', 'notification_padding_top', array(
      'label' => 'Notification icons padding top',      
      'allowEmpty' => false,
      'value' => "0px",
    )); 
	
	//Change style for Main Menu drop down
	$this->addElement('dummy', 'head_main_menu',array(
      'label'=>'Main Menu drop down',
     ));
	 $this->addElement('Text', 'main_menu_sub_menu_background_color', array(
      'label' => 'Submenu - Background color',
      'class' => 'color',   
      'allowEmpty' => false,
      'value' => "79B4D4",
    ));
	$this->addElement('Text', 'main_menu_sub_menu_link_color', array(
      'label' => 'Submenu - Link color',
      'class' => 'color',   
      'allowEmpty' => false,
      'value' => "",
    )); 
	$this->addElement('Text', 'main_menu_sub_menu_link_hover_color', array(
      'label' => 'Submenu - Link hover color',
      'class' => 'color',   
      'allowEmpty' => false,
      'value' => "",
    )); 
	$this->addElement('Text', 'main_menu_sub_menu_vertical_arrow', array(
      'label' => 'Submenu - Vertical arrow position',
      'allowEmpty' => false,
      'value' => "center",
    )); 
	
    // Add submit button
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true
    ));
	// Clear submit button
    $this->addElement('Button', 'clear', array(
      'label' => 'Set Default',
      'type' => 'submit',
      'ignore' => true
    ));
  }
}