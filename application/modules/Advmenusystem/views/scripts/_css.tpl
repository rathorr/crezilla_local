/* Adv.Menu Notification */
li.ynadvmenu_notification
{
	padding-top: [notification_padding_top]!important;
}

/* Sub main menu*/
li.ynadvmenu_downservicesMain.active ul.ynadvmenu_pulldownMainWrapper
{
	background-color: [main_menu_sub_menu_background_color];
	background-color: #[main_menu_sub_menu_background_color];	
}
li.ynadvmenu_downservicesMain.active ul.ynadvmenu_pulldownMainWrapper > li a
{
	color: [main_menu_sub_menu_link_color];
	color: #[main_menu_sub_menu_link_color];
}
li.ynadvmenu_downservicesMain.active ul.ynadvmenu_pulldownMainWrapper > li a:hover
{
	color: [main_menu_sub_menu_link_hover_color];
	color: #[main_menu_sub_menu_link_hover_color];
}
li.ynadvmenu_downservicesMain.active > a:link, li.ynadvmenu_downservicesMain.active > a:visited,
li.ynadvmenu_downservicesMain.active:hover > a:link, li.ynadvmenu_downservicesMain.active:hover > a:visited
{
 	background-position: right [main_menu_sub_menu_vertical_arrow] !important;
}