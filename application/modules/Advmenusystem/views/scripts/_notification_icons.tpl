<div id="ynadvmenu_MessagesUpdates" class="ynadvmenu_mini_wrapper">
	<a href="javascript:void(0);" class="ynadvmenu_NotifiIcon" id="ynadvmenu_messages" >
		<span id="ynadvmenu_MessageIconCount" class="ynadvmenu_NotifiIconWrapper" style="display:none"><span id="ynadvmenu_MessageCount"></span></span>
	</a>
	<div class="ynadvmenuMini_dropdownbox" id="ynadvmenu_messageUpdates" style="display: none;">
		<div class="ynadvmenu_dropdownHeader">
			<div class="ynadvmenu_dropdownArrow"></div>				  
		</div>
		<div class="ynadvmenu_dropdownTitle">
			<h3><?php echo $this->translate("Messages") ?> </h3>				
			<a href="<?php echo $this->baseUrl() ?>/messages/compose"><?php echo $this->translate("Send a New Message") ?></a>
		</div>
		<div class="ynadvmenu_dropdownContent" id="ynadvmenu_messages_content">
			<!-- Ajax get and out contetn to here -->
		</div>				
		<div class="ynadvmenu_dropdownFooter">
			<a class="ynadvmenu_seeMore" href="<?php echo $this->url(array('action'=>'inbox'),'messages_general') ?>">
				<span><?php echo $this->translate("See All Messages") ?> </span>
			</a>				
		</div>				
	</div>
</div>

<div id="ynadvmenu_FriendsRequestUpdates" class="ynadvmenu_mini_wrapper">
	<a href="javascript:void(0);" class="ynadvmenu_NotifiIcon" id = "ynadvmenu_friends">
		<span id="ynadvmenu_FriendIconCount" class="ynadvmenu_NotifiIconWrapper" style="display:none"><span id="ynadvmenu_FriendCount"></span></span>
	</a>
	<div class="ynadvmenuMini_dropdownbox" id="ynadvmenu_friendUpdates" style="display: none;">
		<div class="ynadvmenu_dropdownHeader">
			<div class="ynadvmenu_dropdownArrow"></div>				  
		</div>
		<div class="ynadvmenu_dropdownTitle">
			<h3><?php echo $this->translate("Friend Requests") ?> </h3>				
			<a href="<?php echo $this->baseUrl() ?>/members"><?php echo $this->translate("Find Friends") ?></a>
		</div>
		<div class="ynadvmenu_dropdownContent" id="ynadvmenu_friends_content">
			<!-- Ajax get and out contetn to here -->
		</div>				
		<div class="ynadvmenu_dropdownFooter">
			<a class="ynadvmenu_seeMore" href="<?php echo $this->url(array(),'advmenusystem_friend_requests')?>">
				<span><?php echo $this->translate("See All Friend Requests") ?> </span>
			</a>				
		</div>				
	</div>
</div>

<div id="ynadvmenu_NotificationsUpdates" class="ynadvmenu_mini_wrapper">
	<a href="javascript:void(0);" class="ynadvmenu_NotifiIcon" id = "ynadvmenu_updates">
		<span id="ynadvmenu_NotifyIconCount" class="ynadvmenu_NotifiIconWrapper"><span id="ynadvmenu_NotifyCount"></span></span>
	</a>
	<div class="ynadvmenuMini_dropdownbox" id="ynadvmenu_notificationUpdates" style="display: none;">
		<div class="ynadvmenu_dropdownHeader">
			<div class="ynadvmenu_dropdownArrow"></div>				  
		</div>
		<div class="ynadvmenu_dropdownTitle">
			<h3><?php echo $this->translate("Notifications") ?> </h3>									
		</div>
		<div class="ynadvmenu_dropdownContent" id="ynadvmenu_updates_content">
			<!-- Ajax get and out contetn to here -->
		</div>				
		<div class="ynadvmenu_dropdownFooter">
			<a class="ynadvmenu_seeMore" href="<?php echo $this->baseUrl()?>/adv-menu-system/notifications">
				<span><?php echo $this->translate("See All Notifications") ?> </span>
			</a>				
		</div>				
	</div>
</div>			