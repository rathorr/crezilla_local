<?php echo $this->htmlLink($this->parent_item->getHref(), $this->translate($this->parent_item->getLabel()), array_filter(array(
	        'class' => ( !empty($this->parent_item->class) ? $this->parent_item->class : null ),
	        'alt' => ( !empty($this->parent_item->alt) ? $this->parent_item->alt : null ),
	        'target' => ( !empty($this->parent_item->target) ? $this->parent_item->target : null ),
	      )));?>
<ul class="ynadvmenu_pulldownMainWrapper">
	<?php foreach($this->sub_menus as $item):?>
	<li>							
		<?php echo $this->htmlLink($item->getHref(), $this->translate($item->getLabel()), array_filter(array(
	        'class' => ( !empty($item->class) ? $item->class : null ),
	        'alt' => ( !empty($item->alt) ? $item->alt : null ),
	        'target' => ( !empty($item->target) ? $item->target : null ),
	      ))) ?>
	</li>	
	<?php endforeach;?>				
</ul>
				
