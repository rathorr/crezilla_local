<?php

class Advmenusystem_Bootstrap extends Engine_Application_Bootstrap_Abstract
{
    public function __construct($application)
    {
        parent::__construct($application);
        $this->initViewHelperPath();
    }

	  public function _initCss()
	  {
		  $view = Zend_Registry::get('Zend_View');

		  $str = (string)Engine_Api::_()->getApi('settings', 'core')->getSetting('avdmenusystem.customcss', "");
		  // Engine_Api::_()->advmenusystem()->log($str);
		  $view->headStyle()->prependStyle($str);
		  //check theme active and get css for this theme
		  $themes   = Engine_Api::_()->getDbtable('themes', 'core')->fetchAll();
      $activeTheme = $themes->getRowMatching('active', 1);
		  $arrname = explode("-", $activeTheme->name);
		  $name_theme = $arrname[0];
		  $url = $view->baseUrl(). '/application/modules/Advmenusystem/externals/themes/'.$name_theme.'.css';
		  $view->headLink()->appendStylesheet($url);	
	  }     

}