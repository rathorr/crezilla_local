<?php

define('DEBUG',true);

include dirname(dirname(dirname(__FILE__))) . '/cli.php';

$viewer = Engine_Api::_()->user()->getViewer();
if (!$viewer->getIdentity()) {
	exit('{notification:0,freq:0,msg: ""}');    
    //$this->_helper->viewRenderer->setNoRender(true);
}
$notificationTb = Engine_Api::_()->getDbtable('notifications', 'activity');
$tbName = $notificationTb->info('name');
$db = $notificationTb->getAdapter();
$db->beginTransaction();
try {
    $notif = $db->query("SELECT COUNT(*) AS total FROM $tbName WHERE `user_id` = {$viewer->getIdentity()} AND `read` = 0 AND `type` <> 'friend_request' AND `type` <> 'message_new'")->fetch();
    $freq = $db->query("SELECT COUNT(*) AS total FROM $tbName WHERE `user_id` = {$viewer->getIdentity()} AND `read` = 0 AND `type` = 'friend_request'")->fetch();
} catch (Exception $ex) {
    $db->rollBack();
    $ex->getMessage();
}

$data = array(
    'notification' => $notif['total'],
    'freq' => $freq['total'],
    'msg' => Engine_Api::_()->messages()->getUnreadMessageCount($viewer)   
);
echo Zend_Json::encode($data);