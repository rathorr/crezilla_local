INSERT IGNORE INTO `engine4_core_modules` (`name`, `title`, `description`, `version`, `enabled`, `type`) VALUES
('advmenusystem', 'Advanced Menu System', 'This is Advanced Menu System module.', '4.03', 1, 'extra') ;

DELETE FROM `engine4_core_menus` WHERE `name` = 'advmenusystem_mini' LIMIT 1;

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES  
('core_admin_main_plugins_advmenusystem', 'advmenusystem', 'Adv Menu System', '', '{"route":"admin_default","module":"advmenusystem","controller":"menus"}', 'core_admin_main_plugins', '', 999),
('advmenusystem_admin_main_menus', 'advmenusystem', 'Menu Settings', '', '{"route":"admin_default","module":"advmenusystem","controller":"menus"}', 'advmenusystem_admin_main', '', 2),
('advmenusystem_admin_main_styles', 'advmenusystem', 'Style Settings', '', '{"route":"admin_default","module":"advmenusystem","controller":"styles"}', 'advmenusystem_admin_main', '', 3);

ALTER TABLE `engine4_core_menuitems`     ADD COLUMN `flag_unique` INT(2) DEFAULT '0' NOT NULL ;
ALTER TABLE `engine4_core_menuitems` DROP KEY `name`, ADD UNIQUE `name` (`name`, `flag_unique`);

ALTER TABLE `engine4_activity_notifications` ADD INDEX ( `user_id`,`type`,`mitigated`);
ALTER TABLE `engine4_activity_notifications` ADD INDEX ( `user_id`,`type`,`read`); 