<?php
/**
 * Social Engine
 *
 * @category   Application_Extensions
 * @package    Advanced menu system
 */
return array(
   array(
    'title' => 'Advanced Menu System',
    'description' => 'Displays Advanced Advanced Menu System on header page.',
    'category' => 'Advanced Menu System',
    'type' => 'widget',
    'name' => 'advmenusystem.advanced-menu-system',
  ),

) ?>