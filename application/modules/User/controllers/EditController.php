<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_EditController extends Core_Controller_Action_User
{
  public function init()
  {
    if( !Engine_Api::_()->core()->hasSubject() ) {
      // Can specifiy custom id
      $id = $this->_getParam('id', null);
      $subject = null;
      if( null === $id ) {
        $subject = Engine_Api::_()->user()->getViewer();
        Engine_Api::_()->core()->setSubject($subject);
      } else {
        $subject = Engine_Api::_()->getItem('user', $id);
        Engine_Api::_()->core()->setSubject($subject);
      }
    }

    if( !empty($id) ) {
      $params = array('id' => $id);
    } else {
      $params = array();
    }
    // Set up navigation
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	 if(strtolower($profile_type) == 'talent'){
		 $menu_type	=	'user_edit';
	 }else if(strtolower($profile_type) == 'agency'){
		$menu_type	=	'custom_33'; 
	 }else
	 {
		$menu_type	=	'custom_31'; 
		
		if( isset($is_company->value) && $is_company->value !="" )
		{
			//echo $is_company->value;
			$menu_type	=	'custom_34'; 
		}
		
	
	 }
	 	
      
	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit', array('params' => $params));

	   
    /*$this->view->navigation = $navigation = Engine_Api::_()
      ->getApi('menus', 'core')
      ->getNavigation('user_edit', array('params' => $params));*/

    // Set up require's
    $this->_helper->requireUser();
    $this->_helper->requireSubject('user');
    $this->_helper->requireAuth()->setAuthParams(
      null,
      null,
      'edit'
    );
  }
  
  public function pathapprovalAction(){
		$this->_helper->layout()->disableLayout();
  }
  
  public function profileAction()
  {
	  
    $this->view->user = $user = Engine_Api::_()->core()->getSubject();
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    //$this->view->useridentity= $user->getIdentity();

    $this->view->passform = $passform = new User_Form_Settings_Password();
	 
   ///// Designation Categories start
  //$dasignatedsubcategories = Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->fetchAll();
  $dasignatedsubcategories = Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getdesigall();
  $deoptions  = array();
    if($dasignatedsubcategories){
      foreach($dasignatedsubcategories as $desig){
        $deoptions[$desig['dasignatedsubcategory_id']]  = $desig['dasignatedsubcategory_name']; 
      }
    }
    asort($deoptions, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);

  $this->view->dasignatedsubcategories = $deoptions;
  
  
  $userdcs  = Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategories($user->getIdentity());
  $this->view->userdcs=$userdcs;
  //echo '<pre>';print_r($userdcs); die();
  $udcoptions = array();
    if($userdcs){
      foreach($userdcs as $udsc){
        //echo $udsc['ind_primary_id'];
        $udcoptions[$udsc['dasignatedcategories_id']] = $udsc['dc_primary_id']; 
      }
    }
 
  $this->view->userdc = $udcoptions;
  //// Designation Categories end
    ///// Industry experience start
  $industryexperience = Engine_Api::_()->getDbtable('industryexperiences', 'classified')->fetchAll();
  $ieoptions  = array();
    if($industryexperience){
      foreach($industryexperience as $iexp){
        $ieoptions[$iexp['industryexperience_id']]  = $iexp['industryexperience_name']; 
      }
    }
    //asort($ieoptions, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
  $this->view->industryexperience = $ieoptions;
  
  
  $useriexps  = Engine_Api::_()->getDbtable('industryexperiences', 'user')->userIndustryexperiences($user->getIdentity());
  $this->view->useriexps=$useriexps;
//echo '<pre>';print_r($useriexps);
  $uiexpoptions = array();
    if($useriexps){
      foreach($useriexps as $uiexp){
        //echo $uiexp['ind_primary_id'];
        $uiexpoptions[$uiexp['industryexperiences_id']] = $uiexp['ind_primary_id']; 
      }
    }
  //  print_r($uiexpoptions);
  //die();
  $this->view->useriexp = $uiexpoptions;
  //// Industry experience end

  ///// Functional Areas start
  $functionalareas = Engine_Api::_()->getDbtable('functionalareas', 'classified')->fetchAll();
  $faoptions  = array();
    if($functionalareas){
      foreach($functionalareas as $fa){
        $faoptions[$fa['functionalareas_id']] = $fa['functionalareas_name'];  
      }
    }
    //asort($faoptions, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
  $this->view->functionalareas  = $faoptions;
  
  
  $userfas  = Engine_Api::_()->getDbtable('functionalareas', 'user')->userFunctionalareas($user->getIdentity());
  $this->view->userfas  = $userfas;
//echo '<pre>';print_r($useriexps);
  $ufaoptions = array();
    if($userfas){
      foreach($userfas as $ufa){
        //echo $ufa['ind_primary_id'];
        $ufaoptions[$ufa['functionalareas_id']] = $ufa['fa_primary_id'];  
      }
    }
  //  print_r($uiexpoptions);
  //die();
  $this->view->userfa = $ufaoptions;
  //// Functional Areas end

  ///// skills start

  $skills = Engine_Api::_()->getDbtable('skills', 'classified')->fetchAll();
  $skilloptions = array();
    if($skills){
      foreach($skills as $skill){
        $skilloptions[$skill['skill_id']] = $skill['skill_name']; 
      }
    }
    //asort($faoptions, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
  $this->view->skills = $skilloptions;
  
  
$userskills = Engine_Api::_()->getDbtable('skills', 'user')->userSkills($user->getIdentity());
$this->view->userskills = $userskills;
 //echo '<pre>';print_r($userskills); die();
  $uskilloptions  = array();
    if($userskills){
      foreach($userskills as $uskill){
        //echo $ufa['ind_primary_id'];
        $uskilloptions[$uskill['skill_id']] = $uskill['skill_primary_id'];  
      }
    }
  //  print_r($uiexpoptions);
  //die();
  $this->view->userskill  = $uskilloptions;
  //// skills end
///////////////////
  $usercats = Engine_Api::_()->getDbtable('categories', 'user')->userCategories($user->getIdentity());
  $ucatoptions  = array();
    if($usercats){
      foreach($usercats as $ucat){
        $ucatoptions[$ucat['category_id']]  = $ucat['cat_primary_id'];  
      }
    }
  $this->view->usercat  = $ucatoptions;
  /////////////
	// General form w/o profile type
    $aliasedFields = $user->fields()->getFieldsObjectsByAlias();
    $this->view->topLevelId = $topLevelId = 0;
    $this->view->topLevelValue = $topLevelValue = null;

    if( isset($aliasedFields['profile_type']) ) {
      $aliasedFieldValue = $aliasedFields['profile_type']->getValue($user);
      $topLevelId = $aliasedFields['profile_type']->field_id;
      $topLevelValue = ( is_object($aliasedFieldValue) ? $aliasedFieldValue->value : null );
      if( !$topLevelId || !$topLevelValue ) {
        $topLevelId = null;
        $topLevelValue = null;
      }
      $this->view->topLevelId = $topLevelId;
      $this->view->topLevelValue = $topLevelValue;
    }
    
	 
	
    // Get form
    $form1 = $this->view->form = new User_Form_Standard(array(
      'item' => Engine_Api::_()->core()->getSubject(),
      'topLevelId' => $topLevelId,
      'topLevelValue' => $topLevelValue,
      'hasPrivacy' => true,
      'privacyValues' => $this->getRequest()->getParam('privacy'),
    ));
    //$form->generate();
    $this->view->statusmsg	= "";
	$this->view->country 	= "0";
	$this->view->state 		= "0";
	$this->view->city 		= "0";
	
	 if(!$this->getRequest()->isPost() ) {
      return;
    }
	
	/*$this->view->country = $_POST["1_1_77"];
	$this->view->state = $_POST["1_1_78"];
	$this->view->city = $_POST["1_1_79"];*/
	
	if(!$form1->isValid($this->getRequest()->getPost()) ) {
      return;
    }
    if($this->getRequest()->isPost() && $form1->isValid($this->getRequest()->getPost()) ) {
		
	  $this->view->statusmsg	=	'Your changes have been saved.';
     
	  $formvalues = $form1->getValues();
	  //$year = preg_replace('/\D/', '', $form1->getValue("1_1_431"));
	  //$form1->getElement('1_1_431')->setValue($year);
      $city= trim($form1->getValue('1_1_458'));
    $state= trim($form1->getValue('1_1_460'));
     $citycheck = Engine_Api::_()->getDbtable('cities', 'classified')->getcityname($city);
     if(!$citycheck)
      {
        Engine_Api::_()->getDbtable('cities', 'classified')->insertCity($city,$state);
      }
      $statecheck = Engine_Api::_()->getDbtable('states', 'classified')->getstatename($state);
     if(!$statecheck)
      {
        Engine_Api::_()->getDbtable('states', 'classified')->insertState($state);
      }
      
	  $fname = ucwords(strtolower($form1->getValue('1_1_3')));
	  $mname = ucwords(strtolower($form1->getValue('1_1_66')));
	  $lname = ucwords(strtolower($form1->getValue('1_1_4')));
	  $form1->getElement('1_1_3')->setValue($fname);
	  $form1->getElement('1_1_66')->setValue($mname);
	  $form1->getElement('1_1_4')->setValue($lname);
	  /*echo $fname.$lname;
	  die();*/
	  $form1->saveValues();
	  $formvalues = $form1->getValues();	  	

      // Update display name
      $aliasValues = Engine_Api::_()->fields()->getFieldsValuesByAlias($user);
      $user->setDisplayName($aliasValues);
      //$user->modified_date = date('Y-m-d H:i:s');
      $user->save();
	  
	  if($this->getRe)

      // update networks
      Engine_Api::_()->network()->recalculate($user);
    
    }	
	
	
  }

  public function profileViewed()
  {
    $this->view->user = $user = Engine_Api::_()->core()->getSubject();
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();

    $this->view->passform = $passform = new User_Form_Settings_Password();
	
    // General form w/o profile type
    $aliasedFields = $user->fields()->getFieldsObjectsByAlias();
    $this->view->topLevelId = $topLevelId = 0;
    $this->view->topLevelValue = $topLevelValue = null;
    if( isset($aliasedFields['profile_type']) ) {
      $aliasedFieldValue = $aliasedFields['profile_type']->getValue($user);
      $topLevelId = $aliasedFields['profile_type']->field_id;
      $topLevelValue = ( is_object($aliasedFieldValue) ? $aliasedFieldValue->value : null );
      if( !$topLevelId || !$topLevelValue ) {
        $topLevelId = null;
        $topLevelValue = null;
      }
      $this->view->topLevelId = $topLevelId;
      $this->view->topLevelValue = $topLevelValue;
    }
    
	 
	
    // Get form
    $form1 = $this->view->form1 = new Fields_Form_Standard(array(
      'item' => Engine_Api::_()->core()->getSubject(),
      'topLevelId' => $topLevelId,
      'topLevelValue' => $topLevelValue,
      'hasPrivacy' => true,
      'privacyValues' => $this->getRequest()->getParam('privacy'),
    ));
    //$form->generate();
    
    if( $this->getRequest()->isPost() && $form1->isValid($this->getRequest()->getPost()) ) {
      $form1->saveValues();

      // Update display name
      $aliasValues = Engine_Api::_()->fields()->getFieldsValuesByAlias($user);
      $user->setDisplayName($aliasValues);
      //$user->modified_date = date('Y-m-d H:i:s');
      $user->save();

      // update networks
      Engine_Api::_()->network()->recalculate($user);
      
      $form1->addNotice(Zend_Registry::get('Zend_Translate')->_('Your changes have been saved.'));
    }
	
	// CODE FOR UPDATE PHOTO
	 
	 
    $this->view->user = $user = Engine_Api::_()->core()->getSubject();
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();

    // Get form
    $this->view->form = $form = new User_Form_Edit_Photo();
	
	// Get WallPhoto form
    $this->view->wallform = $wallform = new User_Form_Edit_Wallphoto();

    if( empty($user->photo_id) &&  !$form->isValid($this->getRequest()->getPost())) {
      $form->removeElement('remove');
    }
	
	if( empty($user->wallphoto_id)  &&  !$wallform->isValid($this->getRequest()->getPost())) {
      $wallform->removeElement('remove');
    }

    if( !$this->getRequest()->isPost() ) {
      return;
    }
	
	/*if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }*/

    if( !$form->isValid($this->getRequest()->getPost()) && !$wallform->isValid($this->getRequest()->getPost()) ) {
      return;
    }

//echo 'dsfds'; die;
    // Uploading a new photo
	$imgtype	=	$form->imgtype->getValue(); 
	
    if( $form->Filedata->getValue() !== null ) {
      $db = $user->getTable()->getAdapter();
      $db->beginTransaction();
      
      try {
        $fileElement = $form->Filedata;
        $user->setPhoto($fileElement, 'profile');
        
		
        	$iMain = Engine_Api::_()->getItem('storage_file', $user->photo_id);
			// Insert activity
			$action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($user, $user, 'profile_photo_update', '{item:$subject} added a new profile photo.');
	
			// Hooks to enable albums to work
			if( $action ) {
			  $event = Engine_Hooks_Dispatcher::_()
				->callEvent('onUserProfilePhotoUpload', array(
					'user' => $user,
					'file' => $iMain,
				  ));
	
			  $attachment = $event->getResponse();
			  if( !$attachment ) $attachment = $iMain;
	
			  // We have to attach the user himself w/o album plugin
			  Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $attachment);
			}
			
		
        
        $db->commit();
      }

      // If an exception occurred within the image adapter, it's probably an invalid image
      catch( Engine_Image_Adapter_Exception $e )
      {
        $db->rollBack();
        $form->addError(Zend_Registry::get('Zend_Translate')->_('The uploaded file is not supported or is corrupt.'));
      }

      // Otherwise it's probably a problem with the database or the storage system (just throw it)
      catch( Exception $e )
      {
        $db->rollBack();
        throw $e;
      }
    }
	elseif( $wallform->WallFiledata->getValue() !== null ) {
      $db = $user->getTable()->getAdapter();
      $db->beginTransaction();
      
      try {
        $fileElement = $wallform->WallFiledata;
        $user->setPhoto($fileElement, 'wall');
        
		$iMain = Engine_Api::_()->getItem('storage_file', $user->wallphoto_id);
		
			// Insert activity
			$action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($user, $user, 'wall_profile_photo_update',
			  '{item:$subject} added a new wall photo.');
	
			// Hooks to enable albums to work
			if( $action ) {
			  $event = Engine_Hooks_Dispatcher::_()
				->callEvent('onUserWallProfilePhotoUpload', array(
					'user' => $user,
					'file' => $iMain,
				  ));
	
			  $attachment = $event->getResponse();
			  if( !$attachment ) $attachment = $iMain;
	
			  // We have to attach the user himself w/o album plugin
			  Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $attachment);
			}
		
		
        $db->commit();
      }

      // If an exception occurred within the image adapter, it's probably an invalid image
      catch( Engine_Image_Adapter_Exception $e )
      {
        $db->rollBack();
        $form->addError(Zend_Registry::get('Zend_Translate')->_('The uploaded file is not supported or is corrupt.'));
      }

      // Otherwise it's probably a problem with the database or the storage system (just throw it)
      catch( Exception $e )
      {
        $db->rollBack();

        throw $e;
      }
    }
    // Resizing a photo
    else if( $form->getValue('coordinates') !== '' ) {
      $storage = Engine_Api::_()->storage();

      $iProfile = $storage->get($user->photo_id, 'thumb.profile');
      $iSquare = $storage->get($user->photo_id, 'thumb.icon');
	  
	  $iWallProfile = $storage->get($user->wallphoto_id, 'thumb.wallprofile');
      $iWallSquare = $storage->get($user->wallphoto_id, 'thumb.wallicon');

      // Read into tmp file
      $pName = $iProfile->getStorageService()->temporary($iProfile);
      $iName = dirname($pName) . '/nis_' . basename($pName);
	  
	  $WallpName = $iWallProfile->getStorageService()->temporary($iWallProfile);
      $WalliName = dirname($WallpName) . '/nis_' . basename($WallpName);

      list($x, $y, $w, $h) = explode(':', $form->getValue('coordinates'));

      $image = Engine_Image::factory();
      $image->open($pName)
        ->resample($x+.1, $y+.1, $w-.1, $h-.1, 48, 48)
        ->write($iName)
        ->destroy();

      $iSquare->store($iName);
	  
	  $image->open($WallpName)
        //->resample($x+.1, $y+.1, $w-.1, $h-.1, 48, 48)
        ->write($WalliName)
        ->destroy();

      $iWallSquare->store($WalliName);

      // Remove temp files
      @unlink($iName);
	  @unlink($WalliName);
    }
	$form->Filedata->setValue('');
	$wallform->WallFiledata->setValue('');
	return $this->_helper->redirector->gotoRoute(array('controller' => 'edit', 'action' => 'profile'), 'user_extended', true);
	/*return $this->_helper->redirector->gotoUrl(array('action' => 'profile', 'controller' => 'edit'),  'user_general', array('prependBase' => false));*/
  	
  }

  public function photoAction()
  {
	 
    $this->view->user = $user = Engine_Api::_()->core()->getSubject();
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();

    // Get form
    $this->view->form = $form = new User_Form_Edit_Photo();
	
	// Get WallPhoto form
    $this->view->wallform = $wallform = new User_Form_Edit_Wallphoto();

    if( empty($user->photo_id) &&  !$form->isValid($this->getRequest()->getPost())) {
      $form->removeElement('remove');
    }
	
	if( empty($user->wallphoto_id)  &&  !$wallform->isValid($this->getRequest()->getPost())) {
      $wallform->removeElement('remove');
    }

    if( !$this->getRequest()->isPost() ) {
      return;
    }
	
	/*if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }*/

    if( !$form->isValid($this->getRequest()->getPost()) && !$wallform->isValid($this->getRequest()->getPost()) ) {
      return;
    }

//echo 'dsfds'; die;
    // Uploading a new photo
	$imgtype	=	$form->imgtype->getValue(); 
	
    if( $form->Filedata->getValue() !== null ) {
      $db = $user->getTable()->getAdapter();
      $db->beginTransaction();
      
      try {
        $fileElement = $form->Filedata;
        $user->setPhoto($fileElement, 'profile');
        
		
        	$iMain = Engine_Api::_()->getItem('storage_file', $user->photo_id);
			// Insert activity
			$action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($user, $user, 'profile_photo_update', '{item:$subject} added a new profile photo.');
	
			// Hooks to enable albums to work
			if( $action ) {
			  $event = Engine_Hooks_Dispatcher::_()
				->callEvent('onUserProfilePhotoUpload', array(
					'user' => $user,
					'file' => $iMain,
				  ));
	
			  $attachment = $event->getResponse();
			  if( !$attachment ) $attachment = $iMain;
	
			  // We have to attach the user himself w/o album plugin
			  Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $attachment);
			}
			
		
        
        $db->commit();
      }

      // If an exception occurred within the image adapter, it's probably an invalid image
      catch( Engine_Image_Adapter_Exception $e )
      {
        $db->rollBack();
        $form->addError(Zend_Registry::get('Zend_Translate')->_('The uploaded file is not supported or is corrupt.'));
      }

      // Otherwise it's probably a problem with the database or the storage system (just throw it)
      catch( Exception $e )
      {
        $db->rollBack();
        throw $e;
      }
    }
	elseif( $wallform->WallFiledata->getValue() !== null ) {
      $db = $user->getTable()->getAdapter();
      $db->beginTransaction();
      
      try {
        $fileElement = $wallform->WallFiledata;
        $user->setPhoto($fileElement, 'wall');
        
		$iMain = Engine_Api::_()->getItem('storage_file', $user->wallphoto_id);
		
			// Insert activity
			$action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($user, $user, 'wall_profile_photo_update',
			  '{item:$subject} added a new wall photo.');
	
			// Hooks to enable albums to work
			if( $action ) {
			  $event = Engine_Hooks_Dispatcher::_()
				->callEvent('onUserWallProfilePhotoUpload', array(
					'user' => $user,
					'file' => $iMain,
				  ));
	
			  $attachment = $event->getResponse();
			  if( !$attachment ) $attachment = $iMain;
	
			  // We have to attach the user himself w/o album plugin
			  Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $attachment);
			}
		
		
        $db->commit();
      }

      // If an exception occurred within the image adapter, it's probably an invalid image
      catch( Engine_Image_Adapter_Exception $e )
      {
        $db->rollBack();
        $form->addError(Zend_Registry::get('Zend_Translate')->_('The uploaded file is not supported or is corrupt.'));
      }

      // Otherwise it's probably a problem with the database or the storage system (just throw it)
      catch( Exception $e )
      {
        $db->rollBack();
        throw $e;
      }
    }
    // Resizing a photo
    else if( $form->getValue('coordinates') !== '' ) {
      $storage = Engine_Api::_()->storage();

      $iProfile = $storage->get($user->photo_id, 'thumb.profile');
      $iSquare = $storage->get($user->photo_id, 'thumb.icon');
	  
	  $iWallProfile = $storage->get($user->wallphoto_id, 'thumb.wallprofile');
      $iWallSquare = $storage->get($user->wallphoto_id, 'thumb.wallicon');

      // Read into tmp file
      $pName = $iProfile->getStorageService()->temporary($iProfile);
      $iName = dirname($pName) . '/nis_' . basename($pName);
	  
	  $WallpName = $iWallProfile->getStorageService()->temporary($iWallProfile);
      $WalliName = dirname($WallpName) . '/nis_' . basename($WallpName);

      list($x, $y, $w, $h) = explode(':', $form->getValue('coordinates'));

      $image = Engine_Image::factory();
      $image->open($pName)
        ->resample($x+.1, $y+.1, $w-.1, $h-.1, 48, 48)
        ->write($iName)
        ->destroy();

      $iSquare->store($iName);
	  
	  $image->open($WallpName)
        //->resample($x+.1, $y+.1, $w-.1, $h-.1, 48, 48)
        ->write($WalliName)
        ->destroy();

      $iWallSquare->store($WalliName);

      // Remove temp files
      @unlink($iName);
	  @unlink($WalliName);
    }
	$form->Filedata->setValue('');
	$wallform->WallFiledata->setValue('');
	return $this->_helper->redirector->gotoRoute(array('controller' => 'edit', 'action' => 'photo'), 'user_extended', true);
	/*return $this->_helper->redirector->gotoUrl(array('action' => 'profile', 'controller' => 'edit'),  'user_general', array('prependBase' => false));*/
  }

  public function removePhotoAction()
  {
    // Get form
    $this->view->form = $form = new User_Form_Edit_RemovePhoto();
	
    if( !$this->getRequest()->isPost() || (!$form->isValid($this->getRequest()->getPost())) )
    {
      return;
    }
	
	$user = Engine_Api::_()->core()->getSubject();
	$user->photo_id = 0; 
	//print_r($user);
    $user->save();
    
    $this->view->status = true;
    $this->view->message = Zend_Registry::get('Zend_Translate')->_('Your profile photo has been removed.');

    $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => true,
      'parentRefresh' => true,
      'messages' => array(Zend_Registry::get('Zend_Translate')->_('Your profile photo has been removed.'))
    ));
  }
  
  public function removeWallphotoAction()
  {
    // Get form
	
	$this->view->form = $form = new User_Form_Edit_RemoveWallPhoto();

    if( !$this->getRequest()->isPost() || (!$form->isValid($this->getRequest()->getPost())) )
    {
      return;
    }
	
	$user = Engine_Api::_()->core()->getSubject();
	$user->wallphoto_id = 0; 
    $user->save();
    
    $this->view->status = true;
    $this->view->message = Zend_Registry::get('Zend_Translate')->_('Your wall photo has been removed.');

    $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => true,
      'parentRefresh' => true,
      'messages' => array(Zend_Registry::get('Zend_Translate')->_('Your wall photo has been removed.'))
    ));
  }

  public function styleAction()
  {
    $this->view->user = $user = Engine_Api::_()->core()->getSubject();
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    if( !$this->_helper->requireAuth()->setAuthParams('user', null, 'style')->isValid()) return;


    // Get form
    $this->view->form = $form = new User_Form_Edit_Style();

    // Get current row
    $table = Engine_Api::_()->getDbtable('styles', 'core');
    $select = $table->select()
      ->where('type = ?', $user->getType())
      ->where('id = ?', $user->getIdentity())
      ->limit();

    $row = $table->fetchRow($select);

    // Not posting, populate
    if( !$this->getRequest()->isPost() )
    {
      $form->populate(array(
        'style' => ( null === $row ? '' : $row->style )
      ));
      return;
    }

    // Whoops, form was not valid
    if( !$form->isValid($this->getRequest()->getPost()) )
    {
      return;
    }


    // Cool! Process
    $style = $form->getValue('style');

    // Process
    $style = strip_tags($style);

    $forbiddenStuff = array(
      '-moz-binding',
      'expression',
      'javascript:',
      'behaviour:',
      'vbscript:',
      'mocha:',
      'livescript:',
    );

    $style = str_replace($forbiddenStuff, '', $style);

    // Save
    if( null == $row )
    {
      $row = $table->createRow();
      $row->type = $user->getType();
      $row->id = $user->getIdentity();
    }

    $row->style = $style;
    $row->save();

    $form->addNotice(Zend_Registry::get('Zend_Translate')->_('Your changes have been saved.'));
  }

  public function externalPhotoAction()
  {
    if( !$this->_helper->requireSubject()->isValid() ) return;
    $user = Engine_Api::_()->core()->getSubject();
    
    // Get photo
    $photo = Engine_Api::_()->getItemByGuid($this->_getParam('photo'));
    if( !$photo || !($photo instanceof Core_Model_Item_Abstract) || empty($photo->photo_id) ) {
      $this->_forward('requiresubject', 'error', 'core');
      return;
    }

    if( !$photo->authorization()->isAllowed(null, 'view') ) {
      $this->_forward('requireauth', 'error', 'core');
      return;
    }

    
    // Make form
    $this->view->form = $form = new User_Form_Edit_ExternalPhoto();
    $this->view->photo = $photo;

    if( !$this->getRequest()->isPost() ) {
      return;
    }

    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }

    // Process
    $db = $user->getTable()->getAdapter();
    $db->beginTransaction();
    
    try {
      // Get the owner of the photo
      $photoOwnerId = null;
      if( isset($photo->user_id) ) {
        $photoOwnerId = $photo->user_id;
      } else if( isset($photo->owner_id) && (!isset($photo->owner_type) || $photo->owner_type == 'user') ) {
        $photoOwnerId = $photo->owner_id;
      }

      // if it is from your own profile album do not make copies of the image
      if( $photo instanceof Album_Model_Photo &&
          ($photoParent = $photo->getParent()) instanceof Album_Model_Album &&
          $photoParent->owner_id == $photoOwnerId &&
          $photoParent->type == 'profile' ) {

        // ensure thumb.icon and thumb.profile exist
        $newStorageFile = Engine_Api::_()->getItem('storage_file', $photo->file_id);
        $filesTable = Engine_Api::_()->getDbtable('files', 'storage');
        if( $photo->file_id == $filesTable->lookupFile($photo->file_id, 'thumb.profile') ) {
          try {
            $tmpFile = $newStorageFile->temporary();
            $image = Engine_Image::factory();
            $image->open($tmpFile)
              ->resize(200, 400)
              ->write($tmpFile)
              ->destroy();
            $iProfile = $filesTable->createFile($tmpFile, array(
              'parent_type' => $user->getType(),
              'parent_id' => $user->getIdentity(),
              'user_id' => $user->getIdentity(),
              'name' => basename($tmpFile),
            ));
            $newStorageFile->bridge($iProfile, 'thumb.profile');
            @unlink($tmpFile);
          } catch( Exception $e ) { echo $e; die(); }
        }
        if( $photo->file_id == $filesTable->lookupFile($photo->file_id, 'thumb.icon') ) {
          try {
            $tmpFile = $newStorageFile->temporary();
            $image = Engine_Image::factory();
            $image->open($tmpFile);
            $size = min($image->height, $image->width);
            $x = ($image->width - $size) / 2;
            $y = ($image->height - $size) / 2;
            $image->resample($x, $y, $size, $size, 48, 48)
              ->write($tmpFile)
              ->destroy();
            $iSquare = $filesTable->createFile($tmpFile, array(
              'parent_type' => $user->getType(),
              'parent_id' => $user->getIdentity(),
              'user_id' => $user->getIdentity(),
              'name' => basename($tmpFile),
            ));
            $newStorageFile->bridge($iSquare, 'thumb.icon');
            @unlink($tmpFile);
          } catch( Exception $e ) { echo $e; die(); }
        }

        // Set it
        $user->photo_id = $photo->file_id;
        $user->save();
        
        // Insert activity
        // @todo maybe it should read "changed their profile photo" ?
        $action = Engine_Api::_()->getDbtable('actions', 'activity')
            ->addActivity($user, $user, 'profile_photo_update',
                '{item:$subject} changed their profile photo.');
        if( $action ) {
          // We have to attach the user himself w/o album plugin
          Engine_Api::_()->getDbtable('actions', 'activity')
              ->attachActivity($action, $photo);
        }
      }

      // Otherwise copy to the profile album
      else {
        $user->setPhoto($photo);

        // Insert activity
        $action = Engine_Api::_()->getDbtable('actions', 'activity')
            ->addActivity($user, $user, 'profile_photo_update',
                '{item:$subject} added a new profile photo.');
        
        // Hooks to enable albums to work
        $newStorageFile = Engine_Api::_()->getItem('storage_file', $user->photo_id);
        $event = Engine_Hooks_Dispatcher::_()
          ->callEvent('onUserProfilePhotoUpload', array(
              'user' => $user,
              'file' => $newStorageFile,
            ));

        $attachment = $event->getResponse();
        if( !$attachment ) {
          $attachment = $newStorageFile;
        }
        
        if( $action  ) {
          // We have to attach the user himself w/o album plugin
          Engine_Api::_()->getDbtable('actions', 'activity')
              ->attachActivity($action, $attachment);
        }
      }

      $db->commit();
    }

    // Otherwise it's probably a problem with the database or the storage system (just throw it)
    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }
    
    return $this->_forward('success', 'utility', 'core', array(
      'messages' => array(Zend_Registry::get('Zend_Translate')->_('Set as profile photo')),
      'smoothboxClose' => true,
    ));
  }

  public function clearStatusAction()
  {
    $this->view->status = false;
    
    if( $this->getRequest()->isPost() ) {
      $viewer = Engine_Api::_()->user()->getViewer();
      $viewer->status = '';
      $viewer->status_date = '00-00-0000';
      // twitter-style handling
      // $lastStatus = $viewer->status()->getLastStatus();
      // if( $lastStatus ) {
      //   $viewer->status = $lastStatus->body;
      //   $viewer->status_date = $lastStatus->creation_date;
      // }
      $viewer->save();
      
      $this->view->status = true;
    } 
  }
   public function passwordAction()
  {
	   
    $user = Engine_Api::_()->core()->getSubject();

    $this->view->form = $form = new User_Form_Settings_Password();
    $form->populate($user->toArray());

    if( !$this->getRequest()->isPost() ){
      return;
    }
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }

    // Check conf
    if( $form->getValue('passwordConfirm') !== $form->getValue('password') ) {
      $form->getElement('passwordConfirm')->addError(Zend_Registry::get('Zend_Translate')->_('Passwords did not match'));
      return;
    }
    
    // Process form
    $userTable = Engine_Api::_()->getItemTable('user');
    $db = $userTable->getAdapter();

    // Check old password
    $salt = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.secret', 'staticSalt');
    $select = $userTable->select()
      ->from($userTable, new Zend_Db_Expr('TRUE'))
      ->where('user_id = ?', $user->getIdentity())
      ->where('password = ?', new Zend_Db_Expr(sprintf('MD5(CONCAT(%s, %s, salt))', $db->quote($salt), $db->quote($form->getValue('oldPassword')))))
      ->limit(1)
      ;
    $valid = $select
      ->query()
      ->fetchColumn()
      ;

    if( !$valid ) {
      $form->getElement('oldPassword')->addError(Zend_Registry::get('Zend_Translate')->_('Old password did not match'));
      return;
    }

    
    // Save
    $db->beginTransaction();

    try {

      $user->setFromArray($form->getValues());
      $user->save();
      
      $db->commit();
    } catch( Exception $e ) {
      $db->rollBack();
      throw $e;
    }

    $form->addNotice(Zend_Registry::get('Zend_Translate')->_('Password is successfully changed.'));
	
  }
  
  public function chkoldpassAction(){
	$userTable = Engine_Api::_()->getItemTable('user');
    $db = $userTable->getAdapter();
	
	$values['oldPassword'] 	= $_POST['oldPassword'];
	$values['password'] 	= $_POST['password'];
	$values['passwordConfirm'] = $_POST['passwordConfirm'];
	
	
	
	$salt = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.secret', 'staticSalt');
    $select = $userTable->select()
      ->from($userTable, new Zend_Db_Expr('TRUE'))
      ->where('user_id = ?', Engine_Api::_()->user()->getViewer()->getIdentity())
      ->where('password = ?', new Zend_Db_Expr(sprintf('MD5(CONCAT(%s, %s, salt))', $db->quote($salt), $db->quote($_POST['oldPassword']))))
      ->limit(1)
      ;
    $valid = $select
      ->query()
      ->fetchColumn()
      ; 
	if( !$valid ) {
		echo 'invalid'; die;
	}else{
		$user = Engine_Api::_()->core()->getSubject();
		$user->setFromArray($values);
    	$user->save();
		die;
	}
	die;
 }
 
 public function reviewAction(){
	
		$this -> view -> viewer = $viewer = Engine_Api::_() -> user() -> getViewer();
		if (!Engine_Api::_()->core()->hasSubject()) {
            return $this->setNoRender();
        }
		$this->view->user = $resource = $user = Engine_Api::_()->core()->getSubject();
        $values = $this->_getAllParams();
		
		/*$reviewTbl = Engine_Api::_()->getItemTable("review");
		if($viewer->level_id != 2){
			$params = array(
				'user_id' => $resource->getIdentity()
			);
		}
		if (!empty($values['itemCountPerPage']))
		{
			$params['limit'] = $values['itemCountPerPage'];
		}
		
		if (!empty($values['page']))
		{
			$params['page'] = $values['page'];
		}
		$this -> view -> reviews = $reviews = $reviewTbl->getReviewPaginator($params);*/
	  	$this->view->page = 1;
		$page = (int)  $this->_getParam('page', 1);
		
		$reviewTable = Engine_Api::_()->getItemTable('review');
    	$reviewTableName = $reviewTable->info('name');
		
		$selectreview = $reviewTable->select()
		  ->from($reviewTableName)
		  ->where("{$reviewTableName}.is_deleted = ?", 0)
		  ->where("{$reviewTableName}.user_id = ?", $resource->getIdentity())
		  ->order("{$reviewTableName}.review_date DESC");
		
		
		$reviewpaginator = Zend_Paginator::factory($selectreview);
		$reviewpaginator->setItemCountPerPage(20);
		$reviewpaginator->setCurrentPageNumber($page);
		
		$this->view->reviews = $reviewpaginator;
		$this->view->totalReviews = $reviewpaginator->getTotalItemCount();
		$this->view->reviewCount = $reviewpaginator->getCurrentItemCount();
			
		
		$this -> view ->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
	 
	}
	
	public function delrevAction(){
		$type = $this->_getParam('item_type');  
		if($this->getRequest()->isPost()){
			if($type == 'rev'){
				$delitem	=	'review';
				$this->_helper->layout()->disableLayout(); 
				$review_id	=	base64_decode($this->_getParam('rid'));
				$review		=	Engine_Api::_()->getItem('review', $review_id);
				$review->is_deleted	=	1;
				$review->save();
			}elseif($type == 'com'){
				$delitem	=	'comment';
				$this->_helper->layout()->disableLayout(); 
				$review_id	=	base64_decode($this->_getParam('rid'));
				$review	=	Engine_Api::_()->getItem('review', $review_id);
				$review->user_comment	=	'';
				$review->save();
			}
			
			$this->_forward('success', 'utility', 'core', array(
			  'smoothboxClose' => true,
			  'parentRefresh' => true,
			  'messages' => array(Zend_Registry::get('Zend_Translate')->_("Your $delitem has been deleted."))
			));
		}else{
			$this->view->type = $this->_getParam('item_type');
			$this->view->rid = $this->_getParam('rid');
		}
  }
	
	public function postcommentAction(){
		$review_id	=	$_POST['review_id'];
		$comment	=	$_POST['review_comment'];
		$review	=	Engine_Api::_()->getItem('review', $review_id);
		$review->user_comment	=	$comment;
		$review->save();
		die;	
	}
	
	public  function changereviewstatusAction(){
		$review_id	=	$_POST['review_id'];
		$status		=	$_POST['status'];
		$type		=	$_POST['type'];
		
		$review	=	Engine_Api::_()->getItem('review', $review_id);
		if($type == 'is_approved'){
			$review->is_approved	=	$status;
			if($status == '0'){
				
				echo "<a href='javascript:void(0)' onclick='change_review_status(1 ,\"$type\",\"$review_id\")' class='deactive_bt publish' style='cursor:pointer;font-weight: normal;'>Publish</a>";
			} else{
				echo "<a href='javascript:void(0)' onclick='change_review_status(0 ,\"$type\",\"$review_id\")' class='deactive_bt unpublish' style='cursor:pointer;font-weight: normal;'>Un-publish</a>";
			}
		}
		if($type == 'is_deleted'){
			$review->is_deleted	=	1;	
		}
		
		$review->save();
		die;
	}
	
	public  function deletecommentAction(){
		$review_id	=	$_POST['review_id'];
		$review	=	Engine_Api::_()->getItem('review', $review_id);
		$review->user_comment	=	'';
		$review->save();
		die;
	}
	
	public function messagesettingsAction(){
		$setting =	$_POST['setting'];
		$type	 =	$_POST['type'];
		$user	 =	Engine_Api::_()->user()->getViewer();
		if($type == 'msg_setting'){
			$user->msg_setting		=	$setting;
		}else if($type == 'friend_setting'){
			$user->friend_setting	=	$setting;
		}else if($type == 'cal_setting'){
			$user->cal_setting	=	$setting;
		}
		$user->save();
		die;
	}
	
	function msort($array, $key, $sort_flags = SORT_REGULAR) {
		if (is_array($array) && count($array) > 0) {
			if (!empty($key)) {
				$mapping = array();
				foreach ($array as $k => $v) {
					$sort_key = '';
					if (!is_array($key)) {
						$sort_key = $v[$key];
					} else {
						// @TODO This should be fixed, now it will be sorted as string
						foreach ($key as $key_key) {
							$sort_key .= $v[$key_key];
						}
						$sort_flags = SORT_STRING;
					}
					$mapping[$k] = $sort_key;
				}
				asort($mapping, $sort_flags);
				$sorted = array();
				foreach ($mapping as $k => $v) {
					$sorted[] = $array[$k];
				}
				return $sorted;
			}
		}
		return $array;
	}
	
	public function addendorseAction(){
		$this->_helper->layout->disableLayout();
		
		$type     = $_POST['type'];
		if($type == 'add'){
			$skill_id = $_POST['skill_id'];
			$owner_id = $_POST['owner_id'];
			$user_id  = $_POST['user_id'];
			$tableendorse = Engine_Api::_()->getDbTable('endorsements', 'user');
			$endorse = $tableendorse->createRow();
			$endorse->skill_id		=	$skill_id;
			$endorse->owner_id		=	$owner_id;
			$endorse->created_on	=	date('Y-m-d H:i:s', time());
			$endorse->user_id		=	$user_id;
			$endorse->save();	
		}else{
			$skill_id = $_POST['skill_id'];
			$owner_id = $_POST['owner_id'];
			$endorse_id     = $_POST['endorse_id'];
			$endorse	=	Engine_Api::_()->getItem('endorsement', $endorse_id);
			$endorse->is_deleted	=	1;
			$endorse->save();	
		}
		$endorese_by	=	Engine_Api::_()->getDbtable('endorsements', 'user')->getskillendorseby($_POST['skill_id']);
		echo Engine_Api::_()->getDbtable('endorsements', 'user')->getendorsecount($owner_id, $skill_id).'*'.$endorse->endorsement_id.'*'.$_POST['skill_id'].'*'.$endorese_by;
		die;
	}
	
	public function getendorsedbyAction(){
			$this->_helper->layout->disableLayout();
			$this->view->endorse_by	=	explode(',',$this->_getParam('endorse_by'));
	}
	
	public function suggestskillAction(){
		$skill = $_POST['value'];
		
		$query ="select * from engine4_classified_skills where skill_name like '".$skill."%'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$stmt = $db->query($query);
        $result = $stmt->fetchAll();
		$data	=	array();
		if($result){
			foreach( $result as $skil ) {
				$data[] = array(
				  'type'  => 'user',
				  'label'	=>	$skil['skill_name'],
				  'title'    =>  $skil['skill_name'],
				);
			 }	
		}
		return $this->_helper->json($data);
	}
	
	/* CUSTOM FUNCTION TO UPLOAD USER REELS */
  	public function adduserreelAction()
	{
	$output_dir = "public/temporary";
	 
	if(isset($_FILES["myfile"]))
		{
		$ret = array();
		
		$error =$_FILES["myfile"]["error"];
		{
		
		if(!is_array($_FILES["myfile"]['name'])) //single file
		{
			$fileName = $_FILES["myfile"]["name"];
			$fileExt=end(explode(".",$fileName));
			$unique_key = substr(md5(rand(0, 10000000)), 0, 7);
 			$fileName=time()."".$unique_key.".".$fileExt;
 			// echo '<pre>'; print_r($_FILES); echo '</pre>'; die;
			move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $fileName);
			 //echo "<br> Error: ".$_FILES["myfile"]["error"];
			$ret=$fileName;
		}
		else
		{
			$fileCount = count($_FILES["myfile"]['name']);
			  for($i=0; $i < $fileCount; $i++)
			  {
				$fileName = $_FILES["myfile"]["name"][$i];
				$fileName = $_FILES["myfile"]["name"][$i];
			    $fileExt=end(explode(".",$fileName));
				$unique_key = substr(md5(rand(0, 10000000)), 0, 7);
 			    $fileName=time()."".$unique_key.".".$fileExt;
				$ret=$fileName;
				move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName );
			  }
		
		}
		}
		echo $ret;
 		die();
 		}
		die;
 	}
	
	public function saveuserreelsAction(){
		/* TO REMOVE REELS */
		$remove_reel_ids	=	explode(',',$values['remove_reel_ids']);
		if($remove_reel_ids){
		  foreach($remove_reel_ids as $rel){
			  if($rel){
				$reel	=	Engine_Api::_()->getItem('userreel', $rel);
				$reel->is_deleted	=	1;
				$reel->save();
			  }
		  }
		}
		
		
		$files_name=$_POST['file_name'];
		$files_name=explode("||",$files_name);
		 
		 if(!empty($files_name)){
			foreach($files_name as $key=> $val)
			{
				if($val!="")
				{
					rename("public/temporary/".$val."", "public/user/reels/".$val."");
					$tablereel = Engine_Api::_()->getDbTable('userreels', 'user');
					$reel = $tablereel->createRow();
					$reel->reel_path		=	$val;
					$reel->user_id			=	Engine_Api::_()->user()->getViewer()->getIdentity();
					$reel->created_on		=	date('Y-m-d H:i:s', time());
					$reel->save();
				}
			}
		}
		return $this->_helper->redirector->gotoRoute(array('controller' => 'edit', 'action' => 'profile'), 'user_extended', true);
		
	}
   public function getuserindustriesAction(){
 	$this->view->user = $user = Engine_Api::_()->core()->getSubject();
 	$userinds	=	Engine_Api::_()->getDbtable('industryexperiences', 'user')->userIndustryexperiences($user->getIdentity());
	$uindoptions	=	array();
	$uindoptions1	=	array();
		if($userinds){
			foreach($userinds as $uin){
			$uindoptions[$uin['industryexperiences_id']]	=	$uin['ind_primary_id'];
			$uindoptions1[$uin['industryexperiences_id']]	=	$uin['industryexperience_title'];	
			}
		}
		$userind= implode(',', $uindoptions);
		$userind1= implode(',', $uindoptions1);
	
	echo json_encode(array('ids' => $userind,"titles" => $userind1));

	die();

 }
  public function getuserfunctionalareasAction(){
 	$this->view->user = $user = Engine_Api::_()->core()->getSubject();
 	$userfas	=	Engine_Api::_()->getDbtable('functionalareas', 'user')->userFunctionalareas($user->getIdentity());
	$ufaoptions	=	array();
	$ufaoptions1	=	array();
		if($userfas){
			foreach($userfas as $uin){
			$ufaoptions[$ufa['functionalareas_id']]	=	$ufa['fa_primary_id'];
			$ufaoptions1[$ufa['functionalareas_id']]	=	$ufa['functionalareas_title'];	
			}
		}
		$userfa= implode(',', $ufaoptions);
		$userfa1= implode(',', $ufaoptions1);
	
	echo json_encode(array('ids' => $userfa,"titles" => $userfa1));

	die();

 }

  public function getuserskillsAction(){
 	$this->view->user = $user = Engine_Api::_()->core()->getSubject();
 	$userskills	=	Engine_Api::_()->getDbtable('skills', 'user')->userskills($user->getIdentity());
	$uskilloptions	=	array();
	$uskilloptions1	=	array();
		if($userskills){
			foreach($userskills as $uskill){
			$uskilloptions[$uskill['skill_id']]	=	$uskill['skill_primary_id'];
			$uskilloptions1[$uskill['skill_id']]	=	$uskill['skill_title'];	
			}
		}
		$userskill= implode(',', $uskilloptions);
		$userskill1= implode(',', $uskilloptions1);
	
	echo json_encode(array('ids' => $userskill,"titles" => $userskill1));

	die();

 }

 public function addremovefunctionalareaAction(){
 	$this->view->user = $user = Engine_Api::_()->core()->getSubject();
 	
 	$status='0';
 	$funcsids=trim($_POST['funcsids']);
 	$val=trim($_POST['val']);

		if($val==1)
		{
			if($funcsids){
				
			$mfas	=	Engine_Api::_()->getDbtable('functionalareas', 'classified')
				->getfaById($funcsids);
				$check_user	=	Engine_Api::_()->getDbtable('functionalareas', 'user')
				->checkifexist($funcsids,$user->getIdentity());
				
				if($check_user)
				{
					$status="Already Added!";
				}
				else{
					$tablecat = Engine_Api::_()->getDbTable('functionalareas', 'user');
					$functionalareas = $tablecat->createRow();
					$functionalareas->functionalareas_title	=	$mfas[0]['functionalareas_name'];
					$functionalareas->fa_primary_id	=	$funcsids;
					$functionalareas->created_on		=	date('Y-m-d H:i:s', time());
					$functionalareas->user_id			=	$user->getIdentity();
					$functionalareas->save();	
					$status="Successfully added!";
					}
			}
		}
		else
		{
			$delstaus=	Engine_Api::_()->getDbtable('functionalareas', 'user')
				->deletebyid($funcsids, $user->getIdentity());
				if($delstaus==1)
				{
					$status="Successfully removed!";	
				}
				else
				{
					$status="Error!";
				}
		}
		echo json_encode(array('status' => $status));
		die();	

 	}

  public function addremovesdesigAction(){
    $this->view->user = $user = Engine_Api::_()->core()->getSubject();
    $userdesigs = Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategories($user->getIdentity());
    $status='';
    $desig_ids  = $_POST['desig_ids'];
    if($desig_ids){
       //print_r($_POST['desig_ids']);
      
      $userdesoption = array();
      if($userdesigs){
        foreach($userdesigs as $des){
          $userdesoption[$des['dasignatedcategories_id']]  = $des['dc_primary_id']; 
        }
      }
       if($userdesoption){
      
        
        foreach($userdesoption as $did){
         
          if($did && !in_array($did, $desig_ids)){
            $des_id  = Engine_Api::_()->getDbtable('dasignatedcategories', 'user')
        ->getuserdcbydcprimaryid($did, $user->getIdentity());
          if($des_id){              
              $user_des = Engine_Api::_()->getDbTable('dasignatedcategories', 'user');
              $condition = array(
                'dc_primary_id = ?' => $did,
                'user_id = ?' => $user->getIdentity()
              );
              
              $user_des->delete($condition);
              $status="Successfully Changed!";
            }
          }
        } 
      }
      if($desig_ids){
        foreach($desig_ids as $dc){
          if(!in_array($dc, $userdesoption) && $dc){
              $mdes = Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')
        ->getdscById($dc);
              $tabledes = Engine_Api::_()->getDbTable('dasignatedcategories', 'user');
              $dasignatedcategory = $tabledes->createRow();
              $dasignatedcategory->dasignatedcategory_title  = $mdes[0]['dasignatedsubcategory_name'];
              $dasignatedcategory->dc_primary_id  = $dc;
              $dasignatedcategory->created_on    = date('Y-m-d H:i:s', time());
              $dasignatedcategory->user_id     = $user->getIdentity();
              $dasignatedcategory->save(); 
              $status="Successfully added!";
          }
        } 
      }
     
    //die;  
    }
 
    echo json_encode(array('status' => $status));
    die();  

  }
  public function addremovesdomexpAction(){
    $this->view->user = $user = Engine_Api::_()->core()->getSubject();
    $userexps = Engine_Api::_()->getDbtable('industryexperiences', 'user')->userIndustryexperiences($user->getIdentity());
    $status='';
    $exp_ids  = $_POST['exp_ids'];
    if($exp_ids){
       //print_r($_POST['exp_ids']);
      
      $userexpoptions = array();
      if($userexps){
        foreach($userexps as $exp){
          $userexpoptions[$exp['industryexperiences_id']]  = $exp['ind_primary_id']; 
        }
      }
       if($userexpoptions){
      
        
        foreach($userexpoptions as $eid){
         
          if($eid && !in_array($eid, $exp_ids)){
            $uexp_id  = Engine_Api::_()->getDbtable('industryexperiences', 'user')
        ->getuserexpbyiexpprimaryid($eid, $user->getIdentity());
          if($uexp_id){              
              $user_exps = Engine_Api::_()->getDbTable('industryexperiences', 'user');
              $condition = array(
                'ind_primary_id = ?' => $eid,
                'user_id = ?' => $user->getIdentity()
              );
              
              $user_exps->delete($condition);
              $status="Successfully Changed!";
            }
          }
        } 
      }
      if($exp_ids){
        foreach($exp_ids as $exp){
          if(!in_array($exp, $userexpoptions) && $exp){
              $mexp = Engine_Api::_()->getDbtable('industryexperiences', 'classified')
        ->getinduById($exp);
              $tableexp = Engine_Api::_()->getDbTable('industryexperiences', 'user');
              $experience = $tableexp->createRow();
              $experience->industryexperience_title  = $mexp[0]['industryexperience_name'];
              $experience->ind_primary_id  = $exp;
              $experience->created_on    = date('Y-m-d H:i:s', time());
              $experience->user_id     = $user->getIdentity();
              $experience->save(); 
              $status="Successfully added!";
          }
        } 
      }
     
    //die;  
    }
 
    echo json_encode(array('status' => $status));
    die();  

  }
  public function addremovesfunexpAction(){
    $this->view->user = $user = Engine_Api::_()->core()->getSubject();
    $userfuns = Engine_Api::_()->getDbtable('functionalareas', 'user')->userFunctionalareas($user->getIdentity());
    $status='';
    $fun_ids  = $_POST['func_ids'];
    if($fun_ids){
       //print_r($_POST['fun_ids']);
      
      $userfunoptions = array();
      if($userfuns){
        foreach($userfuns as $fun){
          $userfunoptions[$fun['functionalareas_id']]  = $fun['fa_primary_id']; 
        }
      }
       if($userfunoptions){        
        foreach($userfunoptions as $fid){
         
          if($fid && !in_array($fid, $fun_ids)){
            $ufun_id  = Engine_Api::_()->getDbtable('functionalareas', 'user')
        ->getuserfabyfaprimaryid($fid, $user->getIdentity());
          if($ufun_id){              
              $user_funs = Engine_Api::_()->getDbTable('functionalareas', 'user');
              $condition = array(
                'fa_primary_id = ?' => $fid,
                'user_id = ?' => $user->getIdentity()
              );
              
              $user_funs->delete($condition);
              $status="Successfully Changed!";
            }
          }
        } 
      }
      if($fun_ids){
        foreach($fun_ids as $funs){
          if(!in_array($funs, $userfunoptions) && $funs){
              $mfun = Engine_Api::_()->getDbtable('functionalareas', 'classified')
        ->getfaById($funs);
              $tablefun = Engine_Api::_()->getDbTable('functionalareas', 'user');
              $functionalareas = $tablefun->createRow();
              $functionalareas->functionalareas_title  = $mfun[0]['functionalareas_name'];
              $functionalareas->fa_primary_id  = $funs;
              $functionalareas->created_on    = date('Y-m-d H:i:s', time());
              $functionalareas->user_id     = $user->getIdentity();
              $functionalareas->save(); 
              $status="Successfully added!";
          }
        } 
      }
     
    //die;  
    }
 
    echo json_encode(array('status' => $status));
    die();  

  }
 	public function addremoveskillAction(){
   	$this->view->user = $user = Engine_Api::_()->core()->getSubject();
   	$userSkills = Engine_Api::_()->getDbtable('skills', 'user')->userSkills($user->getIdentity());
   	$status='';
    $skill_ids  = $_POST['skill_ids'];
    if($skill_ids){
       //print_r($_POST['skill_ids']);
      
      $userskilloptions = array();
      if($userSkills){
        foreach($userSkills as $skill){
          $userskilloptions[$skill['skill_id']]  = $skill['skill_primary_id']; 
        }
      }
       if($userskilloptions){
      
        
        foreach($userskilloptions as $sid){
         
          if($sid && !in_array($sid, $skill_ids)){
            $uskl_id  = Engine_Api::_()->getDbtable('skills', 'user')
        ->getuserskillbyskilprimaryid($sid, $user->getIdentity());
          if($uskl_id){              
              $user_Skills = Engine_Api::_()->getDbTable('skills', 'user');
              $condition = array(
                'skill_primary_id = ?' => $sid,
                'user_id = ?' => $user->getIdentity()
              );
              
              $user_Skills->delete($condition);
              $status="Successfully Changed!";
            }
          }
        } 
      }
      if($skill_ids){
        foreach($skill_ids as $skl){
          if(!in_array($skl, $userskilloptions) && $skl){
              $mskl = Engine_Api::_()->getDbtable('skills', 'classified')
        ->getSkillById($skl);
              $tableskl = Engine_Api::_()->getDbTable('skills', 'user');
              $skill = $tableskl->createRow();
              $skill->skill_title  = $mskl[0]['skill_name'];
              $skill->skill_primary_id  = $skl;
              $skill->created_on    = date('Y-m-d H:i:s', time());
              $skill->user_id     = $user->getIdentity();
              $skill->save(); 
              $status="Successfully added!";
          }
        } 
      }
     
    //die;  
    }
 /*	$skillid=trim($_POST['skillid']);
 	$val=trim($_POST['val']);

		if($val==1)
		{
			if($skillid){
				
			$mskills	=	Engine_Api::_()->getDbtable('skills', 'classified')
				->getskillById($skillid);
				$check_user	=	Engine_Api::_()->getDbtable('skills', 'user')
				->checkifexist($skillid,$user->getIdentity());
				
				if($check_user)
				{
					$status="Already Added!";
				}
				else{
					$tableskill = Engine_Api::_()->getDbTable('skills', 'user');
					$skills = $tableskill->createRow();
					$skills->skill_title	=	$mskills[0]['skill_name'];
					$skills->skill_primary_id	=	$skillid;
					$skills->created_on		=	date('Y-m-d H:i:s', time());
					$skills->user_id			=	$user->getIdentity();
					$skills->save();	
					$status="Successfully added!";
					}
			}
		}
		else
		{
			$delstaus=	Engine_Api::_()->getDbtable('skills', 'user')
				->deletebyid($skillid, $user->getIdentity());
				if($delstaus==1)
				{
					$status="Successfully removed!";	
				}
				else
				{
					$status="Error!";
				}
		}*/
		echo json_encode(array('status' => $status));
		die();	

 	}

  public function contactAction()
  {
    $this->view->user = $user = Engine_Api::_()->core()->getSubject();
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    //$form1 = $this->view->form = new User_Form_Edit_Contact();
    $viewer_id=$viewer->getIdentity();
    $countryTable = Engine_Api::_()->getDbtable('countries', 'user');
        $countries = $countryTable->select()
              ->from($countryTable, array('country_name', 'countryid'))
              ->query()
              ->fetchAll();
       // print_r($countries);
        $countriesOptions = array('' => 'Select Country');
        foreach( $countries as $country ) {
          $countriesOptions[$country['countryid']] = $country['country_name'];
        }

        $this->view->countriesOptions=$countriesOptions;

    /*if(!$form1->isValid($this->getRequest()->getPost()) ) {
      return;
    }*/
     /*echo '<pre>';
      print_r($_POST);
      die();*/
    if($_POST['submitcontact'])
    {
      /*echo '<pre>';
      print_r($_POST);
      die();
*/

    $values['houseno']   = $_POST['houseno'];
    $values['street']    = $_POST['street'];
    $values['landmark']  = $_POST['landmark'];
    $values['city']      = $_POST['city'];
    $values['state']     = $_POST['state'];
    $values['pincode']   = $_POST['pincode'];
    $values['country']    = $_POST['country'];
    $values['countrycodehome']  = $_POST['countrycodehome'];
    $values['contactnohome']      = $_POST['contactnohome'];
    $values['countrycodeoffice']    = $_POST['countrycodeoffice'];
    $values['contactnooffice']   = $_POST['contactnooffice'];
    $values['linkedin']    = $_POST['linkedin'];
    $values['twittera']  = $_POST['twittera'];
    $values['instagram']      = $_POST['instagram'];
    $values['google']    = $_POST['google'];
    $values['facebooka']      = $_POST['facebooka'];
    $values['website']    = $_POST['website'];
    $values['contactinfos_id']   = $_POST['coninfo_id'];
    $values['owner_id']   = $viewer_id;
    
    //echo '<pre>';print_r($values);die();
    if($values['contactinfos_id']){
      //echo "id".$values['contactinfos_id'];
      Engine_Api::_()->getDbtable('contactinfos', 'user')->updatecontactinfos($values,$viewer_id);
      //$pro = Engine_Api::_()->getItem('contactinfos', $values['contactinfos_id']);
    }else{
      
      $pro = Engine_Api::_()->getDbtable('contactinfos', 'user')->createRow();
      $pro->setFromArray($values);
      $pro->save();
    }
    
   
    }

    $contable = Engine_Api::_()->getDbTable('contactinfos', 'user')->userContactinfos($viewer_id);
    $this->view->contactinfos=$contable;
  /*  $contable = Engine_Api::_()->getDbTable('contactinfos', 'user');
    $conName = $contable->info('name'); 
    $conselect = $usertable->select("$conName.*")
            ->where("$conName.owner_id = $viewer_id", 1)
            ->where("$conName.row_status = 1", 1);

    $contactinfos = $contable->fetchAll($conselect);*/
   /* echo '<pre>';
    print_r($contable);
    die();*/
  }

  public function keyfieldsAction()
  {
    $this->view->user = $user = Engine_Api::_()->core()->getSubject();
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    //$this->view->useridentity= $user->getIdentity();

    $this->view->passform = $passform = new User_Form_Settings_Password();
    
  //////////////////////// Current status Start
  $cur_status = Engine_Api::_()->getDbtable('currentstatus', 'classified')->fetchAll();
  $cur_statusopt  = array();
    if($cur_status){
      foreach($cur_status as $cur){
        $cur_statusopt[$cur['currentstatus_id']]  = $cur['currentstatus_name']; 
      }
    }
    //asort($cur_statusopt, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
  $this->view->cur_status = $cur_statusopt;

  $usercur  = Engine_Api::_()->getDbtable('currentstatus', 'user')->userCurrentstatus($user->getIdentity());
  $this->view->curstatus=$usercur;
  
  $ucuroption ='';
    if($usercur){
        $ucuroption = $usercur['cur_primary_id']; 
    }
  $this->view->usercur  = $ucuroption;
  //////////////////////// Current Status End

    ///// Industry experience start
  $industryexperience = Engine_Api::_()->getDbtable('industryexperiences', 'classified')->fetchAll();
  $ieoptions  = array();
    if($industryexperience){
      foreach($industryexperience as $iexp){
        $ieoptions[$iexp['industryexperience_id']]  = $iexp['industryexperience_name']; 
      }
    }
    //asort($ieoptions, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
  $this->view->industryexperience = $ieoptions;
  
  
  $useriexps  = Engine_Api::_()->getDbtable('industryexperiences', 'user')->userIndustryexperiences($user->getIdentity());
  $this->view->useriexps=$useriexps;
//echo '<pre>';print_r($useriexps);
  $uiexpoptions = array();
    if($useriexps){
      foreach($useriexps as $uiexp){
        //echo $uiexp['ind_primary_id'];
        $uiexpoptions[$uiexp['industryexperiences_id']] = $uiexp['ind_primary_id']; 
      }
    }
  //  print_r($uiexpoptions);
  //die();
  $this->view->useriexp = $uiexpoptions;
  //// Industry experience end

  ///// Functional Areas start
  $functionalareas = Engine_Api::_()->getDbtable('functionalareas', 'classified')->fetchAll();
  $faoptions  = array();
    if($functionalareas){
      foreach($functionalareas as $fa){
        $faoptions[$fa['functionalareas_id']] = $fa['functionalareas_name'];  
      }
    }
    //asort($faoptions, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
  $this->view->functionalareas  = $faoptions;
  
  
  $userfas  = Engine_Api::_()->getDbtable('functionalareas', 'user')->userFunctionalareas($user->getIdentity());
  $this->view->userfas  = $userfas;
//echo '<pre>';print_r($useriexps);
  $ufaoptions = array();
    if($userfas){
      foreach($userfas as $ufa){
        //echo $ufa['ind_primary_id'];
        $ufaoptions[$ufa['functionalareas_id']] = $ufa['fa_primary_id'];  
      }
    }
  //  print_r($uiexpoptions);
  //die();
  $this->view->userfa = $ufaoptions;
  //// Functional Areas end

  ///// skills start

  $skills = Engine_Api::_()->getDbtable('skills', 'classified')->fetchAll();
  $skilloptions = array();
    if($skills){
      foreach($skills as $skill){
        $skilloptions[$skill['skill_id']] = $skill['skill_name']; 
      }
    }
    //asort($faoptions, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
  $this->view->skills = $skilloptions;
  
  
$userskills = Engine_Api::_()->getDbtable('skills', 'user')->userSkills($user->getIdentity());
$this->view->userskills = $userskills;
 //echo '<pre>';print_r($userskills); die();
  $uskilloptions  = array();
    if($userskills){
      foreach($userskills as $uskill){
        //echo $ufa['ind_primary_id'];
        $uskilloptions[$uskill['skill_id']] = $uskill['skill_primary_id'];  
      }
    }
  //  print_r($uiexpoptions);
  //die();
  $this->view->userskill  = $uskilloptions;
  //// skills end
///////////////////
  $usercats = Engine_Api::_()->getDbtable('categories', 'user')->userCategories($user->getIdentity());
  $ucatoptions  = array();
    if($usercats){
      foreach($usercats as $ucat){
        $ucatoptions[$ucat['category_id']]  = $ucat['cat_primary_id'];  
      }
    }
  $this->view->usercat  = $ucatoptions;
  /////////////
  // General form w/o profile type
    $aliasedFields = $user->fields()->getFieldsObjectsByAlias();
    $this->view->topLevelId = $topLevelId = 0;
    $this->view->topLevelValue = $topLevelValue = null;

    if( isset($aliasedFields['profile_type']) ) {
      $aliasedFieldValue = $aliasedFields['profile_type']->getValue($user);
      $topLevelId = $aliasedFields['profile_type']->field_id;
      $topLevelValue = ( is_object($aliasedFieldValue) ? $aliasedFieldValue->value : null );
      if( !$topLevelId || !$topLevelValue ) {
        $topLevelId = null;
        $topLevelValue = null;
      }
      $this->view->topLevelId = $topLevelId;
      $this->view->topLevelValue = $topLevelValue;
    }
   
  
    // Get form
    $form1 = $this->view->form = new User_Form_Standard(array(
      'item' => Engine_Api::_()->core()->getSubject(),
      'topLevelId' => $topLevelId,
      'topLevelValue' => $topLevelValue,
      'hasPrivacy' => true,
      'privacyValues' => $this->getRequest()->getParam('privacy'),
    ));
  }
   public function getcitiesAction(){
     $search = $_POST['queryString'];

        $search = trim(preg_replace('!\s+!', ' ', $search));
       if($search !='') {
          $subdesig = Engine_Api::_()->getDbtable('cities', 'classified')->getcityByname($search);
          $lis= "";

        foreach($subdesig as $row){
          $lis.= "<li id='".$row["location"]."'>".$row["location"]."</li>";
          }
          $this->_helper->json($lis);
        }
   }

   public function getstatesAction(){
     $search = $_POST['queryString'];
   
        $search = trim(preg_replace('!\s+!', ' ', $search));
       if($search !='') {
          $subdesig = Engine_Api::_()->getDbtable('states', 'classified')->getstatesByname($search);          
          $lis= "";

        foreach($subdesig as $row){
          $lis.= "<li id='".$row["location"]."'>".$row["location"]."</li>";
          }
          $this->_helper->json($lis);
        }
   }

  public function getlocationsAction(){
     $search = $_POST['queryString'];
     
        $search = trim(preg_replace('!\s+!', ' ', $search));
       if($search !='') {
          $subdesig = Engine_Api::_()->getDbtable('states', 'classified')->getstatesByname($search);
          $subdesig1 = Engine_Api::_()->getDbtable('cities', 'classified')->getcityByname($search);
          $lis= "";
          $location=array_unique(array_merge($subdesig, $subdesig1),SORT_REGULAR);
        foreach($location as $row){
          $lis.= "<li class='location_li'>".$row["location"]."</li>";
          }
          $this->_helper->json($lis);
        }
   }
}