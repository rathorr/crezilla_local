<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: IndexController.php 10075 2013-07-30 21:51:18Z jung $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_IndexController extends Core_Controller_Action_Standard
{
  public function indexAction()
  {

  }

  public function homeAction()
  {
    // check public settings
    
    $require_check = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.portal', 1);
	
	if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
	  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
	
    if(!$require_check){
      if( !$this->_helper->requireUser()->isValid() ) return;
    }

   // Check for last login start*/
$user_identity = Engine_Api::_()->user()->getViewer()->getIdentity();
 $searchTable = Engine_Api::_()->fields()->getTable('user', 'search');
    $searchTableName = $searchTable->info('name');
    $select	=	$searchTable->select("item_id")
			//->from ($searchTableName)
			->setIntegrityCheck(false)
			->where("item_id = $user_identity", 1)
			->where("field_458 != '' OR field_460 != '' OR field_464 != ''")
			//->ORwhere("field_460 = '' ")
			//->ORwhere("field_464 = '' ")
			;
		
		$userdata = $searchTable->fetchRow($select);
		//echo $select;
		//echo $userdata->item_id; 
		//die();
	$userDstatus= ($userdata)? $userdata->item_id:false;
  // $userDstatus  = Engine_Api::_()->getDbtable('users', 'user')->getUserfillInfo($user_identity);
   //echo "userDstatus".$userDstatus; 
   //die();
   //if($userDstatus)
	  $userdcs  = Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategories($user_identity);  
  
   if(!$userdata || count($userdcs) == 0)
   {
   	//echo "going";
   	 //return $this->_helper->redirector->gotoRoute(array('controller' => 'edit', 'action' => 'profile'), 'user_extended',true);
   	 return $this->_helper->redirector->gotoRoute(array('controller' => 'edit', 'action' => 'profile','userredirect' => 'novalue'), 'user_extended',true);
   	//$params = array('userredirect' => 'novalue');
	//return $this->_helper->redirector('profile', 'edit', 'user', $params);
   }

   //	echo "not going";
	//die();
    // Check for last login End*/
   
    // Render
    $this->_helper->content
        ->setNoRender()
        ->setEnabled()
        ;
  }


  public function countryAction(){ 

	$country_id = $_POST['country_id'];
	$field_id = $_POST['type'];
		
		 $query ="select * from engine4_user_fields_options where field_id =".$field_id." order by label asc";
				 
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$stmt = $db->query($query);
        $result = $stmt->fetchAll();
		
		 
		
		$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
		
		$query2 ="select value from engine4_user_fields_values 
				  where item_id ='".$viewer->getIdentity()."' 
				  and field_id=".$field_id." ";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$stmt2 = $db->query($query2);
        $result2 = $stmt2->fetchAll();
		$sel_value="";
		if(count($result2)>0)
		{
			$sel_value=	$result2[0]['value'];
		}
	     
		$options='<option value="">Select Country</option>';	
		$sel="";
		foreach ($result as $key => $val)
		{
			if($val['option_id']==$sel_value)
			{
				$sel.='selected="selected"';	
			}
			else
			{
				$sel.='';	
			}
			$options.='<option value="'.$val['option_id'].'" '.$sel.'>'.$val['label'].'</option>';
			$sel="";
		}
		
		echo $options;
		die();
		 

  }
  
  
  public function stateAction(){ 

		$country_id = $_POST['country_id'];
		$field_id = $_POST['type'];
		$type_parent = $_POST['type_parent'];
		
		if($country_id==0)
		{
			
			$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
			$query2 ="select value from engine4_user_fields_values 
					  where item_id ='".$viewer->getIdentity()."' 
					  and field_id=".$type_parent." ";
			 
							
			$file = APPLICATION_PATH . '/application/settings/database.php';
			$options = include $file;
			$db = Zend_Db::factory($options['adapter'], $options['params']);
			$select = new Zend_Db_Select($db);
			$db->getConnection();
 		
			$stmt2 = $db->query($query2);
			$result2 = $stmt2->fetchAll();
		 
		 
			$sel_value="";
			$options='<option value="">Select State</option>';	
			if(count($result2)>0)
			{
 				if($result2[0]['value']!="")
				{
					
				
					$sel_value=	$result2[0]['value'];
					
					$query ="select * from engine4_user_fields_options where parent_id =".$sel_value." and  field_id=".$field_id." order by label asc";
				
					$file = APPLICATION_PATH . '/application/settings/database.php';
					$options = include $file;
					$db = Zend_Db::factory($options['adapter'], $options['params']);
					$select = new Zend_Db_Select($db);
					$db->getConnection();
					
					$stmt = $db->query($query);
					$result = $stmt->fetchAll();
					$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
					
					$query2 ="select value from engine4_user_fields_values 
					where item_id ='".$viewer->getIdentity()."' 
					and field_id='".$field_id."' ";
					
					$file = APPLICATION_PATH . '/application/settings/database.php';
					$options = include $file;
					$db = Zend_Db::factory($options['adapter'], $options['params']);
					$select = new Zend_Db_Select($db);
					$db->getConnection();
					//end DB setup
				
					$stmt2 = $db->query($query2);
					$result2 = $stmt2->fetchAll();
					$sel_value="";
					if(count($result2)>0)
					{
					$sel_value=	$result2[0]['value'];
					}
					$sel_value;
				
				
				
					$sel="";
					$options='<option value="">Select State</option>';	
					foreach ($result as $key => $val)
					{
						if($val['option_id']==$sel_value)
						{
						$sel.='selected="selected"';	
						}
						else
						{
						$sel.='';	
						}
					$options.='<option value="'.$val['option_id'].'" '.$sel.'>'.$val['label'].'</option>';
					$sel="";
					}
 				
				}
 			}
		
		
		}
		else
		{
			
		 
		$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
		$query2 ="select value from engine4_user_fields_values 
				  where item_id ='".$viewer->getIdentity()."' 
				  and field_id=".$field_id." ";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
		
		$stmt2 = $db->query($query2);
        $result2 = $stmt2->fetchAll();
 		 
			$sel_value="";
			$options='<option value="">Select State</option>';	
			if(count($result2)>0)
			{
				 
				if($result2[0]['value']!="")
				{
					
 				$sel_value=	$result2[0]['value'];
 				 $query ="select * from engine4_user_fields_options where parent_id =".$country_id."  and field_id=".$field_id." order by label asc";
				
			
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				$stmt = $db->query($query);
				$result = $stmt->fetchAll();
				$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
				$query2 ="select value from engine4_user_fields_values 
				where item_id ='".$viewer->getIdentity()."' 
				and field_id='".$field_id."' ";
			
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				
				$stmt2 = $db->query($query2);
				$result2 = $stmt2->fetchAll();
				$sel_value="";
				if(count($result2)>0)
				{
				$sel_value=	$result2[0]['value'];
				}
				$sel_value;
			
			
			
				$sel="";
				$options='<option value="">Select State</option>';	
				foreach ($result as $key => $val)
				{
					if($val['option_id']==$sel_value)
					{
					$sel.='selected="selected"';	
					}
					else
					{
					$sel.='';	
					}
				$options.='<option value="'.$val['option_id'].'" '.$sel.'>'.$val['label'].'</option>';
				$sel="";
				}
			
				}
			}
			else
			{
 				 
					
 				$sel_value=	$result2[0]['value'];
 				  $query ="select * from engine4_user_fields_options where parent_id =".$country_id."      and field_id=".$field_id." order by label asc";
				 
			
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				$stmt = $db->query($query);
				$result = $stmt->fetchAll();
				$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
				$query2 ="select value from engine4_user_fields_values 
				where item_id ='".$viewer->getIdentity()."' 
				and field_id=".$field_id." ";
			
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				
				$stmt2 = $db->query($query2);
				$result2 = $stmt2->fetchAll();
				$sel_value="";
				if(count($result2)>0)
				{
				$sel_value=	$result2[0]['value'];
				}
				$sel_value;
			
			
			
				$sel="";
				$options='<option value="">Select State</option>';	
				foreach ($result as $key => $val)
				{
					if($val['option_id']==$sel_value)
					{
					$sel.='selected="selected"';	
					}
					else
					{
					$sel.='';	
					}
				$options.='<option value="'.$val['option_id'].'" '.$sel.'>'.$val['label'].'</option>';
				$sel="";
				}
			
				
			}
 		}
 		echo $options;
			die();
  }
  
  
  
  public function cityAction(){ 

		$state_id = $_POST['state_id'];
		$field_id = $_POST['type'];
		$type_parent = $_POST['type_parent'];
		
		if($state_id==0)
		{
 			
		$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
		$query2 ="select * from engine4_user_fields_values 
				  where item_id ='".$viewer->getIdentity()."' 
				  and field_id=".$field_id." ";
				  
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$stmt2 = $db->query($query2);
        $result2 = $stmt2->fetchAll();
		
		 
		$options='<option value="">Select City</option>';	 
		$sel_value="";
		if(count($result2)>0)
		{
			if($result2[0]['value']!="")
			{
				$sel_value=	$result2[0]['value'];
				
				$query111 ="select * from engine4_user_fields_options where option_id =".$sel_value." and field_id=".$field_id." order by label asc";
			 
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				//end DB setup
				
				$stmt = $db->query($query111);
				$result111 = $stmt->fetchAll();
				
				
				$query ="select * from engine4_user_fields_options where parent_id =".$result111[0]['parent_id']." and field_id=".$field_id." order by label asc";
			 
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				//end DB setup
				
				$stmt = $db->query($query);
				$result = $stmt->fetchAll();
				 
			
				$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
				
				$query2 ="select value from engine4_user_fields_values 
				where item_id ='".$viewer->getIdentity()."' 
				and field_id=".$field_id." ";
				
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				//end DB setup
			
				$stmt2 = $db->query($query2);
				$result2 = $stmt2->fetchAll();
				$sel_value="";
				if(count($result2)>0)
				{
				$sel_value=	$result2[0]['value'];
				}
				$sel_value;
			
				$sel="";
				$options='<option value="">Select City</option>';	
				foreach ($result as $key => $val)
				{
					if($val['option_id']==$sel_value)
					{
					$sel.='selected="selected"';	
					}
					else
					{
					$sel.='';	
					}
				$options.='<option value="'.$val['option_id'].'" '.$sel.'>'.$val['label'].'</option>';
				$sel="";
				}
			
			}
		}
		
		}
		else
		{
		 
			$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
			$query2 ="select value from engine4_user_fields_values 
			where item_id ='".$viewer->getIdentity()."' 
			and field_id=".$field_id." ";
						
			$file = APPLICATION_PATH . '/application/settings/database.php';
			$options = include $file;
			$db = Zend_Db::factory($options['adapter'], $options['params']);
			$select = new Zend_Db_Select($db);
			$db->getConnection();
			//end DB setup
			
			$stmt2 = $db->query($query2);
			$result2 = $stmt2->fetchAll();
			
			 
			 
			$sel_value="";
		 
			if(count($result2)>0)
			{
			 
			if($result2[0]['value']!="")
			{
				 
				$sel_value=	$result2[0]['value'];
				
				$query ="select * from engine4_user_fields_options where parent_id =".$state_id." order by label asc";
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				//end DB setup
				
				$stmt = $db->query($query);
				$result = $stmt->fetchAll();
			
				$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
				
				$query2 ="select value from engine4_user_fields_values 
				where item_id ='".$viewer->getIdentity()."' 
				and field_id=".$field_id." ";
				
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				//end DB setup
			
				$stmt2 = $db->query($query2);
				$result2 = $stmt2->fetchAll();
				$sel_value="";
				if(count($result2)>0)
				{
				$sel_value=	$result2[0]['value'];
				}
				$sel_value;
			
				$sel="";
				$options='<option value="">Select City</option>';	
				foreach ($result as $key => $val)
				{
					if($val['option_id']==$sel_value)
					{
					$sel.='selected="selected"';	
					}
					else
					{
					$sel.='';	
					}
				$options.='<option value="'.$val['option_id'].'" '.$sel.'>'.$val['label'].'</option>';
				$sel="";
				}
			
			}
			else
			{
			 
				$sel_value=	$result2[0]['value'];
				
				$query ="select * from engine4_user_fields_options where parent_id =".$state_id." and field_id=".$field_id." order by label asc";
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				//end DB setup
				
				$stmt = $db->query($query);
				$result = $stmt->fetchAll();
			
				$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
				
				$query2 ="select value from engine4_user_fields_values 
				where item_id ='".$viewer->getIdentity()."' 
				and field_id=".$field_id." ";
				
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				//end DB setup
			
				$stmt2 = $db->query($query2);
				$result2 = $stmt2->fetchAll();
				$sel_value="";
				if(count($result2)>0)
				{
				$sel_value=	$result2[0]['value'];
				}
				$sel_value;
			
				$sel="";
				$options='<option value="">Select City</option>';	
				foreach ($result as $key => $val)
				{
					if($val['option_id']==$sel_value)
					{
					$sel.='selected="selected"';	
					}
					else
					{
					$sel.='';	
					}
				$options.='<option value="'.$val['option_id'].'" '.$sel.'>'.$val['label'].'</option>';
				$sel="";
				}
			
			
			}
		
			}
 			else
			{
			  $query ="select * from engine4_user_fields_options where parent_id =".$state_id."  order by label asc";
			 
				$file = APPLICATION_PATH . '/application/settings/database.php';
				$options = include $file;
				$db = Zend_Db::factory($options['adapter'], $options['params']);
				$select = new Zend_Db_Select($db);
				$db->getConnection();
				//end DB setup
				
				$stmt = $db->query($query);
				$result = $stmt->fetchAll();
				$sel="";
				$options='<option value="">Select City</option>';	
				foreach ($result as $key => $val)
				{
					if($val['option_id']==$sel_value)
					{
					$sel.='selected="selected"';	
					}
					else
					{
					$sel.='';	
					}
				$options.='<option value="'.$val['option_id'].'" '.$sel.'>'.$val['label'].'</option>';
				$sel="";
				}
			}
		}
		echo $options;
			die();
  }
  
  public function browseAction()
  {
    
    if( !$this->_executeSearch() ) {
       throw new Exception('error');
    }
	
	$searchparam	=	$this->_getParam('displayname');
	//echo "searchparam".$searchparam; die();
    if($this->_getParam('ajax') || $searchparam) {
      $this->renderScript('_browseUsers.tpl');
    }

    if( !$this->_getParam('ajax') ) {
    // Render
    $this->_helper->content
        ->setEnabled()
        ;
    }
  }

  protected function _executeSearch()
  {
    // Check form
    $form = new User_Form_Search(array(
      'type' => 'user'
    ));

    if( !$form->isValid($this->_getAllParams()) ) {
      $this->view->error = true;
      $this->view->totalTalent = 0; 
      $this->view->talentCount = 0;
	  $this->view->totalAgency = 0; 
      $this->view->agencyCount = 0;
	  $this->view->totalProducer = 0; 
      $this->view->producerCount = 0;
	  $this->view->totalJobs = 0;
      $this->view->jobCount = 0;
	  $this->view->totalCompanies = 0;
      $this->view->companyCount = 0; 
      
	  $this->view->page = 1;
      return false;
    }

    $this->view->form = $form;

    // Get search params
    $page = (int)  $this->_getParam('page', 1);
    $ajax = (bool) $this->_getParam('ajax', false);
	$type	 =	$this->getRequest()->getParam('type');
    $options = $form->getValues();
    
    // Process options
    $tmp = array();
    $originalOptions = $options;
    foreach( $options as $k => $v ) {
      if( null == $v || '' == $v || (is_array($v) && count(array_filter($v)) == 0) ) {
        continue;
      } else if( false !== strpos($k, '_field_') ) {
        list($null, $field) = explode('_field_', $k);
        $tmp['field_' . $field] = $v;
      } else if( false !== strpos($k, '_alias_') ) {
        list($null, $alias) = explode('_alias_', $k);
        $tmp[$alias] = $v;
      } else {
        $tmp[$k] = $v;
      }
    }
    $options = $tmp;

	//echo '<pre>'; print_r($options); echo '</pre>'; die;

    // Get table info
    $table = Engine_Api::_()->getItemTable('user');
    $userTableName = $table->info('name');

    $searchTable = Engine_Api::_()->fields()->getTable('user', 'search');
    $searchTableName = $searchTable->info('name');
	
	$searchTable2 = Engine_Api::_()->getDbtable('clientids', 'user');
    $searchTableName2 = $searchTable2->info('name');

	$jobTable = Engine_Api::_()->getItemTable('classified');
    $jobTableName = $jobTable->info('name');
	
	$jobsearchTable = Engine_Api::_()->fields()->getTable('classified', 'search');
    $jobsearchTableName = $jobsearchTable->info('name');
	
	$companyTable = Engine_Api::_()->getItemTable('page');
    $comapnyTableName = $companyTable->info('name');
	
    //extract($options); // displayname
	$profile_type	=	'';
    $profile_type = @$options['profile_type'];
    $displayname = @$options['displayname'];
    //echo "displayname".$displayname; die();
    if (!empty($options['extra'])) {
      extract($options['extra']); // is_online, has_photo, submit
    }
	$viewer = Engine_Api::_()->user()->getViewer();
	
    // Contruct query for talents
     $talentselect = $table->select()
      ->from($userTableName)
      ->joinLeft($searchTableName, "`{$searchTableName}`.`item_id` = `{$userTableName}`.`user_id`", null)
      ->joinLeft($searchTableName2, "`{$searchTableName2}`.`user_id` = `{$userTableName}`.`user_id`", null)
      ->where("{$userTableName}.search = ?", 1)
      ->where("{$userTableName}.enabled = ?", 1)
	   ->where("{$userTableName}.level_id != ?", 1)
	  ->where("{$searchTableName}.profile_type = ?", 1)
	  ->where("{$userTableName}.user_id != ?", $viewer->getIdentity())
	  ;
      
    $talentsearchDefault = true;  
      
    // Add displayname
    if( !empty($displayname) ) {
      $talentselect->where("(`{$userTableName}`.`username` LIKE ? || `{$userTableName}`.`displayname` LIKE ? || `{$searchTableName2}`.`clientcode` LIKE ?)", "%{$displayname}%");
      $talentsearchDefault = false;
    }

    // Build search part of query
    $searchParts = Engine_Api::_()->fields()->getSearchQuery('user', $options);
    foreach( $searchParts as $k => $v ) {
      $talentselect->where("`{$searchTableName}`.{$k}", $v);
      if(isset($v) && $v != ""){
        $talentsearchDefault = false;
      }
    }
    
    if($talentsearchDefault){
      $talentselect->order("{$userTableName}.lastlogin_date DESC");
    } else {
      $talentselect->order("{$userTableName}.displayname ASC");
    }
	
	
	// SEARCH QUERY FOR JOBS
	 $selectjob = $jobTable->select()
      //->setIntegrityCheck(false)
      ->from($jobTableName)
	  ->joinLeft("engine4_user_fields_search", "`engine4_user_fields_search`.`item_id` = `{$jobTableName}`.`owner_id`", null)
	   ->joinLeft("engine4_classified_fields_search", "`engine4_classified_fields_search`.`item_id` = `{$jobTableName}`.`classified_id`", null)
      //->group("{$userTableName}.user_id")
      ->where("{$jobTableName}.search = ?", 1)
	  //->where("{$jobTableName}.owner_id != ?", $viewer->getIdentity())
	  ;
      if($profile_type && $profile_type !='6' && $profile_type !='7'){
			$selectjob->where("engine4_user_fields_search.profile_type = ?", $profile_type) ; 
	  }
	  //$selectjob->group("engine4_user_fields_values.item_id"); 
	 
    $jobsearchDefault = true;  
      
    // Add displayname
    if( !empty($displayname) ) {
      $selectjob->where("(`{$jobTableName}`.`title` LIKE ? || `{$jobTableName}`.`body` LIKE ?)", "%{$displayname}%");
      $jobsearchDefault = false;
    }

	
//echo $selectjob;die;
    if($jobsearchDefault){
      $selectjob->order("{$jobTableName}.creation_date DESC");
    } else {
      $selectjob->order("{$jobTableName}.title ASC");
    }
	
	// SEARCH QUERY FOR COMPANIES
	$selectcompany = $companyTable->select()
      ->from($comapnyTableName)
      ->joinLeft("engine4_user_fields_search", "`engine4_user_fields_search`.`item_id` = `{$comapnyTableName}`.`user_id`", null)
	  //->where("{$comapnyTableName}.user_id != ?", $viewer->getIdentity())
	  ->where("{$comapnyTableName}.is_draft = ?", 0);
    	if($profile_type && $profile_type !='6' && $profile_type !='7'){
			$selectcompany->where("engine4_user_fields_search.profile_type = ?", $profile_type); 
	  }
     $companysearchDefault = true;  
     
    // Add displayname
    if( !empty($displayname) ) {
      $selectcompany->where("( `{$companyTableName}`.`services` LIKE ? || `{$companyTableName}`.`title` LIKE ? || `{$companyTableName}`.`displayname` LIKE ? || `{$companyTableName}`.`description` LIKE ?)", "%{$displayname}%");
      $companysearchDefault = false;
    }

    if($companysearchDefault){
      $selectcompany->order("{$companyTableName}.creation_date DESC");
    } else {
      $selectcompany->order("{$companyTableName}.displayname ASC");
    }
	

    // Build paginator for talents
   /* echo '<pre>';
    print_r($talentselect);
    die();*/
    $talentpaginator = Zend_Paginator::factory($talentselect);
    $talentpaginator->setItemCountPerPage(24);
	$talentpage =	($type=='talent')?$page:'1';
    $talentpaginator->setCurrentPageNumber($talentpage);
	
	
	// Build paginator for jobs
    $jobpaginator = Zend_Paginator::factory($selectjob);
    $jobpaginator->setItemCountPerPage(10);
    $jobpage =	($type=='job')?$page:'1';
	$jobpaginator->setCurrentPageNumber($jobpage);
	
	// Build paginator for companies
    $companypaginator = Zend_Paginator::factory($selectcompany);
    $companypaginator->setItemCountPerPage(10);
    $comppage =	($type=='company')?$page:'1';
	$companypaginator->setCurrentPageNumber($comppage);
    
    //$this->view->page = $page;
    //$this->view->ajax = $ajax;
	
    $this->view->talents = $talentpaginator;
    $this->view->totalTalent = $talentpaginator->getTotalItemCount();
    $this->view->talentCount = $talentpaginator->getCurrentItemCount();
	
	$this->view->jobs = $jobpaginator;
	$this->view->totalJobs = $jobpaginator->getTotalItemCount();
    $this->view->jobCount = $jobpaginator->getCurrentItemCount();
	
	$this->view->companies = $companypaginator;
	$this->view->totalCompanies = $companypaginator->getTotalItemCount();
    $this->view->companyCount = $companypaginator->getCurrentItemCount();
	
    $this->view->topLevelId = $form->getTopLevelId();
    $this->view->topLevelValue = $form->getTopLevelValue();
    $this->view->formValues = array_filter($originalOptions);
	
	$this->view->profile_type	=	$profile_type;
	//echo count($talentpaginator); die;

    return true;
  }
  
  
   public function viewedAction()
  {
	  	$this->_helper->layout->disableLayout();
		$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	 
		$table = Engine_Api::_()->getItemTable('user');
		$userTableName = $table->info('name');
	
		$friendTableName = 'engine4_user_views';
		
		// Contruct query for friends
		 $friendselect = $table->select()
		  ->from($userTableName)
		  ->joinLeft($friendTableName, "`{$friendTableName}`.`viewer_id` = `{$userTableName}`.`user_id`", null)
  		  ->where("{$friendTableName}.user_id = ?", $viewer->getIdentity())
		  ->where("{$userTableName}.enabled = ?", 1)
		  ->order("{$userTableName}.displayname ASC")
		  //->where("{$userTableName}.user_id != ?", $viewer->getIdentity())
		  ;
		$this->view->friends =	$table->fetchAll($friendselect);
		
	}
  
  
  
    public function ajaxAction()
  {
 
			$currnet_user 	= Engine_Api::_()->user()->getViewer()->getIdentity();
			$user_id		= $this->getRequest()->getPost('user_id');
			$user_id = explode("/", $user_id);
			$user_id = $user_id[(count($user_id)-1)];
			
			$dateTime		= strtotime(date("d-m-Y"));
			$profile_type 	= Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType($currnet_user);
		
			if( trim($profile_type) == 'Producer')
			{
				$search_info 	= Engine_Api::_()->getDbtable('users', 'user')->getUserProfileInfo($currnet_user, $user_id);
				
				$search_limit   = Engine_Api::_()->getDbtable('users', 'user')->getUserSearchLimit($currnet_user);
			
				if( count($search_info)>= $search_limit)
				{
				
					echo "fail::Search limit is over please upgrade your subscription to increase your search limit";
					die();
				}
				else
				{
					$table 	= Engine_Api::_()->getDbtable('producelogs', 'user');
					$data 	= array( 'log_user_id' => $user_id,
						'log_producer_id' => $currnet_user,
						'log_date_time' => $dateTime);
					$table->insert($data);	
					echo "success::";
					die();
					//Engine_Api::_()->getDbtable('users', 'user')->setUserSearchInfo($data);
			
				}
			}
  }
  
    function msort($array, $key, $sort_flags = SORT_REGULAR) {
		if (is_array($array) && count($array) > 0) {
			if (!empty($key)) {
				$mapping = array();
				foreach ($array as $k => $v) {
					$sort_key = '';
					if (!is_array($key)) {
						$sort_key = $v[$key];
					} else {
						// @TODO This should be fixed, now it will be sorted as string
						foreach ($key as $key_key) {
							$sort_key .= $v[$key_key];
						}
						$sort_flags = SORT_STRING;
					}
					$mapping[$k] = $sort_key;
				}
				asort($mapping, $sort_flags);
				$sorted = array();
				foreach ($mapping as $k => $v) {
					$sorted[] = $array[$k];
				}
				return $sorted;
			}
		}
		return $array;
	}

	/* CASTING SEARCH PAGE */
	public function castingAction(){
		if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
	  		return $this->_helper->redirector->gotoRoute(array(), 'default', true);
   		 }
		$viewer = Engine_Api::_()->user()->getViewer();

		$castingarr=array();
		$this->view->dasignations = $castingdasingations= Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getcastingDesignations();
		foreach ($castingdasingations as $key => $value) {
			$castingarr[$key]= $value['dasignatedsubcategory_id'];
		}
		$castinvals=implode(',', $castingarr);
		//echo '<pre>';
		//print_r($castinvals);
		//die();
		$this->view->langs= $langs = Engine_Api::_()->fields()->getTable('user', 'options')->getuserLang();

		$table = Engine_Api::_()->getItemTable('user');
		$userTableName = $table->info('name');
		 $userselect = $table->select()
		  ->from($userTableName)
		  ->columns(array('avgrating'=> "ROUND(AVG(`engine4_user_reviews`.`rating`),1) AS avgrating"))
		  ->joinLeft("engine4_user_fields_search", "`engine4_user_fields_search`.`item_id` = `{$userTableName}`.`user_id`", null)
		  ->joinLeft("engine4_user_reviews", "`engine4_user_reviews`.`user_id` = `{$userTableName}`.`user_id`", null)
		  ->joinLeft("engine4_user_dasignatedcategories","`engine4_user_dasignatedcategories`.`user_id`=`{$userTableName}`.`user_id`", null)
		  ->where("{$userTableName}.enabled = ?", 1)
		  ->where("{$userTableName}.verified = ?", 1)
		  ->where("{$userTableName}.level_id != ?", 1)
		  ->where("engine4_user_dasignatedcategories.dc_primary_id IN (".$castinvals.")")
		  //->where("{$userTableName}.is_casting_user = ?", 1)
		  ->group("{$userTableName}.user_id")
		 // ->order("{$userTableName}.displayname ASC")
		  //->order("((2 *  (engine4_user_reviews.rating)) + ({$userTableName}.view_count) + (engine4_user_fields_search.field_431))/3  DESC")
		  ->order("(avgrating) DESC")
		  ->limit(20,0);
		 // echo $userselect;
		 //die();
		$userpaginator = $table->fetchAll($userselect);
		$this->view->users = $userpaginator;
		
	}
    
	/* SERVICES SEARCH PAGE */
	
	public function servicesAction(){
		if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
		$viewer = Engine_Api::_()->user()->getViewer();
		$servicearr=array();
		$this->view->dasignations = $servicedesignation= Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getserviceDesignations();
		foreach ($servicedesignation as $key => $value) {
			$servicearr[$key]= $value['dasignatedsubcategory_id'];
		}
		$servicevals=implode(',', $servicearr);

		$this->view->langs= $langs = Engine_Api::_()->fields()->getTable('user', 'options')->getuserLang();
		$this->view->exps= $exps = Engine_Api::_()->fields()->getTable('user', 'options')->getuserExp();


		$table = Engine_Api::_()->getItemTable('user');
		$userTableName = $table->info('name');
		$userselect = $table->select()
		  //->from(array($userTableName,"ROUND(AVG(`engine4_user_reviews`.`rating`),1) AS avgrating"))
		  ->from($userTableName)
		  ->columns(array('avgrating'=> "ROUND(AVG(`engine4_user_reviews`.`rating`),1) AS avgrating"))	
		  ->joinLeft("engine4_user_fields_search", "`engine4_user_fields_search`.`item_id` = `{$userTableName}`.`user_id`", null)
		  ->joinLeft("engine4_user_reviews", "`engine4_user_reviews`.`user_id` = `{$userTableName}`.`user_id`",null)
		  ->joinLeft("engine4_user_dasignatedcategories","`engine4_user_dasignatedcategories`.`user_id`=`{$userTableName}`.`user_id`", null)
		  ->where("{$userTableName}.enabled = ?", 1)
		  ->where("{$userTableName}.level_id != ?", 1)
		  ->where("{$userTableName}.verified = ?", 1)
		  ->where("engine4_user_dasignatedcategories.dc_primary_id IN (".$servicevals.")")
		   //->where("{$userTableName}.is_service_user = ?", 1)
		  ->group("{$userTableName}.user_id")
		 // ->order("{$userTableName}.displayname ASC")
		  //->order("((2 *  (engine4_user_reviews.rating)) + ({$userTableName}.view_count) + (engine4_user_fields_search.field_431))/3  DESC")
		  ->order("(avgrating) DESC")
		  ->limit(20,0);
		 // echo '<pre>'; echo ($userselect); die();
		$userpaginator = $table->fetchAll($userselect);
		$this->view->users = $userpaginator;
		
	}

	/* professional SEARCH PAGE */
	
	public function professionalsAction(){
		if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
	}

	/* suppliers SEARCH PAGE */
	
	public function suppliersAction(){
		if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
		$viewer = Engine_Api::_()->user()->getViewer();
		$servicearr=array();
		$this->view->dasignations = $servicedesignation= Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getsuppliersDesignations();
		foreach ($servicedesignation as $key => $value) {
			$servicearr[$key]= $value['dasignatedsubcategory_id'];
		}
		$servicevals=implode(',', $servicearr);

		$this->view->langs= $langs = Engine_Api::_()->fields()->getTable('user', 'options')->getuserLang();
		$this->view->exps= $exps = Engine_Api::_()->fields()->getTable('user', 'options')->getuserExp();


		$table = Engine_Api::_()->getItemTable('user');
		$userTableName = $table->info('name');
		$userselect = $table->select()
		  ->from($userTableName)
		  ->columns(array('avgrating'=> "ROUND(AVG(`engine4_user_reviews`.`rating`),1) AS avgrating"))
		  ->joinLeft("engine4_user_fields_search", "`engine4_user_fields_search`.`item_id` = `{$userTableName}`.`user_id`", null)
		  ->joinLeft("engine4_user_reviews", "`engine4_user_reviews`.`user_id` = `{$userTableName}`.`user_id`", null)
		  ->joinLeft("engine4_user_dasignatedcategories","`engine4_user_dasignatedcategories`.`user_id`=`{$userTableName}`.`user_id`", null)
		  ->where("{$userTableName}.enabled = ?", 1)
		  ->where("{$userTableName}.level_id != ?", 1)
		  ->where("{$userTableName}.verified = ?", 1)
		  ->where("engine4_user_dasignatedcategories.dc_primary_id IN (".$servicevals.")")
		   //->where("{$userTableName}.is_service_user = ?", 1)
		  ->group("{$userTableName}.user_id")
		 // ->order("{$userTableName}.displayname ASC")
		 // ->order("((2 *  (engine4_user_reviews.rating)) + ({$userTableName}.view_count) + (engine4_user_fields_search.field_431))/3  DESC")
		  ->order("(avgrating) DESC")
		  ->limit(20,0);
		  
		$userpaginator = $table->fetchAll($userselect);
		$this->view->users = $userpaginator;
		
	}
	public function getuserresultsAction()
	  {
		
	  
			$postdata	=	$_POST;
			//echo '<pre>'; print_r($postdata); die;
		  
			$this->_helper->layout->disableLayout();
			$this->view->page		=	$page = $_POST['list_count'];
			
			$table = Engine_Api::_()->getItemTable('user');
			$userTableName = $table->info('name');
			
			 $userselect = $table->select()
			  ->from($userTableName)
			  ->columns(array('avgrating'=> "ROUND(AVG(`engine4_user_reviews`.`rating`),1) AS avgrating"))
			  ->joinLeft("engine4_user_fields_search", "`engine4_user_fields_search`.`item_id` = `{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_fields_values", "`engine4_user_fields_values`.`item_id` = `{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_fields_options", "engine4_user_fields_options.`option_id` = `engine4_user_fields_values`.`value`", null)
			  ->joinLeft("engine4_experience_experiences", "`engine4_experience_experiences`.`owner_id` = `{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_experience_projects", "`engine4_experience_projects`.`owner_id` = `{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_reviews", "`engine4_user_reviews`.`user_id` = `{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_contactinfos","`engine4_user_contactinfos`.`owner_id`=`{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_workexps","`engine4_user_workexps`.`owner_id`=`{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_dasignatedcategories","`engine4_user_dasignatedcategories`.`user_id`=`{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_functionalareas","`engine4_user_functionalareas`.`user_id`=`{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_industryexperiences","`engine4_user_industryexperiences`.`user_id`=`{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_professionalsummarys","`engine4_user_professionalsummarys`.`owner_id`=`{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_skills","`engine4_user_skills`.`user_id`=`{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_user_unionmemberships","`engine4_user_unionmemberships`.`owner_id`=`{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_education_educations","`engine4_education_educations`.`owner_id`=`{$userTableName}`.`user_id`", null)
			  ->joinLeft("engine4_experience_awards","`engine4_experience_awards`.`owner_id`=`{$userTableName}`.`user_id`", null);
			  
			  if($postdata['utype'] =='casting'){
			  	$designations= Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getsearchedDesignations(1);
				 $userselect =  $userselect->where("engine4_user_dasignatedcategories.dc_primary_id IN (".$designations.")"); 
			  }else if($postdata['utype'] =='service'){
			  	$designations= Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getsearchedDesignations(2);
				 $userselect =  $userselect->where("engine4_user_dasignatedcategories.dc_primary_id IN (".$designations.")"); 
			  }else if($postdata['utype'] =='suppliers'){
			  	$designations= Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getsearchedDesignations(3);
				$userselect =  $userselect->where("engine4_user_dasignatedcategories.dc_primary_id IN (".$designations.")"); 
			  }
			  
			  $userselect =  $userselect->where("{$userTableName}.level_id != ?", 1)
			  				->group("{$userTableName}.user_id");

			if(trim($postdata['keyword']) !=''){
				$userselect->where("(engine4_user_fields_values.value LIKE ? || engine4_education_educations.institute LIKE ? || engine4_education_educations.qualification LIKE ? || engine4_user_dasignatedcategories.dasignatedcategory_title LIKE ? || engine4_experience_awards.category LIKE ? || engine4_experience_awards.award LIKE ? || engine4_experience_awards.project LIKE ? || engine4_experience_experiences.client LIKE ? ||
					engine4_experience_experiences.project LIKE ? ||
					engine4_experience_experiences.pastexp LIKE ? ||
					engine4_experience_projects.clientp LIKE ? ||
					engine4_experience_projects.project LIKE ? ||
					engine4_experience_projects.pastpexp LIKE ? || 
					engine4_user_unionmemberships.unionmembership LIKE ? ||
					engine4_user_skills.skill_title LIKE ? ||
					engine4_user_industryexperiences.industryexperience_title LIKE ? ||
					engine4_user_functionalareas.functionalareas_title LIKE ? ||
					engine4_user_professionalsummarys.summary LIKE ?)", "%{$postdata['keyword']}%");
				/*$userselect->where("({$userTableName}.email LIKE ? || {$userTableName}.displayname LIKE ? || engine4_user_fields_search.first_name LIKE ? || engine4_user_fields_search.last_name LIKE ? || engine4_user_fields_search.about_me LIKE ? || engine4_user_fields_search.field_66 LIKE ? )", "%{$postdata['keyword']}%");*/

			}

			if($postdata['designation_id'] !=''){
			
				$userselect->where("engine4_user_dasignatedcategories.dc_primary_id ='".$postdata['designation_id']."' OR engine4_experience_experiences.role_id ='".$postdata['designation_id']."' OR engine4_experience_projects.desig_id ='".$postdata['designation_id']."' ");
				//$userselect->where("engine4_experience_experiences.role_id =?", $postdata['designation_id']);
			}
			
			if($postdata['lang'] !=''){
				
				$userselect->where("engine4_user_fields_values.value =?", $postdata['lang']);
				//$userselect->where("engine4_user_fields_values.value != ''");
			}
			
			if(trim($postdata['location']) != ''){
				if($postdata['location']=="Delhi" OR $postdata['location']=="delhi")
				{
					$location= '"New Delhi","Delhi","Gurgaon","Noida","Faridabad","Ghaziabad"';
				}
				else
				{
					$location='"'.$postdata['location'].'"';
				}

			$userselect->where("engine4_user_fields_values.value IN (".$location.")");
				//$userselect->where("engine4_user_fields_search.field_458 = '".$postdata['location']."' OR engine4_user_fields_search.field_460 ='".$postdata['location']."'");
			}
			if(trim($postdata['exp']) != ''){
				$exp=trim($postdata['exp']);
				if($exp=='ff') {
					$userselect->where("engine4_user_workexps.workexp <=?",1);
				}elseif ($exp=='le') {
					$userselect->where("engine4_user_workexps.workexp <=?",3);
				}elseif ($exp=='ex') {
					$userselect->where("engine4_user_workexps.workexp > ?",3);
				}elseif ($exp=='nc') {
					$userselect->where("engine4_user_workexps.workexp < ?",2);
				}elseif ($exp=='ge') {
					$userselect->where("engine4_user_workexps.workexp BETWEEN 2 AND 5");
				}elseif ($exp=='exs') {
					$userselect->where("engine4_user_workexps.workexp BETWEEN 5 AND 10");
				}elseif ($exp=='ve') {
					$userselect->where("engine4_user_workexps.workexp >",10);
				}
			
			}
			if($postdata['gender'] !=''){
				
				$userselect->where("engine4_user_fields_search.gender =?", $postdata['gender']);
				$userselect->where("engine4_user_fields_search.gender != ''");
			}
		
			// $userselect->order("((2 *  (engine4_user_reviews.rating)) + ({$userTableName}.view_count) + (engine4_user_fields_search.field_431))/3  DESC")
			$userselect->order("(avgrating) DESC")
			->limit(20,$page);
			/*echo $userselect;
			die();*/
			 $userpaginator = $table->fetchAll($userselect);
			 $this->view->users = $userpaginator;
			 $this->view->page_name = $_POST['page_name'];	

	  }
  
  
  /* POST CLASSIFIED */
  public function postclassifiedAction(){
	if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}

	if( !$this->getRequest()->isPost() )
		{
		  $this->view->status = false;
		  $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
		  return;
		}
	else
	{
		$values = $this->getRequest()->getPost();
		
		$db = Engine_Api::_()->getDbtable('postclassifieds', 'album')->getAdapter();
		$db->beginTransaction();

		try
		{
		  $viewer = Engine_Api::_()->user()->getViewer();

		  $postclassifiedTable = Engine_Api::_()->getDbtable('postclassifieds', 'album');
		  $postclassified = $postclassifiedTable->createRow();
		  $postclassified->setFromArray(array(
		  	'title'=>$values['name'],
		  	'description' => $values['cads_body'],
		  	'url' => $values['url'],
		    'owner_type' => 'user',
		    'owner_id' => $viewer->getIdentity(),
		    'creation_date' => new Zend_Db_Expr('NOW()')
		  ));
		  $postclassified->save();
		  $postclassified->order = $postclassified->postclassified_id;
		  $postclassified->save();
		  
		  $this->view->status = true;
		  $this->view->postclassified_id = $postclassified->postclassified_id;

		  $db->commit();
		 // return $postclassified->postclassified_id;
		 //return $this->_helper->redirector->gotoRoute(array('id' => $postclassified->postclassified_id), 'users_editpost', true);
		  return $this->_helper->redirector->gotoRoute(array('module' => 'user', 'controller' => 'index', 'action' => 'myclassified'),'user_general',true); 
		   
		} catch( Album_Model_Exception $e ) {
		  $db->rollBack();
		  $this->view->status = false;
		  $this->view->error = $this->view->translate($e->getMessage());
		  throw $e;
		  return;
		  
		} catch( Exception $e ) {
		  $db->rollBack();
		  $this->view->status = false;
		  $this->view->error = Zend_Registry::get('Zend_Translate')->_('An error occurred.');
		  throw $e;
		  return;
		}

	}
  }
   public function editpostclassifiedAction(){

   	if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
   	$viewer = Engine_Api::_()->user()->getViewer();
   	$viewer_id =$viewer->getIdentity();
   	$this->view->userCls_id = $id = $this->_getParam('id');
    $this->view->userCls = $usercls = Engine_Api::_()->getItem('postclassified', $id);
    $this->view->title=$usercls['title'];
    $this->view->description=$usercls['description'];
    $this->view->url=$usercls['url'];
    
    if( !$this->getRequest()->isPost() )
		{
		  $this->view->status = false;
		  $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
		  return;
		}
	else
	{
		$values = $this->getRequest()->getPost();
		 $postclassifiedTable = Engine_Api::_()->getDbtable('postclassifieds', 'album');
		  $data = array(
		    'title'=>$values['name'],
		  	'description' => $values['cads_body'],
		  	'url' => $values['url']
			);
		    $where = array(
		      'owner_id = ?' => $viewer_id,
		      'postclassified_id = ?' => $values['classi_id']

		    );
		   /* echo '<pre>';
		    print_r($data);
		    print_r($where);
		    die();*/
		    $postclassifiedTable->update($data, $where);

		  $this->view->status = true;
		  $this->view->postclassified_id = $postclassified->postclassified_id;
		 // return $postclassified->postclassified_id;
		 //return $this->_helper->redirector->gotoRoute(array(),'action' 'user_general', true);
		 return $this->_helper->redirector->gotoRoute(array('module' => 'user', 'controller' => 'index', 'action' => 'myclassified'),'user_general',true);  
		

	}
  }
  public function myclassifiedAction()
  {
  	if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}

  	$viewer = Engine_Api::_()->user()->getViewer();
   	$viewer_id =$viewer->getIdentity();
  	
		// FEATCH ADS
		$fetch_classified_ads=$classified_ads=Engine_Api::_()->getDbtable('postclassifieds', 'album')->getmyClassifieds($viewer_id,25);
		
		if (!empty($fetch_classified_ads)) {
			$this->view->classifiedads_array = $fetch_classified_ads;
		} else {
			$this->view->noResult = 1;
		}

	if( !$this->getRequest()->isPost() )
		{
		  $this->view->status = false;
		  $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
		  return;
		}
	else
	{
		$values = $this->getRequest()->getPost();
		/*echo'<pre>';
		echo $viewer_id;
		print_r($values);
		die();*/

		 $postclassifiedTable = Engine_Api::_()->getDbtable('postclassifieds', 'album');
		  
			if($_POST['action_type'] == 'delete')
			{
				$where = array(
			      'owner_id = ?' => $viewer_id,
			      'postclassified_id = ?' => $values['classified_id']
			    );
			   
			    $postclassifiedTable->delete($where);
			}
		    
			
		 return $this->_helper->redirector->gotoRoute(array('module' => 'user', 'controller' => 'index', 'action' => 'myclassified'),'user_general',true);  
	}
		return;
  }

  public function classifiedboardAction()
  {
  	if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}

  	$viewer = Engine_Api::_()->user()->getViewer();
   	$this->view->viewer_id=$viewer_id =$viewer->getIdentity();
  	
		// FEATCH ADS
		$fetch_classified_ads=$classified_ads=Engine_Api::_()->getDbtable('postclassifieds', 'album')->getallClassifieds($viewer_id,25);
		
		if (!empty($fetch_classified_ads)) {
			$this->view->classifiedads_array = $fetch_classified_ads;
		} else {
			$this->view->noResult = 1;
		}
		

	/*if( !$this->getRequest()->isPost() )
		{
		  $this->view->status = false;
		  $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
		  return;
		}
	else
	{
		$values = $this->getRequest()->getPost();
		
		$classified_id=$values['classified_id'];
	$params= '{"url":"\/members\/editpostclassified\/'.$classified_id.'","classifiedpost":"'.$values['title'].'"}';
    $DB = Zend_Db_Table_Abstract::getDefaultAdapter();
    $data = array(
      'user_id' => $values['owner_id'],
      'subject_type' => 'user',
      'subject_id' => $viewer_id,
      'object_type'=> 'user',
      'object_id'=>$values['owner_id'],   
      'type'=>'classified_contact',
      'read'=> 0,
      'mitigated'=>0,
      'params' => $params,
      'date'=>date("Y-m-d h:i:s")
      );
    $DB->insert('engine4_activity_notifications', $data);

    //Send mail for accept Start
    $user = Engine_Api::_()->getItem('user', $values['owner_id']);
              $mailType = 'notify_classified_contact';
                $mailParams = array(
                  'host' => $_SERVER['HTTP_HOST'],
                  'date' => time(),
                  'recipient_title' => $user->getTitle(),
                  'sender_title' => $viewer->getTitle(), 
                  'object_title' => $values['title'], 
                  //'object_description' => $classified->body,
                  //'object_link' => $classified->getHref(),       
                );      
          
    $send = Engine_Api::_()->getApi('mail', 'core')->sendSystem( $user->email, $mailType,$mailParams);

    // Send mail for accept End
	}*/
	//die();
	return;
  }
  /* PREMIUM SEARCH */
  public function premiumsearchAction(){
	  
  }
  
  /* NOTICES */
  public function noticesAction(){
	  
  }
  
  /* SUBSCRIBE TO NEWSLETTER */
  public function subscribeAction(){
	  
  }
  
  /* SHORTLISTED MEMBERS */
  public function shortlistAction()
  {
		if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
	  		return $this->_helper->redirector->gotoRoute(array(), 'default', true);
   		 }
		$viewer = Engine_Api::_()->user()->getViewer();
		$table = Engine_Api::_()->getItemTable('shortlist');
		$shortlistTableName = $table->info('name');
		$userselect = $table->select()
		  ->from($shortlistTableName)
		   ->joinLeft("engine4_user_shortlistfolders", "engine4_user_shortlistfolders.shortlistfolder_id = `{$shortlistTableName}`.`shortlistfolder_id`", "engine4_user_shortlistfolders.*")
		   ->joinLeft("engine4_users", "engine4_users.user_id = `{$shortlistTableName}`.`shortlist_user_id`", array("engine4_users.view_count","engine4_users.displayname")) // new added on 24-04-2017
		  ->setIntegrityCheck(false)
		  ->where("{$shortlistTableName}.user_id = ?", $viewer->getIdentity())
		  ->where("{$shortlistTableName}.status = ?", 1)
		  ->order("{$shortlistTableName}.shortlist_id ASC")
		  ->limit(20,0);
		  
		$userpaginator = $table->fetchAll($userselect);
		//echo '<pre>'; print_r($userpaginator); die();
		$this->view->users = $userpaginator;
		
		$query2 ="select * from engine4_user_shortlistfolders 
				  where user_id ='".$viewer->getIdentity()."' 
				  and status=1";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
		
		$stmt2 = $db->query($query2);
        $result2 = $stmt2->fetchAll();
		$this->view->shortlistfolders = $result2;
		//$this->view->shortlistfolders = Engine_Api::_()->getDbtable('shortlistfolders', 'user')->fetchAll();
	
  }
  
  /* GET MORE SHORTLISTS */
  public function getshortlistresultsAction()
  {
			$postdata	=	$_POST;
		  	$viewer = Engine_Api::_()->user()->getViewer();
			$this->_helper->layout->disableLayout();
			$this->view->page		=	$page = $_POST['list_count'];
			
			$table = Engine_Api::_()->getItemTable('shortlist');
			$shortlistTableName = $table->info('name');
			
			 $userselect = $table->select()
			  ->from($shortlistTableName)
			   ->joinLeft("engine4_user_shortlistfolders", "engine4_user_shortlistfolders.shortlistfolder_id = `{$shortlistTableName}`.`shortlistfolder_id`", "engine4_user_shortlistfolders.*")
			   ->joinLeft("engine4_users", "engine4_users.user_id = `{$shortlistTableName}`.`shortlist_user_id`", array("engine4_users.view_count","engine4_users.displayname")) // new added on 24-04-2017
			  ->setIntegrityCheck(false)
			  ->where("{$shortlistTableName}.user_id = ?", $viewer->getIdentity())
			  ->where("{$shortlistTableName}.status = ?", 1)
			  ;
			  
			if($postdata['shortlistfolder_id'] !=''){
				//$userselect->where("{$shortlistTableName}.shortlistfolder_id IN (?)", $postdata['shortlistfolder_id']);
				$userselect->where("{$shortlistTableName}.shortlistfolder_id = ?", $postdata['shortlistfolder_id']);
			}
			
			
			 $userselect->order("{$shortlistTableName}.shortlist_id ASC")
							->limit(20,$page);
			 
			 $userpaginator = $table->fetchAll($userselect);
			/* echo '<pre>';
			 print_r( $userpaginator);
			 die();*/
			 $this->view->users = $userpaginator;
				
	  }
	  
	  
	public function friendsAction(){
		$this->view->friend_id = $friend_id =  $this->getRequest()->getParam('friend_id'); 
		$this->view->user = $subject = Engine_Api::_()->getItem('user', $friend_id);
		$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
		
		if((($subject->friend_setting == 2 && !$subject->membership()->isMember($viewer)) || ($subject->friend_setting ==3)) && $subject->getIdentity() != $viewer->getIdentity()){
			$this->_forward('requireauth', 'error', 'core');	
		}
		
		$this->view->error = true;
	  $this->view->totalFriend = 0; 
	  $this->view->friendCount = 0;
	  
	  $this->view->page = 1;
	
	  $page = (int)  $this->_getParam('page', 1);
		
		$table = Engine_Api::_()->getItemTable('user');
		$userTableName = $table->info('name');
	
		$searchTable = Engine_Api::_()->fields()->getTable('user', 'search');
		$searchTableName = $searchTable->info('name');
		
		$friendTableName = 'engine4_user_membership';
		
		// Contruct query for friends
		 $friendselect = $table->select()
		  ->from($userTableName)
		  ->setIntegrityCheck(false)
		  ->joinLeft($friendTableName, "`{$friendTableName}`.`user_id` = `{$userTableName}`.`user_id`")
		  ->joinLeft($searchTableName, "`{$searchTableName}`.`item_id` = `{$userTableName}`.`user_id`", null)
		  ->where("{$userTableName}.search = ?", 1)
		  ->where("{$friendTableName}.resource_id = ?", $subject->getIdentity())
		  ->where("{$friendTableName}.active = ?", 1)
		  ->where("{$userTableName}.enabled = ?", 1)
		  ->order("{$userTableName}.displayname ASC")
		  ->limit(30,0)
		  //->where("{$userTableName}.user_id != ?", $viewer->getIdentity())
		  ;
		  
		 // Build paginator for friends
		$friendpaginator = $table->fetchAll($friendselect);
		$this->view->myfriends = $friendpaginator;
		
		
	}
	
	public function getmorefriendsAction(){
			$this->_helper->layout->disableLayout();
			$this->view->page		=	$page = $_POST['list_count'];
			$this->view->section	=	$section	=	$_POST['section'];
			$this->view->friend_id	=	$friend_id	=	$_POST['friend_id'];
			$user = Engine_Api::_()->getItem('user', $friend_id);
			$viewer = Engine_Api::_()->user()->getViewer();
			$table = Engine_Api::_()->getItemTable('user');
			$userTableName = $table->info('name');
			if($section == 'friend'){
				$friendTableName = 'engine4_user_membership';
				// Contruct query for friends
				 $friendselect = $table->select()
				  ->from($userTableName)
				  ->setIntegrityCheck(false)
				  ->joinLeft($friendTableName, "`{$friendTableName}`.`user_id` = `{$userTableName}`.`user_id`")
				  ->where("{$userTableName}.search = ?", 1)
				  ->where("{$friendTableName}.resource_id = ?", $user->getIdentity())
				  ->where("{$friendTableName}.active = ?", 1)
				  ->where("{$userTableName}.enabled = ?", 1)
				  ->order("{$userTableName}.displayname ASC")
				  ->limit(30,$page)
				  ;
				
				  $friendpaginator = $table->fetchAll($friendselect);
				  $this->view->myfriends = $friendpaginator;
				  //echo '<pre>'; print_r($friendpaginator);echo '<pre>';
			}
	}

	public function profilereviewAction()
	 {
	 	$this->_helper->content->setNoRender();
	 	/*echo $this->_getParam('id');
	 	echo $this->_getParam('title');
	 	echo $this->_getParam('summary');
	 	echo $this->_getParam('rating');
	 	die;*/
	
    // Get viewer and other user
    $viewer = Engine_Api::_()->user()->getViewer();
    if( null == ($user_id = $this->_getParam('id')) ||
        null == ($user = Engine_Api::_()->getItem('user', $user_id)) ) {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('No member specified');
      //echo json_encode(array("status" => "Error!", "report" => "No member specified"));
      $this->_helper->json("No member specified.");
      return;
    }
	
	//check hasReviewed
	$tableReview = Engine_Api::_() -> getItemTable('review', 'user');
	$HasReviewed = $tableReview -> checkHasReviewed($user_id, $viewer -> getIdentity());
		
	if($HasReviewed){
		$this->view->status = false;
		$this->view->error = Zend_Registry::get('Zend_Translate')->_('You have already submitted your review for '.$user->getTitle().' .');
		//echo json_encode(array('status' => 'Error!', 'report' => 'You have already submitted your review '. $user->getTitle()));
		//echo json_encode(array('status' => $status));
		$this->_helper->json("You have already submitted your review for ". $user->getTitle().'.');
		//die;
		return;
	}
	
	$user = Engine_Api::_()->getItem('user', $user_id);
	
    // Make form
       
    if( !$this->getRequest()->isPost() )
    {
      return;
    } 
  	
	if($this->getRequest()->isPost()){
		  $viewer = Engine_Api::_()->user()->getViewer();
		  $reviewTable = Engine_Api::_()->getDbtable('reviews', 'user');
		  $review = $reviewTable->createRow();
		  $review->setFromArray(array(
			'user_id' 		=>  $user_id,
			'reviewer_id' 	=>  Engine_Api::_()->user()->getViewer()->getIdentity(),
			'reviewer_name' =>	$viewer->getTitle(),
			'title'	=>	$this->_getParam('title'),
			'summary'	=>	$this->_getParam('summary'),
			'rating'	=>	$this->_getParam('rating'),
			'review_date'	=>	date('Y-m-d H:i:s', time())
		  ));
		  $review->save();	
		  
		 /* SEND NOTIFICATION TO USER */
		 $notifyApi = Engine_Api::_()->getDbtable('notifications', 'activity');
		$reviewUserLink = $this->view->url(array('controller' => 'edit', 'action' => 'review'), 'user_extended');		
		$url="members/edit/review";
		$notifyApi->addNotification($user, $viewer, $user, 'user_rated', array(
					//'text' => $reviewLabel
					'url' => $url,
			  		'name' => 'View now'
		));
		
		//Send mail for review Start

		$mailType = 'notify_review';
			$mailParams = array(
			  'host' => $_SERVER['HTTP_HOST'],
			  //'email' => $user->email,
			  'date' => time(),
			  'recipient_title' => $user->getTitle(),
			  //'sender_email' => 'info@crezilla.com',
			  'sender_title' => $viewer->getTitle(),	
			  //'object_description' => 'This user like to Join our site.',
			  //'object_link' => $inviteUrl			  
			);			
			$send = Engine_Api::_()->getApi('mail', 'core')->sendSystem( $user->email, $mailType,$mailParams);

		// Send mail for review End
	 // echo json_encode(array("status" => "true", "report" => "Your review has been submitted."));
	   $this->_helper->json("Your review has been submitted.");
	}

		
	 	
	    die;


	 }
  	

}