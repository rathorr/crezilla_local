<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: AjaxController.php 10188 2014-04-30 17:00:28Z andres $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_JobController extends Core_Controller_Action_Standard
{
	
public function indexAction()
  {
  
	$viewer = Engine_Api::_()->user()->getViewer()->getIdentity();
	$this->view->userid=$viewer;

	// Related Dedignation
	$userdcs  = Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategories($viewer);
		  $this->view->userdcs=$userdcs;
		  //echo '<pre>';print_r($userdcs); die();
		  $udcoptions = array();
		    if($userdcs){
		      foreach($userdcs as $key=>$udsc){
		        $udcoptions[$key] = $udsc['dc_primary_id']; 
		      }
		    }
		 	//print_r($udcoptions);
		 	$desigs=implode(',', $udcoptions);

	// End


	if( !$viewer ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
	
	 // Set up navigation
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	 if(strtolower($profile_type) == 'talent'){
		 $menu_type	=	'user_edit';
	 }else if(strtolower($profile_type) == 'agency'){
		$menu_type	=	'custom_33'; 
	 }else{
		$menu_type	=	'custom_31'; 
		  if( isset($is_company->value) && $is_company->value !="" )
		{
			//echo $is_company->value;
			$menu_type	=	'custom_34'; 
		}
		
	 }
	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit');

	$table = Engine_Api::_()->getDbTable('classifieds', 'classified');
    $clssName = $table->info('name');
	
	$applytable = Engine_Api::_()->getDbTable('applys', 'classified');
    $applyName = $applytable->info('name');
	
	$usertable = Engine_Api::_()->getDbTable('users', 'user');
    $userName = $usertable->info('name');
	
	$select = $table->select()
			  			->setIntegrityCheck(false)
						->from ( $clssName)   
						->joinLeft($applyName, "$applyName.classified_id = $clssName.classified_id", array("accept", "reject", "hold", "creation_date as applied_date"))
						->joinLeft($userName, "$userName.user_id = $clssName.owner_id", array("displayname"))
						->where("$applyName.user_id = $viewer", 1)
						->order("$applyName.id DESC");
						
	// get the data
	$result = $table->fetchAll($select);
	/*echo'<pre>';
	print_r($result);
	die();*/
	$this->view->jobs 		= $result;
	$this->view->langoptions = Engine_Api::_()->fields()->getTable('classified', 'options')->getClassifiedLang();
	$this->view->dasignations= Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getsubcatjob();
	
	$table = Engine_Api::_()->getItemTable('classified');
		$jobTableName = $table->info('name');
		$jobselect = $table->select()
		  ->from($jobTableName)
		  ->where("{$jobTableName}.closed = ?", 0);
		  if($desigs != '')
		  {
		  	$jobselect->where("{$jobTableName}.category_id IN($desigs)");
		  }
		  else
		  {
		  	$jobselect->where("{$jobTableName}.category_id IN(0)");
		  }
		  
		  //->where("{$jobTableName}.skill_id != ?", 0)
		  $jobselect->order("{$jobTableName}.classified_id DESC");
		  $jobselect->limit(20,0);
		 //echo 'desigs'.$desigs; echo $jobselect;die();
		$jobpaginator = $table->fetchAll($jobselect);
		//echo '<pre>'; print_r($jobpaginator);die();
		$this->view->suggestjobs=$jobpaginator;
  }
  
	
	
	public function getEventsAction()
	{
	
	$table 		= Engine_Api::_()->getDbtable('events', 'User'); /***** Table connection with controller *****/
	$viewer 	= Engine_Api::_()->user()->getViewer()->getIdentity();/**** Current user id *********/
	
	 // Set up navigation
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	 if(strtolower($profile_type) == 'talent'){
		 $menu_type	=	'user_edit';
	 }else if(strtolower($profile_type) == 'agency'){
		$menu_type	=	'custom_33'; 
	 }else{
		$menu_type	=	'custom_31'; 
		
		  if( isset($is_company->value) && $is_company->value !="" )
		{
			//echo $is_company->value;
			$menu_type	=	'custom_34'; 
		}
		
	 }
	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit');
	
	
	$user_id 		= $this->getRequest()->getPost('user_id');
	$result         = array();
	if(isset($user_id) && $user_id>0)
	{
	 $select = $table->select()
		->where("user_id = $user_id", 1)
		->order('id');
					
        // get the data
        $result = $table->fetchAll($select);
	}
	else
	{
		$select = $table->select()
		->where("user_id = $viewer", 1)
		->order('id');
					
        // get the data
        $result = $table->fetchAll($select);
	}
		$data = array();
		foreach( $result as $row )
		{
		$allday = ($row->allDay == "true") ? true : false;
   
		  $data[] = array(
			'id' => $row->id,
			'title' => trim($row->title),
			'start' => trim($row->startdate),
			'end' => trim($row->enddate),
			'allDay' =>$allday,
		  );
		}
	
	echo json_encode($data);
	die;
	}
	
	
	public function newEventAction()
	{
		$table 			= Engine_Api::_()->getDbtable('events', 'User');/******* Table connection ********/
		$viewer 		= Engine_Api::_()->user()->getViewer()->getIdentity();/**** Current user id *********/
		
		/**** Get Post Data *********/
		
		$startdate 		= $this->getRequest()->getPost('startdate').'+'.$this->getRequest()->getPost('zone');
		$title 			= $this->getRequest()->getPost('title'); // Empty set
		
		$data = array( 'title' => $title,
				'startdate' => $startdate,
				'enddate' => $startdate,
				'allDay' => "",
				'user_id' => $viewer);
		
		$table->insert($data);  /******** Insert new event in db *********/
		
		$groupID = Engine_Db_Table::getDefaultAdapter()->lastInsertId();
		echo json_encode(array('status'=>'success','eventid'=>$groupID));
		die;
	}
	
	public function resetDateAction()
	{
		$table 		= Engine_Api::_()->getDbtable('events', 'User'); /******* Table connection ********/
		$viewer 	= Engine_Api::_()->user()->getViewer()->getIdentity(); /**** Current user id *********/
		
		/**** Get Post Data *********/
		
		$startdate 	= $this->getRequest()->getPost('start');
		$title 		= $this->getRequest()->getPost('title'); // Empty set
		$enddate 	= $this->getRequest()->getPost('end');
		$eventid	= $this->getRequest()->getPost('eventid');
		
		/********** Request Data array ***********/
		$data = array( 'title' => $title,
						'startdate' => $startdate,
						'enddate' => $enddate,
						'allDay' => "",
						'user_id' => $viewer);
						
						/******* Update Command ********/
				
		$table->update($data, array('id=?'=>$eventid) ); 
		
		echo json_encode(array('status'=>'success'));
		die;
	}

	
	public function changeTitleAction()
	{
		$table 		= Engine_Api::_()->getDbtable('events', 'User'); /******* Table connection ********/
		$viewer 	= Engine_Api::_()->user()->getViewer()->getIdentity(); /**** Current user id *********/
		
		/**** Get Post Data *********/
		

		$title 		= $this->getRequest()->getPost('title'); // Empty set
		$eventid	= $this->getRequest()->getPost('eventid');
		
		/********** Request Data array ***********/
		$data = array( 'title' => $title, 'user_id' => $viewer);
		
		/******* Update Command ********/
		$table->update($data, array('id=?'=>$eventid) ); 
		
		echo json_encode(array('status'=>'success'));
		die;
	}
	public function removeEventAction()
	{
		$table 		= Engine_Api::_()->getDbtable('events', 'User'); /******* Table connection ********/
		$viewer 	= Engine_Api::_()->user()->getViewer()->getIdentity(); /**** Current user id *********/
		
		/**** Get Post Data *********/
		
		$eventid	= $this->getRequest()->getPost('eventid');
		
		
		/******* Delete Command ********/
		$table->delete(array('id=?'=>$eventid) ); 
		
		echo json_encode(array('status'=>'success'));
		die;
	}


  
  
  }