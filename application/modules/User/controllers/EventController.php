<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: AjaxController.php 10188 2014-04-30 17:00:28Z andres $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_EventController extends Core_Controller_Action_Standard
{
	
	  public function indexAction()
  {
	  $viewer = Engine_Api::_()->user()->getViewer()->getIdentity();
	
	if( !$viewer ) {
      return $this->_helper->redirector->gotoRoute(array(), 'default', true);
    }
	
	 // Set up navigation
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	 if(strtolower($profile_type) == 'talent'){
		 $menu_type	=	'user_edit';
	 }else if(strtolower($profile_type) == 'agency'){
		$menu_type	=	'custom_33'; 
	 }else{
		$menu_type	=	'custom_31'; 
		if( isset($is_company->value) && $is_company->value !="" )
		{
			//echo $is_company->value;
			$menu_type	=	'custom_34'; 
		}
	 }
	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit');
	
	$table = Engine_Api::_()->getDbtable('events', 'user');
	$userdata = $table->getUserCategory($viewer);
	
	$this->view->form = $form = new User_Form_Event();
	
	$this->view->experiences = 	$result;
	$this->view->cat_id		 =	$userdata->value;
  }
  
	
	
	public function getEventsAction()
	{
	
	$table 		= Engine_Api::_()->getDbtable('events', 'User'); /***** Table connection with controller *****/
	$viewer 	= Engine_Api::_()->user()->getViewer()->getIdentity();/**** Current user id *********/
	
	 // Set up navigation
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	 if(strtolower($profile_type) == 'talent'){
		 $menu_type	=	'user_edit';
	 }else if(strtolower($profile_type) == 'agency'){
		$menu_type	=	'custom_33'; 
	 }else{
		$menu_type	=	'custom_31'; 
	 }
	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit');
	
	
	$user_id 		= $this->getRequest()->getPost('user_id');
	$result         = array();
	if(isset($user_id) && $user_id>0)
	{
	 $select = $table->select()
		->where("user_id = $user_id", 1)
		->order('id');
					
        // get the data
        $result = $table->fetchAll($select);
	}
	else
	{
		$select = $table->select()
		->where("user_id = $viewer", 1)
		->order('id');
					
        // get the data
        $result = $table->fetchAll($select);
	}
		$data = array();
		foreach( $result as $row )
		{
		$allday = ($row->allDay == "true") ? true : false;
		$class_date = explode("T", $row->startdate);
		
		$start_date 	= strtotime($row->startdate);
		$end_date 		= strtotime($row->enddate);
		$dates_selected = "";
		if($start_date < $end_date )
		{
			$stop_date = date('Y-m-d', $start_date);
			while(strtotime($stop_date) <= $end_date )
			{
				$dates_selected .= $dates_selected !=""?'::'.$stop_date:$stop_date;
				$stop_date = date('Y-m-d', strtotime($stop_date . ' +1 day'));
			}
			
		}
		else
		{
			$dates_selected = trim($row->startdate);
		}
		
		  $data[] = array(
			'id' => $row->id,
			'title' => trim($row->title),
			'start' => trim($row->startdate),
			'location'	=>trim($row->location),
			'end' => trim($row->enddate),
			'end_date' => trim($row->enddate),
			'selected_days' => $dates_selected,
			'class_date' => $class_date[0],
			'backgroundColor'=>'#E82525',
			'allDay' =>$allday,
		  );
		}
	
	echo json_encode($data);
	die;
	}
	
	public function getEventAction()
	{
	
	$table 		= Engine_Api::_()->getDbtable('events', 'User'); /***** Table connection with controller *****/
	$viewer 	= Engine_Api::_()->user()->getViewer()->getIdentity();/**** Current user id *********/
	$result         = array();
	$event_id	=	$_POST['eventid'];
	if(isset($event_id) && $event_id>0)
	{
	 $select = $table->select()
		->where("id = $event_id", 1)
		->order('id');
					
        // get the data
        $result = $table->fetchAll($select);
	}
	
		$data = array();
		foreach( $result as $row )
		{
		$allday = ($row->allDay == "true") ? true : false;
		$class_date = explode("T", $row->startdate);
		
		$start_date 	= strtotime($row->startdate);
		$end_date 		= strtotime($row->enddate);
		$dates_selected = "";
		if($start_date < $end_date )
		{
			$stop_date = date('Y-m-d', $start_date);
			while(strtotime($stop_date) <= $end_date )
			{
				$dates_selected .= $dates_selected !=""?'::'.$stop_date:$stop_date;
				$stop_date = date('Y-m-d', strtotime($stop_date . ' +1 day'));
			}
			
		}
		else
		{
			$dates_selected = trim($row->startdate);
		}
		
		  $data = array(
			'id' => $row->id,
			'title' => trim($row->title),
			'location'	=>trim($row->location),
			'start' => trim($row->startdate),
			'end' => trim($row->enddate),
			'end_date' => trim($row->enddate),
			'selected_days' => $dates_selected,
			'class_date' => $class_date[0],
			'backgroundColor'=>'#E82525',
			'allDay' =>$allday,
		  );
		}
	echo json_encode($data);
	die;
	}
	
	
	public function getDaysAction()
	{
	
	$table 		= Engine_Api::_()->getDbtable('events', 'User'); /***** Table connection with controller *****/
	$viewer 	= Engine_Api::_()->user()->getViewer()->getIdentity();/**** Current user id *********/
	
	 // Set up navigation
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	 if(strtolower($profile_type) == 'talent'){
		 $menu_type	=	'user_edit';
	 }else if(strtolower($profile_type) == 'agency'){
		$menu_type	=	'custom_33'; 
	 }else{
		$menu_type	=	'custom_31'; 
		  if( isset($is_company->value) && $is_company->value !="" )
		{
			//echo $is_company->value;
			$menu_type	=	'custom_34'; 
		}
		
	 }
	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit');
	
	
	$user_id 		= $this->getRequest()->getPost('user_id');
	$result         = array();
	if(isset($user_id) && $user_id>0)
	{
	 $select = $table->select()
		->where("user_id = $user_id", 1)
		->order('id');
					
        // get the data
        $result = $table->fetchAll($select);
	}
	else
	{
		$select = $table->select()
		->where("user_id = $viewer", 1)
		->order('id');
					
        // get the data
        $result = $table->fetchAll($select);
	}
		$data = array();
		$i=1;
		foreach( $result as $row )
		{
		
		$allday = ($row->allDay == "true") ? true : false;
		$class_date = explode("T", $row->startdate);
		
		$start_date 	= strtotime($row->startdate);
		$end_date 		= strtotime($row->enddate);
		$dates_selected = "";
		if($start_date < $end_date )
		{
			$stop_date = date('Y-m-d', $start_date);
			while(strtotime($stop_date) <= $end_date )
			{
				$dates_selected .= $dates_selected !=""?'::'.$stop_date:$stop_date;
				$stop_date = date('Y-m-d', strtotime($stop_date . ' +1 day'));
			}
			
		}
		else
		{
			$dates_selected = trim($row->startdate);
		}
			if($i == 1){
				$color = "#ff8c00";	
			}else if($i == 2){
				$color = "#CD853F";	
			}else if($i == 3){
				$color = "#ffbe22";	
			}
			
			//echo $i;
		  $data[] = array(
			'id' => $row->id,
			'title' => trim($row->title),
			'start' => trim($row->startdate),
			'end' => date('Y-m-d', strtotime(trim($row->enddate) . ' +1 day')),
			'end_date' => trim($row->enddate),
			'location'	=>trim($row->location),
			'selected_days' => $dates_selected,
			'class_date' => $class_date[0],
			'backgroundColor'=>$color,
			'allDay' =>$allday,
			'imageurl'=>$this->view->baseUrl().'/public/custom/images/round-delete.png',
			'editimageurl'=>$this->view->baseUrl().'/public/custom/images/edit_memeber_profile_icon.png'
		  );
		  
		  if($i == 3){
				$i = 0;	
		  }
		  $i++;
		}
	//die;
	echo json_encode($data);
	die;
	}
	
	
	
	public function newEventAction()
	{
		$table 			= Engine_Api::_()->getDbtable('events', 'User');/******* Table connection ********/
		$viewer 		= Engine_Api::_()->user()->getViewer()->getIdentity();/**** Current user id *********/
		
		/**** Get Post Data *********/
		
		$startdate 		= $this->getRequest()->getPost('startdate').'+'.$this->getRequest()->getPost('zone');
		$enddate 		= $this->getRequest()->getPost('enddate').'+'.$this->getRequest()->getPost('zone');
		$title 			= $this->getRequest()->getPost('title'); // Empty set
		$location		= $this->getRequest()->getPost('location');
		$data = array( 'title' => $title,
		        'location' => $location,
				'startdate' => $startdate,
				'enddate' => $enddate,
				'allDay' => "",
				'user_id' => $viewer);
		$table->insert($data);  /******** Insert new event in db *********/
		
		$groupID = Engine_Db_Table::getDefaultAdapter()->lastInsertId();
		echo json_encode(array('status'=>'success','eventid'=>$groupID));
		die;
	}
	
	public function resetDateAction()
	{
		$table 		= Engine_Api::_()->getDbtable('events', 'User'); /******* Table connection ********/
		$viewer 	= Engine_Api::_()->user()->getViewer()->getIdentity(); /**** Current user id *********/
		
		/**** Get Post Data *********/
		
		$startdate 	= $this->getRequest()->getPost('start');
		$title 		= $this->getRequest()->getPost('title'); // Empty set
		$location	= $this->getRequest()->getPost('location');
		$enddate 	= $this->getRequest()->getPost('end');
		$eventid	= $this->getRequest()->getPost('eventid');
		$pagetype	= $this->getRequest()->getPost('type');
		
		/********** Request Data array ***********/
		$startdate = date("Y-m-d", strtotime($startdate));
		$enddate   = ($pagetype && $pagetype=='eventdrop')?date("Y-m-d", strtotime($enddate. ' -1 day')):date("Y-m-d", strtotime($enddate));
		//$enddate = date("Y-m-d", strtotime($enddate. ' +1 day'));
		$data = array( 'title' => $title,
						'location' => $location,
						'startdate' => $startdate,
						'enddate' => $enddate,
						'allDay' => "",
						'user_id' => $viewer);
				
						/******* Update Command ********/
		if($eventid > 0)
		{
			$table->update($data, array('id=?'=>$eventid) ); 
			echo json_encode(array('status'=>'success'));
		}
	    else
		{
			$table->insert($data);  /******** Insert new event in db *********/
			
			$groupID = Engine_Db_Table::getDefaultAdapter()->lastInsertId();
			echo json_encode(array('status'=>'success','eventid'=>$groupID));
		}
		
		
		die;
	}

	
	public function changeTitleAction()
	{
		$table 		= Engine_Api::_()->getDbtable('events', 'User'); /******* Table connection ********/
		$viewer 	= Engine_Api::_()->user()->getViewer()->getIdentity(); /**** Current user id *********/
		
		/**** Get Post Data *********/
		

		$title 		= $this->getRequest()->getPost('title'); // Empty set
		$eventid	= $this->getRequest()->getPost('eventid');
		
		/********** Request Data array ***********/
		$data = array( 'title' => $title, 'user_id' => $viewer);
		
		/******* Update Command ********/
		$table->update($data, array('id=?'=>$eventid) ); 
		
		echo json_encode(array('status'=>'success'));
		die;
	}
	public function removeEventAction()
	{
		$table 		= Engine_Api::_()->getDbtable('events', 'User'); /******* Table connection ********/
		$viewer 	= Engine_Api::_()->user()->getViewer()->getIdentity(); /**** Current user id *********/
		
		/**** Get Post Data *********/
		
		$eventid	= $this->getRequest()->getPost('eventid');
		
		
		/******* Delete Command ********/
		$table->delete(array('id=?'=>$eventid) ); 
		
		echo json_encode(array('status'=>'success'));
		die;
	}


  
  
  }