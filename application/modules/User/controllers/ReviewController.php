<?php

class User_ReviewController extends Core_Controller_Action_Standard
{
	public function indexAction()
	{
		$this -> view -> viewer = $viewer = Engine_Api::_()->user() ->getViewer();		
		$reviewTbl = Engine_Api::_()->getItemTable("ynmember_review");
		$params = $this->_getAllParams(); 
		if(!empty($params['review_for']))
		{
			$resource_id = array();
			$list_reviewer = Engine_Api::_() -> ynmember() -> getUsersByName($params['review_for']);
			foreach($list_reviewer as $item)
			{
				$resource_id[] = $item -> getIdentity();
			}
			$params['resource_id'] = $resource_id;
		}
		if(!empty($params['review_by']))
		{
			$user_id = array();
			$list_review_for = Engine_Api::_() -> ynmember() -> getUsersByName($params['review_by']);
			foreach($list_review_for as $item)
			{
				$user_id[] = $item ->  getIdentity();
			}
			$params['user_id'] = $user_id;
		}
		$this -> view -> can_edit_own_review = $can_edit_own_review = ($this->_helper->requireAuth()->setAuthParams('ynmember_review', null, 'can_edit_own_review') -> checkRequire());
		$this -> view -> reviews = $reviews = $reviewTbl->getReviewPaginator($params);
    	$this->_helper->content->setEnabled();
	}
	
	public function usefulAction()
	{
		$review_id = $this->_getParam('review_id');
		$value = $this->_getParam('value');
		$inline = $this->_getParam('inline', false);
		if( !$this->getRequest()->isPost() ) {
			$this->view->status = false;
			$this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method.');
			return;
		}
		$this->view->review = $review = Engine_Api::_()->getItem('ynmember_review', $review_id);
		if (!review){
			$this->view->status = false;
			$this->view->error = Zend_Registry::get('Zend_Translate')->_('This review is not existed.');
			return;
		}
		$viewer = Engine_Api::_()->user() ->getViewer();
		if (!$viewer->getIdentity()){
			$this->view->status = false;
			$this->view->error = Zend_Registry::get('Zend_Translate')->_('Can not set useful');
			return;
		}
		$usefulTbl = Engine_Api::_()->getDbTable('usefuls', 'ynmember');
		$row = $usefulTbl->getUseFul($viewer->getIdentity(), $review_id);
		if (!$row)
		{
			$row = $usefulTbl->createRow();
		}
		$row->setFromArray(array(
			'review_id' => $review_id,
			'user_id' => $viewer->getIdentity(),
			'value' => $value,
		));
		$row->save(); 
		$params = $review->getReviewUseful();
		if (isset($params['yes_count']))
		{
			$review->helpful_count += (int)$params['yes_count'];
		}
		if (isset($params['no_count']))
		{
			$review->helpful_count += (int)$params['no_count'];
		}
		$review->save();
		$params['inline'] = ($inline) ? true : false;
		echo $this->view->partial(
	      '_useful.tpl',
	      'ynmember',
	      $params
	    ); exit;
	}
	
	public function userAction()
	{
		$identity = $this->_getParam('id');
		if( $identity ) 
		{
			$this -> user = $user = Engine_Api::_()->getItem('user', $identity);
			if( $user instanceof Core_Model_Item_Abstract ) 
			{
				if( !Engine_Api::_()->core()->hasSubject() ) 
				{
					Engine_Api::_()->core()->setSubject($user);
				}
			}
		}
		
		$reviewTbl = Engine_Api::_()->getItemTable('review');
		$reviews = $reviewTbl->getAllReviewsByResourceId($user->getIdentity());
		$this->view->reviews = $reviews;
		
		// Set up navigation
		$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
		
		
		 if(strtolower($profile_type) == 'talent'){
			 $menu_type	=	'user_edit';
		 }else if(strtolower($profile_type) == 'agency'){
			$menu_type	=	'custom_33'; 
		 }else {
			$menu_type	=	'custom_31'; 
		 }
	 	
		$this->view->navigation = Engine_Api::_()
       		->getApi('menus', 'core')
       		->getNavigation('user_edit');
				
		$this->_helper->content->setEnabled();
	}
	
	public function userGroupAction()
	{
		$identity = $this->_getParam('id');
		if( $identity ) 
		{
			$this -> user = $user = Engine_Api::_()->getItem('user', $identity);
		}
		$this -> _helper -> layout -> setLayout('default-simple');
		$this -> view -> groups =  array();
		if (Engine_Api::_()->hasModuleBootstrap('group') || Engine_Api::_()->hasModuleBootstrap('advgroup'))
		{
			$groupTbl = Engine_Api::_()->getItemTable('group');
			$membership = (Engine_Api::_()->hasModuleBootstrap('advgroup'))
				? Engine_Api::_()->getDbtable('membership', 'advgroup')
				: Engine_Api::_()->getDbtable('membership', 'group');
		
			$select = $membership->getMembershipsOfSelect($user);
			$this -> view -> groups = $groups = $groupTbl->fetchAll($select);
		}
	}
	
	public function detailAction()
	{
		$reviewId = $this->_getParam('id');
		if( $reviewId ) 
		{
			$review = Engine_Api::_()->getItem('review', $reviewId);
		}
		if (!$review)
		{
			return $this -> _helper -> redirector -> gotoRoute(array(
			    'controller' => 'review',
				'action' => 'index'
			), 'user_extended', true);
		}
		$viewer = Engine_Api::_()->user() ->getViewer();
		if ($review->user_id != $viewer->getIdentity())
		{
			$review->view_count++;
			$review->save();
		}
		if( $review instanceof User_Model_Review ) 
		{
			if( !Engine_Api::_()->core()->hasSubject() ) 
			{
				Engine_Api::_()->core()->setSubject($review);
			}
		}
		$this -> view -> can_report_reviews = $can_report_reviews = ($this->_helper->requireAuth()->setAuthParams('user_review', null, 'can_report_reviews') -> checkRequire());
		$this -> view -> can_delete_own_reviews = $can_delete_own_reviews = ($this->_helper->requireAuth()->setAuthParams('user_review', null, 'can_delete_own_reviews') -> checkRequire());
		$this -> view -> review = $review;
		$this -> view -> addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
		$this -> _helper->content->setEnabled();
	}
	
	public function deleteAction()
	{
		$viewer = Engine_Api::_() -> user() -> getViewer();
		$review = Engine_Api::_() -> getItem('ynmember_review', $this -> getRequest() -> getParam('id'));
		/*
		if (!$this -> _helper -> requireAuth() -> setAuthParams($review, null, 'delete') -> isValid())
			return;
		*/
		// In smoothbox
		$this -> _helper -> layout -> setLayout('default-simple');

		$this -> view -> form = $form = new Ynmember_Form_DeleteReview();

		if (!$review)
		{
			$this -> view -> status = false;
			$this -> view -> error = Zend_Registry::get('Zend_Translate') -> _("Review doesn't exists or not authorized to delete.");
			return;
		}

		if (!$this -> getRequest() -> isPost())
		{
			$this -> view -> status = false;
			$this -> view -> error = Zend_Registry::get('Zend_Translate') -> _('Invalid request method');
			return;
		}

		$db = $review -> getTable() -> getAdapter();
		$db -> beginTransaction();

		try
		{
			//delete rating belong to this review
			$ratingTable = Engine_Api::_() -> getItemTable('ynmember_rating');
			$select = $ratingTable -> select() -> where('review_id = ?', $review -> getIdentity());
			$ratings = $ratingTable -> fetchAll($select);
			foreach($ratings as $rating)
			{
				$rating -> delete();
			}
			//delete review
			$review -> delete();
			$db -> commit();
		}
		catch (Exception $e)
		{
			$db -> rollBack();
			throw $e;
		}

		$this -> view -> status = true;
		$this -> view -> message = Zend_Registry::get('Zend_Translate') -> _('Review has been deleted.');
		return $this -> _forward('success', 'utility', 'core', array(
			'parentRedirect' => Zend_Controller_Front::getInstance() -> getRouter() -> assemble(array('controller' => 'review'), 'ynmember_extended', true),
			'messages' => Array($this -> view -> message)
		));
	}
	
}
	
	