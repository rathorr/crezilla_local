<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ProfileController.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_ProfileController extends Core_Controller_Action_Standard
{
  protected $item_type;
  protected $item;
  public function init()
  {
    // @todo this may not work with some of the content stuff in here, double-check
    $subject = null;
    if( !Engine_Api::_()->core()->hasSubject() )
    {
      $id = $this->_getParam('id');

      // use viewer ID if not specified
      //if( is_null($id) )
      //  $id = Engine_Api::_()->user()->getViewer()->getIdentity();

      if( null !== $id )
      {
        $subject = Engine_Api::_()->user()->getUser($id);
        if( $subject->getIdentity() )
        {
          Engine_Api::_()->core()->setSubject($subject);
        }
      }
    }

    $this->_helper->requireSubject('user');
    $this->_helper->requireAuth()->setNoForward()->setAuthParams(
      $subject,
      Engine_Api::_()->user()->getViewer(),
      'view'
    );
	
  }
  
 
  
  public function indexAction()
  {
    $subject 		= Engine_Api::_()->core()->getSubject();
    $viewer 		= Engine_Api::_()->user()->getViewer();
	$currnet_user 	= Engine_Api::_()->user()->getViewer()->getIdentity();
	$user_id		= $subject->getIdentity();
	$dateTime		= strtotime(date("d-m-Y"));
	$profile_type 	= Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType($currnet_user);
	  
	if( trim($profile_type) == 'Producer')
	{
		$search_info 	= Engine_Api::_()->getDbtable('users', 'user')->getUserProfileInfo($currnet_user, $user_id);
		
		$search_limit   = Engine_Api::_()->getDbtable('users', 'user')->getUserSearchLimit($currnet_user);
	
		if( count($search_info)>= $search_limit)
		{
		//	echo 'Your limit is over please upgrade your subscription';
		//	$this->_helper->redirector->gotoUrl( $_SERVER['HTTP_REFERER']);

		}
		else
		{
			$table 	= Engine_Api::_()->getDbtable('producelogs', 'user');
			$data 	= array( 'log_user_id' => $user_id,
				'log_producer_id' => $currnet_user,
				'log_date_time' => $dateTime);
			$table->insert($data);	
			//Engine_Api::_()->getDbtable('users', 'user')->setUserSearchInfo($data);
	
		}
	}
	

    // check public settings
    $require_check = Engine_Api::_()->getApi('settings', 'core')->core_general_profile;
    if( !$require_check && !$this->_helper->requireUser()->isValid() ) {
      return;
    }
	
    // Check enabled
    if( !$subject->enabled && !$viewer->isAdmin() ) {
      return $this->_forward('requireauth', 'error', 'core');
    }

    // Check block
    if( $viewer->isBlockedBy($subject) && !$viewer->isAdmin() ) {
      return $this->_forward('requireauth', 'error', 'core');
    }

    // Increment view count
    if( !$subject->isSelf($viewer) ) {
		$data	=	array('viewer_id' => $viewer->getIdentity(), 'user_id' => $subject->getIdentity(), 'ip_addr'	=>	$_SERVER['REMOTE_ADDR']);
		$isAreadyView	=	Engine_Api::_()->getDbtable('views', 'user')->isReviewGiven($data);
		if(!$isAreadyView){
		  $subject->view_count++;
		  $viewTable = Engine_Api::_()->getDbtable('views', 'user');
		  $view = $viewTable->createRow();
		  $view->setFromArray(array(
			'user_id' 		=>  $subject->getIdentity(),
			'viewer_id' 	=>  $viewer->getIdentity(),
			'ip_addr'		=>	$_SERVER['REMOTE_ADDR']
		  ));
		  $view->save();
		}
     	 $subject->save();
    }

    
    // Check to see if profile styles is allowed
    $style_perm = Engine_Api::_()->getDbtable('permissions', 'authorization')->getAllowed('user', $subject->level_id, 'style');
	
	
    if($style_perm){
      // Get styles
      $table = Engine_Api::_()->getDbtable('styles', 'core');
      $select = $table->select()
        ->where('type = ?', $subject->getType())
        ->where('id = ?', $subject->getIdentity())
        ->limit();

      $row = $table->fetchRow($select);
	
	
      if( null !== $row && !empty($row->style) )
      {
        $this->view->headStyle()->appendStyle($row->style);
      }
    }
	
	
	
    // Render
    $this->_helper->content
        ->setNoRender()
        ->setEnabled()
        ;
  }
  
  public function reviewAction()
  {
	
	if( !$this->_helper->requireUser()->isValid() ) return;

    // Get viewer and other user
    $viewer = Engine_Api::_()->user()->getViewer();
    if( null == ($user_id = $this->_getParam('id')) ||
        null == ($user = Engine_Api::_()->getItem('user', $user_id)) ) {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('No member specified');
      return;
    }
	
	//check hasReviewed
	$tableReview = Engine_Api::_() -> getItemTable('review', 'user');
	$HasReviewed = $tableReview -> checkHasReviewed($user_id, $viewer -> getIdentity());
		
	if($HasReviewed){
		$this->view->status = false;
		$this->view->error = Zend_Registry::get('Zend_Translate')->_('You have already submitted your review for '.$user->getTitle().' .');
		return;
	}
	
	$user = Engine_Api::_()->getItem('user', $user_id);
	
    // Make form
    $this->view->form = $form = new User_Form_Review(array(
			'user' => $user,
		));
    
    if( !$this->getRequest()->isPost() )
    {
      return;
    } 

    if( !$form->isValid($this->getRequest()->getPost()) )
    {
      /*$this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid data');*/
      return;
    }
	
	if($this->getRequest()->isPost()){
		  $viewer = Engine_Api::_()->user()->getViewer();
		  $reviewTable = Engine_Api::_()->getDbtable('reviews', 'user');
		  $review = $reviewTable->createRow();
		  $review->setFromArray(array(
			'user_id' 		=>  $user_id,
			'reviewer_id' 	=>  Engine_Api::_()->user()->getViewer()->getIdentity(),
			'reviewer_name' =>	$viewer->getTitle(),
			'title'	=>	$form->getValue('title'),
			'summary'	=>	$form->getValue('summary'),
			'rating'	=>	$this->_getParam('review_rating'),
			'review_date'	=>	date('Y-m-d H:i:s', time())
		  ));
		  $review->save();	
		  
		 /* SEND NOTIFICATION TO USER */
		 $notifyApi = Engine_Api::_()->getDbtable('notifications', 'activity');
		$reviewUserLink = $this->view->url(array('controller' => 'edit', 'action' => 'review'), 'user_extended');
		//$reviewLabel = "<a href='{$reviewUserLink}'>{$this->view->translate('review')}</a>";
		//$reviewLabel ='{"url":"\/members\/edit\/review","name":"review"}';
		$url="members/edit/review";
		$notifyApi->addNotification($user, $viewer, $user, 'user_rated', array(
					//'text' => $reviewLabel
					'url' => $url,
			  		'name' => 'View now'
		));
		
		//Send mail for review Start

		$mailType = 'notify_review';
			$mailParams = array(
			  'host' => $_SERVER['HTTP_HOST'],
			  //'email' => $user->email,
			  'date' => time(),
			  'recipient_title' => $user->getTitle(),
			  //'sender_email' => 'info@crezilla.com',
			  'sender_title' => $viewer->getTitle(),	
			  //'object_description' => 'This user like to Join our site.',
			  //'object_link' => $inviteUrl			  
			);			
			$send = Engine_Api::_()->getApi('mail', 'core')->sendSystem( $user->email, $mailType,$mailParams);

		// Send mail for review End
	   return $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => true,
        'parentRefresh' => false,
        'messages' => array('Your review has been submitted.')
      ));
	}
}
  
  // UPDATE COVER PIC
  public function uploadAction(){
	 
		$user_id =	$this->_getParam('id');
		$user	 = $this->view->user	=	Engine_Api::_()->getItem('user', $user_id);
        $table   = Engine_Api::_()->getDbTable('settings', 'core');
		$this->view->item_type = $item_type	= $this->_getParam('item_type');
		
        $this->view->form = $form = new User_Form_Photo_Upload();
        if (!$this->getRequest()->isPost()) {
            return;
        }

        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        // Uploading a new photo
        if ($form->Filedata->getValue() !== null) {
            $db = $user->getTable()->getAdapter();
            $db->beginTransaction();

            try {
                $fileElement = $form->Filedata;
				if($item_type == 'cover'){
					$user->setPhoto($fileElement, 'wall');
					$iMain = Engine_Api::_()->getItem('storage_file', $user->wallphoto_id);
					$action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($user, $user, 'profile_photo_update',
				  '{item:$subject} added a new cover photo.');
		
					// Hooks to enable albums to work
					if( $action ) {
					  $event = Engine_Hooks_Dispatcher::_()
						->callEvent('onUserWallProfilePhotoUpload', array(
							'user' => $user,
							'file' => $iMain,
						  ));
			
					  $attachment = $event->getResponse();
					  if( !$attachment ) $attachment = $iMain;
			
					  // We have to attach the user himself w/o album plugin
					  Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $attachment);
					}

				}else{
					$user->setPhoto($fileElement, 'profile');
					$iMain = Engine_Api::_()->getItem('storage_file', $user->photo_id);
					
					$action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($user, $user, 'profile_photo_update',
				  '{item:$subject} added a new profile photo.');
		
					// Hooks to enable albums to work
					if( $action ) {
					  $event = Engine_Hooks_Dispatcher::_()
						->callEvent('onUserProfilePhotoUpload', array(
							'user' => $user,
							'file' => $iMain,
						  ));
			
					  $attachment = $event->getResponse();
					  if( !$attachment ) $attachment = $iMain;
			
					  // We have to attach the user himself w/o album plugin
					  Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $attachment);
					}
				}
	
                $db->commit();
                $this->view->photo_id = $attachment->getIdentity();
            } catch (Engine_Image_Adapter_Exception $e) {
                $db->rollBack();
                $form->addError(Zend_Registry::get('Zend_Translate')->_('The uploaded file is not supported or is corrupt.'));
            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }
        }  
  }
  
  // REMOVE COVER PIC
  
  public function removephotoAction()
  {
    // Get form
	
	$this->view->form = $form = new User_Form_Photo_Remove();
	$del_cover = false;

    if( !$this->getRequest()->isPost() || (!$form->isValid($this->getRequest()->getPost())) )
    {
      return;
    }
	
	$user_id = $this->_getParam('id');
	$user = Engine_Api::_()->getItem('user', $user_id);
	$user->wallphoto_id = 0; 
    $user->save();
	$del_cover = true;
    
    $this->view->status = true;
    $this->view->message = Zend_Registry::get('Zend_Translate')->_('Your wall photo has been removed.');

    /*$this->_forward('success', 'utility', 'core', array(
      //'smoothboxClose' => true,
      //'parentRefresh' => true,
      'messages' => array(Zend_Registry::get('Zend_Translate')->_('Your cover photo has been removed.'))
    ));*/
	$this->view->del_cover	=	$del_cover;
	$this->view->photo_id	=	'';
  }
  
  // GET USER PIC
  public function getuserpicAction(){
	  $this->_helper->content->setNoRender();
        ; 
	  $uid	= $this->_getParam('id');
	  $type	= $this->_getParam('item_type');
	  if($type == 'cover'){
		  echo  $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserWallPicPath($uid);
	  }else if($type == 'dp'){
		   echo  $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($uid);
	  }
	  die;
		 
 }
 
 //GET USER ALBUMS
  public function getuseralbumsAction(){
	  $user_id =	$this->_getParam('id');
	  $this->_helper->layout()->disableLayout(); 
	   $this->view->user	= $user	=	Engine_Api::_()->getItem('user', $user_id);
	  $this->view->paginator = $paginator = Engine_Api::_()->getItemTable('album')
      ->getAlbumPaginator(array('owner' => $user));
  }
 
  
}