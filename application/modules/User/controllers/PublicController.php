<?php
/**
 * SocialEngine 
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: PublicController.php 10259 2014-06-04 21:43:01Z lucas $
 * @author     Priyanka
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_PublicController extends Core_Controller_Action_Standard
{
  public function init()
  {     }
  
  public function suggestAction()
  {
	// Get table info
    $table = Engine_Api::_()->getItemTable('user');
    $userTableName = $table->info('name');

    $searchTable = Engine_Api::_()->fields()->getTable('user', 'search');
    $searchTableName = $searchTable->info('name');

    $searchTable2 = Engine_Api::_()->getDbtable('clientids', 'user');
    $searchTableName2 = $searchTable2->info('name');
	
	$jobTable = Engine_Api::_()->getItemTable('classified');
    $jobTableName = $jobTable->info('name');
	
	$companyTable = Engine_Api::_()->getItemTable('page');
    $comapnyTableName = $companyTable->info('name');
	
	$displayname = $_POST['value'];
	//echo $displayname;
	
	// Contruct query
   $select = $table->select()
      //->setIntegrityCheck(false)
      ->from($userTableName)
      ->joinLeft($searchTableName, "`{$searchTableName}`.`item_id` = `{$userTableName}`.`user_id`", null)
      ->joinLeft($searchTableName2, "`{$searchTableName2}`.`user_id` = `{$userTableName}`.`user_id`", null)
      //->joinLeft("engine4_user_clientids", "`engine4_user_clientids`.`user_id` = `{$userTableName}`.`user_id`", null)
      //->group("{$userTableName}.user_id")
      ->where("{$userTableName}.search = ?", 1)
      ->where("{$userTableName}.enabled = ?", 1);
 
    $searchDefault = true;  

    // Add displayname
	if( !empty($displayname) ) {
      $select->where("(`{$searchTableName}`.`first_name` LIKE ? || `{$userTableName}`.`username` LIKE ? || `{$userTableName}`.`displayname` LIKE ? || `{$searchTableName2}`.`clientcode` LIKE ?)", "%{$displayname}%");
      $searchDefault = false;
    }

    // Build search part of query
    $searchParts = Engine_Api::_()->fields()->getSearchQuery('user', $options);
    foreach( $searchParts as $k => $v ) {
      $select->where("`{$searchTableName}`.{$k}", $v);
      
      if(isset($v) && $v != ""){
        $searchDefault = false;
      }
    }
    
    if($searchDefault){
      $select->order("{$userTableName}.lastlogin_date DESC");
    } else {
      $select->order("{$userTableName}.displayname ASC");
    }
	
	$result = $table->fetchAll($select);
	
	//echo '<pre>'; print_r($result); die;
	$view = Zend_Registry::get('Zend_View');
	$view->getHelper('itemPhoto')->getNoPhoto($subject, 'thumb.icon');
	foreach( $result as $friend ) {
        $data[] = array(
          'type'  => 'user',
          'id'    => $friend->getIdentity(),
          'guid'  => $friend->getGuid(),
          'label' => $friend->getTitle(),
         // 'photo' => $this->view->itemPhoto($friend, 'thumb.icon'),
		      'photo' => $this->view->itemPhoto($friend, 'thumb.icon'),
          'url'   => $friend->getHref(),
        );
        $ids[] = $friend->getIdentity();
        $friend_data[$friend->getIdentity()] = $friend->getTitle();
      }
	  
	  
	  // SEARCH QUERY FOR JOBS -----------------------
	 $selectjob = $jobTable->select()
      //->setIntegrityCheck(false)
      ->from($jobTableName)
	  ->joinLeft("engine4_user_fields_search", "`engine4_user_fields_search`.`item_id` = `{$jobTableName}`.`owner_id`", null)
	   ->joinLeft("engine4_classified_fields_search", "`engine4_classified_fields_search`.`item_id` = `{$jobTableName}`.`classified_id`", null)
      //->group("{$userTableName}.user_id")
      ->where("{$jobTableName}.search = ?", 1)
	  //->where("{$jobTableName}.owner_id != ?", $viewer->getIdentity())
	  ;
      if($profile_type && $profile_type !='6' && $profile_type !='7'){
			$selectjob->where("engine4_user_fields_search.profile_type = ?", $profile_type) ; 
	  }
	  //$selectjob->group("engine4_user_fields_values.item_id"); 
	 
    $jobsearchDefault = true;  
      
    // Add displayname
    if( !empty($displayname) ) {
      $selectjob->where("(`{$jobTableName}`.`title` LIKE ? || `{$jobTableName}`.`body` LIKE ?)", "%{$displayname}%");
      $jobsearchDefault = false;
    }

	
    if($jobsearchDefault){
      $selectjob->order("{$jobTableName}.creation_date DESC");
    } else {
      $selectjob->order("{$jobTableName}.title ASC");
    }
	
	$resultjob = $table->fetchAll($selectjob);
	
	
	
	$view = Zend_Registry::get('Zend_View');
	$view->getHelper('itemPhoto')->getNoPhoto($subject, 'thumb.icon');
	foreach( $resultjob as $friend_classified) {
		$friend = Engine_Api::_()->getItem('classified', $friend_classified['classified_id']);
        $data[] = array(
          'type'  => 'user',
          'id'    => $friend->getIdentity(),
          'guid'  => $friend->getGuid(),
          'label' => $friend->getTitle(),
		  'photo' => $this->view->itemPhoto($friend, 'thumb.icon'),
          'url'   => $friend->getHref(),
        );
        $ids[] = $friend->getIdentity();
        $friend_data[$friend->getIdentity()] = $friend->getTitle();
      }
	  
	  
	  
	  // SEARCH QUERY FOR COMPANIES -----------------------
	$selectcompany = $companyTable->select()
      ->from($comapnyTableName)
      ->joinLeft("engine4_user_fields_search", "`engine4_user_fields_search`.`item_id` = `{$comapnyTableName}`.`user_id`", null)
	  //->where("{$comapnyTableName}.user_id != ?", $viewer->getIdentity())
	  ->where("{$comapnyTableName}.is_draft = ?", 0);
    	if($profile_type && $profile_type !='6' && $profile_type !='7'){
			$selectcompany->where("engine4_user_fields_search.profile_type = ?", $profile_type); 
	  }
     $companysearchDefault = true;  
     
    // Add displayname
    if( !empty($displayname) ) {
      $selectcompany->where("( `{$companyTableName}`.`services` LIKE ? || `{$companyTableName}`.`title` LIKE ? || `{$companyTableName}`.`displayname` LIKE ? || `{$companyTableName}`.`description` LIKE ?)", "%{$displayname}%");
      $companysearchDefault = false;
    }

    if($companysearchDefault){
      $selectcompany->order("{$companyTableName}.creation_date DESC");
    } else {
      $selectcompany->order("{$companyTableName}.displayname ASC");
    }
	
	
	$resultcompany = $table->fetchAll($selectcompany);
	


	
	$view = Zend_Registry::get('Zend_View');
	$view->getHelper('itemPhoto')->getNoPhoto($subject, 'thumb.icon');
	foreach( $resultcompany as $friend_company) {
		$friend = Engine_Api::_()->getItem('page', $friend_company['page_id']);

        $data[] = array(
          'type'  => 'user',
          'id'    => $friend->getIdentity(),
          'guid'  => $friend->getGuid(),
          'label' => $friend->getTitle(),
		      'photo' => $this->view->itemPhoto($friend, 'thumb.icon'),
          'url'   => $friend->getHref(),
        );
        $ids[] = $friend->getIdentity();
        $friend_data[$friend->getIdentity()] = $friend->getTitle();
      }
	 /* echo '<pre>';
    print_r($data);
    die();*/
	 return $this->_helper->json($data);
  }
  
}
