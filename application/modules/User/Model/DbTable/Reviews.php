<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ListItems.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Reviews extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Review';
  
  public function getReviews($user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('is_deleted=?', 0)
		   ->where('is_approved=?', 1)
		   ->order('review_id DESC');
     $result	=	$this->fetchAll($select);
	 
	 return $result;
 }
 
 public function checkHasReviewed($user_id, $reviewer_id)
	{
		$select = $this -> select();
		$select
		-> where('reviewer_id = ?', $reviewer_id)
		-> where('user_id = ?', $user_id)
		-> where('is_deleted = ?', 0)
		-> limit(1);
		
		$row =  $this -> fetchRow($select);
		if($row)
		{
			return true;
		}
		return false;
	}
	
 public function getAllReviewsByResourceId($review_id)
	{
		$select = $this -> select();
		$select -> where("review_id = ?", $review_id);
		$select -> where("is_deleted = ?", 0);
		return $this -> fetchAll($select);
	}
	
	public function getReviewSelect($params)
	{
		$reviewTbl = Engine_Api::_()->getItemTable("review");
		$reviewTblName = $reviewTbl->info('name');
		
		$select = $reviewTbl->select()
		-> setIntegrityCheck(false)
		-> from ($reviewTblName)
		-> where("{$reviewTblName}.is_deleted = ?", 0);
		
		if( !empty($params['user_id']) && is_numeric($params['user_id']) )
		{
			$select->where("{$reviewTblName}.user_id = ?", $params['user_id']);
		}
		if( !empty($params['user']) && $params['user'] instanceof User_Model_User )
		{
			$select->where("{$reviewTblName}.user_id = ?", $params['user']->getIdentity());
		}
		if( !empty($params['users']) )
		{
			$str = (string) ( is_array($params['users']) ? "'" . join("', '", $params['users']) . "'" : $params['users'] );
			$select->where("{$reviewTblName}.user_id in (?)", new Zend_Db_Expr($str));
		}
		//reviewer
		if( !empty($params['resource_id']))
		{
			$select->where("{$reviewTblName}.resource_id in (?)", $params['resource_id']);
		}
		//review for
		if( !empty($params['user_id']))
		{
			$select->where("{$reviewTblName}.user_id in (?)", $params['user_id']);
		}	
		//title
		if( !empty($params['title']))
		{
			$select->where("{$reviewTblName}.title LIKE ?", '%'.$params['title'].'%');
		}
		//from date	
		if( !empty($params['from_date']) )
		{
			$select->where("{$reviewTblName}.review_date >= ?", date('Y-m-d H:i:s', strtotime($params['from_date'])));
		}
		//to date
		if( !empty($params['to_date']) )
		{
			$select->where("{$reviewTblName}.review_date <= ?", date('Y-m-d H:i:s', strtotime($params['to_date'])));
		}
		// Could we use the search indexer for this?
		if( !empty($params['keyword']) )
		{
			$select->where("{$reviewTblName}.title LIKE ? OR {$reviewTblName}.summary LIKE ?", '%'.$params['keyword'].'%');
		}
		// rating filter
		if( !empty($params['filter_rating']) && $params['filter_rating'] != '-1' )
		{
			$select->where("{$reviewTblName}.rating = ?", $params['filter_rating']);
		}
		// ordering
		
		
		$select -> order( "{$reviewTblName}.review_date DESC" );
		return $select;
	}
	
	public function getReviewPaginator($params)
	{
		//echo $params['page'];die;
		$paginator = Zend_Paginator::factory($this->getReviewSelect($params));
		if( !empty($params['page']) )
		{
			$paginator->setCurrentPageNumber($params['page']);
		}
		if( !empty($params['limit']) )
		{
			$paginator->setItemCountPerPage($params['limit']);
		}
		if( empty($params['limit']) )
		{
			$page = (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('ynmember.page', 20);
			$paginator->setItemCountPerPage($page);
		}
		return $paginator;
	}

	public function getAVGuserrating($user_id)
	{
		//$select = $this -> select("ROUND(AVG(rating),1) AS avgrating");
		$select = $this -> select("ROUND(AVG(rating),1) AS avgrating");
		$select->columns(array('avgrating'=> "ROUND(AVG(rating),1)"));		
		$select -> where("user_id = ?", $user_id);
		//$select -> where("is_deleted = ?", 0);
		//echo 'select'.$select;
		$rating=$this -> fetchRow($select)->avgrating;
		//echo "<pre>";
		//print_r($rating);die();
		return $rating;
	}
	public function getuserratingByReviewer($reviwer_id)
	{
		//$select = $this -> select("ROUND(AVG(rating),1) AS avgrating");
		$select = $this -> select("rating");	
		$select -> where("user_id = ?", $user_id);
		$select -> where("reviwer_id = ?", $reviwer_id);
		//echo 'select'.$select;
		$rating=$this -> fetchRow($select)->rating;
		//echo "<pre>";
		//print_r($rating);die();
		return $rating;
	}

}