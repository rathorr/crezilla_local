<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ListItems.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Contactinfos extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Contactinfos';
  
  public function userContactinfos($user_id){

	  $select	= $this->select()
	       ->where('owner_id=?', $user_id)
		   ->order('contactinfos_id ASC');
     $result	=	$this->fetchAll($select);
	 
	 return $result;
 }

  public function updatecontactinfos($values,$user_id){
 	//echo $values['contactnooffice'];
 	//echo $user_id;
 	//die();
    $where = array(
      '`owner_id` = ?' => $user_id
    );

    $result=$this->update($values, $where);

    return $result;
	}
 
  
}