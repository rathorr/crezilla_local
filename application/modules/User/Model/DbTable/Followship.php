<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Membership.php 10088 2013-09-19 13:34:37Z andres $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Followship extends Core_Model_DbTable_Followship
{
  protected $_type = 'user';
  
  public function addMember(Core_Model_Item_Abstract $resource, User_Model_User $user)
  {
    parent::addMember($resource, $user);
  
    return $this;
  }

  public function removeMember(Core_Model_Item_Abstract $resource, User_Model_User $user)
  {
    parent::removeMember($resource, $user);

    return $this;
  }

	public function removeAllUserFollowship(User_Model_User $user)
  {
    // first get all cases where user_id == $user->getIdentity
    $select = $this->getTable()->select()
      ->where('user_id = ?', $user->getIdentity());
    
    $followships = $this->getTable()->fetchAll($select);
    foreach( $followships as $followship ) {
     
      $followship->delete();
    }

    // get all cases where resource_id == $user->getIdentity
    // remove all   
    $this->getTable()->delete(array(
      'resource_id = ?' => $user->getIdentity()
    ));
  }
  
  public function getUserFollowList($user_id)
  {
		$query ="select * from engine4_user_followship as f inner join engine4_users as u on u.user_id=f.resource_id where f.resource_id='".$user_id."' and f.user_approved = 1";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
		
  }
}
