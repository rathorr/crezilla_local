<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ListItems.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Industryexperiences extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Industryexperiences';
  
public function userIndustryexperiences($user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('is_deleted=?', 0)
		   ->order('industryexperiences_id ASC');
     $result	=	$this->fetchAll($select);
	 
	 return $result;
 }
 
  public function getuserexpbyiexpprimaryid($ind_primary_id, $user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('ind_primary_id=?', $ind_primary_id)
		   ->where('is_deleted=?', 0)
		   ->order('industryexperiences_id DESC');
     $result	=	$this->fetchAll($select);
	 return ($result)?$result[0]['industryexperiences_id']:false;
	 
	 //return $result;
 }
public function checkifexist($indus_ids,$user_id){
 $select	=	$this->select("t.user_id")
			->from ('engine4_user_industryexperiences as t')
			->setIntegrityCheck(false)
			->where("t.ind_primary_id = $indus_ids", 1)
			->where("t.user_id = $user_id", 1);

    $userdata = $this->fetchRow($select);
	return $userdata;
}
 
 public function updatecurstatus($currentstatus_title,$cur_primary_id,$user_id){
 	$data = array(
    'currentstatus_title'      => $currentstatus_title,
    'cur_primary_id'      => $cur_primary_id
	);
    $where = array(
      '`user_id` = ?' => $user_id
    );

    $this->update($data, $where);

    return $this;
	}

	public function deletebyid($indid,$user_id)
	 {
	 	$condition = array(
	    'ind_primary_id = ?' => $indid,
	    'user_id = ?' => $user_id
		);
		
		$result = $this->delete($condition);
		return $result;
	 }
 
}