<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ListItems.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Skills extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Skill';
  
  public function userSkills($user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('is_deleted=?', 0)
		   ->order('skill_title ASC');
     $result	=	$this->fetchAll($select);
	 
	 return $result;
 }
 
  public function getuserskillbyskilprimaryid($skil_primary_id, $user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('skill_primary_id=?', $skil_primary_id)
		   ->where('is_deleted=?', 0)
		   ->order('skill_id DESC');
     $result	=	$this->fetchAll($select);
	 return ($result)?$result[0]['skill_id']:false;
	 
	 //return $result;
 }
 
 
 public function userSkillsEndorsements($user_id){
		 $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('is_deleted=?', 0)
		   ->order('skill_id DESC');
     $result	=	$this->fetchAll($select);
	 $final_arr	=	array();
	 $endor_arr	=	array();
	 if($result){
		 $endorsement	=	'';
		 $endorse	=	'';
		foreach($result as $res){
				$final['skill_title']		=	$res['skill_title'];
				$final['skill_id']	=	$res['skill_id'];
				$endorsement	=	Engine_Api::_()->getDbtable('endorsements', 'user')->getSkillEndorsement($res['user_id'], $res['skill_id']);
				
				$i=0;
				$endor_arr	=	'';
				if($endorsement){
					foreach($endorsement as $endorse){
						$i++;
						$end_arr['user_id']	= $endorse['user_id'];
						$end_arr['endorsement_id']	= $endorse['endorsement_id'];
						$endor_arr[]	=	$end_arr;
						//echo '<pre>'; print_r($endor_arr); echo '</pre>';
					}
				}
				$final['total_endorse']	=	$i;
				$final['endorse']		=	$endor_arr;
				
				$final_arr[]	=	$final;
				
				
		}	 
	 }
	
	 //echo '<pre>';print_r($final_arr); echo '</pre>';
	 return $final_arr; 
	
 }
 
 // add/edit/del skills direct from clasifie skills
 public function addeditskills($skill, $action, $skill_id, $old_skill=null){
	 if($action == 'add'){
	 	$query ="insert into engine4_user_fields_options (field_id, label, skil_id_custom) values('433', '".$skill."', '".$skill_id."')";
	 }elseif($action == 'edit'){
	 	$query ="update engine4_user_fields_options set label='".$skill."' where field_id='433' and skil_id_custom='".$skill_id."'";
	 }else{
		$query ="delete from engine4_user_fields_options where field_id='433' and skil_id_custom='".$skill_id."'"; 
	 }
		  
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$db->query($query);
 }
 	
	public function addeditcustomskills($skill, $action, $user_id, $skill_id){
		$query ="select option_id from engine4_user_fields_options where field_id='433' and skil_id_custom = '".$skill_id."'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		$stmt = $db->query($query);
        $result = $stmt->fetchAll();
		
		 $uquery ="select * from engine4_user_fields_values where field_id='433' and item_id='".$user_id."' order by `index` desc limit 1";	
		$stmt1 = $db->query($uquery);
        $result1 = $stmt1->fetchAll();
		$findex = '';
		
		if($result1){
			$result1 = $result1[0];
			 $rindex = $result1['index'];
			 $findex = ($result1['index'] !='')?($result1['index']+1) : '1';
		}
		 
		if($result){
			$result = $result[0];
			if($action == 'add'){
				$inquery ="insert into engine4_user_fields_values (`field_id`, `index`, `value`, `item_id`) values('433', '".$findex."', '".$result['option_id']."', '".$user_id."')";	
			}else{
				$inquery ="delete from engine4_user_fields_values where field_id='433' and value='".$result['option_id']."' and item_id='".$user_id."'"; 	
			}
		}
		
		$db->query($inquery);
	}
	
	// add/edit/del skills direct from clasifie skills
 public function addskills(){
	 $query ="select * from engine4_classified_skills";
		  
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$db->query($query);
		$result = $stmt->fetchAll();
		if($result){
			foreach($result as $res){
					$inquery ="insert into engine4_user_fields_options (field_id, label, skil_id_custom) values('433', '".$res['skill_name']."', '".$res['skill_id']."')";
					$db->query($inquery);	
			}	
		}
 }

public function checkifexist($skillsids,$user_id){
	 $select = $this->select("t.user_id")
				->from ('engine4_user_skills as t')
				->setIntegrityCheck(false)
				->where("t.skill_primary_id = $skillsids", 1)
				->where("t.user_id = $user_id", 1);

	    $userdata = $this->fetchRow($select);
		return $userdata;
	}
	public function deletebyid($funcsids,$user_id)
	 {
	 	$condition = array(
	    'skill_primary_id = ?' => $funcsids,
	    'user_id = ?' => $user_id
		);
		
		$result = $this->delete($condition);
		return $result;
	 }
}