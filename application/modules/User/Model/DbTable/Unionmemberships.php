<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Unionmemberships extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Unionmemberships';
  public function getUnionmembershipPaginator($options = array())
  {
    return Zend_Paginator::factory($this->getUnionmembershipSelect($options));
  }
  public function getmembershiplist($userid)
  {
    $select = $this->select()
        ->where("owner_id = ?",$userid)
        ->where("row_status = ?", 1)
        ->order("creation_date desc");
  return $result  = $this->fetchAll($select);
  }
}