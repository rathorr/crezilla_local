<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Professionalsummarys extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Professionalsummarys';

  public function getProfessionalsummarysPaginator($options = array())
  {
    return Zend_Paginator::factory($this->getProfessionalsummarysSelect($options));
  }

  public function getProfessionalsummarys($userid)
  {
  	$select = $this->select("summary")
               ->setIntegrityCheck(false)
              ->where('owner_id=?', $userid);
     $result  = $this->fetchRow($select)->summary;
    return $result;
  }
}