<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Transactions.php 27.07.11 15:15 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class User_Model_DbTable_Shortlistfolders extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Shortlistfolder';
	
  public function getShortlistFolders($user_id){
		$select	= $this->select()
	       				->where('user_id=?', $user_id)
		   				->where('status = ?', 1);
		 $result	=	$this->fetchAll($select);
		 $data	=	array();
		 $data[] = 'New Shortlist';
		 if(count($result)>0){
		 		foreach( $result as $res ) {
				  $data[$res['shortlistfolder_id']] = $res['folder_name'];
				}
		 }
		 
		 return $data;
  }
}