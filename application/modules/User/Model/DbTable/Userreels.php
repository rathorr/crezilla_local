<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Transactions.php 27.07.11 15:15 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class User_Model_DbTable_Userreels extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Userreel';
	
	public function getUserReels($user_id)
	{
		if (!$user_id){
			return false;
		}
		
		$select = $this->select()
      	->where('user_id = ?',$user_id)
		->where('is_deleted = ?',0)
      	;
		
		$result	=	 $this->fetchAll($select);
		$maindata=	array();
		if($result){
			foreach($result as $res){
				$data['userreel_id']	=	$res['userreel_id'];
				$data['reel_path']	=	$res['reel_path'];
				$data['user_id']	=	$res['user_id'];
				$data['is_deleted']	=	$res['is_deleted'];
				$data['created_on']	=	$res['created_on'];	
				$maindata[]	=	$data;
			}	
		}
		return $maindata;
	}
}