<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Users.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Users extends Engine_Db_Table
{
  protected $_name = 'users';

  protected $_rowClass = 'User_Model_User';
  
  public function getUserCategory($uid){
	/*$select = $this->select('value')
		->setIntegrityCheck(false)
		->from ('engine4_user_fields_values') 
        ->where('item_id = ?', $uid)
		->where('field_id = ?', 38);*/
		
	$select	=	$this->select("t.value")
			->from ('engine4_user_fields_values as t')
			->setIntegrityCheck(false)
			->where("t.item_id = $uid", 1)
			->where('t.field_id = 38', 1)
			->joinLeft("engine4_user_fields_options as t2", "t2.option_id = t.value", array('label'));

    $userdata = $this->fetchRow($select);
	return $userdata;
 }
 
   public function getUserCompany($uid){ 
	
	$select	=	$this->select("t.value")
			->from ('engine4_user_fields_values as t')
			->setIntegrityCheck(false)
			->where("t.item_id = $uid", 1)
			->where('t.field_id = 36', 1)
			->joinLeft("engine4_user_fields_options as t2", "t2.option_id = t.value", array('label'));

    $userdata = $this->fetchRow($select);
	return $userdata;
 }
 
  public function getUserName($uid){ 
	
	$select	=	$this->select("t.value")
			->from ('engine4_user_fields_values as t')
			->setIntegrityCheck(false)
			->where("t.item_id = $uid", 1)
			->where('t.field_id = 36', 1)
			->joinLeft("engine4_user_fields_options as t2", "t2.option_id = t.value", array('label'));

    $userdata = $this->fetchRow($select);
	return $userdata;
 }
 
 
 public function getUserCategoryForListing($uid,$fid){
	$query=Engine_Db_Table::getDefaultAdapter()->select()
					  ->from('engine4_user_fields_values')
					  ->where('item_id = ?', $uid)
					  ->where('field_id = ?', $fid);

	$query= $query->query()->fetchAll();
	
	return $query;
 }
 

   public function getUserTalent($uid){
		
	 $select	=	$this->select()
			->from ('engine4_user_fields_values as t')
			->setIntegrityCheck(false)
			->joinLeft("engine4_user_fields_options as t2", "t2.option_id = t.value", array('label'))
			->where("t.item_id = $uid", 1)
			->where('t.field_id = 38', 1)
			;
    $final_arr	=	'';
    $result = $select->query()->fetchAll();
	if($result){
		$arr	=	array();
		$i=0;
		foreach($result as $res){
				$arr['field_id']	=	$res['field_id'];
				$arr['values']	=	$res['label'];
				
				
				if($res['field_id'] == $field_id){
					$arr['values']	.= ', '.$result[$i-1]['label'];	
						
				}
				if($final_arr[$i-1]['field_id'] == $res['field_id']){
					unset($final_arr[$i-1]);
				}
				$final_arr[]	=	$arr;
				$field_id	=	$res['field_id'];
		$i++;
		}
		$key	=	array_keys($final_arr);
		$key	=	$key[0];
		
		$final_arr	= $final_arr[$key]['values'];	
	}//die;
	return $final_arr;
 }
 
 public function getUserLocation($uid){
		
	 $select	=	$this->select()
			->from ('engine4_user_fields_values as t')
			->setIntegrityCheck(false)
			->joinLeft("engine4_user_fields_options as t2", "t2.option_id = t.value", array('label'))
			->where("t.item_id = $uid", 1)
			->where('t.field_id = 39', 1)
			;
	$final_arr	=	'';
    $result = $select->query()->fetchAll();
	if($result){
		$result = $result[0];
		$final_arr	= $result['value'];	
	}//die;
	return $final_arr;
 
  }
  
   public function getUserAbout($uid){
		
	 $select	=	$this->select()
			->from ('engine4_user_fields_values as t')
			->setIntegrityCheck(false)
			->joinLeft("engine4_user_fields_options as t2", "t2.option_id = t.value", array('label'))
			->where("t.item_id = $uid", 1)
			->where('t.field_id = 13', 1)
			;
	$final_arr	=	'';
    $result = $select->query()->fetchAll();
	if($result){
		$result = $result[0];
		$final_arr	= $result['value'];	
	}//die;
	return $final_arr;
 
  }
 
 public function getUserProfilePicPath($uid){
	 $select	=	$this->select('s.storage_path')
			->from ('engine4_storage_files as s')
			->setIntegrityCheck(false)
			->joinLeft("engine4_users as u", "s.file_id = u.photo_id")
			->where("u.user_id = $uid", 1)
			;
	 $photo_url	=	'';
	 $result = $select->query()->fetchAll();
	if($result){
		$result		=	$result[0];
	    $photo_url  =	$result['storage_path'];
	
	}
	
	return ($photo_url=='')?Zend_Controller_Front::getInstance()->getBaseUrl().'/application/modules/User/externals/images/nophoto_user_thumb_icon.png':$photo_url;
 }
 
 public function getUserWallPicPath($uid){
	 $select	=	$this->select('s.storage_path')
			->from ('engine4_storage_files as s')
			->setIntegrityCheck(false)
			->joinLeft("engine4_users as u", "s.file_id = u.wallphoto_id")
			->where("u.user_id = $uid", 1)
			;
	 $photo_url	=	'';
	 $result = $select->query()->fetchAll();
	if($result){
		$result		=	$result[0];
	    $photo_url  =	$result['storage_path'];
	
	}
	return $photo_url;
 }	
 
  public function getUserRandomWallPicPath(){
    $select = $this->select("storage_path")
    			->from ('engine4_randomstorage_files')
               	->setIntegrityCheck(false)
              	->where('type=?', "wallphoto")
              	->limit(1)
              	->order('RAND()');
     $result  = $this->fetchRow($select)->storage_path;
     return $result;   
 }
 public function getUserJobDesigPicPath($desig){
 	//echo $desig;
 	
    $select = $this->select("storage_path")
    			->from ('engine4_randomstorage_files')
               	->setIntegrityCheck(false)
              	->where('type=?', $desig)
              	->limit(1);
     $result  = $this->fetchRow($select)->storage_path;
     return $result;   
 }

 public function getUserPageProfilePicPath($uid, $photo_id, $page_id){
	 
	 $query ="SELECT storage_path FROM engine4_storage_files WHERE user_id = '".$uid."' AND file_id = '".$photo_id."' AND parent_type = 'page' AND parent_id = '".$page_id."'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$db->query($query);
		
		$stmt = $db->query($query);
        $result = $stmt->fetchAll();
		if($result){
			$result		=	$result[0];
	    	$photo_url  =	$result['storage_path'];
		}
		//echo $photo_url;die;
		return $photo_url;
		
 }
 
 
 /***************************************************/
 public function getUserProfileType($uid){
		
	 $select	=	$this->select()
			->from ('engine4_user_fields_values as t')
			->setIntegrityCheck(false)
			->joinInner("engine4_user_fields_options as t2", "t2.option_id = t.value", array('label'))
			->where("t.item_id = $uid", 1)
			->where('t.field_id = 1', 1)
			;
	$final_arr	=	'';
    $result = $select->query()->fetchAll();
	if($result){
		$result = $result[0];
		$final_arr	= $result['label'];	
	}//die;
	return $final_arr;
 
  }	 
  
   public function getUserProfileInfo($uid, $t_id){
		$dateTime		= strtotime(date("d-m-Y"));
	  $select	=	$this->select()
			->from ('engine4_user_producelogs as l')
			->setIntegrityCheck(false)
			->where("l.log_producer_id = $uid", 1)
			->where("l.log_date_time = $dateTime", 1)
			->where("l.log_user_id != $t_id", 1);
			
	$final_arr	=	array();

    $result = $select->query()->fetchAll();
	if(count($result)>0){
		
		$final_arr	= $result;	
	}
	return $final_arr;
 
  }
  
     public function getUserSearchLimit($uid){
	/* $select	=	$this->select()
			->from ('engine4_user_producelog as l')
			->setIntegrityCheck(false)
			->where("t.log_producer_id = $uid", 1)
			->where("t.log_user_id != $t_id", 1);
	$final_arr	=	array();
    $result = $select->query()->fetchAll();
	if($result){
		$result = $result[0];
		$final_arr	= $result['label'];	
	}//die;
	*/
	$final_arr =3;
	return $final_arr;
  }
  
	public function setUserSearchInfo($data)
	{

		$table 			= Engine_Api::_()->getDbtable('producelog', 'user');
		$table->insert($data);
	}
  
  
  	public function getUserFullName($uid){
	
	$select	=	$this->select("t.first_name, t.field_66, t.last_name")
			->from ('engine4_user_fields_search as t')
			->setIntegrityCheck(false)
			->where("t.item_id = $uid", 1)
			;

    $userdata = $this->fetchRow($select);
	return ($userdata)? $userdata->first_name.' '.$userdata->field_66.' '.$userdata->last_name:false;
 }
 
 public function getUserFirstName($uid){
	
	$select	=	$this->select("t.first_name")
			->from ('engine4_user_fields_search as t')
			->setIntegrityCheck(false)
			->where("t.item_id = $uid", 1)
			;

    $userdata = $this->fetchRow($select);
	return ($userdata)? $userdata->first_name:false;
 }
 
 public function getUserLastName($uid){
	
	$select	=	$this->select("t.last_name")
			->from ('engine4_user_fields_search as t')
			->setIntegrityCheck(false)
			->where("t.item_id = $uid", 1)
			;

    $userdata = $this->fetchRow($select);
	return ($userdata)? $userdata->last_name:false;
 }
 
 public function getUserMobileNo($uid){
	
	$select	=	$this->select("t.value")
			->from ('engine4_user_fields_values as t')
			->setIntegrityCheck(false)
			->where("t.item_id = $uid", 1)
			->where("t.field_id = 69", 1)
			;

    $userdata = $this->fetchRow($select);
	return ($userdata)? $userdata->value:false;
 }
 
 public function getUseryrexp($uid){
	
	$select	=	$this->select("t.value")
			->from ('engine4_user_fields_values as t')
			->setIntegrityCheck(false)
			->where("t.item_id = $uid", 1)
			->where("t.field_id = 431", 1)
			;

    $userdata = $this->fetchRow($select);
	return ($userdata)? $userdata->value:false;
	//return ($userdata)? (($userdata->value>1)?$userdata->value.' years':$userdata->value.' year'):false;
 }
 
  public function getAllLang(){
	
	$query ="select label, option_id from engine4_user_fields_options where field_id=421 order by label asc";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
	
 }
 
  public function getUserycity($uid){
	
	$select	=	$this->select("t.value")
			->from ('engine4_user_fields_values as t')
			->setIntegrityCheck(false)
			->where("t.item_id = $uid", 1)
			->where("t.field_id = 458", 1)
			;

    $userdata = $this->fetchRow($select);
	return ($userdata)? $userdata->value:" ";
	
 }

 public function getAllUserByViews()
 {
 	$select	=	$this->select()
			->from ('engine4_users')
			->setIntegrityCheck(false)
			->where("user_id != 1", 1)
			->where("photo_id != 0", 1)
			->order('view_count DESC')
			->limit(8);
			//echo($select);
    $userdata = $this->fetchAll($select);
    //echo '<pre>'; print_r($userdata); die();
	return $userdata;
 }
}