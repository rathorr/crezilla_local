<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: Transactions.php 27.07.11 15:15 taalay $
 * @author     Taalay
 */

/**
 * @category   Application_Extensions
 * @package    Page
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 */

class User_Model_DbTable_Shortlists extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Shortlist';
	public function isShortlist($user_id, $shortlist_user_id)
	  {
		 $select	= $this->select()
							->where('user_id=?', $user_id)
							->where('shortlist_user_id=?', $shortlist_user_id)
							->where('status =?', 1);
		 $result	=	$this->fetchAll($select);
		 
		 return (count($result)>0) ? TRUE: FALSE;
	  }
	  
	 public function removeshortlist($user_id, $shortlist_user_id){
		 $query ="update engine4_user_shortlists set status=0 where user_id='".$user_id."' and shortlist_user_id='".$shortlist_user_id."'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$db->query($query);
	 }
	
}