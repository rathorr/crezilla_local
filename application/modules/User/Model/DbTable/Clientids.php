<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     Rajesh
 */

/**
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Clientids extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Clientids';

  public function userClientcode($code){
    $select = $this->select()
         ->where('clientcode=?', $code)
       ->order('clientids_id ASC');
     $result  = $this->fetchRow($select);
  //  echo $result;
  //  print_r($result);
     //die();
   
   return $result;
 }

  public function getClientcode($userid){
    //echo $userid;
    $select = $this->select("clientcode")
               ->setIntegrityCheck(false)
              ->where('user_id=?', $userid);
     $result  = $this->fetchRow($select)->clientcode;
    return $result;

    /*$select = $this->select("t.clientcode")
      ->from ('engine4_user_clientids as t')
      ->setIntegrityCheck(false)
      ->where("t.user_id = $userid", 1);

    $userdata = $this->fetchRow($select);
    print_r($userdata);
  die();
  return $userdata;*/
 }

}