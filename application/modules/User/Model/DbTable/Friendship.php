<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Membership.php 10088 2013-09-19 13:34:37Z andres $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Friendship extends Engine_Db_Table
{
  protected $_type = 'User_Model_Friendship';

  public function isFriend($user_id, $friend_id)
  {
     $select	= $this->select()
	       				->where('user_id=?', $user_id)
						->where('friend_id=?', $friend_id)
		   				->where('status =?', 1);
     $result	=	$this->fetchAll($select);
	 
	 return (count($result)>0) ? TRUE: FALSE;
  }
  
  
  public function isFriendStatus($user_id, $friend_id)
  {
     $select	= $this->select()
	       				->where('user_id=?', $user_id)
						->where('friend_id=?', $friend_id)
		   				->where('status = ?', 0);
     $result	=	$this->fetchAll($select);
	 
	 return (count($result)>0) ? TRUE: FALSE;
  }
  
  public function getRelation($user_id, $friend_id){
	   /*$select	= $this->select()
	       				->where('(user_id='.$user_id.' AND friend_id='.$friend_id.') OR (user_id='.$friend_id.' AND friend_id='.$user_id.')')
		   				->where('status != ?', 2);*/
						
		$select	= $this->select()
	       				->where('(user_id='.$user_id.' AND friend_id='.$friend_id.')')
		   				->where('status != ?', 2);
     $result	=	$this->fetchAll($select);
	 
	 return (count($result)>0) ? $result[0]: FALSE;
  }
  
  public function cancelrequest($user_id, $friend_id){
	  
	  $query ="update engine4_user_friendship set is_canceled=1 ,status=2 where user_id='".$user_id."' and friend_id='".$friend_id."'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$db->query($query);
		
		$query2 ="update engine4_user_friendship set is_canceled=1 ,status=2 where user_id='".$friend_id."' and friend_id='".$user_id."'";
		$db->query($query2);
		
		$query3 ="delete from engine4_activity_notifications where user_id='".$friend_id."' and subject_id='".$user_id."'";
		$db->query($query3);
		
        //$result = $stmt->fetchAll();
		//return $result;
	  
	  //$this->update(array('is_canceled'=>'1', 'status'=>'2'),array("`user_id`"=>$user_id, '`friend_id`' => $friend_id));
	  
	 // $this->update(array('is_canceled'=>'1', 'status'=>'2'),array("`user_id`"=>$friend_id, '`friend_id`' => $user_id));
	 // die;
  }
  
  public function removerequest($user_id, $friend_id){
	  
	  $query ="update engine4_user_friendship set is_removed=1 ,status=2 where user_id='".$user_id."' and friend_id='".$friend_id."'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$db->query($query);
		
		$query2 ="update engine4_user_friendship set is_removed=1 ,status=2 where user_id='".$friend_id."' and friend_id='".$user_id."'";
		$db->query($query2);
	  
	  /*$this->update(array('is_removed'=>'1', 'status'=>'2'),array("user_id"=>$user_id, 'friend_id' => $friend_id));
	  
	  $this->update(array('is_removed'=>'1', 'status'=>'2'),array("user_id"=>$friend_id, 'friend_id' => $user_id));*/
  }
  
  public function rejectrequest($user_id, $friend_id){
	  $query ="update engine4_user_friendship set is_rejected=1 ,status=2 where user_id='".$user_id."' and friend_id='".$friend_id."'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$db->query($query);
		
		$query2 ="update engine4_user_friendship set is_rejected=1 ,status=2 where user_id='".$friend_id."' and friend_id='".$user_id."'";
		$db->query($query2);
		
		$query3 ="delete from engine4_activity_notifications where user_id='".$user_id."' and subject_id='".$friend_id."'";
		$db->query($query3);
		
	  /*$this->update(array('is_rejected'=>'1', 'status'=>'2'),array("user_id"=>$user_id, 'friend_id' => $friend_id));
	  
	  $this->update(array('is_rejected'=>'1', 'status'=>'2'),array("user_id"=>$friend_id, 'friend_id' => $user_id));*/
  }
  
  
  
  public function deleteFriend($user_id, $friend_id){
	  $query ="delete from engine4_user_friendship   where user_id in (".$user_id.", ".$friend_id.") and friend_id  in (".$user_id.", ".$friend_id.")";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$db->query($query);
		
 		
	  /*$this->update(array('is_rejected'=>'1', 'status'=>'2'),array("user_id"=>$user_id, 'friend_id' => $friend_id));
	  
	  $this->update(array('is_rejected'=>'1', 'status'=>'2'),array("user_id"=>$friend_id, 'friend_id' => $user_id));*/
  }
  
  public function confirmrequest($user_id, $friend_id){
	  $query ="update engine4_user_friendship set status=1 where user_id='".$user_id."' and friend_id='".$friend_id."'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$db->query($query);
		
		$query2 ="update engine4_user_friendship set status=1 where user_id='".$friend_id."' and friend_id='".$user_id."'";
		$db->query($query2);
		
	  /*$this->update(array('status'=>'1'),array("user_id"=>$user_id, 'friend_id' => $friend_id));
	  $this->update(array('status'=>'1'),array("user_id"=>$friend_id, 'friend_id' => $user_id));*/
  }
  
  
   public function getUserFriendsList($user_id)
  {
		$query ="select * from engine4_user_membership as f inner join engine4_users as u on u.user_id=f.user_id where f.resource_id='".$user_id."' and f.user_approved = 1";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
		
  }
  
  
   public function pageFollowed($user_id,$page_id)
  {
		  $query ="select * from engine4_core_follow where poster_id='".$user_id."' and resource_id ='".$page_id."'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
		
  }
  
  
  public function pageFollowedAll($user_id)
  {
		  $query ="SELECT resource_id, user_id
				   FROM engine4_core_follow cf
				   INNER JOIN engine4_page_pages pp 
				   ON pp.page_id = cf.resource_id 
				   where poster_id='".$user_id."'  ";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
		
  }
  
  
  public function pageFollowingTotal($page_id)
  {
		  $query ="select count(like_id) as totalfollowing from engine4_core_follow where  resource_id ='".$page_id."'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
		
  }
  
  
   public function followPage($user_id,$page_id)
  {
	  $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
		//end DB setup
		 
		
		   $query = "insert into engine4_core_follow set 
						resource_type='page',
						resource_id='".$page_id."',
						poster_type='user',
						poster_id='".$user_id."' ";
		
		 $stmt = $db->query($query);
       
  }
   public function removepageFollowed($user_id,$page_id)
  {
	 
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
		//end DB setup
		$query ="delete from engine4_core_follow where poster_id='".$user_id."' and resource_id ='".$page_id."'";
		
		$db->query($query);
		 
		
  }


 public function getUserFriendsListByDisplay($user_id,$text)
  {
		$query ="select * from engine4_user_membership as f inner join engine4_users as u on u.user_id=f.user_id where f.resource_id='".$user_id."' and f.active = 1 and f.user_approved =1 and u.displayname like '%".$text."%'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
		
  }

  public function getprojectSharedList($project_id)
  {
   $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		$query = "select user_id from engine4_user_projects where project_id='".$project_id."'";
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
	  }
	  
 public function getprojectSharedListWithOther($project_id,$ownerId)
  {
	  
   $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		  $query = "select u.user_id,u.displayname
				  from engine4_user_projects up 
				  left join engine4_users u
				  on up.user_id=u.user_id
		 		  where up.project_id='".$project_id."'
				  and up.user_id!=".$ownerId."";
				  
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		 
		return $result;
	  }
  function setProjectsList($data)
  {
	  $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
		//end DB setup
		$dquery = "delete from engine4_user_projects where user_id='".$data['user_id']."' and project_id='".$data['project_id']."'";
		
		$db->query($dquery);
		
		$query = "insert into engine4_user_projects set user_id='".$data['user_id']."', project_id='".$data['project_id']."', permission='".$data['permission']."', shared_by='".$data['shared_by']."'";
		 $stmt = $db->query($query);
       
  }
  
public function unsetProjectsList($project_id)
  {
	   $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
		//end DB setup
		$dquery = "delete from engine4_user_projects where project_id='".$data['project_id']."'";
		
		$db->query($dquery);
		
	
       
  }
  
 
 public function unsetProjectsListFriend($data_id,$project_id)
  {
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
		//end DB setup
		$dquery = "delete from engine4_user_projects where project_id='".$project_id."' and user_id=".$data_id."";
	
		$db->query($dquery);
		
	
       
  }
  
  public function unsetPageListFriend($data_id,$page_id)
  {
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
		//end DB setup
		$dquery = "delete from engine4_user_pages where page_id='".$page_id."' and user_id=".$data_id."";
	
		$db->query($dquery);
		
	
       
  }
  
  

}
