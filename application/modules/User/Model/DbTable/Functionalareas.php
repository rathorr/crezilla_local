<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ListItems.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Functionalareas extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Functionalareas';
  
public function userFunctionalareas($user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('is_deleted=?', 0)
		   ->order('functionalareas_id ASC');
     $result	=	$this->fetchAll($select);
	 
	 return $result;
 }
 
  public function getuserfabyfaprimaryid($fa_primary_id, $user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('fa_primary_id=?', $fa_primary_id)
		   ->where('is_deleted=?', 0)
		   ->order('functionalareas_id DESC');
     $result	=	$this->fetchAll($select);
	 return ($result)?$result[0]['functionalareas_id']:false;
	 
	 //return $result;
 }
public function getusercurstatus($user_id){
 $select	=	$this->select("t.user_id")
			->from ('engine4_user_currentstatus as t')
			->setIntegrityCheck(false)
			->where("t.user_id = $user_id", 1);

    $userdata = $this->fetchRow($select);
	return $userdata;
}
 
 public function updatecurstatus($currentstatus_title,$cur_primary_id,$user_id){
 	$data = array(
    'currentstatus_title'      => $currentstatus_title,
    'cur_primary_id'      => $cur_primary_id
	);
    $where = array(
      '`user_id` = ?' => $user_id
    );

    $this->update($data, $where);

    return $this;
	}

	public function checkifexist($funcsids,$user_id){
	 $select = $this->select("t.user_id")
				->from ('engine4_user_functionalareas as t')
				->setIntegrityCheck(false)
				->where("t.fa_primary_id = $funcsids", 1)
				->where("t.user_id = $user_id", 1);

	    $userdata = $this->fetchRow($select);
		return $userdata;
	}
	public function deletebyid($funcsids,$user_id)
	 {
	 	$condition = array(
	    'fa_primary_id = ?' => $funcsids,
	    'user_id = ?' => $user_id
		);
		
		$result = $this->delete($condition);
		return $result;
	 }
 
}