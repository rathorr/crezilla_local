<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ListItems.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Endorsements extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Endorsement';
  
  public function getSkillEndorsement($owner_id, $skill_id){
	  /* $select	= $this->select()
	       ->where('owner_id=?', $owner_id)
		   ->where('is_deleted=?', 0)
		   ->where('skill_id=?', $skill_id)
		   ->order('endorsement_id DESC');
     $result	=	$this->fetchAll($select);
	 
	 return $result;*/
	 
	 $query ="select * from engine4_user_endorsements where owner_id='".$owner_id."' and skill_id='".$skill_id."' and is_deleted='0' order by endorsement_id DESC";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$stmt = $db->query($query);
        $result = $stmt->fetchAll();
		
		return $result;
 }
 
 public function getendorsecount($owner_id, $skill_id){
		 $query ="select count(*) as total from engine4_user_endorsements where owner_id='".$owner_id."' and skill_id='".$skill_id."' and is_deleted='0'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$stmt = $db->query($query);
        $result = $stmt->fetchAll();
		
		return $result[0]['total'];
 }
 
 public function getskillendorseby($skill_id){
		 $query ="select user_id from engine4_user_endorsements where  skill_id='".$skill_id."' and is_deleted='0'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$stmt = $db->query($query);
        $result = $stmt->fetchAll();
		$endorse = "";
		if($result){
			$i =0;
				foreach($result as $res){
					$endorse .= 	$res['user_id'];
					if($i<count($result)){
						$endorse .=     ',';
					}
				$i++;
			}
		}
		return $endorse;
 }

}