<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ListItems.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Categories extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Category';
  
  public function userCategories($user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('is_deleted=?', 0)
		   ->order('category_id ASC');
     $result	=	$this->fetchAll($select);
	 
	 return $result;
 }
 
  public function getusercatbycatprimaryid($cat_primary_id, $user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('cat_primary_id=?', $cat_primary_id)
		   ->where('is_deleted=?', 0)
		   ->order('category_id DESC');
     $result	=	$this->fetchAll($select);
	 return ($result)?$result[0]['category_id']:false;
	 
	 //return $result;
 }
 
 
}