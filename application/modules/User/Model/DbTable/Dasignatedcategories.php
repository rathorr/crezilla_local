<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ListItems.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Dasignatedcategories extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Dasignatedcategories';
  
public function userDasignatedcategories($user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('is_deleted=?', 0)
		   ->order('dasignatedcategories_id ASC');
     $result	=	$this->fetchAll($select);
	 
	 return $result;
 }
 public function userDasignatedcategoriesSearch($user_id){
 	$desigs=array();
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('is_deleted=?', 0)
		   ->order('dasignatedcategories_id ASC');
     $result	=	$this->fetchAll($select);
	
	 foreach ($result as $key => $value) {
	 	$desigs[$key]=$value['dasignatedcategory_title'];
	 }
	 if(count($desigs) > 3)
	 {
	 	$udesigs=array_slice($desigs, 0, 3);
	 	$memdesigs= implode(',',$udesigs)." and more..";
	 }
	 else
	 {
	 	 $memdesigs= implode(' , ',$desigs);
	 }
	// $desig=(count($desigs) < 4)?$desigs:array_slice($desigs, 0, 3); 
	  //echo trim($memdesigs); die();
	 return $memdesigs;
 }
 
  public function getuserdcbydcprimaryid($dc_primary_id, $user_id){
	  $select	= $this->select()
	       ->where('user_id=?', $user_id)
		   ->where('dc_primary_id=?', $dc_primary_id)
		   ->where('is_deleted=?', 0)
		   ->order('dasignatedcategories_id DESC');
     $result	=	$this->fetchAll($select);
	 return ($result)?$result[0]['dasignatedcategories_id']:false;
	 
	 //return $result;
 }
 public function deletedesignationbyid($dcid,$user_id)
 {
 	$condition = array(
    'dc_primary_id = ?' => $dcid,
    'user_id = ?' => $user_id
	);
	/*$condition = array(
    'dc_primary_id = ' . $dcid,
    'user_id = ' . $user_id
	);*/
	//print_r($condition);
	$result = $this->delete($condition);
	return $result;
 }

}