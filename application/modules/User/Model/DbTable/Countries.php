<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Countries extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Countries';
 public function getCountryname($countryid){
	$select = $this->select("country_name")
	           ->setIntegrityCheck(false)
	          ->where('countryid=?', $countryid);
	 $result  = $this->fetchRow($select)->country_name;
	return $result;
	 }

}