<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: ListItems.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Views extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_View';
  
  public function isReviewGiven($data){
	  if($data['viewer_id']){
		  $select	= $this->select()
			   ->where('user_id=?', $data['user_id'])
			   ->where('viewer_id=?', $data['viewer_id']);
	  }
	  if(!$data['viewer_id']){
		  $select	= $this->select()
			   ->where('user_id=?', $data['user_id'])
			   ->where('ip_addr=?', $data['ip_addr']);
	  }
	 
     $result	=	$this->fetchAll($select);
	 return (count($result)>0)? TRUE:FALSE;
 }

}