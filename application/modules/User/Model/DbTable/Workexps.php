<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     Sami
 */

/**
 * @category   Application_Extensions
 * @package    Education
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Workexps extends Engine_Db_Table
{
  protected $_rowClass = 'User_Model_Workexps';

  
  public function getWorkexpPaginator($options = array())
  {
    return Zend_Paginator::factory($this->getWorkexpSelect($options));
  }

  public function getUseryrexp($uid){
	
	$select	=	$this->select("workexp")
			->setIntegrityCheck(false)
			->where("owner_id = $uid", 1)
			->where("row_status = 1", 1)
			;
		//echo $select; die();
    $userdata = $this->fetchRow($select);
	return ($userdata)? $userdata->workexp:"0";
	//return ($userdata)? (($userdata->value>1)?$userdata->value.' years':$userdata->value.' year'):false;
 }

}