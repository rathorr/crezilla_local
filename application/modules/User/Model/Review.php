<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: User.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_Review extends Core_Model_Item_Abstract
{
	public function getRating()
	{
		$ratingTbl = Engine_Api::_()-> getItemTable('ynmember_rating');
		$ratingTblName = $ratingTbl -> info('name');
		
		$ratingTypeTbl = Engine_Api::_()-> getItemTable('ynmember_ratingtype');
		$ratingTypeTblName = $ratingTypeTbl -> info('name');
		
		$select = $ratingTbl -> select() -> setIntegrityCheck(false)
			-> from($ratingTblName)
			-> joinleft($ratingTypeTblName, "{$ratingTblName}.rating_type = {$ratingTypeTblName}.ratingtype_id")
			-> where("$ratingTblName.review_id = ?", $this -> getIdentity())
			;
		$ratings = $ratingTbl -> fetchAll($select);
		return $ratings;
	}
}
