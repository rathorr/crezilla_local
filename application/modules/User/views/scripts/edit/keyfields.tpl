<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: profile.tpl 9984 2013-03-20 00:00:04Z john $
 * @author     John
 */
?>
<style>
div.field-privacy-selector > span.caret{display:none;}

.adding_cat_height{margin-top: 145px;} /* add 25px + .form-errors */

.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:auto;
  width:100%;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}

ul.form-errors > li, ul.form-notices > li {
 float:none;
}
body#global_page_user-edit-profile .category_list .category {
    height: 100% !important;
  width:357px !important;
}
</style>

<style type="text/css">
.thumb_profile, .thumb_icon {border-radius:0 !important;}
form.global_form .form-elements label{ display:none;}
.CaptionCont.SlectBox > label, .select-all.partial > label, .select-all > label{ display:block !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}


  .hidebox{
    display: none;
  }
  select#subdesignation
  {
    border: 1px solid #d8d8d8 !important;
    border-radius: 3px;
    padding: 5px 5px;
    width: 100%;
    background-color: #fafafa;
    font-size: 13px;
    line-height: normal;
    height: 37px;
    color: #9C9999;
    margin-top: 5px;
  }
  #subdesignation_id
  {
    border: 1px solid #d8d8d8 !important;
    border-radius: 3px;
    width: 100%;
    background-color: #fafafa;
    font-size: 13px;
    line-height: normal;
    height: 37px;
    color: #9C9999;
    padding: 9px 0;
      color: #888;
      font-weight: normal;
      vertical-align: top; margin-right:3px; padding-right: 3px;
      margin-top: 5px;
    }
    .fancybox-overlay{z-index:999999 !important;}
    .thumb_profile, .thumb_icon {border-radius:0 !important;}

    .global_form > div > div {
      background:transparent !important;
      padding:0 10px !important;
      width:100%;
    }
    .layout_main .layout_middle {
        width: 100% !important;
       background-color: #fff !important;
    }
    .skip_div.abs_d {
        margin-top: 7px;
    }

    .global_form #1_1_6-label { display:none;}
    .global_form div.form-label { display:none;}
    .SumoSelect > .optWrapper > .options {
      max-height: 160px !important;
    }
</style>

<div class="layout_page_user_edit_profile">
<div class="generic_layout_container layout_top">
<div class="generic_layout_container layout_middle">
<h2>Edit profile </h2>
 <div class="tabs">
        <?php
          // Render the menu
            echo $this->navigation()
            ->menu()
            ->setContainer($this->navigation)
            ->render();
       ?>
      </div>
</div>
</div>

<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
      'topLevelId' => (int) @$this->topLevelId,
      'topLevelValue' => (int) @$this->topLevelValue
    ))
?>

<?php
  $this->headTranslate(array(
    'Everyone', 'All Members', 'Friends', 'Only Me',
  ));
?>
<?php 
if($this->statusmsg){
  //echo '<ul class="form-notices"><li>'.$this->statusmsg.'</li></ul>';
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function(){
       jQuery('.loading_image').hide();
    /*jQuery.fancybox({
            'width': '500px',
            'height': '200px',
            //'autoScale': true,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'type': 'iframe',
            'href': '<?php echo $this->baseUrl()."/members/edit/pathapproval";?>'
        });*/
  });
    </script>
<?php 
}
?>
<!-- <p class="skip_div abs_d">
  <a href="javascript:void(0);" class="skip_text skip_bt" onclick="jQuery('.save_personal_info').click()">Next/Save</a>
</p> -->

<!--Industries Experience start-->
<div class="iexp">
<h3 >Domain Experience</h3>
  <span style="font-weight: 700; width: 100%; display: inline-block; border-bottom: 1px solid; height: 0.2em; margin-bottom: 8px;border-bottom:1px solid #dbdbdb;"></span>
    <div class="exps">
    <input type="hidden" name="exp_ids" id="exp_ids" value="">
<?php if($this->useriexps){$i=0;
        foreach($this->useriexps as $exps){
            echo "<p class='exp_edit' id='".$exps['ind_primary_id']."'>
            <span class='exp_set'>
           <span class='exp bg_none'>".$exps['industryexperience_title']."</span>";?>
            <?php echo "</span></p>";
          $i++;
        }
    }
    if($i==0){ ?>
      No Domain Experience added yet.
    <?php } ?>
</div>
<div class="form-element userindus_list">
<select class="field_container userindus" id="industryexp"  multiple="multiple">
  <?php if($this->industryexperience){
      foreach($this->industryexperience  as $key=>$val){?>
        <option  value="<?php echo $key;?>" <?php if(in_array($key, $this->useriexp)){echo 'selected="selected"';}?>><?php echo $val?></option>
      <?php }
   }?>
</select>
</div>
   <div style="width: 20%; margin: 0 auto;">
      <input type="submit" name="submitdomainexp" id="submitdomainexp" value="Save"><!-- 
        <button name="submitcontact" id="submit" value="s" type="submit">Save</button>  -->      
      </div>
</div>
<!-- Industries Experience end-->

<!--My Functonal Area start-->
<div class="func">
<h3 >Functional Experience</h3>
  <span style="font-weight: 700; width: 100%; display: inline-block; border-bottom: 1px solid; height: 0.2em; margin-bottom: 8px;border-bottom:1px solid #dbdbdb;"></span>
  <input type="hidden" name="func_ids" id="func_ids" value="">
   <div class="funs">
<?php if($this->userfas){$i=0;
        foreach($this->userfas as $funs){
            echo "<p class='fun_edit' id='".$funs['fa_primary_id']."'>
            <span class='fun_set'>
           <span class='fun bg_none'>".$funs['functionalareas_title']."</span>";?>
            <?php echo "</span></p>";
          $i++;
        }
    }
    if($i==0){ ?>
      No Functional Experience added yet.
    <?php } ?>
    <!-- <p class="removefuns" id="removefuns"></p> -->
</div>

  <div class="form-element functionalarea_list">
<select class="field_container functionalarea" id="functionalarea"  multiple="multiple">
  <?php if($this->functionalareas){
      foreach($this->functionalareas  as $key=>$val){?>
        <option  value="<?php echo $key;?>" <?php if(in_array($key, $this->userfa)){echo 'selected="selected"';}?>><?php echo $val?></option>
      <?php }
   }?>
</select>
</div>
<div style="width: 20%; margin: 0 auto;">
      <input type="submit" name="submitfunexp" id="submitfunexp" value="Save"><!-- 
        <button name="submitcontact" id="submit" value="s" type="submit">Save</button>  -->      
      </div>
</div>
<!-- My Functonal Area end-->

<!--My Skills start-->
<div class="skill">
<h3 >Skills</h3>
  <span style="font-weight: 700; width: 100%; display: inline-block; border-bottom: 1px solid; height: 0.2em; margin-bottom: 8px;border-bottom:1px solid #dbdbdb;"></span>
  <input type="hidden" name="skill_ids" id="skill_ids" value="">
   <div class="skills">
<?php if($this->userskills) {
 $i=0;
        foreach($this->userskills as $skills){
            echo "<p class='skill_edit' id='".$skills['skill_primary_id']."'>
            <span class='skill_set'>
           <span class='skill bg_none'>".$skills['skill_title']."</span>";?>
            <?php echo "</span></p>";
          $i++;
        }
    }
    if($i==0){?>
      No Skill added yet.
    <?php } ?>
    <!-- <p class="removeskills" id="removeskills"></p> -->
</div>

  <div class="form-element skill_list">
<select class="field_container skill" id="skill"  multiple="multiple">
  <?php if($this->skills){
      foreach($this->skills  as $key=>$val){?>
        <option  value="<?php echo $key;?>" <?php if(in_array($key, $this->userskill)){echo 'selected="selected"';}?>><?php echo $val?></option>
      <?php }
   }?>
</select>
</div>
<div style="width: 20%; margin: 0 auto;">
      <input type="submit" name="submitskills" id="submitskills" value="Save"><!-- 
        <button name="submitcontact" id="submit" value="s" type="submit">Save</button>  -->      
      </div>
</div>
<!-- My Skills end-->
<div id="divheight" style="height: 150px;">
</div>

</div>
</div>
</div>
</div>

<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.css" media="screen" />
<script src="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.js"></script>
<link href="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.css" rel="stylesheet" />
<script type="text/javascript">

 window.addEvent('domready', function() {
    en4.user.buildFieldPrivacySelector($$('.global_form *[data-field-id]'));
  });

jQuery(document).ready(function()
{
  /*var loc = window.location.pathname;
    var pathname = loc.substring(loc.lastIndexOf('/') +1);
   console.log(pathname);*/
   jQuery("#global_content > div > div.generic_layout_container.layout_top > div > div > ul > li:nth-child(3)").addClass('active');
   
  //jQuery("#divheight").hide();
  jQuery('.userindus').SumoSelect({ triggerChangeCombined: true,selectAll: false,placeholder: 'Select Only 5 Domain Experiences'});

  jQuery('.functionalarea').SumoSelect({ triggerChangeCombined: true,selectAll: false,placeholder: 'Select Only 5 Functonal Areas'});
  jQuery('.skill').SumoSelect({ triggerChangeCombined: true,selectAll: false,placeholder: 'Select Only 5 skills'});

    setTimeout(function(){ 
     jQuery('#exp_ids').val(jQuery('#industryexp').val());
  }, 1000);

    setTimeout(function(){ 
     jQuery('#func_ids').val(jQuery('#functionalarea').val());
  }, 1000);
   setTimeout(function(){ 
     jQuery('#skill_ids').val(jQuery('#skill').val());
  }, 1000);  

jQuery(document).on("click","#submitdomainexp", function(){
  var val= jQuery("#exp_ids").val();
  var exp_ids = jQuery('#exp_ids').val().split(",");
  console.log('val-'+ val);
  console.log('array-'+ exp_ids);
  console.log('length'+exp_ids.length);
  if(exp_ids.length > 5)
  {
    alert("You can only select 5 Domain Experience!");
    return false;
  }
  else
  { 
      jQuery('.loading_image').show();
      console.log('exps'+exp_ids);
      jQuery.post("<?php echo $this->baseUrl();?>/members/edit/addremovesdomexp",{exp_ids:exp_ids}, function(resp){
            var data=jQuery.parseJSON(resp);
            if(data.status == "Error!")
             {
              jQuery('.loading_image').hide();
              alert(resp);
              return false;
             }
             else
             {
              jQuery('.loading_image').hide();
              alert(data.status);
              window.location.reload();
             }
          }); 
  }

});

jQuery(document).on("click","#submitfunexp", function(){
  var val= jQuery("#func_ids").val();
  var func_ids = jQuery('#func_ids').val().split(",");
  console.log('val-'+ val);
  console.log('array-'+ func_ids);
  console.log('length'+func_ids.length);
  if(func_ids.length > 5)
  {
    alert("You can only select 5 Functional Experience!");
    return false;
  }
  else
  { 
      jQuery('.loading_image').show();
      console.log('funs'+func_ids);
      jQuery.post("<?php echo $this->baseUrl();?>/members/edit/addremovesfunexp",{func_ids:func_ids}, function(resp){
            var data=jQuery.parseJSON(resp);
            if(data.status == "Error!")
             {
              jQuery('.loading_image').hide();
              alert(resp);
              return false;
             }
             else
             {
              jQuery('.loading_image').hide();
              alert(data.status);
              window.location.reload();
             }
          }); 
  }

});


jQuery(document).on("click","#submitskills", function(){
  var val= jQuery("#skill_ids").val();
  var skill_ids = jQuery('#skill_ids').val().split(",");
  console.log('val-'+ val);
  console.log('array-'+ skill_ids);
  console.log('length'+skill_ids.length);
  if(skill_ids.length > 5)
  {
    alert("You can only select 5 Skills!");
    return false;
  }
  else
  { 
      jQuery('.loading_image').show();
      console.log('skills'+skill_ids);
      jQuery.post("<?php echo $this->baseUrl();?>/members/edit/addremoveskill",{skill_ids:skill_ids}, function(resp){
            var data=jQuery.parseJSON(resp);
            if(data.status == "Error!")
             {
              jQuery('.loading_image').hide();
              alert(resp);
              return false;
             }
             else
             {
              jQuery('.loading_image').hide();
              alert(data.status);
              window.location.reload();
             }
          }); 
  }

});


});

jQuery(document).on('click', '#industryexp', function(){
  jQuery('#exp_ids').val(jQuery(this).val());
});

jQuery(document).on('click', '#functionalarea', function(){
  
  jQuery('#func_ids').val(jQuery(this).val());
});
jQuery(document).on('click', '#skill', function(){
  
  jQuery('#skill_ids').val(jQuery(this).val());
});

jQuery('#submit-element').click(function(){
       jQuery('.loading_image').show();
    });

</script>
