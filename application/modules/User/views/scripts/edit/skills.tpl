<div class="skilllist">
<p>Skills</p>

<?php 
if($this->skills){$i=0;
        foreach($this->skills as $key=>$val){
        $i++;?>
        	<div class="skilbox">
            <input type="checkbox" name="skil" class="skil"  value="<?php echo $key;?>" <?php if(in_array($key, $this->userskills)){echo 'checked="checked"';}?>/>
            <label><?php echo $val;?></label>
            </div>
        <?php }
    }
    if($i==0){ ?>
    	No skills.
    <?php } ?>
<input type="hidden" id="remov_skills_id"  value=""/>
<input type="hidden" id="save_skills_id"  value=""/>
<a href="javascript:void(0);" class="save_skill">Save</a>
</div>

<script type="text/javascript">
var remove_id_arr	=	[];
var save_id_arr	=	[];
var userskills	=	'<?php echo $this->userskills;?>';
jQuery('.skil').on('click', function(){
		
		var is_checked	=	jQuery(this).prop("checked");
		var chk_val	=	jQuery(this).val();
		if(is_checked == true){
			remove_id_arr.remove(chk_val);
			save_id_arr.push(chk_val);
		}else{
			remove_id_arr.push(chk_val);
			save_id_arr.remove(chk_val);
		}
		jQuery('#remov_skills_id').val(remove_id_arr);
		jQuery('#save_skills_id').val(save_id_arr);
		
});

jQuery(document).ready(function(){
	
	Array.prototype.remove = function(x) { 
		var i;
		for(i in this){
			if(this[i].toString() == x.toString()){
				this.splice(i,1)
			}
		}
	}
	
	jQuery('.save_skill').on('click', function(){
		jQuery('.loading_img').show();
		var remove_skill_ids	=	jQuery('#remov_skills_id').val();
		var skills				=	jQuery('#save_skills_id').val();
		jQuery.post("<?php echo $this->baseUrl().'/members/edit/addeditskills';?>", {skills:skills,remove_skill_ids:remove_skill_ids}, function(resp){
				jQuery('.loading_img').hide();
				jQuery('.fancybox-close').click();
				jQuery('.skills').html(jQuery.trim(resp));
				
			});
			
	});
	
});
</script>