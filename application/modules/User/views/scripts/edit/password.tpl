<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: password.tpl 9869 2013-02-12 22:37:42Z shaun $
 * @author     Steve
 */
?>
<style type="text/css">
ul.form-notices{margin-top:0px !important;}
</style>
<div class="headline">
<h2>
 <?php echo $this->translate('Edit My Profile');?>
</h2>
<div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>
 </div>
 
<?php echo $this->form->render($this) ?>
<div id="signinerror" style=" display:none;position:relative !important; margin-top:40px;">password does not match</div>
<div id="signinerror" style=" background:green none repeat scroll 0 0;display:none;position:relative !important; margin-top:40px;">Password changed successfully.</div>
<style>

.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
</style>
<script type="text/javascript">
jQuery("#oldPassword, #password, #passwordConfirm").keypress(function(e) {
   if(e.which == 13 || e.which == 1 ) {
      validatechangepass();
   }
});

jQuery(document).ready(function(){
	jQuery('.custom_294').parent().addClass('active');
	jQuery('.custom_313').parent().addClass('active');
	jQuery('.custom_326').parent().addClass('active');
});
//var $ = jQuery.noConflict();
  function validatechangepass(){
	 
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#oldPassword").val()) == ''){
			jQuery("#oldPassword").focus();
			jQuery("#oldPassword").addClass("com_form_error_validatin");
			return false;
	}
	if(jQuery.trim(jQuery("#password").val()) == ''){
			jQuery("#password").focus();
			jQuery("#password").addClass("com_form_error_validatin");
			return false;
	}
	if(jQuery.trim(jQuery("#passwordConfirm").val()) == ''){
			jQuery("#passwordConfirm").focus();
			jQuery("#passwordConfirm").addClass("com_form_error_validatin");
			return false;
	}
	if(jQuery.trim(jQuery("#passwordConfirm").val()) != jQuery.trim(jQuery("#password").val())){
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Password does not match.");
			jQuery("#signinerror").fadeOut(15000);
			jQuery("#passwordConfirm").focus();
			jQuery("#passwordConfirm").addClass("com_form_error_validatin");
			return false;
	}
	
	jQuery.post("<?php echo $this->baseUrl();?>/members/edit/chkoldpass",{oldPassword:jQuery.trim(jQuery("#oldPassword").val()),password:jQuery.trim(jQuery("#password").val()),passwordConfirm:jQuery.trim(jQuery("#passwordConfirm").val())}, function(resp){
		if(!jQuery.trim(resp)){
			//jQuery('#user_form_changepass').submit();
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Password changed successfully.");
			jQuery("#signinerror").fadeOut(15000);
			return false;
		}else{
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Old password did not match.");
			jQuery("#signinerror").fadeOut(15000);
			return false;
		}
		//$('#signupform').submit();
	});
	  
  }
</script>
