<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: external-photo.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/paginator.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/modernizr-1.7.min.js';?>"></script>
<script type="text/javascript">
jQuery(function($){
    $('.paginationControl').find('a').live('click', function(){
		$('.loading_image').show();
        var link = $(this);
        var container = link.parents('#global_content');
		var type	=	link.parent().parent().parent().parent().attr('title');
        $.get(link.attr('href'), { format: 'html',type:type}, function(data){
			$('.loading_image').hide();
            container.html(data);
        }, 'html');
        return false;
    });
	
	jQuery('.custom_605').parent().addClass('active');
 	jQuery('.custom_606').parent().addClass('active');
   jQuery('.custom_607').parent().addClass('active');
   jQuery('.custom_611').parent().addClass('active');
   jQuery('.custom_631').parent().addClass('active');
   jQuery('.custom_651').parent().addClass('active');

});
</script>
<div class="headline">
  <h2>
    <?php if ($this->viewer->isSelf($this->user)):?>
      <?php echo $this->translate('Friends-Followers');?>
    <?php else:?>
      <?php echo $this->translate('%1$s\'s Profile', $this->htmlLink($this->user->getHref(), $this->user->getTitle()));?>
    <?php endif;?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
        echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
		
    ?>
  </div>
</div>
<?php
echo $this->form
  ->setAction($this->url(array('module'=>'user','controller'=>'edit','action'=>'friends-follower'), '', true))
  ->render($this);
?>
<div class="layout_page_user_index_browse">
<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
<div class="browsemembers_results" id="browsemembers_results">
<style type="text/css">
.thumb_profile{border-radius:0 !important;}
</style>
<div class="page_class browsemembers_results">
<!-- FRIEND LISTINGS START -->

<h3>
  <?php echo $this->translate(array('%s friend', '%s friends', $this->totalFriend),$this->locale()->toNumber($this->totalFriend)) ?>
</h3>
<?php $viewer = Engine_Api::_()->user()->getViewer();?>

<?php if( count($this->friends) ): 
?>
  <ul class="row_start talent_start">
  
    <?php $i = 1;foreach( $this->friends as $friend ): ?>
      <li>
      <span class="lftimg_shadow"></span>
      <span class="rgtimg_shadow"></span>
         
        <?php 
		$friendpic	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($friend->getIdentity());
        $friendpic	=	($friendpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$friendpic;
        ?>
        <a href="<?php echo $friend->getHref();?>" style="cursor:pointer;">
        	<span style="background-image:url(<?php echo $this->baseUrl().'/'.$friendpic;?>);" class="search_user_pic"></span>
		</a>
        <span class="user_name">
         <?php
           echo $this->htmlLink($friend->getHref(), strtoupper($friend->getTitle()), array("class"=>'profile_link')) ?>
        </span>
      </li>
       
    <?php 
    	echo ($i%6==0)?'</ul><ul class="row_start friend_start">':'';
       $i++;
     endforeach; ?>
  </ul>
<?php endif ?>

<?php  if( $this->friends ):
    $pagination = $this->paginationControl($this->friends, null, null, array(
      'pageAsQuery' => true,
      'query' => $this->formValues,
    ));
  ?>

  
  <?php if( trim($pagination) ): ?>
    <div class='browsemembers_viewmore' id="browsemembers_viewmore" title="friend">
      <?php echo $pagination ?>
    </div>
  <?php endif ?>
<?php endif; ?>
<!-- FRIEND LISTINGS END -->

<!-- FOLLOWER LISTINGS START -->
<h3>
  <?php echo $this->translate(array('%s follower', '%s followers', $this->totalFollower),$this->locale()->toNumber($this->totalFollower)) ?>
</h3>
<?php $viewer = Engine_Api::_()->user()->getViewer();?>

<?php if( count($this->follower) ): 
?>
  <ul class="row_start agency_start">
  
    <?php $i = 1;foreach( $this->follower as $follower ): ?>
      <li>
      <span class="lftimg_shadow"></span>
      <span class="rgtimg_shadow"></span>
         
        <?php 
		$followerpic	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($follower->getIdentity());

        $followerpic	=	($followerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$followerpic;
        ?>
       <a href="<?php echo $follower->getHref();?>" style="cursor:pointer;">
        	<span style="background-image:url(<?php echo $this->baseUrl().'/'.$followerpic;?>);" class="search_user_pic"></span>
	   </a>      
          <span class="user_name">
         <?php echo $this->htmlLink($follower->getHref(), strtoupper($follower->getTitle()), array("class"=>'profile_link')) ?>
        </span>
      </li>
       
    <?php 
    	echo ($i%6==0)?'</ul><ul class="row_start follower_start">':'';
       $i++;
     endforeach; ?>
  </ul>
<?php endif ?>

<?php if( $this->follower ):
    $pagination = $this->paginationControl($this->follower, null, null, array(
      'pageAsQuery' => true,
      'query' => $this->formValues,
    ));
  ?>
  <?php if( trim($pagination) ): ?>
    <div class='browsemembers_viewmore' id="browsemembers_viewmore" title="follower">
      <?php echo $pagination ?>
    </div>
  <?php endif ?>
<?php endif; ?>
<!-- FOLLOWER LISTINGS END -->

<!-- FOLLOWING LISTINGS END -->
<h3>
  <?php echo $this->translate(array('%s following', '%s followings', $this->totalFollowing),$this->locale()->toNumber($this->totalFollowing)) ?>
</h3>
<?php $viewer = Engine_Api::_()->user()->getViewer();?>

<?php if( count($this->following) ): 
?>
  <ul class="row_start producer_start">
  
    <?php $i = 1;foreach( $this->following as $following ): ?>
      <li>
      <span class="lftimg_shadow"></span>
      <span class="rgtimg_shadow"></span>
         
		   <?php 
		$followingpic	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($following->getIdentity());
        $followingpic	=	($followingpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$followingpic;
        ?>
        <a href="<?php echo $following->getHref();?>" style="cursor:pointer;">
        	<span style="background-image:url(<?php echo $this->baseUrl().'/'.$followingpic;?>);" class="search_user_pic"></span>  
     	</a>
        <span class="user_name">
         <?php echo $this->htmlLink($following->getHref(), strtoupper($following->getTitle()), array("class"=>'profile_link')) ?>
        </span>
      </li>
       
    <?php 
    	echo ($i%6==0)?'</ul><ul class="row_start following_start">':'';
       $i++;
     endforeach; ?>
  </ul>
<?php endif ?>


    
 <?php if( $this->following ):
    $pagination = $this->paginationControl($this->following, null, null, array(
      'pageAsQuery' => true,
      'query' => $this->formValues,
    ));
  ?>
  <?php if( trim($pagination) ): ?>
    <div class='browsemembers_viewmore' id="browsemembers_viewmore" title="following">
      <?php echo $pagination ?>
    </div>
  <?php endif ?>
<?php endif; ?>
  
  </div>
 
<!-- FOLLOWING LISTINGS END -->

</div>
</div>
</div>
</div>
</div>