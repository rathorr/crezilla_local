<style>
.endore_name a.user_link {
    color: rgb(57, 139, 204); text-decoration: none; margin-left: 10px; font-size: 24px; font-family: Arial; vertical-align: -34px;}

.fancybox-skin
{ padding: 0 !important;}
 

.ynfeed_members_popup, .yncomment_members_popup {
    padding: 7px !important;
    
}


.ppl_vwed{ background-color: #eee;
    border-bottom: 1px solid #dbdbdb;
    border-radius: 0;
    color: #4a4a4a;
    font-family: roboto,sans-serif;
    font-size: 15px;
    font-weight: normal !important;
    padding: 9px 10px;
    text-transform: capitalize;
    
	margin: 0 !important;
	}
.thumb_profile{border-radius:0 !important;}
#global_content h2 {
    color: #3faee8;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 700;
    margin: 0;
    padding: 10px 20px;
    text-transform: capitalize;
}
.item_member_thumb { width:auto; height:auto; float:left;}
.seao_common_add_tooltip_link a { color: #000; display: block; padding: 14px 61px; text-decoration: none;  font-family: Georgia, Arial, Helvetica, sans-serif; font-size:17px}
.item_member {background: rgb(240, 240, 240) none repeat scroll 0 0;    margin-bottom: 5px;    padding: 8px;}
.item_member_thumb a img {
    border: medium none;
    height: 20px;
    margin-right: 7px;
    width: 20px;
}
.item_member_name {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}
.item_member_details a.seao_common_add_tooltip_link {
    color: #4c5153;
    font-size: 14px;
}
</style>
<div class="ynfeed_members_popup">
	<div class="ynfeed_members_popup_content" id="likes_popup_content">
		<?php $count_user = count($this->friends);
					foreach( $this->endorse_by as $user ) {
                        if($user){
                        $imgpath	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($user);
                        $imgpath	=	($imgpath=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$imgpath;
                        $u	=	Engine_Api::_()->getItem('user', $user); ?>
				<div class="item_member">
					<div class="item_member_thumb">
						<?php echo $this->htmlLink($u->getHref(), $this->itemPhoto($u, 'thumb.icon', $u->getTitle()), array('class' => 'item_photo seao_common_add_tooltip_link', 'target' => '_parent', 'title' => $u->getTitle(), 'rel'=> 'user'.' '.$u->getIdentity()));?>
					</div>
					<div class="item_member_details">
						<div class="item_member_name">
							<?php  $title1 = $u->getTitle(); ?>
							<?php  $truncatetitle = Engine_String::strlen($title1) > 20 ? Engine_String::substr($title1, 0, 20) . '..' : $title1?>
							<?php echo $this->htmlLink($u->getHref(), $title1, array('title' => $u->getTitle(), 'target' => '_parent', 'class' => 'seao_common_add_tooltip_link', 'rel'=> 'user'.' '.$u->getIdentity())); ?>
						</div>
					</div>	
				</div>
				<?php	}
			 } ?>
	</div>
</div>
          
          
          <a href="<?php echo $u->getHref();?>" style="cursor:pointer; display:none;">
        	<span style="background-image:url(<?php echo $this->baseUrl().'/'.$imgpath;?>); display:block; background-position: 50% 50%; background-size: cover;  display: inline-block; height: 80px; width:80px; float: left;" class="skill_profile"></span>
		 </a>
        <span class="endore_name" style="display:none;">
         <?php  echo $this->htmlLink($u->getHref(), strtoupper($u->getTitle()), array("class"=>'user_link')) ?>
        </span>
