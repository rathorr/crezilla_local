<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: profile.tpl 9984 2013-03-20 00:00:04Z john $
 * @author     John
 */
?>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
*
{
  font-family: 'Roboto', sans-serif !important;
}
.thumb_profile, .thumb_icon {border-radius:0 !important;}
form.global_form .form-elements label{ display:none;}
.CaptionCont.SlectBox > label, .select-all.partial > label, .select-all > label{ display:block !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
.hidebox{
    display: none;
  }
.fancybox-overlay{z-index:999999 !important;}
.thumb_profile, .thumb_icon {border-radius:0 !important;}

.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
.layout_main .layout_middle {
    width: 100% !important;
   background-color: #E9EAED !important;
}
.skip_div.abs_d {
    margin-top: 7px;
}


.global_form div.form-label { display:none;}

div.field-privacy-selector > span.caret{display:none;}

.adding_cat_height{margin-top: 145px;} /* add 25px + .form-errors */

.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:auto;
  width:100%;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}

ul.form-errors > li, ul.form-notices > li {
 float:none;
}
body#global_page_user-edit-profile .category_list .category {
    height: 100% !important;
  width:357px !important;
}
div.tabs2
{
  width: 73%;
  background-color: #fff !important;
}
.tabs
    {
      position: absolute;
      padding:0px !important;
      width: 20%;
      background-color: #fff !important;
      float: left;
    } 
#global_content > div > div.generic_layout_container.layout_main > div > div > div > div.tabs > ul > li
  {
    width: 100%;
    padding-top: 5px !important;
  }
  .tab3 {
    position:relative;
    left: 25%;
    width: 70%;
    border-left: 1px solid #ccc;
    padding-top: 1px;
}
.tabs > ul li.active > a,
.tabs > ul li > a:hover
 {
border-left: 2px solid #fb933c !important;
  }

#global_content h2 {
  border-bottom: 1px solid #ccc;
  margin: 0 0 10px 0; 
  padding: 0px 0px; 
  width: 100%;
}
.form-control
{
  -webkit-box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075);
  box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075); 
  height: 40px;
}
#global_page_user-edit-contact form#contactinfoform div.form-wrapper
{
  border-bottom: 1px solid #ccc;
  margin-bottom: 10px;
}
</style>


<div class="layout_page_user_edit_contact">
<div class="generic_layout_container layout_top">
</div>

<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
<h2>Social Contacts</h2>
<div class="tabs2"> 
 <div class="tabs">
        <?php
          // Render the menu
            echo $this->navigation()
            ->menu()
            ->setContainer($this->navigation)
            ->render();
       ?>
  </div>
<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
      'topLevelId' => (int) @$this->topLevelId,
      'topLevelValue' => (int) @$this->topLevelValue
    ))
?>

<?php
  $this->headTranslate(array(
    'Everyone', 'All Members', 'Friends', 'Only Me',
  ));
?>
<?php 
if($this->statusmsg){
  //echo '<ul class="form-notices"><li>'.$this->statusmsg.'</li></ul>';
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function(){
       jQuery('.loading_image').hide();
    /*jQuery.fancybox({
            'width': '500px',
            'height': '200px',
            //'autoScale': true,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'type': 'iframe',
            'href': '<?php echo $this->baseUrl()."/members/edit/pathapproval";?>'
        });*/
  });
    </script>
<?php 
}
?>
<div class="tab3">
<!-- <h2>Contact Information</h2> -->
  <form method="post" action="/members/edit/contact" id="contactinfoform" class="form-control">
   <?php
    $contactinfo  =   $this->contactinfos;
    if(isset($contactinfo[0])){
    foreach($this->contactinfos as $con){
    $houseno=$con->houseno;
    $street= $con->street;
    $landmark=$con->landmark;
    $city=$con->city;
    $state=$con->state;
    $pincode=$con->pincode;
    $usercon = $con->country;
    $countrycodehome=$con->countrycodehome;
    $contactnohome=$con->contactnohome;
    $countrycodeoffice=$con->countrycodeoffice;
    $contactnooffice=$con->contactnooffice;
    $linkedin=$con->linkedin;
    $twittera=$con->twittera;
    $instagram=$con->instagram;
    $google=$con->google;
    $facebooka=$con->facebooka;
    $website=$con->website;
    }
    }
    else
    {
    }
    ?>
    
<div class="coninfo">
<!-- <h3>Social Information</h3> -->

 <div id="linkedin-wrapper" class="form-wrapper">
  <div id="linkedin-label" class="form-label">
  <label for="linkedin" class="optional">Linkedin</label>
  </div>
  <div id="linkedin-element" class="form-element">
  <input type="text" name="linkedin" id="linkedin" value="<?php echo $linkedin;?>" placeholder="Linkedin" tabindex="12">
  </div>
</div>

<div id="twittera-wrapper" class="form-wrapper">
  <div id="twittera-label" class="form-label">
  <label for="twittera" class="optional">Twitter</label>
  </div>
  <div id="twittera-element" class="form-element">
  <input type="text" name="twittera" id="twittera" value="<?php echo $twittera;?>" placeholder="Twitter" tabindex="13"></div>
</div>

<div id="instagram-wrapper" class="form-wrapper">
  <div id="instagram-label" class="form-label">
  <label for="instagram" class="optional">Instagram</label>
  </div>
  <div id="instagram-element" class="form-element">
  <input type="text" name="instagram" id="instagram" value="<?php echo $instagram;?>" placeholder="Instagram" tabindex="14">
  </div>
</div> 

<div id="google-wrapper" class="form-wrapper">
  <div id="google-label" class="form-label">
  <label for="google" class="optional">Google +</label>
  </div>
  <div id="google-element" class="form-element">
  <input type="text" name="google" id="google" value="<?php echo $google;?>" placeholder="Google+" tabindex="15">
  </div>
</div> 

<div id="facebooka-wrapper" class="form-wrapper">
  <div id="facebooka-label" class="form-label">
  <label for="facebooka" class="optional">Facebook</label>
  </div>
  <div id="facebooka-element" class="form-element">
  <input type="text" name="facebooka" id="facebooka" value="<?php echo $facebooka;?>" placeholder="Facebook" tabindex="16"></div>
</div> 

<div id="website-wrapper" class="form-wrapper">
  <div id="website-label" class="form-label">
  <label for="website" class="optional">Website</label>
  </div>
  <div id="website-element" class="form-element">
  <input type="text" name="website" id="website" value="<?php echo $website;?>" placeholder="Website" tabindex="17">
  </div>
</div>

</div>
<div>
<input type="submit" name="submitcontact" id="submitcontact" value="Save"><!-- 
  <button name="submitcontact" id="submit" value="s" type="submit">Save</button>  -->      
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- PROFILE FORM START -->
<script type="text/javascript">
  window.addEvent('domready', function() {
    en4.user.buildFieldPrivacySelector($$('.global_form *[data-field-id]'));
  });
 jQuery('.custom_292').parent().addClass('active');
 jQuery('.custom_311').parent().addClass('active');
 jQuery('.custom_324').parent().addClass('active');
 
  
 jQuery(document).ready(function(){
   /*  var loc = window.location.pathname;
    var pathname = loc.substring(loc.lastIndexOf('/') +1);
   console.log(pathname);*/

   jQuery("#global_content > div > div.generic_layout_container.layout_main > div > div > div > div.tabs > ul > li:nth-child(2)").addClass('active');
 });
</script>


<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.css" media="screen" />
<script src="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.js"></script>
<link href="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.css" rel="stylesheet" />
<script type="text/javascript">


   function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || jQuery(element).val().indexOf('-') != -1) &&      // "-" CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || jQuery(element).val().indexOf('.') != -1) &&      // "." CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
  
jQuery('#submit-element').click(function(){
       jQuery('.loading_image').show();
    });
jQuery("form").bind("keypress", function (e) {
    if (e.keyCode == 13) {
       // $("#btnSearch").attr('value');
        //add more buttons here
        console.log('enter');
        return false;
    }
});
</script>
