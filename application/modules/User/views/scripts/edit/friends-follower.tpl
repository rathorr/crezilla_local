
<div class="headline">
  <h2>
    <?php if ($this->viewer->isSelf($this->user)):?>
      <?php echo $this->translate('Friends-Followers');?>
    <?php else:?>
      <?php echo $this->translate('%1$s\'s Profile', $this->htmlLink($this->user->getHref(), $this->user->getTitle()));?>
    <?php endif;?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
        echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
		
    ?>
  </div>
</div>

<div class="search_form" style="display:none;">
<?php
echo $this->form
  ->setAction($this->url(array('module'=>'user','controller'=>'edit','action'=>'friends-follower'), '', true))
  ->render($this);
?>
</div>

<div class="profile_edit_info">
	<ul class="tab">
        <li><a href="<?php echo $this->baseUrl();?>/members/edit/friends-follower#tab25" id="friend">Friend</a></li>
        <li><a href="<?php echo $this->baseUrl();?>/members/edit/friends-follower#tab26" id="follower">Follower</a></li>
        <li><a href="<?php echo $this->baseUrl();?>/members/edit/friends-follower#tab27" id="following">Following</a></li>
    </ul>
	<section class="tab_content_wrapper">
        <article class="tab_content" id="tab25" style="width:100%;height: 500px;overflow-y: scroll;">
            <div class="form-wrapper" id="msg-setting-wrapper">
                <ul class='profile_friends' id="user_profile_friends">
                    <?php
                    $k=0;
                      foreach( $this->myfriends as $friends ):
                        $friend = Engine_Api::_()->getItem('user', $friends['friend_id']);
                        ?>
                    
                        <li id="user_friend_<?php echo $friend->getIdentity() ?>">
                    
                          <?php echo $this->htmlLink($friend->getHref(), $this->itemPhoto($friend, 'thumb.icon'), array('class' => 'profile_friends_icon')) ?>
                    
                          <div class='profile_friends_body'>
                            <div class='profile_friends_status'>
                              <span>
                                <?php echo $this->htmlLink($friend->getHref(), $friend->getTitle()) ?>
                              </span>
                              <div>
              <a style="background-image: url(<?php echo $this->baseUrl();?>/application/modules/User/externals/images/friends/remove.png);" class="buttonlink smoothbox remove_friend" href="<?php echo $this->baseUrl();?>members/friends/removefriend/user_id/<?php echo $friend->getIdentity();?>">Unfriend</a> 
                          </div>
                            </div>
                            
                          </div>
                          
                          
                    
                        </li>
                    
                      <?php $k++; endforeach;
                      	if($k==0){
                    		echo '<li>No Friends</li>';
                  		}
                      ?>
                        
                    </ul>
                    <img style="left: 41%;position: fixed;top: 75%;width: 21px; display:none;" class="process_image1" src="<?php echo $this->baseUrl();?>/public/custom/images/loading.gif">
            </div>
   
        </article>
        <article class="tab_content" id="tab26" style="width:100%;height: 500px;overflow-y: scroll;"">
   			 <div class="form-wrapper" id="msg-setting-wrapper">
    	<ul class='profile_friends' id="user_profile_friends_followers">
  
              <?php $i=0;
              foreach( $this->follower as $membership ):
                if( !isset($this->friendUsers[$membership->user_id]) ) continue;
                $member = $this->friendUsers[$membership->user_id];
                ?>
            
                <li id="user_friend_<?php echo $member->getIdentity() ?>">
            
                  <?php echo $this->htmlLink($member->getHref(), $this->itemPhoto($member, 'thumb.icon'), array('class' => 'profile_friends_icon')) ?>
            
                  <div class='profile_friends_body'>
                    <div class='profile_friends_status'>
                      <span>
                        <?php echo $this->htmlLink($member->getHref(), $member->getTitle()) ?>
                      </span>
                    </div>
            <div>
                  	<a style="background-image: url(<?php echo $this->baseUrl()?>/application/modules/User/externals/images/friends/remove.png);" class="buttonlink smoothbox remove_follow" href="<?php echo $this->baseUrl()?>/members/friends/remove/user_id/<?php echo $member->getIdentity() ?>/rev/1">Remove</a>
                  </div>
                  </div>
                  
                  
            
                </li>
            
              <?php $i++; endforeach;
                  if($i==0){
                    echo '<li>No Followers</li>';
                  }
              ?>
                
            </ul>
            <img style="left: 41%;position: fixed;top: 75%;width: 21px; display:none;" class="process_image2" src="<?php echo $this->baseUrl();?>/public/custom/images/loading.gif">
            
	</div>
    	</article>
        <article class="tab_content" id="tab27" style="width:100%;height: 500px;overflow-y: scroll;"">
    
   			 <div class="form-wrapper" id="msg-setting-wrapper">
    	<ul class='profile_friends' id="user_profile_friends_following">
  
          <?php $j=0; foreach( $this->following as $membership2 ):
            if( !isset($this->friendUsers2[$membership2->resource_id]) ) continue;
            $member2 = $this->friendUsers2[$membership2->resource_id];
            ?>
        
            <li id="user_friend_<?php echo $member2->getIdentity() ?>">
        
              <?php echo $this->htmlLink($member2->getHref(), $this->itemPhoto($member2, 'thumb.icon'), array('class' => 'profile_friends_icon')) ?>
        
              <div class='profile_friends_body'>
                <div class='profile_friends_status'>
                  <span>
                    <?php echo $this->htmlLink($member2->getHref(), $member2->getTitle()) ?>
                  </span>
                </div>
        		<div>
                <?php echo $this->userFriendship($member2) ?>
              </div>
              </div>
              
               
        
            </li>
        
          <?php $j++; endforeach;
          	if($j==0){
              	echo '<li>No Followings</li>';
              }
          ?>
            
        </ul>
        <img style="left: 41%;position: fixed;top: 75%;width: 21px; display:none;" class="process_image3" src="<?php echo $this->baseUrl();?>/public/custom/images/loading.gif">
    
	</div>
    	</article>
        
   
    </section>
</div>
<input type="hidden" id="list_count" value="20"/>
<link rel="stylesheet" href="<?php echo $this->baseUrl();?>/public/custom/tabs/jQueryTab.css" type="text/css" media="screen" />
<script src="<?php echo $this->baseUrl();?>/public/custom/tabs/jQueryTab.js"></script> 
<script type="text/javascript">
jQuery('.tab').on('click', function(){
		jQuery('#list_count').val('20')
});
jQuery('.profile_edit_info').jQueryTab({
    initialTab: 1,
	tabClass:'tab',
    tabInTransition: 'fadeIn',
    tabOutTransition: 'fadeOut',
    cookieName: 'active-tab-7'
    
});

jQuery('.tab_content').scroll(function(){
	//if (jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()){
		
		if(jQuery(this).scrollTop() + jQuery(this).innerHeight() >= jQuery(this)[0].scrollHeight){
				var active_section = 	jQuery('.tab').find('.active a').attr("id");
				var list_count	   =	jQuery.trim(jQuery('#list_count').val());
				/*if(active_section == 'friend'){
					getresult(active_section, list_count);
				}else if(active_section == 'follower'){
					alert('11');
				}else if(active_section == 'following'){
					alert('111');
				}*/
				if(jQuery('#list_count').val() != '0'){
					getmoreresult(active_section, list_count);
				}
		}
	});
function getmoreresult(section, list_count) {
	if(section == 'friend'){
			jQuery('.process_image1').show();
	}else if(section == 'follower'){
			jQuery('.process_image2').show();
	}else if(section == 'following'){
			jQuery('.process_image3').show();
	}
	jQuery.post("<?php echo $this->baseUrl().'/members/edit/getmorefriends';?>",{section:section, list_count:list_count}, function(resp){
		//jQuery('.loading_image').hide();
		var response	=	jQuery.trim(resp).split('**');
		if(section == 'friend'){
				jQuery('#user_profile_friends1').append(jQuery.trim(response[0]));
				jQuery('.process_image1').hide();
				if(response[0] == ''){
					jQuery('#list_count').val(0);
				}else{
					jQuery('#list_count').val(response[1]);
				}
		}else if(section == 'follower'){
				jQuery('#user_profile_friends_followers').append(jQuery.trim(response[0]));
				jQuery('.process_image2').hide();
				if(response[0] == ''){
					jQuery('#list_count').val(0);
				}else{
					jQuery('#list_count').val(response[1]);
				}
		}else if(section == 'following'){
				jQuery('#user_profile_friends_following').append(jQuery.trim(response[0]));
				jQuery('.process_image3').hide();
				if(response[0] == ''){
					jQuery('#list_count').val(0);
				}else{
					jQuery('#list_count').val(response[1]);
				}
		}
	});
	
}

 jQuery('.custom_605').parent().addClass('active');
 jQuery('.custom_606').parent().addClass('active');
 jQuery('.custom_607').parent().addClass('active');

</script>

