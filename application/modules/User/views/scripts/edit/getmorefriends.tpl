<?php if($this->section == 'friend'){ ?>
    <?php
      if($this->myfriends){
      foreach( $this->myfriends as $friends ):
        $friend = Engine_Api::_()->getItem('user', $friends['friend_id']);
        ?>
        <li id="user_friend_<?php echo $friend->getIdentity() ?>">
          <?php echo $this->htmlLink($friend->getHref(), $this->itemPhoto($friend, 'thumb.icon'), array('class' => 'profile_friends_icon')) ?>
          <div class='profile_friends_body'>
            <div class='profile_friends_status'>
              <span>
                <?php echo $this->htmlLink($friend->getHref(), $friend->getTitle()) ?>
              </span>
              <div>
<a style="background-image: url(<?php echo $this->baseUrl();?>/application/modules/User/externals/images/friends/remove.png);" class="buttonlink smoothbox remove_friend" href="<?php echo $this->baseUrl();?>members/friends/removefriend/user_id/<?php echo $friend->getIdentity();?>">Unfriend</a> 
          </div>
            </div>
          </div>
        </li>
      <?php endforeach;
      } ?>
<?php }else if($this->section == 'follower'){?>
<?php 
  foreach( $this->follower as $membership ):
    if( !isset($this->friendUsers[$membership->user_id]) ) continue;
    $member = $this->friendUsers[$membership->user_id];
    ?>
    <li id="user_friend_<?php echo $member->getIdentity() ?>">
      <?php echo $this->htmlLink($member->getHref(), $this->itemPhoto($member, 'thumb.icon'), array('class' => 'profile_friends_icon')) ?>
      <div class='profile_friends_body'>
        <div class='profile_friends_status'>
          <span>
            <?php echo $this->htmlLink($member->getHref(), $member->getTitle()) ?>
          </span>
        </div>
<div>
        <a style="background-image: url(<?php echo $this->baseUrl()?>/application/modules/User/externals/images/friends/remove.png);" class="buttonlink smoothbox remove_follow" href="<?php echo $this->baseUrl()?>/members/friends/remove/user_id/<?php echo $member->getIdentity() ?>/rev/1">Remove</a>
      </div>
      </div>
    </li>
  <?php  endforeach;  ?>
<?php } else if($this->section == 'following'){ ?>
 <?php foreach( $this->following as $membership2 ):
    if( !isset($this->friendUsers2[$membership2->resource_id]) ) continue;
    $member2 = $this->friendUsers2[$membership2->resource_id];
    ?>
    <li id="user_friend_<?php echo $member2->getIdentity() ?>">
      <?php echo $this->htmlLink($member2->getHref(), $this->itemPhoto($member2, 'thumb.icon'), array('class' => 'profile_friends_icon')) ?>
      <div class='profile_friends_body'>
        <div class='profile_friends_status'>
          <span>
            <?php echo $this->htmlLink($member2->getHref(), $member2->getTitle()) ?>
          </span>
        </div>
        <div>
        <?php echo $this->userFriendship($member2) ?>
      </div>
      </div>
    </li>
  <?php $j++; endforeach;
  ?>
<?php } ?>
<?php 
echo '**';
$page	=	$this->page;
echo $page+20;
?>