<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: profile.tpl 9984 2013-03-20 00:00:04Z john $
 * @author     John
 */
?>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
*
{
  font-family: 'Roboto', sans-serif !important;
}
.thumb_profile, .thumb_icon {border-radius:0 !important;}
form.global_form .form-elements label{ display:none;}
.CaptionCont.SlectBox > label, .select-all.partial > label, .select-all > label{ display:block !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
.hidebox{
    display: none;
  }
.fancybox-overlay{z-index:999999 !important;}
.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
      background:transparent !important;
      padding:0 10px !important;
      width:100%;
}
.layout_main .layout_middle {
    width: 100% !important;
   background-color: #E9EAED !important;
}
.skip_div.abs_d {
    margin-top: 7px;
}

.global_form #1_1_6-label { display:none;}
.global_form div.form-label { display:none;}



div.field-privacy-selector > span.caret{display:none;}

.adding_cat_height{margin-top: 145px;} /* add 25px + .form-errors */

.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:auto;
  width:100%;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}

ul.form-errors > li, ul.form-notices > li {
 float:none;
}
body#global_page_user-edit-profile .category_list .category {
    height: 100% !important;
  width:357px !important;
}
#autoSuggestionsList > li {
         background: none repeat scroll 0 0 #FFF;
         border-bottom: 1px solid #E3E3E3;
         list-style: none outside none;
         padding: 3px 15px 3px 15px;
         text-align: left;
    }
    .auto_list {
         border: 1px solid #E3E3E3;
         border-radius: 5px 5px 5px 5px;
         position: absolute;
         cursor:pointer;
         z-index: 999 !important;
         max-height: 250px;
         overflow: auto;
         }
      
    
     #autoSuggestionsList > li:hover,#autoSuggestionsList > li.selected {
        background-color: #dfdfdf;
    }
    #autoSuggestionsLists > li {
         background: none repeat scroll 0 0 #FFF;
         border-bottom: 1px solid #E3E3E3;
         list-style: none outside none;
         padding: 3px 15px 3px 15px;
         text-align: left;
    }
    .auto_lists {
         border: 1px solid #E3E3E3;
         border-radius: 5px 5px 5px 5px;
         position: absolute;
         cursor:pointer;
         z-index: 999 !important;
         max-height: 250px;
         overflow: auto;
         }
      
    
     #autoSuggestionsLists > li:hover,#autoSuggestionsLists > li.selected {
        background-color: #dfdfdf;
    }

    #global_content > div > div.generic_layout_container.layout_main > div > div > div.tabs2
    {
      width: 73%;
      background-color: #fff !important;
    }
    #global_content > div > div.generic_layout_container.layout_main > div > div > div > div.tabs > ul > li
    {
      width: 100%;
      padding-top: 5px !important;
    }
    .tabs
    {
      padding:0px !important;
      width: 20%;
      background-color: #fff !important;
      /*border-right: 1px solid #ccc;*/
      float: left;
    } 
    .login_my_wrapper #global_content .layout_main {
      padding: 10px 1% !important;
    } 
   #global_content > div > div.generic_layout_container.layout_main > div > div > div > div.desi
   {
    /*top: 5px;*/
    position:relative;
    width: 74%;
    margin-top: 10px;
    padding-left: 0px;
    padding-right: 0px;
    left: 20%;
    }
    #global_content > div > div.generic_layout_container.layout_main > div > div > div > div.iexp {
    /*top: 5px;*/
    position:relative;
    width: 74%;
    margin-top: 10px;
    padding-left: 0px;
    padding-right: 0px;
    left: 20%;
} 
    #global_content > div > div.generic_layout_container.layout_main > div > div > div > div.desi > h3
    {
      top: 10px;
    } 
    #designation
    {
      border-radius: 0px;
    } 
    #global_content > div > div.generic_layout_container.layout_main > div > div > div > div.desi > div.savecat
    {
      width: 20%;
      border-radius: 0px;
      margin-top: 10px;
    } 
    
    .SumoSelect > .CaptionCont {
    background-color: #fff !important;
    border-radius: 0px !important; 
    max-width: 100%;
    }
    .SumoSelect > .optWrapper
    {
      background-color: #fff !important;
    }
    .tabs
    {
      position: absolute;
    }
    .tab3
    {
      position: relative;
      left: 25%;
      background-color: #fff;
      width: 70%;
      border-left: 1px solid #ccc;
      padding-top: 1px;
    }
    .savecat
    {
    position: relative;
    margin-top: 0%;
    margin-bottom: 10px;
    border-bottom: 1px solid #ccc;
    width: 100%;
    }
    .global_form div.form-wrapper-heading>span {
      position: relative;
    }
    body#global_page_user-edit-profile .SumoSelect .select-all {
    background: #fff;
    max-width: 100%;
    }
    #\31 _1_5-element
    {
      overflow: overlay !important;
    }
    #\31 _1_6-element
    {
      overflow: overlay !important;
    }
    #\31 _1_462-element
    {
      overflow: overlay !important;
    }
    #\31 _1_464-element
    {
      overflow: overlay !important;
    }
    #\31 _1_451-element
    {
      overflow: overlay !important;
      max-width: 100%;
    }
    #\31 _1_421-element
    {
      max-width: 100%;
    }
    #\31 _1_456-element
    {
      max-width: 100%;
    }
    #\31 _1_465-wrapper
    {
      width: 100% !important;
    }
    #\31 _1_465-element
    {
      min-width: 100% !important;
    }
    #\31 _1_5-wrapper
    {
      width: 50% !important;
    }
    #\31 _1_6-wrapper
    {
      width: 49% !important;
      padding-right: 5px !important;
    }
    #\31 _1_434-wrapper
    {
      display: none;
    }
    #\31 _1_421-wrapper
    {
      width: 100% !important;
    }
    #\31 _1_458-wrapper
    {
      width: 49.5% !important;
    }
    #\31 _1_459-wrapper
    {
      display: none;
    }
    #\31 _1_460-wrapper
    {
      width: 49.5% !important;
    }
    #\31 _1_462-wrapper
    {
      width: 49.5% !important;
    }
    #\31 _1_463-wrapper
    {
      display: none;
    }
    #\31 _1_464-wrapper
    {
      width: 49.5% !important;
    }
    #\31 _1_451-wrapper
    {
      width: 100% !important;
    }
    #\31 _1_456-wrapper
    {
      width: 100% !important;
    }
    #\31 _1_461-wrapper
    {
      margin-top: 10px;
    }
.SumoSelect > .optWrapper.open
  {
    position: absolute !important;
  }

</style>

<div class="layout_page_user_edit_profile">
<div class="generic_layout_container layout_top">
</div>

<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
<h2>Edit profile </h2>

<div class="tabs2"> 
<div class="alert alert-info" id="redirectcheck" style="display: none;">
  <strong>Info!</strong> You're here because you haven't filled your Designation Category and Personal Information.
</div>
<div class="tabs">
        <?php
          // Render the menu
           echo $this->navigation()
            ->menu()
            ->setContainer($this->navigation)
            ->render();
       ?>
</div>

<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
      'topLevelId' => (int) @$this->topLevelId,
      'topLevelValue' => (int) @$this->topLevelValue
    ))
?>

<?php
  $this->headTranslate(array(
    'Everyone', 'All Members', 'Friends', 'Only Me',
  ));
?>
<?php 
if($this->statusmsg){
  //echo '<ul class="form-notices"><li>'.$this->statusmsg.'</li></ul>';
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function(){
       jQuery('.loading_image').hide();
    /*jQuery.fancybox({
            'width': '500px',
            'height': '200px',
            //'autoScale': true,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'type': 'iframe',
            'href': '<?php echo $this->baseUrl()."/members/edit/pathapproval";?>'
        });*/
  });
    </script>
<?php 
}
?>
<div class="tab3">
<!--Designation start-->
     <div class="desi">
  <h3>Designation Category</h3>
  
    <div class="desigs">
<?php if($this->userdcs){$i=0;
        foreach($this->userdcs as $dc){
            echo "<p class='desig_edit' id='".$dc['dasignatedcategories_id']."'>
            <span class='desig_set'>
           <span class='desig bg_none'>".$dc['dasignatedcategory_title']."</span>";?>
            <?php echo "</span></p>";
          $i++;
        }
    }
    if($i==0){ ?>
      No Designation added yet.
    <?php } ?>
</div>
<div class="form-element designation_list">
<select class="field_container designations" id="designation"  multiple="multiple">
  <?php if($this->dasignatedsubcategories){
      foreach($this->dasignatedsubcategories  as $key=>$val){?>
        <option  value="<?php echo $key;?>" <?php if(in_array($key, $this->userdc)){echo 'selected="selected"';}?>><?php echo $val?></option>
      <?php }
   }?>
</select>
</div>
   <div class="savecat">
      <input type="submit" name="submitdesignation" id="submitdesignation" value="Save">      
      </div>
</div>
<!-- Designation end-->

<!--Industries Experience start-->
<div class="iexp">
<h3 >Domain Experience</h3>
  
    <div class="exps">
<?php if($this->useriexps){$i=0;
        foreach($this->useriexps as $exps){
            echo "<p class='exp_edit' id='".$exps['ind_primary_id']."'>
            <span class='exp_set'>
           <span class='exp bg_none'>".$exps['industryexperience_title']."</span>";?>
            <?php echo "</span></p>";
          $i++;
        }
    }
    if($i==0){ ?>
      No Domain Experience added yet.
    <?php } ?>
</div>
<div class="form-element userindus_list">
<select class="field_container userindus" id="industryexp"  multiple="multiple">
  <?php if($this->industryexperience){
      foreach($this->industryexperience  as $key=>$val){?>
        <option  value="<?php echo $key;?>" <?php if(in_array($key, $this->useriexp)){echo 'selected="selected"';}?>><?php echo $val?></option>
      <?php }
   }?>
</select>
</div>
   <div class="savecat">
      <input type="submit" name="submitdomainexp" id="submitdomainexp" value="Save">      
      </div>
</div>
<!-- Industries Experience end-->

<!--My Functonal Area start-->
<div class="func">
<h3 >Functional Experience</h3>
     <div class="funs">
<?php if($this->userfas){$i=0;
        foreach($this->userfas as $funs){
            echo "<p class='fun_edit' id='".$funs['fa_primary_id']."'>
            <span class='fun_set'>
           <span class='fun bg_none'>".$funs['functionalareas_title']."</span>";?>
            <?php echo "</span></p>";
          $i++;
        }
    }
    if($i==0){ ?>
      No Functional Experience added yet.
    <?php } ?>
</div>

  <div class="form-element functionalarea_list">
<select class="field_container functionalarea" id="functionalarea"  multiple="multiple">
  <?php if($this->functionalareas){
      foreach($this->functionalareas  as $key=>$val){?>
        <option  value="<?php echo $key;?>" <?php if(in_array($key, $this->userfa)){echo 'selected="selected"';}?>><?php echo $val?></option>
      <?php }
   }?>
</select>
</div>
<div class="savecat">
      <input type="submit" name="submitfunexp" id="submitfunexp" value="Save">      
      </div>
</div>
<!-- My Functonal Area end-->

<!--My Skills start-->
<div class="skill">
<h3 >Skills</h3>
     <div class="skills">
<?php if($this->userskills) {
 $i=0;
        foreach($this->userskills as $skills){
            echo "<p class='skill_edit' id='".$skills['skill_primary_id']."'>
            <span class='skill_set'>
           <span class='skill bg_none'>".$skills['skill_title']."</span>";?>
            <?php echo "</span></p>";
          $i++;
        }
    }
    if($i==0){?>
      No Skill added yet.
    <?php } ?>
</div>

  <div class="form-element skill_list">
<select class="field_container skill" id="skill"  multiple="multiple">
  <?php if($this->skills){
      foreach($this->skills  as $key=>$val){?>
        <option  value="<?php echo $key;?>" <?php if(in_array($key, $this->userskill)){echo 'selected="selected"';}?>><?php echo $val?></option>
      <?php }
   }?>
</select>
</div>
<div class="savecat">
      <input type="submit" name="submitskills" id="submitskills" value="Save">      
      </div>
</div>
<!-- My Skills end-->
<?php echo $this->form->render($this) ?>


</div>
</div>
</div>
</div>
</div>
</div>

<!-- PROFILE FORM START -->
<script type="text/javascript">
  window.addEvent('domready', function() {
    en4.user.buildFieldPrivacySelector($$('.global_form *[data-field-id]'));
  });
 jQuery('.custom_292').parent().addClass('active');
 jQuery('.custom_311').parent().addClass('active');
 jQuery('.custom_324').parent().addClass('active');
 
  
 jQuery(document).ready(function(){
  jQuery('#skill_ids-wrapper, #desig_ids-wrapper, #cat_ids-wrapper, #indu_ids-wrapper, #func_ids-wrapper').css('display','none');
  var url=window.location.href;
  var value = url.substring(url.lastIndexOf('/') + 1);
  console.log('last value'+value);
  if(value=='novalue')
  {
    jQuery('#redirectcheck').delay(500).fadeIn('normal', function() {
      jQuery(this).delay(2500).fadeOut();
   });
  }

  jQuery('<div id="autoSuggestionsList" autocomplete="off" style="display: block;margin-top: -10px;width: 99%;"></div>').appendTo( jQuery( "#1_1_458-wrapper" ) );
  jQuery('<div id="autoSuggestionsLists" autocomplete="off" style="display: block;margin-top: -10px;width: 99%;"></div>').appendTo( jQuery( "#1_1_460-wrapper" ) );
  jQuery("#1_1_458").attr("autocomplete","off");
  jQuery("#1_1_460").attr("autocomplete","off");
  jQuery('#1_1_421').SumoSelect({selectAll: true,placeholder: 'Select Language'});
  jQuery('#1_1_455').attr('placeholder','Hobbies & Interests (e.g. Singing,Dancing,Acting etc)');
  //jQuery('#1_1_433-wrapper').css("display", "none");
  jQuery("#1_1_5 option[value='']").text('Gender');
  jQuery("#1_1_6-day option[value='0']").text('Day');
  jQuery("#1_1_6-month option[value='0']").text('Month');
  jQuery("#1_1_6-year option[value='0']").text('Year');
  jQuery("#1_1_451 option[value='']").text('Select Relationship status');
  jQuery("#1_1_464 option[value='']").text('Country');

  jQuery('.required').each(function(){
    var area  = jQuery(this).parent().next();
    jQuery( '<span class="requied_field" style="font-weight: bold; color: rgb(255, 0, 0); font-size: 16px;"> *</span>' ).insertAfter( area );
    
  });
  
  jQuery('.designations').SumoSelect({ triggerChangeCombined: true,selectAll: false,placeholder: 'Select Designations'});
    jQuery('.userindus').SumoSelect({ triggerChangeCombined: true,selectAll: false,placeholder: 'Select Domain Experiences'});

  jQuery('.functionalarea').SumoSelect({ triggerChangeCombined: true,selectAll: false,placeholder: 'Select Functonal Areas'});
  jQuery('.skill').SumoSelect({ triggerChangeCombined: true,selectAll: false,placeholder: 'Select skills'});
    
jQuery(document).on("click","#submitdesignation", function(){
  var desig_ids = jQuery('#designation').val();
  
      jQuery('.loading_image').show();
      jQuery.post("<?php echo $this->baseUrl();?>/members/edit/addremovesdesig",{desig_ids:desig_ids}, function(resp){
            var data=jQuery.parseJSON(resp);
            if(data.status == "Error!")
             {
              jQuery('.loading_image').hide();
              return false;
             }
             else
             {
              jQuery('.loading_image').hide();
              window.location.href='/members/edit/profile';
             }
          });

});
jQuery(document).on("click","#submitdomainexp", function(){
  var exp_ids = jQuery('#industryexp').val();
 
      jQuery('.loading_image').show();
      jQuery.post("<?php echo $this->baseUrl();?>/members/edit/addremovesdomexp",{exp_ids:exp_ids}, function(resp){
            var data=jQuery.parseJSON(resp);
            if(data.status == "Error!")
             {
              jQuery('.loading_image').hide();
              return false;
             }
             else
             {
              jQuery('.loading_image').hide();
             window.location.href='/members/edit/profile';
             }
          }); 
  
});

jQuery(document).on("click","#submitfunexp", function(){
  var func_ids = jQuery('#functionalarea').val();
      jQuery('.loading_image').show();
      jQuery.post("<?php echo $this->baseUrl();?>/members/edit/addremovesfunexp",{func_ids:func_ids}, function(resp){
            var data=jQuery.parseJSON(resp);
            if(data.status == "Error!")
             {
              jQuery('.loading_image').hide();
              return false;
             }
             else
             {
              jQuery('.loading_image').hide();
              window.location.href='/members/edit/profile';
             }
          }); 

});


jQuery(document).on("click","#submitskills", function(){
  var skill_ids = jQuery('#skill').val();
      jQuery('.loading_image').show();
      jQuery.post("<?php echo $this->baseUrl();?>/members/edit/addremoveskill",{skill_ids:skill_ids}, function(resp){
            var data=jQuery.parseJSON(resp);
            if(data.status == "Error!")
             {
              jQuery('.loading_image').hide();
              return false;
             }
             else
             {
              jQuery('.loading_image').hide();
              window.location.href='/members/edit/profile';
             }
          }); 

});
 });

</script>


<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.css" media="screen" />
<script src="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.js"></script>
<link href="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.css" rel="stylesheet" />
<script type="text/javascript">


   function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || jQuery(element).val().indexOf('-') != -1) &&      // "-" CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || jQuery(element).val().indexOf('.') != -1) &&      // "." CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
  
jQuery('#submit-element').click(function(){
       jQuery('.loading_image').show();
    });

jQuery(document).on('keyup','#1_1_458', function(e) {
         var inputString = jQuery(this).val();
         if(inputString.length < 1) {
                jQuery('#autoSuggestionsList').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/user/edit/getcities";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                  
                      if(data)
                        {
                            jQuery('#autoSuggestionsList').show();
                            jQuery('#autoSuggestionsList').addClass('auto_list');
                            jQuery('#autoSuggestionsList').html(data);
                        }
                        else
                        {
                            jQuery('#autoSuggestionsList').hide();
                        }
                           
                     });
                 
             }
            
    });
      jQuery(document).on('click','#autoSuggestionsList li',function(){ 
            var el=jQuery(this).text();
           jQuery('#1_1_458').val(el);
        setTimeout("jQuery('#autoSuggestionsList').hide();", 200);
       });

      jQuery(document).on('keyup','#1_1_460', function(e) {
         var inputString = jQuery(this).val();
         if(inputString.length < 1) {
                jQuery('#autoSuggestionsLists').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/user/edit/getstates";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                  
                      if(data)
                        {
                            jQuery('#autoSuggestionsLists').show();
                            jQuery('#autoSuggestionsLists').addClass('auto_lists');
                            jQuery('#autoSuggestionsLists').html(data);
                        }
                        else
                        {
                            jQuery('#autoSuggestionsLists').hide();
                        }
                           
                     });
                 
             }
            
    });
      jQuery(document).on('click','#autoSuggestionsLists li',function(){ 
            var el=jQuery(this).text();
           jQuery('#1_1_460').val(el);
         setTimeout("jQuery('#autoSuggestionsLists').hide();", 200);
       });
</script>
