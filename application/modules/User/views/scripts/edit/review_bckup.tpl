<style type="text/css">
.rating_star_big_disabled {
    background-image: url("<?php echo $this->baseUrl();?>/application/modules/User/externals/images/star_big_disabled.png");
}
.ynmember_rating_star_generic {
    background-repeat: no-repeat;
    cursor: pointer;
    display: inline-block;
    float: left;
    font-size: 1px;
    height: 24px;
    width: 24px;
}
.rating_star_big {
    background-image: url("<?php echo $this->baseUrl();?>/application/modules/User/externals/images/star_big.png");
}
</style>

<?php 
$this -> headScript()
      -> appendFile($this->layout()->staticBaseUrl . 'application/modules/Ynmember/externals/scripts/core.js');
?>
<div class="headline">
  <h2>
    <?php if ($this->viewer->isSelf($this->user)):?>
      <?php echo $this->translate('Reviews');?>
    <?php else:?>
      <?php echo $this->translate('%1$s\'s Profile', $this->htmlLink($this->user->getHref(), $this->user->getTitle()));?>
    <?php endif;?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
        echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
		
    ?>
  </div>
</div>
<!--<?php if ($this -> reviews -> getTotalItemCount()):?>
<?php foreach ($this -> reviews as $review): ?>
	<div class="ynmember-review-detail-main" id="main_row_<?php echo $review->review_id;?>">
		<div class="ynmember-review-item-rate">
			

			<span class="ynmember-review-item-rate-avatar">
				<?php
					$resourceUser = Engine_Api::_()->user()->getUser($review->reviewer_id);
					if (is_null($resourceUser))
						continue;
				?>


				<?php echo $this->itemPhoto($resourceUser, 'thumb.icon');?>
			</span>
			
			<span class="ynmember-review-item-rate-author">			
			
			<?php 
				$reviewer = Engine_Api::_()->user()->getUser($review->reviewer_id);
				echo $this->htmlLink($reviewer->getHref(), $reviewer->getTitle());
			?>			
			</span>
			<div class="clear"></div>
			<span class="ynmember-review-item-rate-time">
			<?php
		        // Convert the dates for the viewer
		        $reviewDateObject = new Zend_Date(strtotime($review->review_date));
		        if( $this->viewer() && $this->viewer()->getIdentity() ) 
		        {
					$tz = $this->viewer()->timezone;
					$reviewDateObject->setTimezone($tz);
		        }
		    ?>
		    <?php echo  $this->timestamp($review->review_date);
			//echo $this->locale()->toDate($reviewDateObject) . " ". $this->locale()->toTime($reviewDateObject); ?>
		    </span>		    
		</div>

		<h3 class="ynmember-review-item-title">
			<?php echo $review->title;?>
			
		</h3>

		<div class="ynmember-review-item-rate-group">
				<div class="ynmember-review-item-rate-item ynmember-clearfix">
					<?php echo $this->partial('_review_rating_big.tpl', 'user', array('rate_number' => $review->rating));?>
				</div>
		</div>

		<div class="ynmember-review-item-description">
			<?php echo $review -> summary;?>
		</div>
        
        <?php if($this->viewer->level_id != 2){
         if($review->user_comment != ""){$style1 = "style='display:none;'";}else{$style1 = "style='display:block;'";}?>
        <div class="comment_box-<?php echo $review->review_id;?>" <?php echo $style1;?>>
        	<textarea placeholder="Write Comment..." rows="1" cols="45" id="review-<?php echo $review->review_id;?>" name="comment" style="overflow-x: auto; overflow-y: hidden; resize: none; padding-bottom: 0px; padding-top: 4px; padding-left: 4px; height: 100px; width:50% !important;"></textarea>
        	<button type="button" id="comment_submit-<?php echo $review->review_id;?>" name="submit" class="submit_button">Post</button>
            <button type="button" id="cancel_submit-<?php echo $review->review_id;?>" class="cancel_button" style="display:none;">Cancel</button>
        </div>
        
        <?php if($review->user_comment == ""){$style = "style='display:none;'";}else{$style = "style='display:block;'";}?>
        	<div class="user_comment-<?php echo $review->review_id;?>" <?php echo $style;?>>
        		<p class="comment-<?php echo $review->review_id;?>"><?php echo $review->user_comment;?></p>
                <span id="edit_comment-<?php echo $review->review_id;?>" class="edit_comment">edit</span>
        	</div>
        
        <?php } else{?>
        <div class="publish_button" id="review_status_<?php echo $review->review_id;?>">
        	<?php if($review->is_approved == '0') { ?><a href="javascript:void(0)" onclick="change_review_status('1', 'is_approved' , <?php echo $review->review_id;?>)" class="deactive_bt" style="cursor:pointer;font-weight: normal;">Publish</a><?php }
                if($review->is_approved == '1') {?>
                <a href="javascript:void(0)" onclick="change_review_status('0', 'is_approved' , <?php echo $review->review_id;?>)" class="active_bt" style="cursor:pointer;font-weight: normal;">Un-publish</a><?php } ?>
            
        </div>
        
        <div class="delete_button">
    	<a href="javascript:void(0)" onclick="change_review_status('0', 'is_deleted' , <?php echo $review->review_id;?>)" class="delete_bt" style="cursor:pointer;font-weight: normal;">Delete</a>
        </div>
        <?php } ?>
	</div>
     
<?php endforeach;?>
<?php else:?>
	<div class="tip">
    <span>
          <?php echo $this -> translate("Nobody has reviewed yet.");?>
   	</span>
  	</div>
<?php endif;?>-->


<?php if( count($this->reviews) ): 
?>
    <?php $i = 1;foreach( $this->reviews as $review ): ?>
      <div class="ynmember-review-detail-main" id="main_row_<?php echo $review->review_id;?>">
		<div class="ynmember-review-item-rate">
			

			<span class="ynmember-review-item-rate-avatar">
				<?php
					$resourceUser = Engine_Api::_()->user()->getUser($review->reviewer_id);
					if (is_null($resourceUser))
						continue;
				?>


				<?php echo $this->itemPhoto($resourceUser, 'thumb.icon');?>
			</span>
			
			<span class="ynmember-review-item-rate-author">			
			
			<?php 
				$reviewer = Engine_Api::_()->user()->getUser($review->reviewer_id);
				echo $this->htmlLink($reviewer->getHref(), $reviewer->getTitle());
			?>			
			</span>
			<div class="clear"></div>
			<span class="ynmember-review-item-rate-time">
			<?php
		        // Convert the dates for the viewer
		        $reviewDateObject = new Zend_Date(strtotime($review->review_date));
		        if( $this->viewer() && $this->viewer()->getIdentity() ) 
		        {
					$tz = $this->viewer()->timezone;
					$reviewDateObject->setTimezone($tz);
		        }
		    ?>
		    <?php echo  $this->timestamp($review->review_date);
			//echo $this->locale()->toDate($reviewDateObject) . " ". $this->locale()->toTime($reviewDateObject); ?>
		    </span>		    
		</div>

		<h3 class="ynmember-review-item-title">
			<?php echo $review->title;?>
			
		</h3>

		<div class="ynmember-review-item-rate-group">
				<div class="ynmember-review-item-rate-item ynmember-clearfix">
					<?php echo $this->partial('_review_rating_big.tpl', 'user', array('rate_number' => $review->rating));?>
				</div>
		</div>
        
        <div class="review_status">
        	<?php if($review->is_approved == '1'){?>
            	<span class="review_publish">Published</span>
            <?php }else { ?>
            	<span class="review_unpublish">Unpublished</span>
            <?php } ?>
        </div>

		<div class="ynmember-review-item-description">
			<?php echo $review -> summary;?>
		</div>
        
        <?php 
         if($review->user_comment != ""){$style1 = "style='display:none;'";}else{$style1 = "style='display:block;'";}?>
        <div class="comment_box-<?php echo $review->review_id;?>" <?php echo $style1;?>>
        	<div class="col-md-12 nopadding">
			<textarea class="review_comments" placeholder="Write Comment..." rows="1" cols="45" id="review-<?php echo $review->review_id;?>" name="comment"></textarea>
			</div>
			<div class="col-md-12 nopadding">
			<div class="col-md-2 nopadding">
        	<button type="button" id="comment_submit-<?php echo $review->review_id;?>" name="submit" class="submit_button">Post</button>
			</div>
			<div class="col-md-8"></div>
			<div class="col-md-2 nopadding">
            <button type="button" id="cancel_submit-<?php echo $review->review_id;?>" class="cancel_button" style="display:none;">Cancel</button>
			</div>
			</div>
        </div>
        
        <?php if($review->user_comment == ""){$style = "style='display:none;'";}else{$style = "style='display:block;'";}?>
        	
			<div class="row user_review">
			<div class="user_comment-<?php echo $review->review_id;?>" <?php echo $style;?>>
        		<div class="comment_review">
				<p class="comment-<?php echo $review->review_id;?>"><?php echo $review->user_comment;?></p>
				</div>
                <span id="edit_comment-<?php echo $review->review_id;?>" class="edit_comment edit_comment_review">Edit Comment</span>
        	</div>
			</div>
        
	</div>
       
    <?php endforeach; ?>
<?php else:?>
	<div class="tip">
    <span>
          <?php echo $this -> translate("Nobody has reviewed yet.");?>
   	</span>
  	</div>
<?php endif; ?>


<?php 
    $pagination = $this->paginationControl($this->reviews, null, null, array(
      'pageAsQuery' => true,
      'query' => $this->formValues,
    ));
  ?>
  <?php if( trim($pagination) ): ?>
    <div class='browsemembers_viewmore' id="browsemembers_viewmore" title="agency">
      <?php echo $pagination ?>
    </div>
  <?php endif ?>

<?php //echo $this->paginationControl($this->reviews); ?>
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/paginator.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/modernizr-1.7.min.js';?>"></script>
<script type="text/javascript">
jQuery(function($){
    $('.paginationControl').find('a').on('click', function(){
		$('.loading_image').show();
        var link = $(this);
        var container = link.parents('#global_content');
        $.get(link.attr('href'), { format: 'html'}, function(data){
			$('.loading_image').hide();
            container.html(data);
        }, 'html');
        return false;
    });
});
</script>
<script type="text/javascript">
 jQuery('.custom_602').parent().addClass('active');
 jQuery('.custom_603').parent().addClass('active');
 jQuery('.custom_604').parent().addClass('active');
 jQuery('.custom_641').parent().addClass('active');
 jQuery('.custom_661').parent().addClass('active');
 jQuery('.custom_621').parent().addClass('active');
  
function change_review_status(status, type, review_id){
			var review_id	=	review_id;
			jQuery('.loading_image').show();
			jQuery.post('<?php echo $this->baseUrl();?>/members/edit/changereviewstatus',{status:status,review_id:review_id, type:type},function(resp){
				if(type == 'is_deleted'){
					jQuery('#main_row_'+review_id).remove();	
				}else{
					jQuery('#review_status_'+review_id).html(resp);
				}
				jQuery('.loading_image').hide();
			});
 	
}

jQuery(document).on('click','.submit_button', function(){
	var button_id	=	jQuery(this).attr('id');
	var review_id_arr	=	button_id.split('-');
	var	review_id	=	review_id_arr[1];
	var comment	=	jQuery.trim(jQuery('#review-'+review_id).val());
	
	if(comment != ''){
		jQuery('.loading_image').show();
		jQuery.post("<?php echo $this->baseUrl().'/members/edit/postcomment';?>",{review_id:review_id,review_comment:comment}, function(resp){
			jQuery('.comment_box-'+review_id).css("display","none");
			jQuery('.user_comment-'+review_id).css("display","block");
			jQuery('.comment-'+review_id).html(comment);
			jQuery('.loading_image').hide();
		});	
	}
});

jQuery(document).on('click','.edit_comment', function(){
	var button_id	=	jQuery(this).attr('id');
	var review_id_arr	=	button_id.split('-');
	var	review_id	=	review_id_arr[1];
	var comment	=	jQuery.trim(jQuery('.comment-'+review_id).html());
	jQuery('#review-'+review_id).val(comment);
	jQuery('.user_comment-'+review_id).css("display","none");
	jQuery('.comment_box-'+review_id).css("display","block");
	jQuery('#cancel_submit-'+review_id).css("display","block");
});

jQuery(document).on('click', '.cancel_button', function(){
	var button_id	=	jQuery(this).attr('id');
	var review_id_arr	=	button_id.split('-');
	var	review_id	=	review_id_arr[1];
	jQuery('.comment_box-'+review_id).css("display","none");
	jQuery('.user_comment-'+review_id).css("display","block");
});
</script>

