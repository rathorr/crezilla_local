<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: browse.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */
?>
<style type="text/css">
body{
  overflow-x:hidden!important; 
  font-family: 'Lato' !important;
  font-size: 13px;
}
._banner_section{
  background: url('img/banner-img.jpg') no-repeat center center;
  background-size: cover;
  min-height: 200px;  
  position: relative;
  margin-bottom: 36px;
  padding: 0px;
} 
._form_area{
  position: absolute;
  height: 40px;
  top: 50%;
  width: 100%;
  margin:-20px 0 0;
}
.browsemembers_criteria ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
/*.browsemembers_criteria ul li{
  display: inline-block;  
  min-width: 200px;
  max-width: 200px;
  margin-right: 28px;
  margin-bottom: 11px;
}*/
._job_results,
._community_ads {
  padding: 0 15px!important;
}
._job_results_count {
    font-size: 18px;
    text-transform: uppercase;
}
._job_results_tab {
    text-align: right;
    font-size: 14px;
}
._job_results_tab ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
._job_results_tab ul li{
  display: inline-block;
}
._job_results_tab ul li a{
  padding: 8px 8px;
  text-decoration: none;
  color: #000;
  border-bottom: 2px solid transparent;
}
._job_results_tab ul li.active a{
  border-color:#f55731; 
}

._job_results_head{
  float:left;
  width: 100%;
  padding-bottom: 8px;
  border-bottom: 1px solid #e1e1e1;
  margin-bottom: 30px;
}
/*._job_results_list_item {
    background: #fff;
    padding: 10px 5px;
}*/

._job_list_item_block {
    background: #fff;
    padding: 10px 5px;
    float: left;
    width: 100%;
    margin-bottom: 30px;
}
._item_block_head{
  float: left;
  width: 100%;
  padding-bottom: 10px;
  padding-left: 10px;
}
._createdby,
.viewedby {
    font-size: 14px;
    color: #888888;
}
._createdby_name{
  color: #000000;
}
._createdby{
  float:left;
}
.viewedby{
  float: right;
  padding-right: 15px;
}
._item_block_content{
  float: left;
  width: 100%;
}
._job_item_img {
    float: left;
    height: 105px;
    width: 105px;
} 
._job_item_content{
  width: calc(100% - 105px);
    float: right;
    padding: 0 9px;
}
._job_title {
    padding-bottom: 10px;
}
._job_title a{
   color: #333;
    font-size: 18px;
 }
 ._job_details {
    font-size: 14px;
    color: #333;
    padding-bottom: 10px;
}
 ._job_details i {
  color: #cacaca;
  padding-right:3px;
 } 
 span._job_loc {
    padding-left: 25px;
}
._job_desc{
  font-size: 14px;
  color: #333;
}
._job_desc_title {
    font-size: 12px;
    color: #888;
}
._item_block_footer {
    float: left;
    width: 100%;
    padding-left: 10px;
}
._addfolderr_icn{
  width: 18px;
  height: 18px;
  display: inline-block;
  background: url('public/jobs/add_folder_icn.png') no-repeat center center;
  background-size: cover;
  margin-right: 10px;
}
._item_block_footer a {
    float: left;
    width: 18px;
    margin-right: 10px;
    color: #cacaca;
    font-size: 18px;
}
._community_ads_area{
  background: #ffffff;
  border:1px solid #e6e7e8;
}
._community_ads_head {
    padding: 8px 0 3px;
}
._community_ads_head a{
  color: #666666;
  font-size: 14px;
  line-height: 1.4;
}
._community_ads_head a._createads{
  float: right;
  color: #f55731;
}
._community_ads_img{
  margin-bottom: 8px;
}
._community_ads_img img{
  width: 100%;
}
._community_ads_desc{
  font-size: 14px;
  color: #666666;
}
._community_ads_desc span{
  float: left;
  width: 100%;
}
._clear{clear:both;}
._community_ads_desc a{
  color: #666;
}
._community_ads_desc a._community_ads_title{
  color: #000;
  font-weight: bold;
}
._community_ads_items {
    border-bottom: 1px solid #e6e7e8;
    padding-top: 10px;
}
._community_ads_items:last-child{
    border-bottom: none;
    padding-bottom: 0px;
}
#job_result > li > div > div > div._item_block_content > div._job_item_img > img
{
  max-height: 105px;
  max-width: 105px;
}
#job_results > div > div > div > div > div._item_block_content > div._job_item_img > img
{
  max-height: 105px;
  max-width: 105px;

}
#job_results > div > div > div > div > div._item_block_content > div._job_item_content > div._job_desc > p
{
  font-size: 14px !important;
  }
  #job_result > li > div > div > div._item_block_content > div._job_item_content > div._job_desc > p
  {
    font-size: 14px !important;
  }

#job_results > form > div.browsemembers_criteria2
{
  padding: 10px;
  background-color: #E9EAED;
  text-align: right;
  border-bottom: 1px solid #ccc;
  margin-bottom: 3%;
}
  #job_results > form > div > ul > li
  {
    display: inline;
    margin-top: 0;
    width: 10% !important;
    padding: 10px;
    margin-right: 10px;
    color: #000;
  }
 #job_results > form > div > ul > li > label > a
 {
  color: #000;
 }
 .jobselect
 {
  color: #fb9236 !important;
  border-bottom: 2px solid;
  border-color: #f55731 !important;
 }
 .browsemembers_criteria
 {
  background: url("public/jobs/banner-img.jpg") no-repeat center center;
    background-size: cover;
    min-height: 200px;
    position: relative;
    /*margin-bottom: 36px;*/
    padding: 0px;
 }
 
#global_content > div > div > div.generic_layout_container.layout_user_browse_search > form > div > ul
{
    padding-top: 6%;
    padding-left: 8%;
 }
 
 .container {
    padding-left: 0px !important;
    padding-right: 0px !important;
}
._job_applied{
    padding-right: 15px;
    padding-left: 15px;
}
._suggested_job
{
    padding-right: 15px;
    padding-left: 30px;
}
  #masthead
  {
    margin-top: 8%;
  }
  div.modal-dialog
  {
        padding-top: 5%;
  }
  .modal-footer_des
  {
    background-color: #e6f8ff !important;
    border: 1px solid #98d7f1 !important;
    overflow: hidden;
    margin: 10px 14px !important;
    padding: 10px !important;
  }
  div.modal-header > p
  {
    font-size: 14px;
    color: #777;
    margin-top: 0px;
  }
div.modal-header > h3
{
  font-size: 18px !important;
  color: #5a5a5a !important;
  margin-top: 10px !important;
  margin-bottom: 0px !important;
}
div.modal-header
{
     /*top-right-bottom-left*/
          padding: 0px 0px 0px 10px !important;
          border-bottom: none !important;
}
div.modal-content
{
  display: block !important;
    padding: 0% !important;
    overflow-y: auto;
    margin: 0px;
    padding-top: 15%;
}
div.modal-body
{
     padding: 0px 0px 0px 10px !important;
         margin-top: -15px !important;
}
div.modal-footer
{
  padding: 10px 0px 0px 10px !important;
    text-align: left !important;
    border-top: none !important;
}
div.modal-footer > button:nth-child(1)
{
  background-color: #eb8831 !important;
  border-radius: 5px !important;
  color: #FFFFFF!important;
    padding: 10px 15px !important;
    font-size: 12px !important;
}
div.modal-footer > button:nth-child(2)
{
  background-color: #5a5a5a;
    border-radius: 5px !important;
    color: #fff;
    padding: 10px 12px !important;
    text-transform: capitalize;
    font-size: 12px !important;
}
#popup_box { 
    display:none;
    position:fixed; 
    height:100px;  
    width:300px;  
    background: #fff;  
    left: 20%;
    top: 50%;
    /*margin-left: -100px;
    margin-top: -150px;*/
    z-index:100;     
    padding:15px;  
    font-size:12px;  
    -moz-box-shadow: 0 0 5px;
    -webkit-box-shadow: 0 0 5px;
    box-shadow: 0 0 5px;
    background-color: #dbdbdb;
}
#overlay {
    background:rgba(0,0,0,0.3);
    display:none;
    width:100%; height:100%;
    position:absolute; top:0; left:0; z-index:99998;
}

.padrgt0{padding-right: 0!important}
.padlft0{padding-left: 0!important}
._job_applied_head {
    float: left;
    width: 100%;
    padding-bottom: 8px;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 10px;
    font-size: 18px;
    text-transform: uppercase;
}
._job_applied_list,
._suggested_job_list{
    clear: both;
}
._job_applied_list ul,
._suggested_job_list ul{
    margin:0;
    padding:0;
    list-style: none;   
}
._job_applied_list ul li,
._suggested_job_list ul li{
    float:left;
    width: 100%;
    margin-bottom: 10px;
}
.edu_listingj{
    background:#fff;
    padding:8px;
    float: left;
    width: 100%;
}
.edu_listingj .title .exp-titlej{
    display: block;
    font-size: 18px;
    color: #000;
    text-transform: capitalize;
    font-weight: 600 !important;   
    padding-bottom: 7px; 
}
.edu_listingj .title .exp-titlej a{
    color: #333;
    font-weight: 600 !important;   
}

._applied_job_desc label,
._applied_job_postedby label,
._applied_job_applyby label,
._applied_job_appliedon,
._applied_job_appliedon label,
._applied_job_status label{
    font-size: 12px;
    color: #888888;
    font-weight: normal;
}
._applied_job_desc{
    margin-bottom: 8px;
}
._applied_job_desc label{
    display: block;
    margin-bottom: 1px;
}
._applied_job_desc span{
    font-size:14px;
    color:#333;
}
._applied_job_postedby a,
._applied_job_applyby a{
    color: #333;
    font-weight: 600;   
}
._applied_job_applyby
{
  text-align: right;
}
._applied_job_appliedon{
    text-align: right;
}
._applied_job_action{
    text-align: right;
}
._applied_job_action a{
    height: 40px;
    line-height: 38px;
    border: 1px solid #000;
    text-transform: uppercase;
    text-align: center;
    display: inline-block;
    width: 100%;
    color: #333;
    margin-top: 20px;
    margin-bottom: 25px;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    -ms-transition: 0.3s ease-in-out;
    -o-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
._applied_job_action a:hover{
    color: #fa933c;
    border:1px solid #fa933c;
}
._applied_job_status{
    font-size:14px;
    text-align: right
}
._applied_job_status span.pendingj{color:#888;}
._applied_job_status span.holdj{color:#5bc0de;}
._applied_job_status span.acceptedj{color:#2dd625;}
._applied_job_status span.rejectedj{color:#f14646;}
._applied_job_loadmore{
    height: 40px;
    line-height: 38px;
    border: 1px solid #fa933c;
    text-transform: uppercase;
    text-align: center;
    display: inline-block;
    width: 100%;
    max-width: 200px;
    color: #fa933c !important;
    margin-top: 20px;
    margin-bottom: 25px;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    -ms-transition: 0.3s ease-in-out;
    -o-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
._suggested_job_list .exp-titlej {
    display: block;
    font-size: 14px;
    color: #000;
    text-transform: capitalize;
    font-weight: 600 !important;
    padding-bottom: 7px;
}
._suggested_job_list .exp-titlej a{
    color: #333;
}
._suggested_job_divider{
    width: 40px;
    height: 1px;
    background: #888;
    clear: both;
    float: left;
    margin:5px 0 10px;
}
._suggested_job_createdon{
    font-size: 12px;
    color: #888;
    clear: both;
}
._suggested_job_list ul li{
    padding-bottom: 15px;
    border-bottom: 1px solid #e1e1e1;
}
@media(max-width: 1024px){

._posted_job_cat_items_action a._posted_action_btn{
    margin-bottom: 10px;
}
}
@media(max-width: 767px){
    .padrgt0 {
        padding-right: 15px!important;
    }
    .padlft0 {
        padding-left: 15px!important;
    }
    ._applied_job_postedby {
        margin-bottom: 10px;
    }
    ._applied_job_appliedon,
    ._applied_job_status {
        text-align: center;
    }
    ._applied_job_action a{
        margin-top: 5px;
        margin-bottom: 5px;
    }
}
</style>
<div class="layout_page_user_job_index">
<!-- <div class="generic_layout_container layout_top">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_user_browse_menu">
<div class="headline">
  <h2>Applied Jobs  </h2>
  </div></div>

</div>
</div> -->

<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_user_browse_search">
<form method="post" action="<?php echo $this->baseUrl().'/job/search';?>" class="field_search_criteria">
<input type="hidden" name="type" value="search"  id="type"/>
<div class="browsemembers_criteria"><ul>
<li>
<select name="designation_id" id="designation_id">
    <?php 
        if($this->dasignations){
            echo '<option value="">Designation</option>';
            foreach($this->dasignations as $desig){
                echo '<option value="'.$desig['dasignatedsubcategory_id'].'">'.$desig['dasignatedsubcategory_name'].'</option>';
            }
        }
    ?>
    </select>
 </li>

 <li class="job_location">
<input type="text" name="location" value=""  id="location"  placeholder="Location"/>
<div id="autoSuggestionsListl" autocomplete="off"></div>
</li>

<li >
<select name="language_id" id="language_id">
        <?php 
            if($this->langoptions){
                echo '<option value="">Language</option>';
                foreach($this->langoptions as $lang){
                    echo '<option value="'.$lang['option_id'].'">'.$lang['label'].'</option>';
                }
            }
        ?>
        </select>
 </li>
 <li>
<input type="text" value="" id="keyword" name="keyword" placeholder="Keyword">
</li>
 <li class="search_button">
<input type="submit" name="search" value="Search"  id="search"/>
</li>

 <!-- <li class="job_advance_search" style="display:none;">
<input type="button" name="serach" value=""  id="ad_search"/>
</li> -->

<input type="hidden" name="list_count" id="list_count" value="20"/>
</ul>
</div>
</form>
</div>
<div class="generic_layout_container layout_middle">

<div class="generic_layout_container layout_core_content">

  <!--  <h2 class="heading" style="border-bottom:none; background-color:#fff;font-size:14px;"><a href="<?php echo $this->baseUrl().'/classifieds/manage';?>">My Jobs</a></h2>
   <h2 class="heading" style="border-bottom: 2px solid #fb933c; font-size:14px;"><a href="<?php echo $this->baseUrl().'/user/job';?>">My Job Applications</a></h2>
   <h2 class="heading" style="border-bottom:none; background-color:#fff;font-size:14px;"><a href="<?php echo $this->baseUrl().'/classified/index/parent';?>">My Projects</a></h2> -->
   
        <div class="container">
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 _job_applied">
        <?php $jobs = $this->jobs;
          if(count($jobs) > 0)
            { $i++;?>
            <div class="_job_applied_head">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">My Applied Jobs (<?php echo count($jobs);?>)</div> 
                </div>
            </div>
            <div class="_job_applied_list">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="applied_job_manage">
                     <?php   foreach($jobs as $job){
                        $data  = Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedDataById($job['classified_id']);
                        $applyby= Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedsapplyBy($job['classified_id']);
                    // echo '<pre>'; print_r($data);die();
                     foreach($data as $res){
                  
                    if($res['field_id'] == 61){
                      $desig=$res['values'];
                    }
                    if($res['field_id'] == 74){
                      $expe1=$res['values'];
                      if($expe1=='')
                      {
                        $expe="0";
                      }
                      else
                      {
                        $expe= $expe1;
                      }
                    }
                    if($res['field_id'] == 73){                    
                      $lang=$res['values'];
                    }
                
                    if($res['field_id'] == 75){
                      $keyw=$res['values'];
                    }

                    if($res['field_id'] == 41){
                      $dura=$res['values'];
                    }
                    
                    if($res['field_id'] == 71){
                      $pay_term=$res['values'];
                    }
                                 
                    if($res['field_id'] == 14){
                      $work_loc=$res['values'];
                    }

                    if($res['field_id'] == 72){
                      $out_app=$res['values'];
                      if($out_app=='Yes')
                      {
                        $out_app="may";
                      }
                      else
                      {
                        $out_app= "need not";
                      }
                    }
                          
                }

                      $ownername='';
                       $jobownerpic = '';
                        $classified = Engine_Api::_()->getItem('classified', $job['classified_id']);
                        //echo '<pre>'; print_r($classified); 
                        if($job['page_id'] != 0){
                            $companypage  = Engine_Api::_()->getItem('page', $job['page_id']);
                            //echo '<pre>'; print_r($companypage);
                            $ownername=$companypage['displayname'];
                            $jobownerpic =Engine_Api::_()->getDbtable('users', 'user')->getUserJobDesigPicPath($desig);
                            $link = $companypage->getHref();
                            $jobownerpic  = ($jobownerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$jobownerpic;
                            $isApply  = Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($job['classified_id'], $this->userid); 

                        }else{
                           $user = Engine_Api::_()->getItem('user', $job['owner_id']);
                           //echo '<pre>'; print_r($user); 
                           $ownername=$user['displayname'];
                           $link =  $user->getHref();
                           $jobownerpic =Engine_Api::_()->getDbtable('users', 'user')->getUserJobDesigPicPath($desig); 
                           $jobownerpic = ($jobownerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$jobownerpic;
                          $isApply  = Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($job['classified_id'], $this->userid);

                        }
                    //die();
                    $status = "Pending";
                    $class  =   "pendingj";
                    if($job->accept == 1 )
                    {
                        $status = "Selected";
                        $class  =   "acceptedj";
                    }
                    if($job->reject == 1)
                    {
                        $status = "Rejected";
                        $class  =   "rejectedj";
                    }
                    if($job->hold == 1)
                    {
                        $status = "Shortlisted";
                        $class= "holdj";
                    }    
      ?>
            
                            <li class="classified-sectionj">
                                <div class="edu_listingj">
                                    <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12 padlft0">
                                        <div class="title">
                                            <span class="exp-titlej">
                                                <a href="<?php echo $this->baseUrl().'/classifieds/'.$job->owner_id.'/'.$job->classified_id.'/'.urlencode($job->title);?>">
                        <?php echo $job->title;?></a>
                                            </span>
                                            <div class="_job_details">
                                            <span class="_job_exp"><i class="fa fa-briefcase" aria-hidden="true"></i> <?php echo $expe ?></span>
                                            <span class="_job_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $work_loc ?></span>
                                          </div>
                                        </div>  

                                        <div class="activitiesj _applied_job_desc">
                                            <label>Description :</label><span><b><?php echo $desig ?></b> wanted, with <b><?php echo $expe ?></b> of experience, fluent in <b><?php echo $lang ?></b> for a <b><?php echo $dura ?></b> assignment. Payment terms will be on a <b><?php echo $pay_term ?></b> basis. This position is primarily based in <b><?php echo $work_loc ?></b>.  Outstation candidates <b><?php echo $out_app ?></b> apply.</p></span>
                                        </div>
                                        <div class="activitiesj _applied_job_postedby">
                                        <label>Posted By </label> <a class="wp_init"><?php echo $ownername; ?></a>, <span title="<?php echo date('d F Y', strtotime($job->creation_date));?>" class="timestamp"><?php echo date('d F Y', strtotime($job->creation_date));?></span>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 padrgt0">
                                        <div class="activitiesj _applied_job_appliedon">
                                            <label>Applied on :</label> <span><?php echo date('d F Y', strtotime($job->applied_date));?></span>
                                        </div>
                                        <div class="activitiesj _applied_job_action">
                                            <a id="jobunapply" class="smoothbox classified_applyj wp_init" href="<?php echo $this->baseUrl();?>/classifieds/application/applyremove/classified_id/<?php echo $job->classified_id;?>">Delete Application</a>
                                        </div>
                                        <div class="activitiesj _applied_job_status">
                                            <label>Status :&nbsp;</label><span class="<?php echo $class;?>"><?php echo $status;?></span>
                                        </div>
                                        <?php if($applyby){?>
                                        <div class="activitiesj _applied_job_applyby">
                                        <label>Apply By: </label> <a class="wp_init"><?php echo date('d F Y',strtotime($applyby))  ?></a></div>
                                        <?php }?>
                                    </div>
                                </div>
                            </li>
                         <?php $i++; }?>
                            <!-- <div class="text-center">
                                <a href="javascript:void(0);" title="Load More" class="_applied_job_loadmore">Load More</a>
                            </div> -->
                        </ul>

                    </div>
                </div>
            </div>
           <?php } else{ ?>
            <div class='tip'>
                    <span id='no-experience'>You haven't applied for any Job. </span>
                 </div>
        <?php }?>      
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 _suggested_job">
            <div class="_job_applied_head">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Suggested Jobs</div>   
                </div>
            </div>
            <?php $suggestjobs = $this->suggestjobs;
          if(count($suggestjobs) > 0)
            { $i++;?>
            <div class="_suggested_job_list">
                <div class="row">
                    <div class="col-xs-12">
                        <ul>
                           <?php   foreach($suggestjobs as $suggestjob)
                        {
                        $data  = Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedDataById($suggestjob['classified_id']);
                        $applyby= Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedsapplyBy($suggestjob['classified_id']);
                     foreach($data as $res){
                  
                    if($res['field_id'] == 61){
                      $desig=$res['values'];
                    }
                    if($res['field_id'] == 74){
                      $expe1=$res['values'];
                      if($expe1=='')
                      {
                        $expe="0";
                      }
                      else
                      {
                        $expe= $expe1;
                      }
                    }
                    if($res['field_id'] == 73){                    
                      $lang=$res['values'];
                    }
                
                    if($res['field_id'] == 75){
                      $keyw=$res['values'];
                    }

                    if($res['field_id'] == 41){
                      $dura=$res['values'];
                    }
                    
                    if($res['field_id'] == 71){
                      $pay_term=$res['values'];
                    }
                                 
                    if($res['field_id'] == 14){
                      $work_loc=$res['values'];
                    }

                    if($res['field_id'] == 72){
                      $out_app=$res['values'];
                          if($out_app=='Yes')
                          {
                            $out_app="may";
                          }
                          else
                          {
                            $out_app= "need not";
                          }
                    }
                          
                }

                      $ownername='';
                       $jobownerpic = '';
                        $classified = Engine_Api::_()->getItem('classified', $suggestjob['classified_id']);
                        //echo '<pre>'; print_r($classified); 
                        if($suggestjob['page_id'] != 0){
                            $companypage  = Engine_Api::_()->getItem('page', $suggestjob['page_id']);
                            //echo '<pre>'; print_r($companypage);
                            $ownername=$companypage['displayname'];
                            $jobownerpic =Engine_Api::_()->getDbtable('users', 'user')->getUserJobDesigPicPath($desig);
                            $link = $companypage->getHref();
                            $jobownerpic  = ($jobownerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$jobownerpic;
                            $isApply  = Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($suggestjob['classified_id'], $this->userid); 

                        }else{
                           $user = Engine_Api::_()->getItem('user', $suggestjob['owner_id']);
                           //echo '<pre>'; print_r($user); 
                           $ownername=$user['displayname'];
                           $link =  $user->getHref();
                           $jobownerpic =Engine_Api::_()->getDbtable('users', 'user')->getUserJobDesigPicPath($desig); 
                           $jobownerpic = ($jobownerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$jobownerpic;
                          $isApply  = Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($suggestjob['classified_id'], $this->userid);

                        }
                    //die();
                      
      ?>
       <li>
                                <div class="title">
                                    <span class="exp-titlej">
                                        <a href="<?php echo $this->baseUrl().'/classifieds/'.$suggestjob->owner_id.'/'.$suggestjob->classified_id.'/'.urlencode($suggestjob->title);?>">
                        <?php echo $suggestjob->title;?></a>
                                    </span>
                                    <div class="_job_details">
                                    <span class="_job_exp"><i class="fa fa-briefcase" aria-hidden="true"></i> <?php echo $expe ?></span>
                                    <span class="_job_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $work_loc ?></span>
                                  </div>
                                </div>
                                <span class="_suggested_job_divider"> </span>
                                <div class="_suggested_job_createdon">Created On: <?php echo date('d F Y', strtotime($suggestjob->creation_date));?></div>
                                <?php if($applyby){?>
                                <div class="_suggested_job_createdon">Apply By: <?php echo date('d F Y', strtotime($applyby));?></div>
                                <?php } ?>
                            </li>
                       <?php $i++; } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <div class='tip'>
                    <span id='no-experience'>No suggested job found! </span>
                 </div>
        <?php }?>    
        </div>      
    </div>
     
</div>
</div>
</div>
</div>

<script type="text/javascript">
    jQuery(document).on('keyup','#location', function(e) {
         var inputString = jQuery(this).val();
        if(inputString.length < 1) {
                jQuery('#autoSuggestionsListl').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/user/edit/getlocations";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                      if(data)
                        {
                            jQuery('#autoSuggestionsListl').show();
                            jQuery('#autoSuggestionsListl').addClass('auto_listl');
                            jQuery('#autoSuggestionsListl').html(data);
                            jQuery("#autoSuggestionsListl").attr('style','height: auto; overflow-y: auto; max-height: 400px; width: inherit;');
                        }
                        else
                        {
                            jQuery('#autoSuggestionsListl').hide();
                        }
                     });
             }
    });
      jQuery(document).on('click','#autoSuggestionsListl li',function(){ 
            var el=jQuery(this).text();
           jQuery('#location').val(el);
        setTimeout("jQuery('#autoSuggestionsListl').hide();", 200);
       });
      jQuery("#serach").click(function(){
        jQuery(this).form.submit();
      });
</script>