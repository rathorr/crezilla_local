<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: general.tpl 9874 2013-02-13 00:48:05Z shaun $
 * @author     Steve
 */
?>

<div class="global_form">
  <?php if ($this->form->saveSuccessful): ?>
    <h3><?php echo $this->translate('Settings were successfully saved.');?></h3>
  <?php endif; ?>
  <?php echo $this->form->render($this) ?>

<style>
select {
    width:100%;
    height:auto;
}
.global_form div.form-label {
  color:#777777;
  font-size:0.9em;
  font-weight:700;
  text-align:left;
}
#facebook-wrapper, #twitter-wrapper {
  clear:none;
  float:left;
  width:100%;
}

.global_form > div > div {
  width:100%;
}

.global_form #facebook-element {
  width:100%;
}
.global_form div > p {
  max-width:100%;
}
</style>

</div>

<?php if( Zend_Controller_Front::getInstance()->getRequest()->getParam('format') == 'html' ): ?>
  <script type="text/javascript">
    en4.core.runonce.add(function(){
      var req = new Form.Request($$('.global_form')[0], $('global_content'), {
        requestOptions : {
          url : '<?php echo $this->url(array()) ?>'
        },
        extraData : {
          format : 'html'
        }
      });
    });
  </script>
<?php endif; ?>