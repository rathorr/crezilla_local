<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: password.tpl 9869 2013-02-12 22:37:42Z shaun $
 * @author     Steve
 */
?>

<?php echo $this->form->render($this) ?>

<style>

.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
</style>