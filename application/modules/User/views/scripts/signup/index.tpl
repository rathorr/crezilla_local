<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>


<style>
#global_header
{
	display: block !important;
}
.layout_core_menu_mini #core_menu_mini_menu>ul>li>a {
display: none !important;
	}
.form-control.com_form_error_validatin::-webkit-input-placeholder { color:#00B1CB !important; }
.form-control.com_form_error_validatin::-moz-placeholder { color:#00B1CB !important; }

#global_content{width:100% !important;}

.SumoSelect > .CaptionCont{ width:100% !important;}
body{background-color:#FFF !important;}
#middlename
{
	padding: 2%;
    border-radius: 3px;
    border: 0px;
    width: 100%;
    margin-bottom: 10px;
    color: #555;
    font-size: 13px;
    font-family: robotoregular;
    height: 40px;
}
#last_name
{
	padding: 2%;
    border-radius: 0px;
    border: 1px solid #ccc;
    width: 100%;
    margin-bottom: 10px;
    color: #555;
    font-size: 13px;
    font-family: robotoregular;
    height: 40px;
    background-color: #fff !important;
}
#signup_email
{
	padding: 2%;
    border-radius: 0px;
    border: 1px solid #ccc;
    width: 100%;
    margin-bottom: 10px;
    color: #555;
    font-size: 13px;
    font-family: robotoregular;
    height: 40px;
    background-color: #fff !important;
}
.signup_container form#signupform .form-elements #submit_login-wrapper {
margin-right: 38px;
	}
#bmonth-wrapper #bmonth-label label.optional {
    margin-left: -135px;
}
.signup_container form#signupform .form-elements #bmonth-element select#bmonth {
    margin-top: 7px;
}
#signupform > div > div > p > a.asign
{
	color: #1bcde7 !important;
	font-weight: 500 !important;

}
ul.form-options-wrapper li 
	{
		min-width: 17% !important;
	}
#bdate-label
{
	display: block !important;
}
#bdate-wrapper #bdate-label label.optional {	
    font-weight: 500 !important;
    font-size: 15px !important;
    color: #666666 !important;
    float: left;
}
#global_header > div.layout_page_header > div > div.generic_layout_container.layout_core_menu_mini > div.device_on
{
	display: none;
}
.logout .layout_core_mobi_menu_mini #core_menu_mini_menu_mobile {
    display: none;
}

input[type=radio],
input[type=checkbox]{
  /* Hide original inputs */
  visibility: hidden;
  position: absolute;
}
input[type=radio] + label:before,
input[type=checkbox] + label:before{
	-webkit-appearance: none;
  height:12px;
  width:12px;
  margin-right: 10px;
  content: " ";
  display:inline-block;
  vertical-align: baseline;
  border:1px solid #777;
}
input[type=radio]:checked + label:before,
input[type=checkbox]:checked + label:before{
  background:black;
}

/* CUSTOM RADIO AND CHECKBOX STYLES */
input[type=radio] + label:before{
  border-radius:50%;
}
input[type=checkbox] + label:before{
  border-radius:2px;
}

</style>
<?php echo $this->headLink()->appendStylesheet($this->baseUrl().'/public/custom/home-page.css') ?>
<?php //echo $this->headScript()->appendFile($this->baseUrl().'/public/js/jquery.js')?>
<script src="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.js"></script>
<link href="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.css" rel="stylesheet" />
<script type="text/javascript">

jQuery(document).on('keypress', '#name, .signupemail, #password', function(e){
	 if(e.which == 13 || e.which == 1 ) {
      validatesignup();
   }
});

jQuery(document).ready(function(){
	jQuery('#skill_id').SumoSelect({selectAll: false, placeholder: 'Select Skill'});
	jQuery('.menu_core_mini').removeClass('active');	
	jQuery('.core_mini_signup').addClass('active');

});

//var $ = jQuery.noConflict();
  function skipForm() {
    document.getElementById("skip").value = "skipForm";
    $('SignupForm').submit();
  }
  function finishForm() {
    document.getElementById("nextStep").value = "finish";
  }
  function validatesignup(){

  	if(jQuery.trim(jQuery("#first_name").val()) == ''){
			jQuery("#first_name").focus();
			jQuery("#first_name").addClass("com_form_error_validatin");
			return false;
	}
	if(jQuery.trim(jQuery("#last_name").val()) == ''){
			jQuery("#last_name").focus();
			jQuery("#last_name").addClass("com_form_error_validatin");
			return false;
	}
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery(".signupemail").val()) == ''){
			//$("#signinerror").html("Email is required.");
			//$("#signinerror").show();
			jQuery(".signupemail").focus();
			jQuery(".signupemail").addClass("com_form_error_validatin");
			//$("#signinerror").fadeOut(7000);
			return false;
	}
	if(regex.test(jQuery.trim(jQuery(".signupemail").val())) == false){
 		jQuery(".signupemail").focus();
		jQuery(".signupemail").val('');
		jQuery(".signupemail").addClass("com_form_error_validatin");
		//$('#email').focus();
		//$('#email').addClass('error');
		return false;
  	 }
	 var is_fb_login	=	"<?php echo empty($_SESSION['facebook_signup'])?0:1;?>";
	 var is_twt_login	=	"<?php echo empty($_SESSION['twitter_signup'])?0:1;?>";
	if(is_fb_login ==0 && is_fb_login ==0){
		if(jQuery.trim(jQuery("#password").val()) == ''){
				//$("#signinerror").html("Password is required.");
				//$("#signinerror").show();
				jQuery("#password").focus();
				jQuery("#password").addClass("com_form_error_validatin");
				//$("#signinerror").fadeOut(7000);
				return false;
		}
		
	}

	if(jQuery.trim(jQuery("#bdate").val()) == ''){
			jQuery("#bdate").focus();
			jQuery("#bdate").addClass("com_form_error_validatin");
			return false;
	}
	if(jQuery.trim(jQuery("#bmonth").val()) == ''){
			jQuery("#bmonth").focus();
			jQuery("#bmonth").addClass("com_form_error_validatin");
			return false;
	}
	
	if(jQuery.trim(jQuery("#byear").val()) == ''){
			jQuery("#byear").focus();
			jQuery("#byear").addClass("com_form_error_validatin");
			return false;
	}
	
		var gender = jQuery('input:radio[name^="gender"]:checked').val();
			if(gender == undefined)

			{
				jQuery('input:radio[name^="gender"]').focus();
				jQuery('input:radio[name^="gender"]').parent().addClass("com_form_error_validatin");
				return false;

			}
	
	
	if(jQuery('#terms').is(":checked") == false){
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("You must agree to the terms of service to continue.");
			jQuery("#signinerror").fadeOut(7000);
			jQuery("#terms").focus();
			return false;
	}
	
	jQuery.post("<?php echo $this->baseUrl();?>/members/signup/chkemail",{email:jQuery.trim(jQuery(".signupemail").val()) }, function(resp){
		var data=JSON.parse(resp);
		//alert(data.status);
		if(data.status==0){	
		//return false;	
			jQuery('#signupform').submit();
		}else{
			
			jQuery("#signinerror").show();
			jQuery("#signinerror").html(data.msg);
			jQuery("#signinerror").fadeOut(7000);
			jQuery(".signupemail").focus();
			return false;
		}
			jQuery('#continue').prop('disabled', true);
			jQuery('#continue').css('background-color', '#999');
			jQuery('#continue').text('Processing....');
		//$('#signupform').submit();
	});
	
	  
  }
</script>


<!-- slider section start -->

<div class="main-head-home">

<!-- <img src="<?php echo $this->baseUrl().'/public/custom/images/ipro-img.jpg'?>" class="main-index-img" alt="" /> -->

<!-- <div class="home-header">
<a href="<?php echo $this->baseUrl();?>">
<img src="<?php echo $this->baseUrl().'/public/admin/iproducer-logo-index.png'?>" /> </a>




<nav class="navbar navbar-inverse nologinmenu">
  <div class="">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
     
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
    
      <ul class="nav navbar-nav">
     
        <li><a href="login" class="hl-menu">Sign In</a></li>
      </ul>
      

      
    </div> 
  </div>
</nav>
  
 
</div> -->

<div id="jms-slideshow" class="jms-slideshow">
				<div class="step " style="width: 100%; display: block; text-align: center; height: 350px; vertical-align: middle; line-height: 300px;">
					
					<div class="signup_container">
					
					<div class="col-md-12 col-sm-12  col-xs-12 custom_log_sec custom_singup_frm"><?php echo $this->partial($this->script[0], $this->script[1], array(
                      'form' => $this->form
                    )) ?> </div>
					</div>
					
                  <div id="signinerror" style="display: none;"></div>
				  
                    
				</div>
				
			
			</div>
		
			<div class="clear"></div>
				<!--<span class="slider_half_circle"><i class="fa fa-caret-down"></i>
</span>-->
<!--</div><!-- slider section end -->

</div>

<!-- lower section start -->


    <style>
	.hide_box{display:none;}
	</style>
<!-- lower section end -->



