<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: verify.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */
?>
<style type="text/css">
  #global_content > div > div.generic_layout_container.layout_main > div > div > div > span
  {
    margin-left: 2%;
  }
</style>
<div class="layout_page_user-signup_verify">
<div class="generic_layout_container layout_top">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_user_browse_menu">
  <div class="headline">
  <h2>
  <?php echo $this->translate("Thanks for verifying!") ?>
</h2>
  </div>
</div>
</div>
</div>

<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">

<?php if( $this->status ): ?>

  <script type="text/javascript">
    setTimeout(function() {
      parent.window.location.href = '<?php echo $this->url(array(), 'user_login', true); ?>';
    }, 5000);
  </script>

  <?php echo $this->translate("Your account has been verified. Please wait to be redirected or click %s to login.",
      $this->htmlLink(array('route'=>'user_login'), $this->translate("here"))) ?>

<?php else: ?>

  <div class="error">
    <span>
      <?php echo $this->translate($this->error) ?>
    </span>
  </div>

<?php endif;?>
</div>
</div>
</div>

</div>