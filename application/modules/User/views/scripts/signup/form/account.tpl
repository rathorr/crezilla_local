<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: account.tpl 10143 2014-03-26 16:18:25Z andres $
 * @author     John
 */
?>

<style>
/*#name-wrapper {
  display: none;
}*/
</style>

<script type="text/javascript">
//<![CDATA[
  window.addEvent('load', function() {
    if( $('#username') && $('#profile_address') ) {
		var html	=	$('#profile_address').html();
		var data 	=	'';
		if(html){
		var data = html.replace('<?php echo /*$this->translate(*/'yourname'/*)*/?>','<span id="profile_address_text"><?php echo $this->translate('yourname') ?></span>')
		}
     // var data = $('#profile_address').html();
		$('profile_address').html(data);
      $('#username').on('keyup', function() {
        var text = '<?php echo $this->translate('yourname') ?>';
        if( this.value != '' ) {
          text = this.value;
        }
        
        //$('profile_address_text').innerHTML = text.replace(/[^a-z0-9]/gi,'');
		$('#profile_address_text').html( text.replace(/[^a-z0-9]/gi,''));
      });
      // trigger on page-load
      if( $('#username').length ) {
          $('#username').fireEvent('keyup');
      }
    }
  });
//]]>
</script>

<?php echo $this->form->render($this) ?>
