<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: reset.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<?php if( empty($this->reset) ): ?>

  <?php echo $this->form->render($this) ?>
   <div id="signinerror" style="position: relative ! important; margin-top: 54px; left: 29px; display:none;"></div>
<?php else: ?>

  <div class="tip">
    <span>
      <?php echo $this->translate("Your password has been reset. Click %s to sign-in.", $this->htmlLink(array('route' => 'user_login'), $this->translate('here'))) ?>
    </span>
  </div>

<?php endif; ?>
<script type="text/javascript">
jQuery("#password_confirm, #password").keypress(function(e) {
   if(e.which == 13 || e.which == 1 ) {
      validatereset();
   }
});

//var $ = jQuery.noConflict();
  function validatereset(){
	 
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#password").val()) == ''){
			jQuery("#password").focus();
			jQuery("#password").addClass("com_form_error_validatin");
			//$("#signinerror").fadeOut(7000);
			return false;
	}
	
	if(jQuery.trim(jQuery("#password_confirm").val()) == ''){
			jQuery("#password_confirm").focus();
			jQuery("#password_confirm").addClass("com_form_error_validatin");
			return false;
	}
	
	if(jQuery.trim(jQuery("#password_confirm").val()) != jQuery.trim(jQuery("#password").val())){
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Password does not match.");
			jQuery("#password_confirm").focus();
			jQuery("#password_confirm").addClass("com_form_error_validatin");
			jQuery("#signinerror").fadeOut(7000);
			return false;
	}
	jQuery('#user_form_auth_reset').submit();
	return false;
	  
  }
</script>
