<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: reset.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?><style type="text/css">
body{background-color:#FFF !important;}


#global_content{width:100% !important;}
.jms-slideshow{ margin:0 !important;}
.success_msg{  background-color: #3f7500; border-radius: 3px;bottom: 45px;color: #fff;font-family: robotoregular;font-size: 15px;height: 34px;left: 404px;line-height: 13px;padding: 10px;position: absolute;text-align: center;vertical-align: top;width: 34%}
</style>


<!-- slider section start -->
<div class="home-box hero_bg">
  <section id="jms-slideshow" class="jms-slideshow">
				<div style="width: 100%; display: block; text-align: center; height: 400px; vertical-align: middle; line-height: 300px;">
				<?php if( !empty($this->reset) ): ?>
						<span class="success_msg"> <?php echo $this->translate("Your password has been reset. Click %s to sign-in.", $this->htmlLink(array('route' => 'user_login'), $this->translate('here'))) ?>
						</span>
						<?php endif; ?>
				<div class="forgot_container">
				
					<div class="col-md-12 custom_log_sec" style="padding: 117px 0px; position:static !important;">
                    <div style="position: relative !important; width: 70% !important; display: none; bottom: 0px !important; left: 14px !important;" id="signinerror"></div>
                        <?php echo $this->form->render($this) ?>
					</div>
				</div>
				
				
   <!--  <div id="signinerror" style="position: relative ! important; margin-top: 54px; left: 29px; display:none;"></div>-->
     
       
    </div>
				
			
			</section>
		
			<div class="clear"></div>
				<!--<span class="slider_half_circle"><i class="fa fa-caret-down"></i>
</span>-->
</div><!-- slider section end -->



<!-- lower section start -->

<div class="lower_container" style="display:none;">
	
	<section id="discover">
	
	<div class="discover_cont">
	
	<div class="col-md-12 text-center" style="margin:0px 0px 20px;">
			<h1 class="discover_heading">Discover</h1>
			<span class="discover_title_footer"></span>
			
	</div>
	
		<div class="col md-12 text-center" style="clear:both; margin-top:15px;">
		<h2 class="sub_heading">Lorem Title</h2>
		<p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
		
		</div>
		
        
        
        
          <div class="circle-box-main">
        <ul>
        
        <li>
		<div class="ih-item circle effect15 left_to_right"><a href="#">
        <div class="img" style="width:88px; height:88px;"><img src="<?php echo $this->baseUrl().'/public/custom/images/photographer.png'  ?>">
		</div>
        <div class="info" style="width:88px; height:88px;">
          <h3>PHOTOGRAPHER</h3>
         
        </div></a></div>
	</li>
		
        
            <li>
		<div class="ih-item circle effect15 left_to_right"><a href="#">
        <div class="img" style="width:88px; height:88px;"><img src="<?php echo $this->baseUrl().'/public/custom/images/singer.png'  ?>">
		</div>
        <div class="info" style="width:88px; height:88px;">
          <h3>SINGER</h3>
         
        </div></a></div>
	</li>
		
        
            <li>
		<div class="ih-item circle effect15 left_to_right"><a href="#">
        <div class="img" style="width:88px; height:88px;"><img src="<?php echo $this->baseUrl().'/public/custom/images/dancer.png'  ?>">
		</div>
        <div class="info" style="width:88px; height:88px;">
          <h3>DANCER</h3>
         
        </div></a></div>
		</li>
		
        
            <li>
		<div class="ih-item circle effect15 left_to_right"><a href="#">
        <div class="img" style="width:88px; height:88px;"><img src="<?php echo $this->baseUrl().'/public/custom/images/writer.png'  ?>">
		</div>
        <div class="info" style="width:88px; height:88px;">
          <h3>WRITER</h3>
         
        </div></a></div>
		</li>
		
            <li>
        
        <div class="ih-item circle effect15 left_to_right"><a href="#">
        <div class="img" style="width:88px; height:88px;"><img src="<?php echo $this->baseUrl().'/public/custom/images/crew.png'  ?>">
		</div>
        <div class="info" style="width:88px; height:88px;">
          <h3>CREW</h3>
         
        </div></a></div>
		</li>
        
		    
            <li>
		<div class="ih-item circle effect15 left_to_right"><a href="#">
        <div class="img" style="width:88px; height:88px;"><img src="<?php echo $this->baseUrl().'/public/custom/images/editor.png'  ?>">
		</div>
        <div class="info" style="width:88px; height:88px;">
          <h3>EDITOR</h3>
         
        </div></a></div>
	</li>
    
		
        
        
        </ul>
        </div>
	
	</div><!-- Discover container end -->
	
	</section>
	<div class="clear"></div>
	
	<section id="network">
	
	<div class="network_cont">
	<div class="col-md-12 text-center" style="margin:0px 0px 20px;">
			<h1 class="heading">Network</h1>
			<span class="title_footer"></span>
			
	</div>
	
		<div class="col md-12 text-center" style="clear:both; margin-top:15px;">
		<h2 class="sub_heading">Lorem Title</h2>
		<p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
		
		</div>
		
		<div class="col-md-12 text-center spacer-top-bottom-30"><img src="<?php echo $this->baseUrl().'/public/custom/images/network-logos.jpg'  ?>"></div>
	</div><!-- network container end -->
	
	</section>
	<div class="clear"></div>

</div>

<!-- lower section end -->

<?php echo $this->headScript()->appendFile($this->baseUrl().'/public/js/jquery.min.js')?>
<script type="text/javascript">
jQuery("#password_confirm, #password").keypress(function(e) {
   if(e.which == 13 || e.which == 1 ) {
      validatereset();
   }
});

//var $ = jQuery.noConflict();
  function validatereset(){
	 
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#password").val()) == ''){
			jQuery("#password").focus();
			jQuery("#password").addClass("com_form_error_validatin");
			//$("#signinerror").fadeOut(7000);
			return false;
	}
	
	if(jQuery.trim(jQuery("#password_confirm").val()) == ''){
			jQuery("#password_confirm").focus();
			jQuery("#password_confirm").addClass("com_form_error_validatin");
			return false;
	}
	
	if(jQuery.trim(jQuery("#password_confirm").val()) != jQuery.trim(jQuery("#password").val())){
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Password does not match.");
			jQuery("#password_confirm").focus();
			jQuery("#password_confirm").addClass("com_form_error_validatin");
			jQuery("#signinerror").fadeOut(7000);
			return false;
	}
	jQuery('#user_form_auth_reset').submit();
	return false;
	  
  }
</script>



