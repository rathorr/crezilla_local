<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: login.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style type="text/css">
body{background-color:#FFF !important;}
.header_menu_class {display:none !important;}
/*#global_header
{
	background-color: #000;
}
.login_container form#user_form_login h3
{
	color: #000;
	text-align: left;
    margin-left: 23%;
    font-weight: 400;
}
.login_container form#user_form_login #buttons-wrapper fieldset#fieldset-buttons #remember-element label 
{
	color: #000;

}
.login_container form#user_form_login .form-description
{
	color: #000;
	text-align: left;
    margin-left: 20%;
    font-weight: 500;
}
form#user_form_login {
margin-left: 50%;
	}
.login_container form#user_form_login .form-element input.loginemail {	
	width: 78%;
	border: 1px solid #8e8e8e;
	background-color: #fff !important;
	border-radius: 0px;
}
.login_container form#user_form_login .form-elements #password-element input#password {
	border: 1px solid #8e8e8e;
	    background-color: #fff !important;
	    border-radius: 0px;
	}
.login_container form#user_form_login .form-elements #submit_login-wrapper {
    width: 78%;
    border-radius: 0px;
}
.login_container form#user_form_login #forgot-wrapper {
   
    margin-right: 22%;
}*/
#global_content{width:100% !important;}
.jms-slideshow{ margin:0 !important;}
</style>
 
<script src="https://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<?php echo $this->headLink()->appendStylesheet($this->baseUrl().'/public/js/demo.css') ?>


<?php //echo $this->headScript()->appendFile($this->baseUrl().'/application/modules/Photoviewer/externals/scripts/jquery-1.9.0.min.js'); ?>

<?php //echo $this->headScript()->appendFile($this->baseUrl().'/public/js/jmpress.js'); ?>
<?php //echo $this->headScript()->appendFile($this->baseUrl().'/public/js/jquery.jmslideshow.js'); ?>
<?php  //echo $this->headScript()->appendFile($this->baseUrl().'/public/js/jquery.bxslider.min.js'); ?>

<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300,100' rel='stylesheet' type='text/css'>

  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  
<!-- slider section start -->
<div class="home-box ">
  <div id="jms-slideshow" class="jms-slideshow">
				<div class="step">
				
				<div class="login_container">
					<div class="col-md-12 col-sm-12 col-xs-12 login-form-section custom_log_sec">	
						
						<div class="col-md-7 col-sm-12 col-xs-12 ">
							<?php echo $this->form->render($this) ?> 
						</div>
						
					</div>
				<div>
				
				   <div id="signinerror" style="display:none;"></div>
				   
				   
			
			</div>
		
			<div class="clear"></div>
				<!--<span class="slider_half_circle"><i class="fa fa-caret-down"></i>
</span>-->
</div><!-- slider section end -->

<script type="text/javascript">
jQuery(".loginemail, #password").keypress(function(e) {
   if(e.which == 13 || e.which == 1 ) {
      validatesignin();
   }
});

jQuery(document).ready(function(){
	jQuery('.menu_core_mini').removeClass('active');	
	jQuery('.core_mini_auth').addClass('active');
});
//var $ = jQuery.noConflict();
  function validatesignin(){
	 
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery(".loginemail").val()) == ''){
			//$("#signinerror").html("Email is required.");
			//$("#signinerror").show();
			jQuery(".loginemail").focus();
			jQuery(".loginemail").addClass("com_form_error_validatin");
			//$("#signinerror").fadeOut(7000);
			return false;
	}
	if(regex.test(jQuery.trim(jQuery(".loginemail").val())) == false){
 		jQuery(".loginemail").focus();
		jQuery(".loginemail").val('');
		jQuery(".loginemail").addClass("com_form_error_validatin");
		//$('#email').focus();
		//$('#email').addClass('error');
		return false;
  	 }
	if(jQuery.trim(jQuery("#password").val()) == ''){
			//$("#signinerror").html("Password is required.");
			//$("#signinerror").show();
			jQuery("#password").focus();
			jQuery("#password").addClass("com_form_error_validatin");
			//$("#signinerror").fadeOut(7000);
			return false;
	}
	
	jQuery.post("<?php echo $this->baseUrl();?>/members/signup/chkemail",{email:jQuery.trim(jQuery(".loginemail").val()), password:jQuery.trim(jQuery('#password').val())}, function(resp){
		var data=JSON.parse(resp);
		//alert(data.status);
		if(data.status==0){	
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Invalid email or password.");
			jQuery("#signinerror").fadeOut(7000);
			return false;
		}else{
			jQuery('#user_form_login').submit();
		}
		//$('#signupform').submit();
	});
	  
  }
</script>
<script type="text/javascript">
			jQuery(function() {			
				jQuery('#submit_email,#submit_code').on("click",function(){
				
					var type=jQuery(this).attr('id');
					
					if(type=='submit_email'){					
						var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
						jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
						if(jQuery.trim(jQuery("#email").val()) == ''){		
							jQuery("#email").focus();
							jQuery("#email").addClass("com_form_error_validatin");
							return false;
						}
						if(regex.test(jQuery.trim(jQuery("#email").val())) == false){
							jQuery("#email").focus();
							jQuery("#email").val('');
							jQuery("#email").addClass("com_form_error_validatin");				
							return false;
						 }													
						var data={email:jQuery.trim(jQuery("#email").val())};						
					}
					
					if(type=='submit_code'){
					
						if(jQuery.trim(jQuery("#code").val()) == ''){		
							jQuery("#code").focus();
							jQuery("#code").addClass("com_form_error_validatin");
							return false;
						}
						var data={code:jQuery.trim(jQuery("#code").val())};						
					}					
					
					jQuery.post("<?php echo $this->baseUrl();?>/members/signup/chkcode",data, function(resp){
						//alert(resp);
						var data=JSON.parse(resp);	
						
						if(data.status==1){	
							alert(data.msg);
							return false;
						}else{
						
							if(type=='submit_code'){
								window.location.href = "<?php echo $this->baseUrl();?>/members/signup?code="+jQuery.trim(jQuery("#code").val());
							}else{
								alert('Thank you! We will notify you when CREZILLA goes live.');
								$('#email').val('');
								$('#myModal').modal('hide');
							}
								
							return false;
						}
						
					});
				
				});
			
								
				// var jmpressOpts	= {
					// animation		: { transitionDuration : '1s' }
				// };
				
				// jQuery( '#jms-slideshow' ).jmslideshow( jQuery.extend( true, { jmpressOpts : jmpressOpts }, {
					// autoplay	: true,
					// arrows		: false,
					// interval	: 5000,
					// dots		: false,
				// }));
				
				
				
				 // jQuery('.testimonials-slider').bxSlider({
					// slideWidth: 300,
					// minSlides: 1,
					// maxSlides: 1,
					// slideMargin: 32,
					// auto: true,
					// autoControls: true
			    // });
				
			});
		</script>