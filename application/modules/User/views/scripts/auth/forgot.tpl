<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: forgot.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style>

body{background-color:#FFF !important;}

#global_content{width:100% !important;}
.jms-slideshow{ margin:0 !important;}
.success_msg{ background-color:#3f7500; width: 50%;line-height: 18px;height: 52px;left: 371px;border-radius: 3px;bottom: 45px;color: #fff;font-family: robotoregular;font-size: 15px;padding: 10px;position: absolute;text-align: center;vertical-align: top;}
#core_menu_mini_menu > ul.header_menu_class,
#global_header > div.layout_page_header > div > div.generic_layout_container.layout_core_menu_mini > div.device_on,
#core_menu_mini_menu_mobile
{
	display: none;
}
#user_form_auth_forgot
{
	-webkit-box-shadow:none ;
	box-shadow:none;
	-webkit-transition:none;
	-o-transition:none;
	
}

</style>


<!-- slider section start -->
<div class="home-box hero_bg static_bg">
  <section id="jms-slideshow" class="jms-slideshow">
				<div style="width: 100%; display: block; text-align: center; height: 400px; vertical-align: middle; line-height: 300px;">
				<?php if( !empty($this->sent) ): ?>
						<span class="success_msg">You have been sent an email with instructions how to reset your password. If the email does not arrive within several minutes, be sure to check your spam or junk mail folders.
						</span>
						<?php endif; ?>
				<div class="forgot_container">
				
					<div class="custom_log_sec" >
                    	<div id="signinerror" style="display: none; position: relative !important; width: 70% !important; bottom: 0px !important; margin-left: -243px !important;"></div>
                        <?php echo $this->form->render($this) ?>
					</div>
				</div>
				
				
     <!--<div id="signinerror" style="display:none; margin-left:156px;"></div>-->
       
    </div>
				
			
			</section>
		
			<div class="clear"></div>
				<!--<span class="slider_half_circle"><i class="fa fa-caret-down"></i>
</span>-->
</div><!-- slider section end -->



<!-- lower section start -->

<!-- lower section end -->

<?php echo $this->headScript()->appendFile($this->baseUrl().'/public/js/jquery.min.js')?>
<script type="text/javascript">
$(document).ready(function(){
	$(".forgotemail").val("");	
	$('.forgotemail').keypress(function() {
		$(this).removeClass("com_form_error_validatin");
	});
});
$(document).on('keypress', '.forgotemail', function(e){
	 if(e.which == 13 || e.which == 1 ) {
      validateforgot();
	  return false;
   }
});

function validateforgot(){
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	$(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if($.trim($(".forgotemail").val()) == ''){
		$(".forgotemail").focus();
		$(".forgotemail").addClass("com_form_error_validatin");
		return false;
	}
	if(regex.test($.trim($(".forgotemail").val())) == false){
 		$(".forgotemail").focus();
		$(".forgotemail").val('');
		$(".forgotemail").addClass("com_form_error_validatin");
		return false;
  	 }
	 $.post("<?php echo $this->baseUrl();?>/members/signup/chkemail",{email:$.trim($("#email").val())}, function(resp){
		if(!$.trim(resp)){
			$("#signinerror").show();
			$("#signinerror").html("A user account with this email is not found.");
			$("#signinerror").fadeOut(7000);
			$("#email").focus();
			return false;
			
		}else{
			$('#user_form_auth_forgot').submit();
		}
		
		//$('#signupform').submit();
	});
	return false;
}
</script>
