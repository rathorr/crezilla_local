<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: confirm.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style>
	#smoothbox_window form.global_form_popup button#submit {
        padding: 11px 36px !important;
	}
#smoothbox_window form.global_form_popup #buttons-wrapper {
	width: 100%;
}
</style>
<?php echo $this->form->render($this) ?>
