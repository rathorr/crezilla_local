<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9915 2013-02-15 01:30:19Z alex $
 * @author     John
 */
?>

<h2>
  <?php echo $this->translate("Member Reviews") ?>
</h2>

<?php if( count($this->navigation) ): ?>
  <div class='tabs'>
    <?php
      // Render the menu
      //->setUlClass()
      echo $this->navigation()->menu()->setContainer($this->navigation)->render()
    ?>
  </div>
<?php endif; ?>

<br />

<script type="text/javascript">
  var currentOrder = '<?php echo $this->order ?>';
  var currentOrderDirection = '<?php echo $this->order_direction ?>';
  var changeOrder = function(order, default_direction){
    // Just change direction
    if( order == currentOrder ) {
      $('order_direction').value = ( currentOrderDirection == 'ASC' ? 'DESC' : 'ASC' );
    } else {
      $('order').value = order;
      $('order_direction').value = default_direction;
    }
    $('filter_form').submit();
  }

</script>

<div class='admin_search'>
  <?php //echo $this->formFilter->render($this) ?>
</div>

<br />

<div class='admin_results'>
  <div>
    <?php $count = $this->totalReviews; ?>
    <?php echo $this->translate(array("%s review found", "%s reviews found", $count),
        $this->locale()->toNumber($count)) ?>
  </div>
  <div>
    <?php echo $this->paginationControl($this->reviews, null, null, array(
      'pageAsQuery' => true,
      'query' => $this->formValues,
      //'params' => $this->formValues,
    )); ?>
  </div>
</div>

<br />

<div class="admin_table_form">
<form id='multimodify_form' method="post" action="<?php echo $this->url(array('action'=>'multi-modify'));?>" onSubmit="multiModify()">
  <table class='admin_table'>
    <thead>
      <tr>
        <th style='width: 1%;'><input onclick="selectAll()" type='checkbox' class='checkbox'></th>
        <th><a href="javascript:void(0);" onclick="javascript:changeOrder('username', 'ASC');"><?php echo $this->translate("Username") ?></a></th>
        <th style='width: 1%;'><a href="javascript:void(0);" onclick="javascript:changeOrder('email', 'ASC');"><?php echo $this->translate("Email") ?></a></th>
        <th style='width: 1%;' class='admin_table_centered'><a href="javascript:void(0);" onclick="javascript:changeOrder('title', 'ASC');"><?php echo $this->translate("Title") ?></a></th>
        <th style='width: 1%;' class='admin_table_centered'><a href="javascript:void(0);" onclick="javascript:changeOrder('summary', 'ASC');"><?php echo $this->translate("Review") ?></a></th>
         <th style='width: 1%;' class='admin_table_centered'><a href="javascript:void(0);" onclick="javascript:changeOrder('rating', 'ASC');"><?php echo $this->translate("Rating") ?></a></th>
        <th style='width: 1%;' class='admin_table_centered'><a href="javascript:void(0);" onclick="javascript:changeOrder('is_approved', 'ASC');"><?php echo $this->translate("Status") ?></a></th>
        <th style='width: 1%;'><a href="javascript:void(0);" onclick="javascript:changeOrder('review_date', 'DESC');"><?php echo $this->translate("Review Date") ?></a></th>
        <th style='width: 1%;' class='admin_table_options'><?php echo $this->translate("Options") ?></th>
      </tr>
    </thead>
    <tbody>
      <?php if( count($this->reviews) ): ?>
        <?php foreach( $this->reviews as $review ):
          $user = $this->item('user', $review->reviewer_id);
          ?>
          <tr>
            <td><input name='modify_<?php echo $review->review_id();?>' value='<?php echo $review->review_id();?>' type='checkbox' class='checkbox'></td>
            <td class='admin_table_bold'>
              <?php echo $this->htmlLink($user->getTitle(), 
                  array('target' => '_blank'))?>
            </td>
            <td class='admin_table_email'>
                <a href='mailto:<?php echo $item->email ?>'><?php echo $item->email ?></a>
            </td>
            <td class="admin_table_centered nowrap">
              <?php echo $review->title;?>
            </td>
            <td class='admin_table_centered'>
              <?php echo $review->summary;?>
            </td>
            <td class='admin_table_centered'>
              <?php echo $this->partial('_review_rating_big.tpl', 'user', array('rate_number' => $review->rating));?>
            </td>
            <td class='admin_table_centered'>
              <?php echo ( $review->is_approved ? $this->translate('Published') : $this->translate('Unpublished') ) ?>
            </td>
            <td class="nowrap">
              <?php echo $this->locale()->toDateTime($review->rating_date) ?>
            </td>
            <td class='admin_table_options'>
              <a class='smoothbox' href='<?php echo $this->url(array('action' => 'stats', 'id' => $item->user_id));?>'>
                <?php echo $this->translate("stats") ?>
              </a>
              |
              <a class='smoothbox' href='<?php echo $this->url(array('action' => 'edit', 'id' => $item->user_id));?>'>
                <?php echo $this->translate("edit") ?>
              </a>
              <?php if (($this->superAdminCount>1 && $item->level_id==1) || $item->level_id != 1): // @todo change this to look up actual superadmin level ?>
                |
                <a class='smoothbox' href='<?php echo $this->url(array('action' => 'delete', 'id' => $item->user_id));?>'>
                  <?php echo $this->translate("delete") ?>
                </a>
              <?php endif;?>
              <?php if( $item->level_id != 1 ): // @todo change this to look up actual superadmin level ?>
                |
                <a href='<?php echo $this->url(array('action' => 'login', 'id' => $item->user_id));?>' onclick="loginAsUser(<?php echo $item->user_id ?>); return false;">
                  <?php echo $this->translate("login") ?>
                </a>
              <?php endif; ?>
              |
              <a class='user_review' href='<?php echo $this->url(array('action' => 'review', 'id' => $item->user_id));?>'>
                <?php echo $this->translate("reviews") ?>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
  <br />
  <div class='buttons'>
    <button type='submit' name="submit_button" value="approve"><?php echo $this->translate("Approve Selected") ?></button>
    <button type='submit' name="submit_button" value="delete" style="float: right;"><?php echo $this->translate("Delete Selected") ?></button>
  </div>
</form>
</div>