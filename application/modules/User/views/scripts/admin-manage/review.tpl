<style type="text/css">
.rating_star_big_disabled {
    background-image: url("<?php echo $this->baseUrl();?>/application/modules/User/externals/images/star_big_disabled.png");
}
.ynmember_rating_star_generic {
    background-repeat: no-repeat;
    cursor: pointer;
    display: inline-block;
    float: left;
    font-size: 1px;
    height: 24px;
    width: 24px;
}
.rating_star_big {
    background-image: url("<?php echo $this->baseUrl();?>/application/modules/User/externals/images/star_big.png");
}
table.admin_table tbody tr td {
    padding: 7px !important;
}
table.admin_table thead tr th a {
     text-decoration:none !important;
	 cursor:default !important;
}
</style>
<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9915 2013-02-15 01:30:19Z alex $
 * @author     John
 */
?>

<h2>
  <?php echo $this->translate("Member Reviews") ?>
</h2>

<?php if( count($this->navigation) ): ?>
  <div class='tabs'>
    <?php
      // Render the menu
      //->setUlClass()
      echo $this->navigation()->menu()->setContainer($this->navigation)->render()
    ?>
  </div>
<?php endif; ?>

<br />

<script type="text/javascript">
  var currentOrder = '<?php echo $this->order ?>';
  var currentOrderDirection = '<?php echo $this->order_direction ?>';
  var changeOrder = function(order, default_direction){
    // Just change direction
    if( order == currentOrder ) {
      $('order_direction').value = ( currentOrderDirection == 'ASC' ? 'DESC' : 'ASC' );
    } else {
      $('order').value = order;
      $('order_direction').value = default_direction;
    }
    $('filter_form').submit();
  }

</script>

<div class='admin_search'>
  <?php echo $this->formFilter->render($this) ?>
</div>


<div class='admin_results'>
  <?php /*?><div>
    <?php $count = $this->totalReviews; ?>
    <?php echo $this->translate(array("%s review found", "%s reviews found", $count),
        $this->locale()->toNumber($count)) ?>
  </div><?php */?>
  <div>
    <?php echo $this->paginationControl($this->reviews, null, null, array(
      'pageAsQuery' => true,
      'query' => $this->formValues,
      //'params' => $this->formValues,
    )); ?>
  </div>
</div>

<br />

<div class="admin_table_form">
<form id='multimodify_form' method="post" action="<?php echo $this->url(array('action'=>'multi-modify'));?>" onSubmit="multiModify()">
  <table class='admin_table'>
    <thead>
      <tr>
        <th style='width: 5%;'><input onclick="selectAll()" type='checkbox' class='checkbox'></th>
        <th style='width: 10%;'><a href="javascript:void(0);"><?php echo $this->translate("Username") ?></a></th>
        <th style='width: 10%;' class='admin_table_centered'><a href="javascript:void(0);" ><?php echo $this->translate("Title") ?></a></th>
        <th style='width: 35%;' class='admin_table_centered'><a href="javascript:void(0);" ><?php echo $this->translate("Review") ?></a></th>
         <th style='width: 15%;' class='admin_table_centered'><a href="javascript:void(0);" ><?php echo $this->translate("Rating") ?></a></th>
        <th style='width: 5%;' class='admin_table_centered'><a href="javascript:void(0);" ><?php echo $this->translate("Published") ?></a></th>
        <th style='width: 10%;'><a href="javascript:void(0);" ><?php echo $this->translate("Review Date") ?></a></th>
        
      </tr>
    </thead>
    <tbody>
      <?php if( count($this->reviews) ): ?>
        <?php foreach( $this->reviews as $review ):
          $user = $this->item('user', $review->reviewer_id);
          ?>
          <tr id="main_row_<?php echo $review->review_id;?>">
            <td><input name='modify' value='<?php echo $review->review_id;?>' type='checkbox' class='checkbox_review'></td>
            <td class='admin_table_bold'>
              <a href='<?php echo $user->getHref(); ?>'><?php echo $user->getTitle() ?></a>
            </td>
            <td class="admin_table_centered nowrap">
              <?php echo $review->title;?>
            </td>
            <td class='admin_table_centered'>
              <?php echo $review->summary;?>
            </td>
            <td class='admin_table_centered'>
              <?php echo $this->partial('_review_rating_big.tpl', 'user', array('rate_number' => $review->rating));?>
            </td>
            <td class='admin_table_centered is_approved_<?php echo $review->review_id;?>'>
              <?php echo ( $review->is_approved ? $this->translate('Yes') : $this->translate('No') ) ?>
            </td>
            <td class="nowrap">
              <?php echo $this->timestamp($review->review_date); ?>
            </td>
            
          </tr>
        <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
  <br />
  <div class='buttons'>
    <button type='button' name="submit_button" value="publish" onclick="change_review_status('1','is_approved')"><?php echo $this->translate("Publish Selected") ?></button>
    <button type='button' name="submit_button" value="unpublish" onclick="change_review_status('0','is_approved')"><?php echo $this->translate("Unpublish Selected") ?></button>
     <a href="<?php echo $this->baseUrl().'/admin/user/manage';?>" style="float: right;background-color: #619dbe;border: 1px solid #50809b;border-radius: 3px;color: #fff;font-family: arial,sans-serif;font-weight: 700;padding: 0.6em 1em; margin-left:4px; text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.3);">Back</a>
    <button type='button' name="submit_button" value="delete" style="float: right;" onclick="change_review_status('1','is_deleted')"><?php echo $this->translate("Delete Selected") ?></button>
  </div>
</form>
</div>
<script type="text/javascript">
function change_review_status(status, type){
			var review_ids	=	[];
			jQuery('.checkbox_review').each(function(){
				if(jQuery(this).is(":checked")	==true){
					review_ids.push(jQuery(this).val());		
				}
			});
			if(review_ids.length>0){
				if(type == 'is_approved' && status == '0'){
					var msg	=	"Are you sure you want to unpublish these reviews?";
				}else if(type == 'is_approved' && status == '1'){
					var msg	=	"Are you sure you want to publish these reviews?";
				}else if(type == 'is_deleted'){
					var msg	=	"Are you sure you want to delete these reviews?";
				}
				if( confirm(msg) ){
					jQuery('.loading_image').show();
					jQuery.post('<?php echo $this->baseUrl();?>/admin/user/manage/changereviewstatus',{status:status,review_ids:review_ids, type:type},function(resp){
					jQuery.each(review_ids , function(i, val) {
						if(type == 'is_deleted'){
							jQuery('#main_row_'+val).remove();	
						}else{
						var is_app	='';
						  if(status == 0){
								is_app	=	'No';  
						  }else{
								is_app	=	'Yes';  
						  }
						  jQuery('.is_approved_'+val).html(jQuery.trim(is_app)); 
						}
					});
					jQuery('input[type="checkbox"]').attr("checked", false);
					
					jQuery('.loading_image').hide();
				});  
				  }
				
				
				
			}
 	
}

function selectAll()
{
  var i;
  var multimodify_form = $('multimodify_form');
  var inputs = multimodify_form.elements;
  for (i = 1; i < inputs.length - 1; i++) {
    if (!inputs[i].disabled) {
      inputs[i].checked = inputs[0].checked;
    }
  }
}
</script>