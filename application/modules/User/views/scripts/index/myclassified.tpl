<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: profile.tpl 9984 2013-03-20 00:00:04Z john $
 * @author     John
 */
?>

<style type="text/css">

.layout_main .layout_middle {
    width: 100% !important;
   background-color: #E9EAED !important;
}
.container {
    padding-right: 0px;
    padding-left: 0px;
  }
 div._member_list_item_subtitle > a,
 div.joburl > a {
    color: #fb933c;
  }
a.create_classified
{
    background-color: #fb933c !important;
    padding-right: 30px;
    border-radius: 0;
    padding: 5px 8px;
    height: 30px !important;
    outline: none;
    color: #fff;
    text-decoration: none !important;
    float: right;
  }
  a.contact_classified
{
    color: #fff;
    text-decoration: none !important;
  }
  ._member_shortlist_head {
    float: left;
    width: 100%;
    padding-bottom: 8px;
    padding-top: 10px;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 10px;
    font-size: 18px;
    text-transform: uppercase;
}
 ._member_shortlist_section .member_list_item_area {
    border: 1px solid #f1f1f1;
    color: #666;
    padding: 5px 5px 2px;
}
.member_list_item_area {
    background: #fff;
    padding: 5px 5px 8px;
    float: left;
    width: 100%;
}
.padrgtnone {
    padding-right: 0px;
}
.jobdescription {
    width: 100%;
    margin-bottom: 10px;
    margin-top: 10px;
    max-height: 100px;
    min-height: 100px;
}
._member_shortlist_section ._member_list_item_title a {
    font-family: 'roboto';
    color: #333;
}
._member_list_item_title a {
    color: #333;
    font-size: 18px;
    font-weight: 500 !important;
}
._member_shortlist_section ._member_list_item_action {
    padding-top: 10px;
}
._member_list_item_action {
    float: left;
    width: 100%;
}
.padlftnone {
    padding-left: 0px;
}
._member_list_item_action a {
    text-decoration: none;
    color: #cacaca;
}
.search-members
{
  float: right;
}
.member_list_item {
    margin-bottom: 20px;
    max-height: 250px;
    min-height: 250px;
}
._member_list_item_detials
{
  width: 100%;
    margin-bottom: 10px;
    margin-top: 10px;
    min-height: 20px;
    height: 30px;
    line-height: 30px;  
}
._member_list_item_title
{
    border-bottom: 1px solid #ccc;
    margin-bottom: 10px;
}
.joburl
{
  min-height: 20px;
  max-height: 20px;
}
.edit_classified
{
  background-color: #fb933c !important;
  color: #fff !important;
  text-decoration: none !important;
  float: left;
  width: 48%;
  text-align: center;
  height: 30px;
  line-height: 30px;
  margin-right: 10px; 
  font-weight: 500 !important;
}
.delete_classified
{
  background-color: #fb933c !important;
  color: #fff !important;
  text-decoration: none !important;
  float: left;
  width: 48%;
  text-align: center;
  height: 30px;
  line-height: 22px; 
  border: 0 none !important;
}
@media (max-width: 768px){

  .container {
    padding-right: 15px;
    padding-left: 15px;
  }
  a.create_classified
  {
    margin-top: 10px;
  }
}
</style>
<div class="layout_page_user_index_myclassified">
<div class="generic_layout_container layout_top">
</div>

<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
  <?php $viewerid = Engine_Api::_()->user()->getViewer()->getIdentity();?>
<div class="container">
<div class="_member_shortlist_head">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 member-shortlist_result_count">My Classifieds Board : Total <?php echo count($this->classifiedads_array);?> Classifieds</div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class=" col-lg-5 col-md-5 col-sm-6 col-xs-6 search-members">
      <a href="/members/postclassified" class="create_classified">Create a Classified</a>
      </div>
    </div>  
  </div>
</div>
</div>
<section class="_member_shortlist_section">
  <div class="container">    
    <div class=" member_list _manage_pages_list">
      <div class="row">
      <?php
      foreach ($this->classifiedads_array as $classified_ad) { 
        $id=$classified_ad['postclassified_id'];
        $ownerid=$classified_ad['owner_id'];
        $title= $classified_ad['title'];
        $description= $classified_ad['description'];
        $url= $classified_ad['url'];
        $path="/members/editpostclassified/$id";
        $link='href="'."http://".$url.'"';
         $postedby = Engine_Api::_()->getItem('user', $ownerid)->getTitle();
         //echo $classified_ad['creation_date'];
         $postdate= date('d-m-Y',strtotime($classified_ad['creation_date']));
         $contactlink='href=/messages/classifiedcompose?'.'id='.$id;
          ?>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 member_list_item">
          <div class="member_list_item_area">
            <!-- <div class="col-xs-12 padrgtnone text-right">
              <i class="fa fa-eye" aria-hidden="true"></i> 20
            </div> -->
            
            <div class="_member_list_item_title">
          <?php if (empty($classified_ad['postclassified_id'])) {
            $set_target = '';
          } else {
            $set_target = 'target="_blank"';
          }
          if($viewerid == $ownerid)
          {
          ?>
          <a href="<?php echo $path?>" <?php echo $set_target ?>> <?php echo ucfirst($title) ?></a>
          <?php } else{?>
          <a style="cursor:default;"> <?php echo ucfirst($title) ?></a>
          <?php }?>
            </div>
            <div class="_member_list_item_subtitle">Posted By: <a href="/profile/<?php echo $ownerid?>" <?php echo $set_target ?>><?php echo ucfirst($postedby) ?></a> Dated: <?php echo $postdate;?></div>
            <div class="jobdescription">
           <?php echo $description ?>
            </div>
            <div class="joburl">
            <a style="text-transform: lowercase;" <?php echo $link ?> <?php echo $set_target ?>> <?php echo $url ?></a>
            </div>
            <div class="_member_list_item_detials">
             <!-- <a title=""><i class="fa fa-heart" aria-hidden="true"></i></a> -->
             <a class="edit_classified" href="<?php echo $path?>"> Edit</a>
             <input type="submit" class="delete_classified" name="delete_<?php echo $i?>" id="delete_<?php echo $i?>" value="Delete">
            </div>
          </div>
        </div>
        <?php }?>
    </div>  
      </div>
    </div>
</section>

</div>

</div>

</div>

</div>

<script type="text/javascript">
  jQuery(document).ready(function(){
     jQuery('.loading_image').css("display", "none");
     jQuery('#actions input').click(function (){
      jQuery('.loading_image').css("display", "block");
     });
     /*jQuery('div #classified_url').click(function(){
      window.location.href =jQuery(this).attr("href");
      alert(jQuery(this).attr("href"));

     });*/
  });
</script>

  


