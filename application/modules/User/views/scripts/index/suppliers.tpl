<style type="text/css">
.browsemembers_criteria {
    background: url(public/jobs/banner-img.jpg) no-repeat center center;
    background-size: cover;
    min-height: 200px;
    position: relative;
    /* margin-bottom: 36px; */
    padding: 0px;
}  
#global_content > div > div > div.generic_layout_container.layout_user_browse_search > form > div > ul {
    padding-top: 6%;
    padding-left: 2%;
}
.browsemembers_criteria ul {
    margin: 0px;
    padding: 0px;
    list-style: none;
} 
#job_result > li > div > div.job_lft_box > a > img
{
    background-size: contain;
    float: left;
    height: 100px;
    width: 100px;
} 
._job_details {
    font-size: 14px;
    color: #333;
    padding-bottom: 10px;
    padding-top: 30px;
}
._job_details i {
    color: #cacaca;
    padding-right: 3px;
}
span._job_loc {
    padding-left: 25px;
}
._member_list_item_review {
    font-size: 14px;
    color: #ffc100;
    float: left;
    /*width: 100%;
    padding-top: 20px;*/
    padding-top: 7px;
    padding-left: 10px;
}
.padlftnone {
    float: right;
    /*width: 100%;*/
}
#job_result > li> div > div.job_rgt_box > h3 > a
{
    float: left;
}
._member_list_item_action {
    float: left;
    width: 100%;
    padding-left: 10px;
    padding-top: 10px;
}
._member_list_item_action a {
    text-decoration: none;
    color: #cacaca;
}
._addfolderr_icn {
    width: 13px;
    height: 13px;
    display: inline-block;
    background: url(public/custom/images/add_folder_icn.png) no-repeat center center;
    background-size: cover;
    margin-right: 0;
    margin-left: 8px;
}
#job_result > li.job_total_count
{
    background-color: #e8e8e8;
}
.browsemembers_criteria2
{
  padding-top: 10px;
    background-color: #E9EAED;
    text-align: left;
    border-bottom: 1px solid #ccc;
}
@media only screen and (max-device-width: 768px)
{
    ._member_list_item_review {
    font-size: 14px;
    color: #ffc100;
    float: left;
    padding-top: 5px;
    padding-left: 10px;
    width: auto;
}
.padlftnone {
    float: right;
    width: auto;
    text-align: right;
    padding-top: 5px;
}
span._job_loc {
    padding-left: 15px;
}
._member_list_item_action {
    padding-top: 5px;
}
._job_details {
    padding-bottom: 5px;
    }
.browsemembers_results>ul>li
{
    padding: 5px 0 5px 0;
}
}
</style>
<div class="layout_page_user_index_suppliers">
<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_user_browse_search">
<form method="post" action="<?php echo $this->baseUrl().'/members/getuserresults';?>" class="field_search_criteria ajaxform">
<input type="hidden" name="utype" value="suppliers" />
<div class="browsemembers_criteria">
<ul>

 <li>
<select name="designation_id" id="designation_id">
    <?php 
        if($this->dasignations){
            echo '<option value="">Categories</option>';
            foreach($this->dasignations as $desig){
                echo '<option value="'.$desig['dasignatedsubcategory_id'].'">'.$desig['dasignatedsubcategory_name'].'</option>';
            }
        }
    ?>
    </select>
 </li>

 <li>
<input type="text" name="location" value=""  id="location"  placeholder="Location"/>
<div id="autoSuggestionsListl" autocomplete="off"></div>
</li>

 <li>
<select name="exp" id="exp">

<option value="">Experience</option>
<option value="nc">New Comers</option>
<option value="ge">Gaining Experience</option>
<option value="ex">Experienced</option>  
<option value="ve">Veterans</option>        
</select>
 </li>
 <li>
<select name="lang" id="lang">
<option value="">Language</option>
        <?php 
            if($this->langs){
                foreach($this->langs as $lang){
                        echo '<option value="'.$lang['option_id'].'">'.$lang['label'].'</option>';
                }
            }
        ?>
        </select>
 </li>
<li>
<select name="gender" id="gender">
<option value="">Gender</option>
        <option value="2">Male</option>
        <option value="3">Female</option>
        <option value="111">Other</option>
</select>
 </li>
 <li>
<input type="text" value="" id="keyword" name="keyword" placeholder="Keyword">
</li>
 <li class="search_button">
<input type="button" name="serach" value="Search"  id="search"/>
</li>
<input type="hidden" name="list_count" id="list_count" value="20"/>
<input type="hidden" name="page_name" value="services"/>
</ul>

</div>
</form>
</div>

<div class="generic_layout_container layout_middle" style="float:left;">
<div class="generic_layout_container layout_core_content">
<div class="browsemembers_criteria2">
<ul>
 <li class="job_total_count">
 <label name="jobcount" id="jobcount">Total <span id="job_count"><?php echo count($this->users);?></span> Members</label>
</li>
</ul>
</div>
<div id="job_results" class="browsemembers_results">
  <?php if($this->users){?>
      <ul class="job_start row_start" style="background-color:#FFFFFF;" id="job_result">
      <!-- <li class="job_total_count">
         <label name="jobcount" id="jobcount">Total <span id="job_count"><?php echo count($this->users);?></span> members</label>
        </li> -->
     <?php foreach($this->users as $user){
     $userating= $user['avgrating'];?>
           <li>
            <div class="job_box">
                <div class="job_lft_box">
                    <?php 
                       $userpic =   Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($user['user_id']); 
                       $userpic =   ($userpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$userpic;
                        $user = Engine_Api::_()->getItem('user', $user['user_id']);
                        $link = $user->getHref().'?q=suppliers';
                    ?>
                    <a href="<?php echo $link;?>" style="cursor:pointer;">
                    <!-- <span style="background-image:url(<?php echo $userpic;?>);" class="search_user_profile"></span>  -->
                    <img src="<?php echo $userpic;?>">   
                    </a>

                </div>
                
                <div class="job_rgt_box">
                    
                    <h3 class="job_title">
                    <a href="<?php echo $link;?>">
                        <?php
                        $fname =  Engine_Api::_()->getDbtable('users', 'user')->getUserFirstName($user['user_id']); 
                        echo ($fname)?$fname:$user->getTitle();?>
                        
                        <span><?php
                        $lname =  Engine_Api::_()->getDbtable('users', 'user')->getUserLastName($user['user_id']); 
                        echo ($lname)?$lname:$user->getTitle();?>
                        </span>
                    </a>
                    <div class="_member_list_item_review">
                        <span>
                             <?php 
                            // $userating=Engine_Api::_()->getDbtable('reviews', 'user')->getAVGuserrating($user['user_id']); 
                             for($x=1;$x<=$userating;$x++) {?>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <?php } 
                            if ((int) $userating != $userating) {  ?>
                                     <i class="fa fa-star-half" aria-hidden="true"></i>
                             <?php }
                                if($x !=1)
                                  {
                                    while ($x<=5) { ?>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                              <?php $x++; }} ?>
                              <!-- <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i> -->
                        </span>
                    </div>
                    <div class=" padlftnone">
                     <i class="fa fa-eye" aria-hidden="true"></i>  <?php echo $user['view_count'];?>
                    </div>
                    </h3>
                    
                    <div class="_job_details">
                        <span class="_job_exp"><i class="fa fa-briefcase" aria-hidden="true"></i> <?php $expe=Engine_Api::_()->getDbtable('workexps', 'user')->getUseryrexp($user['user_id']);  
                        $exps=($expe < 2)?"Year":"Years";
                        echo $expe." ".$exps;?></span>
                        <span class="_job_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $work_loc=Engine_Api::_()->getDbtable('users', 'user')->getUserycity($user['user_id']);  ?></span>
                    </div>
                    <div class="job_desc">
                    <?php echo $professionalsummary =  Engine_Api::_()->getDbtable('professionalsummarys', 'user')->getProfessionalsummarys($user['user_id']); 
                    ?>
                    </div>
                </div>
                <div class="_member_list_item_action">
                    <div class="padrgtnone">
                    <a href="#" title="Like"><i class="fa fa-heart" aria-hidden="true"></i></a>
                    <a href="/members/friends/shortlist/user_id/<?php echo $user['user_id'];?>?q=" title="Shortlist"><i class="_addfolderr_icn" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            </li>
        <?php }
        echo '</ul>';
    }?>

</div>
<img style="position: fixed; top: 75%; display: none; width: 57px; left: 45%;" class="process_image" src="<?php echo $this->baseUrl();?>/public/custom/images/loading.gif">
<div id="view" style="display:none;"></div>
</div>
</div>
<div class="generic_layout_container layout_right">
<?php echo $this->content()->renderWidget('communityad.ads') ?>
</div>
</div>
</div>
<script src="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.js"></script>
 <link href="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/jquery.form.js';?>"></script>
<script src="https://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript">
jQuery(document).on('keypress', '#keyword, #location', function(e){
   if(e.which == 13 || e.which == 1 ) {
       jQuery('#list_count').val(0);
    searchusers();
   }
});
if(typeof(jQuery('.third_ad').offset()) !='undefined'){
 //Comment if no ads
var topv = jQuery('.third_ad').offset().top;
 jQuery(document).scroll(function() {
      var windscroll = jQuery(document).scrollTop();
      if (windscroll > topv) {
      jQuery('.create_div').show();
            jQuery(".third_ad").addClass("fxd_header");
           jQuery(".third_ad").removeClass("absolute_header");
    }else {
    jQuery('.create_div').hide();
        jQuery(".third_ad").removeClass("fxd_header");
        jQuery(".third_ad").addClass("absolute_header");
    }
    
 });
 }

jQuery(document).ready(function(){
  //jQuery('#skill_id').SumoSelect({ selectAll: true,placeholder: 'Select Skill'});
    //jQuery('#lang').SumoSelect({selectAll: true,placeholder: 'Select Language'});
    //jQuery('#gender').SumoSelect({selectAll: true,placeholder: 'Select Gender'});
    //jQuery('#exp').SumoSelect({placeholder: 'Select Experience'});
    //jQuery('#roles').SumoSelect({selectAll: true,placeholder: 'Select Role'});
    jQuery(window).scroll(bindScroll);
});

jQuery(document).on('click', '#search', function(){
  jQuery('#list_count').val(0);
  searchusers();
});

function bindScroll(){
   if(jQuery(window).scrollTop() + jQuery(window).height() > jQuery(document).height() - 900) {
       jQuery(window).unbind('scroll');
        if(jQuery('#list_count').val() != 0){
     loadmoreresults(); 
     }
   }
}
function searchusers(){
jQuery('.loading_image').css("display", "block");

//jQuery('#list_count').val(20);
jQuery(".ajaxform").ajaxForm({
      /* target:'#view',*/
      success: function(data){
        jQuery('.loading_image').css("display", "none");
        var response  = jQuery.trim(data).split('**');
        //jQuery('#job_results').html(jQuery.trim(response[0]));
        if((jQuery.trim(response[0]) =='<li>No Users Found.</li>')){
          jQuery('#job_result').html('<li style="list-style: outside none none; text-align: center;border-bottom: medium none !important;min-height: 20px !important;">No Users Found</li>');
        }else{
          jQuery('#job_result').html(jQuery.trim(response[0]));
          jQuery('#list_count').val(jQuery.trim(response[1]));
        }
        return false;
      }
    }).submit();
}


/*jQuery(window).scroll(function() {
    if(jQuery(window).scrollTop() >= jQuery(document).height() - jQuery(window).height() - 1000) {
       if(jQuery('#list_count').val() != 0){
         setTimeout(function(){
        checkPendingRequest();
        }, 1000);
         
         //loadmoreresults();
       }
    }
});*/

function loadmoreresults() {
  jQuery('.process_image').css("display", "block");
  jQuery('.process_image3').show();
  jQuery(".ajaxform").ajaxForm({
      /* target:'#view',*/
      success: function(data){
        jQuery('.process_image').css("display", "none");
        jQuery('.process_image3').hide();
        var response  = jQuery.trim(data).split('**');
        
        if(jQuery.trim(response[0]) == ''||(jQuery.trim(response[0]) =='<li>No Users Found.</li>')){
          jQuery('#job_result').append('<li style="list-style: outside none none; text-align: center;min-height: 20px !important;">No More Users</li>');
          jQuery('#list_count').val(0);
        }else{
          jQuery('#job_result').append(jQuery.trim(response[0]));
          jQuery('#list_count').val(response[1]);
        }
        jQuery(window).bind('scroll', bindScroll);
        return false;
      }
    }).submit();
}
jQuery(document).on('keyup','#location', function(e) {
         var inputString = jQuery(this).val();
        if(inputString.length < 1) {
                jQuery('#autoSuggestionsListl').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/user/edit/getlocations";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                      if(data)
                        {
                            jQuery('#autoSuggestionsListl').show();
                            jQuery('#autoSuggestionsListl').addClass('auto_listl');
                            jQuery('#autoSuggestionsListl').html(data);
                            jQuery("#autoSuggestionsListl").attr('style','height: auto; overflow: auto; max-height: 400px;')
                        }
                        else
                        {
                            jQuery('#autoSuggestionsListl').hide();
                        }
                     });
             }
    });
      jQuery(document).on('click','#autoSuggestionsListl li',function(){ 
            var el=jQuery(this).text();
           jQuery('#location').val(el);
        setTimeout("jQuery('#autoSuggestionsListl').hide();", 200);
       });
</script>