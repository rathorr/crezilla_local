<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: profile.tpl 9984 2013-03-20 00:00:04Z john $
 * @author     John
 */
?>
<!-- PROFILE FORM START -->
<style>
body{
  /*font-family: 'roboto';*/
  font-size:14px;
  color: #000;
  background: transparent!important;
}
ul{
  padding: 0;
  margin: 0;
  list-style: none;
}
#job_result > li > div > div > img{
  width: 100%;
}

._member_shortlist_head {
    float: left;
    width: 100%;
    padding-bottom: 8px;
    padding-top: 10px;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 10px;
    font-size: 18px;
    text-transform: uppercase;
}

._member_shortlist_section .member_list_item_area {
    border: 1px solid #f1f1f1;
    color: #666;
    padding: 5px 5px 2px;
}
.select-member-category,
.search-members{
    float: right;
}
.select-member-category{padding-right: 0px;}
.search-members input{
    background-color: #fb933c !important;
    padding-right: 30px;
    border-radius: 0;
    padding: 5px 8px;
    height: 30px !important;
    outline: none;
    color: #fff;
}
.search-members i{
    position: absolute;
    right: 25px;
    top: 6px;
    color: #cacaca;
    cursor: pointer;
}
.select-member-category select{
    border-radius: 0;
    padding: 5px 8px;
    height: 30px !important;
    outline: none;
    background-color: #fff !important;
}
img.member_img{
    height: 250px;
    object-fit: cover;
}

._member_shortlist_section ._member_list_item_review{
    padding-top:0px;
}
._member_shortlist_section ._member_list_item_action{
    padding-top: 20px;
}
._member_shortlist_section  ._member_list_item_title a{
    font-family: 'roboto';
    color: #333;
}
._member_shortlist_section ._member_list_item_subtitle{
    font-size: 14px;
    min-height: 20px;
    color: #666;
}
._member_shortlist_section ._member_list_item_action i.fa-heart{font-size: 14px;}
._member_shortlist_section ._member_list_item_action ._member_list_item_action_btns a{margin-right: 2px; padding: 0 5px;}
._member_shortlist_section ._member_list_item_action ._member_list_item_action_btns a:first-child{padding-left:0;}

@media(max-width:767px){
    .member-shortlist_result_count{
        text-align: center;
        padding-bottom: 10px;
    }
    .select-member-category {
        padding-right: 15px;
    }
}
._manage_pages_section, ._community_ads, .member_list_section {
    padding: 0 15px!important;
}
._manage_pages_head, .member_list_head{
  float: left;
    width: 100%;
    padding-bottom: 8px;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 30px;
}
._manage_pages_head_content, .member_list_head_content {
    font-size: 18px;
    text-transform: uppercase;
}
._manage_pages_list_item_area{
  float: left;
  width: 100%;
  background: #fff;
}
.padnone{
  padding: 0px;
}
._manage_pages_list_item_block {
    background: #fff;
    padding: 15px 15px;
    float: left;
    border-bottom: 1px solid #e5e5e5;
}
._manage_pages_list_item_block:last-of-type, ._manage_pages_list_item_block:nth-last-of-type(2) {
    border-bottom: none;
}
._manage_pages_list_item_block:nth-child(even) {
    border-left: 1px solid #e5e5e5;
}

._item_block_content {
    float: left;
    width: 100%;
}
._manage_pages_item_img {
    float: left;
    height: 105px;
    max-width: 100px;
}
._manage_pages_item_img img{
  width: 100%;
}
._manage_pages_item_content {
    width: calc(100% - 105px);
    float: right;
    padding: 0 9px;
}
  
._manage_pages_title a {
    color: #333;
    font-size: 18px;
}
._manage_pages_date{
  font-size: 14px;
  color: #999999;
}
._manage_pages_details {
    font-size: 14px;
    color: #999;
}
._manage_pages_desc {
    font-size: 14px;
    color: #333;
}
._item_block_footer {
    float: left;
    width: 100%;
}
._manage_pages_action {
  float: left;
  width: 100%;
    margin-top: 30px;
}
._manage_pages_action a._manage_pages_action_btn {
    text-decoration: none;
    color: #333;
    text-transform: uppercase;
    border: 1px solid #333;
    width: 85px;
    height: 30px;
    display: inline-block;
    text-align: center;
    line-height: 30px;
    margin-right: 10px;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    -ms-transition: 0.3s ease-in-out;
    -o-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
._manage_pages_action a._manage_pages_action_btn:last-child{margin-right: 0px;}
._manage_pages_action a._manage_pages_action_btn:hover {
    color: #fa933c;
    border: 1px solid #fa933c;
}

.footer_section {
    padding: 40px 0;
    background: #e8e8e8;
    float: left;
    width: 100%;
    margin-top: 100px;
}
.footer-links a{
  text-decoration: none;
  color: #1f1f1f;
  font-size: 14px;
  text-transform: uppercase;   
}
.footer-links ul{
  margin: 0px;
  padding:0px;
  list-style: none;
}
.footer-links ul li{
  display: block;
  margin-bottom: 8px;
}
.footer_head{
  font-size: 14px;
  color: #1f1f1f;
  text-transform: uppercase;
  padding-bottom: 8px; 
}
.footer-social-content a {
    text-decoration: none;
    display: inline-block;
    margin-right: 10px;
    -webkit-transition: 0.5s ease-in-out;
    -moz-transition: 0.5s ease-in-out;
    -ms-transition: 0.5s ease-in-out;
    -o-transition: 0.5s ease-in-out;
    transition: 0.5s ease-in-out;
}
.footer-social-content a:hover{
  opacity: 0.5;
}
.footer-newsletter{padding-right: 0px;}
.footer-newsletter-content input.newsletter-input{
  height: 46px;
  line-height: 46px;
  border: 1px solid #000000;
  padding:0 20px;
  background: #fff;
  color: #1f1f1f;
  font-size:14px;
  text-transform: uppercase; 
  width: 65%;
  margin-right: -5px;
}
.footer-newsletter-content input.newsletter-submit{
  height: 47px;
  line-height: 46px;
  border: 1px solid #000000;
  padding:0 20px;
  background: #000;
  color: #fff;
  font-size:14px;
  text-transform: uppercase; 
  width: 35%;
  
}
.copyright_section {
    background: #d4d4d4;
    float: left;
    width: 100%;
    padding: 15px 0 10px;
}
.copyright_section span{text-transform: uppercase;}
.member_list_item_area{
  background: #fff;
  padding:5px 5px 8px;
  float: left;
  width: 100%;
}

._member_list_item_title a {
    color: #333;
    font-size: 18px;
    font-weight: 500 !important;
}
._member_list_item_subtitle{
  font-size: 14px;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
  word-wrap: ;
  white-space: nowrap;
}
._member_list_item_action{
  float: left;
  width: 100%;
}
._member_list_item_action a{
  text-decoration: none;
  color: #cacaca; 
}
._member_list_item_review {
  font-size:14px;
  color: #ffc100; 
  float: left;
  width: 100%;
  padding-top: 20px;
}
._member_list_item_review .fa-star{
  cursor: pointer;
}
.padrgtnone{padding-right: 0px;}
.padlftnone{padding-left: 0px;}
._addfolderr_icn {
    width: 13px;
    height: 13px;
    display: inline-block;
    background: url(img/add_folder_icn.png) no-repeat center center;
    background-size: cover;
    margin-right: 0;
    margin-left: 8px;
}
.member_list_item {
    margin-bottom: 30px;
    max-height: 350px;
    min-height: 350px;
}
.member_list_search{
  float: left;
  width: 100%;
  padding-bottom: 20px;
}
.member_list_search input.search-input{
  border-radius: 0;
  box-shadow: none;
  margin-bottom: 10px;
}
.member_list_search input.search-submit{
  background: #ff9e20;
  color: #fff;
  border-radius: 0;
  box-shadow: none;
  -webkit-transition: 0.5s ease-in-out;
  -moz-transition: 0.5s ease-in-out;
  -ms-transition: 0.5s ease-in-out;
  -o-transition: 0.5s ease-in-out;
  transition: 0.5s ease-in-out;
}
.member_list_search input.search-submit:hover{
  background: #1f1f1f;
}
@media (max-width: 1024px){
  .footer-newsletter,
  .footer-social-links {padding-left:0px; clear:both; margin-top: 20px;}

}
@media (max-width: 786px){

  .footer-social-links{
    clear: none;
    margin-top: 0px;
  }
}
#job_result > li > div > div > div._member_list_item_detials > div > div.text-right.padrgtnone > div > span
{
 float: right;
}
</style>
<!-- PROFILE FORM END -->



<!-- PHOTOS FORM START -->
<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle" style="width:100%; background-color: #E9EAED;">
<div class="generic_layout_container layout_core_content">
   <!-- SHORTLIST SECTION START -->
<div class="container">
<div class="_member_shortlist_head">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 member-shortlist_result_count"><?php echo count($this->users);?> Member Shortlisted</div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <form method="post" action="<?php echo $this->baseUrl().'/members/getshortlistresults';?>" class="field_search_criteria ajaxform">
      <div class=" col-lg-5 col-md-5 col-sm-6 col-xs-6 search-members">
      <input type="button" class="form-control" name="serach" value="Search"  id="search"/>
        <!-- <i class="fa fa-search" aria-hidden="true"></i> -->
        <input type="hidden" name="list_count" id="list_count" value="20"/>
      </div>
      <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 select-member-category">
        <select class="form-control" name="shortlistfolder_id" id="shortlistfolder_id">
            <?php 
          if($this->shortlistfolders){
        echo '<option value="">Select folder</option>';
              foreach($this->shortlistfolders as $shortlistfolder){
                    echo '<option value="'.$shortlistfolder['shortlistfolder_id'].'">'.$shortlistfolder['folder_name'].'</option>';
                }
            }
        ?>
        </select>
      </div>
    
      </form>

    </div>  
  </div>
</div>
</div>

<section class="_member_shortlist_section">
  <div class="container">    
    <div class=" member_list _manage_pages_list">
      <div class="row">
        <?php if( $this->users){ 
      echo '<ul style="background-color:#FFFFFF;" id="job_result">';
      foreach($this->users as $user){
      $talentpic  = Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($user->shortlist_user_id);
        $talentpic  = ($talentpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$talentpic;

        $userating=Engine_Api::_()->getDbtable('reviews', 'user')->getAVGuserrating($user->shortlist_user_id);
        ?>
                      
       <li>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 member_list_item">
          <div class="member_list_item_area">
            <div class="col-xs-12 padrgtnone text-right">
              <i class="fa fa-eye" aria-hidden="true"></i> <?php echo $user->view_count;?>
            </div>
            <img src="<?php echo $talentpic;?>" class="img-responsive member_img">
            <div class="_member_list_item_title">
            <a href="/profile/<?php echo $user->shortlist_user_id;?>" title=""> <?php echo $user->displayname;?></a></div>
            <div class="_member_list_item_subtitle"><?php echo $userdesig=Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategoriesSearch($user->shortlist_user_id); 
            ?></div>
            <div class="_member_list_item_detials">
              <div class="_member_list_item_action">
                <div class="col-md-6 col-sm-6 col-xs-6 text-left padlftnone _member_list_item_action_btns">
                  <a title=""><i class="fa fa-heart" aria-hidden="true"></i></a>
                  <a title=""><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                  <a href="<?php echo $this->baseUrl().'/members/friends/removeshortlist/user_id/'.$user->shortlist_user_id.'?q=shortlist';?>" class="smoothbox"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 text-right padrgtnone">
                  <div class="_member_list_item_review">
                    <span>
                      <?php for($x=1;$x<=$userating;$x++) {?>
                          <i class="fa fa-star" aria-hidden="true"></i>
                <?php } 
                if ((int) $userating != $userating) {  ?>
                         <i class="fa fa-star-half" aria-hidden="true"></i>
                 <?php }
                     if($x !=1) { while ($x<=5) { ?>
                          <i class="fa fa-star-o" aria-hidden="true"></i>
                  <?php $x++; } }?>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </li>
        <?php } echo '</ul>'; } else{ ?>
   <div class="tip">
      <span id="no-album">
        <?php echo $this->translate('You do not have any shortlists yet.');?>
         
      </span>
    </div>  
  <?php } ?>

      </div>
    </div>
  </div>  
</section>
<!-- SHORTLIST SECTION END -->
<img style="position: fixed; top: 75%; display: none; width: 57px; left: 45%;" class="process_image" src="<?php echo $this->baseUrl();?>/public/custom/images/loading.gif">
</div>
</div>
</div>

<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/jquery.form.js';?>"></script>
<script src="https://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
    //jQuery('#shortlistfolder_id').SumoSelect({selectAll: true,placeholder: 'Select folder'});
    jQuery(window).scroll(bindScroll);  
});

jQuery(document).on('click', '#search', function(){
  jQuery('#list_count').val(0);
  searchusers();
});

function bindScroll(){
   if(jQuery(window).scrollTop() + jQuery(window).height() > jQuery(document).height() - 950) {
       jQuery(window).unbind('scroll');
     if(jQuery('#list_count').val() != 0){
     loadmoreresults(); 
     }
       
   }
}
function searchusers(){
jQuery('.loading_image').css("display", "block");
//jQuery('#list_count').val(20);
jQuery(".ajaxform").ajaxForm({
      /* target:'#view',*/
      success: function(data){
        jQuery('.loading_image').css("display", "none");
        var response  = jQuery.trim(data).split('**');
        //jQuery('#job_results').html(jQuery.trim(response[0]));
        if((jQuery.trim(response[0]) =='<li>No Member Found.</li>')){
          //jQuery('#job_result').html('<li style="list-style: outside none none; text-align: center;border-bottom: medium none !important;">No More Members</li>');
        }else{
          jQuery('#job_result').html(jQuery.trim(response[0]));
          jQuery('#list_count').val(jQuery.trim(response[1]));
        }
        return false;
      }
    }).submit();
}

function loadmoreresults() {
  jQuery('.process_image').css("display", "block");
  jQuery(".ajaxform").ajaxForm({
      /* target:'#view',*/
      success: function(data){
        jQuery('.process_image').css("display", "none");
        var response  = jQuery.trim(data).split('**');
        
        if(jQuery.trim(response[0]) == ''||(jQuery.trim(response[0]) =='<li>No Member Found.</li>')){
          //jQuery('#job_result').append('<li style="list-style: outside none none; text-align: center;">No More Members</li>');
          jQuery('#list_count').val(0);
        }else{
          jQuery('#job_result').append(jQuery.trim(response[0]));
          jQuery('#list_count').val(response[1]);
        }
        jQuery(window).bind('scroll', bindScroll);
        return false;
      }
    }).submit();
}

</script>