<style>

#global_content h2 {
    color: #fb933c;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 700;
    margin: 0 20px;
    padding: 10px 0px;
    text-transform: capitalize;
}
.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:auto;
  width:100%;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}


ul.form-errors > li, ul.form-notices > li {
 float:none;
}

.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
h2.create_pdf
{margin:6px !important;}
h2.create_pdf a
{padding:10px !important;}
</style>

<div class="headline">
  <h2>
    <?php if ($this->viewer->isSelf($this->user)):?>
      <?php echo $this->translate('Connections');?>
    <?php else:?>
      <?php echo $this->translate('%1$s\'s Connections', $this->htmlLink($this->user->getHref(), $this->user->getTitle()));?>
    <?php endif;?>
  </h2>
  
</div>

<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle" style="width:100%; background-color: #fff;
    padding: 1% 2%;">
<div class="generic_layout_container layout_core_content">
<div class="profile_edit_info" style="padding:0;">
	<a href="<?php echo $this->baseUrl().'/profile/'.$this->friend_id;?>" class="skip_text skip_bt2" style="float: right;">Go to Profile</a>
	<ul class='profile_friends' id="user_profile_friends">
                    <?php
                    $k=0;
                      foreach( $this->myfriends as $friends ):
                        $friend = Engine_Api::_()->getItem('user', $friends['user_id']);
                        ?>
                    
                        <li id="user_friend_<?php echo $friend->getIdentity() ?>">
		
        <?php $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($friend->getIdentity());
if($pic == '')$pic	=	$this->baseUrl().'/application/modules/User/externals/images/nophoto_user_thumb_profile.png';
 ?>
        
        <div id='friend_photo' style="background-image:url(<?php echo $pic; ?>);margin:0 auto;" class="wall_background set_friend">

</div>
      <div class='profile_friends_body_div text-center'>
            <?php echo $this->htmlLink($friend->getHref(), $friend->getTitle()) ?>
          
      </div>
      
      <div class='profile_friends_options_div text-center'>
        <?php echo $this->userFriendship($friend) ?>
      </div>

    </li>
                    
                      <?php $k++; endforeach;
                      	if($k==0){
                    		echo '<li style="text-align:left;">No Connections</li>';
                  		}
                      ?>
                        
                    </ul>
                    <img style="left: 41%;position: fixed;top: 75%;width: 21px; display:none;" class="process_image1" src="<?php echo $this->baseUrl();?>/public/custom/images/loading.gif">
</div>
<input type="hidden" id="list_count" value="30"/>
<script type="text/javascript">

jQuery(document).scroll(function(){
	if (jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()){
				var list_count	   =	jQuery.trim(jQuery('#list_count').val());
				if(jQuery('#list_count').val() != '0'){
					getmoreresult(list_count);
				}
		}
	});
function getmoreresult(list_count) {	
	var section = 'friend';
	jQuery.post("<?php echo $this->baseUrl().'/members/index/getmorefriends';?>",{section:section, list_count:list_count, friend_id:'<?php echo $this->friend_id;?>'}, function(resp){
		//jQuery('.loading_image').hide();
		var response	=	jQuery.trim(resp).split('**');
		if(section == 'friend'){
				jQuery('#user_profile_friends').append(jQuery.trim(response[0]));
				jQuery('.process_image1').hide();
				if(response[0] == ''){
					jQuery('#list_count').val(0);
				}else{
					jQuery('#list_count').val(response[1]);
				}
		}
	});
	
}
</script>
</div>
</div>
</div>










