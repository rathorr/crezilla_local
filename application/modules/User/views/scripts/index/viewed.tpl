<div class="headline">
  <h2 class="ppl_vwed" style="text-transform:none !important;">People viewed your profile</h2>
</div>
<div class="page_class browsemembers_results" style="display:none;">
<?php if( count($this->friends) ): 
?>
  <ul class="row_start talent_start">
  
    <?php $i = 1;foreach( $this->friends as $friend ): ?>
      <li>
      <span class="lftimg_shadow"></span>
      <span class="rgtimg_shadow"></span>
         
        <?php 
		$friendpic	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($friend->getIdentity());
        $friendpic	=	($friendpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$friendpic;
        ?>
        <?php echo $this->htmlLink($friend->getHref(), $this->itemPhoto($friend, 'thumb.icon', $friend->getTitle()), array('class' => 'item_photo seao_common_add_tooltip_link', 'target' => '_parent', 'title' => $friend->getTitle(), 'rel'=> 'user'.' '.$friend->getIdentity()));?>
        <!--<a href="<?php echo $friend->getHref();?>" style="cursor:pointer;">
        	<span style="background-image:url(<?php echo $this->baseUrl().'/'.$friendpic;?>);" class="search_user_pic"></span>
		</a>-->
        <span class="user_name">
         <?php
            echo $this->htmlLink($friend->getHref(), strtoupper($friend->getTitle()), array("class"=>'profile_link')) ?>
        </span>
      </li>
       
    <?php 
       $i++;
     endforeach; ?>
  </ul>
<?php endif ?>
  </div>

<style>
.fancybox-skin
{ padding: 0 !important;}
 

.ynfeed_members_popup, .yncomment_members_popup {
    padding: 7px !important;
    
}


.ppl_vwed{ background-color: #eee;
    border-bottom: 1px solid #dbdbdb;
    border-radius: 0;
    color: #4a4a4a;
    font-family: roboto,sans-serif;
    font-size: 15px;
    font-weight: normal !important;
    padding: 9px 10px;
    text-transform: capitalize;
    
	margin: 0 !important;
	}
.thumb_profile{border-radius:0 !important;}
#global_content h2 {
    color: #3faee8;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 700;
    margin: 0;
    padding: 10px 20px;
    text-transform: capitalize;
}
</style>


<a id="like_members_profile" style="posituin:absolute;"></a>
<div class="ynfeed_members_popup">
	<div class="ynfeed_members_popup_content" id="likes_popup_content">
		<?php $count_user = count($this->friends);
				if(!empty($count_user)) {
					foreach( $this->friends as $friend ) { ?>
				<div class="item_member">
					<div class="item_member_thumb">
						<?php echo $this->htmlLink($friend->getHref(), $this->itemPhoto($friend, 'thumb.icon', $friend->getTitle()), array('class' => 'item_photo seao_common_add_tooltip_link', 'target' => '_parent', 'title' => $friend->getTitle(), 'rel'=> 'user'.' '.$friend->getIdentity()));?>
					</div>
					<div class="item_member_details">
						<div class="item_member_name">
							<?php  $title1 = $friend->getTitle(); ?>
							<?php  $truncatetitle = Engine_String::strlen($title1) > 20 ? Engine_String::substr($title1, 0, 20) . '..' : $title1?>
							<?php echo $this->htmlLink($friend->getHref(), $title1, array('title' => $friend->getTitle(), 'target' => '_parent', 'class' => 'seao_common_add_tooltip_link', 'rel'=> 'user'.' '.$friend->getIdentity())); ?>
						</div>
					</div>	
				</div>
				<?php	}
			 } else { ?>
			<div class='tip' style="margin:10px 0 0 0;"><span>
			 		<?php
			 			echo 'No one viewed your profile.';
			 		?>
			 </span></div>
			<?php } ?>
	</div>
</div>
