<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: profile.tpl 9984 2013-03-20 00:00:04Z john $
 * @author     John
 */
?>

<style type="text/css">

    .layout_main .layout_middle {
        width: 100% !important;
       background-color: #fff !important;
    }
    
</style>

<div class="layout_page_user_index_editpostclassified">
<div class="generic_layout_container layout_top">
<div class="generic_layout_container layout_middle">
<!-- <h2>Create Classifiend</h2> -->
 <!-- <div class="tabs">
      	<?php
          // Render the menu
            echo $this->navigation()
            ->menu()
            ->setContainer($this->navigation)
            ->render();
       ?>
      </div> -->
</div>
</div>

<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
   
<h2>Edit Classified</h2>
  <form method="post" enctype="multipart/form-data" action="/members/index/editpostclassified" id="editpostclassifiedform" class="form-control">
      <input type="hidden" name="classi_id" value="<?php echo $body=($this->userCls_id ?: '');?>">
<div class="classipostinfo">
<!-- <h3>Social Information</h3>
<span style="font-weight: 700; width: 100%; display: inline-block; border-bottom: 1px solid; height: 0.2em; margin-bottom: 15px;border-bottom:1px solid #dbdbdb;"></span> -->
    
 <div id="title-wrapper" class="form-wrapper">
  <div id="title-label" class="form-label">
  <label for="title" class="optional">Classified Title</label>
  </div>
  <div id="title-element" class="form-element">
  <input type="text" name="name" id="name" value="<?php echo $body=($this->title ?: '');?>" placeholder="Title" tabindex="1">
  </div>
</div>

<div id="description-wrapper" class="form-wrapper">
  <div id="description-label" class="form-label">
  <label for="description" class="optional">Classified Description</label>
  </div>
  <div id="description-element" class="form-element">
  <textarea name="cads_body" id="cads_body" tabindex="2" rows="6" placeholder="Description" ><?php echo $body=($this->description ?: '');?></textarea></div>
</div>

<div id="classi_image-wrapper" class="form-wrapper">
  <div id="classi_image-label" class="form-label">
  <label for="classi_image" class="optional">Classified URL</label>
  </div>
  <div id="classi_image-element" class="form-element">
   <input type="text" name="url" id="url" value="<?php echo $body=($this->url ?: '');?>" placeholder="URL" tabindex="3">

  </div>
</div> 
<div class="form-wrapper">
  <div id="classi_image-element" class="form-element">
     <input type="submit" name="edit_classified" id="edit_classified" value="SAVE">
  </div>
      
</div>
</div>

      
</form>
</div>

</div>

</div>

</div>

<script type="text/javascript">
var is_image = '<?php echo $this->photoDisplay ?>';
jQuery(document).ready(function(){
       jQuery('.loading_image').hide();
		
	});
jQuery('#edit_classified').click(function(){

	if(jQuery("#name").val()=="")
	{
		alert("Please fill the Classified Title!");
		jQuery("#name").focus();
		return false;
	}
	if(jQuery("#cads_body").val()=="")
	{
		alert("Please fill the Classified Description!");
		jQuery("#name").focus();
		return false;
	}
       jQuery('.loading_image').show();
	
    });
jQuery("form").bind("keypress", function (e) {
    if (e.keyCode == 13) {
       // $("#btnSearch").attr('value');
        //add more buttons here
        console.log('enter');
        return false;
    }
});

</script>

    <script type="text/javascript">
 
  window.addEvent('load', function(){
    var title_count;
    var body_count;
    $('name').addEvent('keyup', function()
    {
      nameTitle(this);
    });

    $('name').addEvent('blur', function()
    {
      nameTitle(this);
    });

    function nameTitle(thisValue){
      if($('validation_name')){
        document.getElementById("name-element").removeChild($('validation_name'));
      }

      if( thisValue.value != '' ){
        title = thisValue.value;
         var maxSizeTitle= 40;
          if(title.length>maxSizeTitle)
          { title = title.substring(0,maxSizeTitle);
            thisValue.value=title.substring(0,maxSizeTitle);
          }
       $('ad_title').innerHTML = '<a href="javascript:void(0);" >'+title +'</a>';
      }
      else
      { 
       $('ad_title').innerHTML = title = '<a href="javascript:void(0);" >'+'Example Ad Title'+'</a>';
      }
     
    }

    
   
    $('cads_body').addEvent('keyup', function(){
      var body='';
      if($('validation_cads_body')){
        document.getElementById("cads_body-element").removeChild($('validation_cads_body'));
      }
      if( this.value != '' ){
        body = this.value;
      }
      else{
        body =  'Example ad body text.';
      }
      var maxSize= 300;
      if(body.length<=maxSize)
        $('ad_body').innerHTML = body;
      else
      { $('ad_body').innerHTML = body.substring(0,maxSize);
        this.value=body.substring(0,maxSize);}
    });
	$('cads_body').addEvent('blur', function(){
      var body=''; 
      if($('validation_cads_body')){
        document.getElementById("cads_body-element").removeChild($('validation_cads_body'));
      }
      if( this.value != '' ){
        body = this.value;
      }
      else{
        body =  'Example ad body text.';
      }
      var maxSize= 300;
      if(body.length<=maxSize)
        $('ad_body').innerHTML = body;
      else
      { $('ad_body').innerHTML = body.substring(0,maxSize);
        this.value=body.substring(0,maxSize);}
    });

  });



</script>

<script type="text/javascript">
	jQuery("#image").on('change', function () {
 
     var countFiles = jQuery(this)[0].files.length;
 
     var imgPath = jQuery(this)[0].value;
     var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
     var image_holder = jQuery("#ad_photo");
     image_holder.empty();
 
     if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
         if (typeof (FileReader) != "undefined") {
 
             for (var i = 0; i < countFiles; i++) {
 
                 var reader = new FileReader();
                 reader.onload = function (e) {
                     jQuery("<img />", {
                         "src": e.target.result,
                             "class": "thumbimage"
                     }).appendTo(image_holder);
                 }
 
                 image_holder.show();
                 reader.readAsDataURL(jQuery(this)[0].files[i]);
             }
 
         } else {
             alert("It doesn't supports");
         }
     } else {
         alert("Select Only images");
     }
 });

</script>
<style type="text/css">
.thumbimage {
    float:left;
    width:398px;
    height: 120px;
    /*position:relative;
    padding:5px;*/
}
</style>