<div class="ynmember-review-user-avatar ynmember-clearfix">

	<div class="ynmember-review-user-main">
		<h3 class="ynmember-review-user-title">
			<?php
	         $onlineTable = Engine_Api::_() -> getDbtable('online', 'user');
	         $step = 900;
	         $select = $onlineTable -> select() -> where('user_id=?', (int)$this->subject() -> getIdentity()) -> where('active > ?', date('Y-m-d H:i:s', time() - $step));
	         $online = $onlineTable -> fetchRow($select);
	         if(is_object($online)): ?>
	     		<span class="ynmember-item-status online"></span>
	        <?php else:?>
	            <span class="ynmember-item-status off"></span>
	        <?php endif;?>

			<?php echo $this->htmlLink($this->subject()->getHref(), $this->subject()->getTitle(), array());?>
		</h3>		
        
		

		<div class="ynmember-review-user-rating">
			<span class="ynmember-review-user-reviews">
				<?php echo $this->translate(array('%s review', '%s reviews', count($this->reviews)), $this->locale()->toNumber(count($this->reviews))) ?>
			</span>

			<?php echo $this->partial('_review_rating_big.tpl', 'user', array('rate_number' => 0));?>
		</div>
	</div>
	
</div>