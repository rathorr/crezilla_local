<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Album
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Sami
 */
?>
<link rel="stylesheet" href="<?php echo $this->baseUrl().'/application/modules/Album/externals/styles/prettyPhoto.css';?>" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $this->baseUrl().'/application/modules/Album/externals/scripts/jquery.prettyPhoto.js'?>"></script>

  <?php $i=0;foreach( $this->paginator as $album ): ?>
  <ul id="profile_albums" class="thumbs gallery<?php echo $i;?>">
    <li>
      <a class="thumbs_photo img_hov_grey_active" href="<?php echo $this->baseUrl().'/albums/photo/view/album_id/'.$album->getIdentity().'/photo_id/'.$album->photo_id; ?>" rel="prettyPhoto">
       <span class="img_hov_grey">
       <p class="thumbs_info">
        <span class="thumbs_title">
          <?php echo $this->string()->chunk($this->string()->truncate($album->getTitle(), 45), 10); ?>
        </span>
        <span class="photo_count">
        <?php echo $this->translate(array('%s photo', '%s photos', $album->count()),$this->locale()->toNumber($album->count())) ?>
        </span>
      </p>
       </span>
       <span style="background-image: url(<?php echo $album->getPhotoUrl('thumb.main'); ?>); background-size: cover; background-position: 50% 50%; width:100%;"></span>
      </a>
      <?php 
      $data=Engine_Api::_()->getDbtable('photos', 'album')->getAlbumPhotos($album->album_id, $this->user->getIdentity());
       if($data){
       	foreach($data as $img){
            if($img['photo_id'] != $album->photo_id){
                echo '</li><li style="display:none;">';?>
                <a class="thumbs_photo" href="<?php echo $this->baseUrl().'/albums/photo/view/album_id/'.$album->getIdentity().'/photo_id/'.$album->photo_id; ?>" rel="prettyPhoto[gallery1]">
              		<img src="<?php echo $this->baseUrl().'/'.$img['storage_path']; ?>" style="width:160px !important; height:180px !important;"/>
               
              </a>
        	
        <?php }
         }
       }
     ?>
    </li>
     
    </ul>
  <?php $i++;endforeach;?>


