<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Timeline
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: upload.tpl 2012-02-01 16:58:20 mt.uulu $
 * @author     Mirlan
 */

?>
<?php if ($this->photo_id): ?>
<script type="text/javascript">
	var item_type = '<?php echo $this->item_type;?>';
    setTimeout(function () {
			if(item_type == 'cover'){
        		window.parent.changepic();
			}else{
				window.parent.changeprofilepic();
			}
			parent.Smoothbox.close();
    }, '100');
</script>
<?php else: ?>
<script type="text/javascript">
    var uploadWallPhoto = function () {
        document.getElementById('UploadUserCoverPhoto').submit();
        $(document.getElementById('Filedata-wrapper')).setStyles({'width':'400px', 'font-weight':'bold'});
        $(document.getElementById('Filedata-wrapper')).set('html', $(document.getElementById('photo-uploader')).get('html'));
    }
</script>

<div style="margin: 20px; text-align: center;">
    <div id="photo-uploader" class="hidden">
        <img class='loading_icon' src='application/modules/Core/externals/images/loading.gif'/>
        <?php echo $this->translate('Loading...')?>
    </div>

    <?php echo $this->form->render($this) ?>
</div>
<?php endif; ?>