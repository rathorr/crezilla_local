<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Timeline
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: remove-cover.tpl 2012-02-01 16:58:20 mt.uulu $
 * @author     Mirlan
 */

?>



<?php if ($this->del_cover): ?>
<script type="text/javascript">
    setTimeout(function () {
        	window.parent.changepic();
			parent.Smoothbox.close();
    }, '100');
</script>
<?php else: 
 echo $this->form->render($this); 
 endif; ?>