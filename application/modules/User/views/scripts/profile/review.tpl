<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Activity
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: share.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style type="text/css">
.com_form_error_validatin{border:0px !important; color:#fff !important; background-color: #dc2324 !important;}
.global_form div.form-element{float:none !important;}
</style>
<?php 
if($this->error){
	echo '<p>'.$this->error.'</p>';
}else{
	echo $this->form->render($this);  
}?>
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/jquery.min.js'?>"></script>

  <script type="text/javascript">
  var jQuery	=	$.noConflict();
  </script>
<script type="text/javascript">
function validatereview(){
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#title").val()) == ''){
			jQuery("#title").focus();
			jQuery("#title").addClass("com_form_error_validatin");
			return false;
	}
	if(jQuery.trim(jQuery("#summary").val()) == ''){
			jQuery("#summary").focus();
			jQuery("#summary").addClass("com_form_error_validatin");
			return false;
	}
}
</script>