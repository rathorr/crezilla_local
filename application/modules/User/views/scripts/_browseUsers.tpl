<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: _browseUsers.tpl 9979 2013-03-19 22:07:33Z john $
 * @author     John
 */
?>
<style type="text/css">

body#global_page_user-index-browse .browsemembers_criteria ul li select#profile_type{
	border: 1px solid #dbdbdb;
    height: 34px !important;
    padding: 6px;
    width: 98%;
}
body#global_page_user-index-browse .browsemembers_criteria ul li select{
    height: auto !important;
}
._manage_pages_section, ._community_ads, .member_list_section {
    padding: 0 15px!important;
}
._manage_pages_head, .member_list_head{
  float: left;
    width: 100%;
    padding-bottom: 8px;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 30px;
}
._manage_pages_head_content, .member_list_head_content {
    font-size: 18px;
    text-transform: uppercase;
}
._manage_pages_list_item_area{
  float: left;
  width: 100%;
  background: #fff;
}
.padnone{
  padding: 0px;
}
._manage_pages_list_item_block {
    background: #fff;
    padding: 15px 15px;
    float: left;
    border-bottom: 1px solid #e5e5e5;
}
._manage_pages_list_item_block:last-of-type, ._manage_pages_list_item_block:nth-last-of-type(2) {
    border-bottom: none;
}
._manage_pages_list_item_block:nth-child(even) {
    border-left: 1px solid #e5e5e5;
}

._item_block_content {
    float: left;
    width: 100%;
}
._manage_pages_item_img {
    float: left;
    height: 105px;
    max-width: 100px;
}
._manage_pages_item_img img{
  width: 100%;
}
._manage_pages_item_content {
    width: calc(100% - 105px);
    float: right;
    padding: 0 9px;
}
  
._manage_pages_title a {
    color: #333;
    font-size: 18px;
}
._manage_pages_date{
  font-size: 14px;
  color: #999999;
}
._manage_pages_details {
    font-size: 14px;
    color: #999;
}
._manage_pages_desc {
    font-size: 14px;
    color: #333;
}
._item_block_footer {
    float: left;
    width: 100%;
}
._manage_pages_action {
  float: left;
  width: 100%;
    margin-top: 30px;
}
._manage_pages_action a._manage_pages_action_btn {
    text-decoration: none;
    color: #333;
    text-transform: uppercase;
    border: 1px solid #333;
    width: 85px;
    height: 30px;
    display: inline-block;
    text-align: center;
    line-height: 30px;
    margin-right: 10px;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    -ms-transition: 0.3s ease-in-out;
    -o-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
._manage_pages_action a._manage_pages_action_btn:last-child{margin-right: 0px;}
._manage_pages_action a._manage_pages_action_btn:hover {
    color: #fa933c;
    border: 1px solid #fa933c;
}

.footer_section {
    padding: 40px 0;
    background: #e8e8e8;
    float: left;
    width: 100%;
    margin-top: 100px;
}
.footer-links a{
  text-decoration: none;
  color: #1f1f1f;
  font-size: 14px;
  text-transform: uppercase;   
}
.footer-links ul{
  margin: 0px;
  padding:0px;
  list-style: none;
}
.footer-links ul li{
  display: block;
  margin-bottom: 8px;
}
.footer_head{
  font-size: 14px;
  color: #1f1f1f;
  text-transform: uppercase;
  padding-bottom: 8px; 
}
.footer-social-content a {
    text-decoration: none;
    display: inline-block;
    margin-right: 10px;
    -webkit-transition: 0.5s ease-in-out;
    -moz-transition: 0.5s ease-in-out;
    -ms-transition: 0.5s ease-in-out;
    -o-transition: 0.5s ease-in-out;
    transition: 0.5s ease-in-out;
}
.footer-social-content a:hover{
  opacity: 0.5;
}
.footer-newsletter{padding-right: 0px;}
.footer-newsletter-content input.newsletter-input{
  height: 46px;
  line-height: 46px;
  border: 1px solid #000000;
  padding:0 20px;
  background: #fff;
  color: #1f1f1f;
  font-size:14px;
  text-transform: uppercase; 
  width: 65%;
  margin-right: -5px;
}
.footer-newsletter-content input.newsletter-submit{
  height: 47px;
  line-height: 46px;
  border: 1px solid #000000;
  padding:0 20px;
  background: #000;
  color: #fff;
  font-size:14px;
  text-transform: uppercase; 
  width: 35%;
  
}
.copyright_section {
    background: #d4d4d4;
    float: left;
    width: 100%;
    padding: 15px 0 10px;
}
.copyright_section span{text-transform: uppercase;}
.member_list_item_area{
  background: #fff;
  padding:5px 5px 8px;
  float: left;
  width: 100%;
  min-height: 350px;
  max-height: 350px;
}

._member_list_item_title a {
    color: #333;
    font-size: 18px;
}
._member_list_item_subtitle{
  font-size: 14px;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
  word-wrap: ;
  white-space: nowrap;
}
._member_list_item_action{
  float: left;
  width: 100%;
}
._member_list_item_action a{
  text-decoration: none;
  color: #cacaca; 
}
._member_list_item_review {
  font-size:14px;
  color: #ffc100; 
  float: left;
  width: 100%;
  padding-top: 10px;
}
._member_list_item_review .fa-star{
  cursor: pointer;
}
.padrgtnone{padding-left: 0px; float: left; color: #cacaca; text-align: left;}
.padlftnone{padding-left: 0px; text-align: right; float: right; }
._addfolderr_icn {
    width: 13px;
    height: 13px;
    display: inline-block;
    background: url(public/custom/images/add_folder_icn.png) no-repeat center center;
    background-size: cover;
    margin-right: 0;
    margin-left: 8px;
}
.member_list_item {
    margin-bottom: 30px;
}
.member_list_search{
  float: left;
  width: 100%;
  padding-bottom: 20px;
}
.member_list_search input.search-input{
  border-radius: 0;
  box-shadow: none;
  margin-bottom: 10px;
}
.member_list_search input.search-submit{
  background: #ff9e20 !important;
  color: #fff !important;
  border-radius: 0;
  box-shadow: none;
  -webkit-transition: 0.5s ease-in-out;
  -moz-transition: 0.5s ease-in-out;
  -ms-transition: 0.5s ease-in-out;
  -o-transition: 0.5s ease-in-out;
  transition: 0.5s ease-in-out;
}
.member_list_search input.search-submit:hover{
  background: #1f1f1f;
}
@media (max-width: 1024px){
  .footer-newsletter,
  .footer-social-links {padding-left:0px; clear:both; margin-top: 20px;}

}
@media (max-width: 786px){

  .footer-social-links{
    clear: none;
    margin-top: 0px;
  }
}
body{
  overflow-x:hidden!important; 
  font-family: 'Lato';
  font-size: 13px;
}
._banner_section{
  background: url('img/banner-img.jpg') no-repeat center center;
  background-size: cover;
  min-height: 200px;  
  position: relative;
  margin-bottom: 36px;
  padding: 0px;
} 
._form_area{
  position: absolute;
  height: 40px;
  top: 50%;
  width: 100%;
  margin:-20px 0 0;
}
.browsemembers_criteria ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
.browsemembers_criteria ul li{
  display: inline-block;  
  min-width: 200px;
  max-width: 200px;
  margin-right: 28px;
  margin-bottom: 11px;
}
._job_results,
._community_ads {
  padding: 0 15px!important;
}
._job_results_count {
    font-size: 18px;
    text-transform: uppercase;
}
._job_results_tab {
    text-align: right;
    font-size: 14px;
}
._job_results_tab ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
._job_results_tab ul li{
  display: inline-block;
}
._job_results_tab ul li a{
  padding: 8px 8px;
  text-decoration: none;
  color: #000;
  border-bottom: 2px solid transparent;
}
._job_results_tab ul li.active a{
  border-color:#f55731; 
}

._job_results_head{
  float:left;
  width: 100%;
  padding-bottom: 8px;
  border-bottom: 1px solid #e1e1e1;
  margin-bottom: 30px;
}
/*._job_results_list_item {
    background: #fff;
    padding: 10px 5px;
}*/

._job_list_item_block {
    background: #fff;
    padding: 10px 5px;
    float: left;
    width: 100%;
    margin-bottom: 30px;
}
._item_block_head{
  float: left;
  width: 100%;
  padding-bottom: 10px;
}
._createdby,
.viewedby {
    font-size: 14px;
    color: #888888;
}
._createdby_name{
  color: #000000;
}
._createdby{
  float:left;
}
.viewedby{
  float: right;
}
._item_block_content{
  float: left;
  width: 100%;
}
._job_item_img {
    float: left;
    height: 105px;
} 
._job_item_content{
  width: calc(100% - 105px);
    float: right;
    padding: 0 9px;
}
._job_title {
    padding-bottom: 10px;
}
._job_title a{
   color: #333;
    font-size: 18px;
 }
 ._job_details {
    font-size: 14px;
    color: #333;
    padding-bottom: 10px;
}
 ._job_details i {
  color: #cacaca;
  padding-right:3px;
 } 
 span._job_loc {
    padding-left: 25px;
}
._job_desc{
  font-size: 14px;
  color: #333;
}
._job_desc_title {
    font-size: 12px;
    color: #888;
}
._item_block_footer {
    float: left;
    width: 100%;
}
/*._addfolderr_icn{
  width: 18px;
  height: 18px;
  display: inline-block;
  background: url('img/add_folder_icn.png') no-repeat center center;
  background-size: cover;
  margin-right: 10px;
}*/
._item_block_footer a {
    float: left;
    width: 18px;
    margin-right: 10px;
    color: #cacaca;
    font-size: 18px;
}
._community_ads_area{
  background: #ffffff;
  border:1px solid #e6e7e8;
}
._community_ads_head {
    padding: 8px 0 3px;
}
._community_ads_head a{
  color: #666666;
  font-size: 14px;
  line-height: 1.4;
}
._community_ads_head a._createads{
  float: right;
  color: #f55731;
}
._community_ads_img{
  margin-bottom: 8px;
}
._community_ads_img img{
  width: 100%;
}
._community_ads_desc{
  font-size: 14px;
  color: #666666;
}
._community_ads_desc span{
  float: left;
  width: 100%;
}
._clear{clear:both;}
._community_ads_desc a{
  color: #666;
}
._community_ads_desc a._community_ads_title{
  color: #000;
  font-weight: bold;
}
._community_ads_items {
    border-bottom: 1px solid #e6e7e8;
    padding-top: 10px;
}
._community_ads_items:last-child{
    border-bottom: none;
    padding-bottom: 0px;
}

@media(max-width: 767px){
  #tog_button > img
  {
    display: none;
  }
  #core_menu_mini_menu_mobile
  {
    display: none;
  }
  .browsemembers_criteria ul li {
      display: inline-block;
      min-width: 145px;
      max-width: 145px;
      margin-right: 18px;
      margin-bottom: 11px;
  }
  ._form_area {
      margin: -70px 0 0;
  }
  ._job_results_count,
  ._job_results_tab {
      font-size: 12px;
  }
}
.generic_layout_container .layout_middle
{
  background-color: #e8e8e8;
}
form > input.search-input.form-control
/*#displayname*/
{
  color: #555;  
  background-color: #f2f2f2 !important;
}

form > input.form-control
{
  display: block;
    width: 100%;
    height: 40px !important;
    padding: 6px 12px;
    font-size: 14px !important;
    line-height: 1.42857143;
    background-image: none;
    border: 1px solid #ccc !important;
}

.img-responsive
{  
  width: 100%;
  max-height: 245px;
  object-fit: cover;
  min-height: 245px;
}
#browsemembers_results > div > div > div._member_list_section > div.member_list._manage_pages_list > div > div > div > div._member_list_item_detials > div > span
{
  float: right;
}
._member_list_head
{
  text-transform:uppercase;
}
.generic_layout_container .layout_left
{
  display: none;
}
#browsemembers_results > div > div > div._community_ads
{
 float: right;
}
.core_mini_profile span img.thumb_icon
{
  margin-right: 0px;
}
</style>
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/paginator.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->baseUrl().'/public/js/modernizr-1.7.min.js';?>"></script>
<script type="text/javascript">
jQuery(function($){
    $("a.core_mini_auth:contains('Sign In')").css('display','none');
  
    $('.paginationControl').find('a').on('click', function(){
		$('.loading_image').show();
        var link = $(this);
        var container = link.parents('#global_content');
		var type	=	link.parent().parent().parent().parent().attr('title');
        $.get(link.attr('href'), { format: 'html',type:type}, function(data){
			$('.loading_image').hide();
            container.html(data);
        }, 'html');
        return false;
    });
});
</script>
<style type="text/css">
.thumb_profile{border-radius:0 !important;}
</style>
<div class="page_class browsemembers_results">
<!-- TALENT LISTINGS START -->
<?php if($this->profile_type == '' || $this->profile_type == '1'){?>
<!-- <h3>
  <?php echo $this->translate(array('Member - %s match', 'Member - %s matches', $this->totalTalent),$this->locale()->toNumber($this->totalTalent)) ?>
</h3> -->
<?php $viewer = Engine_Api::_()->user()->getViewer();
//echo "viewer".$viewer; 
//echo "id".$viewer->getIdentity();die();?>

<?php if(count($this->talents) ){?> 
  <div class="container">
  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 _community_ads">
      <div class="member_list_search">
        <form action="/members" method="get">
          <input type="text" class="search-input form-control" name="displayname" id="displayname" placeholder="Search for Member">
          <!-- <button name="extra[done]"  id="extra-done" type="submit">Search</button> -->
          <input type="submit" name="extra[done]" id="extra-done" class="search-submit form-control" value="search">
        </form>
      </div>
    
    </div>  
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 _member_list_section">
      <div class="_member_list_head">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 _member_list_head_content">Member - <?php echo $this->totalTalent;?> matches </div>
        </div>
      </div>
      <div class=" member_list _manage_pages_list">
        <div class="row">
        <?php foreach( $this->talents as $talent ){
       
        $talentpic  = Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($talent->getIdentity());
        $talentpic  = ($talentpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$talentpic;

        $userating=Engine_Api::_()->getDbtable('reviews', 'user')->getAVGuserrating($talent->user_id); 

       // die();
        ?>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 member_list_item">
            <div class="member_list_item_area">
            <div class="col-md-6 col-sm-6 col-xs-6 padlftnone">
                  <i class="fa fa-eye" aria-hidden="true"></i> <?php echo $talent->view_count;?>
                </div>              
            <img src="<?php echo $talentpic;?>" class="img-responsive">
            <div class="_member_list_item_title"><a href="<?php echo $talent->getHref();?>" title=""><?php echo $talent->getTitle();?></a></div>
            <div class="_member_list_item_subtitle"><?php echo $userdesig=Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategoriesSearch($talent->user_id); 
            ?></div>
            <div class="_member_list_item_detials">
                <!-- <div class="_member_list_item_action">
                <div class="col-md-6 col-sm-6 col-xs-6 padlftnone">
                  <i class="fa fa-eye" aria-hidden="true"></i> 870
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 text-right padrgtnone">
                  <a href="#" title=""><i class="fa fa-heart" aria-hidden="true"></i></a>
                  <a href="#" title=""><i class="_addfolderr_icn" aria-hidden="true"></i></a>

                </div>
              </div> -->
              <div class="_member_list_item_review">
                
                <div class="col-md-6 col-sm-6 col-xs-6 text-right padrgtnone">
                  <a href="#" title="" ><i class="fa fa-heart" style="color: #cacaca;" aria-hidden="true"></i></a>
                  <?php
                  if($viewer->getIdentity() != 0){?>
                  <a href="/members/friends/shortlist/user_id/<?php echo $talent->user_id;?>?q=" class="smoothbox" title="Shortlist"><i class="_addfolderr_icn" aria-hidden="true"></i></a>
                  <?php } else { ?>
                  <a href="/login"  title="Shortlist"><i class="_addfolderr_icn" aria-hidden="true"></i></a>
                  <?php }?>

                </div>
                <span>
                <?php for($x=1;$x<=$userating;$x++) {?>
                          <i class="fa fa-star" aria-hidden="true"></i>
                <?php } 
                if ((int) $userating != $userating) {  ?>
                         <i class="fa fa-star-half" aria-hidden="true"></i>
                 <?php }
                     if($x !=1) { while ($x<=5) { ?>
                          <i class="fa fa-star-o" aria-hidden="true"></i>
                  <?php $x++; } }?>
                  <!-- <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i> -->
                </span>
              </div>
            
            </div>
            </div>
          </div>
         <?php }?>
                   
        </div>
      </div>
    </div>
    
  </div>
<?php }?>
<?php  if( $this->talents ):
    $pagination = $this->paginationControl($this->talents, null, null, array(
      'pageAsQuery' => true,
      'query' => $this->formValues,
    ));
  ?>

  
  <?php if( trim($pagination) ): ?>
    <div class='browsemembers_viewmore' id="browsemembers_viewmore" title="talent">
      <?php echo $pagination ?>
    </div>
  <?php endif ?>
<?php endif; ?>
<?php } ?>
<!-- TALENT LISTINGS END -->

</div>

<script >
jQuery(document).ready(function(){
  var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
if( isMobile.any() )
{
 var user_id="<?php echo $viewer->getIdentity();?>";
    if(user_id == 0)
    {     
    }
    else{
      jQuery("#core_menu_mini_menu_mobile").css('display','block');
      jQuery("#tog_button > img").css('display','block');
    }
}
else
{
    jQuery("#core_menu_mini_menu_mobile").css('display','none');
} 

    
    });
    </script>

