<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioPersonalDetailsController extends Engine_Content_Widget_Abstract {
	
	public function indexAction(){

		// Don't render this if not logged in
		$user = Engine_Api::_()->core()->getSubject();
	    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	    if( !$viewer->getIdentity() ) {
	      return $this->setNoRender();
	    }

	    $userdes	=	Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategories($user->getIdentity());
	$this->view->userdes	=$userdes;

	$designations=array();
	$designations2=array();
	$designations3=array();
	$client=array();
	$client2=array();
	$experience=array();
	$userprodesig	=	Engine_Api::_()->getDbtable('projects', 'experience')->getDesignationPro($user->getIdentity());
	foreach ($userprodesig as $key => $value) {
		# code...
		$designations[$key]=$value->desig_name;
		$client[$key]=$value->clientp;
	}
	$userexpdesig	=	Engine_Api::_()->getDbtable('experiences', 'experience')->getDesignationExp($user->getIdentity());
	foreach ($userexpdesig as $key => $value) {
		# code...
		$designations2[$key]=$value->desig_name;
		$client2[$key]=$value->client;
		if($key === 0){
			$experience[$key]= $value->desig_name. " at ". $value->client;
		}else
		{
			$experience[$key]= "Former ". $value->desig_name. " at ". $value->client;
		}
		
	}
	foreach ($userdes as $key => $value) {
		$designations3[$key]=$value['dasignatedcategory_title'];
	}
	$this->view->userdesigs	=	array_unique(array_merge($designations,$designations2,$designations3));
	$this->view->userclients	=	array_unique(array_merge($client,$client2));
	$this->view->keyclients	=	$client;
	$this->view->keyemployers	=	$client2;
	$this->view->expdesigs	=$designations;
	$this->view->profsummary=$profsummary =Engine_Api::_()->getDbtable('professionalsummarys', 'user')->getProfessionalsummarys($user->getIdentity());
	$this->view->experience=$experience;
	$userskills	=	Engine_Api::_()->getDbtable('skills', 'user')->userskills($user->getIdentity());
	$uskills=array();
	foreach ($userskills as $key => $uskill) {
		$uskills[$key]=$uskill->skill_title;
	}	
	$this->view->userskills	=	$uskills;
	$this->view->awards=$awards= Engine_Api::_()->getDbtable('awards','experience')->getawardlist($user->getIdentity());
	$this->view->unionmemberships=$unionmembership= Engine_Api::_()->getDbtable('unionmemberships','user')->getmembershiplist($user->getIdentity());
	$this->view->educations=$education= Engine_Api::_()->getDbtable('educations','education')->geteducationlist($user->getIdentity());

	$this->view->subject	=	$subject = Engine_Api::_()->core()->getSubject('user');

	$view = $this->view;
    $view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
	$this->view->fieldStructure = $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);

    if( count($fieldStructure) <= 1 ) { // @todo figure out right logic
         return $this->setNoRender();
       }
	   
	//echo '<pre>';print_r($fieldStructure);die;
   
    $valuesStructure = array();
    $valueCount = 0;
	
	$show_hidden = $viewer->getIdentity()
                 ? ($subject->getOwner()->isSelf($viewer) || 'admin' === Engine_Api::_()->getItem('authorization_level', $viewer->level_id)->type)
                 : false;
		
	
	foreach( $fieldStructure as $map )
       {
			// Get field meta object
			$field = $map->getChild();
			$value = $field->getValue($subject);

		    if( !$field || $field->type == 'profile_type' ) continue;
		    if( !$field->display && !$show_hidden ) continue;
		    $isHidden = !$field->display;
		  
		    // Get first value object for reference
		    $firstValue = $value;
		    if( is_array($value) && !empty($value) ) {
		    	$firstValue = $value[0];
		    }
			
			// Evaluate privacy
			if( $usePrivacy && !empty($firstValue->privacy) && $relationship != 'self' ) {
				if( $firstValue->privacy == 'self' && $relationship != 'self' ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'friends' && ($relationship != 'friends' && $relationship != 'self') ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'registered' && $relationship == 'everyone' ) {
				  $isHidden = true; //continue;
				}
			}
			
			
		    //print_r($firstValue);
		    $helper = new Fields_View_Helper_FieldValueLoop();
			$helper->view = Zend_Registry::get('Zend_View');
			$tmp = $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);
			if( $field->type != 'heading' && (!$isHidden || $show_hidden )) {
				$val[$field->label] = $tmp;
			}
		}
	$this->view->fieldStructure = $val;
	}
}