<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
<style type="text/css">
._pp_area_desc2
{
	text-transform: none;
    font-size: 14px;
    padding-left: 15px;
    position: relative;
    top: -1px;
}
._pp_summary_area.block_summary > ._pp_area_heading
{
	border-bottom: 1px solid #ccc;
}
/*#global_content > div > div.generic_layout_container.layout_main > div.generic_layout_container.layout_middle > div.generic_layout_container.layout_ynfeed_feed > h3:nth-child(1)
{
	display: none;
}*/
</style>
 <!-- First Block -->
 <div id="_portfolio_profile">
<div class="_pp_area">
	<div class="col-xs-12">
		<div class="_pp_summary_area block_summary">
			<div class="_pp_area_heading">
				<span class="_pp_area_heading_icn"><i class="fa fa-empire" aria-hidden="true"></i></span> Intro <span class="_pp_area_desc2">
				<?php
				//echo implode(" | ",array_slice($this->userdesigs, 0, 3));
				echo implode(" | ",$this->userdesigs);
				?>				
			</span>
			</div>
			<div class="_pp_area_desc" style="padding-top: 10px;">
				<p><?php echo $this->profsummary;?> </p>
			</div>
		</div>
		<div class="_pp_summary_area block_eudcation">
			<div class="_pp_area_heading">
				<span class="_pp_area_heading_icn"><i class="fa fa-suitcase" aria-hidden="true"></i></span> experience
			</div>
			<div class="_pp_area_desc">
			<?php foreach($this->experience as $exps){?>
			<p><?php echo $exps;?>.</p>
			<?php }?>				
			</div>
		</div>
		<div class="_pp_summary_area block_skill">
			<div class="_pp_area_heading">
				<span class="_pp_area_heading_icn"><i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Key Work Areas
			</div>
			<div class="_pp_area_desc">
				<p><?php echo implode(" | ",$this->userskills);?></p>
			</div>
		</div>
		<div class="_pp_summary_area block_eudcation">
			<div class="_pp_area_heading">
				<span class="_pp_area_heading_icn"><i class="fa fa-trophy" aria-hidden="true"></i></span> Awards
			</div>
			<div class="_pp_area_desc">
			<?php foreach($this->awards as $awd){
			//echo '<pre>'; print_r($awd->award); die();?>
			<p><?php echo trim($awd->category);?></p>
			<p><?php echo trim($awd->award);?>
				<?php echo ($awd->project) ?', '.$awd->project:'';?>
                <?php echo ($awd->position)?', '.$awd->position:'';?>
                <?php echo ($awd->year)?', '.$awd->year:'';?>
			</p>
			<?php }?>
				
			</div>
		</div>
		<div class="_pp_summary_area block_eudcation">
			<div class="_pp_area_heading">
				<span class="_pp_area_heading_icn"><i class="fa fa-users" aria-hidden="true"></i></span> Unions
			</div>
			<div class="_pp_area_desc">
			<?php foreach($this->unionmemberships as $mem){?>
			<p><?php echo trim($mem->unionmembership);?></p>
			<?php }?>
			</div>
		</div>
		<div class="_pp_summary_area block_eudcation">
			<div class="_pp_area_heading">
				<span class="_pp_area_heading_icn"><i class="fa fa-book" aria-hidden="true"></i></span> Educations
			</div>
			<div class="_pp_area_desc">
			<?php foreach($this->educations as $edu){?>
			<p><?php echo trim($edu->qualification);?>
			<?php echo ($edu->institute) ?', '.$edu->institute:'';?>
			<?php echo ($edu->year) ?', '.$edu->year:'';?>
				
			</p>
			<?php }?>
			</div>
		</div>
		<div class="_pp_summary_area block_eudcation">
			<div class="_pp_area_heading">
				<span class="_pp_area_heading_icn"><i class="fa fa-globe" aria-hidden="true"></i></span> Language
			</div>
			<div class="_pp_area_desc">
				<p><?php echo ($this->fieldStructure['Language'])? $this->fieldStructure['Language']:'';?></p>
			</div>
		</div>		
		<!-- <div class="_pp_summary_area block_eudcation">
			<div class="_pp_area_heading">
				<span class="_pp_area_heading_icn"><i class="fa fa-user" aria-hidden="true"></i></span> Personal
			</div>
			<div class="_pp_area_desc">
				<p>
					<span class="_pp_area_desc_personal_label">Mobile</span>
					<span class="_pp_area_desc_personal_detail">+91 9958025352</span>
				</p>
				<p>
					<span class="_pp_area_desc_personal_label">Email</span>
					<span class="_pp_area_desc_personal_detail">1987daydreamer@gmail.com</span>
				</p>
				<p>
					<span class="_pp_area_desc_personal_label">Date of Birth</span>
					<span class="_pp_area_desc_personal_detail">31 January 1990</span>
				</p>
				<p>
					<span class="_pp_area_desc_personal_label">Gender</span>
					<span class="_pp_area_desc_personal_detail">Male</span>
				</p>
				<p>
					<span class="_pp_area_desc_personal_label">Languages</span>
					<span class="_pp_area_desc_personal_detail">English, Hindi &amp; Punjabi</span>
				</p>
				<p>
					<span class="_pp_area_desc_personal_label">City</span>
					<span class="_pp_area_desc_personal_detail">Delhi, India</span>
				</p>
				<p>
					<span class="_pp_area_desc_personal_label">Relationship</span>
					<span class="_pp_area_desc_personal_detail">Married</span>
				</p>
				<p>
					<span class="_pp_area_desc_personal_label">Hobbies &amp; Interests</span>
					<span class="_pp_area_desc_personal_detail">Traveling &amp; Reading</span>
				</p>
			</div>
		</div> -->
	</div>
	<div class="col-xs-12 _pp_area_bottom text-center">
		<a href="#" title="">Read Less</a>
	</div>
</div>
</div>
<!-- End First Block -->