<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<?php //echo $this->itemPhoto($this->subject()) ?>
<?php $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($this->subject()->getIdentity());
if($pic == '')$pic	=	$this->baseUrl().'/application/modules/User/externals/images/nophoto_user_thumb_profile.png';
 ?>
 <!--<div id='profile_photo' style="background-image:url(<?php echo $pic; ?>);" class="wall_background set_profile">-->
<div id='profile_photo'  class="wall_background set_profile">
<?php echo $this->itemPhoto($this->subject()) ?>
<?php if($this->viewer()->getIdentity() == $this->subject()->getIdentity()){?>
<span>
 	<a href="<?php echo $this->baseUrl().'/members/profile/upload/item_type/dp/id/'.$this->subject()->getIdentity();?>" class="smoothbox custom_oupload_camera_icon_small">
	<span>Update Profile Picture</span></a>
 </span>
 <?php } ?>
</div> 
<script type="text/javascript">
function changeprofilepic(){
	jQuery.post('<?php echo $this->baseUrl()."/members/profile/getuserpic/item_type/dp/id/".$this->subject()->getIdentity();?>', function(resp){
			var pic	=	jQuery.trim(resp);
			if(pic == ''){
				pic = '<?php echo $this->baseUrl();?>/application/modules/User/externals/images/nophoto_user_thumb_profile.png';	
			}
			//jQuery('#profile_photo').css('background-image','url('+pic+')');
			jQuery('#profile_photo .thumb_profile').attr('src',pic);
			jQuery.post('<?php echo $this->baseUrl()."/members/profile/getuseralbums/id/".$this->subject()->getIdentity();?>', function(resp){jQuery('.albums_list').html(jQuery.trim(resp))});
			
		});
}
</script>
