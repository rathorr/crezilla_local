<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/job_search.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/manage_pages.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/portfolio-tab.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/fonts.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/slick.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/home-3.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/portfolio.css" />
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
 <style type="text/css">
   ._community_ads_items
   {
    min-height: 236px;
  }
 </style>
<!-- Eighth Block -->
<div class="_ps_common_heading">SPONSERED ADS</div>
<div class="_pp_area _ps_pas ads_slicker">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 _community_ads_items">
    <div class="_community_ads_img"><a href=""><img src="application/modules/User/externals/portfolio/img/add_1.jpg" alt=""></a></div>
    <div class="_community_ads_desc">
    <span><a class="_community_ads_title _clear" href="#" title="">Build Your Store on FB!</a></span>
    <span><a class="_community_ads_web_link _clear" href="#" title="">www.shopify.com</a></span>
      <p class="_clear">Today isn't just another day.</p>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 _community_ads_items">
    <div class="_community_ads_img"><a href=""><img src="application/modules/User/externals/portfolio/img/add_2.jpg" alt=""></a></div>
    <div class="_community_ads_desc">
    <span><a class="_community_ads_title _clear" href="#" title="">Build Your Store on FB!</a></span>
    <span><a class="_community_ads_web_link _clear" href="#" title="">www.shopify.com</a></span>
      <p class="_clear">Today isn't just another day.</p>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 _community_ads_items">
    <div class="_community_ads_img"><a href=""><img src="application/modules/User/externals/portfolio/img/add_3.jpg" alt=""></a></div>
    <div class="_community_ads_desc">
      <span><a class="_community_ads_title _clear" href="#" title="">Urban Ladder</a></span>
      <span><a class="_community_ads_web_link _clear" href="#" title="">www.urbanladder.com</a></span>
      <p class="_clear">Love these products? Make a hassle-free purchase on Urban Ladder...</p>
    </div>
  </div>
</div>
<!-- End Eighth Block -->
<script type="text/javascript">
  jQuery(document).ready(function(){
    
    jQuery('.ads_slicker').slick({
      slidesToShow: 2,
        slidesToScroll: 1
    });
  });
</script>