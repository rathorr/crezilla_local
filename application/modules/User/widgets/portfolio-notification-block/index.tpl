<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
 <style type="text/css">
#core_menu_mini_menu > ul.header_menu_class > li.menu_core_mini.core_mini_profile > a > span > img
{
	width: auto;
}
.custom_oupload_camera_icon {
	position: absolute !important; 
}
._pb_img img._pb_img_shadow
{
	position: relative !important;
}
#global_content > div > div.generic_layout_container.layout_main > div.generic_layout_container.layout_middle
{
	width: 50%;
	background-color: #E9EAED;
}
</style>
<div id="_portfolio_social">
<!-- First Block -->
<div class="_pp_area ps_top_block">
	<ul>
		<li class="first">
			<a href="#">
				<i class="fa fa-picture-o" aria-hidden="true"></i>
				<span class="ps_top_block_label">Share an Update / Portfolio</span>
			</a>
		</li>
		<li>
			<a href="#">
				<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				<span class="ps_top_block_label">Post a Job</span>
			</a>
		</li>
		<li class="last">
			<a href="#" class="ps_top_block_new_user">
				<i class="fa fa-user-plus" aria-hidden="true"></i>
				<span class="ps_top_block_new_user_count _ps_counter">3</span>
			</a>
			<a href="#" class="ps_top_block_group">
				<i class="fa fa-users" aria-hidden="true"></i>
				<span class="ps_top_block_group_count _ps_counter">3</span>
			</a>
			<a href="#" class="ps_top_block_notification">
				<i class="fa fa-globe" aria-hidden="true"></i>
				<span class="ps_top_block_notification_count _ps_counter">3</span>
			</a>
		</li>
	</ul>
</div>
</div>
<!-- End First Block -->