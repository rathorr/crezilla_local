<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioYoumayknowController extends Engine_Content_Widget_Abstract {
	
	protected $_childCount;
	public function indexAction(){

		// Don't render this if not logged in
	    $viewer = Engine_Api::_()->user()->getViewer();
		$params = $this->_getAllParams();
		if(!empty($params['itemCountPerPage']))
		{
			$limit = $params['itemCountPerPage'];
		}
		else
		{
			$limit = 6;
		}		
		// Don't render this if friendships are disabled
	    if( !Engine_Api::_()->getApi('settings', 'core')->user_friends_eligible ) {
	      return $this->setNoRender();
	    }
		if(!$viewer -> getIdentity())
		{
			return $this->setNoRender();
		}
		$tableUser = Engine_Api::_() -> getItemTable('user');
		$select = $tableUser -> select() -> where('user_id <> ?', $viewer->getIdentity()) -> where('enabled = 1') -> where('verified = 1') -> where('approved = 1') -> order(new Zend_Db_Expr(('rand()')));
		$list_users = $tableUser -> fetchAll($select);
	    $list_show_users = array();
	    
	    $memtable = Engine_Api::_()->getDbTable('membership', 'user');
    	$memName = $memtable->info('name'); 
	
		//$table = Engine_Api::_()->getItemTable('education');
    	$resource_id=$viewer->getIdentity();
    	//echo $resource_id;
    	$memselect = $memtable->select("user_id")
					->where("resource_id = $resource_id", 1);
					//->where("$memName.row_status = 1", 1)
					//->order("$memName.creation_date desc");
		 // get the data
		 $usermem= $memtable->fetchAll($memselect);
		 $existuserrequest=array();
		 foreach ($usermem as $key => $value) {
		 	$existuserrequest[$key]= $value['user_id'];
		 }
		foreach($list_users as $subject)
		{
			if(count($list_show_users) >= $limit)
			{
				break;
			}
		    // Diff friends
		    $friendsTable = Engine_Api::_()->getDbtable('membership', 'user');
		    $friendsName = $friendsTable->info('name');
		
		    // Mututal friends/following mode
		    $col1 = 'resource_id';
		    $col2 = 'user_id';
		
		    $select = new Zend_Db_Select($friendsTable->getAdapter());
		    $select
		      ->from($friendsName, $col1)
		      ->join($friendsName, "`{$friendsName}`.`{$col1}`=`{$friendsName}_2`.{$col1}", null)
		      ->where("`{$friendsName}`.{$col2} = ?", $viewer->getIdentity())
		      ->where("`{$friendsName}_2`.{$col2} = ?", $subject->getIdentity())
		      ->where("`{$friendsName}`.active = ?", 1)
		      ->where("`{$friendsName}_2`.active = ?", 1)
		      ;
		    // Now get all common friends
		    $uids = array();
		    foreach( $select->query()->fetchAll() as $data ) {
		      $uids[] = $data[$col1];
		    }
		
		    // Do not render if nothing to show
		    if( count($uids) > 0 ) {
		      if(!$subject->membership()->isMember($viewer))
			  {
			      $list_show_users[] = array(
			      	'user_id' => $subject->getIdentity(),
			      	'number_mutual' => count($uids),
				  );
			  }
		    }
	    }
		if(count($list_show_users) <= 0)
		{
			foreach($list_users as $subject)
			{
				if(count($list_show_users) >= $limit)
				{
					break;
				}
				if(!$subject->membership()->isMember($viewer))
			  	{
			  		$list_show_users[] = array(
			      	'user_id' => $subject->getIdentity());
				}
			}
		}
		
		$this->view->existuserrequest=$existuserrequest;
		$this->view->list_show_users = $list_show_users;
	    
	}
	public function getChildCount()
	  {
	    return $this->_childCount;
	  }
}