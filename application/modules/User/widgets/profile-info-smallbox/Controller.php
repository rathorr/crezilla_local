<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_ProfileInfoSmallboxController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    // Don't render this if not authorized
    if( !Engine_Api::_()->core()->hasSubject() ) {
      $subject = Engine_Api::_()->user()->getViewer();
      if( null !== $subject )
      {
          Engine_Api::_()->core()->setSubject($subject);
      }
	}/*else{
		$subject = 	 Engine_Api::_()->core()->getSubject('user');
	}*/
	
    // Member type
    $this->view->subject	=	$subject = Engine_Api::_()->core()->getSubject('user');
	
    $fieldsByAlias = Engine_Api::_()->fields()->getFieldsObjectsByAlias($subject);
	
   $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);
	  
	   foreach( $fieldStructure as $map )
	   {
			// Get field meta object
			$field = $map->getChild();
			$value = $field->getValue($subject);
		   
		    if( !$field || $field->type == 'profile_type' ) continue;
		    if( !$field->display && !$show_hidden ) continue;
		    $isHidden = !$field->display;
		   
			// Get first value object for reference
			$firstValue = $value;
			if( is_array($value) && !empty($value) ) {
				$firstValue = $value[0];
			}
			
			$helper = new Fields_View_Helper_FieldValueLoop();
			$helper->view = Zend_Registry::get('Zend_View');
			$tmp = $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);
			if($field->type != 'heading') {
				/*$val[$field->label] =  $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);*/
				$val[$field->label] =  $tmp;
			}
			
		}
		
		$this->view->fieldStructure	=	$val;
		if( !empty($fieldsByAlias['profile_type']) )
		{
		  $optionId = $fieldsByAlias['profile_type']->getValue($subject);
		  //echo '<pre>'; print_r($optionId);
		  if( $optionId ) {
			$optionObj = Engine_Api::_()->fields()
			  ->getFieldsOptions($subject)
			  ->getRowMatching('option_id', $optionId->value);
			if( $optionObj ) {
			
			  $this->view->memberType = $optionObj->label;
			}
		  }
		}

    
  }
}