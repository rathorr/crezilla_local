<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 10158 2014-04-10 19:07:53Z lucas $
 * @author     John
 */
?>

<ul>
  <li class="profile-name"><?php echo $this->subject->displayname;?></li>
  <?php if( !empty($this->memberType) ): ?>
  <li>
    <?php echo ($this->fieldStructure['Category'])?$this->fieldStructure['Category']:$this->memberType; ?>
  </li>
  <?php endif; ?>
    
  <li>
    <?php echo $this->translate('Lives in:'); ?>
     <?php echo $this->fieldStructure['Location']; ?>
  </li>
  <li>
    <?php echo $this->translate('Member since:') ?>
    <?php echo date('F d, Y', strtotime($this->subject->creation_date)); ?>
  </li>

</ul>


<script type="text/javascript">
  $$('.core_main_user').getParent().addClass('active');
</script>
