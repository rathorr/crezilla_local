<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<script type="text/javascript" src="<?php echo $this->baseUrl()?>/externals/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl()?>/externals/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" charset="utf-8">
					jQuery(document).ready(function(){
						jQuery('.fancybox').fancybox();
					});
			</script>
<div id='profile_status'>
  <!--<h2>
    <?php echo 'Skills/Endorsement'; ?>
	<br>
  </h2>-->
  <?php 
  //echo '<pre>'; print_r($this->skills); echo '</pre>';
  if($this->skills){$i=0;
        foreach($this->skills as $skill){?>
        	
        
            <p id='<?php echo $skill['skill_id'];?>'>
            
            <?php 
            $user_arr	=	'';
            $endore_arr	=	'';
            $endorse	=	$skill['endorse'];
            $endore_id	=	'';
            foreach($endorse as $end){
                $user_arr[]		=	$end['user_id'];
                $endore_arr[]	=	$end['endorsement_id'];
            }?>
           
           <?php if($skill['total_endorse']==0){?>
           	<a href="javascript:void(0);" class="no_endorse">
            	<span class="endorse_count"><?php echo $skill['total_endorse'];?></span>
            </a>
           <?php } else{ ?>
            <a href="<?php echo $this->baseUrl().'/members/edit/getendorsedby/endorse_by/'.implode(',',$user_arr);?>" class="fancybox fancybox.iframe">
            	<span class="endorse_count"><?php echo $skill['total_endorse'];?></span>
            </a>
            <?php } ?>
            <span class='skill'><?php echo $skill['skill_title'];?></span>
            <?php 
            //echo '<pre>'; print_r($user_arr); echo '</pre>';
           // echo '<pre>'; print_r($endore_arr); echo '</pre>';
            if($this->viewer->getIdentity() != $this->subject->getidentity()){
                if(in_array($this->viewer->getIdentity(), $user_arr)){
                    $endore_id = array_search($this->viewer->getIdentity(), $user_arr);
                    echo "<a href='javascript:void(0);' class='remove_endorse' id='".$endore_arr[$endore_id]."'>-</a>";
                }else{
                    echo '<a href="javascript:void(0);" class="add_endorse">+</a>';
                    
                }
            }
            ?>
            <input type="hidden" class="endorse_by" value="<?php echo implode(',',$user_arr);?>" />
            
            <?php $i++;
        }
    }
    if($i==0){ ?>
    	<span>No skills added.</span>
    <?php } ?>
</div>
<div class="show_endor_user" style="display:none;">
	
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(document).on('click','.add_endorse', function(){
		jQuery('.loading_image').show();
		//var skill_parent	=	jQuery.trim(jQuery(this).parent().html());
		var skill_id	=	jQuery.trim(jQuery(this).parent().attr("id"));
		var skill_parent	=	jQuery(this).parent();
		jQuery.post("<?php echo $this->baseUrl().'/members/edit/addendorse'; ?>",{skill_id:skill_id,owner_id:<?php echo $this->subject->getIdentity();?>,user_id:'<?php echo $this->viewer->getIdentity();?>',type:'add'}, function(response){
			jQuery('.loading_image').hide();
			var resp	=	response.split('*');
			jQuery(skill_parent).find('.endorse_count').html(jQuery.trim(resp[0]));
			jQuery(skill_parent).find('.add_endorse').remove();
			jQuery(skill_parent).append('<a href="javascript:void(0);" class="remove_endorse" id="'+resp[1]+'">-</a>');
		});	
	});
	
	jQuery(document).on('click','.remove_endorse', function(){
		//var skill_parent	=	jQuery.trim(jQuery(this).parent().html());
		jQuery('.loading_image').show();
		var skill_id	=	jQuery.trim(jQuery(this).parent().attr("id"));
		var endorse_id	=	jQuery(this).attr("id");
		var skill_parent	=	jQuery(this).parent();
		jQuery.post("<?php echo $this->baseUrl().'/members/edit/addendorse'; ?>",{skill_id:skill_id,owner_id:<?php echo $this->subject->getIdentity();?>,type:'delete',endorse_id:endorse_id}, function(response){
			jQuery('.loading_image').hide();
			var resp	=	response.split('*');
			jQuery(skill_parent).find('.endorse_count').html(jQuery.trim(resp[0]));
			jQuery(skill_parent).find('.remove_endorse').remove();
			jQuery(skill_parent).append('<a href="javascript:void(0);" class="add_endorse">+</a>');
		});	
	});
	
	/*jQuery(document).on('click','.endorse_count', function(){
			var endorse_by	=	jQuery(this).parent().find('.endorse_by').val();
			jQuery.post("<?php echo $this->baseUrl().'/members/edit/getendorsedby'; ?>",{endorse_by:endorse_by}, function(resp){
				jQuery('.show_endor_user').html(jQuery.trim(resp));
				jQuery('.show_endor_user').css("display", "block");
			});
	});*/
});
</script>