<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<div class="col-md-12 col-sm-12 col-md-xs index-right-job-posted">
<div class="col-md-7 col-sm-5 col-xs-8 index-right-text-lfet"><span style="font-size: 15px; color: rgb(76, 76, 76); font-weight: 600;">People you may know?</span></div>
<div class="col-md-5 col-sm-7 col-xs-4 classified_header text-right"><a class="fmp" href="members"><i class="fa fa-search" aria-hidden="true"></i>
 Find more</a></div>
</div>

<div class="container col-md-12 col-sm-12 col-xs-12">
		  <?php foreach( $this->list_show_users as $arr_item ): 
  			$user_id = $arr_item['user_id'];
			$number_mutual = $arr_item['number_mutual'];
  			$user = Engine_Api::_() -> getItem('user', $user_id); 
  			if (!in_array($user_id, $this->existuserrequest, TRUE)):

          ?>
          
          
          <div class="col-md-4 col-sm-3 col-xs-4 nopadding">
			<div class="col-md-12 peopl_conect" style="padding:5px 0px;">
				
				  <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon', ''), array('class' => 'newestmembers_thumb')) ?></div>	
				  
				  <div class="col-md-12" style=" ">
					  <div class='newestmembers_info feed_item_photo' style="margin-right: 0px;">
						<div class='newestmembers_name feed_item_body text-center' >
						  <?php echo $this->htmlLink($user->getHref(), $this->string()->chunk($this->string()->truncate($user->getTitle(), 11), 10)) ?>
							
						</div>
				</div>
				  </div>
				  
                    <!-- add friend button -->
                <div class="col-md-12 text-center" style="padding:1px 0 8px;">
                            <a class="button_blue smoothbox" style=" display: inline-block;" href="<?php echo $this->baseUrl().'/members/friends/add/user_id/'.$user_id;?>">Connect</a>
                </div>
				  
            </div>
          
          
          <?php endif; endforeach;?>
</div>

