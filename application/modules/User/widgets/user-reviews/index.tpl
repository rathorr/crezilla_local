<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style type="text/css">
.rating_star_big_disabled {
    background-image: url("<?php echo $this->baseUrl();?>/application/modules/User/externals/images/star_big_disabled.png");
}
.ynmember_rating_star_generic {
    background-repeat: no-repeat;
    cursor: pointer;
    display: inline-block;
    float: left;
    font-size: 1px;
    height: 24px;
    width: 24px;
}
.rating_star_big {
    background-image: url("<?php echo $this->baseUrl();?>/application/modules/User/externals/images/star_big.png");
}
</style>
<script type="text/javascript">
jQuery(document).ready(function(){
	var t	=	jQuery('#main_tabs').find('.active').html();
	var review_anchor	=	jQuery('#main_tabs').find('.tab_layout_user_user_reviews').find('a');
	
	if(jQuery.trim(t) == ''){
		setTimeout(function(){jQuery('#main_tabs').find('.tab_layout_user_profile_complete_fields').find('a').click();}, 1000);
		
	}
});
function change_review_status(status, type, review_id){
			var review_id	=	review_id;
			jQuery('.loading_image').show();
			jQuery.post('<?php echo $this->baseUrl();?>/members/edit/changereviewstatus',{status:status,review_id:review_id, type:type},function(resp){
				if(type == 'is_deleted'){
					jQuery('#main_row_'+review_id).remove();	
				}else{
					jQuery('#review_status_'+review_id).html(resp);
				}
				jQuery('.loading_image').hide();
			});
 	
}
</script>

  <?php $reviews	=	$this->reviews;
  if($reviews && count($reviews)>0){
  	foreach($reviews as $rev){?>
	<div class="reviews-section" id="main_row_<?php echo $rev['review_id'];?>">
<div id='reviews'>
    	<div class="main_div">
        	<div class="img">
            	<?php 
                	$user = Engine_Api::_()->getItem('user', $rev['reviewer_id']);
                    echo $this->itemPhoto($user, 'thumb.icon');
                ?>
            </div>
        	<div class="name">
            	<?php echo $rev['reviewer_name'];?>
            </div>
            <span class="review_time">
            	<?php echo $this->timestamp($rev['review_date']);?>
            </span>
        </div>
        
        <div class="ynmember-review-item-rate-item ynmember-clearfix">
					<?php echo $this->partial('_review_rating_big.tpl', 'user', array('rate_number' => $rev['rating']));?>
				</div>
        
        <div class="review_title">
        	<?php echo $rev['title'];?>
        </div>
        <div class="review">
        	<?php echo $rev['summary'];?>
        </div>
        <?php if($rev['user_comment']){?>
            <div class="user_comment">
                <?php echo $rev['user_comment'];?>
            </div>
        <?php } ?>
		</div>
        
        
        
        
        <!--<?php if($this->is_owner){?>
        <div class="publish_button" id="review_status_<?php echo $rev['review_id'];?>">
        	<?php if($rev['is_approved'] == '0') { ?><a href="javascript:void(0)" onclick="change_review_status('1', 'is_approved' , <?php echo $rev['review_id'];?>)" class="deactive_bt" style="cursor:pointer;font-weight: normal;">Un-published</a><?php }
                if($rev['is_approved'] == '1') {?>
                <a href="javascript:void(0)" onclick="change_review_status('0', 'is_approved' , <?php echo $rev['review_id'];?>)" class="active_bt" style="cursor:pointer;font-weight: normal;">Published</a><?php } ?>
            
        </div>
        
        <div class="delete_button">
    	<a href="javascript:void(0)" onclick="change_review_status('0', 'is_deleted' , <?php echo $rev['review_id'];?>)" class="delete_bt" style="cursor:pointer;font-weight: normal;">Delete</a>
        </div>
        <?php } ?>-->
</div>
   <?php 
   	}  
   }else{
  	echo '<div class="no_review">No Reviews yet.</div>';
  }
  ?>
