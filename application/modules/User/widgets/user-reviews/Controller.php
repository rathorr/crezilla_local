<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_UserReviewsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	// Don't render this if not logged in
	$subjectId = Engine_Api::_()->core()->getSubject('user')->getIdentity();
    $viewer = Engine_Api::_()->user()->getViewer();
	
    if($viewer->getIdentity() == $subjectId) {
      //return $this->setNoRender();
    }
	
	$this->view->reviews = $reviews = 	Engine_Api::_()->getDbtable('reviews', 'user')->getReviews($subjectId);
	
	// Add count to title if configured
    if( $this->_getParam('titleCount', false) && count($reviews) > 0 ) {
      $this->_childCount = count($reviews);
    }
	$this->view->is_owner	=	false;
	if($viewer->getIdentity() == $subjectId) {
		$this->view->is_owner	=	true;	
	}
  }
  
   public function getChildCount()
  {
    return $this->_childCount;
  }
}