<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */

class User_Widget_ListSignupsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    $table = Engine_Api::_()->getDbtable('users', 'user');
    $userName = $table->info('name');
	
	$friendTable1 = Engine_Api::_()->getDbTable('friendship', 'user');
    $friendName1 = $friendTable1->info('name');
	
	$friendTable2 = Engine_Api::_()->getDbTable('friendship', 'user');
    $friendName2 = $friendTable2->info('name'); 
	
   //EnginegetUserFriendsList
   $friends = Engine_Api::_()->getDbtable('friendship', 'user')->getUserFriendsList(Engine_Api::_()->user()->getViewer()->getIdentity());
   $friendlist	=	'';
   if($friends){
	   $i=1;
		foreach($friends as $frnd){
			$friendlist	.=	$frnd['friend_id'];	
			if($i<count($friends))$friendlist	.=	',';	
			$i++;
		}   
	}
	
	
	$user_id	=	Engine_Api::_()->user()->getViewer()->getIdentity();
	/*echo $select = $table->select("$userName.*")
						->setIntegrityCheck(false)
                        ->where("$userName.search = ?", 1)
						->where("$userName.enabled = ?", 1)
	  					->where("$userName.user_id != ?", $user_id)
						//->where("$friendName1.status != ?", 1)
						->where("$friendName1.user_id NOT IN ($user_id) AND $friendName1.friend_id NOT IN ($user_id) AND $friendName1.status !=1")
						//->where("$friendName2.status != ?", 1)
       					->joinLeft($friendName1, "$friendName1.user_id = $userName.user_id")
                        ->order("$userName.creation_date DESC");
						die;*/
	 $select = $table->select()
      ->where('search = ?', 1)
      ->where('enabled = ?', 1)
	  ->where("user_id != ?", Engine_Api::_()->user()->getViewer()->getIdentity());
	  if($i>1) $select->where("user_id NOT IN (".$friendlist.")");
	  $select->order("user_id desc")
	  		  ->limit(3);
      ;
  
   //echo $select;die;
    $this->view->paginator = $paginator = Zend_Paginator::factory($select);

    // Set item count per page and current page number
    $paginator->setItemCountPerPage($this->_getParam('itemCountPerPage', 3));
    $paginator->setCurrentPageNumber($this->_getParam('page', 1));

    // Do not render if nothing to show
    if( $paginator->getTotalItemCount() <= 0 ) {
      return $this->setNoRender();
    }
  }

  public function getCacheKey()
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    $translate = Zend_Registry::get('Zend_Translate');
	$locale = Zend_Registry::get('Locale');
    return $viewer->getIdentity() . $translate->getLocale() . $locale->toString();
  }

  public function getCacheSpecificLifetime()
  {
    return 120;
  }
}