<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 10167 2014-04-15 19:18:29Z lucas $
 * @author     John
 */
?>
<span class="ppl_umay">People you may know? <a href="javascript:void(0);" style="cursor:default;">Connect with them.</a></span>
<div class="container col-md-12">
	

	
		  <?php foreach( $this->paginator as $user ): ?>
		  <div class="col-md-4 nopadding">
			<div class="col-md-12 peopl_conect">
				
				  <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon', $user->getTitle()), array('class' => 'newestmembers_thumb')) ?></div>	
				  
				  <div class="col-md-12">
					  <div class='newestmembers_info feed_item_photo' style="margin-right: 0px;">
						<div class='newestmembers_name feed_item_body text-center' >
						  <?php echo $this->htmlLink($user->getHref(), $user->getTitle()) ?>
							
						</div>
				</div>
				  </div>
				  
				  <div class="col-md-12  text-center">
                  <?php $talent=Engine_Api::_()->getDbtable('users', 'user')->getUserTalent($user->getIdentity());
                  echo ($talent)?$talent:'Talent';?>
                  </div>
						
                            <div class="col-md-12 text-center"><a class="button_blue" style=" display: inline-block;" href="<?php echo $user->getHref();?>">Connect</a></div>
				  
					</div>
				<?php endforeach; ?>
			
			<div class="col-md-12" style="display:block; padding-bottom:10px; padding-top:10px;">
		
		<span class="col-md-12 classified_header text-right"><a href="members">Find more people you know</a></span>
	</div>	

</div>
