<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 10167 2014-04-15 19:18:29Z lucas $
 * @author     John
 */
?>
<span class="ppl_umay">People you may know? Connect with them.</span>
<div class="container col-md-12">
		  <?php foreach( $this->paginator as $user ): ?>
		  <div class="col-md-4 nopadding">
			<div class="col-md-12 peopl_conect" style="padding:10px 0px;">
				
				  <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon', $user->getTitle()), array('class' => 'newestmembers_thumb')) ?></div>	
				  
				  <div class="col-md-12" style="padding-bottom:5px;">
					  <div class='newestmembers_info feed_item_photo' style="margin-right: 0px;">
						<div class='newestmembers_name feed_item_body text-center' >
						  <?php echo $this->htmlLink($user->getHref(), $user->getTitle()) ?>
							
						</div>
				</div>
				  </div>
				  
				  <div class="col-md-12  text-center" style="padding-bottom:5px;">
                  <?php //$talent=Engine_Api::_()->getDbtable('users', 'user')->getUserTalent($user->getIdentity());
                  //echo ($talent)?$talent:Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType($user->getIdentity());?>
                  </div>
						
                            <div class="col-md-12 text-center" style="padding:5px 0 15px;">
                            <a class="button_blue smoothbox" style=" display: inline-block;" href="<?php echo $this->baseUrl().'/members/friends/addfriend/user_id/'.$user->getIdentity();?>">Connect</a></div>
				  
					</div>
				<?php endforeach; ?>
			
			<div class="col-md-12" style="display:block; padding-bottom:10px;">
		
		<span class="col-md-12 classified_header text-right"><a class="fmp" href="members">Find more people you know</a></span>
	</div>	

</div>
