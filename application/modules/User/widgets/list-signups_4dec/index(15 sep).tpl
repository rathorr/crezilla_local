<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 10167 2014-04-15 19:18:29Z lucas $
 * @author     John
 */
?>
<div class="container">
	<div class="col-md-12">
		<span class="col-md-9 classified_header">Classified</a></span>
		<span class="col-md-3 classified_header"><a href="members">View All</a></span>
	</div>

	
		  <?php foreach( $this->paginator as $user ): ?>
		  <div class="col-md-12">
			<div class="col-md-3">
				
				  <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon', $user->getTitle()), array('class' => 'newestmembers_thumb')) ?></div>	
				  
				  <div class="col-md-9">
					  <div class='newestmembers_info feed_item_photo'>
						<div class='newestmembers_name feed_item_body'>
						  <?php echo $this->htmlLink($user->getHref(), $user->getTitle()) ?>
							<span class='newestmembers_date'>
							  <?php echo $this->timestamp($user->creation_date) ?>
							</span>
						</div>
						
					  </div>
				  </div>
					</div>
				<?php endforeach; ?>
			
				

</div>
