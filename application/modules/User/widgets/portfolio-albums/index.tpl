<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
 <style type="text/css">
 	.img-responsive2 {
    width: 100%;
    max-height: 122px;
    object-fit: cover;
    min-height: 122px;
}
.video_overlay{
  position: absolute;
  width: 40px;
    height: 40px;
    top: 50%;
    left: 50%;
    margin: -20px 0 0 -20px;
  cursor: pointer;
  background:url('application/modules/Video/externals/images/play_icn.png') no-repeat center center;
  opacity: 0;
  -webkit-transition: 0.3s ease-in-out;
  -moz-transition: 0.3s ease-in-out;
  -ms-transition: 0.3s ease-in-out;
  -o-transition: 0.3s ease-in-out;
  transition: 0.3s ease-in-out;
}
._pp_photos_item:hover .video_overlay{
  opacity: 1;
}
.time_overlay
{
  position: absolute;
  right: 10px;
  bottom: 10px;
  color: #fff;
}
 </style>
<!-- Second Block -->
<div class="_pp_area">
	<div class="border-bottom col-xs-12">
		<div class="_pp_area_heading">
			<span class="_pp_area_heading_icn"><i class="fa fa-picture-o" aria-hidden="true"></i></span> Album
			<div class="tabbable-panel">
				<div class="tabbable-line">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#tab_photos" data-toggle="tab">
							Photos </a>
						</li>
						<li>
							<a href="#tab_videos" data-toggle="tab">
							Videos </a>
						</li>
						<li>
							<a href="#tab_links" data-toggle="tab">
							PDFs </a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="_pp_summary_area block_tabs">
			<div class="_pp_area_desc">
				<div class="tabbable-panel">
					<div class="tabbable-line">
						<div class="tab-content">
							<div class="tab-pane active" id="tab_photos">
								<div class="row">
								<?php foreach( $this->albumpaginator as $album ){ ?>
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 _pp_photos_item">
									<?php
					                    if( $album->photo_id ) {?>
					                    <a href="<?php echo $this->baseUrl().'/albums/photo/view/album_id/'.$album->getIdentity().'/photo_id/'.$album->photo_id; ?>" rel="prettyPhoto[gallery1]">
					                  <?php echo $this->itemPhoto($album, 'img-responsive2');?>
					                   </a>
					                 <?php } else {?>
					                    <img class="img-responsive2" src="application/modules/Album/externals/images/empty.png">
					                  <?php  } ?>
									</div>
									<?php }?>
								</div>
								<div class="col-xs-12 _pp_area_bottom text-center">
									<a href="#" title="">Show More-3</a>
								</div>
							</div>
							<div class="tab-pane" id="tab_videos">
								<div class="row">
								<?php foreach( $this->videopaginator as $item ){?>
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 _pp_photos_item">
										<a href="<?php echo $item->getHref();?>" title="">
										<?php
						                    if( $item->photo_id ) {
						                      echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'img-responsive2'));
						                    } else {
						                      echo '<img class="img-responsive2" src="' . $this->layout()->staticBaseUrl . 'application/modules/Video/externals/images/video.png">';
						                    }
						                    ?>
										</a>
										 <a href="<?php echo $item->getHref();?>"><span class="video_overlay"></span></a>
										<span class="time_overlay"><?php if ($item->duration){
					                      if( $item->duration >= 3600 ) {
					                        $duration = gmdate("H:i:s", $item->duration);
					                      } else {
					                        $duration = gmdate("i:s", $item->duration);
					                      }
					                      echo $duration;
					                      }?>              

					                </span>
									</div>
									<?php }?>
								</div>
								<div class="col-xs-12 _pp_area_bottom text-center">
									<a href="#" title="">Show More-2</a>
								</div>
							</div>
							<div class="tab-pane" id="tab_links">
								<div class="row">
								<?php foreach( $this->pdfpaginator as $pdf ){
								$rec= Engine_Api::_()->getDbTable('pdfs', 'pdf')->getPageFile($pdf->pdf_id);
			                    $file=$rec[0]['pdf_file'];
			                    $modified_date=$rec[0]['modified_date'];?>
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 _pp_photos_item">
										<a href="<?php echo $this->baseUrl();?>/public/pdf/<?php echo $file;?>" title="">
											<img src="<?php echo $this->baseUrl();?>/public/PDF-icon.png" alt="" class="img-responsive2" />
										</a>
									</div>
									<?php }?>
								</div>
								<div class="col-xs-12 _pp_area_bottom text-center">
									<a href="#" title="">Show More-1</a>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	<!-- <div class="col-xs-12 _pp_area_bottom text-center">
		<a href="#" title="">Show More</a>
	</div> -->
</div>
<!-- End Second Block -->