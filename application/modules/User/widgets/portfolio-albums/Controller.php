<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioAlbumsController extends Engine_Content_Widget_Abstract {
	
	public function indexAction(){

		// Don't render this if not logged in
	    $viewer = Engine_Api::_()->user()->getViewer();
	    if( !Engine_Api::_()->core()->hasSubject() || !$viewer->getIdentity()) {
	      return $this->setNoRender();
	    }
	    // Get subject and reels
	    $this->view->subject	= $subject = Engine_Api::_()->core()->getSubject('user');
		
		// ALBUM PAGINATOR
		
		$this->view->albumpaginator = $albumpaginator = Engine_Api::_()->getItemTable('album')
	      ->getAlbumPaginator(array('owner' => $subject));

		$this->view->totalalbums = $albumpaginator->getTotalItemCount();
		
		// VIDEOS PAGINATOR
	    $this->view->videopaginator = $videopaginator = Engine_Api::_()->video()->getVideosPaginator(array(
	      'user_id' => $subject->getIdentity(),
	      'status' => 1,
	      'search' => 1
	    ));

	      $this->view->totalvideos = $videopaginator->getTotalItemCount();
		  
		  // PDF PAGINATOR
		  $this->view->pdfpaginator = $pdfpaginator = Engine_Api::_()->getItemTable('pdf')->getPdfPaginator(array(
	      'orderby' => 'modified_date',
		  'owner'	=>	$subject));
		  
		  $this->view->totalpdfs = $pdfpaginator->getTotalItemCount();
	    
	}
}