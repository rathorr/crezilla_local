<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioPostBlockController extends Engine_Content_Widget_Abstract {
	
	public function indexAction(){

		// Don't render this if not logged in
	    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	    if( !$viewer->getIdentity() ) {
	      return $this->setNoRender();
	    }
	}
}