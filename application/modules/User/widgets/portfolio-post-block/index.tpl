<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
<style type="text/css">
	#core_menu_mini_menu > ul.header_menu_class > li.menu_core_mini.core_mini_profile > a > span > img
	{
		width: auto;
	}
	.custom_oupload_camera_icon {
		position: absolute !important; 
	}
	._pb_img img._pb_img_shadow
	{
		position: relative !important;
	}
</style>
<!-- Second Blcok -->
<div id="_portfolio_social">
	<div class="_pp_area _ps_woym_block">
	
			<div class="_ps_woym">
				<textarea cols="3" rows="3" placeholder="What's on your mind?" class="form-control"></textarea>
				
			</div>
	
		<div class="col-xs-12 _ps_woym_links_block">
			<div class="_ps_woym_links first col-lg-4 cols-md-4 col-sm-5 cols-xs-5">
				<a href="#" title="">
					<i class="fa fa-share-alt" aria-hidden="true"></i><span class="_ps_woym_links_label"> Connections</span>
				</a>
				<a href="#" title="" class="flt-right"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
			</div>
			<div class="_ps_woym_links last col-lg-8 cols-md-8 col-sm-7 cols-xs-7">
				<a href="#" title=""><i class="fa fa-picture-o" aria-hidden="true"></i><span class="_ps_woym_links_label">Upload a Photos/Videos</span></a>
				<a href="#" title="" class="_ps_woym_sahre_btn">Share</a>
				
			</div>
		</div>
	</div>
</div>	
<!-- End Second Block -->