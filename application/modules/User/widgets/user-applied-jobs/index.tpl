<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
  <table class="profile_jp">
      <tr>
        <th scope="col">Sr No</th>
        <th scope="col">Project</th>
        <th scope="col">Job Title</th>
        <th scope="col">Posted By</th>
        <th scope="col">Posted Date</th>
        <th scope="col">Applied Date</th>
        <th scope="col">Status</th>
      </tr>

        <?php if( count($this->jobs) > 0 ){$i=1;  
   			foreach($this->jobs as $job){  ?>
      <tr>
      	<td><?php echo $i;?></td>
      	<td><?php echo $job->project_name;?></td>
        <td>
        <a href="<?php echo $this->baseUrl().'/classifieds/'.$job->owner_id.'/'.$job->classified_id.'/'.urlencode($job->title);?>">
            	<?php echo $job->title;?>
            </a>
         </td>
        <td><a href="<?php echo $this->baseUrl().'/profile/'.$job->owner_id;?>"><?php echo $job->displayname;?></a></td>
        <td><?php echo date('d/m/Y', strtotime($job->creation_date));?></td>
        <td><?php echo date('d/m/Y', strtotime($job->applied_date));?></td>
        <td>
        <?php 
        $status = "Pending";
			$class	=	"pending";
			if($job->accept == 1 )
			{
				$status = "Selected";
				$class	=	"accepted";
			}
			if($job->reject == 1)
			{
				$status = "Rejected";
				$class	=	"rejected";
			}
			if($job->hold == 1)
			{
				$status = "Shortlisted";
				$class=	"hold";
			}
          ?>
          <span class="<?php echo $class;?>"><?php echo $status;?></span>
        </td>
      </tr>
	  <?php $i++;} } else{?>
         <tr>
              <td colspan="7">You do not have applied for any Job. </td>
          </tr>
         <?php }?>

  </table>