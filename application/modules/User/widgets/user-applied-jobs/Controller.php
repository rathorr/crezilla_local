<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_UserAppliedJobsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	// Don't render this if not logged in
    $viewer = Engine_Api::_()->user()->getViewer();
    if( !$viewer->getIdentity() ) {
      return $this->setNoRender();
    }
	
	$subject = Engine_Api::_()->core()->getSubject('user');
	if( !$subject->getOwner()->isSelf($viewer) ) {
		return $this->setNoRender();
	}
	
	/*$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	 if(strtolower($profile_type) == 'producer'){
		 return $this->setNoRender();
	 }*/
	
	$table = Engine_Api::_()->getDbTable('classifieds', 'classified');
    $clssName = $table->info('name');
	
	$applytable = Engine_Api::_()->getDbTable('applys', 'classified');
    $applyName = $applytable->info('name');
	
	$usertable = Engine_Api::_()->getDbTable('users', 'user');
    $userName  = $usertable->info('name');
	
	$projtable = Engine_Api::_()->getDbTable('parenttypes', 'classified');
    $projName  = $projtable->info('name');
	
	$viewer_id	   = $viewer->getIdentity();	
	  $select = $table->select()
			  			->setIntegrityCheck(false)
						->from ( $clssName)   
						->joinLeft($applyName, "$applyName.classified_id = $clssName.classified_id", array("accept", "reject", "hold", "creation_date as applied_date"))
						->joinLeft($userName, "$userName.user_id = $clssName.owner_id", array("displayname"))
						->joinLeft($projName, "$projName.parent_id = $clssName.project_id", array("parent_name as project_name"))
						->where("$applyName.user_id = $viewer_id", 1)
						->order("$applyName.id DESC");
						
		
	// get the data
	$result = $table->fetchAll($select);
//echo '<pre>'; print_r($result); die;
	$this->view->jobs 		= $result;
    $this->_childCount = count($result);
 }
  public function getChildCount()
  {
    return $this->_childCount;
  }
	
}