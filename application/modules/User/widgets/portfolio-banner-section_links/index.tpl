<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
 <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/job_search.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/manage_pages.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/portfolio-tab.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/fonts.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/slick.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/home-3.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/portfolio.css" />
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
