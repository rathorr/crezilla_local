<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<ul>
    <li>
   <b> <?php echo $this->translate('First Name:'); ?> </b>
    <?php echo $this->fieldStructure['First Name']; ?>
  </li>
  <li>
    <b><?php echo $this->translate('Last Name:'); ?></b>
     <?php echo $this->fieldStructure['Last Name']; ?> 
  </li>
  <li>
    <b><?php echo $this->translate('Gender:'); ?></b>
     <?php echo $this->fieldStructure['Gender']; ?> 
  </li>
  <li>
    <b><?php echo $this->translate('Lives in:'); ?></b>
     <?php echo $this->fieldStructure['Location']; ?> 
  </li>
  <li>
    <b><?php echo $this->translate('Birthday:'); ?></b>
     <?php echo $this->fieldStructure['Birthday']; ?> 
  </li>
  

</ul>
<?php 
//echo $this->fieldValueLoop($this->subject(), $this->fieldStructure) ?>

