<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 10189 2014-04-30 18:51:06Z andres $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_ProfileFieldsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    // Don't render this if not authorized
    $viewer = Engine_Api::_()->user()->getViewer();
    if( !Engine_Api::_()->core()->hasSubject() ) {
      return $this->setNoRender();
    }

    // Get subject and check auth
    $subject = Engine_Api::_()->core()->getSubject('user');
    if( !$subject->authorization()->isAllowed($viewer, 'view') ) {
      return $this->setNoRender();
    }

    // Load fields view helpers
    $view = $this->view;
    $view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');


		$this->view->subject	=	$subject = Engine_Api::_()->core()->getSubject('user');
	
    $fieldsByAlias = Engine_Api::_()->fields()->getFieldsObjectsByAlias($subject);
	
   $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);
	  
	   foreach( $fieldStructure as $map )
	   {
			// Get field meta object
			$field = $map->getChild();
			$value = $field->getValue($subject);
		   
		    if( !$field || $field->type == 'profile_type' ) continue;
		    if( !$field->display && !$show_hidden ) continue;
		    $isHidden = !$field->display;
		   
			// Get first value object for reference
			$firstValue = $value;
			if( is_array($value) && !empty($value) ) {
				$firstValue = $value[0];
			}
			
			$helper = new Fields_View_Helper_FieldValueLoop();
			$helper->view = Zend_Registry::get('Zend_View');
			$tmp = $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);
			if($field->type != 'heading') {
				/*$val[$field->label] =  $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);*/
				$val[$field->label] =  $tmp;
			}
			
		}
		
		$this->view->fieldStructure	=	$val;
		if( !empty($fieldsByAlias['profile_type']) )
		{
		  $optionId = $fieldsByAlias['profile_type']->getValue($subject);
		  //echo '<pre>'; print_r($optionId);
		  if( $optionId ) {
			$optionObj = Engine_Api::_()->fields()
			  ->getFieldsOptions($subject)
			  ->getRowMatching('option_id', $optionId->value);
			if( $optionObj ) {
			
			  $this->view->memberType = $optionObj->label;
			}
		  }
		}
		
		//die;

    // Values
    /*$this->view->fieldStructure = $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);
	//echo '<pre>';print_r($fieldStructure); echo '</pre>';die;
    if( count($fieldStructure) <= 1 ) { // @todo figure out right logic
      return $this->setNoRender();
    }*/
  }
}