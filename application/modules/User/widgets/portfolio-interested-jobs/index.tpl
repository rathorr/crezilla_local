<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
<style type="text/css">
#global_content > div > div.generic_layout_container.layout_main > div.generic_layout_container.layout_left > div.generic_layout_container.layout_user_portfolio_interested_jobs > div > div._pp_area_heading.block_pymk > span._pp_area_heading_icn
{
  left: 10px;
}
._pp_area_heading 
{
  padding-left: 35px;
}
</style>
<!-- Seventh Block -->
<!-- <div class="_ps_common_heading">Jobs you may interested </div> -->
<div class="_pp_area _ps_jymi_block">
<div class="_pp_area_heading block_pymk">
			<span class="_pp_area_heading_icn"><i class="fa fa-briefcase" aria-hidden="true"></i></span> Jobs you may interested
		</div>
	<div id="_ps_jymi_block">
<?php foreach($this->jobs as $job){
       $data  = Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedDataById($job['classified_id']);
       $applyby= Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedsapplyBy($job['classified_id']);
                    // echo '<pre>'; print_r($data);die();
                     foreach($data as $res){
                  
                    if($res['field_id'] == 61){
                      $desig=$res['values'];
                    }
                    if($res['field_id'] == 74){
                      $expe1=$res['values'];
                      if($expe1=='')
                      {
                        $expe="0";
                      }
                      else
                      {
                        $expe= $expe1;
                      }
                    }
                    if($res['field_id'] == 73){                    
                      $lang=$res['values'];
                    }
                
                    if($res['field_id'] == 75){
                      $keyw=$res['values'];
                    }

                    if($res['field_id'] == 41){
                      $dura=$res['values'];
                    }
                    
                    if($res['field_id'] == 71){
                      $pay_term=$res['values'];
                    }
                                 
                    if($res['field_id'] == 14){
                      $work_loc=$res['values'];
                    }

                    if($res['field_id'] == 72){
                      $out_app=$res['values'];
                      if($out_app=='Yes')
                      {
                        $out_app="may";
                      }
                      else
                      {
                        $out_app= "need not";
                      }
                    }
                                                          
                }

                      $ownername='';
                       $jobownerpic = '';
                        $classified = Engine_Api::_()->getItem('classified', $job['classified_id']);
                        //echo '<pre>'; print_r($classified); 
                        if($job['page_id'] != 0){
                            $companypage  = Engine_Api::_()->getItem('page', $job['page_id']);
                            //echo '<pre>'; print_r($companypage);
                            $ownername=$companypage['displayname'];
                            $jobownerpic =Engine_Api::_()->getDbtable('users', 'user')->getUserJobDesigPicPath($desig);
                            $link = $companypage->getHref();
                            $jobownerpic  = ($jobownerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$jobownerpic;
                            $isApply  = Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($job['classified_id'], $this->userid); 

                        }else{
                           $user = Engine_Api::_()->getItem('user', $job['owner_id']);
                           //echo '<pre>'; print_r($user); 
                           $ownername=$user['displayname'];
                           $link =  $user->getHref();
                           $jobownerpic =Engine_Api::_()->getDbtable('users', 'user')->getUserJobDesigPicPath($desig); 
                           $jobownerpic = ($jobownerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$jobownerpic;
                          $isApply  = Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($job['classified_id'], $this->userid);

                        }
                    //die();
      ?>	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 _item_block_content">
			<div class="">
				<div class="_item_block_head">
					<div class="_createdby left">by <span class="_createdby_name"><?php echo $ownername; ?>,</span> <span class="_dated"><?php //echo $this->timestamp($classified['creation_date']); 
                echo date('d F Y',strtotime($classified['creation_date'])); ?></span></div>	
				</div>
				<div class="_job_item_content">
					<div class="_job_item_img"><img src="<?php echo $jobownerpic;?>"></div>
					<div class="_job_title"><a href="<?php echo $classified->getHref();?>" title=""><?php echo $job['title']; ?></a></div>
					<div class="_job_details">
						<span class="_job_exp"><i class="fa fa-briefcase" aria-hidden="true"></i> <?php echo $expe ?></span>
						<span class="_job_loc"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $work_loc ?></span>
					</div>
					<div class="_job_desc">
						<div class="_job_desc_title">Job Description</div>
						<p><b><?php echo $desig ?></b> wanted, with <b><?php echo $expe ?></b> of experience, fluent in <b><?php echo $lang ?></b> for a <b><?php echo $dura ?></b> assignment. Payment terms will be on a <b><?php echo $pay_term ?></b> basis. This position is primarily based in <b><?php echo $work_loc ?></b>.  Outstation candidates <b><?php echo $out_app ?></b> apply.</p>
					</div>
				</div>
				<div class="_item_block_footer">
					<a href="/activity/index/share/type/classified/id/<?php echo $job['classified_id']?>/format/smoothbox" title="" class="smoothbox _shared_list_item"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
					<?php if ($isApply) { ?>
					<a id="unapply_<?php echo $job['classified_id'];?>" class="smooth_box add_directory" title="Un Apply" data-index="<?php echo $job['classified_id'];?>"><i class="fa fa-remove"></i></a>
                <?php } else {?>
					<a href="/classifieds/application/applysave/classified_id/<?php echo $job['classified_id'];?>" title="" class="smoothbox add_directory"><i class="fa fa-check-square"></i></a>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>						
	</div>
</div>
<!-- End Seventh Block -->