<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_ProfileWallphotoController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    // Don't render this if not authorized
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    if( !Engine_Api::_()->core()->hasSubject() ) {
      return $this->setNoRender();
    }
	
    // Get subject and check auth
    $subject = Engine_Api::_()->core()->getSubject('user');
	$this->view->backurl	=	$_GET['q'];
	$this->view->is_shortlist		=	Engine_Api::_()->getDbtable('shortlists', 'user')->isShortlist($viewer->getidentity(), $subject->getIdentity());

    /*if( !$subject->authorization()->isAllowed($viewer, 'view') ) {
			return $this->setNoRender();
    }*/

	$is_owner = false;
	if($subject->getIdentity() == $viewer->getIdentity()){
			$is_owner = true;  
	}

	$this->view->is_owner = $is_owner;
    //echo '<pre>'; print_r($subject); die();
    $this->view->user = $subject;

	
	$this->view->navigation = Engine_Api::_()
      ->getApi('menus', 'core')
      ->getNavigation('user_profile');
	  
	  $this->view->friend_navigation = Engine_Api::_()
      ->getApi('menus', 'core')
      ->getNavigation('user_profile_friend');
      /*$follow= Engine_Api::_()->getApi('menus', 'core')->getNavigation('user_profile_follow');
      foreach ($follow->_params as $value) {
        echo $action= $value->action;
              }
      echo "action".$follow->action;
      echo '<pre>'; print_r($follow); die();*/
     $this->view->follow_navigation = Engine_Api::_()
      ->getApi('menus', 'core')
      ->getNavigation('user_profile_follow');
	  
	  $follow_list	=	Engine_Api::_()->getDbtable('followship', 'user')->getUserFollowList($subject->getIdentity());
	  $this->view->follow_count = count($follow_list);
  }
  
  
  
}