<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

?>



<?php $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserWallPicPath($this->user->getIdentity());
//if($pic == '')$pic	=	$this->baseUrl().'/application/modules/User/externals/images/nophoto_user_thumb_wallmain.png';
if($pic == '')$pic	=	Engine_Api::_()->getDbtable('users', 'user')->getUserRandomWallPicPath();
 ?>
 <span style="position: absolute;right: 9px;top: 8px;">
 	<?php //if($this->backurl){
	if($this->is_shortlist){?>
    	<a href="javascript:void(0);" class="remove_shortlist" title="Shortlisted">&nbsp;</a>
 <?php }else{ ?>
	<a href="<?php echo $this->baseUrl().'/members/friends/shortlist/user_id/'.$this->user->getIdentity().'?q='.$this->backurl;?>" class="smoothbox shortlist">&nbsp;</a>
<?php }
//} ?>
 </span>
<div id='profile_wallphoto' style="background-image:url(<?php echo $pic ?>);" class="wall_background">
 <?php if($this->viewer()->getIdentity() == $this->user->getIdentity()){?>
 <span >
 	<a href="javascript:void(0);" class="custom_oupload_camera_icon"> 
    	<span>Update Cover Picture</span>
     </a>
	 <ul class="cover_option custom_cover_bx" style="display:none;">
    	<li>
        <a href="<?php echo $this->baseUrl().'/members/profile/upload/item_type/cover/id/'.$this->user->getIdentity();?>" class="smoothbox upload_photo">Upload Photo</a></li>
        <li><a href="<?php echo $this->baseUrl().'/members/profile/removephoto/item_type/cover/id/'.$this->user->getIdentity();?>" class="smoothbox remove_photo">Remove Photo</a></li>
    </ul>
    
 </span>
 <?php } ?>
</div>
<span class="wall_tp"></span>



<div class="user_views_followers">


<?php if(!$this->is_owner){ ?>
<span class="friend_request">
<img src="<?php echo $this->baseUrl().'/public/custom/images/freind-icon.jpg';?>">
<?php  echo $this->navigation()
      ->menu()
      ->setContainer($this->friend_navigation)
      ->setPartial(array('_navIcons.tpl', 'core'))
      ->setUlClass('profile_options_menu')
      ->render();
	  ?>
	  </span>
<?php }  ?>

<?php if(!$this->is_owner){ ?>
<span class="friend_follow" id="friend_follow">
<!-- <img src="<?php echo $this->baseUrl().'/public/custom/images/profile_options_add_friend.png';?>"> -->
<?php echo $this->navigation()
      ->menu()
      ->setContainer($this->follow_navigation)
      ->setPartial(array('_navIcons.tpl', 'core'))
      ->setUlClass('profile_options_menu')
      ->render();
	  ?>
	  </span>
<?php }  ?>


<span class="views">
	<span class="view_count"><?php echo $this->locale()->toNumber($this->user->view_count);?>
    </span>
	<span class="view_title">views</span>
</span>


<span class="followers">
	<span class="view_count">
     <?php echo $this->follow_count;?>
    
    </span>
<span class="view_title">followers</span>
	
</span>
</div>
<?php if(!$this->is_owner){?>
<div id='profile_options'>
<a  href="javascript:void(0);" class="tft show_profile_options">...
</a>
  <?php // This is rendered by application/modules/core/views/scripts/_navIcons.tpl
    echo $this->navigation()
      ->menu()
      ->setContainer($this->navigation)
      ->setPartial(array('_navIcons.tpl', 'core'))
      ->setUlClass('profile_options_menu')
      ->render()
  ?>
</div>
<?php } ?>
<script type="text/javascript">
jQuery(document).on('click','.show_profile_options, .tft',function(){
	jQuery('#profile_options ul').slideToggle();	
});

function changepic(){
	jQuery.post('<?php echo $this->baseUrl()."/members/profile/getuserpic/item_type/cover/id/".$this->user->getIdentity();?>', function(resp){
			var pic	=	jQuery.trim(resp);
			if(pic == ''){
				pic = '<?php echo $this->baseUrl();?>/application/modules/User/externals/images/nophoto_user_thumb_wallmain.png';	
			}
			jQuery('#profile_wallphoto').css('background-image','url('+pic+')');
			jQuery('.cover_option').css("display", "none");
			jQuery('.custom_oupload_camera_icon').css("background-color", "");
		});
}

jQuery(document).on('click', '#profile_wallphoto', function(){
	if(jQuery('.cover_option').css("display") =='none'){
		jQuery('.cover_option').css("display", "block");
		jQuery('.custom_oupload_camera_icon span').addClass("show_cover_icon");
		jQuery('.custom_oupload_camera_icon').css("background-color", "rgba(0,0,0,0.4)");	
	}else{
		jQuery('.cover_option').css("display", "none");
		jQuery('.custom_oupload_camera_icon span').removeClass("show_cover_icon");
		jQuery('.custom_oupload_camera_icon').css("background-color", "");
	}
});
/*jQuery(document).mouseup(function (e)
{
	// CLOSE PROFILE OPTIONS ON MY PROFILE
	var menucontainer = jQuery(".user_profile_option_menus");

    if (!menucontainer.is(e.target) // if the target of the click isn't the container...
        && menucontainer.has(e.target).length === 0) // ... nor a descendant of the container
    { 
	   menucontainer.addClass('tab_closed');
	   menucontainer.removeClass('tab_open');
    }
});*/
</script>

