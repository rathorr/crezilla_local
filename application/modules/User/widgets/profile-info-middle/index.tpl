<style>
span.info_middle{width:100% !important;}
</style>
<div id="prof_mid_sec" class=" tab_977 generic_layout_container layout_user_profile_fields profile_middle_section">
    <div class="profile_fields">
	<ul>
		<?php if(isset($this->fieldStructure['About Me']) && $this->fieldStructure['About Me'] != ''){ ?>
		<h2>
			  About Me
			</h2>
		<li data-field-id="3">    
			
			<span class="info_middle">
			  <?php //echo '<pre>';print_r($this->fieldStructure);echo '</pre>';
			  echo $this->fieldStructure['About Me']; ?>
			</span>
		</li>  
		<?php } ?>
		
		<?php if(isset($this->fieldStructure['education']) && $this->fieldStructure['education'] != ''){ ?>		
		<h2>
			  Education
		</h2>
         <?php if($this->is_owner){?>
        	<a href="<?php echo $this->baseUrl().'/education';?>">edit</a><?php } ?>
		
		<?php foreach($this->fieldStructure['education'] as $k=>$v){ ?>
		<li data-field-id="4">
			<span class="info_middle">
			   <b><?php echo $v; ?></b><br>
			   <?php echo $this->fieldStructure['education_fos'][$k]; ?>
			</span>
		</li>
		<?php } ?>
		<?php } ?>
		
		
		<?php if(isset($this->fieldStructure['experiences']) && $this->fieldStructure['experiences'] != ''){ ?>		
		<h2>
			  Career Info
		</h2>
        <?php if($this->is_owner){?>
        	<a href="<?php echo $this->baseUrl().'/experience';?>">edit</a><?php } ?>
		<?php 
		//echo '<pre>';print_r($this->fieldStructure['experiences']);
		foreach($this->fieldStructure['experiences'] as $k=>$v){ ?>
		<li data-field-id="6">
			<span class="info_middle">
				<b><?php echo $v['title']; ?></b>
			</span>
		</li>
		<li data-field-id="6">
			<span class="info_middle">
				<?php echo $v['channel']; ?><br>
				<?php echo $v['company']; ?><br>
				<?php echo $v['role']; ?><br>
				<?php echo $v['duration']; ?><br>
			</span>
		</li> 		
		<?php } ?>
		<?php } ?>
	</ul>
     </div>        
</div>