<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_ProfileInfoMiddleController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    // Don't render this if not authorized
    $viewer = Engine_Api::_()->user()->getViewer();
    if( !Engine_Api::_()->core()->hasSubject() ) {
         return $this->setNoRender();
    }
	
	// Get subject and check auth
   $subject = Engine_Api::_()->core()->getSubject('user');
	if( !$subject->authorization()->isAllowed($viewer, 'view') ) {
		return $this->setNoRender();
	}
	

	
	// Calculate viewer-subject relationship
    $usePrivacy = ($subject instanceof User_Model_User);
    if( $usePrivacy ) {
      $relationship = 'everyone';
      if( $viewer && $viewer->getIdentity() ) {
        if( $viewer->getIdentity() == $subject->getIdentity() ) {
          $relationship = 'self';
        } else if( $viewer->membership()->isMember($subject, true) ) {
          $relationship = 'friends';
        } else {
          $relationship = 'registered';
        }
      }
    }
   
    


	
	
	// Load fields view helpers
    $view = $this->view;
    $view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
   
    // Values
    $this->view->fieldStructure = $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);

    if( count($fieldStructure) <= 1 ) { // @todo figure out right logic
         return $this->setNoRender();
       }
	   
	//print_r($fieldStructure);die;
   
    $valuesStructure = array();
    $valueCount = 0;
	
	$show_hidden = $viewer->getIdentity()
                 ? ($subject->getOwner()->isSelf($viewer) || 'admin' === Engine_Api::_()->getItem('authorization_level', $viewer->level_id)->type)
                 : false;
		
	
	foreach( $fieldStructure as $map )
       {
			// Get field meta object
			$field = $map->getChild();
			$value = $field->getValue($subject);

		    if( !$field || $field->type == 'profile_type' ) continue;
		    if( !$field->display && !$show_hidden ) continue;
		    $isHidden = !$field->display;
		  
		    // Get first value object for reference
		    $firstValue = $value;
		    if( is_array($value) && !empty($value) ) {
		    	$firstValue = $value[0];
		    }
			
			// Evaluate privacy
			if( $usePrivacy && !empty($firstValue->privacy) && $relationship != 'self' ) {
				if( $firstValue->privacy == 'self' && $relationship != 'self' ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'friends' && ($relationship != 'friends' && $relationship != 'self') ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'registered' && $relationship == 'everyone' ) {
				  $isHidden = true; //continue;
				}
			}
			
			
		    //print_r($firstValue);
		    $helper = new Fields_View_Helper_FieldValueLoop();
			$helper->view = Zend_Registry::get('Zend_View');
			$tmp = $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);
			if( $field->type != 'heading' && (!$isHidden || $show_hidden )) {
				$val[$field->label] = $tmp;
			}
		}
	

	$catTable = Engine_Api::_()->getDbTable('categories', 'education');
    $catName = $catTable->info('name'); 
	
	$table = Engine_Api::_()->getDbTable('educations', 'education');
    $eduName = $table->info('name'); 
	
	$sid = $subject->getIdentity();
	
	//$table = Engine_Api::_()->getItemTable('education');
    $select = $table->select("$eduName.*")
						->setIntegrityCheck(false)
                        ->where("$eduName.owner_id = $sid", 1)
						->where("$eduName.row_status = 1", 1)
       					->joinLeft($catName, "$catName.category_id = $eduName.category_id", array('category_name'))
                        ->order("$eduName.creation_date");
	// get the data
	$result = $table->fetchAll($select);
	
	//echo '<pre>'; print_r($result); die;
	
	foreach($result as $edu){
		//$edu->education_id == 
		$val['education'][] = $edu->institute_name;
		$val['education_cat'][] = $edu->category_name;
		$val['education_fos'][] = $edu->field_of_study;
	}
	
	//EXPERIENCE INFORMATION STARTS
	
	$table = Engine_Api::_()->getDbTable('experiences', 'experience');
    $expName = $table->info('name');
	
	$roletable = Engine_Api::_()->getDbTable('roles', 'experience');
    $roleName = $roletable->info('name');
	
	$roletable2 = Engine_Api::_()->getDbTable('roles', 'experience');
    $roleName2 = $roletable2->info('name'); 
	
	//$table = Engine_Api::_()->getItemTable('experience');
      $select = $table->select("$expName.*")
						->setIntegrityCheck(false)
						->joinLeft($roleName, "$roleName.role_id = $expName.role_id", array('title as role_name'))
						->joinLeft($roleName2, "engine4_experience_roles_2.role_id = $expName.role_subcat_id", array('title as role_cat_name'))
                        ->where("$expName.owner_id = $sid", 1)
						->where("$expName.row_status = 1", 1)
                        ->order("$expName.creation_date DESC");
	// get the data
	$result = $table->fetchAll($select);
	//echo '<pre>'; print_r($result); die;
	$this->view->experiences = 	$result;
	$i = 0;
	foreach($result as $exp){
		//$edu->education_id == 
		$val['experiences'][$i]['title'] = $exp->title;
		$val['experiences'][$i]['channel'] = $exp->channel;
		$val['experiences'][$i]['company'] = $exp->company;
		$val['experiences'][$i]['role'] = $exp->role_name;
		$val['experiences'][$i]['duration'] = date('F Y',strtotime($exp->start_date))." - ".date('F Y',strtotime($exp->end_date));
		$i++;
	//$val[$edu->category_name][] = $edu->institute_name;
	}
	
	//echo '<pre>';print_r($val);echo '</pre>';die;
	
	
	$this->view->fieldStructure = $val;
	$this->view->is_owner	=	($subject->getOwner()->isSelf($viewer))?true:false;

    
  }
}