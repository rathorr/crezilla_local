<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioBannerMobile2Controller extends Engine_Content_Widget_Abstract {
	
	public function indexAction(){

		// Don't render this if not logged in
	    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	    if( !$viewer->getIdentity() ) {
	      return $this->setNoRender();
	    }
	    $this->view->user = $subject = Engine_Api::_()->core()->getSubject('user');
	   $this->view->userid= $subject->getIdentity();
	    // Get subject and check auth	    
	    $is_owner = false;
		if($subject->getIdentity() == $viewer->getIdentity()){
				$is_owner = true;  
		}
		$this->view->is_owner = $is_owner;
		$this->view->resource= $resource= Engine_Api::_()->getDbtable('membership', 'user')->checkFriendship($subject->getIdentity(),$viewer->getidentity());
		//echo "subject".$subject->getIdentity(); die();
		$this->view->is_shortlist= $is_shortlist= Engine_Api::_()->getDbtable('shortlists', 'user')->isShortlist($viewer->getidentity(), $subject->getIdentity());
		//echo "shortlists". $is_shortlist;die();

		 $view = $this->view;
    $view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');

    // Values
    $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);
	
	foreach( $fieldStructure as $map )
       {
			// Get field meta object
			$field = $map->getChild();
			$value = $field->getValue($subject);

		    if( !$field || $field->type == 'profile_type' ) continue;
		    if( !$field->display && !$show_hidden ) continue;
		    $isHidden = !$field->display;
		  
		    // Get first value object for reference
		    $firstValue = $value;
		    if( is_array($value) && !empty($value) ) {
		    	$firstValue = $value[0];
		    }
			
			// Evaluate privacy
			if( $usePrivacy && !empty($firstValue->privacy) && $relationship != 'self' ) {
				if( $firstValue->privacy == 'self' && $relationship != 'self' ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'friends' && ($relationship != 'friends' && $relationship != 'self') ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'registered' && $relationship == 'everyone' ) {
				  $isHidden = true; //continue;
				}
			}
			
			
		    //print_r($firstValue);
		    $helper = new Fields_View_Helper_FieldValueLoop();
			$helper->view = Zend_Registry::get('Zend_View');
			$tmp = $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);
			if( $field->type != 'heading' && (!$isHidden || $show_hidden )) {
				$val[$field->label] = $tmp;
			}
		}
		$this->view->fieldStructure = $val;
	    
	}
}