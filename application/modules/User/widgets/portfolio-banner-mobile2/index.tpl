<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
 <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/job_search.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/manage_pages.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/portfolio-tab.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/fonts.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/slick.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/home-3.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/portfolio.css" />
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
<style type="text/css">
	#core_menu_mini_menu > ul.header_menu_class > li.menu_core_mini.core_mini_profile > a > span > img
	{
		width: auto;
	}
	.custom_oupload_camera_icon {
		position: absolute !important; 
	}
	._pb_img img._pb_img_shadow
	{
		position: relative !important;
	}
	#global_content > div > div.generic_layout_container.layout_main > div.generic_layout_container.layout_middle
	{
		width: 50% !important;
		background-color: #E9EAED !important;
	}
	.login_my_wrapper #global_content .layout_main {
    padding: 5px 8.7% !important;
    }
    .layout_left
    {
    	width: 50%;
    }
    #_portfolio_social_mobile > div > h3:nth-child(1)
    {
    	display: none;
    }
    #_portfolio_social > div._pp_area._ps_pinned_post_block
    {
    	margin-bottom: 10px;
    }
	@media(max-width:767px){
		._porfolio_section_content
	{
		padding-left: 0px;
		padding-right: 0px;
	}
	#core_menu_mini_menu_mobile > ul.header_menu_class2 > li.menu_core_mini.core_mini_profile > a > span > img:nth-child(2)
	{
		width: auto;
	}
	#global_content > div > div.generic_layout_container.layout_main > div.generic_layout_container.layout_left,
	#global_content > div > div.generic_layout_container.layout_main > div.generic_layout_container.layout_middle
	{
		width: 100% !important;
		padding-left: 10px;
		padding-right: 10px;
	}
	.login_my_wrapper #global_content .layout_main {
    padding: 0px 0% !important;
	}
	.layout_main > .layout_middle
	{
		display: none;
	}
	}
	.img-responsive3 {
    width: 100%;
    max-height: 160px;
    object-fit: cover;
    min-height: 160px;
}
</style>
 <?php $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($this->userid);
if($pic == '')$pic	=	$this->baseUrl().'/application/modules/User/externals/images/nophoto_user_thumb_profile.png';
//wallphoto
$wallpic=Engine_Api::_()->getDbtable('users', 'user')->getUserWallPicPath($this->userid);
if($wallpic == '')$wallpic	=	Engine_Api::_()->getDbtable('users', 'user')->getUserRandomWallPicPath();

$user = Engine_Api::_() -> getItem('user', $this->userid);
 ?>

 <!-- BANNER START -->
<section class="_protfolio_banner_section">
	<div class="container">
		<div class="col-xs-12">
			<div class="_pb_banner_area _desktop_version">
				<div class="_pb_img">				
				 <?php if($this->is_owner){?>
					<a href="javascript:void(0);" id="profile_wallphoto" class="custom_oupload_camera_icon"> 
				    	<span>Update Cover Picture</span>
				     </a>
					 <ul class="cover_option custom_cover_bx" style="display:none;">
				    	<li>
				        <a href="<?php echo $this->baseUrl().'/members/profile/upload/item_type/cover/id/'.'1';?>" class="smoothbox upload_photo">Upload Photo</a></li>
				        <li><a href="<?php echo $this->baseUrl().'/members/profile/removephoto/item_type/cover/id/'.$this->user->getIdentity();?>" class="smoothbox remove_photo">Remove Photo</a></li>
				    </ul>
				    <?php } ?>
					<img src="<?php echo $wallpic; ?>" alt="" class="img-responsive4">	
				</div>
				<div class="_pb_actions text-right hidden-xs">
				<?php $resource=$this->resource;
					if ($resource == '0'){?>
					<a href="/members/friends/cancel/user_id/<?php echo $this->user->getIdentity();?>" title="Cancel Request" class="btn btn-default _pb_update_profile_btn smoothbox"><i class="fa fa-minus" aria-hidden="true"></i> Cancel</a>
					<?php } elseif ($resource == '1'){?>
					<a href="/members/friends/remove/user_id/<?php echo $this->user->getIdentity();?>" title="Remove Connection" class="btn btn-default _pb_update_profile_btn smoothbox"><i class="fa fa-times" aria-hidden="true"></i> Remove</a>
					<?php } else {?>
					<a href="/members/friends/add/user_id/<?php echo $this->user->getIdentity();?>" title="Add Connection" class="btn btn-default _pb_update_profile_btn smoothbox"><i class="fa fa-plus" aria-hidden="true"></i> Connect </a>
					<?php }?>
					<a href="/messages/compose" title="Update Profile" class="btn btn-default _pb_update_profile_btn"><i class="fa fa-commenting" aria-hidden="true"></i> Message</a>
				</div>
				<div class="_pb_user_img">
					<img src="<?php echo $pic; ?>" alt="" class="img-responsive3" />
					<?php if($this->is_owner){?>
					<a href="<?php echo $this->baseUrl().'/members/profile/upload/item_type/dp/id/'.$this->user->getIdentity();?>" class="smoothbox custom_oupload_camera_icon_small">
						<span>Update Profile Picture</span></a>
					 <?php } ?>
				</div>
				<div class="_pb_user_name">
					<?php if($this->fieldStructure['First Name'] == '' && $this->fieldStructure['Last Name'] ==''){ 
					 echo ucwords(strtolower($this->viewer()->getTitle()));  
			     }else{
			          echo ucwords(strtolower($this->fieldStructure['First Name']).' '.ucwords($this->fieldStructure['Middle Name']).' '.ucwords($this->fieldStructure['Last Name']));
			   } ?>
				</div>
				<div class="_pb_user_actions hidden-xs">
					<a href="#" title="" class="_pb_user_actions_download"><i class="fa fa-download" aria-hidden="true"></i></a>
					<a href="#" title="" class="_pb_user_actions_share"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
					<?php if($this->is_shortlist){?>
				    	<a title="Already shortlisted" class="add_directory" tabindex="0"><i class="_addfolderr_icn"></i></a>
				 <?php }else{ ?>
					<a href="<?php echo $this->baseUrl().'/members/friends/shortlist/user_id/'.$this->user->getIdentity().'?q='.$this->backurl;?>" title="shortlist" class="add_directory smoothbox" tabindex="0"><i class="_addfolderr_icn"></i></a>
				<?php } ?>					
					<a href="#" title="" class="_pb_user_actions_likes count-active"><i class="fa fa-eye" aria-hidden="true"></i></a><span class="_pb_user_actions_likes_count"><?php echo $user->view_count;?> Views</span>
				</div>
			</div>		
		</div>
	</div>
</section>
<div class="col-xs-12 visible-xs hidden-lg hidden-md hidden-sm _mobile_version _tabbing_header">	<a href="#_portfolio_profile_mobile" class="current" title="">Profile</a>		
	<a href="#_portfolio_social_mobile" title="">Social</a>			
</div>
<div id="_portfolio_profile" class="_porfolio_section_content col-lg-6 col-md-6 col-sm-6 col-xs-12">
	<div class="visible-xs hidden-lg hidden-md hidden-sm _mobile_version _pn_banner_area">
		<div class="_pb_user_actions">
			<a href="#" title="" class="_pb_user_actions_download"><i class="fa fa-download" aria-hidden="true"></i></a>
			<a href="#" title="" class="_pb_user_actions_share"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
			<?php if($this->is_shortlist){?>
				    	<a title="Already shortlisted" class="add_directory" tabindex="0"><i class="_addfolderr_icn"></i></a>
				 <?php }else{ ?>
					<a href="<?php echo $this->baseUrl().'/members/friends/shortlist/user_id/'.$this->user->getIdentity().'?q='.$this->backurl;?>" title="shortlist" class="add_directory smoothbox" tabindex="0"><i class="_addfolderr_icn"></i></a>
				<?php } ?>	
			<a href="#" title="" class="_pb_user_actions_likes count-active"><i class="fa fa-heart" aria-hidden="true"></i> <span class="_pb_user_actions_likes_count"><?php echo $user->view_count;?> Views</span></a>
		</div>
		<div class="_pb_actions text-right">
			<?php $resource=$this->resource;
				if ($resource == '0'){?>
				<a href="/members/friends/cancel/user_id/<?php echo $this->user->getIdentity();?>" title="Cancel Request" class="btn btn-default _pb_update_profile_btn smoothbox"><i class="fa fa-minus" aria-hidden="true"></i> Cancel</a>
				<?php } elseif ($resource == '1'){?>
					<a href="/members/friends/remove/user_id/<?php echo $this->user->getIdentity();?>" title="Remove Connection" class="btn btn-default _pb_update_profile_btn smoothbox"><i class="fa fa-times" aria-hidden="true"></i> Remove</a>
					<?php } else {?>
					<a href="/members/friends/add/user_id/<?php echo $this->user->getIdentity();?>" title="Add Connection" class="btn btn-default _pb_update_profile_btn smoothbox"><i class="fa fa-plus" aria-hidden="true"></i> Connect </a>
					<?php }?>
					<a href="/messages/compose" title="Update Profile" class="btn btn-default _pb_update_profile_btn"><i class="fa fa-commenting" aria-hidden="true"></i> Message</a>
		</div>
	</div>
</div>
<!-- BANNER END -->
<script type="text/javascript" src="application/modules/User/externals/portfolio/bootstrap.min.js"></script>
<script type="text/javascript" src="application/modules/User/externals/portfolio/slick.js"></script>
<script type="text/javascript">
function changepic(){
	jQuery.post('<?php echo $this->baseUrl()."/members/profile/getuserpic/item_type/cover/id/".$this->userid;?>', function(resp){
			var pic	=	jQuery.trim(resp);
			if(pic == ''){
				pic = '<?php echo $this->baseUrl();?>/application/modules/User/externals/images/nophoto_user_thumb_wallmain.png';	
			}
			jQuery('.img-responsive4').attr('src',pic);
			jQuery('.cover_option').css("display", "none");
			jQuery('.custom_oupload_camera_icon').css("background-color", "");
		});
}
function changeprofilepic(){
	jQuery.post('<?php echo $this->baseUrl()."/members/profile/getuserpic/item_type/dp/id/".$this->userid;?>', function(resp){
			var pic	=	jQuery.trim(resp);
			if(pic == ''){
				pic = '<?php echo $this->baseUrl();?>/application/modules/User/externals/images/nophoto_user_thumb_profile.png';	
			}
			//jQuery('#profile_photo').css('background-image','url('+pic+')');
			jQuery('.img-responsive3').attr('src',pic);
			jQuery.post('<?php echo $this->baseUrl()."/members/profile/getuseralbums/id/".$this->userid;?>', function(resp){jQuery('.albums_list').html(jQuery.trim(resp))});
			
		});
}
jQuery(document).on('click', '#profile_wallphoto', function(){
	if(jQuery('.cover_option').css("display") =='none'){
		jQuery('.cover_option').css("display", "block");
		jQuery('.custom_oupload_camera_icon span').addClass("show_cover_icon");
		jQuery('.custom_oupload_camera_icon').css("background-color", "rgba(0,0,0,0.4)");	
	}else{
		jQuery('.cover_option').css("display", "none");
		jQuery('.custom_oupload_camera_icon span').removeClass("show_cover_icon");
		jQuery('.custom_oupload_camera_icon').css("background-color", "");
	}
});
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		
		jQuery('#block_pymk_carousel, #_ps_jymi_block').slick({
			slidesToShow: 2,
  			slidesToScroll: 1
		});
		
		jQuery('.layout_main > .layout_left').attr("id","_portfolio_profile_mobile");
		jQuery('.layout_main > .layout_middle').attr("id","_portfolio_social_mobile");
		//jQuery(".layout_main > .layout_middle").css('display','none');
		jQuery("._tabbing_header a").click(function(event) {
			//jQuery("._tabbing_header a").removeClass("wp_init");
	        event.preventDefault();
	        jQuery(this).addClass("current");
	        jQuery(this).siblings().removeClass("current");
	         var tab = jQuery(this).attr("href");
	         var tab2 =jQuery(this).siblings().attr("href");
	         //jQuery(tab).fadeIn();
	         //jQuery(tab2).fadeOut();
	         console.log(tab);
	         console.log(tab2);
	         jQuery(tab).css("display", "block");
	        jQuery(tab2).css("display", "none");
	        //jQuery(tab).fadeIn();
	    });
		
		/*jQuery('#block_pymk_carousel, #_ps_jymi_block').slick({
			slidesToShow: 2,
  			slidesToScroll: 1,
  			 responsive: [
			    {
			      breakpoint: 600,
			      settings: {
			        centerMode: true,
			        centerPadding: '80px',
			        slidesToShow: 1
			      }
			    }
			]
		});*/
	});
</script>