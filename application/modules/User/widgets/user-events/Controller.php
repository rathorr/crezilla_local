<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_UserEventsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    
	// Don't render this if not logged in
	$subject 				= Engine_Api::_()->core()->getSubject('user');
	$subjectId 				= Engine_Api::_()->core()->getSubject('user')->getIdentity();
    $viewer 				= Engine_Api::_()->user()->getViewer();
	$this->view->id		 	= $subjectId;
	
	//$is_friend	=	Engine_Api::_()->getDbtable('friendship', 'user')->isFriend($viewer->getIdentity(), $subject->getIdentity());
	
	if($subjectId== $viewer->getIdentity()){
		return $this->setNoRender();
	}
	
	 // Don't render this if friends are hidden
    if((($subject->friend_setting == 2 && !$subject->membership()->isMember($viewer)) || ($subject->friend_setting ==3)) && $subject->getIdentity() != $viewer->getIdentity()){
		return $this->setNoRender();	
	}
	
 }
}