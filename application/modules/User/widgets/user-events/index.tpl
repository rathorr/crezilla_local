<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<div class="headline">
  <h2>
    <?php //echo $this->translate('Events list');?>
  </h2>
  
</div>
<?php  
 $this->headLink()
 ->appendStylesheet($this->baseUrl().'/application/modules/User/externals/css/fullcalendar.css');
 $this->headScript()
		->appendFile($this->baseUrl() . '/application/modules/User/externals/js/moment.min.js')
		
	   ->appendFile($this->baseUrl() . '/application/modules/User/externals/assets/js/jquery-ui.min.js')
       ->appendFile($this->baseUrl() . '/application/modules/User/externals/js/fullcalendar.min.js');
 ?>
<script>
var json_events =[];
var user_id = '<?php echo $this->id ; ?>';
 	var ajax_url = '<?php echo $this->baseUrl()."/user/event/";?>';

	jQuery(document).ready(function() {
		var zone = "05:30";  //Change this to your timezone
var user_id = '<?php echo $this->id ; ?>';
	jQuery.ajax({
		url: ajax_url+'get-events',
        type: 'POST', // Send post data
        data: 'type=fetch&user_id='+user_id,
        async: false,
        success: function(s){
        	json_events = s;
			
        }
	});


	var currentMousePos = {
	    x: -1,
	    y: -1
	};
		jQuery(document).on("mousemove", function (event) {
        currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
    });

		/* initialize the external events
		-----------------------------------------------------------------*/

		jQuery('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			jQuery(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			jQuery(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/
          // var events_data = JSON.parse(json_events);
		jQuery('#calendar').fullCalendar({
		
			events: JSON.parse(json_events),
			 eventRender: function (event, element, view) {
				//to change color of the event with respect to response
				var e_start_date = getEventStartDate(event);
			    var list_dates = getEventListDates(event);
				var list_date = list_dates.split("::");
				
				for (var i = 0; i < list_date.length; i++) 
				{
					if(!jQuery('.fc-day[data-date="' + list_date[i] + '"]').hasClass("fc-past")){
								jQuery('.fc-day[data-date="' + list_date[i] + '"]').addClass("orange");
							}
							
							/*
						text += list_date[i] + "<br>";
					

						if(!jQuery('.fc-day[data-date="' + e_start_date + '"]').hasClass("fc-past")){
								jQuery('.fc-day[data-date="' + e_start_date+ '"]').addClass("orange");
							}
							*/
				}
				
				
			},
			dayRender: function(date, cell) {
				//alert(jQuery(cell).attr('class'));
				if(!jQuery(cell).hasClass("orange") && !jQuery(cell).hasClass("fc-past")){
					jQuery(cell).addClass("available");
				}
			  	 // can modify 'element' and add a class to it
				},
				header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month'
			},
			editable: false,
			droppable: false, 
			slotDuration: '00:30:00'
			
			
		});
		
function getEventStartDate(event) { 
	var dateobj = event.start;
	return dateobj;
}

function getEventListDates(event) { 
	var dateobj = event.selected_days;
	return dateobj;
}


function getEventEndDate(event) { 
	var dateobj = event.end;
	return dateobj;
}

	function getFreshEvents(){
	var user_id = '<?php echo $this->id ; ?>';
		jQuery.ajax({
			url: ajax_url+'get-events',
	        type: 'POST', // Send post data
	        data: 'type=fetch&user_id='+user_id,
	        async: false,
	        success: function(s){
	        	freshevents = s;
				
	        }
		});
		jQuery('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
	}


	function isElemOverDiv() {
        var trashEl = jQuery('#trash');

        var ofs = trashEl.offset();

        var x1 = ofs.left;
        var x2 = ofs.left + trashEl.outerWidth(true);
        var y1 = ofs.top;
        var y2 = ofs.top + trashEl.outerHeight(true);

        if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
            currentMousePos.y >= y1 && currentMousePos.y <= y2) {
            return true;
        }
        return false;
    }
		jQuery(".tab_layout_user_user_events").click(function(){
		jQuery('.fc-month-button').trigger('click');
		})
	});
jQuery(document).on('ready', function(){
	setTimeout(changecalcolor, 1000);
});

function changecalcolor(){
	clr = 1;
	jQuery('.fc-event').each(function() {
		if(clr == 1){
			jQuery(this).addClass("color1");	
		}else if(clr == 2){
			jQuery(this).addClass("color2");	
		}else if(clr == 3){
			jQuery(this).addClass("color3");	
		}
		if(clr == 3){
			clr = 0;	
		}
		clr++;
	});	
}
</script>


<style>
.orange {background-color: #ff0000;}
.fc-content-skeleton table tbody{
display:none;
}

	#trash{
		width:32px;
		height:32px;
		float:left;
		padding-bottom: 15px;
		position: relative;
	}
		
	#wrap {
		width: 1100px;
		margin: 0 auto;
	}
		
	#external-events {
		float: left;
		width: 150px;
		padding: 0 10px;
		border: 1px solid #ccc;
		background: #eee;
		text-align: left;
	}
		
	#external-events h4 {
		font-size: 16px;
		margin-top: 0;
		padding-top: 1em;
	}
		
	#external-events .fc-event {
		margin: 10px 0;
		cursor: pointer;
	}
		
	#external-events p {
		margin: 1.5em 0;
		font-size: 11px;
		color: #666;
	}
		
	#external-events p input {
		margin: 0;
		vertical-align: middle;
	}

	#calendar {
		float: left;
		width: 800px;
	}

</style>
	<div id='wrap'>

<div id='calendar'></div>
<div style='clear:both'></div>

	</div>