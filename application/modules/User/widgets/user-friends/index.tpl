<style type="text/css">
body#global_page_user-profile-index .tab_1362.layout_user_user_friends, body#global_page_user-profile-index .tab_1461.layout_user_user_friends {
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 1px 2px 0 rgba(1, 1, 1, 0.3);
    margin-top: 11px;
    padding-top: 0px;
}
</style>

<div class="profile_edit_info" style="padding:0 !important;">
            <div class="form-wrapper">
                <ul class='profile_friends user_pages' id="user_profile_friends">
                    <?php
                    $k=0;
                      foreach( $this->myfriends as $friends ):
                        $friend = Engine_Api::_()->getItem('user', $friends['user_id']);
                        ?>
                    
                        <li id="user_friend_<?php echo $friend->getIdentity() ?>">
                    
                          <?php echo $this->htmlLink($friend->getHref(), $this->itemPhoto($friend, 'thumb.icon'), array('class' => 'profile_friends_icon')) ?>
                    
                          <div class='profile_friends_body'>
                            <div class='profile_friends_status'>
                              <span>
                                <?php echo $this->htmlLink($friend->getHref(), $friend->getTitle()) ?>
                              </span>
                              <?php if($this->user->getIdentity() == $this->viewer->getIdentity()){?>
                              <div>
             							 <a style="background-image: url(<?php echo $this->baseUrl();?>/application/modules/User/externals/images/friends/remove.png);" class="buttonlink smoothbox remove_friend" href="<?php echo $this->baseUrl();?>members/friends/removefriend/user_id/<?php echo $friend->getIdentity();?>">Remove friend</a> 
                          </div>
                          <?php } ?>
                            </div>
                            
                          </div>
                          
                          
                    
                        </li>
                    
                      <?php $k++; endforeach;
                      	if($k==0){
                    		echo '<li style="text-align:left;">No Friends</li>';
                  		}
                      ?>
                        
                    </ul>
                    <img style="left: 41%;position: fixed;top: 75%;width: 21px; display:none;" class="process_image1" src="<?php echo $this->baseUrl();?>/public/custom/images/loading.gif">
            </div>
       
</div>
<input type="hidden" id="list_count" value="30"/>
<script type="text/javascript">

jQuery(document).scroll(function(){
	if (jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()){
				var list_count	   =	jQuery.trim(jQuery('#list_count').val());
				/*if(active_section == 'friend'){
					getresult(active_section, list_count);
				}else if(active_section == 'follower'){
					alert('11');
				}else if(active_section == 'following'){
					alert('111');
				}*/
				if(jQuery('#list_count').val() != '0'){
					getmoreresult(list_count);
				}
		}
	});
function getmoreresult(list_count) {	
	var section = 'friend';
	jQuery.post("<?php echo $this->baseUrl().'/members/getmorefriends';?>",{section:section, list_count:list_count,friend_id:'<?php echo $this->friend_id;?>'}, function(resp){
		//jQuery('.loading_image').hide();
		var response	=	jQuery.trim(resp).split('**');
		if(section == 'friend'){
				jQuery('#user_profile_friends').append(jQuery.trim(response[0]));
				jQuery('.process_image1').hide();
				if(response[0] == ''){
					jQuery('#list_count').val(0);
				}else{
					jQuery('#list_count').val(response[1]);
				}
		}
	});
	
}
</script>

