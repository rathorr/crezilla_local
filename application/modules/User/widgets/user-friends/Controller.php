<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_UserFriendsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	// Don't render this if not logged in
	$subjectId = Engine_Api::_()->core()->getSubject('user')->getIdentity();
    $viewer = Engine_Api::_()->user()->getViewer();
	
    // Don't render this if not authorized
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	$this->view->user = $user = Engine_Api::_()->core()->getSubject();
    if( !Engine_Api::_()->core()->hasSubject()) {
      return $this->setNoRender();
    }    
    
   	// Get lists if viewing own profile
    if( !$viewer->isSelf($user) ) {
		return $this->setNoRender();
	}
	
	//$is_friend	=	Engine_Api::_()->getDbtable('membership', 'user')->isFriend($viewer->getIdentity(), $user->getIdentity());
	 // Don't render this if friends are hidden
    if((($user->friend_setting == 2 && !$user->membership()->isMember($viewer)) || ($user->friend_setting ==3)) && $user->getIdentity() != $viewer->getIdentity()){
		return $this->setNoRender();	
	}
	
	  $this->view->error = true;
	  $this->view->totalFriend = 0; 
	  $this->view->friendCount = 0;
	  
	  $this->view->page = 1;
	
	  $page = (int)  $this->_getParam('page', 1);
		
		$table = Engine_Api::_()->getItemTable('user');
		$userTableName = $table->info('name');
	
		$searchTable = Engine_Api::_()->fields()->getTable('user', 'search');
		$searchTableName = $searchTable->info('name');
		
		$friendTableName = 'engine4_user_membership';
		
		// Contruct query for friends
		 $friendselect = $table->select()
		  ->from($userTableName)
		  ->setIntegrityCheck(false)
		  ->joinLeft($friendTableName, "`{$friendTableName}`.`user_id` = `{$userTableName}`.`user_id`")
		  ->joinLeft($searchTableName, "`{$searchTableName}`.`item_id` = `{$userTableName}`.`user_id`", null)
		  ->where("{$userTableName}.search = ?", 1)
		  ->where("{$friendTableName}.resource_id = ?", $viewer->getIdentity())
		  ->where("{$friendTableName}.active = ?", 1)
		  ->where("{$userTableName}.enabled = ?", 1)
		  ->limit(30,0)
		  ->order("{$userTableName}.displayname ASC")
		  //->where("{$userTableName}.user_id != ?", $viewer->getIdentity())
		  ;
		  
		 // Build paginator for friends
		$friendpaginator = $table->fetchAll($friendselect);
		$this->view->myfriends = $friendpaginator;
		
  }
  
   public function getChildCount()
  {
    return $this->_childCount;
  }
}