<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
 <!-- Seventh Block -->
 <style type="text/css">
	 .img-responsive {
	    width: 100%;
	    max-height: 215px;
	    object-fit: cover;
	    min-height: 215px;
	 }
 </style>
 <?php //echo '<pre> asdas'; print_r($this->similar_services); die();	?>
<div class="_pp_area">
	<div class="border-bottom col-xs-12">
		<div class="_pp_area_heading block_pymk">
			<span class="_pp_area_heading_icn"><i class="fa fa-user-circle-o" aria-hidden="true"></i></span> Similar Services
		</div>
	</div>
	<div class="col-xs-12">
		<div class="_pp_summary_area block_pymk">
			<div class="_pp_area_desc">
				<div class="row">
					<div id="block_pymk_carousel">
		  <?php foreach($this->similar_services as $arr_item ){ 
  			$user_id = $arr_item['user_id'];
  			$user = Engine_Api::_()->getItem('user', $user_id); 
			$talentpic  = Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($user_id);
			$talentpic  = ($talentpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$talentpic;
			$userating=Engine_Api::_()->getDbtable('reviews', 'user')->getAVGuserrating($user_id);

 			//if (!in_array($user_id, $this->existuserrequest, TRUE)){
          ?>						
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 member_list_item">
								<div class="member_list_item_area">
									<div class="col-xs-12 padrgtnone text-right member_list_item_views ">
										<i class="fa fa-eye" aria-hidden="true"></i> <?php echo $user->view_count;?>
									</div>
									<img src="<?php echo $talentpic;?>" class="img-responsive">
									<div class="_member_list_item_title"><a href="#" title=""> <?php echo $user->getTitle();?></a></div>
									<div class="_member_list_item_subtitle"><?php echo $userdesig=Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategoriesSearch($user_id); 
            						?></div>
									<div class="_member_list_item_detials">
										<div class="_member_list_item_action">
											<div class="col-md-6 col-sm-6 col-xs-6 text-left padlftnone">
												<a href="#" title=""><i class="fa fa-heart" aria-hidden="true"></i></a>
												<a href="/members/friends/shortlist/user_id/<?php echo $user_id;?>?q=" class="smoothbox" title=""><i class="_addfolderr_icn" aria-hidden="true"></i></a>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-6 text-right padrgtnone">
												<div class="_member_list_item_review">
											<span>
											<?php for($x=1;$x<=$userating;$x++) {?>
					                        <i class="fa fa-star" aria-hidden="true"></i>
					                <?php } 
					                if ((int) $userating != $userating) {  ?>
					                         <i class="fa fa-star-half" aria-hidden="true"></i>
					                 <?php }
					                     if($x !=1) { while ($x<=5) { ?>
					                          <i class="fa fa-star-o" aria-hidden="true"></i>
					                  <?php $x++; } }?>
											</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php }//}?>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
<!-- End Seventh Block -->