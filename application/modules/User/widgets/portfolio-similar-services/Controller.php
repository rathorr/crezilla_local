<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioSimilarServicesController extends Engine_Content_Widget_Abstract {
	
	protected $_childCount;
	public function indexAction(){

		// Don't render this if not logged in
	    $viewer = Engine_Api::_()->user()->getViewer();
		$userdcs  = Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategories($viewer->getIdentity());
		  $this->view->userdcs=$userdcs;
		  $udcoptions = array();
		    if($userdcs){
		      foreach($userdcs as $key=>$udsc){
		        $udcoptions[$key] = $udsc['dc_primary_id']; 
		      }
		    }
		 	$desigs=implode(',', $udcoptions);   

		$servicearr=array();
		$this->view->dasignations = $servicedesignation= Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getserviceDesignations();
		foreach ($servicedesignation as $key => $value) {
			$servicearr[$key]= $value['dasignatedsubcategory_id'];
		}
		$servicevals=implode(',', $servicearr); 
		$table = Engine_Api::_()->getItemTable('user');
		$userTableName = $table->info('name');
		$userselect = $table->select()
		  ->from($userTableName)
		  ->columns(array('avgrating'=> "ROUND(AVG(`engine4_user_reviews`.`rating`),1) AS avgrating"))	
		  ->joinLeft("engine4_user_fields_search", "`engine4_user_fields_search`.`item_id` = `{$userTableName}`.`user_id`", null)
		  ->joinLeft("engine4_user_reviews", "`engine4_user_reviews`.`user_id` = `{$userTableName}`.`user_id`",null)
		  ->joinLeft("engine4_user_dasignatedcategories","`engine4_user_dasignatedcategories`.`user_id`=`{$userTableName}`.`user_id`", null)
		  ->where("{$userTableName}.enabled = ?", 1)
		  ->where("{$userTableName}.level_id != ?", 1)
		  ->where("{$userTableName}.verified = ?", 1)
		  ->where("engine4_user_dasignatedcategories.dc_primary_id IN (".$servicevals.")")
		  ->where("engine4_user_dasignatedcategories.dc_primary_id IN (".$desigs.")")
		  ->group("{$userTableName}.user_id")
		  ->order("(avgrating) DESC")
		  ->limit(20,0);
		  //echo '<pre>';echo ($userselect);die();
		$userpaginator = $table->fetchAll($userselect);
		//echo '<pre>'; print_r($userpaginator); die();
		$this->view->similar_services = $userpaginator;

	}
	public function getChildCount()
	  {
	    return $this->_childCount;
	  }
}