<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
<style type="text/css">
	#core_menu_mini_menu > ul.header_menu_class > li.menu_core_mini.core_mini_profile > a > span > img
	{
		width: auto;
	}
	.custom_oupload_camera_icon {
		position: absolute !important; 
	}
	._pb_img img._pb_img_shadow
	{
		position: relative !important;
	}
</style>
<!-- Third Block -->
<div id="_portfolio_social1">
	<div class="_ps_common_heading">PINNED POST</div>
	<div class="_pp_area _ps_pinned_post_block">
		<div class="_ps_pinned_post_block_head">
			<div class="_ps_pinned_post_img"><img src="application/modules/User/externals/portfolio/img/_ps_logo.jpg" alt="" /></div>
			<div class="_ps_pinned_post_title">CreZILLA</div>
			<div class="_ps_pinned_post_subtitle">@Admin</div>
		</div>
		<div class="_ps_pinned_post_block_desc">
			<p>Hi Guys, just keep posting your work and sevices to get in touch with people.</p>
		</div>
	</div>
</div>
<!-- End Third Block -->