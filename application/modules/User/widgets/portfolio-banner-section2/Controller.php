<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioBannerSection2Controller extends Engine_Content_Widget_Abstract {
	
	public function indexAction(){

		// Don't render this if not logged in
	    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	    if( !$viewer->getIdentity() ) {
	      return $this->setNoRender();
	    }
	    /*
	      // Get subject and check auth
    $subject = Engine_Api::_()->core()->getSubject('user');
	$this->view->backurl	=	$_GET['q'];
	$this->view->is_shortlist		=	Engine_Api::_()->getDbtable('shortlists', 'user')->isShortlist($viewer->getidentity(), $subject->getIdentity());
	$is_owner = false;
	if($subject->getIdentity() == $viewer->getIdentity()){
			$is_owner = true;  
	}

	$this->view->is_owner = $is_owner;
    echo '<pre>'; print_r($subject); die();
    $this->view->user = $subject;*/

		if( !Engine_Api::_()->core()->hasSubject() ) {
      	// Can specifiy custom id
	      $id = $this->_getParam('id', null);
	      $subject = null;
	      if( null === $id ) {
	        $subject = Engine_Api::_()->user()->getViewer();
	        Engine_Api::_()->core()->setSubject($subject);
	      } else {
	        $subject = Engine_Api::_()->getItem('user', $id);
	        Engine_Api::_()->core()->setSubject($subject);
	      }
	    }

	    if( !Engine_Api::_()->core()->hasSubject() ) {
	      return $this->setNoRender();
	    }
	    // Get subject and check auth
	    $this->view->user = $user = Engine_Api::_()->core()->getSubject('user');
	    $is_owner = false;
		if($user->getIdentity() == $viewer->getIdentity()){
				$is_owner = true;  
		}
		$this->view->is_owner = $is_owner;
		$this->view->resource= $resource= Engine_Api::_()->getDbtable('membership', 'user')->checkFriendship($subject->getIdentity(),$viewer->getidentity());
		//echo "subject".$subject->getIdentity(); die();
		$this->view->is_shortlist= $is_shortlist= Engine_Api::_()->getDbtable('shortlists', 'user')->isShortlist($viewer->getidentity(), $subject->getIdentity());
		//echo "shortlists". $is_shortlist;die();
	    
	}
}