<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style type="text/css">
.rating_star_big_disabled {
    background-image: url("<?php echo $this->baseUrl();?>/application/modules/User/externals/images/star_big_disabled.png");
}
.ynmember_rating_star_generic {
    background-repeat: no-repeat;
    cursor: pointer;
    display: inline-block;
    float: left;
    font-size: 1px;
    height: 24px;
    width: 24px;
}
.rating_star_big {
    background-image: url("<?php echo $this->baseUrl();?>/application/modules/User/externals/images/star_big.png");
}
#twitlink a {
  text-transform: lowercase !important;
  }
#linklink a {
  text-transform: lowercase !important;
  }
  #weblink a
  {
    text-transform: lowercase !important;
  }
  #selfcomment span
  {
    /*border: 1px solid #ccc;*/
    margin-top: 10px;
    margin-left: 10px;
    font-style: italic;
  }
</style>
<script src="https://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl();?>/public/vtabs/css/jquery-jvert-tabs-1.1.4.css" />
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/vtabs/js/jquery-jvert-tabs-1.1.4.js"></script>
<script type="text/javascript">
/*jQuery(document).ready(function(){
	var t	=	jQuery('#main_tabs').find('.active').html();
	var review_anchor	=	jQuery('#main_tabs').find('.tab_layout_user_user_reviews').find('a');
	
	if(jQuery.trim(t) == ''){
		setTimeout(function(){jQuery('#main_tabs').find('.tab_layout_user_profile_complete_fields').find('a').click();}, 1000);
		
	}
});*/

jQuery(document).ready(function(){

	jQuery("#vtabs1").jVertTabs();
});
</script>
<?php if($this->is_owner){?>
<a href="<?php echo $this->baseUrl().'/members/edit/profile';?>" class="update_info">Update Info</a>
<?php }?>
<div id="vtabs1">
	<div>
		<ul>
        	<li><a href="#vtabs-content-a">Personal</a></li>
            <li><a href="#vtabs-content-b">Qualifications</a></li>           
            <li><a href="#vtabs-content-c">Professional</a></li>
             <li><a href="#vtabs-content-d">Key Fields</a></li>             
            <li><a href="#vtabs-content-e">Social Contacts</a></li>
            <?php if(!$this->owner){?>
            	<li><a href="#vtabs-content-f">Reviews</a></li>
            <?php } ?>
		</ul>
	</div>
	<div>
		<div id="#vtabs-content-a">
			<ul class="pf_info">
                <li>
               <span class="comp_label"> <?php echo $this->translate('Name :'); ?> </span>
                <span class="comp_details"><p id="summary"><?php $uname= Engine_Api::_()->getDbTable('users', 'user')->getUserFullName($this->subject->getidentity());
                if($uname==''){ echo $this->fieldStructure['First Name'].' '. $this->fieldStructure['Last Name'];} else { echo $uname;} ?></p></span>
              </li>
              
              <li>
                <span class="comp_label"><?php echo $this->translate('Gender :'); ?></span>
                <span class="comp_details"><p id="summary"> <?php echo $this->fieldStructure['Gender']; ?></p></span> 
              </li>
                 <?php if($this->is_owner){?>
                <li>
                <span class="comp_label"><?php echo $this->translate('Birthday :'); ?></span>
                <span class="comp_details"><p id="summary"> <?php echo  $this->fieldStructure['Birthday']; ?></p></span>
              </li>
            <?php } ?>
                 <li>
                <span class="comp_label"><?php echo $this->translate('Languages :'); ?></span>
                <span class="comp_details"><p id="summary"><?php echo ($this->fieldStructure['Language'])? $this->fieldStructure['Language']:' No Language Added';?>
                 <?php //echo $this->fieldStructure['Language']; ?></p></span>
              </li>
                  <li>
                <span class="comp_label"><?php echo $this->translate('Relationship Status :'); ?></span>
                <span class="comp_details"><p id="summary">
                <?php echo ($this->fieldStructure['Relationship Status'])? $this->fieldStructure['Relationship Status']:' No Relationship Status Added';?>
                 <?php //echo $this->fieldStructure['Relationship Status']; ?></p></span>
              </li>
                <li>
                <span class="comp_label"><?php echo $this->translate('Hobbies & Interests :'); ?></span>
                <span class="comp_details"><p id="summary"><?php echo ($this->fieldStructure['Hobbies & Interests'])? $this->fieldStructure['Hobbies & Interests']:' No Hobbies & Interests Added';?>
                 <?php //echo $this->fieldStructure['Hobbies & Interests']; ?></p></span>
                </li>
                <li>
                <span class="comp_label"><?php echo $this->translate('Professional Summary :'); ?>
                </span>
                <span class="comp_details"> <p id="summary">
                <?php 
                $professionalsummary  = $this->professionalsummarys;
                   
                    if(isset($professionalsummary[0])){
                      echo $professionalsummary[0]['summary'];
                    }
                    else
                    {
                      echo "No Professional Summary Added";
                    }
                   ?>
                   </p>
                   </span>
                </li>
              </ul>
		</div>
          
        <div id="#vtabs-content-b">
			<?php
                    $education  = $this->educations;
                    if(isset($education[0])){
                    echo '<div class="work_edu"><h4>Education</h4></div> <div class="edu_list">';
                    foreach($this->educations as $edu){?>
                        <div class="edu_box">
                            <span class="Pro_qua" id="qua_<?php echo $edu->education_id;?>"><?php echo trim($edu->qualification);?></span>
                            <?php if($edu->year || $edu->institute){?>
                                
                                    <?php if($edu->institute){?>
                                        <span class="inst pro_institute" id="ins_<?php echo $edu->education_id;?>">
                                            <?php echo trim($edu->institute);?>
                                        </span>
                                    <?php }
                                     if($edu->year){?>
                                        <span class="year pro_year" id="year_<?php echo $edu->education_id;?>">
                                            <?php echo trim($edu->year);?>
                                        </span>
                                    <?php } ?>
                                
                            <?php } ?>
                        </div>
                <?php }
                  echo '</div>';
                    }?>
                 
                
                    <?php
                    $award  = $this->awards;
                    if(isset($award[0])){
                    echo '<div class="work_edu"><h4>Awards</h4></div><div class="awd_list">';
                    foreach($this->awards as $awd){?>
                        <div class="adw_box">
                        <span class="Pro_qua"><?php echo trim($awd->category);?></span>
                            <span class="project Pro_award">
                              <?php echo trim($awd->award);?>
                                <?php echo ($awd->project || $awd->position)?', ':'';?> 
                             </span>
                             <span class="project Pro_project">
                              <?php echo trim($awd->project);?>
                                <?php echo ($awd->position || $awd->year)?', ':'';?> 
                             </span>
                            <span class="project Pro_project">
                              <?php echo trim($awd->position);?>
                                <?php echo ($awd->year)?', ':'';?>
                            </span>
                             <?php if($awd->year){?>
                            <span class="project Pro_year">
                              <?php echo trim($awd->year);?>
                            </span>
                            <?php } ?>
                        </div>
                        
                    <?php }
                    echo '</div>';
                    }?>
		</div>
     
		<div id="#vtabs-content-c">
      <?php
       $workexp = $this->workexps;
                   
                    if(isset($workexp[0])){?>
                    <div class="work_edu"><h4>Work Experience</h4></div>
                    <div class="exp_list">
                    <?php foreach($this->workexps as $work){?>
                        <div class="exp_box">
                        <?php if($work->workexp){?>
                                <span class="Pro_qua exp_year" id="expyear_<?php echo $pro->projects_id;?>">
                                   <?php echo trim($work->workexp);?> Years
                                </span>
                            <?php } ?>                                             
                           
                        </div>
                    <?php }?>
                        </div>
                    <?php }?>
             <?php
                    $experience = $this->experiences;
                   
                    if(isset($experience[0])){?>
                    <div class="work_edu"><h4>Employment History</h4></div>
                    <div class="exp_list">
                    <?php foreach($this->experiences as $exp){?>
                        <div class="exp_box">
                        <?php if($exp->fromyear){?>
                            <span class="Pro_desig" id="rol_<?php echo $exp->experience_id;?>"><?php echo trim($exp->role_name);?>
                            </span> 
                            <span class="client exp_client" id="clnt_<?php echo $exp->experience_id;?>">
                              <?php echo trim($exp->client);?>
                            </span>
                            <span class="year exp_year" id="expyear_<?php echo $exp->experience_id;?>">
                                From <?php echo trim($exp->fromyear);?> To <?php echo trim($exp->toyear);?>
                            </span>
                            <?php } ?>
                            <span class="pastexp exp_pastexp" id="pastexp_<?php echo $exp->experience_id;?>">
                                <?php echo trim($exp->pastexp);?>
                            </span>
                                                                                      
                           
                        </div>
                    <?php }?>
                        </div>
                    <?php }?>

                    <?php
                    $project = $this->projects;
                   /*echo '<pre>';
                    print_r($project);
                    die();*/
                    if(isset($project[0])){?>
                    <div class="work_edu"><h4>Project History</h4></div>
                    <div class="exp_list">
                    <?php foreach($this->projects as $pro){?>
                        <div class="exp_box">
                         <span class="Pro_desig" id="rol_<?php echo $pro->projects_id;?>"><?php echo trim($pro->desig_name);?></span> 
                          <span class="client exp_client" id="clnt_<?php echo $pro->projects_id;?>">
                                <?php echo trim($pro->clientp);?>
                            </span>
                        <?php if($pro->year){?>
                                <span class="year exp_year" id="expyear_<?php echo $pro->projects_id;?>">
                                   <?php echo trim($pro->year);?>
                                </span>
                            <?php } ?>
                                                        
                            <span class="pro exp_project" id="exppro_<?php echo $pro->projects_id;?>">
                                <?php echo trim($pro->project);?>
                            </span>                      
                           
                            <span class="pastexp exp_pastexp" id="pastexp_<?php echo $pro->projects_id;?>">
                                <?php echo trim($pro->pastpexp);?>
                            </span>
                                                                                      
                           
                        </div>
                    <?php }?>
                        </div>
                    <?php }?>
                        <?php
                    $unionmembership  = $this->unionmemberships;
                    if(isset($unionmembership[0])){
                    echo '<div class="work_edu"><h4>Professional Memberships</h4></div> <div class="edu_list">';
                    foreach($this->unionmemberships as $mem){?>
                        <div class="mem_box">
                            <span class="Pro_qua" id="mem_<?php echo $mem->unionmemberships_id;?>"><?php echo trim($mem->unionmembership);?></span>
                           
                        </div>
                <?php }
                  echo '</div>';
                    }?>

                        <?php
                    /*$professionalsummary  = $this->professionalsummarys;
                    if(isset($professionalsummary[0])){
                    echo '<div class="work_edu"><h4>Professional Summary</h4></div> <div class="edu_list">';
                    foreach($this->professionalsummarys as $psum){?>
                        <div class="mem_box">
                            <span class="Pro_qua" id="psum_<?php echo $psum->professionalsummarys_id;?>"><?php echo trim($psum->summary);?></span>
                           
                        </div>
                <?php }
                  echo '</div>';
                    }
                    */?>
                    
                    
		</div>
       <div id="#vtabs-content-d">
        <ul class="pf_info">
     <!--  <li>
               <span class="comp_label"> <?php echo $this->translate('Designation'); ?> </span>
                <span class="comp_details" id="user_listings">
                <div id="listingdiv">
                    <?php if($this->userdes){$i=0;
                        foreach($this->userdes as $desig){
                        echo "<p id='".$desig['dc_primary_id']."'>
                        <span class='uprofile_set'>
                       <span>".$desig['dasignatedcategory_title']."</span>";?>
                        <?php echo "</span></p>";
                        $i++;
                         }
                     }
                    if($i==0){ ?>
                    No Designation added yet.
                    <?php } ?>
                </div>
                </span>
              </li> -->
              <li>
               <span class="comp_label"> <?php echo $this->translate('Designation'); ?> </span>
                <span class="comp_details" id="user_listings">
                <div id="listingdiv">
                    <?php if($this->userdesigs){$i=0;
                        foreach($this->userdesigs as $desig){
                        echo "<p><span class='uprofile_set'>
                       <span>".$desig."</span>";?>
                        <?php echo "</span></p>";
                        $i++;
                         }
                     }
                    if($i==0){ ?>
                    No Designation added yet.
                    <?php } ?>
                </div>
                </span>
              </li>
              <li>
               <span class="comp_label"> <?php echo $this->translate('Domain Experience'); ?> </span>
                <span class="comp_details" id="user_listings">
                <div id="listingdiv">
                    <?php if($this->useriexps){$i=0;
                        foreach($this->useriexps as $exps){
                        echo "<p id='".$exps['ind_primary_id']."'>
                        <span class='uprofile_set'>
                       <span>".$exps['industryexperience_title']."</span>";?>
                        <?php echo "</span></p>";
                        $i++;
                         }
                     }
                    if($i==0){ ?>
                    No Domain Experience added yet.
                    <?php } ?>
                </div>
                </span>
              </li>
              
              <li>
                <span class="comp_label"><?php echo $this->translate('Functional Experience'); ?></span>
                  <span class="comp_details" id="user_listings">
                <div id="listingdiv">
                    <?php if($this->userfas){$i=0;
                        foreach($this->userfas as $exps){
                        echo "<p id='".$exps['fa_primary_id']."'>
                        <span class='uprofile_set'>
                       <span>".$exps['functionalareas_title']."</span>";?>
                        <?php echo "</span></p>";
                        $i++;
                         }
                     }
                    if($i==0){ ?>
                    No Functional Experience added yet.
                    <?php } ?>
                </div>
                </span>
              </li>
              
               <li>
                <span class="comp_label"><?php echo $this->translate('Skills'); ?></span>
                  <span class="comp_details" id="user_listings">
                <div id="listingdiv">
                    <?php if($this->userskills){$i=0;
                        foreach($this->userskills as $skills){
                        echo "<p id='".$skills['skill_primary_id']."'>
                        <span class='uprofile_set'>
                       <span>".$skills['skill_title']."</span>";?>
                        <?php echo "</span></p>";
                        $i++;
                         }
                     }
                    if($i==0){ ?>
                    No Skill added yet.
                    <?php } ?>
                </div>
                </span>
              </li>
               <li>
                <span class="comp_label"><?php echo $this->translate('Key Clients'); ?></span>
                  <span class="comp_details" id="user_listings">
                <div id="listingdiv">
                    <?php if($this->keyclients){$i=0;
                        foreach($this->keyclients as $clients){
                        echo "<p><span class='uprofile_set'><span>".$clients."</span>";?>
                        <?php echo "</span></p>";
                        $i++;
                         }
                     }
                    if($i==0){ ?>
                    No Key Client added yet.
                    <?php } ?>
                </div>
                </span>
              </li>
               <li>
                <span class="comp_label"><?php echo $this->translate('Key Employers'); ?></span>
                  <span class="comp_details" id="user_listings">
                <div id="listingdiv">
                    <?php if($this->keyemployers){$i=0;
                        foreach($this->keyemployers as $employer){
                        echo "<p><span class='uprofile_set'><span>".$employer."</span>";?>
                        <?php echo "</span></p>";
                        $i++;
                         }
                     }
                    if($i==0){ ?>
                    No Key Client added yet.
                    <?php } ?>
                </div>
                </span>
              </li>
              <li>
                <span class="comp_label"><?php echo $this->translate('Key Projects'); ?></span>
                  <span class="comp_details" id="user_listings">
                <div id="listingdiv">
                    <?php  if($this->userpros){$i=0;
                        foreach($this->userpros as $userpros){
                        echo "<p id='".$userpros['projects_id']."'>
                        <span class='uprofile_set'>
                       <span>".$userpros['project']."</span>";?>
                        <?php echo "</span></p>";
                        $i++;
                         }
                     }
                    if($i==0){ ?>
                    No Key Project added yet.
                    <?php } ?>
                </div>
                </span>
              </li>

              <!-- <li>
                <span class="comp_label"><?php echo $this->translate('Key Clients'); ?></span>
                  <span class="comp_details" id="user_listings">
                <div id="listingdiv">
                    <?php if($this->userclients){$i=0;
                        foreach($this->userclients as $clients){
                        echo "<p><span class='uprofile_set'><span>".$clients."</span>";?>
                        <?php echo "</span></p>";
                        $i++;
                         }
                     }
                    if($i==0){ ?>
                    No Key Client added yet.
                    <?php } ?>
                </div>
                </span>
              </li> -->
              
              </ul>
    </div>
          <div id="#vtabs-content-e">
       <?php $contactinfo  = $this->contactinfos;
                    if(isset($contactinfo[0])){
                     foreach($this->contactinfos as $con){?>
      <ul class="pf_info">
       <?php if($this->is_owner){?>
      <!--  <div class="contact_add"><h4>Address</h4></div>
                <li>
               <span class="comp_label"> <?php echo $this->translate('House No :'); ?> </span>
                <span class="comp_details"><p id="summary"><?php echo $con->houseno; ?></p></span>
              </li>
                         
              <li>
                <span class="comp_label"><?php echo $this->translate('Street/ Building :'); ?></span>
                <span class="comp_details"><p id="summary"> <?php echo $con->street; ?></p></span> 
              </li>
              
             
                <li>
                <span class="comp_label"><?php echo $this->translate('Landmark :'); ?></span>
                <span class="comp_details"><p id="summary"> <?php echo $con->landmark; ?></p></span>
              </li>
            
                 <li>
                <span class="comp_label"><?php echo $this->translate('City :'); ?></span>
                <span class="comp_details"><p id="summary"><?php echo $this->fieldStructure['City']; ?></p></span>
              </li>
                  <li>
                <span class="comp_label"><?php echo $this->translate('State :'); ?></span>
                <span class="comp_details"><p id="summary"><?php echo $this->fieldStructure['State']; ?></p></span>
              </li>
                <li>
                <span class="comp_label"><?php echo $this->translate('Pincode :'); ?></span>
                <span class="comp_details"><p id="summary"><?php echo $this->fieldStructure['Pincode']; ?></p></span>
                </li>
                <li>
                <span class="comp_label"><?php echo $this->translate('Country :'); ?>
                </span>
                <span class="comp_details"> <p id="summary"><?php echo $this->fieldStructure['Country']; ?></p></span>
                </li>
                <div class="contact_head"><h4>Contact Numbers</h4></div>
                
                 <li>
               <span class="comp_label"> <?php echo $this->translate('Home :'); ?> </span>
                <span class="comp_details"><p id="summary"><?php echo $con->countrycodehome."-".$con->contactnohome;; ?></p></span>
              </li>
           
              <li>
               <span class="comp_label"> <?php echo $this->translate('Office :'); ?> </span>
                <span class="comp_details"><p id="summary"><?php echo $con->countrycodeoffice."-".$con->contactnooffice;; ?></p></span>
              </li> -->
            <?php }?>
              <div class="contact_head"><h4>Social Information</h4></div>
                 <li>
                <span class="comp_label"><?php echo $this->translate('Linkedin :'); ?></span>
                <span class="comp_details"><p id="summary"><?php echo $con->linkedin; ?></p></span>
              </li>
                  <li>
                <span class="comp_label"><?php echo $this->translate('Twitter :'); ?></span>
                <span class="comp_details"><p id="summary"><?php echo $con->twittera; ?></p></span>
              </li>
                <li>
                <span class="comp_label"><?php echo $this->translate('Instagram :'); ?></span>
                <span class="comp_details"><p id="summary"><?php echo $con->instagram;?></p></span>
                </li>
                <li>
                <span class="comp_label"><?php echo $this->translate('Google :'); ?>
                </span>
                <span class="comp_details"> <p id="summary"><?php echo $con->google;?></p></span>
                </li>
                     <li>
                <span class="comp_label"><?php echo $this->translate('Facebook :'); ?></span>
                <span class="comp_details"><p id="summary"><?php echo $con->facebooka; ?></p></span>
              </li>
                <li>
                <span class="comp_label"><?php echo $this->translate('Website :'); ?></span>
                <span class="comp_details"><p id="summary"><?php echo $con->website;?></p></span>
                </li>
              
              </ul>
              <?php }
                
                    }?>
    </div>
        <div id="#vtabs-content-f">
			<?php $reviews	=	$this->reviews;
              if($reviews && count($reviews)>0){
                foreach($reviews as $rev){
                //echo '<pre>';
                //print_r($rev);
                //die();?>
                <div class="reviews-section" id="main_row_<?php echo $rev['review_id'];?>">
            <div id='reviews'>
                    <div class="main_div">
                        <div class="img">
                            <?php 
                                $user = Engine_Api::_()->getItem('user', $rev['reviewer_id']);
                                echo $this->itemPhoto($user, 'thumb.icon');
                            ?>
                        </div>
                        <div class="name">
                            <?php echo $rev['reviewer_name'];?>
                        </div>
                        <span class="review_time">
                            <?php echo $this->timestamp($rev['review_date']);?>
                        </span>
                    </div>
                    
                    <div class="ynmember-review-item-rate-item ynmember-clearfix">
                                <?php echo $this->partial('_review_rating_big.tpl', 'user', array('rate_number' => $rev['rating']));?>
                            </div>
                    
                    <div class="review_title">
                        <?php echo $rev['title'];?>
                    </div>
                    <div class="review" style="margin-bottom: 10px;">
                        <?php echo $rev['summary'];?>
                    </div>
                    <?php if($rev['user_comment']){?>
                        <div class="user_comment" id="selfcomment">
                           Response:<span><?php echo $rev['user_comment'];?></span>
                        </div>
                    <?php } ?>
                    </div>
                    
                    
                    
                    
                    <!--<?php if($this->is_owner){?>
                    <div class="publish_button" id="review_status_<?php echo $rev['review_id'];?>">
                        <?php if($rev['is_approved'] == '0') { ?><a href="javascript:void(0)" onclick="change_review_status('1', 'is_approved' , <?php echo $rev['review_id'];?>)" class="deactive_bt" style="cursor:pointer;font-weight: normal;">Un-published</a><?php }
                            if($rev['is_approved'] == '1') {?>
                            <a href="javascript:void(0)" onclick="change_review_status('0', 'is_approved' , <?php echo $rev['review_id'];?>)" class="active_bt" style="cursor:pointer;font-weight: normal;">Published</a><?php } ?>
                        
                    </div>
                    
                    <div class="delete_button">
                    <a href="javascript:void(0)" onclick="change_review_status('0', 'is_deleted' , <?php echo $rev['review_id'];?>)" class="delete_bt" style="cursor:pointer;font-weight: normal;">Delete</a>
                    </div>
                    <?php } ?>-->
            </div>
               <?php 
                }  
               }else{
                echo '<div class="no_review">No Reviews yet.</div>';
              }
              ?>
        </div>
	</div>
</div>

<script type="text/javascript" src="<?php echo $this->baseUrl()?>/externals/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl()?>/externals/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox-media.js"></script>
<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function(){
		jQuery('.fancybox').fancybox();
		
	jQuery(document).on('click','.add_endorse', function(){
		jQuery('.loading_image').show();
		//var skill_parent	=	jQuery.trim(jQuery(this).parent().html());
		var skill_id	=	jQuery.trim(jQuery(this).parent().parent().attr("id"));
		var skill_parent	=	jQuery(this).parent();
		var current		=	jQuery(this);
		jQuery(this).removeClass('add_endorse');
		jQuery.post("<?php echo $this->baseUrl().'/members/edit/addendorse'; ?>",{skill_id:skill_id,owner_id:<?php echo $this->subject->getIdentity();?>,user_id:'<?php echo $this->viewer->getIdentity();?>',type:'add'}, function(response){
			console.log(response);
			jQuery(current).addClass('add_endorse');
			jQuery('.loading_image').hide();
			var resp	=	response.split('*');
			if(jQuery.trim(resp[0])==0){
				jQuery('#'+resp[2]).find('.no_endorse').hide();
			}else{
				jQuery('#'+resp[2]).find('.no_endorse').show();	
			}
			
			if(jQuery.trim(resp[0])!=0){
				jQuery('#'+resp[2]).find('.no_endorse').attr('href','<?php echo $this->baseUrl();?>/members/edit/getendorsedby/endorse_by/'+resp[3]);
				jQuery('#'+resp[2]).find('.no_endorse').addClass('fancybox fancybox.iframe add_bg');
			}
			jQuery(skill_parent).find('.endorse_count').html(jQuery.trim(resp[0]));
			jQuery(skill_parent).find('.add_endorse').remove();
			jQuery(skill_parent).append('<a href="javascript:void(0);" class="remove_endorse" id="'+resp[1]+'">-</a>');
		});	
	});
	
	jQuery(document).on('click','.remove_endorse', function(){
		//var skill_parent	=	jQuery.trim(jQuery(this).parent().html());
		jQuery('.loading_image').show();
		var skill_id	=	jQuery.trim(jQuery(this).parent().parent().attr("id"));
		var endorse_id	=	jQuery(this).attr("id");
		var skill_parent	=	jQuery(this).parent();
		var current		=	jQuery(this);
		jQuery(this).removeClass('remove_endorse');
		jQuery.post("<?php echo $this->baseUrl().'/members/edit/addendorse'; ?>",{skill_id:skill_id,owner_id:<?php echo $this->subject->getIdentity();?>,type:'delete',endorse_id:endorse_id}, function(response){
			console.log(response);
			jQuery(current).addClass('remove_endorse');
			jQuery('.loading_image').hide();
			var resp	=	response.split('*');
			if(jQuery.trim(resp[0])==0){
				jQuery('#'+resp[2]).find('.no_endorse').hide();
			}else{
				jQuery('#'+resp[2]).find('.no_endorse').show();	
			}
			if(jQuery.trim(resp[0])!=0){
				jQuery('#'+resp[2]).find('.no_endorse').attr('href','<?php echo $this->baseUrl();?>/members/edit/getendorsedby/endorse_by/'+resp[3]);
				jQuery('#'+resp[2]).find('.no_endorse').addClass('fancybox fancybox.iframe add_bg');
			}
			jQuery(skill_parent).find('.endorse_count').html(jQuery.trim(resp[0]));
			jQuery(skill_parent).find('.remove_endorse').remove();
			jQuery(skill_parent).append('<a href="javascript:void(0);" class="add_endorse">+</a>');
		});	
	});
	
	/*jQuery(document).on('click','.endorse_count', function(){
			var endorse_by	=	jQuery(this).parent().find('.endorse_by').val();
			jQuery.post("<?php echo $this->baseUrl().'/members/edit/getendorsedby'; ?>",{endorse_by:endorse_by}, function(resp){
				jQuery('.show_endor_user').html(jQuery.trim(resp));
				jQuery('.show_endor_user').css("display", "block");
			});
	});*/

	});
</script>
  
    

