<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 10189 2014-04-30 18:51:06Z andres $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_ProfileCompleteFieldsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    // Don't render this if not authorized
    $user = Engine_Api::_()->core()->getSubject();
     $this->view->viewer	=	$viewer = Engine_Api::_()->user()->getViewer();
    if( !Engine_Api::_()->core()->hasSubject() ) {
      return $this->setNoRender();
    }

    // Get subject and check auth
    $this->view->subject	=	$subject = Engine_Api::_()->core()->getSubject('user');
	
    
	// REVIEWS
	$this->view->reviews = $reviews = 	Engine_Api::_()->getDbtable('reviews', 'user')->getReviews($subject->getIdentity());
	
	$this->view->tot_reviews	=	count($reviews);
    // Load fields view helpers
    $view = $this->view;
    $view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
	$this->view->skills = Engine_Api::_()->getDbtable('skills', 'user')->userSkillsEndorsements($subject->getIdentity());
	// Values
    $this->view->fieldStructure = $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);

    if( count($fieldStructure) <= 1 ) { // @todo figure out right logic
         return $this->setNoRender();
       }
	   
	//echo '<pre>';print_r($fieldStructure);die;
   
    $valuesStructure = array();
    $valueCount = 0;
	
	$show_hidden = $viewer->getIdentity()
                 ? ($subject->getOwner()->isSelf($viewer) || 'admin' === Engine_Api::_()->getItem('authorization_level', $viewer->level_id)->type)
                 : false;
		
	
	foreach( $fieldStructure as $map )
       {
			// Get field meta object
			$field = $map->getChild();
			$value = $field->getValue($subject);

		    if( !$field || $field->type == 'profile_type' ) continue;
		    if( !$field->display && !$show_hidden ) continue;
		    $isHidden = !$field->display;
		  
		    // Get first value object for reference
		    $firstValue = $value;
		    if( is_array($value) && !empty($value) ) {
		    	$firstValue = $value[0];
		    }
			
			// Evaluate privacy
			if( $usePrivacy && !empty($firstValue->privacy) && $relationship != 'self' ) {
				if( $firstValue->privacy == 'self' && $relationship != 'self' ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'friends' && ($relationship != 'friends' && $relationship != 'self') ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'registered' && $relationship == 'everyone' ) {
				  $isHidden = true; //continue;
				}
			}
			
			
		    //print_r($firstValue);
		    $helper = new Fields_View_Helper_FieldValueLoop();
			$helper->view = Zend_Registry::get('Zend_View');
			$tmp = $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);
			if( $field->type != 'heading' && (!$isHidden || $show_hidden )) {
				$val[$field->label] = $tmp;
			}
		}
	
	
	$sid = $subject->getIdentity();
	
	//$table = Engine_Api::_()->getItemTable('education');
    
	$this->view->is_owner	=	($subject->getOwner()->isSelf($viewer))?true:false;
	//$val['About Me'] =htmlspecialchars($val['About Me'],ENT_QUOTES);
	$this->view->fieldStructure = $val;
	//echo '<pre>';print_r($val);die;
	$this->view->profile_type	=	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());

	/*$roletable = Engine_Api::_()->getDbTable('roles', 'experience');
    $roleName = $roletable->info('name');*/
    $roletable = Engine_Api::_()->getDbTable('dasignatedsubcategories', 'classified');
    $roleName = $roletable->info('name');
	
	$edutable = Engine_Api::_()->getDbTable('educations', 'education');
    $eduName = $edutable->info('name'); 
	
	//$table = Engine_Api::_()->getItemTable('education');
    $eduselect = $edutable->select("$eduName.*")
					->where("$eduName.owner_id = $sid", 1)
					->where("$eduName.row_status = 1", 1)
					->order("$eduName.creation_date desc");
	// get the data
	 $useredu= $edutable->fetchAll($eduselect);
	 //echo '<pre>';
	 //print_r($useredu);
	 //die();
	 $this->view->educations =$useredu;
	 $memtable = Engine_Api::_()->getDbTable('unionmemberships', 'user');
    $memName = $memtable->info('name'); 
	
	//$table = Engine_Api::_()->getItemTable('education');
    $memselect = $memtable->select("$memName.*")
					->where("$memName.owner_id = $sid", 1)
					->where("$memName.row_status = 1", 1)
					->order("$memName.creation_date desc");
	// get the data
	 $usermem= $memtable->fetchAll($memselect);
	 //echo '<pre>';
	 //print_r($useredu);
	 //die();
	 $this->view->unionmemberships =$usermem;

	$exptable = Engine_Api::_()->getDbTable('experiences', 'experience');
    $expName = $exptable->info('name');
	
    $expselect = $exptable->select("$expName.*")
						->setIntegrityCheck(false)
						->joinLeft($roleName, "$roleName.dasignatedsubcategory_id = $expName.role_id", array('dasignatedsubcategory_name as role_name'))
                        ->where("$expName.owner_id = $sid", 1)
						->where("$expName.row_status = 1", 1)
                        ->order("$expName.creation_date DESC");
	// get the data
	$this->view->experiences =$exptable->fetchAll($expselect);
	
	$protable = Engine_Api::_()->getDbTable('projects', 'experience');
    $proName = $protable->info('name');
	
	$proselect = $protable->select("$proName.*")
						->setIntegrityCheck(false)
						->joinLeft($roleName, "$roleName.dasignatedsubcategory_id = $proName.desig_id", array('dasignatedsubcategory_name as desig_name'))
                        ->where("$proName.owner_id = $sid", 1)
						->where("$proName.row_status = 1", 1)
                        ->order("$proName.creation_date DESC");
		// get the data
	$this->view->projects =$protable->fetchAll($proselect);

	$awdtable = Engine_Api::_()->getDbTable('awards', 'experience');
    $awdName = $awdtable->info('name'); 
	
	//$table = Engine_Api::_()->getItemTable('education');
    $awdselect = $awdtable->select("$awdName.*")
					->where("$awdName.owner_id = $sid", 1)
					->where("$awdName.row_status = 1", 1)
					->order("$awdName.creation_date desc");
	// get the data
	$this->view->awards = $awdtable->fetchAll($awdselect);

	$workexptable = Engine_Api::_()->getDbTable('workexps', 'user');
    $workexpName = $workexptable->info('name'); 
		$workexpselect = $workexptable->select("$workexpName.*")
					->where("$workexpName.owner_id = $sid", 1)
					->where("$workexpName.row_status = 1", 1)
					->order("$workexpName.creation_date desc");
	// get the data
	$this->view->workexps = $workexptable->fetchAll($workexpselect);


	$sumtable = Engine_Api::_()->getDbTable('professionalsummarys', 'user');
		$sumName = $sumtable->info('name'); 
		
		$sumselect = $sumtable->select("$sumName.*")
						->where("$sumName.owner_id = $sid", 1)
						->where("$sumName.row_status = 1", 1)
						->order("$sumName.creation_date desc");
		// get the data
		$this->view->professionalsummarys = $sumtable->fetchAll($sumselect);

	//Designations
	$userdes	=	Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategories($user->getIdentity());
	$this->view->userdes	=$userdes;
	
	// industries
	$useriexps	=	Engine_Api::_()->getDbtable('industryexperiences', 'user')->userIndustryexperiences($user->getIdentity());
	$this->view->useriexps=$useriexps;

	//functional areas
	$userfas	=	Engine_Api::_()->getDbtable('functionalareas', 'user')->userFunctionalareas($user->getIdentity());
	$this->view->userfas	=	$userfas;
	
	//Skills 
	$userskills	=	Engine_Api::_()->getDbtable('skills', 'user')->userskills($user->getIdentity());
	$this->view->userskills	=	$userskills;

	$contable = Engine_Api::_()->getDbTable('contactinfos', 'user')->userContactinfos($user->getIdentity());
    $this->view->contactinfos=$contable;

    $userpros	=	Engine_Api::_()->getDbtable('projects', 'experience')->getProjectnames($user->getIdentity());
	$this->view->userpros	=	$userpros;

	// Get Designation
	$designations=array();
	$designations2=array();
	$designations3=array();
	$client=array();
	$client2=array();
	$userprodesig	=	Engine_Api::_()->getDbtable('projects', 'experience')->getDesignationPro($user->getIdentity());
	foreach ($userprodesig as $key => $value) {
		# code...
		$designations[$key]=$value->desig_name;
		$client[$key]=$value->clientp;
	}
	$userexpdesig	=	Engine_Api::_()->getDbtable('experiences', 'experience')->getDesignationExp($user->getIdentity());
	foreach ($userexpdesig as $key => $value) {
		# code...
		$designations2[$key]=$value->desig_name;
		$client2[$key]=$value->client;
	}
	foreach ($userdes as $key => $value) {
		$designations3[$key]=$value['dasignatedcategory_title'];
	}
	/*echo '<pre>';
	print_r($designations2);
	print_r($designations3);
	die();*/
	
	$this->view->userdesigs	=	array_unique(array_merge($designations,$designations2,$designations3));
	$this->view->userclients	=	array_unique(array_merge($client,$client2));
	$this->view->keyclients	=	$client;
	$this->view->keyemployers	=	$client2;
	
    
  }
}