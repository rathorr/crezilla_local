<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioPostedJobsController extends Engine_Content_Widget_Abstract {
	
	public function indexAction(){

		// Don't render this if not logged in
		$user = Engine_Api::_()->core()->getSubject();
	    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	    if( !$viewer->getIdentity() ) {
	      return $this->setNoRender();
	    }
	    $this->view->userid=$viewer->getIdentity();
	    $this->view->postedjobs=$postedjobs	=	Engine_Api::_()->getDbtable('classifieds', 'classified')->getPostedjobsViewWise($user->getIdentity());
	}
}