<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_ProfileViewsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	 
    // Don't render this if not authorized
    $viewer = Engine_Api::_()->user()->getViewer();
	$this->view->view_count	=	 $viewer->view_count;
  }
}