<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 10158 2014-04-10 19:07:53Z lucas $
 * @author     John
 */
?>
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl();?>/public/fancybox/jquery.fancybox.css" media="screen" />
<div class="ppl_profile_view">
<span>
<a href="<?php echo $this->baseUrl().'/members/viewed';?>" class="fancybox  fancybox.ajax" style="color:#328dc6 !important; font-size:18px;"><?php echo $this->view_count;?></a> people viewed your profile
</span>
</div>

<div class="grow_network">
<?php echo 'Connect with more people. Grow your network.';?>
</div>

<script type="text/javascript">
jQuery(document).ready(function()
{
 jQuery('.fancybox').fancybox();
});
</script>
