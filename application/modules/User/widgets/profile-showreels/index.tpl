<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl();?>/public/vtabs/css/jquery-jvert-tabs-1.1.4.css" />
<script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/vtabs/js/jquery-jvert-tabs-1.1.4.js"></script>

<link rel="stylesheet" href="<?php echo $this->baseUrl().'/application/modules/Album/externals/styles/prettyPhoto.css';?>" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $this->baseUrl().'/application/modules/Album/externals/scripts/jquery.prettyPhoto.js'?>"></script>
<style type="text/css">
  ul.videos_manage > li {
    border-bottom: 1px solid #ddd;
    padding-bottom: 0px !important;
    padding-top: 0px !important;
    margin-top: 0px !important;
}
@media screen and (max-width :768px) {
  ul.videos_manage > li {
  border-bottom: 1px solid #ddd;
  padding-bottom: 0px !important;
  padding-top: 5px !important;
  margin-top: 0px !important;
  }
}
</style>
<script type="text/javascript">
jQuery(document).ready(function(){

	jQuery("#vtabs2").jVertTabs();
});
</script>
<div id="vtabs2" style="min-height:90px;">
	<div>
		<ul>
        	<li><a href="#vtabs-content-x">Albums (<?php echo $this->totalalbums;?>)</a></li>
			<li><a href="#vtabs-content-y">Pdf (<?php echo $this->totalpdfs;?>)</a></li>
            <li><a href="#vtabs-content-z">Video/Audio (<?php echo $this->totalvideos;?>)</a></li>
		</ul>
	</div>
	<div>
		<div id="#vtabs-content-x" class="albums_list">
        	 <?php if( $this->totalalbums > 0 ): ?>
    <ul class='albums_manage'>
      <?php foreach( $this->albumpaginator as $album ): ?>
        <li>
          
          <div class="albums_manage_photo">
            <?php //echo $this->htmlLink(array('route' => 'album_specific', 'action' => 'editphotos', 'album_id' => $album->album_id), $this->itemPhoto($album, 'thumb.normal')) ?>
            
            <a class="thumbs_photo" href="<?php echo $this->baseUrl().'/albums/photo/view/album_id/'.$album->getIdentity().'/photo_id/'.$album->photo_id; ?>" rel="prettyPhoto[gallery1]">
                  <?php echo $this->itemPhoto($album, 'thumb.normal');?>               
              </a>
            
          </div>
      
          <div class="albums_manage_info">
            <h3><a class="thumbs_photo" href="<?php echo $this->baseUrl().'/albums/photo/view/album_id/'.$album->getIdentity().'/photo_id/'.$album->photo_id; ?>" rel="prettyPhoto[gallery1]">
                  <?php echo $album->getTitle();?>               
              </a></h3>
            <div class="albums_manage_info_photos">
              <?php echo $this->translate(array('%s photo', '%s photos', $album->count()),$this->locale()->toNumber($album->count())) ?>
            </div>
            <div class="albums_manage_info_desc">
              <?php echo $album->getDescription() ?>
            </div>
          </div>
        </li>
      <?php endforeach; ?>
      <?php if( $this->albumpaginator->count() > 1 ): ?>
        <br />
        <?php echo $this->paginationControl($this->paginator, null, null); ?>
      <?php endif; ?>
    </ul>
  <?php else: ?>
    <div class="tip">
      <span id="no-album">
        <?php echo $this->translate('You do not have any albums yet.');?>
        <?php if( $this->canCreate ): ?>
          <?php $create = $this->translate('Be the first to %1$screate%2$s one!', 
                          '<a href="'.$this->url(array('action' => 'upload')).'">', '</a>'); 
          ?>
          
        <?php endif; ?>
      </span>
    </div>  
  <?php endif; ?>
		</div>
		
        <div id="#vtabs-content-y">
              <ul class="pdf_manage">
            <?php if( $this->totalpdfs > 0 ): ?>
                  <?php foreach( $this->pdfpaginator as $pdf ): 
                      $rec= Engine_Api::_()->getDbTable('pdfs', 'pdf')->getPageFile($pdf->pdf_id);
                      $file=$rec[0]['pdf_file'];
                      $modified_date=$rec[0]['modified_date'];
                  ?>
               <li>
                  <div class="pdf_manage_photo">
                    <a href="<?php echo $this->baseUrl();?>/public/pdf/<?php echo $file;?>" target="_blank" class="wp_init"><img class="thumb_normal item_photo_album  thumb_normal" style="width:75px; height:75px;" alt="" src="<?php echo $this->baseUrl();?>/public/PDF-icon.png"></a>          </div>
                  <div class="pdf_manage_info">
                    <h3><a href="javascript:void(0);" class="wp_init">
                    	<?php echo $pdf->getTitle(); ?>
                       </a>
                    </h3>
                  </div>
                </li>
                <?php endforeach; ?>
                       
           <?php else: ?>
           	<li>No Pdfs</li>
           <?php endif; ?>
           </ul>
		</div>
        
        <div id="#vtabs-content-z">
           
             <?php if( $this->videopaginator->getTotalItemCount() > 0 ): ?>

  <ul class='videos_manage'>
    <?php foreach( $this->videopaginator as $item ): 
   //echo '<pre>'; print_r($item); echo $item['video_id']; echo $item['title']; echo '</pre>';
    ?>
 
    <li class="video<?php echo $i;?>">
      <div class="video_thumb_wrapper">
        <?php if ($item->duration):?>
        <span class="video_length">
          <?php
            if( $item->duration >= 3600 ) {
              $duration = gmdate("H:i:s", $item->duration);
            } else {
              $duration = gmdate("i:s", $item->duration);
            }
           
            echo $duration;
          ?>
        </span>
        <?php endif;?>
        <?php
          if( $item->photo_id ) {
            echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'thumb.normal'));
          } else {
            echo '<img alt="" src="' . $this->layout()->staticBaseUrl . 'application/modules/Video/externals/images/video.png">';
          }
        ?>
      </div>
      <div class="video_info">
        <h3>
          <?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?>
        </h3>
        
        <div class="video_stats">
          <span class="video_views"><?php echo $this->translate('Added');?> <?php echo $this->timestamp(strtotime($item->creation_date)) ?> </span>
          <span class="video_star"></span><span class="video_star"></span><span class="video_star"></span><span class="video_star"></span><span class="video_star_half"></span>
        </div>
        <?php if($item->status == 0):?>
          <div class="tip">
            <span>
              <?php echo $this->translate('Your video/audio is in queue to be processed - you will be notified when it is ready to be viewed.')?>
            </span>
          </div>
        <?php elseif($item->status == 2):?>
          <div class="tip">
            <span>
              <?php echo $this->translate('Your video/audio is currently being processed - you will be notified when it is ready to be viewed.')?>
            </span>
          </div>
        <?php elseif($item->status == 3):?>
          <div class="tip">
            <span>
             <?php echo $this->translate('Video/audio conversion failed. Please try %1$suploading again%2$s.', '<a href="'.$this->url(array('action' => 'create', 'type'=>3)).'">', '</a>'); ?>
            </span>
          </div>
        <?php elseif($item->status == 4):?>
          <div class="tip">
            <span>
             <?php echo $this->translate('Video/Audio conversion failed. Video format is not supported by FFMPEG. Please try %1$sagain%2$s.', '<a href="'.$this->url(array('action' => 'create', 'type'=>3)).'">', '</a>'); ?>
            </span>
          </div>
         <?php elseif($item->status == 5):?>
          <div class="tip">
            <span>
             <?php echo $this->translate('Video/Audio conversion failed. Audio files are not supported. Please try %1$sagain%2$s.', '<a href="'.$this->url(array('action' => 'create', 'type'=>3)).'">', '</a>'); ?>

            </span>
          </div>
         <?php elseif($item->status == 7):?>
          <div class="tip">
            <span>
             <?php echo $this->translate('Video/Audio conversion failed. You may be over the site upload limit.  Try %1$suploading%2$s a smaller file, or delete some files to free up space.', '<a href="'.$this->url(array('action' => 'create', 'type'=>3)).'">', '</a>'); ?>

            </span>
          </div>
        <?php endif;?>
      </div>
    </li>
    <?php $i++; endforeach; ?>
  </ul>
  <?php else:?>
    <div class="tip">
     <span>

      <?php echo $this->translate('You do not have any videos/audios.');?>
      <?php if ($this->can_create): ?>
        <?php echo $this->translate('Get started by %1$sposting%2$s a new video/audio.', '<a href="'.$this->url(array('action' => 'create')).'">', '</a>'); ?>
      <?php endif; ?>
      </span>
    </div>

  <?php endif; ?>
  <?php echo $this->paginationControl($this->videopaginator); ?>
		</div>
        	
	</div>
</div>