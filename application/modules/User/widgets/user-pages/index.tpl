<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style type="text/css">
  .page_list_item_photo  
  {
    float: left;
  }
  .page_list_item_info
  {
    display: inline-block;
    float: left;
    margin-left: 10px;
  }
  .page_list_title, .page_list_info
  {
    margin-left: 10px;
    clear: none !important;
  }
</style>
<ul class="user_pages profile_friends">
 <?php $pages	=	$this->pages;
  if($pages && count($pages)>0){
   $i=1;
  	foreach($pages as $page){
       $page_id = $page['page_id'];
       $page=Engine_Api::_()->getItem('page', $page_id);
     $page_likes=Engine_Api::_()->getDbtable('likes', 'core')->getlikes('page',$page_id);
     if(!$page_likes)
     {
      $page_likes= 0;
     }
    ?>
    <li>
      	<div class="page_list_item_photo  <?php if($pageInfo['featured']) echo 'featured_page'?>">
          <a href="<?php echo $page->getHref()?>">
            <span style="background-image: url(<?php echo $page->getPhotoUrl('thumb.normal'); ?>);"></span>
          </a>
        </div>   
        
        <div class="page_list_item_info">
          <div class="page_list_title">
            <a href="<?php echo $page->getHref(); ?>">
              <?php echo $page->getTitle(); ?>
            </a>
          </div>
          
          <div class="page_list_info">

            <div class="l">
              <div class="page_list_submitted">
                <?php echo $this->timestamp($page->creation_date); ?> 
                <br />
                <?php if (Engine_Api::_()->getDbTable('settings', 'core')->getSetting('page.show.owner', 0) == 1):?>
                  <?php echo  "Posted by" ; ?>
                  <?php echo "Posted by"; ?>
                  <a href="<?php echo $page->getOwner()->getHref(); ?>"><?php echo $page->getOwner()->getTitle(); ?></a>
                <?php endif;?>
                <?php //echo $page->category; ?>
                 <?php echo $page_likes." ". $this->translate('likes');?>
                 <?php echo $page->view_count. " ". $this->translate("views"); ?>
              
              </div>
            </div>

          </div>
           
        </div>

         

      </li>	
  <?php
  	if($i%4==0){
    	echo '</ul><ul class="user_pages profile_friends">';
    }
   $i++;}  
   echo '</ul>';
   }else{
  	echo '<li class="" style="text-align:left;">No Pages</li>';
  }
  ?>
  
