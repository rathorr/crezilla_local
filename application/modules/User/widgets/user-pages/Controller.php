<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_UserPagesController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	// Don't render this if not logged in
	$subjectId = Engine_Api::_()->core()->getSubject('user')->getIdentity();
    $viewer = Engine_Api::_()->user()->getViewer();
    
	$table = Engine_Api::_()->getDbtable('pages', 'page');
    $membershipTbl = Engine_Api::_()->getDbtable('membership', 'page');

    $select = $table
      ->select()->setIntegrityCheck(false)
      ->from(array('pages' => 'engine4_user_pages'))
	  ->join(array('page' => 'engine4_page_pages'), 'pages.user_id = page.user_id', array())
	  ->joinLeft(array('m' => 'engine4_page_membership'), "pages.page_id = m.resource_id AND m.type = 'ADMIN'", array())
      ->joinLeft(array('fv' => 'engine4_page_fields_values'), "fv.item_id = pages.page_id", array())
      ->joinLeft(array('fo' => 'engine4_page_fields_options'), "fo.option_id = fv.value", array('category' => 'label', 'category_id' => 'option_id'))
       ->where('pages.user_id = ?', $subjectId)
	   ->order('page.creation_date DESC')
	   ->group("pages.page_id");
	  
	$this->view->pages	=	$result = $table->fetchAll($select);
	
	// Add count to title if configured
    if( $this->_getParam('titleCount', false) && count($result) > 0 ) {
      $this->_childCount = count($result);
    }
  }
  
   public function getChildCount()
  {
    return $this->_childCount;
  }
}