<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 10128 2014-01-24 18:47:54Z lucas $
 * @author     John
 */
?>

<script type="text/javascript">
  var toggleFriendsPulldown = function(event, element, user_id) {
    event = new Event(event);
    if( $(event.target).get('tag') != 'a' ) {
      return;
    }
    
    $$('.profile_friends_lists').each(function(otherElement) {
      if( otherElement.id == 'user_friend_lists_' + user_id ) {
        return;
      }
      var pulldownElement = otherElement.getElement('.pulldown_active');
      if( pulldownElement ) {
        pulldownElement.addClass('pulldown').removeClass('pulldown_active');
      }
    });
    if( $(element).hasClass('pulldown') ) {
      element.removeClass('pulldown').addClass('pulldown_active');
    } else {
      element.addClass('pulldown').removeClass('pulldown_active');
    }
    OverText.update();
  }
  var handleFriendList = function(event, element, user_id, list_id) {
    new Event(event).stop();
    if( !$(element).hasClass('friend_list_joined') ) {
      // Add
      en4.user.friends.addToList(list_id, user_id);
      element.addClass('friend_list_joined').removeClass('friend_list_unjoined');
    } else {
      // Remove
      en4.user.friends.removeFromList(list_id, user_id);
      element.removeClass('friend_list_joined').addClass('friend_list_unjoined');
    }
  }
  var createFriendList = function(event, element, user_id) {
    var list_name = element.value;
    element.value = '';
    element.blur();
    var request = en4.user.friends.createList(list_name, user_id);
    request.addEvent('complete', function(responseJSON) {
      if( responseJSON.status ) {
        var topRelEl = element.getParent();
        $$('.profile_friends_lists ul').each(function(el) {
          var relEl = el.getElement('input').getParent();
          new Element('li', {
            'html' : '\n\
<span><a href="javascript:void(0);" onclick="deleteFriendList(event, ' + responseJSON.list_id + ');">x</a></span>\n\
<div>' + list_name + '</div>',
            'class' : ( relEl == topRelEl ? 'friend_list_joined' : 'friend_list_unjoined' ) + ' user_profile_friend_list_' + responseJSON.list_id,
            'onclick' : 'handleFriendList(event, $(this), \'' + user_id + '\', \'' + responseJSON.list_id + '\');'
          }).inject(relEl, 'before');
        });
        OverText.update();
      } else {
        //alert('whoops');
      }
    });
  }
  var deleteFriendList = function(event, list_id) {
    event = new Event(event);
    event.stop();

    // Delete
    $$('.user_profile_friend_list_' + list_id).destroy();

    // Send request
    en4.user.friends.deleteList(list_id);
  }
  en4.core.runonce.add(function(){
    $$('.profile_friends_lists input').each(function(element) { new OverText(element); });
    
    <?php if( !$this->renderOne ): ?>
    var anchor = $('user_profile_friends').getParent();
    $('user_profile_friends_previous').style.display = '<?php echo ( $this->friends->getCurrentPageNumber() == 1 ? 'none' : '' ) ?>';
    $('user_profile_friends_next').style.display = '<?php echo ( $this->friends->count() == $this->friends->getCurrentPageNumber() ? 'none' : '' ) ?>';

    $('user_profile_friends_previous').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->friends->getCurrentPageNumber() - 1) ?>
        }
      }), {
        'element' : anchor
      })
    });

    $('user_profile_friends_next').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->friends->getCurrentPageNumber() + 1) ?>
        }
      }), {
        'element' : anchor
      })
    });
    <?php endif; ?>

    $$('.friends_lists_menu_input input').each(function(element){
      element.addEvent('blur', function() {
        this.getParents('.drop_down_frame')[0].style.visibility = "hidden";
      });
    });
  });
</script>
<h4 class=" connection_txt "> Connections 
<?php if($this->totalfriends>4){?>
	<a class="paj wp_init connection_see_ll" href="<?php echo $this->baseUrl().'/members/index/friends/friend_id/'.$this->subject->getIdentity();?>">See All</a>
<?php } ?>
</h4>
<ul class='profile_friends' id="user_profile_friends">
  <?php 
  $tshow=0;
  foreach( $this->friends as $membership ):
   $tshow++;
  if($tshow <= 4){
    if( !isset($this->friendUsers[$membership->resource_id]) ) continue;
    $member = $this->friendUsers[$membership->resource_id];
    ?>

    <li id="user_friend_<?php echo $member->getIdentity() ?>">
		
        <?php $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($member->getIdentity());
if($pic == '')$pic	=	$this->baseUrl().'/application/modules/User/externals/images/nophoto_user_thumb_profile.png';
 ?>
        
        <div id='friend_photo' style="background-image:url(<?php echo $pic; ?>);margin:0 auto;" class="wall_background set_friend">

</div>
      <div class='profile_friends_body_div text-center'>
            <?php echo $this->htmlLink($member->getHref(), $member->getTitle()) ?>
          
      </div>
      
      <div class='profile_friends_options_div text-center'>
        <?php echo $this->userFriendship($member) ?>
      </div>

    </li>

  <?php } endforeach ?>
  <?php if($this->totalfriends<1){?>
	<li style="margin:0 19px !important;">No Connections</li>
<?php } ?>
</ul>

<div style="display:none;">
  <div id="user_profile_friends_previous" class="paginator_previous" style="display:none;">
    <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Previous'), array(
      'onclick' => '',
      'class' => 'buttonlink icon_previous'
    )); ?>
  </div>
  <div id="user_profile_friends_next" class="paginator_next" style="display:none;">
    <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Next'), array(
      'onclick' => '',
      'class' => 'buttonlink_right icon_next'
    )); ?>
  </div>
</div>
