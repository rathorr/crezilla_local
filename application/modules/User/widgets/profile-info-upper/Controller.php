<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_ProfileInfoUpperController extends Engine_Content_Widget_Abstract
{
  
  public function indexAction()
  {
	 $subject = null;
    if( !Engine_Api::_()->core()->hasSubject() )
    {
      $subject = Engine_Api::_()->user()->getViewer();
      if( null !== $subject )
      {
          Engine_Api::_()->core()->setSubject($subject);
      }
	}
    // Don't render this if not authorized
    $viewer = Engine_Api::_()->user()->getViewer();
    if( !Engine_Api::_()->core()->hasSubject() ) {
         return $this->setNoRender();
    }
	
	// Get subject and check auth
   $this->view->subject	= $subject = Engine_Api::_()->core()->getSubject('user');
	/*if( !$subject->authorization()->isAllowed($viewer, 'view') ) {
		return $this->setNoRender();
	}*/
	
	// Get user type
	$fieldsByAlias = Engine_Api::_()->fields()->getFieldsObjectsByAlias($subject);
	if( !empty($fieldsByAlias['profile_type']) ) {
      $optionId = $fieldsByAlias['profile_type']->getValue($subject);
      if( $optionId ) {
        $optionObj = Engine_Api::_()->fields()
          ->getFieldsOptions($subject)
          ->getRowMatching('option_id', $optionId->value);
        if( $optionObj ) {
          $this->view->memberType = $optionObj->label;
        }
      }
    }
	
	// Calculate viewer-subject relationship
    $usePrivacy = ($subject instanceof User_Model_User);
    if( $usePrivacy ) {
      $relationship = 'everyone';
      if( $viewer && $viewer->getIdentity() ) {
        if( $viewer->getIdentity() == $subject->getIdentity() ) {
          $relationship = 'self';
        } else if( $viewer->membership()->isMember($subject, true) ) {
          $relationship = 'friends';
        } else {
          $relationship = 'registered';
        }
      }
    }
   
    
	// Load fields view helpers
    $view = $this->view;
    $view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
   
    // Values
    $this->view->fieldStructure = $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);

    if( count($fieldStructure) <= 1 ) { // @todo figure out right logic
         return $this->setNoRender();
       }
	   
	//print_r($fieldStructure);die;
   
    $valuesStructure = array();
    $valueCount = 0;
	
	$show_hidden = $viewer->getIdentity()
                 ? ($subject->getOwner()->isSelf($viewer) || 'admin' === Engine_Api::_()->getItem('authorization_level', $viewer->level_id)->type)
                 : false;
	$this->view->is_owner	=	($subject->getOwner()->isSelf($viewer))?true:false;
	
	$this->view->is_admin	=	($viewer->level_id =='2')?true:false;
	
	foreach( $fieldStructure as $map )
       {
			// Get field meta object
			$field = $map->getChild();
			$value = $field->getValue($subject);

		    if( !$field || $field->type == 'profile_type' ) continue;
		    if( !$field->display && !$show_hidden ) continue;
		    $isHidden = !$field->display;
		  
		    // Get first value object for reference
		    $firstValue = $value;
		    if( is_array($value) && !empty($value) ) {
		    	$firstValue = $value[0];
		    }
			
			// Evaluate privacy
			if( $usePrivacy && !empty($firstValue->privacy) && $relationship != 'self' ) {
				if( $firstValue->privacy == 'self' && $relationship != 'self' ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'friends' && ($relationship != 'friends' && $relationship != 'self') ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'registered' && $relationship == 'everyone' ) {
				  $isHidden = true; //continue;
				}
			}
			
			
		    //print_r($firstValue);
		    $helper = new Fields_View_Helper_FieldValueLoop();
			$helper->view = Zend_Registry::get('Zend_View');
			$tmp = $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);
			if( $field->type != 'heading' && (!$isHidden || $show_hidden )) {
				$val[$field->label] = $tmp;
			}
		}
	

	$catTable = Engine_Api::_()->getDbTable('categories', 'education');
    $catName = $catTable->info('name'); 
	
	$table = Engine_Api::_()->getDbTable('educations', 'education');
    $eduName = $table->info('name'); 
	
	$sid = $subject->getIdentity();
	
	//$table = Engine_Api::_()->getItemTable('education');
    $select = $table->select("$eduName.*")
						->setIntegrityCheck(false)
                        ->where("$eduName.owner_id = $sid", 1)
						->where("$eduName.row_status = 1", 1)
       					->joinLeft($catName, "$catName.category_id = $eduName.category_id", array('category_name'))
                        ->order("$eduName.creation_date desc")
						->limit(1);
	// get the data
	$result = $table->fetchAll($select);
	
	
	
	foreach($result as $edu){
		//echo '<pre>'; print_r($edu); echo '</pre>';
		//$edu->education_id == 
		$val['education_cat'][] = ($edu->category_id == 'other')?$edu->other_category:$edu->category_name;
		//$val['education_cat'][] = $edu->category_name;
		$val['education'][] = $edu->institute_name;
	}
	
	//echo '<pre>';print_r($val);echo '</pre>';//die;
	
	
	$this->view->fieldStructure = $val;
	
	

    
  }
}