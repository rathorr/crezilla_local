<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioConnectionsController extends Engine_Content_Widget_Abstract {
	
	protected $_childCount;
  
  public function indexAction()
  {

    //General Friend settings
    $this->view->make_list = Engine_Api::_()->getApi('settings', 'core')->user_friends_lists;

    // Don't render this if not authorized
    $viewer = Engine_Api::_()->user()->getViewer();
    if( !Engine_Api::_()->core()->hasSubject() ) {
      return $this->setNoRender();
    }    
    
    // Don't render this if friendships are disabled
    if( !Engine_Api::_()->getApi('settings', 'core')->user_friends_eligible ) {
      return $this->setNoRender();
    }

    // Get subject and check auth
    $this->view->subject = $subject = Engine_Api::_()->core()->getSubject('user');
    if( !$subject->authorization()->isAllowed($viewer, 'view') ) {
      return $this->setNoRender();
    }
	
	// Get lists if viewing own profile
   /* if( $viewer->isSelf($subject) ) {
		return $this->setNoRender();
	}*/

	
	if((($subject->friend_setting == 2 && !$subject->membership()->isMember($viewer)) || ($subject->friend_setting ==3)) && $subject->getIdentity() != $viewer->getIdentity()){
		return $this->setNoRender();	
	}

    // Multiple friend mode // friends of viewer
    $select = $subject->membership()->getMembersOfSelect();
    $this->view->friends = $friends = $paginator = Zend_Paginator::factory($select);  

    // Set item count per page and current page number
    $paginator->setItemCountPerPage($this->_getParam('itemCountPerPage', 8));
    $paginator->setCurrentPageNumber($this->_getParam('page', 1));

    // Get stuff
   $viewerselect= $viewer->membership()->getMembersOfSelect();
     $viewerfriend = Zend_Paginator::factory($viewerselect); 
    $fdsids=array();
    foreach ($friends as $key => $value) {
    	# code...
    	$fdsids[$key]=$value->resource_id;

    }
    $fdsids2=array();
    //echo '<pre>';print_r($viewerfriend); die();
    foreach ($viewerfriend as $key => $val) {
    	# code...
    	$fdsids2[$key]=$val->resource_id;

    }
    //echo '<pre>';print_r($fdsids); print_r($fdsids2); //die();
    $this->view->friendmutual=count(array_intersect($fdsids,$fdsids2));
    //echo $mutual; die();

    $ids = array();
    foreach( $friends as $friend ) {
      $ids[] = $friend->resource_id;
    }
    $this->view->friendIds = $ids;

    // Get the items
    $friendUsers = array();
    foreach( Engine_Api::_()->getItemTable('user')->find($ids) as $friendUser ) {
      $friendUsers[$friendUser->getIdentity()] = $friendUser;
    }
    $this->view->friendUsers = $friendUsers;

    
    // Do not render if nothing to show
    /*if( $paginator->getTotalItemCount() <= 0 ) {
      return $this->setNoRender();
    }*/
	
	$this->view->totalfriends = $paginator->getTotalItemCount();
	
    // Add count to title if configured
    if( $this->_getParam('titleCount', false) && $paginator->getTotalItemCount() > 0 ) {
      $this->_childCount = $paginator->getTotalItemCount();
    }
  }

  public function getChildCount()
  {
    return $this->_childCount;
  }
}