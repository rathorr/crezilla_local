<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
 <style type="text/css">
	#_portfolio_profile > div > div > div > div > div > div > a > img.img-responsive
	{
		max-height: 120px;
		min-height: 120px;
	}
</style>
 <!-- Sixth Block -->
<div id="_portfolio_profile">
<div class="_pp_area">
	<div class="border-bottom col-xs-12">
		<div class="_pp_area_heading block_conections">
			<span class="_pp_area_heading_icn"><i class="fa fa-users" aria-hidden="true"></i></span> Connections<span class="_pp_block_conections_friends"><?php echo $this->totalfriends;?> (<?php echo $this->friendmutual;?> Mutual) </span>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="_pp_summary_area block_conections">
			<div class="_pp_area_desc">
				<div class="row">
				<?php foreach( $this->friends as $membership ){

				$member = $this->friendUsers[$membership->resource_id];
				$pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($member->getIdentity());
				if($pic == '')$pic	=	$this->baseUrl().'/application/modules/User/externals/images/nophoto_user_thumb_profile.png';
				 ?>
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 _pp_connection_item">
						<a href="<?php echo $member->getHref();?>" title="">
							<img src="<?php echo $pic; ?>" alt="" class="img-responsive">
							<div class="_pp_connection_user"><?php echo $member->getTitle();?></div>
						</a>
					</div>
						<?php } ?>				
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 _pp_area_bottom text-center">
		<a href="<?php echo $this->baseUrl().'/members/index/friends/friend_id/'.$this->subject->getIdentity();?>" title="">See All Connections</a>
	</div>
</div>
</div>	
<!-- End Sixth Block -->