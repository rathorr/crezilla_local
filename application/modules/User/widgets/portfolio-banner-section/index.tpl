<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
 <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/job_search.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/manage_pages.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/portfolio-tab.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/fonts.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/slick.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/home-3.css" />
<link rel="stylesheet" type="text/css" href="application/modules/User/externals/portfolio/portfolio.css" />
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
<style type="text/css">
	#core_menu_mini_menu > ul.header_menu_class > li.menu_core_mini.core_mini_profile > a > span > img
	{
		width: auto;
	}
	.custom_oupload_camera_icon {
		position: absolute !important; 
	}
	._pb_img img._pb_img_shadow
	{
		position: relative !important;
	}
	#global_content > div > div.generic_layout_container.layout_main > div.generic_layout_container.layout_middle
	{
		width: 50% !important;
		background-color: #E9EAED !important;
	}
</style>
 <?php $pic=Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($this->viewer()->getIdentity());
if($pic == '')$pic	=	$this->baseUrl().'/application/modules/User/externals/images/nophoto_user_thumb_profile.png';
//wallphoto
$wallpic=Engine_Api::_()->getDbtable('users', 'user')->getUserWallPicPath($this->viewer()->getIdentity());
if($wallpic == '')$wallpic	=	Engine_Api::_()->getDbtable('users', 'user')->getUserRandomWallPicPath();
 ?>

 <!-- BANNER START -->
<section class="_protfolio_banner_section">
	<div class="container">
		<div class="col-xs-12">
			<div class="_pb_banner_area">
				<div class="_pb_img">
				<!-- <span id="shortlist" style="position: absolute;right: 9px;top: 8px;">
				 	<?php //if($this->backurl){
					if($this->is_shortlist){?>
				    	<a href="javascript:void(0);" class="remove_shortlist" title="Shortlisted">&nbsp;</a>
				 <?php }else{ ?>
					<a href="<?php echo $this->baseUrl().'/members/friends/shortlist/user_id/'.'1'.'?q='.$this->backurl;?>" class="smoothbox shortlist">&nbsp;</a>
				<?php }
				//} ?>
				 </span> -->
				 <?php if($this->is_owner){?>
					<a href="javascript:void(0);" id="profile_wallphoto" class="custom_oupload_camera_icon"> 
				    	<span>Update Cover Picture</span>
				     </a>
					 <ul class="cover_option custom_cover_bx" style="display:none;">
				    	<li>
				        <a href="<?php echo $this->baseUrl().'/members/profile/upload/item_type/cover/id/'.'1';?>" class="smoothbox upload_photo">Upload Photo</a></li>
				        <li><a href="<?php echo $this->baseUrl().'/members/profile/removephoto/item_type/cover/id/'.$this->user->getIdentity();?>" class="smoothbox remove_photo">Remove Photo</a></li>
				    </ul>
				    <?php } ?>
					<img src="<?php echo $wallpic; ?>" alt="">					
					<!-- <img class="_pb_img_shadow" src="application/modules/User/externals/images/profile_banner_shadow.png" alt=""> -->	
				</div>
				<div class="_pb_actions text-right">
					<a href="/members/edit/profile" title="Update Profile" class="btn btn-default _pb_update_profile_btn"><i class="fa fa-pencil"></i> Update Profile</a>
				</div>
				<div class="_pb_user_img">
					<img src="<?php echo $pic; ?>" alt="" />
					<?php if($this->is_owner){?>
					<a href="<?php echo $this->baseUrl().'/members/profile/upload/item_type/dp/id/'.$this->user->getIdentity();?>" class="smoothbox custom_oupload_camera_icon_small">
						<span>Update Profile Picture</span></a>
					 <?php } ?>
				</div>
				<div class="_pb_user_name">
					Rohit Mittal
				</div>
				<div class="_pb_user_actions">
					<a href="#" title="" class="_pb_user_actions_download"><i class="fa fa-download" aria-hidden="true"></i></a>
					<a href="#" title="" class="_pb_user_actions_share"><i class="fa fa-share-alt" aria-hidden="true"></i></i></a>
					<a href="#" title="" class="_pb_user_actions_likes count-active"><i class="fa fa-heart" aria-hidden="true"></i></a><span class="_pb_user_actions_likes_count">1201 Likes</span>
				</div>
			</div>		
		</div>
	</div>
</section>
<!-- BANNER END -->
<script type="text/javascript" src="application/modules/User/externals/portfolio/bootstrap.min.js"></script>
<script type="text/javascript" src="application/modules/User/externals/portfolio/slick.js"></script>
<script type="text/javascript">
function changepic(){
	jQuery.post('<?php echo $this->baseUrl()."/members/profile/getuserpic/item_type/cover/id/"."1";?>', function(resp){
			var pic	=	jQuery.trim(resp);
			if(pic == ''){
				pic = '<?php echo $this->baseUrl();?>/application/modules/User/externals/images/nophoto_user_thumb_wallmain.png';	
			}
			jQuery('#profile_wallphoto').css('background-image','url('+pic+')');
			jQuery('.cover_option').css("display", "none");
			jQuery('.custom_oupload_camera_icon').css("background-color", "");
		});
}

jQuery(document).on('click', '#profile_wallphoto', function(){
	if(jQuery('.cover_option').css("display") =='none'){
		jQuery('.cover_option').css("display", "block");
		jQuery('.custom_oupload_camera_icon span').addClass("show_cover_icon");
		jQuery('.custom_oupload_camera_icon').css("background-color", "rgba(0,0,0,0.4)");	
	}else{
		jQuery('.cover_option').css("display", "none");
		jQuery('.custom_oupload_camera_icon span').removeClass("show_cover_icon");
		jQuery('.custom_oupload_camera_icon').css("background-color", "");
	}
});
/*jQuery(document).mouseup(function (e)
{
	// CLOSE PROFILE OPTIONS ON MY PROFILE
	var menucontainer = jQuery(".user_profile_option_menus");

    if (!menucontainer.is(e.target) // if the target of the click isn't the container...
        && menucontainer.has(e.target).length === 0) // ... nor a descendant of the container
    { 
	   menucontainer.addClass('tab_closed');
	   menucontainer.removeClass('tab_open');
    }
});*/
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		
		jQuery('#block_pymk_carousel, #_ps_jymi_block').slick({
			slidesToShow: 2,
  			slidesToScroll: 1
		});
	});
</script>