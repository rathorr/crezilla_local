<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style type="text/css">
body#global_page_user-profile-index .layout_middle .layout_user_profile_name{left: 3%; margin-left: auto;margin-right: auto;position: absolute;right: 0;top: 55%;width: 33%;}
h1{font-size:55px !important;}
</style>
<div id='profile_name'>
	<?php if($this->fieldStructure['First Name'] == '' && $this->fieldStructure['Last Name'] ==''){?>
    	<h2><?php echo $this->subject->getTitle(); ?></h2>
    <?php }else{?>
		<li><h2><?php echo $this->fieldStructure['First Name']; ?><span><?php echo $this->fieldStructure['Middle Name']; ?></span></h2>
        </li>
    	<li><h2><?php echo $this->fieldStructure['Last Name']; ?></h2></li>
  <?php } ?>
</div>
