<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<?php 
if(isset($this->flashmsgs[0])) { 
$flashmsg	=	$this->flashmsgs[0]; 
echo "<p class='alert alert-success' style=' top: 0px; width: 100%; text-align: center; z-index: 99999;'>".$flashmsg['welcome']."<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></p>";
}?>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('.alert-success').fadeOut(35000);	
});
</script>
