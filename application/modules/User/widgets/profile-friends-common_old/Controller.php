<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Widget_ProfileFriendsCommonController extends Engine_Content_Widget_Abstract
{
  protected $_childCount;

  public function indexAction()
  {
	 
    // Don't render this if not authorized
    $viewer = Engine_Api::_()->user()->getViewer();
    if( !$viewer->getIdentity() ) {
      return $this->setNoRender();
    }
    
 	        $query ="SELECT user_id FROM `engine4_user_membership`
					WHERE `resource_id` =".$viewer->getIdentity()." AND resource_approved=1 AND active=1";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
         $result = $stmt->fetchAll();
		 $user_common=array();
		 $user_common_string	=	'';
		 if($result){
			 foreach ($result as $key=> $val)
			 {
				 $user_common[]=$val['user_id'];
			 }
			 $user_common_string=implode(",",$user_common);
		 }
		 
		   
		 
		 $query2 ="SELECT * FROM `engine4_user_membership`
				  WHERE user_id !='".$viewer->getIdentity()."'";
		if($user_common_string) $query2 .="AND resource_id IN (".$user_common_string.")";
			
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query2);
         $result2 = $stmt->fetchAll();
		 
 		 $uids=array();
		 if($result2){
			 foreach ($result2 as $key=> $val)
			 {
				 //$uids[]=$val['user_id'];
				 $uids[]=$val['user_id'];
			 }
		 }
		
		$final_uid	=	array_diff($uids, $user_common);
		 if( count($uids) <= 0 ) {
      		return $this->setNoRender();
    	 }
	 
 		$table = Engine_Api::_()->getItemTable('user');
		$userTableName = $table->info('name');
   
		$talentselect = $table->select()
		->from($userTableName)
		->where('user_id IN(?)', $final_uid);
   
	
    $this->view->paginator = $paginator = Zend_Paginator::factory($talentselect);
 
    // Set item count per page and current page number
    $paginator->setItemCountPerPage($this->_getParam('itemCountPerPage', 3));
    $paginator->setCurrentPageNumber($this->_getParam('page', 1));

    // Do not render if nothing to show
    /*if( $paginator->getTotalItemCount() <= 0 ) {
      return $this->setNoRender();
    }*/

    // Add count to title if configured
    if( $this->_getParam('titleCount', false) && $paginator->getTotalItemCount() > 0 ) {
      $this->_childCount = $paginator->getTotalItemCount();
    }

    // Diff friends
     
  }

  public function getChildCount()
  {
    return $this->_childCount;
  }
}