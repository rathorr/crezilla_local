<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>


<!--<ul>
  <?php foreach( $this->paginator as $user ): 
  
  
    $user_id=end(explode("/",$user->getHref()));
  
  ?>
    <li>
    
    <?php echo $this->htmlLink(''.$this->baseUrl().'/members/friends/addfriend/user_id/'.$user_id.'', $this->itemPhoto($user, 'thumb.icon', $user->getTitle()), array('class' => 'smoothbox')) ?>
    
     </li>
  <?php endforeach; ?>
</ul>-->


<div style="display:block; padding-top:10px; padding-right:20px; padding-bottom: 10px;" class="col-md-12">
<div class="col-md-8 text-right"><span style="font-size: 15px; color: rgb(76, 76, 76); text-align: center; font-weight: 600;">People you may know?</span></div>
<div class="col-md-4 text-right"><a class="fmp" href="members">Find more</a></div>
</div>
<div class="container col-md-12">
		  <?php foreach( $this->paginator as $user ): 
          $user_id=end(explode("/",$user->getHref()));
          ?>
		  <div class="col-md-4 nopadding">
			<div class="col-md-12 peopl_conect" style="padding:10px 0px;">
				
				  <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon', ''), array('class' => 'newestmembers_thumb')) ?></div>	
				  
				  <div class="col-md-12" style="padding-bottom:5px;">
					  <div class='newestmembers_info feed_item_photo' style="margin-right: 0px;">
						<div class='newestmembers_name feed_item_body text-center' >
						  <?php echo $this->htmlLink($user->getHref(), $this->string()->chunk($this->string()->truncate($user->getTitle(), 11), 10)) ?>
							
						</div>
				</div>
				  </div>
				  
				  <div class="col-md-12  text-center" style="padding-bottom:5px;">
                  <?php //$talent=Engine_Api::_()->getDbtable('users', 'user')->getUserTalent($user->getIdentity());
                  ?>
                  </div>
						
                            <div class="col-md-12 text-center" style="padding:5px 0 15px;">
                            <a class="button_blue smoothbox" style=" display: inline-block;" href="<?php echo $this->baseUrl().'/members/friends/addfriend/user_id/'.$user_id;?>">Connect</a></div>
				  
					</div>
				<?php endforeach; ?>
			
			
		
		
	

</div>
