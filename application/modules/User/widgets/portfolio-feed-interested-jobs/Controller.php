<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioFeedInterestedJobsController extends Engine_Content_Widget_Abstract {
	
	public function indexAction(){

		// Don't render this if not logged in
	    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	    if( !$viewer->getIdentity() ) {
	      return $this->setNoRender();
	    }
	    $userdcs  = Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategories($viewer->getIdentity());
		  $this->view->userdcs=$userdcs;
		  $udcoptions = array();
		    if($userdcs){
		      foreach($userdcs as $key=>$udsc){
		        $udcoptions[$key] = $udsc['dc_primary_id']; 
		      }
		    }
		 	$desigs=implode(',', $udcoptions);

		 	$table = Engine_Api::_()->getItemTable('classified');
			$jobTableName = $table->info('name');
			$jobselect = $table->select()
			  ->from($jobTableName)
			  ->where("{$jobTableName}.closed = ?", 0);
			  if($desigs != '')
			  {
			  	$jobselect->where("{$jobTableName}.category_id IN($desigs)");
			  }
			  else
			  {
			  	$jobselect->where("{$jobTableName}.category_id IN(0)");
			  }
			  
			  $jobselect->order("{$jobTableName}.classified_id DESC");
			  $jobselect->limit(20,0);
			  /*$jobselect = $table->select()
			  ->from($jobTableName)
			  ->where("{$jobTableName}.closed = ?", 0)
			  ->order("{$jobTableName}.view_count DESC")
			  ->limit(20,0);*/
			  $jobpaginator = $table->fetchAll($jobselect);
			  $this->view->jobs = $jobpaginator;
			  $this->view->userid=$viewer->getIdentity();
	}
}