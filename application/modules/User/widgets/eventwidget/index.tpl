<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>
<style type="text/css">

.fc-day-grid-event .fc-content {overflow:visible;}
.fc-event .fc-content{  z-index: auto;}
.login_my_wrapper #global_content .layout_main .layout_middle{width:100% !important;}
.login_my_wrapper #global_content .layout_main{padding:0 !important;}
</style>


 <style>
    .black_overlay{
        display: none;
        position: fixed;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index:1001;
        -moz-opacity: 0.6;
        opacity:.80;
        filter: alpha(opacity=80);
    }
    .white_content {
        display: none;
        position: fixed;
        top: 25%;
        left: 25%;
        width: 50%;
        
        padding: 16px;
        border: 8px solid #444;
        background-color: white;
        z-index:1002;
        overflow: auto;
    }
	.error_msg{
	color:#ff0000;
	font-weight:bold;
	text-align:center;
	
	}
</style>

	
	
<!--<div class="headline">
  <h2>
    <?php echo $this->translate('Edit My Profile');?>
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>
</div>-->

<div style="padding:0 12px;">
<h2 class="heading">Add/Edit Date Availibility</h2>
<?php $tit = "Click Add New Event to create an event. Click Delete icon on Event to delete an event.";?>
<img src="<?php echo $this->baseUrl().'/public/custom/images/infoicon.jpg';?>" title="<?php echo $tit;?>">
</div>
<p style="padding: 8px; background-color: rgb(238, 238, 238); display: block; text-align: center; margin: 10px 0px; color: rgb(90, 90, 90); font-weight: 800; font-size: 16px; font-family: robotolight; display:none;">
Click Add New Event to create an event.<br />Click Delete icon on Event to delete an event.</p>

<?php //echo $this->message; 


 $this->headLink()
 ->appendStylesheet($this->baseUrl().'/application/modules/User/externals/css/fullcalendar.css')
 ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/jquery/jquery-ui.css');
 $this->headScript()
		->appendFile($this->baseUrl() . '/application/modules/User/externals/js/moment.min.js')
	   ->appendFile($this->baseUrl() . '/application/modules/User/externals/assets/js/jquery-ui.min.js')
       ->appendFile($this->baseUrl() . '/application/modules/User/externals/js/fullcalendar.min.js');
 ?>

<script type="text/javascript">
	
	


  
function update(title,start,end,id)
{

	
	jQuery.ajax({
					url: ajax_url+'reset-date',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+id,
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')		    				
						revertFunc();
					},
					error: function(e){		    			
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
}
jQuery(document).ready(function(){
	jQuery('.user_edit_date_availibility').parent().addClass('active');
	jQuery('.custom_301').parent().addClass('active');
	jQuery('.custom_321').parent().addClass('active');
	jQuery('.custom_332').parent().addClass('active');
});
var json_events =[];
 	var ajax_url = '<?php echo $this->baseUrl()."/user/event/";?>';
	jQuery(document).ready(function() {

		var zone = "05:30";  //Change this to your timezone
 
	jQuery.ajax({
		url: ajax_url+'get-days',
        type: 'POST', // Send post data
        data: 'type=fetch',
        async: false,
        success: function(s){
        	json_events = s;
			
        }
	});


	var currentMousePos = {
	    x: -1,
	    y: -1
	};
		jQuery(document).on("mousemove", function (event) {
			currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
    });

		/* initialize the external events
		-----------------------------------------------------------------*/

		jQuery('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			jQuery(this).data('event', {
				title: jQuery.trim(jQuery(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			jQuery(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/
          // var events_data = JSON.parse(json_events);
		jQuery('#calendar').fullCalendar({
		
			events: JSON.parse(json_events),
			header: 
			{
				left: 'prev,next',
				center: 'title',
				right: ''
				//right: 'month,agendaWeek,agendaDay'
			},
			editable: true,
			droppable: true,
			allday:false, 
			slotDuration: '00:30:00',
			
			eventReceive: function(event)
			{
				var title = event.title;
				var start = event.start.format("YYYY-MM-DD[T]HH:MM:SS");
								
				jQuery.ajax({
		    		url: ajax_url+'new-event',
		    		data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
		    		type: 'POST',
		    		dataType: 'json',
		    		success: function(response){
		    			event.id = response.eventid;
		    			jQuery('#calendar').fullCalendar('updateEvent',event);
		    		},
		    		error: function(e){
		    			console.log(e.responseText);

		    		}
		    	});
				jQuery('#calendar').fullCalendar('updateEvent',event);
				console.log(event);
			},
			eventRender: function(event, eventElement) {
				if (event.imageurl) {
					eventElement.find("div.fc-content").prepend("<img src='" + event.editimageurl +"' width='12' height='12' eid='"+event.id+"' class='edit_event' style='background-color: #ffdab9;padding: 7px 10px; position: absolute; right: 30px;top:-5px;z-index: 99999;' title='Edit' alt='Delete Event'><img src='" + event.imageurl +"' width='12' height='12' eid='"+event.id+"' class='delete_img' style='background-color: #ffdab9;padding: 7px 10px; position: absolute; right: -5px;top:-5px;z-index: 99999;' title='Delete' alt='Delete Event'>");
				}
			},
			eventDrop: function(event, delta, revertFunc) 
			{
		        var title = event.title;
		        var start = event.start.format();
		        var end = (event.end == null) ? start : event.end.format();
		        jQuery.ajax({
					url: ajax_url+'reset-date',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id+'&type=eventdrop',
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')		    				
						revertFunc();
					},
					error: function(e){		    			
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
		    },
		    eventClick: function(event, jsEvent, view) 
			{
			/*return false;
				var sdateString = event.start;
				var sdate = new Date(sdateString);
				var syr = sdate.getFullYear();
				var smo = sdate.getMonth()+1;
				var sday = sdate.getDate();
			
				var edateString = event.end_date;
				var edate = new Date(edateString);
				var eyr = edate.getFullYear();
				var emo = edate.getMonth()+1;
				var eday = edate.getDate();

				pop_show_form(event.title, event.location, ((sday<10?"0"+sday:sday))+"-"+smo+"-"+syr,((eday<10?"0"+eday:eday))+"-"+emo+"-"+eyr, event.id );*/
				/*
		    	console.log(event.id);
		          var title = prompt('Event Title:', event.title, { buttons: { Ok: true, Cancel: false} });
		          if (title){
			
		              event.title = title;
		              console.log('type=changetitle&title='+title+'&eventid='+event.id);
		              jQuery.ajax({
				    		url: ajax_url+'change-title',
				    		data: 'type=changetitle&title='+title+'&eventid='+event.id,
				    		type: 'POST',
				    		dataType: 'json',
				    		success: function(response){	
				    			if(response.status == 'success')			    			
		              				jQuery('#calendar').fullCalendar('updateEvent',event);
				    		},
				    		error: function(e){
				    			alert('Error processing your request: '+e.responseText);
				    		}
				    	});
		          }
				  */
				 
			},
			eventResize: function(event, delta, revertFunc) 
			{
				console.log(event);
				var title = event.title;
				var end = event.end.format();
				var start = event.start.format();
		        update(title,start,end,event.id);
		    },
			eventDragStop: function (event, jsEvent, ui, view) 
			{
			    if (isElemOverDiv()) 
				{
			    	var con = confirm('Are you sure to delete this event permanently?');
			    	if(con == true) 
					{
						jQuery.ajax({
				    		url: ajax_url+'remove-event',
				    		data: 'type=remove&eventid='+event.id,
				    		type: 'POST',
				    		dataType: 'json',
				    		success: function(response)
							{
				    			console.log(response);
				    			if(response.status == 'success')
								{
				    				jQuery('#calendar').fullCalendar('removeEvents');
            						getFreshEvents();
            					}
				    		},
				    		error: function(e)
							{	
				    			alert('Error processing your request: '+e.responseText);
				    		}
			    		});
					}   
				}
			}
		});




	function isElemOverDiv() {
        var trashEl = jQuery('#trash');

        var ofs = trashEl.offset();

        var x1 = ofs.left;
        var x2 = ofs.left + trashEl.outerWidth(true);
        var y1 = ofs.top;
        var y2 = ofs.top + trashEl.outerHeight(true);

        if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
            currentMousePos.y >= y1 && currentMousePos.y <= y2) {
            return true;
        }
        return false;
    }
	
	

	});
	
	
		function getFreshEvents(){
		jQuery.ajax({
			url: ajax_url+'get-days',
	        type: 'POST', // Send post data
	        data: 'type=fetch',
	        async: false,
	        success: function(s){
	        	freshevents = s;
				
	        }
		});
		jQuery('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
	}
	
jQuery(document).on('click','.delete_img', function(){
	var event_id = jQuery(this).attr('eid');
	if(confirm("Are you sure you want to delete this event?")) {
					// delete event in backend
		jQuery.post(ajax_url+'remove-event',{type:'remove', eventid:event_id}, function(){
				jQuery('#calendar').fullCalendar('removeEvents');
				getFreshEvents();
		});
		
		
	}	
});

jQuery(document).on('click','.edit_event', function(){
	
	var event_id = jQuery(this).attr('eid');
	jQuery.post(ajax_url+'get-event',{eventid:event_id}, function(resp){
		console.log(resp);
		resp = JSON.parse(resp);
		var sdateString = resp.start;
		var sdate = new Date(sdateString);
		var syr = sdate.getFullYear();
		var smo = sdate.getMonth()+1;
		var sday = sdate.getDate();
	
		var edateString = resp.end_date;
		var edate = new Date(edateString);
		var eyr = edate.getFullYear();
		var emo = edate.getMonth()+1;
		var eday = edate.getDate();
		pop_show_form(resp.title, resp.location, ((sday<10?"0"+sday:sday))+"-"+smo+"-"+syr,((eday<10?"0"+eday:eday))+"-"+emo+"-"+eyr, resp.id );	
	});
});

jQuery(document).ready(function(){

	jQuery("#event_start_date").datepicker({ dateFormat: 'dd-mm-yy' });
	jQuery("#event_end_date").datepicker({ dateFormat: 'dd-mm-yy' });
	
});

jQuery(document).on('change', '#event_start_date, #event_end_date', function(){
	if((jQuery('#event_start_date').val() != '') && (jQuery('#event_end_date').val() != '')){
		if(parseInt(fn_DateCompare(jQuery.trim(jQuery('#event_start_date').val()), jQuery.trim(jQuery('#event_end_date').val()))) == 1){
			alert('Start date can not be greater than end date.');
			jQuery('#event_start_date').val('');
			jQuery('#event_end_date').val('');
			return false;	
		}	
	}
});

function submit_form()
{
	var event_id 			= jQuery("#event_id").val();
	var event_title		 	= jQuery("#event_title").val();
	var event_location	 	= jQuery("#event_location").val();
	var start 				= jQuery("#event_start_date").val();
	var end 				= jQuery("#event_end_date").val();
	var validator			= 0;
	var error_msg			= "";
	var ajax_url 			= '<?php echo $this->baseUrl()."/user/event/";?>';
	
	if(jQuery.trim(event_title) == "")
	{
		validator = 1;
		error_msg = "Please Enter Event Title<br />";
	}
	if(jQuery.trim(start) == "")
	{
		validator = 1;
		error_msg += "Please Enter Start Date<br />";
	}
	if(jQuery.trim(end) == "")
	{
		validator = 1;
		error_msg += "Please Enter End Date<br/>";
	}
	if( validator == 0)
	{
			
		   jQuery.ajax({
					url: ajax_url+'reset-date',
					data: 'type=resetdate&title='+event_title+'&location='+event_location+'&start='+start+'&end='+end+'&eventid='+event_id,
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status == 'success')
						{
							//close_form();
							//getFreshEvents();
							//window.relaod();
							location.reload(true);
							jQuery('#calendar').fullCalendar('updateEvent',event);
						}
						
					},
					error: function(e){		    			
						getFreshEvents();
						alert('Error processing your request: '+e.responseText);
					}
				});
	}
	else
	{
		jQuery("#span_error_msg").html(error_msg);
	}
	
}	
	
function fn_DateCompare(DateA, DateB) {
  var a = new Date(DateA.replace(/-/g, '/'));
  var b = new Date(DateB.replace(/-/g, '/'));

  var msDateA = Date.UTC(a.getFullYear(), a.getMonth()+1, a.getDate());
  var msDateB = Date.UTC(b.getFullYear(), b.getMonth()+1, b.getDate());

  if (parseFloat(msDateA) < parseFloat(msDateB))
	return -1;  // less than
  else if (parseFloat(msDateA) == parseFloat(msDateB))
	return 0;  // equal
else if (parseFloat(msDateA) > parseFloat(msDateB))
	return 1;  // greater than
else
	return null;
} 

function pop_show_form(obj, obj4, obj1, obj2, obj3)
{
		if(obj3 != "")
		{
			jQuery("#pop_form_title").html("Update Event");
			//jQuery(".event_delete").show();
		}
		else
		{
			jQuery("#pop_form_title").html("Add New Event");
			//jQuery(".event_delete").hide();
		}
		
		jQuery("#event_id").val(obj3);
		jQuery("#event_title").val(obj);
		jQuery("#event_location").val(obj4);
		jQuery("#event_start_date").val(obj1);
		jQuery("#event_end_date").val(obj2);
				
				
		jQuery("#overlay").show();
		jQuery("#form_div").show();
		
}
  
function close_form()
  {
    
	jQuery("#event_title").val(""); 
	jQuery("#event_start_date").val("");
	jQuery("#event_end_date").val("");
	
	jQuery("#form_div").hide();
	jQuery("#overlay").hide();
	
	
  }
function delete_event(){
	
			var con = confirm('Are you sure to delete this event permanently?');
			if(con == true) 
			{
				jQuery.ajax({
					url: ajax_url+'remove-event',
					data: 'type=remove&eventid='+jQuery.trim(jQuery('#event_id').val()),
					type: 'POST',
					dataType: 'json',
					success: function(response)
					{
						console.log(response);
						if(response.status == 'success')
						{
							jQuery('#calendar').fullCalendar('removeEvents');
							getFreshEvents();
							jQuery('#form_div').fadeOut();
							jQuery('#overlay').css("display", "none");
						}
					},
					error: function(e)
					{	
						alert('Error processing your request: '+e.responseText);
					}
				});
			}   
			
}

jQuery(document).on('ready', function(){
	//setTimeout(changecalcolor, 1000);
});

function changecalcolor(){
	clr = 1;
	jQuery('.fc-event').each(function() {
		if(clr == 1){
			jQuery(this).addClass("color1");	
		}else if(clr == 2){
			jQuery(this).addClass("color2");	
		}else if(clr == 3){
			jQuery(this).addClass("color3");	
		}
		if(clr == 3){
			clr = 0;	
		}
		clr++;
	});	
}
</script>


<style>

	#trash{
		width:32px;
		height:32px;
		float:left;
		padding-bottom: 15px;
		position: relative;
	}
		
	#wrap {
		width: 1100px;
		margin: 0 auto;
	}
		
	#external-events {
		float: left;
		width: 150px;
		padding: 0 10px;
		border: none;
		background: #eee;
		text-align: left;
	}
		
	#external-events h4 {
		font-size: 16px;
		margin-top: 0;
		padding-top: 1em;
	}
		
	#external-events .fc-event {
		margin: 10px 0;
		cursor: pointer;
	}
		
	#external-events p {
		margin: 1.5em 0;
		font-size: 11px;
		color: #666;
	}
		
	#external-events p input {
		margin: 0;
		vertical-align: middle;
	}

	#calendar {
		float: left;
		width: 800px;
	}
	div.white_content.evnt_pop_sec input {
    background-color: #fafafa;
    border: 1px solid #dbdbdb;
    height: 35px;
    line-height: 30px;
    padding: 5px 6px;
    width: 365px;
}
div.white_content.evnt_pop_sec label {
    color: #5a5a5a;
    display: inline-block;
    font-size: 13px;
    min-width: 130px;}
	
	div.white_content.evnt_pop_sec input.save_project{background-color: #3faee8;
    border: 0 none;
    border-radius: 3px;
    color: #fff;
    font-size: 17px;
    width: 30%;}
	
	.white_content.evnt_pop_sec form#parent_form {
    padding: 15px 0 !important;}
	
	.white_content.evnt_pop_sec h2#pop_form_title {
    border-bottom: 1px solid #dbdbdb;
    color: #5a5a5a !important;
    text-align: center;
}

.event_close {
    background-color: #da2e20;
    border-radius: 3px;
    color: #fff !important;
    padding: 5px 8px !important;}
	
	.event_close:hover{text-decoration:none !important; background-color:#cf2315;}
	
	.ad_new_event {
    background-color: #65c436;
    border-radius: 5px;
    color: #fff !important;
    display: block;
    padding: 5px;
    text-align: center;
}

div.white_content.evnt_pop_sec input.event_delete{
	background-color: #5a5a5a;
    border: 0 none;
    border-radius: 3px;
    color: #fff;
    font-size: 17px;
    width: 30%;}

</style>
	<div id='wrap' class="">
<div id='external-events'>
			<a class="ad_new_event" href="javascript:void(0)" onClick="pop_show_form('', '', '', '', '')">Add New Event</a>
			
		</div>
		<div style='clear:both'></div>
<div id='calendar'></div>
<div style='clear:both'></div>
</div>

<div  id="form_div" class="white_content evnt_pop_sec" >  
<a class="event_close" href = "javascript:void(0)" style="float:right; padding:5px;" onClick="close_form()">Close</a>
<h2 id="pop_form_title"></h2>
<span class="error_msg" id="span_error_msg" ></span>

	<form name="parent_form" id="parent_form">
   
		<input type="hidden" name="event_id" id="event_id" value="" />
		
		<label>Title</label>
		<input type="text"  name="event_title" id="event_title" value="" placeholder="Event Title"/>
		<br /><br/>
        <label>Location</label>
		<input type="text"  name="event_location" id="event_location" value="" placeholder="Event Location"/>
		<br /><br/>
		<label>Start Date</label>
		<input type="text" class="datepicker"  name="event_start_date" id="event_start_date" value="" placeholder="Event start date"/>
		<br /><br/>
		<label>End Date</label>
		<input type="text"  class="datepicker" name="event_end_date" id="event_end_date" value="" placeholder="Event end date"/>
		<br /><br/>
		<div style="margin: 0px auto; float: none; width: 56%;"><input class="save_project" type="button" onClick="submit_form()" name="button" value="Save" />
        <input class="event_delete" type="button" onClick="delete_event()" name="delete" value="Delete"  style="display:none;"/>
        </div>
       
	</form>
	
			
	</div>
	<div id="overlay" class="black_overlay"></div>
	