<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
 ?>
 <style type="text/css">
 	#review_title
 	{
	    background: none;
	    border-radius: 0;
	    resize: none;
	    min-height: 40px;
	    border: 1px solid #e6e7e8 !important;
	    margin-bottom: 10px;
	    width: 100%;
 	}
	.error
	{
		background-color: red !important;
	}
 </style>
<!-- Fourth Block -->
<div class="_pp_area">
	<div class="border-bottom col-xs-12">
		<div class="_pp_area_heading block_review">
			<span class="_pp_area_heading_icn"><i class="fa fa-star" aria-hidden="true"></i></span> <?php echo ($this->tot_reviews > 1)? $this->tot_reviews." Reviews": $this->tot_reviews." Review";?> <span class="_pp_review_rating">(<?php echo $this->avgreviews;?>) 
			<?php $userating=$this->avgreviews;
			for($x=1;$x<=$userating;$x++) {?>
            <i class="fa fa-star" aria-hidden="true"></i>
		    <?php } 
		    if ((int) $userating != $userating) {  ?>
		             <i class="fa fa-star-half" aria-hidden="true"></i>
		     <?php }
		         if($x !=1) { while ($x<=5) { ?>
		              <i class="fa fa-star-o" aria-hidden="true"></i>
		      <?php $x++; } }?></span>
		</div>
	</div>
	<!-- <div class="border-bottom col-xs-12 _pp_wyr">
	<?php if ($this->error){ 
		echo $this->error;} else{ ?>
		<div class="_pp_wyr_lft">
			<input type="text" name="review_title" id="review_title" placeholder="Enter Review Title...">
			<textarea class="form-control" name="review_summary" id="review_summary" cols="3" rows="2" placeholder="Write your review here..."></textarea>
		</div>
		<div class="_pp_wyr_rgt">
			<div class="text-center _pp_wyr_starrating">
			<a class="class_rating" data-val="1" title=""><i class="fa fa-star-o" aria-hidden="true"></i></a>
			<a class="class_rating" data-val="2" title=""><i class="fa fa-star-o" aria-hidden="true"></i></a>
			<a class="class_rating" data-val="3" title=""><i class="fa fa-star-o" aria-hidden="true"></i></a>
			<a class="class_rating" data-val="4" title=""><i class="fa fa-star-o" aria-hidden="true"></i></a>
			<a class="class_rating" data-val="5" title=""><i class="fa fa-star-o" aria-hidden="true"></i></a>
			</div>
			<div class="_pp_wyr_action">
			<a title="" class="btn btn-block _review_submit">Submit</a>
			<input type="hidden" name="profile_rating" id="profile_rating">
			</div>
		</div>
		<?php }?>
	</div> -->
	<div class="col-xs-12">
		<div class="_pp_summary_area block_review">
			<div class="_pp_area_desc">
				<ul>
				<?php foreach($this->reviews as $rev){
				//echo '<pre>';print_r($rev); die();
				$talentpic  = Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($rev->reviewer_id);
			$talentpic  = ($talentpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$talentpic;
				?>
					<li>
						<div class="_pp_block_review_img">
							<img src="<?php echo $talentpic;?>" alt="" />
						</div>
						<div class="_pp_block_review_desc">
							<div class="_pp_block_review_user_name"><?php echo $rev->reviewer_name;?> <span class="_pp_block_review_user_rating">
			<?php $rating=$rev->rating;
			for($x=1;$x<=$userating;$x++) {?>
            <i class="fa fa-star" aria-hidden="true"></i>
		    <?php } 
		    if ((int) $rating != $rating) {  ?>
		             <i class="fa fa-star-half" aria-hidden="true"></i>
		     <?php }
		         if($x !=1) { while ($x<=5) { ?>
		              <i class="fa fa-star-o" aria-hidden="true"></i>
		      <?php $x++; } }?>
							</span></div>
							<div class="_pp_block_review_comments"><?php echo $rev->title;?></div>
							<div class="_pp_block_review_comments"><?php echo $rev->summary;?>.</div>
							<div class="_pp_block_review_time"><?php echo $this->timestamp($rev->review_date);?></div>
						</div>
					</li>		
				<?php }?>											
				</ul>
			</div>
		</div>
	</div>
	<div class="col-xs-12 _pp_area_bottom text-center">
		<a href="#" title="">Show More</a>
	</div>
</div>
<!-- End Fourth Block -->
<script type="text/javascript">
	jQuery("document").ready(function(){
		jQuery('a.class_rating').on('click', function(){
			console.log(jQuery(this).data('val'));
			console.log(jQuery(this).attr('data-val'));
			jQuery('#profile_rating').val(jQuery(this).data('val'));
		});
		jQuery('#review_title').focus(function(){
			this.removeClass('error');
		});
		jQuery('#review_summary').focus(function(){
			this.removeClass('error');
		});
		jQuery('#profile_rating').focus(function(){
			this.removeClass('error');
		});
		jQuery('._review_submit').on('click',function(){
			if(jQuery('#review_title').val() == '')
			{	
				jQuery('#review_title').addClass('error');
				return false;
			}
			if(jQuery('#review_summary').val() == '')
			{
				jQuery('#review_summary').addClass('error');
				return false;
			}
			if(jQuery('#profile_rating').val() == '')
			{
				jQuery('._pp_wyr_starrating').addClass('error');
				return false;
			}
			var title =jQuery('#review_title').val();
			var summary =jQuery('#review_summary').val();
			var rating =jQuery('#profile_rating').val();
			var url = window.location.pathname;
			var id = url.substring(url.lastIndexOf('/') + 1);
			//console.log('id'+id);
			//return false;
			jQuery.post("<?php echo $this->baseUrl();?>/members/index/profilereview",{title:title,summary:summary,rating:rating,id:id}, function(resp){
		            //var data=jQuery.parseJSON(resp);		            
		              jQuery('._pp_wyr_lft').hide();
		              jQuery('._pp_wyr_rgt').hide();
		              jQuery('._pp_wyr').text(resp);
		              //window.location.href='/members/edit/profile';
	          });
		});

	});
</script>