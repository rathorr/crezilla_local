<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2016-2017 Fameworks Developments
 * @license    http://www.crezilla.com/license/
 * @author     Rajesh
 */
class User_Widget_PortfolioReview2Controller extends Engine_Content_Widget_Abstract {
	
	public function indexAction(){

		// Don't render this if not logged in
	    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	    if( !$viewer->getIdentity() ) {
	      return $this->setNoRender();
	    }
	    // Get subject and check auth
	    $this->view->subject	=	$subject = Engine_Api::_()->core()->getSubject('user');	    
		// REVIEWS
		$this->view->reviews = $reviews = 	Engine_Api::_()->getDbtable('reviews', 'user')->getReviews($subject->getIdentity());	
		$this->view->tot_reviews	=	count($reviews);
		$this->view->avgreviews = $avgreviews = 	Engine_Api::_()->getDbtable('reviews', 'user')->getAVGuserrating($subject->getIdentity());
		
	$user_id='3';
	$user = Engine_Api::_()->getItem('user', $user_id);	
	$tableReview = Engine_Api::_() -> getItemTable('review', 'user');
	$HasReviewed = $tableReview -> checkHasReviewed($user_id, $viewer -> getIdentity());
		
	if($HasReviewed){
		$this->view->status = false;
		$this->view->error = Zend_Registry::get('Zend_Translate')->_('You have already submitted your review for '.$user->getTitle().' .');		
		return;
	}
	}
}