<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     John
 */
return array(
  // Package -------------------------------------------------------------------
  'package' => array(
    'type' => 'module',
    'name' => 'user',
    'version' => '4.8.7',
    'revision' => '$Revision: 10271 $',
    'path' => 'application/modules/User',
    'repository' => 'socialengine.com',
    'title' => 'Members',
    'description' => 'Members',
    'author' => 'Webligo Developments',
    'changeLog' => 'settings/changelog.php',
    'dependencies' => array(
      array(
        'type' => 'module',
        'name' => 'core',
        'minVersion' => '4.2.0',
      ),
    ),
    'actions' => array(
       'install',
       'upgrade',
       'refresh',
       //'enable',
       //'disable',
     ),
    'callback' => array(
      'path' => 'application/modules/User/settings/install.php',
      'class' => 'User_Installer',
      'priority' => 3000,
    ),
    'directories' => array(
      'application/modules/User',
    ),
    'files' => array(
      'application/languages/en/user.csv',
    ),
  ),
  // Compose -------------------------------------------------------------------
  'compose' => array(
    array('_composeFacebook.tpl', 'user'),
    array('_composeTwitter.tpl', 'user'),
  ),
  'composer' => array(
    'facebook' => array(
      'script' => array('_composeFacebook.tpl', 'user'),
    ),
    'twitter' => array(
      'script' => array('_composeTwitter.tpl', 'user'),
    ),
  ),
  // Hooks ---------------------------------------------------------------------
  'hooks' => array(
    array(
      'event' => 'onUserEnable',
      'resource' => 'User_Plugin_Core',
    ),
    array(
      'event' => 'onUserDeleteBefore',
      'resource' => 'User_Plugin_Core',
    ),
    array(
      'event' => 'onUserCreateAfter',
      'resource' => 'User_Plugin_Core',
    ),
    array(
      'event' => 'getAdminNotifications',
      'resource' => 'User_Plugin_Core',
    )
  ),
  // Items ---------------------------------------------------------------------
  'items' => array(
    'user',
    'user_list',
    'user_list_item',
  'review',
  'friendship',
  'membership',
  'followship',
  'skill',
  'category',
  'endorsement',
  'userreel',
  'shortlistfolder',
  'shortlist',
  'functionalareas',
  'industryexperiences',
  'currentstatus',
  'dasignatedcategories',
  'dasignatedsubcategories',
  'unionmembership',
  'workexp',
  'professionalsummarys',
  'clientids',
  'Countries',
  'contactinfos'
  ),
  // Routes --------------------------------------------------------------------
  'routes' => array(
    // User - Update photos
    'user_update' => array(
      'route' => 'members/profile/:action/:item_type/:id',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'profile',
        'action' => 'upload',
    'item_type' =>  'cover',
      ),
      'reqs' => array(
        'action' => 'upload|removephoto|delrev',
    'type' => '(cover|dp|rev|com)',
      )
    ),
  'user_signupcheck'=> array(
      'route' => 'members/signupcheck/',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'signupcheck',
        'action' => 'index',
    
      ),
      'reqs' => array(
        'action' => 'index',
    
      )
    ),
  'user_signupcheck'=> array(
      'route' => 'members/signupcheck/username',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'signupcheck',
        'action' => 'username',
    
      ),
      'reqs' => array(
      'action' => 'index|username',    
      )
    ),
  'user_update' => array(
      'route' => 'members/edit/:action/:item_type/:rid',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'edit',
        'action' => 'delrev',
    'item_type' =>  'rev',
      ),
      'reqs' => array(
        'action' => 'delrev',
    'type' => '(rev|com)',
      )
    ),
    // User - General
    'user_extended' => array(
      'route' => 'members/:controller/:action/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'index',
        'action' => 'index'
      ),
      'reqs' => array(
        'controller' => '\D+',
        'action' => '\D+',
      )
    ),
  
  
  
    'user_general' => array(
      'route' => 'members/:action/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'index',
        'action' => 'browse'
      ),
      'reqs' => array(
        'action' => '(home|browse|casting|getuserresults|services|postclassified|myclassified|classifiedboard|premiumsearch|notices|country|state|city|viewed|shortlist|getshortlistresults|subscribe|professionals|suppliers)',
      )
    ),
  
  
  /*'user_generals' => array(
      'route' => 'members/:action/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'index',
        'action' => 'viewed'
      ),
      'reqs' => array(
        'action' => '(viewed)',
      )
    ),
  
  'userss_generals' => array(
      'route' => 'members/:action/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'index',
        'action' => 'country'
      ),
      'reqs' => array(
        'action' => '(country)',
      )
    ),
  
  'userss_generalsstate' => array(
      'route' => 'members/:action/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'index',
        'action' => 'state'
      ),
      'reqs' => array(
        'action' => '(state)',
      )
    ),
  
  'userss_generalscity' => array(
      'route' => 'members/:action/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'index',
        'action' => 'city'
      ),
      'reqs' => array(
        'action' => '(city)',
      )
    ),*/
    'users_editpost' => array(
      'route' => 'members/editpostclassified/:id/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'index',
        'action' => 'editpostclassified'
      ),
      'reqs' => array(
        'action' => '(editpostclassified)',
      )
    ),
    // User - Specific
    'user_profile' => array(
      'route' => 'profile/:id/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'profile',
        'action' => 'index'
      )
    ),
    
    'user_login' => array(
      //'type' => 'Zend_Controller_Router_Route_Static',
      'route' => '/login/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'auth',
        'action' => 'login'
      )
    ),
    'user_logout' => array(
      'type' => 'Zend_Controller_Router_Route_Static',
      'route' => '/logout',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'auth',
        'action' => 'logout'
      )
    ),
  
  'page_pay' => array(
      'route' => 'page-pay/:action/:page_id/*',
      'defaults' => array(
        'module' => 'page',
        'controller' => 'page',
        'action' => 'pay',
        'page_id' => 0
      )
    ),
  
   
    'user_signup' => array(
      'route' => '/signup/:action/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'signup',
        'action' => 'index'
      ),
      'reqs' => array(
        'action' => '(index|getemail|confirm|verify)',
      )
    ),
    'user_getsubdesig' => array(
      'route' => '/edit/:action/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'edit',
        'action' => 'getsubDesignation'
      ),
      'reqs' => array(
        'action' => '(getsubDesignation)',
      )
    ),
     'user_savesubdesig' => array(
      'route' => '/edit/:action/*',
      'defaults' => array(
        'module' => 'user',
        'controller' => 'edit',
        'action' => 'savesubDesignation'
      ),
      'reqs' => array(
        'action' => '(savesubDesignation)',
      )
    ),
  )
); ?>
