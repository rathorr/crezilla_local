<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: content.php 9868 2013-02-12 21:50:45Z shaun $
 * @author     John
 */
return array(

  array(
    'title' => 'Quick Links',
    'description' => 'Displays a list of quick links.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.home-links',
    'requirements' => array(
      'viewer',
    ),
  ),array(
    'title' => 'Profile Views',
    'description' => 'Displays a number of profile views.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-views',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'User Photo',
    'description' => 'Displays the logged-in member\'s photo.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.home-photo',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'User Wall Photo',
    'description' => 'Displays the logged-in member\'s wall photo.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.wall-photo',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'User Signup Welcome Message',
    'description' => 'Displays welcome message.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.signup-msg',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Online Users',
    'description' => 'Displays a list of online members.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.list-online',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => '%d Members Online',
    ),
    'requirements' => array(
      'no-subject',
    ),
  ),
  array(
    'title' => 'Popular Members',
    'description' => 'Displays the list of most popular members.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.list-popular',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Popular Members',
    ),
    'requirements' => array(
      'no-subject',
    ),
  ),
  array(
    'title' => 'Recent Signups',
    'description' => 'Displays the list of most recent signups.',
    'category' => 'User',
    'type' => 'widget',
    /*'name' => 'user.profile-friends-common',*/
	'name' => 'user.list-signups',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Recent Signups',
    ),
    'requirements' => array(
      'no-subject',
    ),
  ),
  
  array(
    'title' => 'Recent Member Signups',
    'description' => 'Displays the list of most recent signups on member login page.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.list-signups-member-signup',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Recent Member Signups',
    ),
    'requirements' => array(
      'no-subject',
    ),
  ),
  array(
    'title' => 'Login or Signup',
    'description' => 'Displays a login form and a signup link for members that are not logged in.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.login-or-signup',
    'requirements' => array(
      'no-subject',
    ),
  ),
  array(
    'title' => 'Profile Fields',
    'description' => 'Displays a member\'s limited profile field data on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-fields',
    'defaultParams' => array(
      'title' => 'Info',
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Complete Fields',
    'description' => 'Displays a member\'s complete profile field data on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-complete-fields',
    'defaultParams' => array(
      'title' => 'Info',
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Friends',
    'description' => 'Displays a member\'s friends on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-friends',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Friends',
      'titleCount' => true,
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Followers',
    'description' => 'Displays a member\'s followers on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-friends-followers',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Followers',
      'titleCount' => true,
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Following',
    'description' => 'Displays the members a member is following on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-friends-following',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Following',
      'titleCount' => true,
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Mutual Friends',
    'description' => 'Displays the mutual friends between the viewer and the subject.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-friends-common',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Mutual Friends'
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Info',
    'description' => 'Displays a member\'s info (signup date, friend count, etc) on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-info',
	'defaultParams' => array(
      'title' => 'Login History'
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Small Info Box',
    'description' => 'Displays a member\'s info (signup date, name, lives in etc) on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-info-smallbox',
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Info Upper',
    'description' => 'Displays a member\'s info as per design',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-info-upper',
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
   array(
    'title' => 'User Applied Jobs',
    'description' => 'Displays a user\'s applied jobs',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.user-applied-jobs',
    'requirements' => array(
      'subject' => 'user',
    ),
	'defaultParams' => array(
      'titleCount' => true,
    ),
  ),
  array(
    'title' => 'Profile Info Middle',
    'description' => 'Displays a member\'s middle info as per design',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-info-middle',
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Options',
    'description' => 'Displays a list of actions that can be performed on a member on their profile (report, add as friend, etc).',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-options',
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Photo',
    'description' => 'Displays a member\'s photo on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-photo',
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Name',
    'description' => 'Displays a member\'s name on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-name',
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Wall Photo',
    'description' => 'Displays a member\'s wall photo on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-wallphoto',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Profile Status',
    'description' => 'Displays a member\'s name and most recent status on their profile.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-status',
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Tags',
    'description' => 'Displays photos, blogs, etc that a member has been tagged in.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-tags',
    'isPaginated' => true,
    'defaultParams' => array(
      'title' => 'Tags',
      'titleCount' => true,
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'User Settings Menu',
    'description' => 'Displays a menu in the user settings pages.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.settings-menu',
    'requirements' => array(
      'no-subject',
    ),
  ),
  array(
    'title' => 'Member Browse Menu',
    'description' => 'Displays a menu in the member browse page.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.browse-menu',
    'requirements' => array(
      'no-subject',
    ),
  ),
  array(
    'title' => 'Member Browse Search',
    'description' => 'Displays a search form in the member browse page.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.browse-search',
    'requirements' => array(
      'no-subject',
    ),
  ),
    array(
    'title' => 'Events Widget',
    'description' => 'Displays all events of that perticular user.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.eventwidget',
    'requirements' => array(
      'no-subject',
    ),
  ),
   array(
    'title' => 'User Events',
    'description' => 'Displays all events of that selected user.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.user-events',
    'requirements' => array(
      'no-subject',
    ),
  ),
  array(
    'title' => 'User Reviews',
    'description' => 'Displays all user reviews',
    'category' => 'User',
    'type' => 'widget',
	'isPaginated' => true,
    'name' => 'user.user-reviews',
	'defaultParams' => array(
      'title' => 'Reviews',
      'titleCount' => true,
    ),
    'requirements' => array(
       'subject' => 'user',
    ),
  ),
  array(
    'title' => 'User Pages',
    'description' => 'Displays all user pages',
    'category' => 'User',
    'type' => 'widget',
	'isPaginated' => true,
    'name' => 'user.user-pages',
	'defaultParams' => array(
      'title' => 'Pages',
      'titleCount' => true,
    ),
    'requirements' => array(
       'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Skills/Endoresment',
    'description' => 'Displays users skills/endorsements',
    'category' => 'User',
    'type' => 'widget',
	'isPaginated' => true,
    'name' => 'user.profile-skills',
	'defaultParams' => array(
      'title' => 'Skills',
      'titleCount' => true,
    ),
    'requirements' => array(
       'subject' => 'user',
    ),
  ),
  array(
    'title' => 'User Wall Posts',
    'description' => 'Displays all user wall posts',
    'category' => 'User',
    'type' => 'widget',
	'isPaginated' => true,
    'name' => 'user.user-wall-post',
	'defaultParams' => array(
      'title' => 'Posts',
      'titleCount' => true,
    ),
    'requirements' => array(
       'subject' => 'user',
    ),
  ),
  array(
    'title' => 'User Friends',
    'description' => 'Displays all user friends',
    'category' => 'User',
    'type' => 'widget',
	'isPaginated' => true,
    'name' => 'user.user-friends',
	'defaultParams' => array(
      'title' => 'Friends',
      'titleCount' => true,
    ),
    'requirements' => array(
       'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Profile Show Reels',
    'description' => 'Displays a member\'s show reels.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.profile-showreels',
	'defaultParams' => array(
      'title' => 'Show Reels',
      'titleCount' => true,
    ),
    'requirements' => array(
      'subject' => 'user',
    ),
  ),
  array(
    'title' => 'Portfolio Banner Section',
    'description' => 'Displays the top section of the Portfolio.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.portfolio-banner-section',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Portfolio Banner Section 2',
    'description' => 'Displays the top section of the Portfolio.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.portfolio-banner-section2',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Portfolio Banner Mobile',
    'description' => 'Displays the top section of the Portfolio with mobile version.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.portfolio-banner-mobile',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Portfolio Banner Mobile2',
    'description' => 'Displays the top section of the Portfolio with mobile version as a user.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.portfolio-banner-mobile2',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Portfolio Banner Section Links',
    'description' => 'All scripts and css links for Portfolio.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.portfolio-banner-section-links',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Portfolio Personal Details',
    'description' => 'Displays Personal details of user.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.portfolio-personal-details',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Portfolio Notification Block',
    'description' => 'Displays Notification details of user.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.portfolio-notification-block',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Portfolio Post Block',
    'description' => 'Displays Block for new post.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.portfolio-post-block',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Portfolio Pinned Post',
    'description' => 'Displays pinned post.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.portfolio-pinnedpost',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
    'title' => 'Portfolio Connections',
    'description' => 'Displays all connections of user.',
    'category' => 'User',
    'type' => 'widget',
    'name' => 'user.portfolio-connections',
    'requirements' => array(
      'viewer',
    ),
  ),
  array(
  'title' => 'Portfolio Posted Jobs',
  'description' => 'Displays all jobs posted by user.',
  'category' => 'User',
  'type' => 'widget',
  'name' => 'user.portfolio-posted-jobs',
  'requirements' => array(
    'viewer',
  ),
  ),
  array(
  'title' => 'Portfolio You May Know',
  'description' => 'Displays all members a user might know.',
  'category' => 'User',
  'type' => 'widget',
  'name' => 'user.portfolio-youmayknow',
  'requirements' => array(
    'viewer',
  ),
  ),
  array(
  'title' => 'Portfolio Reviews',
  'description' => 'Displays all reviews about a member and can submit new review.',
  'category' => 'User',
  'type' => 'widget',
  'name' => 'user.portfolio-review',
  'requirements' => array(
    'viewer',
  ),
  ),
  array(
  'title' => 'Portfolio Reviews2',
  'description' => 'Displays all reviews about a member.',
  'category' => 'User',
  'type' => 'widget',
  'name' => 'user.portfolio-review2',
  'requirements' => array(
    'viewer',
  ),
  ),
  array(
  'title' => 'Portfolio Interested Jobs',
  'description' => 'Displays all jobs user may like.',
  'category' => 'User',
  'type' => 'widget',
  'name' => 'user.portfolio-interested-jobs',
  'requirements' => array(
    'viewer',
  ),
  ),
  array(
  'title' => 'Portfolio Feed Interested Jobs',
  'description' => 'Displays all jobs user may like.',
  'category' => 'User',
  'type' => 'widget',
  'name' => 'user.portfolio-feed-interested-jobs',
  'requirements' => array(
    'viewer',
  ),
  ),
  array(
  'title' => 'Portfolio Similar Services',
  'description' => 'Displays all similar services of user.',
  'category' => 'User',
  'type' => 'widget',
  'name' => 'user.portfolio-similar-services',
  'requirements' => array(
    'viewer',
  ),
  ),
  array(
  'title' => 'Portfolio Album',
  'description' => 'Displays all Photos and videos of user.',
  'category' => 'User',
  'type' => 'widget',
  'name' => 'user.portfolio-albums',
  'requirements' => array(
    'viewer',
  ),
  ),
) ?>