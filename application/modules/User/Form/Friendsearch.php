<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Search.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Form_Friendsearch extends Fields_Form_Search
{
  public function init()
  {
    // Add custom elements
    $this->getDisplayNameElement();
    $this->getAdditionalOptionsElement();

    parent::init();

    $this->loadDefaultDecorators();
    $this->setMethod('get');
    $this->getDecorator('HtmlTag')->setOption('class', 'browsemembers_criteria');
  }

 
  public function getDisplayNameElement()
  {
    $this->addElement('Text', 'displayname', array(
      'label' => '',
	  'placeholder'	=>	'Search..',
      'order' => -1000001,
      'decorators' => array(
        'ViewHelper',
        array('Label', array('tag' => 'span')),
        array('HtmlTag', array('tag' => 'li'))
      ),
      //'onkeypress' => 'return submitEnter(event)',
    ));
    return $this->displayname;
  }

  public function getAdditionalOptionsElement()
  {
    $subform = new Zend_Form_SubForm(array(
      'name' => 'extra',
      'order' => 1000000,
      'decorators' => array(
        'FormElements',
      )
    ));
    Engine_Form::enableForm($subform);

    /*$subform->addElement('Checkbox', 'has_photo', array(
      'label' => 'Only Members With Photos',
      'decorators' => array(
        'ViewHelper',
        array('Label', array('placement' => 'APPEND', 'tag' => 'label')),
        array('HtmlTag', array('tag' => 'li'))
      ),
    ));

    $subform->addElement('Checkbox', 'is_online', array(
	'label' => 'Only Online Members',
      'decorators' => array(
        'ViewHelper',
        array('Label', array('placement' => 'APPEND', 'tag' => 'label')),
        array('HtmlTag', array('tag' => 'li'))
      ),
    ));*/

    $subform->addElement('Button', 'done', array(
      'label' => 'Search',
      'type' => 'submit',
      'ignore' => true,
    ));

    $this->addSubForm($subform, $subform->getName());

    return $this;
  }
}