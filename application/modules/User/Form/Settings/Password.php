<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Password.php 9747 2012-07-26 02:08:08Z john $
 * @author     Steve
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Form_Settings_Password extends Engine_Form
{
  public function init()
  {
    // @todo fix form CSS/decorators
    // @todo replace fake values with real values
    $this->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()))
      ;
	
	$this->setAttrib('id', 'user_form_changepass');
	$this->setAttrib('class', 'change_pass_form');
    // Init old password
    $this->addElement('Password', 'oldPassword1', array(
      'label' => 'Old Password',
	  'placeholder'	=>	'Enter Old Password',
      'autocomplete'	=>	'off',
	  'required' => true,
      'allowEmpty' => false,
    ));

    // Init password
    $this->addElement('Password', 'password1', array(
      'label' => 'New Password',
     // 'description' => 'Passwords must be at least 6 characters in length.',
      'required' => true,
	  'placeholder'	=>	'Enter New Password',
      'autocomplete'	=>	'off',
	  'allowEmpty' => false,
      'validators' => array(
        array('stringLength', false, array(6, 32))
      )
    ));

    // Init password confirm
    $this->addElement('Password', 'passwordConfirm1', array(
      'label' => 'Confirm Password',
	  'placeholder'	=>	'Re-enter New Password',
      'autocomplete'	=>	'off',
      'required' => true,
      'allowEmpty' => false
    ));
    
    // Init submit
    $this->addElement('Button', 'submit_changepass', array(
      'label' => 'Change Password',
      'type' => 'button',
      'class' => 'change_pass',
	  'ignore' => true,
	  'onclick'	=>	'validatechangepass()'
    ));
    
    // Create display group for buttons
    #$this->addDisplayGroup($emailAlerts, 'checkboxes');

    // Set default action
    $this->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()));
  }
}
