<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Forgot.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Form_Auth_Forgot extends Engine_Form
{
  public function init()
  {
    $this
      ->setTitle('Forgot Password')
      ->setDescription('Please type your email ID to create new password.')
      ->setAttrib('id', 'user_form_auth_forgot')
      ;
	//$this->setAttrib('id', 'forgotform');
	$this->setAttrib('class', 'form-control');
    // init email
    $this->addElement('Text', 'email', array(
      'label' => '',
	  'class'	=>	'forgotemail',
	  'placeholder'	=>	'Email',
      'required' => true,
      'allowEmpty' => false,
      'validators' => array(
        'EmailAddress'
      ),
      'tabindex' => 1,
	  'autofocus' => 'autofocus',
      'inputType' => 'email',
    ));
    //$this->email->getValidator('EmailAddress')->getHostnameValidator()->setValidateTld(false);
    // Init submit
    $this->addElement('Button', 'forgot_button', array(
      'label' => 'Send',
	  'onclick'	=>	'validateforgot()',
      'type' => 'button',
      'ignore' => true,
      'tabindex' => 2,
      
    ));
	

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'Go Back To Login Page',
      'link' => true,
     /* 'prependText' => Zend_Registry::get('Zend_Translate')->_(' or '),*/
      'href' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('module' => 'user', 'controller' => 'auth', 'action' => 'login'), 'default', true),
      'decorators' => array(
        'ViewHelper',
      ),
    ));
    
    $this->addDisplayGroup(array(
      'submit',
      'cancel'
    ), 'buttons', array(
      'decorators' => array(
        'FormElements',
        'DivDivDivWrapper',
      ),
    ));
  }
}