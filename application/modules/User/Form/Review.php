<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Messages
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Compose.php 10246 2014-05-30 21:34:20Z andres $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Messages
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Form_Review extends Engine_Form
{
  protected $_user;
  
  
  public function getUser(){
		return $this -> _user;
  }
	
  public function setUser($user){
		$this -> _user = $user;
  } 
	
  public function init()
  {
	$this->setTitle('Add review for '.$this -> _user->getTitle());
    //$this->setTitle('Write a review');
    $this->setAttrib('id', 'user_review_form');
	$this->setAttrib('onsubmit', 'return validatereview()');
	
		
	$this -> addElement('dummy', 'rate', array(
		'decorators' => array( array(
			'ViewScript',
			array(
				'viewScript' => '_rate_member.tpl',
				'class' => 'form element',
			)
		)), 
	));
    
	
	$this->addElement('Text', 'title', array(
      'label' => 'Review Title',
      'placeholder' => 'Title of the review...',
      'allowEmpty' => false,
      'required' => true,
      'validators' => array(
        array('NotEmpty', true),
        array('StringLength', false, array(1, 64)),
      ),
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
      ),
    ));
      // init body - plain text
      $this->addElement('Textarea', 'summary', array(
        'label' => 'Summary',
        'order' => 4,
        'required' => true,
        'allowEmpty' => false,
		'filters' => array(
          new Engine_Filter_HtmlSpecialChars(),
          new Engine_Filter_Censor(),
          new Engine_Filter_EnableLinks(),
        ),
      ));
    // init submit
    $this->addElement('Button', 'submit', array(
      'label' => 'Submit',
      'order' => 5,
      'type' => 'submit',
      'ignore' => true
    ));
  }
  
}