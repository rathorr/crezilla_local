<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Create.php 9802 2012-10-20 16:56:13Z pamela $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Form_Shortlist extends Engine_Form
{
  public function init()
  {
    $this->setTitle('Shortlist User')
      ->setAttrib('name', 'shortlist_user')
	  ->setAttrib('id', 'shortlist_user');
	  
	$viewer = Engine_Api::_()->user()->getViewer();
	
    // prepare shortlistfolder
    $shortlistfolders = Engine_Api::_()->getDbtable('shortlistfolders', 'user')->getShortlistFolders($viewer->getIdentity());
  
      $this->addElement('Select', 'shortlistfolder_id', array(
        'label' => 'Shortlist Folder',
		'required' => true,
        'multiOptions' => $shortlistfolders,
		'onchange'	=>	'showhideprobox();',
      ));
	  
	  $this->addElement('Text', 'folder_name', array(
		  'label' => 'Shortlist Folder Title',
		  'placeholder'	=>	'Enter folder name',
		  'allowEmpty' => false,
		  'filters' => array(
			'StripTags',
			new Engine_Filter_Censor(),
		  ),
		));
		
    // Element: execute
    $this->addElement('Button', 'execute', array(
      'label' => 'Save',
      'type' => 'button',
	  'onclick'	=>	'validateshortlist()',
      'ignore' => true,
      'decorators' => array(
        'ViewHelper',
      ),
    ));

    // DisplayGroup: buttons
    $this->addDisplayGroup(array(
      'execute',
      //'cancel',
    ), 'buttons', array(
      'decorators' => array(
        'FormElements',
        'DivDivDivWrapper'
      ),
    ));
  }

}