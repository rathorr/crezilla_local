<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Form_Social extends Engine_Form
{
  protected $_mode;
  
  public function init()
  {
	$this->setAttrib('id', 'user_form_sociallogin');
	$this->setDescription('Sign In With');
    $_SESSION['redirectURL'] = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
	$settings = Engine_Api::_()->getApi('settings', 'core');
    //echo $settings->getSetting('core_facebook_enable', 'none'); die;
    // Init facebook login link
    if( 'none' != $settings->getSetting('core_facebook_enable', 'none')
        && $settings->core_facebook_secret ) {
      $this->addElement('Dummy', 'facebook', array(
        'content' => User_Model_DbTable_Facebook::loginButton(),
      ));
    }
	
	// Init twitter login link
    if( 'none' != $settings->getSetting('core_twitter_enable', 'none')
        && $settings->core_twitter_secret ) {
      $this->addElement('Dummy', 'twitter', array(
        'content' => User_Model_DbTable_Twitter::loginButton(),
      ));
    }
    
  }
}
