<?php

/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Form_Signup_Account extends Engine_Form_Email
{  
  public function init()
  {
	  	 
    $settings = Engine_Api::_()->getApi('settings', 'core');
    
    $this->_emailAntispamEnabled = ($settings
        ->getSetting('core.spam.email.antispam.signup', 1) == 1) &&
      empty($_SESSION['facebook_signup']) &&
      empty($_SESSION['twitter_signup']) &&
      empty($_SESSION['janrain_signup']);
    
    $inviteSession = new Zend_Session_Namespace('invite');
    $tabIndex = 1;
    
	$this->setAttrib('id', 'signupform');
	$this->setAttrib('class', 'form-control');
    // Init form
	if( $settings->getSetting('user.signup.random', 0) == 0 &&  empty($_SESSION['facebook_signup']) && empty($_SESSION['twitter_signup'])) {
    		$this->setTitle('JOIN');
	}else{
		$this->setTitle('JOIN');	
	}
  
 $description = Zend_Registry::get('Zend_Translate')->_("Already Registered? <a  class='asign' href='/login'> SIGN IN</a>");
$this->setDescription($description);

$this->loadDefaultDecorators();
    $this->getDecorator('Description')->setOption('escape', false);
    // Element: name (trap)
    /*$this->addElement('Text', 'name', array(
      'class' => 'signup-name',
      'label' => 'Name',
      'validators' => array(
	      array('StringLength', true, array('max' => 0)))));

    $this->name->getValidator('StringLength')->setMessage('An error has occured, please try again later.');*/
        $this->addElement('Text', 'first_name', array(
        'label' => 'First Name',
		    'placeholder'	=>	'First name',
        'description' => 'You must give a first name.',
        'required' => true,
        'allowEmpty' => false,
        'validators' => array(
          array('NotEmpty', true),
          array('StringLength', false, array(4, 40)),
        ),
        'autofocus' => 'autofocus',
        'tabindex' => $tabIndex++,
      ));
      $this->first_name->getDecorator('Description')->setOptions(array('placement' => 'APPEND'));
      $this->first_name->getValidator('NotEmpty')->setMessage('Please enter your first name.', 'isEmpty');
      // Middle Name
     /* $this->addElement('Text', 'middlename', array(
        'label' => 'middlename',
		'placeholder'	=>	'Middle name',
        'description' => 'You must give a middle name.',
        'required' => true,
        'allowEmpty' => false,
        'validators' => array(
          array('NotEmpty', true),
          array('StringLength', false, array(4, 40)),
        ),
        'autofocus' => 'autofocus',
        'tabindex' => $tabIndex++,
      ));
      $this->middlename->getDecorator('Description')->setOptions(array('placement' => 'APPEND'));
      $this->middlename->getValidator('NotEmpty')->setMessage('Please enter your middle name.', 'isEmpty');*/
      // Last Name
      $this->addElement('Text', 'last_name', array(
        'label' => 'Last Name',
		'placeholder'	=>	'Last name',
        'description' => 'You must give a last name.',
        'required' => true,
        'allowEmpty' => false,
        'validators' => array(
          array('NotEmpty', true),
          array('StringLength', false, array(4, 40)),
        ),
        'autofocus' => 'autofocus',
        'tabindex' => $tabIndex++,
      ));
      $this->last_name->getDecorator('Description')->setOptions(array('placement' => 'APPEND'));
      $this->last_name->getValidator('NotEmpty')->setMessage('Please enter your last name.', 'isEmpty');

    $emailElement = $this->addEmailElement(array(
      'label' => 'Email Address',
	  'placeholder'	=>	'Email Address',
      'description' => 'You will use your email address to login.',
      'required' => true,
      'allowEmpty' => false,
	  'class'	=>	'signupemail',
	  'id'	=>	'signup_email',
      'validators' => array(
        array('NotEmpty', true),
        array('EmailAddress', true),
        array('Db_NoRecordExists', true, array(Engine_Db_Table::getTablePrefix() . 'users', 'email'))
      ),
      'filters' => array(
        'StringTrim'
      ),
      // fancy stuff
      'inputType' => 'email',
      'autofocus' => 'autofocus',
      'tabindex' => $tabIndex++,
    ));
    $emailElement->getDecorator('Description')->setOptions(array('placement' => 'APPEND'));
    $emailElement->getValidator('NotEmpty')->setMessage('Please enter a valid email address.', 'isEmpty');
    $emailElement->getValidator('Db_NoRecordExists')->setMessage('Someone has already registered this email address, please use another one.', 'recordFound');
    $emailElement->getValidator('EmailAddress')->getHostnameValidator()->setValidateTld(false);
    // Add banned email validator
    $bannedEmailValidator = new Engine_Validate_Callback(array($this, 'checkBannedEmail'), $emailElement);
    $bannedEmailValidator->setMessage("This email address is not available, please use another one.");
    $emailElement->addValidator($bannedEmailValidator);
    
    if( !empty($inviteSession->invite_email) ) {
      $emailElement->setValue($inviteSession->invite_email);
    }
    
    if( $settings->getSetting('user.signup.random', 0) == 0 && 
        empty($_SESSION['facebook_signup']) && 
        empty($_SESSION['twitter_signup']) && 
        empty($_SESSION['janrain_signup']) ) {

      // Element: password
      $this->addElement('Text', 'password', array(
        'label' => 'Password',
		'placeholder'	=>	'Password',
        'description' => 'Passwords must be at least 6 characters in length.',
        'required' => true,
        'allowEmpty' => false,
        'validators' => array(
          array('NotEmpty', true),
          array('StringLength', false, array(6, 32)),
        ),
        'tabindex' => $tabIndex++,
      ));
      $this->password->getDecorator('Description')->setOptions(array('placement' => 'APPEND'));
      $this->password->getValidator('NotEmpty')->setMessage('Please enter a valid password.', 'isEmpty');

     $this->addElement('Text', 'mobileno', array(
        'label' => 'Mobile',
        'required' => false,
        'allowEmpty' => true,
        'tabindex' => $tabIndex++,
    'placeholder'=>'Mobile Number',
    'id' => 'mobileno',
      ));
$this->addElement('Select', 'bdate', array(
		  'label' => 'Birthday',
		  'value' => '',
		  'multiOptions' => array(
			'' => 'Date',
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
			'13' => '13',
			'14' => '14',
			'15' => '15',
			'16' => '16',
			'17' => '17',
			'18' => '18',
			'19' => '19',
			'20' => '20',
			'21' => '21',
			'22' => '22',
			'23' => '23',
			'24' => '24',
			'25' => '25',
			'26' => '26',
			'27' => '27',
			'28' => '28',
			'29' => '29',
			'30' => '30',
			'31' => '31',
		  ),
		  'tabindex' => $tabIndex++,
		));
      $this->addElement('Select', 'bmonth', array(
		  'label' => '',
		  'value' => '',
		  'multiOptions' => array(
		  	'' => 'Month',
			'1' => 'January',
			'2' => 'February',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		  ),
		  'tabindex' => $tabIndex++,
		));
	  
		
	  	  
	  $this->addElement('Select', 'byear', array(
		  'label' => '',
		  'value' => '',
		  'multiOptions' => array(
			'' => 'Year',
			'2016' => '2016',
			'2015' => '2015',
			'2014' => '2014',
			'2013' => '2013',
			'2012' => '2012',
			'2011' => '2011',
			'2010' => '2010',
			'2009' => '2009',
			'2008' => '2008',
			'2007' => '2007',
			'2006' => '2006',
			'2005' => '2005',
			'2004' => '2004',
			'2003' => '2003',
			'2002' => '2002',
			'2001' => '2001',
			'2000' => '2000',
			'1999' => '1999',
			'1998' => '1998',
			'1997' => '1997',
			'1996' => '1996',
			'1995' => '1995',
			'1994' => '1994',
			'1993' => '1993',
			'1992' => '1992',
			'1991' => '1991',
			'1990' => '1990',
			'1989' => '1989',
			'1988' => '1988',
			'1987' => '1987',
			'1986' => '1986',
			'1985' => '1985',
			'1984' => '1984',
			'1983' => '1983',
			'1982' => '1982',
			'1981' => '1981',
			'1980' => '1980',
			'1979' => '1979',
			'1978' => '1978',
			'1977' => '1977',
			'1976' => '1976',
			'1975' => '1975',
			'1974' => '1974',
			'1973' => '1973',
			'1972' => '1972',
			'1971' => '1971',
			'1970' => '1970',
			'1969' => '1969',
			'1968' => '1968',
			'1967' => '1967',
			'1966' => '1966',
			'1965' => '1965',
			'1964' => '1964',
			'1963' => '1963',
			'1962' => '1962',
			'1961' => '1961',
			'1960' => '1960',
			'1959' => '1959',
			'1958' => '1958',
			'1957' => '1957',
			'1956' => '1956',
			'1955' => '1955',
			'1954' => '1954',
			'1953' => '1953',
			'1952' => '1952',
			'1951' => '1951',
			'1950' => '1950',
		  ),
		  'tabindex' => $tabIndex++,
		));
		
		
	/* OPTIONAL FIELDS */
	 
	
	}
	//Gender
	$this->addElement('radio', 'gender', array(
    'label'=>'Gender',
    'class' => 'signup-name',
    'multiOptions'=>array(
        '2' => 'Male',
        '3' => 'Female',
        '111' => 'Other',
    ),
));
	
    // Element: username
    if( $settings->getSetting('user.signup.username', 1) > 0 ) {
      $description = Zend_Registry::get('Zend_Translate')
          ->_('This will be the end of your profile link, for example: <br /> ' .
              '<span id="profile_address">http://%s</span>');
      $description = sprintf($description, $_SERVER['HTTP_HOST']
          . Zend_Controller_Front::getInstance()->getRouter()
          ->assemble(array('id' => 'yourname'), 'user_profile'));

      $this->addElement('Text', 'username', array(
        'label' => 'Profile Address',
        'description' => $description,
        'required' => true,
        'allowEmpty' => false,
        'validators' => array(
          array('NotEmpty', true),
          array('Alnum', true),
          array('StringLength', true, array(4, 64)),
          array('Regex', true, array('/^[a-z][a-z0-9]*$/i')),
          array('Db_NoRecordExists', true, array(Engine_Db_Table::getTablePrefix() . 'users', 'username'))
        ),
        'tabindex' => $tabIndex++,
          //'onblur' => 'var el = this; en4.user.checkUsernameTaken(this.value, function(taken){ el.style.marginBottom = taken * 100 + "px" });'
      ));
      $this->username->getDecorator('Description')->setOptions(array('placement' => 'APPEND', 'escape' => false));
      $this->username->getValidator('NotEmpty')->setMessage('Please enter a valid profile address.', 'isEmpty');
      $this->username->getValidator('Db_NoRecordExists')->setMessage('Someone has already picked this profile address, please use another one.', 'recordFound');
      $this->username->getValidator('Regex')->setMessage('Profile addresses must start with a letter.', 'regexNotMatch');
      $this->username->getValidator('Alnum')->setMessage('Profile addresses must be alphanumeric.', 'notAlnum');

      // Add banned username validator
      $bannedUsernameValidator = new Engine_Validate_Callback(array($this, 'checkBannedUsername'), $this->username);
      $bannedUsernameValidator->setMessage("This profile address is not available, please use another one.");
      $this->username->addValidator($bannedUsernameValidator);
    }    
 	
	 $url_data=$_SERVER['REQUEST_URI'];
	 
	$url_array=explode('/',$url_data);
	$i=count($url_array);
	$set_id=$url_array[$i-1];
	 $set_id=(int)$set_id;
	
    // Element: profile_type
    $topStructure = Engine_Api::_()->fields()->getFieldStructureTop('user');
	//echo '<pre>'; print_r($topStructure);echo '</pre>'; die;
	
	
    if( count($topStructure) == 1 && $topStructure[0]->getChild()->type == 'profile_type' ) {
      $profileTypeField = $topStructure[0]->getChild();
      $options = $profileTypeField->getOptions();
	 /* echo "<pre>";
	  print_r($options);
	  echo "</pre>";*/
	  
      if( count($options) > 1 ) {
		 
        $options = $profileTypeField->getElementParams('user');
        unset($options['options']['order']);
        //unset($options['options']['multiOptions']['0']);
		$options['options']['multiOptions']['']	=	'Profile Type';
        
			
	   $this->addElement('Text', 'profile_type', array(
          'value' => $set_id,
		  'class' => 'hide_box',
        ));		
      } else if( count($options) == 1 ) {
		 
        /*$this->addElement('Hidden', 'profile_type', array(
          'value' => $options[0]->option_id,
        ));*/
		
		$this->addElement('Text', 'profile_type', array(
          'value' => $options[0]->option_id,
		  'class' => 'hide_box',
        ));
		
		
      }
    }
	
	// Element: language

    // Languages
    $translate = Zend_Registry::get('Zend_Translate');
    $languageList = $translate->getList();

    //$currentLocale = Zend_Registry::get('Locale')->__toString();
    // Prepare default langauge
    $defaultLanguage = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.locale.locale', 'en');
    if( !in_array($defaultLanguage, $languageList) ) {
      if( $defaultLanguage == 'auto' && isset($languageList['en']) ) {
        $defaultLanguage = 'en';
      } else {
        $defaultLanguage = null;
      }
    }

    // Prepare language name list
    $localeObject = Zend_Registry::get('Locale');
    
    $languageNameList = array();
    $languageDataList = Zend_Locale_Data::getList($localeObject, 'language');
    $territoryDataList = Zend_Locale_Data::getList($localeObject, 'territory');

    foreach( $languageList as $localeCode ) {
      $languageNameList[$localeCode] = Zend_Locale::getTranslation($localeCode, 'language', $localeCode);
      if( empty($languageNameList[$localeCode]) ) {
        list($locale, $territory) = explode('_', $localeCode);
        $languageNameList[$localeCode] = "{$territoryDataList[$territory]} {$languageDataList[$locale]}";
      }
    }
   /* $languageNameList = array_merge(array(
      $defaultLanguage => $defaultLanguage
    ), $languageNameList);*/

    if(count($languageNameList)>1){
      $this->addElement('Select', 'language', array(
        'label' => 'Language',
        'multiOptions' => $languageNameList,
        'tabindex' => $tabIndex++,
      ));
      $this->language->getDecorator('Description')->setOptions(array('placement' => 'APPEND'));
    }
    else{
      $this->addElement('Hidden', 'language', array(
        'value' => current((array)$languageNameList) 
      ));
    }

    // Element: captcha
    /*if( Engine_Api::_()->getApi('settings', 'core')->core_spam_signup ) {
      $this->addElement('captcha', 'captcha', Engine_Api::_()->core()->getCaptchaOptions(array(
        'tabindex' => $tabIndex++,
      )));
    }*/
    
    if( $settings->getSetting('user.signup.terms', 1) == 1 ) {
      // Element: terms
   
      $this->addElement('Checkbox', 'terms', array(
        'label' => 'Terms of Service',
        'description' => 'I have read and agree to the <a target="_blank" href="/help/terms">terms of service</a> and <a target="_blank" href="/help/privacy">privacy policy.</a>',
        //'description' => $description,
        'required' => true,
        'validators' => array(
          'notEmpty',
          array('GreaterThan', false, array(0)),
        ),
        'tabindex' => $tabIndex++,
      ));
      $this->terms->getValidator('GreaterThan')->setMessage('You must agree to the terms of service to continue.', 'notGreaterThan');
      //$this->terms->getDecorator('Label')->setOption('escape', false);

      $this->terms->clearDecorators()
          ->addDecorator('ViewHelper')
          ->addDecorator('Description', array('placement' => Zend_Form_Decorator_Abstract::APPEND, 'tag' => 'label', 'class' => 'null', 'escape' => false, 'for' => 'terms'))
          ->addDecorator('DivDivDivWrapper');

      //$this->terms->setDisableTranslator(true);
    }
    
    // Init submit
    $this->addElement('Button', 'continue', array(
      'label' => 'JOIN',
      'type' => 'button',
      'title' => 'Joining is currently disabled',
     // 'ignore' => true,
     // 'tabindex' => $tabIndex++,
      'attribs'    => array('disabled' => 'disabled'),
	  'onclick'	=>	'validatesignup()',
    ));
    
    if( empty($_SESSION['facebook_signup']) ){
   
    }
    
    if( empty($_SESSION['twitter_signup']) ){
    
    } 
    // Set default action
    $this->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'user_signup', true));
	
	
	
	/*$content = Zend_Registry::get('Zend_Translate')->_("<span><a href='%s' class='back_login'>Back to login</a></span>");
    $content= sprintf($content, Zend_Controller_Front::getInstance()->getRouter()->assemble(array('module' => 'user', 'controller' => 'auth', 'action' => 'login'), 'default', true));


    // Init forgot password link
    $this->addElement('Dummy', 'submit_login', array(
      'content' => $content,
    ));*/
  }

  public function checkPasswordConfirm($value, $passwordElement)
  {
    return ( $value == $passwordElement->getValue() );
  }

  public function checkInviteCode($value, $emailElement)
  {
    $inviteTable = Engine_Api::_()->getDbtable('invites', 'invite');
    $select = $inviteTable->select()
      ->from($inviteTable->info('name'), 'COUNT(*)')
      ->where('code = ?', $value)
      ;
      
    if( Engine_Api::_()->getApi('settings', 'core')->getSetting('user.signup.checkemail') ) {
      $select->where('recipient LIKE ?', $emailElement->getValue());
    }
    
    return (bool) $select->query()->fetchColumn(0);
  }

  public function checkBannedEmail($value, $emailElement)
  {
    $bannedEmailsTable = Engine_Api::_()->getDbtable('BannedEmails', 'core');
    return !$bannedEmailsTable->isEmailBanned($value);
  }

  public function checkBannedUsername($value, $usernameElement)
  {
    $bannedUsernamesTable = Engine_Api::_()->getDbtable('BannedUsernames', 'core');
    return !$bannedUsernamesTable->isUsernameBanned($value);
  }
  
  function msort($array, $key, $sort_flags = SORT_REGULAR) {
		if (is_array($array) && count($array) > 0) {
			if (!empty($key)) {
				$mapping = array();
				foreach ($array as $k => $v) {
					$sort_key = '';
					if (!is_array($key)) {
						$sort_key = $v[$key];
					} else {
						// @TODO This should be fixed, now it will be sorted as string
						foreach ($key as $key_key) {
							$sort_key .= $v[$key_key];
						}
						$sort_flags = SORT_STRING;
					}
					$mapping[$k] = $sort_key;
				}
				asort($mapping, $sort_flags);
				$sorted = array();
				foreach ($mapping as $k => $v) {
					$sorted[] = $array[$k];
				}
				return $sorted;
			}
		}
		return $array;
	}
}
