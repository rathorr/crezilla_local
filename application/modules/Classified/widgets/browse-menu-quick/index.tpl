<style type="text/css">
.generic_layout_container.layout_classified_browse_menu_quick {
    float: right;margin-right: 30px;
}
 

</style>
<?php if( count($this->quickNavigation) > 0 ): ?>
  <div class="quicklinks">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->quickNavigation)
        ->render();
    ?>
  </div>
<?php endif; ?>
