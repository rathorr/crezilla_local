<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Widget_ApplyClassifiedsController extends Engine_Content_Widget_Abstract
{
  public function init()
  {
    if( !$this->_helper->requireAuth()->setAuthParams('classified', null, 'view')->isValid() ) return;
  }
  
  public function indexAction()
  {
	$viewer = Engine_Api::_()->user()->getViewer();
    if( !Engine_Api::_()->core()->hasSubject() ) {
      return $this->setNoRender();
    }

    // Get subject and check auth
    $subject = Engine_Api::_()->core()->getSubject();
    if( !$subject->authorization()->isAllowed($viewer, 'view') ) {
      return $this->setNoRender();
    }

    $this->view->allowed_upload = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'classified', 'photo');
    
    $this->view->form = $form = new Classified_Form_Search();
    $form->removeElement('show');

    // Populate form
    $categories = Engine_Api::_()->getDbtable('categories', 'classified')->getCategoriesAssoc();
    if( !empty($categories) && is_array($categories) && $form->getElement('category') ) {
      $form->getElement('category')->addMultiOptions($categories);
    }    
   
    $values = array();
    //$customFieldValues = $form->getSubForm('custom')->getValues();
    $values['user_id'] = $viewer->getIdentity();

    // custom field search
    $customFieldValues = array_intersect_key($values, $form->getFieldElements());

    // Get paginator
    $this->view->paginator = $paginator = Engine_Api::_()->getItemTable('classified')->getClassifiedsPaginator($values, $customFieldValues);
    $items_count = (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('classified.page', 10);
	
    $paginator->setItemCountPerPage($items_count);
    $this->view->paginator = $paginator->setCurrentPageNumber( $this->_getParam('page',1) );

	foreach($paginator as $item){
		if( !Engine_Api::_()->core()->hasSubject() ) {
			 return $this->setNoRender();
		}
	
		// Get subject and check auth
		$subject = $item;
		if( !$subject->authorization()->isAllowed($viewer, 'view') ) {
			return $this->setNoRender();
		}
		
		// Calculate viewer-subject relationship
		$usePrivacy = ($subject instanceof User_Model_User);
		if( $usePrivacy ) {
		  $relationship = 'everyone';
		  if( $viewer && $viewer->getIdentity() ) {
			if( $viewer->getIdentity() == $subject->getIdentity() ) {
			  $relationship = 'self';
			} else if( $viewer->membership()->isMember($subject, true) ) {
			  $relationship = 'friends';
			} else {
			  $relationship = 'registered';
			}
		  }
		}	
		
		// Load fields view helpers
		$view = $this->view;
		$view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
	   
		// Values
		$this->view->fieldStructure = $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);

		if( count($fieldStructure) <= 1 ) { // @todo figure out right logic
			 return $this->setNoRender();
		   }
	   
		$valuesStructure = array();
		$valueCount = 0;
		
		$show_hidden = $viewer->getIdentity()
		 ? ($subject->getOwner()->isSelf($viewer) || 'admin' === Engine_Api::_()->getItem('authorization_level', $viewer->level_id)->type)
		 : false;
		 
		$fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($item); 

		foreach( $fieldStructure as $map )
			   {							
				  // Get field meta object
				  $field = $map->getChild();
				  
				  //print '<pre>';print_r($field);
				  $value = $field->getValue($subject);
				  if( !$field || $field->type == 'profile_type' ) continue;
				  if( !$field->display && !$show_hidden ) continue;
				  $isHidden = !$field->display;
				  
				  // Get first value object for reference
				  $firstValue = $value;
				  if( is_array($value) && !empty($value) ) {
					$firstValue = $value[0];
				  }
				  
				  // Evaluate privacy
				  if( $usePrivacy && !empty($firstValue->privacy) && $relationship != 'self' ) {
					if( $firstValue->privacy == 'self' && $relationship != 'self' ) {
					  $isHidden = true; //continue;
					} else if( $firstValue->privacy == 'friends' && ($relationship != 'friends' && $relationship != 'self') ) {
					  $isHidden = true; //continue;
					} else if( $firstValue->privacy == 'registered' && $relationship == 'everyone' ) {
					  $isHidden = true; //continue;
					}
				  }
				  $helper = new Fields_View_Helper_FieldValueLoop();
					$helper->view = Zend_Registry::get('Zend_View');
				  $tmp = $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);
					if($field->label=='Category'){
						$val['category'][$item->classified_id] = $tmp;
					}
					if($field->label=='Location'){
						$val['location'][$item->classified_id] = $tmp;
					}

				}
				
				//GET TOTAL NUMBER OF APPLICANTS
				$table = Engine_Api::_()->getDbtable('applys', 'classified');	
				$applyname = $table->info('name'); 
				$select = $table->select("$applyname.*")
					->where("$applyname.classified_id = ".$item->classified_id, 1);
	
		
				$result = $table->fetchAll($select);
				//echo $result->count();
				$val['applicant'][$item->classified_id] = $result->count();
	}
	
	//echo '<pre>';print_r($val);die;
	$this->view->category =  $val['category'];
	$this->view->location =  $val['location'];
	$this->view->applicant =  $val['applicant'];

    // maximum allowed classifieds
    $this->view->quota = $quota = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'classified', 'max');
    $this->view->current_count = $paginator->getTotalItemCount();
  }
}