<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: manage.tpl 9987 2013-03-20 00:58:10Z john $
 * @author     Jung
 */
?>
<style>
.classifieds_browse_photo{
	width:111px;
}
</style>

<script type="text/javascript">
  var pageAction =function(page){
    $('page').value = page;
    $('filter_form').submit();
  }

  var searchClassifieds = function() {
    if( Browser.Engine.trident ) {
      document.getElementById('filter_form').submit();
    } else {
      $('filter_form').submit();
    }
  }

  en4.core.runonce.add(function(){
    $$('#filter_form input[type=text]').each(function(f) {
        if (f.value == '' && f.id.match(/\min$/)) {
            new OverText(f, {'textOverride':'min','element':'span'});
            //f.set('class', 'integer_field_unselected');
        }
        if (f.value == '' && f.id.match(/\max$/)) {
            new OverText(f, {'textOverride':'max','element':'span'});
            //f.set('class', 'integer_field_unselected');
        }
    });
  });

  window.addEvent('onChangeFields', function() {
    var firstSep = $$('li.browse-separator-wrapper')[0];
    var lastSep;
    var nextEl = firstSep;
    var allHidden = true;
    do {
      nextEl = nextEl.getNext();
      if( nextEl.get('class') == 'browse-separator-wrapper' ) {
        lastSep = nextEl;
        nextEl = false;
      } else {
        allHidden = allHidden && ( nextEl.getStyle('display') == 'none' );
      }
    } while( nextEl );
    if( lastSep ) {
      lastSep.setStyle('display', (allHidden ? 'none' : ''));
    }
  });
</script>

<script type="text/javascript">
  en4.core.runonce.add(function() {
    // Enable links
    $$('.classifieds_browse_info_blurb').enableLinks();
  });
</script>

<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
    //'topLevelId' => (int) @$this->topLevelId,
    //'topLevelValue' => (int) @$this->topLevelValue
  ))
?>

  <?php if (($this->current_count >= $this->quota) && !empty($this->quota)):?>
    <div class="tip">
      <span>
        <?php echo $this->translate('You have already created the maximum number of listings allowed. If you would like to create a new listing, please delete an old one first.');?>
      </span>
    </div>
    <br/>
  <?php endif; ?>
   <ul class="classifieds_browse">
	  <li> <div class='classifieds_browse_info'><div class='classifieds_browse_photo'>Image</div>
		<div class='classifieds_browse_photo'>Headlines</div>
		<div class='classifieds_browse_photo'>Talent Required</div>
	  <div class='classifieds_browse_photo'>Location</div>
	  <div class='classifieds_browse_photo'>Applicants</div>
	  <div class='classifieds_browse_photo'>Craeted Date</div>
	  <div class='classifieds_browse_photo'>Status</div>
	  <div class='classifieds_browse_photo'>Actions</div></div></li>
  </ul>
  <?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
    <ul class="classifieds_browse">
      <?php foreach( $this->paginator as $item ): 
	  //echo '<pre>';print_r($item);
	  ?>
        <li>           
          <div class='classifieds_browse_info'>
			<div class='classifieds_browse_photo'>
				<?php echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'thumb.normal')) ?>
			</div>
            <div class='classifieds_browse_photo'>
              <h3>
                <?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?>
                <?php if( $item->closed ): ?>
                  <img alt="close" src='<?php echo $this->layout()->staticBaseUrl ?>application/modules/Classified/externals/images/close.png'/>
                <?php endif;?>
              </h3>
            </div>
			<div class='classifieds_browse_photo'>
              <?php echo $this->category[$item->classified_id]; ?>
            </div>
			<div class='classifieds_browse_photo'>
              <?php echo $this->location[$item->classified_id]; ?>
            </div>
			<div class='classifieds_browse_photo'>
              <?php 
			  $getlink = "<a href ='".$this->baseUrl()."/classifieds/application/list/classified_id/".$item->classified_id."'>".$this->applicant[$item->classified_id]." Applicant(s)</a>";
			  echo ($this->applicant[$item->classified_id] == 0)?"No Applicant yet":$getlink; ?>
            </div>
            <div class='classifieds_browse_photo'>
              <?php 
			  //print_r($this->category);
			  echo $this->timestamp(strtotime($item->creation_date)) ?>
            </div>
			<div class='classifieds_browse_photo'>
              <?php echo ($item->closed == 1)?"Closed":"Open"; ?>
            </div>
			

		  <div class='classifieds_browse_photo'>
			
            <?php echo $this->htmlLink(array(
              'route' => 'classified_specific',
              'action' => 'edit',
              'classified_id' => $item->getIdentity(),
            ), $this->translate('Edit Listing'), array(
              'class' => 'buttonlink icon_classified_edit'
            )) ?>
            
            <?php if( $this->allowed_upload ): ?>
              <?php echo $this->htmlLink(array(
                  'route' => 'classified_extended',
                  'controller' => 'photo',
                  'action' => 'upload',
                  'classified_id' => $item->getIdentity(),
                ), $this->translate('Add Photos'), array(
                  'class' => 'buttonlink icon_classified_photo_new'
              )) ?>
            <?php endif; ?>

            <?php if( !$item->closed ): ?>
              <?php echo $this->htmlLink(array(
                'route' => 'classified_specific',
                'action' => 'close',
                'classified_id' => $item->getIdentity(),
                'closed' => 1,
              ), $this->translate('Close Listing'), array(
                'class' => 'buttonlink icon_classified_close'
              )) ?>
            <?php else: ?>
              <?php echo $this->htmlLink(array(
                'route' => 'classified_specific',
                'action' => 'close',
                'classified_id' => $item->getIdentity(),
                'closed' => 0,
              ), $this->translate('Open Listing'), array(
                'class' => 'buttonlink icon_classified_open'
              )) ?>
            <?php endif; ?>
            
            <?php echo $this->htmlLink(array('route' => 'default', 'module' => 'classified', 'controller' => 'index', 'action' => 'delete', 'classified_id' => $item->getIdentity(), 'format' => 'smoothbox'), $this->translate('Delete Listing'), array(
              'class' => 'buttonlink smoothbox icon_classified_delete'
            )) ?>
          </div>
		  </div>
        </li>
      <?php endforeach; ?>
    </ul>

  <?php elseif($this->search): ?>
    <div class="tip">
      <span>
        <?php echo $this->translate('You do not have any classified listing that match your search criteria.');?>
      </span>
    </div>
  <?php else:?>
    <div class="tip">
      <span>
        <?php echo $this->translate('You do not have any classified listings.');?>
        <?php if ($this->can_create): ?>
          <?php echo $this->translate('Get started by <a href=\'%1$s\'>posting</a> a new listing.', $this->url(array('action' => 'create'), 'classified_general'));?>
        <?php endif; ?>
      </span>
    </div>
  <?php endif; ?>
  <?php echo $this->paginationControl($this->paginator, null, null); ?>


<script type="text/javascript">
  $$('.core_main_classified').getParent().addClass('active');
</script>
