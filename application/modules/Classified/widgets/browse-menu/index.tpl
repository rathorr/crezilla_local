
<div class="headline">
  <h2>
    <?php echo $this->translate('Manage Jobs') ?>
  </h2>
  <div class="tabs" style="display:none;">
    <?php
      // Render the menu
      echo $this->navigation()
        ->menu()
        ->setContainer($this->navigation)
        ->render();
    ?>
  </div>
</div>
