<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Widget_ProfileClassifiedsController extends Engine_Content_Widget_Abstract
{
  protected $_childCount;
  
  public function indexAction()
  {
    // Don't render this if not authorized
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    if( !Engine_Api::_()->core()->hasSubject() ) {
      return $this->setNoRender();
    }

    // Get subject and check auth
    $this->view->subject = $subject = Engine_Api::_()->core()->getSubject();
	
	if( $subject->getIdentity() == $viewer->getIdentity() ) {
      return $this->setNoRender();
    }
    /*if( !$subject->authorization()->isAllowed($viewer, 'view') ) {
      return $this->setNoRender();
    }*/
	
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType($subject->getIdentity());
	
	 /*if(strtolower($profile_type) == 'talent'){
		 return $this->setNoRender();
	 }*/

    // Just remove the title decorator
    $this->getElement()->removeDecorator('Title');
 	$values = array(
      'orderby' => 'creation_date',
      'user_id' =>  Engine_Api::_()->core()->getSubject()->getIdentity(),
    );
	
	
	$viewer = Engine_Api::_()->user()->getViewer();
	$this->view->user_id = $viewer->getIdentity();
	$projects = $categories = Engine_Api::_()->getDbtable('parenttypes', 'classified')->getListAssoc($subject);
		
	$posted_jobs = array();
	$i = 0;
	
	 // Get paginator
    $this->view->paginator = $paginator = Engine_Api::_()->getItemTable('classified')->getClassifiedsPaginator($values);
//echo '<pre>'; print_r($values); die;
    // Set item count per page and current page number
    $paginator->setItemCountPerPage($this->_getParam('itemCountPerPage', 5));
    $paginator->setCurrentPageNumber($this->_getParam('page', 1));
	
	
	if(count($projects) > 0)
	{
		foreach($projects as $project)
		{
			$values['project'] 					= $project['project_id']; 
			$posted_jobs[$i]['project_name'] 	= $project['parent_name'];
			$posted_jobs[$i]['project_desc'] 	= $project['parent_desc'];
			$posted_jobs[$i]['project_id'] 		= $project['project_id'];
			$posted_jobs[$i]['permission'] 		= $project['permission'];
			
			
			$posted_jobs[$i]['project_jobs'] 	= Engine_Api::_()->getItemTable('classified')->getClassifiedsPaginator($values);
			$i++;
			$total_jobs	+=	count($posted_jobs[$i]['project_jobs']);
		}
	}
   
	$this->view->projects = $posted_jobs;


    // Do not render if nothing to show
    /*if( $paginator->getTotalItemCount() <= 0 ) {
      return $this->setNoRender();
    }*/
	$total_jobs	=	0;
	
	
	if(count($posted_jobs)>0){
		foreach($posted_jobs as $prj){
			foreach($prj['project_jobs'] as $post){
				$total_jobs++;
			}
			//$total_jobs	+=	count($prj['project_jobs']);
		}	
	}
	$this->_childCount	=	$total_jobs;
    // Add count to title if configured
    /*if( $this->_getParam('titleCount', false) && $paginator->getTotalItemCount() > 0 ) {
      //$this->_childCount = $paginator->getTotalItemCount();
	  //$this->_childCount	=	count($posted_jobs);
	  $this->_childCount	=	$total_jobs;
    }*/
  }

  public function getChildCount()
  {
    return $this->_childCount;
  }
}