<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Controller.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Widget_HomePageJobsController extends Engine_Content_Widget_Abstract
{
  protected $_childCount;
  
  public function indexAction()
  {
    // Don't render this if not authorized
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    if( !$viewer->getIdentity() ) {
      return $this->setNoRender();
    }
 	$user_id = $viewer->getIdentity();
	/*if( Engine_Api::_()->core()->hasSubject() ) {
      $user_id =  Engine_Api::_()->core()->getSubject()->getIdentity();
    }*/
	
	 $profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType($viewer->getIdentity());
	 /*if(strtolower($profile_type) == 'talent'){
		 return $this->setNoRender();
	 }*/

    // Just remove the title decorator
    $this->getElement()->removeDecorator('Title');

    // Get paginator
    $this->view->paginator = $paginator = Engine_Api::_()->getItemTable('classified')->getClassifiedsPaginator(array(
      'orderby' => 'creation_date',
      'user_id' =>  $user_id,
    ));
	
	

    // Set item count per page and current page number
    $paginator->setItemCountPerPage($this->_getParam('itemCountPerPage', 3));
    $paginator->setCurrentPageNumber($this->_getParam('page', 1));

    // Do not render if nothing to show
    /*if( $paginator->getTotalItemCount() <= 0 ) {
      return $this->setNoRender();
    }*/

    // Add count to title if configured
    if( $this->_getParam('titleCount', false) && $paginator->getTotalItemCount() > 0 ) {
      $this->_childCount = $paginator->getTotalItemCount();
    }
  }

  public function getChildCount()
  {
    return $this->_childCount;
  }
}