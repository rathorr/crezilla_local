<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */
?>

<script type="text/javascript">
  en4.core.runonce.add(function(){

    <?php if( !$this->renderOne ): ?>
    var anchor = $('profile_classifieds').getParent();
    $('profile_classifieds_previous').style.display = '<?php echo ( $this->paginator->getCurrentPageNumber() == 1 ? 'none' : '' ) ?>';
    $('profile_classifieds_next').style.display = '<?php echo ( $this->paginator->count() == $this->paginator->getCurrentPageNumber() ? 'none' : '' ) ?>';

    $('profile_classifieds_previous').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() - 1) ?>
        }
      }), {
        'element' : anchor
      })
    });

    $('profile_classifieds_next').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() + 1) ?>
        }
      }), {
        'element' : anchor
      })
    });
    <?php endif; ?>
  });
</script>

<table class="profile_jp">
      <tr>
        <th scope="col">Image</th>
        <th scope="col">Headlines</th>
        <th scope="col">Talent Required</th>
        <th scope="col">Location</th>
         <?php if($this->viewer->getIdentity() == $this->subject->getIdentity()){?>
        <th scope="col">Applicants</th>
        <?php } ?>
        <th scope="col">Created date</th>
        <th scope="col">Status</th>
         <?php if($this->viewer->getIdentity() == $this->subject->getIdentity()){?>
        <th scope="col">Action</th>
        <?php } ?>
      </tr>
  <?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
      <?php foreach( $this->paginator as $item ): 
     // echo '<pre>'; print_r($item); echo '</pre>';
      ?>
      <tr>
        <td><?php echo $this->itemPhoto($item, 'thumb.icon');?></td>
        <td><?php echo $this->htmlLink($item->getHref(), $item->getTitle());?></td>
        <td><?php echo Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedTalentById($item->classified_id);?></td>
        <td><?php echo Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedLocationById($item->classified_id);?></td>
        <?php if($this->viewer->getIdentity() == $this->subject->getIdentity()){?>
        <td class="applicants">
        <?php
        $classified_id	=	$item->getIdentity();
        $table = Engine_Api::_()->getDbtable('applys', 'classified');	
		//$select = $table->select();
		$applyname = $table->info('name'); 
		
		$select = $table->select("$applyname.*")
				->where("$applyname.classified_id = $classified_id", 1);
				
		$result = $table->fetchAll($select);
        $text	=	($result->count()>0)? '('.$result->count().') applicants':'No Applicant Yet';
		$class	=	($result->count()>0)? 'yes-applicant':'no-applicant';
		$spanclass	=	($result->count()>0)? 'span-yes-applicant':'span-no-applicant';
         echo '<span class='.$spanclass.'></span>';
		 echo $this->htmlLink(array(
				'route' => 'classified_apply',
				'controller' => 'application',
				'action' => 'list',				
				'classified_id' => $item->getIdentity(),
			  ), $text, array(
              'class' => $class
            ));
			
			 ?></td>
        <?php } ?>
        <td><?php echo $this->timestamp($item->creation_date);
        //echo date('F d, Y H:i a', strtotime($item->creation_date));
        ?></td>
        <td><?php echo ($item->closed )?'Closed':'Open'?></td>
        <?php if($this->viewer->getIdentity() == $this->subject->getIdentity()){?>
        <td>
        
             
             <?php /*?><?php if( $this->allowed_upload ): ?>
              <?php echo $this->htmlLink(array(
                  'route' => 'classified_extended',
                  'controller' => 'photo',
                  'action' => 'upload',
                  'classified_id' => $item->getIdentity(),
                ), $this->translate('Add Photos'), array(
                  'class' => 'buttonlink icon_classified_photo_new'
              )) ?>
            <?php endif; ?><?php */?>
            <?php if( !$item->closed ): ?>
              <?php echo $this->htmlLink(array(
                'route' => 'classified_specific',
                'action' => 'close',
                'classified_id' => $item->getIdentity(),
                'closed' => 1,
                'format' => 'smoothbox'
              ), $this->translate('Hold'), array(
                'class' => 'buttonlink smoothbox icon_classified_close jp_hold'
              )) ?>
            <?php else: ?>
              <?php echo $this->htmlLink(array(
                'route' => 'classified_specific',
                'action' => 'close',
                'classified_id' => $item->getIdentity(),
                'closed' => 0,
                'format' => 'smoothbox'
              ), $this->translate('Open'), array(
                'class' => 'buttonlink smoothbox icon_classified_open jp_open'
              )) ?>
            <?php endif; ?>
             <?php echo $this->htmlLink(array(
             	'route' => 'default', 
                'module' => 'classified', 
                'controller' => 'index', 
                'action' => 'delete', 
                'classified_id' => $item->getIdentity(), 
                'format' => 'smoothbox'), 
                $this->translate('Delete'), array(
              'class' => 'buttonlink smoothbox icon_classified_delete jp_delete'
            )) ?>
			<?php echo $this->htmlLink(array(
              'route' => 'classified_specific',
              'action' => 'edit',
              'classified_id' => $item->getIdentity(),
            ), $this->translate('Edit'), array(
              'class' => 'buttonlink icon_classified_edit jp_edit'
            )) ?>
           </td>
         <?php } ?>
      </tr>
     <?php endforeach; ?>
  <?php endif; ?>
  </table>

<?php if($this->viewer->getIdentity() == $this->subject->getIdentity()){ ?>
<span class="job_post_span">
<a href="<?php echo $this->baseUrl().'/classifieds/create';?>" class="post_job">Post A New Job</a>
</span>
<?php }?>
<ul id="profile_classifieds" class="classifieds_profile_tab" style="display:none;">
  <?php foreach( $this->paginator as $item ): ?>
    <li>
      <div class='classifieds_profile_tab_photo'>
        <?php echo $this->htmlLink($item->getHref(), $this->itemPhoto($item, 'thumb.normal')) ?>
      </div>
      <div class='classifieds_profile_tab_info'>
        <div class='classifieds_profile_tab_title'>
          <?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?>
          <?php if( $item->closed ): ?>
            <img src='<?php echo $this->layout()->staticBaseUrl ?>application/modules/Classified/externals/images/close.png'/>
          <?php endif;?>
        </div>
        <div class='classifieds_browse_info_date'>
          <?php echo $this->timestamp(strtotime($item->creation_date)) ?>
        </div>
        <div class='classifieds_browse_info_blurb'>
          <?php echo $this->string()->truncate($this->string()->stripTags($item->body), 300) ?>
        </div>
      </div>
    </li>
  <?php endforeach; ?>
</ul>

<div>
  <div id="profile_classifieds_previous" class="paginator_previous">
    <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Previous'), array(
      'onclick' => '',
      'class' => 'buttonlink icon_previous'
    )); ?>
  </div>
  <div id="profile_classifieds_next" class="paginator_next">
    <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Next'), array(
      'onclick' => '',
      'class' => 'buttonlink_right icon_next'
    )); ?>
  </div>
</div>