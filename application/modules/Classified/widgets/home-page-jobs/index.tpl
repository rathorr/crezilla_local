<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */
?>
<style type="text/css">
.login_my_wrapper .layout_classified_home_page_jobs {
    border-radius: 5px;
    margin-bottom: 0;
}
.layout_classified_home_page_jobs {
    clear: both;
    margin-bottom: 15px;
    overflow: auto;
    width: 100%;
	margin-top:10px;
}
</style>
<script type="text/javascript">
  en4.core.runonce.add(function(){

    <?php if( !$this->renderOne ): ?>
    var anchor = $('profile_classifieds').getParent();
    $('profile_classifieds_previous').style.display = '<?php echo ( $this->paginator->getCurrentPageNumber() == 1 ? 'none' : '' ) ?>';
    $('profile_classifieds_next').style.display = '<?php echo ( $this->paginator->count() == $this->paginator->getCurrentPageNumber() ? 'none' : '' ) ?>';

    $('profile_classifieds_previous').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() - 1) ?>
        }
      }), {
        'element' : anchor
      })
    });

    $('profile_classifieds_next').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() + 1) ?>
        }
      }), {
        'element' : anchor
      })
    });
    <?php endif; ?>
  });
</script>




<div class="index-right-job-posted">
	    
    	<div class="col-md-7 col-sm-5 col-xs-6 index-right-text-lfet"> <span style="font-size: 15px; color: rgb(76, 76, 76); font-weight: 600;">Jobs Posted</span></div>
		<div class="col-md-5 col-sm-7 col-xs-6 classified_header text-right">
	    <a href="<?php echo $this->baseUrl().'/classifieds/create';?>" class="paj"><i class="fa fa-plus-circle" aria-hidden="true"></i> Post A Job</a></div>
	
    </div>
    

    
  <div class="col-md-12" style="padding-bottom:5px;">
  <table class="job_post_listing">
      <tr>
        <th class="job_titl" scope="col">Job Title</th>
        <th class="job_app" scope="col"># Applicants</th>
      </tr>
  <?php if( $this->paginator->getTotalItemCount() > 0 ){ ?>
      <?php foreach( $this->paginator as $item ): 
      ?>
      <tr>
        <td><?php echo $this->htmlLink($item->getHref(), ucfirst($item->getTitle()));?></td>
        <td class="applicants">
        <?php
        $classified_id	=	$item->getIdentity();
        $table = Engine_Api::_()->getDbtable('applys', 'classified');	
		//$select = $table->select();
		$applyname = $table->info('name'); 
		
		$select = $table->select("$applyname.*")
				->where("$applyname.classified_id = $classified_id", 1);
				
		$result = $table->fetchAll($select);
        $text	=	($result->count()>0)? $result->count():'0';
		 echo $this->htmlLink(array(
				'route' => 'classified_apply',
				'controller' => 'application',
				'action' => 'list',				
				'classified_id' => $item->getIdentity(),
			  ), $text);
			
			 ?></td>
      </tr>
     <?php endforeach; ?>
  <?php }else{
  echo '<tr><td colspan=2>You have not posted a job yet.</td></tr>'; 
  }?>
  </table>
  
    </div>
  
