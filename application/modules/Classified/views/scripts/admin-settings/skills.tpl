<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: categories.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */
?>

<h2><?php echo $this->translate("Classifieds Plugin") ?></h2>

<?php if( count($this->navigation) ): ?>
  <div class='tabs'>
    <?php
      // Render the menu
      //->setUlClass()
      echo $this->navigation()->menu()->setContainer($this->navigation)->render()
    ?>
  </div>
<?php endif; ?>

  <div class='clear'>
    <div class='settings'>
    <form class="global_form">
      <div>
      <?php echo $this->htmlLink(array('route' => 'admin_default', 'module' => 'classified', 'controller' => 'settings', 'action' => 'add-skill'), $this->translate('Add New Skill'), array(
          'class' => 'smoothbox buttonlink',
          'style' => 'float:right;background-image: url(' . $this->layout()->staticBaseUrl . 'application/modules/Core/externals/images/admin/new_category.png);')) ?>
        <h3><?php echo $this->translate("Skills") ?> </h3>
        <p class="description">
        
          <?php if(count($this->skills)>0):?>

         <table class='admin_table'>
          <thead>

            <tr>
              <th><?php echo $this->translate("Skill") ?></th>
              <th><?php echo $this->translate("Type") ?></th>
              <th><?php echo $this->translate("Options") ?></th>
            </tr>

          </thead>
          <tbody>
            <?php foreach ($this->skills as $skills): ?>
              <tr>
                <td><?php echo $skills->skill_name?></td>
                <td><?php echo ($skills->type==1)?'Casting':'Services';?></td>
                <td>
                  <?php echo $this->htmlLink(array('route' => 'admin_default', 'module' => 'classified', 'controller' => 'settings', 'action' => 'edit-skill', 'id' =>$skills->skill_id), $this->translate('edit'), array(
                    'class' => 'smoothbox',
                  )) ?>
                  |
                  <?php echo $this->htmlLink(array('route' => 'admin_default', 'module' => 'classified', 'controller' => 'settings', 'action' => 'delete-skill', 'id' =>$skills->skill_id), $this->translate('delete'), array(
                    'class' => 'smoothbox',
                  )) ?>

                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php else:?>
      <br/>
      <div class="tip">
      <span><?php echo $this->translate("There are currently no skills.") ?></span>
      </div>
      <?php endif;?>
        
    </div>
    </form>
    </div>
  </div>
     