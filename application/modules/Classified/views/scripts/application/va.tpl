<link rel="stylesheet" href="<?php echo $this->baseUrl().'/application/modules/Album/externals/styles/prettyPhoto.css';?>" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $this->baseUrl().'/application/modules/Album/externals/scripts/jquery.prettyPhoto.js'?>"></script>
<div class=" tab_977 generic_layout_container">
    <div class="applicant_fields">
	<div class="view_applicant_headline">
    <h2>View Applicant</h2>
	
    <a href="<?php echo $this->baseUrl().'/classifieds/application/list/classified_id/'.$this->classified_id;?>" class="add_more icon_classified_new back_button">Go Back</a>
	</div>
	<ul class="classifieds_browse">
		<div class='classifieds_browse_photo'>
				<?php
					$user = Engine_Api::_()->getItem('user', $this->user_id);
					echo $this->itemPhoto(Engine_Api::_()->user()->getUser($this->user_id),'thumb.profile');
				?>
		</div>
		<div class='classifieds_browse_info_c'>
			<div class='classifieds_browse_info_title'>
				<h3>
					<?php 
						$user = Engine_Api::_()->getItem('user', $this->user_id);
						echo $user->displayname;
					?>
				</h3>
				
			</div>
			<div class='classifieds_browse_info_category va_gender'>
				  Gender: <?php echo $this->fieldStructure['Gender']; ?>
			</div>
			<!--<div class='classifieds_browse_info_category va_location'>
				  Current: <?php //echo '<pre>';print_r($this->fieldStructure);echo '</pre>';
					echo $this->fieldStructure['Location']; ?>
			</div>-->
			<div class='classifieds_browse_info_category va_email'>
				  Email: <?php echo $user->email; ?>
			</div>		
			<!--<div class='classifieds_browse_info_category va_phone'>
				  Contact: <?php  echo $this->fieldStructure['Phone Number']; ?>
			</div>-->
            
            <div class='classifieds_browse_info_category va_about'>
				  <span style="display:block;">About me</span> <p><?php  echo $this->fieldStructure['About Me']; ?></p>
			</div>
			<div class='classifieds_browse_info_category va_phone va_portfolio' style="display:none;">
				   <span style="display:block;">Portfolio</span>
                  <div class="portfolio_applicant_va">
                  <?php
                  foreach ($this->portfolio as $key => $val)
                  {
                  ?>
                      <?php if($val['post_type']==1)
                      {
                      ?>
                      <a href="<?php echo $this->baseUrl();?>/classifieds/application/audio/id/<?php echo $val['file_name'];?>" class="smoothbox"><img src="<?php echo $this->baseUrl();?>/public/audio&video.png"  style="width:140px; height:140px;" /></a>
                      <?php
                      }
                      else
                      {
                      ?>
                      <a href="<?php echo $this->baseUrl();?>/classifieds/application/video/id/<?php echo $val['file_name'];?>" class="smoothbox"><img src="<?php echo $this->baseUrl();?>/public/video_gallery.png"  style="width:140px; height:140px;" /></a>
                      <?php
                      }
                      ?>
                  <?php
                  }
                  ?>
                  </div>
			</div>
		</div>
		<div class='classifieds_browse_actions'>
			<div class='classifieds_browse_info_category'>
				<a href="<?php echo $this->baseUrl();?>/messages/compose/to/<?php echo $this->user_id;?>/format/smoothbox" class="buttonlink smoothbox va_message" target="">Message</a> 
            
                <?php
                $result = $this->result[0];	
                if($result->accept == 1){
                echo "<div class='va_accepted'>Selected</div>";
                }else{
                echo "<div val = '".$this->classified_id."' title='".$this->user_id."' id = 'app_accept' style='cursor:pointer;' class='nt_accepted'>Select</div>";
                }
                
                if($result->reject == 1){
                echo "<div class='va_rejected'>Rejected</div>";
                }else{
                echo "<div val = '".$this->classified_id."' title='".$this->user_id."' id = 'app_reject' style='cursor:pointer;' class='nt_rejected'>Reject</div>";
                }
                if($result->hold == 1){
                echo "<div class='on_hold'>Shortlisted</div>";
                }else{
                echo "<div val = '".$this->classified_id."' title='".$this->user_id."' id = 'app_hold' style='cursor:pointer;' class='nt_hold'>Shortlist</div>";
                }
			?>
            <div style='cursor:pointer;' class='view_profile'><a class="va_profile_view" href="<?php echo $user->getHref();?>" target="_blank">View Profile</a></div>
                <?php	  
					//echo "<div val = '".$this->classified_id."' id = 'app_accept' style='cursor:pointer;'>Accept</div>";
					//echo "<div val = '".$this->classified_id."' id = 'app_reject' style='cursor:pointer;'>Reject</div>";
					//echo "<div val = '".$this->classified_id."' id = 'app_hold' style='cursor:pointer;'>Hold</div>";
				?>
			</div>
		</div>
		
		</ul>
	
	
		<div class="clear"></div>
		
		<div class="va_media_section" style="display:none;">
			<div class="audio_video">
				<div class='classifieds_browse_info_video'>
					 
					  <?php  
						 Engine_Api::_()->core()->setSubject($this->subject);   
						 //echo $this->content()->renderWidget('video.profile-videos'); 
						 ?> 
                  <ul id="profile_videos" class="profile_videos_<?php echo $uid ?> " >
                              <?php $i=1;
                                if($this->video_paginator){
                                 foreach( $this->video_paginator as $item ): 
                                 ?>
                              <script type="text/javascript" charset="utf-8">
                            jQuery(document).ready(function(){
                                var i	=	'<?php echo $i;?>';
                                jQuery("area[rel^='prettyPhoto']").prettyPhoto();
                                jQuery(".video"+i+":first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: false});
                            });
                                </script>
                                <li class="video<?php echo $i;?>" style="float: left;margin-right: 20px;max-width: 300px;overflow: hidden;">
                                  <div class="video_thumb_wrapper">
                                    <?php if ($item->duration):?>
                                   <!-- <span class="video_length" style="display:none;">
                                      <?php
                                        if( $item->duration>360 ) $duration = gmdate("H:i:s", $item->duration); else $duration = gmdate("i:s", $item->duration);
                                        if ($duration[0] =='0') $duration = substr($duration,1); echo $duration;
                                      ?>
                                    </span>-->
                                    <?php endif;?>
                                    <?php
                                        $url	=	'';
                                        if($item->type == 2){
                                            $url = 'http://vimeo.com/'.$item->code;
                                        }
                                        if($item->type == 1){
                                            $url = 'http://www.youtube.com/watch?v=/'.$item->code;
                                        }
                                      if( $item->photo_id ) {
                                      echo $this->htmlLink($url, $this->itemPhoto($item, 'thumb.main', '', array('class'=>'test')), array('class'=>'thumbs_photo videobgplay', 'rel'=>'prettyPhoto[gallery1]'));
                                      } else {
                                        echo '<img alt="" src="' . $this->layout()->staticBaseUrl . 'application/modules/Video/externals/images/video.png">';
                                      }
                                    ?>
                                    <span class="video_name">
                                     <?php echo $this->string()->chunk($this->string()->truncate($item->getTitle(), 45), 10); ?>
                                    </span>
                                  </div>
                                  <?php /*?><a class="video_title" href='<?php echo $item->getHref();?>'><?php echo $item->getTitle();?></a>
                                  <div class="video_author"><?php echo $this->translate('By');?> <?php echo $this->htmlLink($item->getOwner()->getHref(), $item->getOwner()->getTitle()) ?></div>
                                  <div class="video_stats">
                                    <span class="video_views"><?php echo $item->view_count;?> <?php echo $this->translate('views');?></span>
                                    <?php if($item->rating>0):?>
                                      <?php for($x=1; $x<=$item->rating; $x++): ?><span class="rating_star_generic rating_star"></span><?php endfor; ?><?php if((round($item->rating)-$item->rating)>0):?><span class="rating_star_generic rating_star_half"></span><?php endif; ?>
                                    <?php endif; ?>
                                  </div><?php */?>
                                </li>
                              <?php $i++; 
                               endforeach;
                              } ?>
                            </ul> 
				</div>
				<div class='classifieds_browse_info_album'>
					
					  <?php  
						 //echo $this->content()->renderWidget('album.profile-albums'); 
						 ?>
                     <?php $i=0;
                          if($this->album_paginator){
                            foreach( $this->album_paginator as $album ):  ?>
                          <script type="text/javascript" charset="utf-8">
                        jQuery(document).ready(function(){
                            var i	=	'<?php echo $i;?>';
                            jQuery("area[rel^='prettyPhoto']").prettyPhoto();
                            jQuery(".gallery"+i+":first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: false});
                        });
                            </script>
                          <ul id="profile_albums" class="thumbs gallery<?php echo $i;?>">
                            <li>
                              <a class="thumbs_photo img_hov_grey_active" href="<?php echo $album->getPhotoUrl('thumb.main'); ?>" rel="prettyPhoto[gallery1]">
                              
                               <span class="img_hov_grey1">
                               <p class="thumbs_info">
                                <span class="thumbs_title">
                                  <?php echo $this->string()->chunk($this->string()->truncate($album->getTitle(), 45), 10); ?>
                                </span>
                                <span class="photo_count">
                                <?php echo $this->translate(array('%s photo', '%s photos', $album->count()),$this->locale()->toNumber($album->count())) ?>
                                </span>
                              </p>
                               </span>
                               
                              <img src="<?php echo $album->getPhotoUrl('thumb.main'); ?>" />
                               
                              </a>
                              <?php 
                              $data=Engine_Api::_()->getDbtable('photos', 'album')->getAlbumPhotos($album->album_id, $this->subject->getIdentity());
                               if($data){
                                foreach($data as $img){
                                    if($img['photo_id'] != $album->photo_id){
                                        echo '</li><li style="display:none;">';?>
                                        <a class="thumbs_photo" href="<?php echo $this->baseUrl().'/'.$img['storage_path']; ?>" rel="prettyPhoto[gallery1]">
                                            <img src="<?php echo $this->baseUrl().'/'.$img['storage_path']; ?>" style="width:160px !important; height:180px !important;"/>
                                       
                                      </a>
                                    
                                <?php }
                                 }
                               }
                             ?>
                             
                            </li>
                             
                            </ul>
                          <?php $i++;
                           endforeach;
                          }?>    
                           
				</div>
			</div>
		</div>	
	
    </div>
	
</div>


<script type="application/javascript">
	jQuery(document).on('click', '#app_accept',function(){
		var classified_id = jQuery(this).attr('val');
		var user_id = jQuery(this).attr('title');
		jQuery('.loading_image').show();
		formURL = "classifieds/application/accept/classified_id/" + classified_id;
			jQuery.ajax({
					url: formURL,
					type: "POST",
					data: "cid=" + classified_id+"&user_id="+user_id,
					dataType: 'json',
					success: function(resp) {
							alert(resp);
							jQuery('.loading_image').hide();
							location.reload();
					}
					
			})
	});

	jQuery(document).on('click', '#app_reject',function(){
		var classified_id = jQuery(this).attr('val');
		var user_id = jQuery(this).attr('title');
		jQuery('.loading_image').show();
		formURL = "classifieds/application/reject/classified_id/" + classified_id;
			jQuery.ajax({
					url: formURL,
					data: "cid=" + classified_id+"&user_id="+user_id,
					type: "POST",
					dataType: 'json',
					success: function(resp) {
							alert(resp);
							jQuery('.loading_image').hide();
							location.reload();
					}
					
			})
	});

	jQuery(document).on('click', '#app_hold',function(){
		var classified_id = jQuery(this).attr('val');
		var user_id = jQuery(this).attr('title');
		jQuery('.loading_image').show();
		formURL = "classifieds/application/hold/classified_id/" + classified_id;
			jQuery.ajax({
					url: formURL,
					data: "cid=" + classified_id+"&user_id="+user_id,
					type: "POST",
					dataType: 'json',
					success: function(resp) {
							alert(resp);
							jQuery('.loading_image').hide();
							location.reload();
					}
					
			})
	});
</script>
