<style type="text/css">
body{
  overflow-x:hidden!important; 
  font-family: 'Lato';
  font-size: 13px;
}
._banner_section{
  background: url('img/banner-img.jpg') no-repeat center center;
  background-size: cover;
  min-height: 200px;  
  position: relative;
  margin-bottom: 36px;
  padding: 0px;
} 
._form_area{
  position: absolute;
  height: 40px;
  top: 50%;
  width: 100%;
  margin:-20px 0 0;
}
.browsemembers_criteria ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
/*.browsemembers_criteria ul li{
  display: inline-block;  
  min-width: 200px;
  max-width: 200px;
  margin-right: 28px;
  margin-bottom: 11px;
}*/
._job_results,
._community_ads {
  padding: 0 15px!important;
}
._job_results_count {
    font-size: 18px;
    text-transform: uppercase;
}
._job_results_tab {
    text-align: right;
    font-size: 14px;
}
._job_results_tab ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
._job_results_tab ul li{
  display: inline-block;
}
._job_results_tab ul li a{
  padding: 8px 8px;
  text-decoration: none;
  color: #000;
  border-bottom: 2px solid transparent;
}
._job_results_tab ul li.active a{
  border-color:#f55731; 
}

._job_results_head{
  float:left;
  width: 100%;
  padding-bottom: 8px;
  border-bottom: 1px solid #e1e1e1;
  margin-bottom: 30px;
}
/*._job_results_list_item {
    background: #fff;
    padding: 10px 5px;
}*/

._job_list_item_block {
    background: #fff;
    padding: 10px 5px;
    float: left;
    width: 100%;
    margin-bottom: 30px;
}
._item_block_head{
  float: left;
  width: 100%;
  padding-bottom: 10px;
  padding-left: 10px;
}
._createdby,
.viewedby {
    font-size: 14px;
    color: #888888;
}
._createdby_name{
  color: #000000;
}
._createdby{
  float:left;
}
.viewedby{
  float: right;
  padding-right: 15px;
}
._item_block_content{
  float: left;
  width: 100%;
}
._job_item_img {
    float: left;
    height: 105px;
    width: 105px;
} 
._job_item_content{
  width: calc(100% - 105px);
    float: right;
    padding: 0 9px;
    max-height: 170px;
}
._job_title {
    padding-bottom: 10px;
}
._job_title a{
   color: #333;
    font-size: 18px;
 }
 ._job_details {
    font-size: 14px;
    color: #333;
    padding-bottom: 10px;
}
 ._job_details i {
  color: #cacaca;
  padding-right:3px;
 } 
 span._job_loc {
    padding-left: 25px;
}
._job_desc{
  font-size: 14px;
  color: #333;
}
._job_desc_title {
    font-size: 12px;
    color: #888;
}
._item_block_footer {
    float: left;
    width: 100%;
    padding-left: 10px;
}
._addfolderr_icn{
  width: 18px;
  height: 18px;
  display: inline-block;
  background: url('public/jobs/add_folder_icn.png') no-repeat center center;
  background-size: cover;
  margin-right: 10px;
}
._item_block_footer a {
    float: left;
    width: 18px;
    margin-right: 10px;
    color: #cacaca;
    font-size: 18px;
}
._community_ads_area{
  background: #ffffff;
  border:1px solid #e6e7e8;
}
._community_ads_head {
    padding: 8px 0 3px;
}
._community_ads_head a{
  color: #666666;
  font-size: 14px;
  line-height: 1.4;
}
._community_ads_head a._createads{
  float: right;
  color: #f55731;
}
._community_ads_img{
  margin-bottom: 8px;
}
._community_ads_img img{
  width: 100%;
}
._community_ads_desc{
  font-size: 14px;
  color: #666666;
}
._community_ads_desc span{
  float: left;
  width: 100%;
}
._clear{clear:both;}
._community_ads_desc a{
  color: #666;
}
._community_ads_desc a._community_ads_title{
  color: #000;
  font-weight: bold;
}
._community_ads_items {
    border-bottom: 1px solid #e6e7e8;
    padding-top: 10px;
}
._community_ads_items:last-child{
    border-bottom: none;
    padding-bottom: 0px;
}
#job_result > li > div > div > div._item_block_content > div._job_item_img > img
{
  max-height: 105px;
  max-width: 105px;
}
#job_results > div > div > div > div > div._item_block_content > div._job_item_img > img
{
  max-height: 105px;
  max-width: 105px;

}
#job_results > div > div > div > div > div._item_block_content > div._job_item_content > div._job_desc > p
{
  font-size: 14px !important;
  }
  #job_result > li > div > div > div._item_block_content > div._job_item_content > div._job_desc > p
  {
    font-size: 14px !important;
    max-height: 95px;
  }
/*.login_my_wrapper #global_content .layout_main .layout_right {
    width: 25%;
}
body#global_page_classified-job-search .login_my_wrapper #global_content .layout_main .layout_middle {
    width: 75% !important;
}*/

#job_results > form > div.browsemembers_criteria2
{
  padding: 10px;
  background-color: #E9EAED;
  text-align: right;
  border-bottom: 1px solid #ccc;
  margin-bottom: 3%;
}
  #job_results > form > div > ul > li
  {
    display: inline;
    margin-top: 0;
    width: 10% !important;
    padding: 10px;
    margin-right: 10px;
    color: #000;
  }
 #job_results > form > div > ul > li > label > a
 {
  color: #000;
 }
 .jobselect
 {
  color: #fb9236 !important;
  border-bottom: 2px solid;
  border-color: #f55731 !important;
 }
 .browsemembers_criteria
 {
  background: url("public/jobs/banner-img.jpg") no-repeat center center;
    background-size: cover;
    min-height: 200px;
    position: relative;
    /*margin-bottom: 36px;*/
    padding: 0px;
 }
 #global_content > div > div > div.generic_layout_container.layout_user_browse_search > form > div > ul
 {
    padding-top: 6%;
    padding-left: 8%;
 }
 
  #masthead
  {
    margin-top: 8%;
  }
  div.modal-dialog
  {
        padding-top: 5%;
  }
  .modal-footer_des
  {
    background-color: #e6f8ff !important;
    border: 1px solid #98d7f1 !important;
    overflow: hidden;
    margin: 10px 14px !important;
    padding: 10px !important;
  }
  div.modal-header > p
  {
    font-size: 14px;
    color: #777;
    margin-top: 0px;
  }
div.modal-header > h3
{
  font-size: 18px !important;
  color: #5a5a5a !important;
  margin-top: 10px !important;
  margin-bottom: 0px !important;
}
div.modal-header
{
     /*top-right-bottom-left*/
          padding: 0px 0px 0px 10px !important;
          border-bottom: none !important;
}
div.modal-content
{
  display: block !important;
    padding: 0% !important;
    overflow-y: auto;
    margin: 0px;
    padding-top: 15%;
}
div.modal-body
{
     padding: 0px 0px 0px 10px !important;
         margin-top: -15px !important;
}
div.modal-footer
{
  padding: 10px 0px 0px 10px !important;
    text-align: left !important;
    border-top: none !important;
}
div.modal-footer > button:nth-child(1)
{
  background-color: #eb8831 !important;
  border-radius: 5px !important;
  color: #FFFFFF!important;
    padding: 10px 15px !important;
    font-size: 12px !important;
}
div.modal-footer > button:nth-child(2)
{
  background-color: #5a5a5a;
    border-radius: 5px !important;
    color: #fff;
    padding: 10px 12px !important;
    text-transform: capitalize;
    font-size: 12px !important;
}
#popup_box { 
    display:none;
    position:fixed; 
    height:100px;  
    width:300px;  
    background: #fff;  
    left: 20%;
    top: 50%;
    /*margin-left: -100px;
    margin-top: -150px;*/
    z-index:100;     
    padding:15px;  
    font-size:12px;  
    -moz-box-shadow: 0 0 5px;
    -webkit-box-shadow: 0 0 5px;
    box-shadow: 0 0 5px;
    background-color: #dbdbdb;
}
#overlay {
    background:rgba(0,0,0,0.3);
    display:none;
    width:100%; height:100%;
    position:absolute; top:0; left:0; z-index:99998;
}
._applydate
{
  color: #000;
}
/*#job_result > li > div._job_results_list_item
{
  max-height: 300px;
  border-bottom: 1px solid #e8e8e8;
}*/
@media(max-width: 767px){
  #job_result > li > div > div > div._item_block_content > div._job_item_content > div._job_desc > p {
    max-height: none;
          }
    ._job_item_content {
      max-height: none;
    }
  }
</style>

<div class="layout_page_classified_job_search">
<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_user_browse_search">
<form method="post" action="<?php echo $this->baseUrl().'/job/getjobresults';?>" class="field_search_criteria ajaxform">
<input type="hidden" name="type" value="search"  id="type"/>
<div class="browsemembers_criteria"><ul>
<li>
<select name="designation_id" id="designation_id">
    <?php 
        if($this->dasignations){
            echo '<option value="">Designation</option>';
            foreach($this->dasignations as $desig){
                echo '<option value="'.$desig['dasignatedsubcategory_id'].'">'.$desig['dasignatedsubcategory_name'].'</option>';
            }
        }
    ?>
    </select>
 </li>

 <li class="job_location">
<input type="text" name="location" value=""  id="location"  placeholder="Location"/>
<div id="autoSuggestionsListl" autocomplete="off"></div>
</li>

<li >
<select name="language_id" id="language_id">
        <?php 
            if($this->langoptions){
                echo '<option value="">Language</option>';
                foreach($this->langoptions as $lang){
                    echo '<option value="'.$lang['option_id'].'">'.$lang['label'].'</option>';
                }
            }
        ?>
        </select>
 </li>
 <li>
<input type="text" value="" id="keyword" name="keyword" placeholder="Keyword">
</li>
 <li class="search_button">
<input type="button" name="serach" value="Search"  id="search"/>
</li>

 <li class="job_advance_search" style="display:none;">
<input type="button" name="serach" value=""  id="ad_search"/>
</li>

<input type="hidden" name="list_count" id="list_count" value="20"/>
</ul>
</div>
</form>
</div>


<div class="generic_layout_container layout_middle" style="float:left;">
<div class="generic_layout_container layout_core_content">

<div id="job_results" class="browsemembers_results">

	<?php //echo "Total jobs". count($this->jobs); 
  
  if($this->jobs){ //$tj=count($this->jobs);?>
  
<form method="post" action="<?php echo $this->baseUrl().'/job/getjobresults';?>" class="field_search_criteria ajaxform2">

<div class="browsemembers_criteria2">
<ul>


 <li class="job_total_count">
 <label name="jobcount" style="font-size: 14px; float: left;" id="jobcount">Total <span id="job_count"><?php echo count($this->ttjobs);?></span> Jobs</label>
</li>
<li class="job_rele_count">
<label name="jobrele" style="font-size: 14px;" id="jobtrele"><a href="javascript:void(0);" name="relevant" id="relevant" title="Jobs related to you">Relevant</a>  </label>

</li>
<li class="job_trend_count">
<label name="jobtrend" style="font-size: 14px;" id="jobtrend"><a href="javascript:void(0);" name="trending" id="trending" title="Most viewed jobs">Trending</a>  </label>
</li>

<li class="job_latest_count">
<label name="joblatest" style="font-size: 14px;" id="joblatest"><a href="javascript:void(0);" name="latest" id="latest" title="Lates jobs">Latest</a>  </label>
</li>
<input type="hidden" name="list_count" id="list_count" value="20"/>
<input type="hidden" name="job_search_status" id="job_search_status" value=""/>
</ul>
</div>
</form>
<div class="_job_results_list">
    <div class="row">

  <?php  	  echo '<ul class="job_start row_start" id="job_result">';
      	//echo '<pre>'; print_r($this->jobs); echo '</pre>';
    	foreach($this->jobs as $job){
       $data  = Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedDataById($job['classified_id']);
       $applyby= Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedsapplyBy($job['classified_id']);
                    // echo '<pre>'; print_r($data);die();
                     foreach($data as $res){
                  
                    if($res['field_id'] == 61){
                      $desig=$res['values'];
                    }
                    if($res['field_id'] == 74){
                      $expe1=$res['values'];
                      if($expe1=='')
                      {
                        $expe="0";
                      }
                      else
                      {
                        $expe= $expe1;
                      }
                    }
                    if($res['field_id'] == 73){                    
                      $lang=$res['values'];
                    }
                
                    if($res['field_id'] == 75){
                      $keyw=$res['values'];
                    }

                    if($res['field_id'] == 41){
                      $dura=$res['values'];
                    }
                    
                    if($res['field_id'] == 71){
                      $pay_term=$res['values'];
                    }
                                 
                    if($res['field_id'] == 14){
                      $work_loc=$res['values'];
                    }

                    if($res['field_id'] == 72){
                      $out_app=$res['values'];
                      if($out_app=='Yes')
                      {
                        $out_app="may";
                      }
                      else
                      {
                        $out_app= "need not";
                      }
                    }
                                                          
                }

                      $ownername='';
                       $jobownerpic = '';
                        $classified = Engine_Api::_()->getItem('classified', $job['classified_id']);
                        //echo '<pre>'; print_r($classified); 
                        if($job['page_id'] != 0){
                            $companypage  = Engine_Api::_()->getItem('page', $job['page_id']);
                            //echo '<pre>'; print_r($companypage);
                            $ownername=$companypage['displayname'];
                            $jobownerpic =Engine_Api::_()->getDbtable('users', 'user')->getUserJobDesigPicPath($desig);
                            $link = $companypage->getHref();
                            $jobownerpic  = ($jobownerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$jobownerpic;
                            $isApply  = Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($job['classified_id'], $this->userid); 

                        }else{
                           $user = Engine_Api::_()->getItem('user', $job['owner_id']);
                           //echo '<pre>'; print_r($user); 
                           $ownername=$user['displayname'];
                           $link =  $user->getHref();
                           $jobownerpic =Engine_Api::_()->getDbtable('users', 'user')->getUserJobDesigPicPath($desig); 
                           $jobownerpic = ($jobownerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$jobownerpic;
                          $isApply  = Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($job['classified_id'], $this->userid);

                        }
                    //die();
      ?>
        	
     <li>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 _job_results_list_item">
            <div class="_job_list_item_block">
              <div class="_item_block_head">
                <div class="_createdby left">by <span class="_createdby_name"><?php echo $ownername; ?>,</span> <span class="_dated"><?php //echo $this->timestamp($classified['creation_date']); 
                echo date('d F Y',strtotime($classified['creation_date'])); ?></span></div>
                <div class="viewedby right"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo $classified['view_count'];?></div> 
              </div>
              <div class="_item_block_content">
                <div class="_job_item_img"><img src="<?php echo $jobownerpic;?>"></div>
                <div class="_job_item_content">
                  <div class="_job_title"><a href="<?php echo $classified->getHref();?>" title=""><?php echo $job['title']; ?></a></div>
                  <div class="_job_details">
                    <span class="_job_exp"><i class="fa fa-briefcase" aria-hidden="true"></i> <?php echo $expe ?></span>
                    <span class="_job_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $work_loc ?></span>
                  </div>
                  <div class="_job_desc">
                    <div class="_job_desc_title">Job Description</div>
                    <b><?php echo $desig ?></b> wanted, with <b><?php echo $expe ?></b> of experience, fluent in <b><?php echo $lang ?></b> for a <b><?php echo $dura ?></b> assignment. Payment terms will be on a <b><?php echo $pay_term ?></b> basis. This position is primarily based in <b><?php echo $work_loc ?></b>.  Outstation candidates <b><?php echo $out_app ?></b> apply.</p>
                  </div>
                  <?php if($applyby){?>
                  <div class="_job_desc">
                    <div class="_job_desc_title">Apply By: <span class="_applydate"><?php echo date('d F Y',strtotime($applyby))?></span></div>
                    
                  </div>
                  <?php }?>
                </div>
              </div>
                    <div class="container">
              <div class="_item_block_footer">
                <?php if( $this->userid == $job['owner_id']) {?>
            <a class="add_directory" title="Can't Apply"><i class="fa fa-ban"></i></a>
           <?php } else { if ($isApply) { ?>
                <a id="unapply_<?php echo $job['classified_id'];?>" class="smooth_box add_directory" title="Un Apply" data-index="<?php echo $job['classified_id'];?>"><i class="fa fa-remove"></i></a>
                <?php } else {?>
                <a id="apply_<?php echo $job['classified_id'];?>"  class="smooth_box add_directory" title="Apply" data-index="<?php echo $job['classified_id'];?>"><i class="fa fa-check-square"></i></a>
                <?php }} ?>
                <a data-toggle="modal" data-target="#myModal_<?php echo $job['classified_id'];?>"><i class="fa fa-share-alt" aria-hidden="true" class="_shared_list_item"></i></a>  
          </div>
                
  <!-- Trigger the modal with a button -->
   <!-- Modal -->
  <div class="modal fade" id="myModal_<?php echo $job['classified_id'];?>" role="dialog">
    <div class="modal-dialog modal-sm" >
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h3>Share</h3>
          <p class="form-description">Share this by re-posting it with your own message.</p>
        </div>
        <div class="modal-body">
          <div class="form-elements">
                  <div id="body-wrapper" class="form-wrapper">
                  <div id="body-label" class="form-label">&nbsp;</div>
                  <div id="body-element" class="form-element">
                  <textarea name="body" id="share_textarea_<?php echo $job['classified_id'];?>" cols="40" rows="6" maxlength="300"></textarea>
                  </div>
                  </div>
                  </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="jobshare_<?php echo $job['classified_id'];?>" value="<?php echo $job['classified_id'];?>" data-dismiss="modal">Share</button>
          Or <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        <div class="modal-footer_des">
          <div class="sharebox_title">
              <?php echo $job['title']; ?>
            </div>
            <div class="sharebox_description">
              <?php echo $job['body']; ?>
            </div>
        </div>
      </div>
      
    </div>
  </div>
  
              </div>            
            </div>            
          </div>
          
        
              </li>
        <?php }
        
        echo '</ul>';
    }?>
    </div>
      </div>

      <div id="popup_box">    <!-- OUR PopupBox DIV--> 
      <p></p>  
      </div>
      <div id="overlay" style="display: none;" ></div>
</div>
<img style="position: fixed; top: 75%; display: none; width: 57px; left: 45%;" class="process_image" src="<?php echo $this->baseUrl();?>/public/custom/images/loading.gif">
<div id="view" style="display:none;"></div>
</div>
</div>
<div class="generic_layout_container layout_right">
<?php echo $this->content()->renderWidget('communityad.ads') ?>
</div>
</div>
</div>

<script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- <script src="<?php echo $this->baseUrl().'/public/js/jquery.form.js';?>"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>

<script>
    var jQuery = jQuery.noConflict();
</script>
<script type="text/javascript">
jQuery(document).on('keypress', '#keyword, #location', function(e){
	 if(e.which == 13 || e.which == 1 ) {
       jQuery('#list_count').val(0);
		searchjobs();
   }
});

if(typeof(jQuery('.third_ad').offset()) !='undefined'){
var topv = jQuery('.third_ad').offset().top;
 jQuery(document).scroll(function() {
      var windscroll = jQuery(document).scrollTop();
      if (windscroll > topv) {
			jQuery('.create_div').show();
            jQuery(".third_ad").addClass("fxd_header");
           jQuery(".third_ad").removeClass("absolute_header");
    }else{
		jQuery('.create_div').hide();
        jQuery(".third_ad").removeClass("fxd_header");
        jQuery(".third_ad").addClass("absolute_header");
    }
    
 });
 }

jQuery(document).ready(function(){
	jQuery(window).scroll(bindScroll);
  jQuery("#relevant").addClass('jobselect');
});

function bindScroll(){
   if(jQuery(window).scrollTop() + jQuery(window).height() > jQuery(document).height() - 900) {
       jQuery(window).unbind('scroll');
        if(jQuery('#list_count').val() != 0){
		jQuery('#type').val('');
		 loadmoreresults();	
	   }
   }
}
jQuery(document).on('click', '#search', function(){
	jQuery('#list_count').val(0);
	searchjobs();
});
jQuery(document).on('click', '#relevant', function(){
  jQuery('#trending').removeClass("jobselect");
  jQuery('#latest').removeClass("jobselect");
  jQuery(this).addClass("jobselect");
  jQuery('#job_search_status').val(jQuery("#relevant").text());
  //alert("click");
  searchjobs2();
});
jQuery(document).on('click', '#trending', function(){
  jQuery('#relevant').removeClass("jobselect");
  jQuery('#latest').removeClass("jobselect");
  jQuery(this).addClass("jobselect");
  jQuery('#job_search_status').val(jQuery("#trending").text());
  //alert("click");
  searchjobs2();
});
jQuery(document).on('click', '#latest', function(){
  jQuery('#trending').removeClass("jobselect");
  jQuery('#relevant').removeClass("jobselect");
  jQuery(this).addClass("jobselect");
  //alert("click");
  jQuery('#job_search_status').val(jQuery("#latest").text());
  searchjobs2();
});

function searchjobs(){
jQuery('.loading_image').css("display", "block");
//jQuery('#list_count').val(20);
jQuery(".ajaxform").ajaxForm({
			/* target:'#view',*/
			success: function(data){
				jQuery('.loading_image').css("display", "none");
				var response	=	jQuery.trim(data).split('**');
				if((jQuery.trim(response[0]) =='<li>No Jobs Found.</li>')){
					jQuery('#job_result').html('<li style="list-style: outside none none; text-align: center;border-bottom: medium none !important;min-height: 20px !important;">No Jobs Found</li>');
				}else{
					jQuery('#job_result').html(jQuery.trim(response[0]));
					jQuery('#list_count').val(jQuery.trim(response[1]));
				}
				return false;
			}
		}).submit();
}
function searchjobs2(){
jQuery('.loading_image').css("display", "block");
//jQuery('#list_count').val(20);
jQuery(".ajaxform2").ajaxForm({
      /* target:'#view',*/
      success: function(data){
        jQuery('.loading_image').css("display", "none");
        var response  = jQuery.trim(data).split('**');   
        if((jQuery.trim(response[0]) =='<li>No Jobs Found.</li>')){
          jQuery('#job_result').html('<li style="list-style: outside none none; text-align: center;border-bottom: medium none !important;min-height: 20px !important;">No Jobs Found</li>');
        }else{
          
          console.log("count"+ jQuery.trim(response[1]).length);
          //console.log("count2"+ jQuery.trim(response[0]).size());
          jQuery('#job_result').html(jQuery.trim(response[0]));
          jQuery('#list_count').val(jQuery.trim(response[1]));
        }
        return false;
      }
    }).submit();
}

function loadmoreresults() {
	jQuery('.process_image').css("display", "block");
	jQuery('.process_image3').show();
	jQuery(".ajaxform").ajaxForm({
			/* target:'#view',*/
			success: function(data){
				jQuery('.process_image').css("display", "none");
				jQuery('.process_image3').hide();
				var response	=	jQuery.trim(data).split('**');
				
				if(jQuery.trim(response[0]) == ''||(jQuery.trim(response[0]) =='<li>No Jobs Found.</li>')){
					jQuery('#job_result').append('<li style="list-style: outside none none; text-align: center;min-height: 20px !important;">No More Jobs</li>');
					jQuery('#list_count').val(0);
				}else{
					jQuery('#job_result').append(jQuery.trim(response[0]));
					jQuery('#list_count').val(response[1]);
				}
				jQuery(window).bind('scroll', bindScroll);
				return false;
			}
		}).submit();
}
jQuery(document).on('keyup','#location', function(e) {
         var inputString = jQuery(this).val();
        if(inputString.length < 1) {
                jQuery('#autoSuggestionsListl').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/user/edit/getlocations";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                      if(data)
                        {
                            jQuery('#autoSuggestionsListl').show();
                            jQuery('#autoSuggestionsListl').addClass('auto_listl');
                            jQuery('#autoSuggestionsListl').html(data);
                            jQuery("#autoSuggestionsListl").attr('style','height: auto; overflow-y: auto; max-height: 400px; width: inherit;');
                        }
                        else
                        {
                            jQuery('#autoSuggestionsListl').hide();
                        }
                     });
             }
    });
      jQuery(document).on('click','#autoSuggestionsListl li',function(){ 
            var el=jQuery(this).text();
           jQuery('#location').val(el);
        setTimeout("jQuery('#autoSuggestionsListl').hide();", 200);
       });

      /* share or aplly */
      jQuery(document).on('click','button', function(e) {
        jQuery('.process_image').css("display", "block");
        jQuery('.process_image').show();
         var classifiedid = jQuery(this).val();
         var body=jQuery("#share_textarea_"+classifiedid).val();         
         console.log('classifiedid' +classifiedid);
        // alert('classifiedid'+classifiedid);
      //return false;
        if(classifiedid != ''){
               jQuery.post("activity/index/share/type/classified/id/"+classifiedid+"/sharetyp/searchshare/body/"+body, {classifiedid: ""+classifiedid+""}, function(data) { // Do an AJAX call
                  jQuery('.process_image').css("display", "none");
                  jQuery('.process_image').hide();
                      console.log(data);
                      jQuery("#popup_box > p").text(data);
                      jQuery('#popup_box').fadeIn("slow");
                      //jQuery('#overlay').fadeIn();
                      setTimeout(function(){jQuery('#popup_box').fadeOut(300);}, 2000);
                     });
             }else{ jQuery('.process_image').css("display", "none");
        jQuery('.process_image').hide();}
    });

      jQuery(document).on('click','a.smooth_box', function(e) {
        jQuery('.process_image').css("display", "block");
        jQuery('.process_image').show();
         var classifiedid = jQuery(this).attr("data-index"); //(this).data("index"); 
         var type = jQuery(this).attr("title");
         var url='';
         if(type == "Apply")
         {
           url="classifieds/application/applysave/classified_id/"+classifiedid+"/apply/searchapply";

         }
         if(type== "Un Apply")
         {
            url="classifieds/application/applyremove/classified_id/"+classifiedid+"/apply/searchapply";
         }
        // alert('classifiedid' +classifiedid);
        // alert('url' +url);
         console.log('classifiedid' +classifiedid);
      //return false;
        if(classifiedid == ' ') {} else {
               jQuery.post(url, {classifiedid: ""+classifiedid+""}, function(data) { // Do an AJAX call
                  jQuery('.process_image').css("display", "none");
                  jQuery('.process_image').hide();
                      console.log(data);
                      jQuery("#popup_box > p").text(data);
                      jQuery('#popup_box').fadeIn("slow");
                      //jQuery('#overlay').fadeIn();
                      setTimeout(function(){jQuery('#popup_box').fadeOut(300);}, 2000);
                      window.location.reload();
                     });
             }
    });
    
</script>
