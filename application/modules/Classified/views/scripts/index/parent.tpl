<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: create.tpl 10110 2013-10-31 02:04:11Z andres $
 * @author     Jung
 */
?>
<style type="text/css">
.success_msg{ background-color: #3f7500; border-radius: 0px; bottom:5px !important; color: #fff;  font-family: robotoregular;font-size: 15px;height: 35px !important;line-height:13px !important;padding: 10px;text-align: center; vertical-align: top; display:none;width:51% !important; margin-bottom:10px; float:left;left:120px !important;   margin-top:10px !important; position: inherit !important;}
#global_content > div > div.generic_layout_container.layout_top
{
  display: none;
}
body {
    overflow-x: hidden!important;
    font-family: 'Lato' !important;
    font-size: 13px;
}
  ._job_applied_head {
    float: left;
    width: 100%;
    padding-bottom: 8px;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 10px;
    font-size: 18px;
    text-transform: uppercase;
}
  ._posted_job_right
  {
    float: right;
    padding-left: 15px;
    padding-right: 15px;
  }
  ._job_applied
  {
    float: left;
    padding-left: 15px !important;
    padding-right: 15px !important;
  }
  /*Poseted Job Style*/
._posted_job_cat{
  float: left;
  width: 100%;
  background: #fff;
  margin-bottom: 10px;
}
._posted_job_cat_items_lists ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
._posted_job_cat_heading{
    font-size: 18px;
    border-bottom: 1px solid #e5e5e5;
    padding-top: 10px;
    padding-bottom: 10px;
    color: #333 
}

._posted_job_cat_items_lists{
  font-size: 14px;
  color: #333;
  padding-top: 10px;
    padding-bottom: 10px;
    border-bottom: 1px solid #e5e5e5 !important;
}
._posted_job_cat_items_lists:nth-child(even){
  border-left: 1px solid #e5e5e5;
}
._posted_job_cat_items_lists:last-of-type,
._posted_job_cat_items_lists:nth-last-of-type(2){
  border-bottom: none;
}
._posted_job_cat_items_lists ._posted_job_cat_label{
  font-size: 12px; 
  padding-left: 15px !important;
    padding-right: 15px !important;
}
._posted_job_cat_items_lists li{
    display: inline-block;
    margin-bottom: 10px;
    width: 100%;
}
.has_applicant{
  color: #2aa0b9;
}
._posted_job_cat_items_action {
    margin-top: 10px;
}
._posted_job_cat_items_action a._posted_action_btn {
    text-decoration: none;
    color: #333;
    text-transform: uppercase;
    border: 1px solid #333;
    width: 100px;
    height: 30px;
    display: inline-block;
    text-align: center;
    line-height: 30px;
    margin-right: 10px;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    -ms-transition: 0.3s ease-in-out;
    -o-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
    margin-bottom: 5px;
    font-weight: 400;
}
._posted_job_cat_items_action a._posted_action_btn:hover{
  color: #fa933c;
  border:1px solid #fa933c;
}
.block_display{
  float: right;
  text-decoration: none;
  color: #cacaca !important;
}
._posted_job_cat_block {
  
    float: left;
    width: 100%;
}

._posted_job_right_actions {
    background: white;
    padding-top: 15px;
    margin-bottom: 25px;
}
._posted_job_right_actions_btn{
  float: left;
  width: 100%;
  background: #fa933c;
  text-align: center;
  color: #fff !important;
  font-size: 14px;
  height: 40px;
  line-height: 40px;
  margin-bottom: 15px;
  text-transform: uppercase !important;
}
._posted_job_right_actions_btn i{
  font-size: 18px;
  margin-right: 5px;
}
._posted_projct,
._posted_projct_list {
    float: left;
    width: 100%;
}
._posted_projct_list_heading {
    font-size: 18px;
    color: #000000;
    padding-bottom: 10px;
    margin-bottom: 20px;  
    border-bottom: 1px solid #e1e1e1;
}
._posted_projct_list ul{
  margin:0px;
  padding:0px;
  list-style: none; 
}
._posted_projct_list li {
  float: left;
  width: 100%;
  margin-bottom: 8px;
  font-size: 16px;
  color: #000;
  text-transform: uppercase;
}
._posted_projct_list li label {
  position: relative;
  cursor: pointer;
}
._posted_projct_list li input[type="checkbox"]{
  -webkit-appearance:none;
  -moz-appearance:none;
  -ms-appearance:none;
  -o-appearance:none;
  -webkit-appearance:none;
  width: 20px;
  height: 20px;
  border:1px solid #c9c9c9;
  margin-right: 8px;
    position: relative;
    top: 0px;
    outline: none;
}
._posted_projct_list li input[type="checkbox"] + i {
    position: absolute;
    top: 10px;
    left: 3px;
    color: #fa933c;
    font-size: 13px;
    opacity: 0;
}
._posted_projct_list li input[type="checkbox"]:checked{
  border:1px solid #fa933c;
}
._posted_projct_list li input[type="checkbox"]:checked + i{
  opacity: 1;
}
@media(max-width: 1024px){

._posted_job_cat_items_action a._posted_action_btn{
  margin-bottom: 10px;
}
}
@media(max-width: 767px){
  ._posted_job_right
  {
    padding-left: 15px !important;
    padding-right: 15px !important;
  }
  ._job_applied {
    float: left;
    padding-left: 0px !important;
    padding-right: 0px !important;
}
 ._posted_job_cat_items_lists:nth-child(even){
  border-left: none;
  }
  ._posted_job_cat_items_lists:nth-last-of-type(2){
  border-bottom: 1px solid #e5e5e5;
  }
  li.set_projects:nth-child(even) {float:left !important;}
}
#global_content > div > div > div > div.generic_layout_container.layout_classified_browse_menu_quick
{
  display: none;
}
#global_content > div > div > div
{
  background-color: #e9eaed !important;
}
#parent_name
{
  background-color: #fff;border: 1px solid #dbdbdb;border-radius: 3px;margin-bottom: 10px;padding: 6px;width: 100%;
}
#parent_desc
{
  background-color: #fff;border: 1px solid #dbdbdb;border-radius: 3px;margin-bottom: 10px;padding: 6px;width: 100%;
}
._post_project
{
  padding-right: 15px !important;
  padding-left: 15px !important;
}
#parent_form > input.save_project
{
  width: 100%;
    background-color: #f0ad4e;
    text-align: center;
    height: 38px;
    line-height: 38px;
    padding-bottom: 10px;
    margin-bottom: 10px;
    border: 0px;
    color: #fff;
    text-transform: uppercase;
}
span.my_proj_parent_desc
{
  border: 1px solid #dedede;
    border-radius: 3px;
    display: block;
    height: 90px;
    margin-bottom: 10px;
    overflow: auto;
    padding: 2%;
}
.row {
    margin-right: 0px !important;
    margin-left: 0px !important;
}
li.set_projects:nth-child(even) {float: right;}
li.set_projects:nth-child(odd) {}
span.shared_by
{
  float: right;
  font-size: 15px;
}
span.shared_by > a
{
  color: #5a5a5a;
  font-weight: 500;
}
</style>
<?php
  if (APPLICATION_ENV == 'production')
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.min.js');
  else
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');
?>

<script type="text/javascript">

  en4.core.runonce.add(function()
  {
    new Autocompleter.Request.JSON('tags', '<?php echo $this->url(array('controller' => 'tag', 'action' => 'suggest'), 'default', true) ?>', {
      'postVar' : 'text',

      'minLength': 1,
      'selectMode': 'pick',
      'autocompleteType': 'tag',
      'className': 'tag-autosuggest',
      'customChoices' : true,
      'filterSubset' : true,
      'multiple' : true,
      'injectChoice': function(token){
        var choice = new Element('li', {'class': 'autocompleter-choices', 'value':token.label, 'id':token.id});
        new Element('div', {'html': this.markQueryValue(token.label),'class': 'autocompleter-choice'}).inject(choice);
        choice.inputValue = token;
        this.addChoiceEvents(choice).inject(this.choices);
        choice.store('autocompleteChoice', token);
      }
    });
  });
  
  function add_new_parent(type)
  {
		jQuery("#overlay").show();
		jQuery("#form_div").show();
		if(type == 'add'){
			jQuery("#parent_name").val('');	
		}
  }
  
  function edit_parent(obj)
  {
    jQuery('#parent_name').focus();
	jQuery("#parent_name").val(stripslashes(jQuery(obj).attr("data_title")));
	jQuery("#parent_desc").val(jQuery(obj).attr("data_desc")); 
	jQuery("#parent_id").val(jQuery(obj).attr("data_id"));
	add_new_parent('edit');
  jQuery("#overlay").hide();
  }

  function close_form()
  {
   jQuery("#parent_name").val(""); 
	jQuery("#parent_id").val("");
	
	jQuery("#form_div").hide();
	
	jQuery("#overlay").hide();
	
	
  }
  /***************** Add Edit functionality *********************/
  function submit_form()
  {
	  
   jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#parent_name").val()) == '' || jQuery.trim(jQuery("#parent_name").val().replace('""', '')) == ''  || jQuery.trim(jQuery("#parent_name").val().replace("''", "")) == ''){
			jQuery("#parent_name").focus();
			jQuery("#parent_name").addClass("com_form_error_validatin");
			return false;
	}
   var ajax_url = '<?php echo $this->baseUrl()."/classified/index/add-parent";?>';
   
   var form_data = jQuery("#parent_form").serialize();
   if(jQuery.trim(jQuery("#parent_name").val()) != "")
   {
	jQuery.ajax({
		url: ajax_url,
        type: 'POST',
        data: form_data,
        async: false,
        success: function(s){
        	var msg= s.split("::");
			if(jQuery.trim(msg[0]) == 'success')
			{
				//alert(msg[1]);
				jQuery(".success_msg").show();
				//jQuery(".success_msg").html(msg[1]);
				jQuery("#parent_name").val(); 
				jQuery("#parent_id").val(); 
				//jQuery("#form_div").hide(); 
				//jQuery("#overlay").hide(); 
				setTimeout(function(){ location.reload(); }, 1000);
				//setTimeout(2000,function(){location.reload();});
			  
			}
			else
			{
				jQuery("#signinerror").html(msg[1]);
			}
			
        }
	});
	}
	else
	{
	alert("Please enter Project name");
	return false;
	}
  }
  
  /********************* Remove Parent functionaltiy ************************/
  
  
  function remove_parent(obj)
  {
	  if( confirm("Are you sure you want to remove this project and its jobs?") )
	  {
		  var ajax_url = '<?php echo $this->baseUrl()."/classified/index/remove-parent";?>';
		  var parent_id = jQuery(obj).attr("data_id");
		  jQuery.ajax({
				url: ajax_url,
				type: 'POST',
				data: 'parent_id='+parent_id,
				async: false,
				success: function(s){
					var msg= s.split("::");
					if(jQuery.trim(msg[0]) == 'success')
					{
						location.reload();
					  
					}
					else
					{
						jQuery("#signinerror").html(msg[1]);
					}
					
				}
			});
	  }
  
  }
  
  
</script>
	<div class="layout_page_classified_index_parent">
<div class="generic_layout_container layout_top">
<div class="generic_layout_container layout_middle">
<!-- <div class="generic_layout_container layout_user_browse_menu">
<div class="headline">
  <h2>Manage Projects  </h2>
  </div></div> -->
</div>
</div>

<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
<div id="overlay" class="black_overlay"></div>
  <div class="container">
  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 _posted_job_right">
    <div class="_job_applied_head">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 _post_project">Create New  Project</div>   
                </div>
            </div>
      <div class="col-xs-12 _posted_job_right_actions">
          <div id="signinerror" style=" display:none; color:#ff0000; position: sticky; left: 293px;"></div>
               <!-- <div  id="form_div" class="white_content"> -->  
              <form name="parent_form" id="parent_form">
               
              <input type="hidden" name="parent_id" id="parent_id" value="" />
              
              <label>Title</label>
                <input type="text"  name="parent_name" id="parent_name" value="" placeholder="Enter Project Name"/>
                <br />
                <label>Description</label>
                <textarea name="parent_desc" id="parent_desc" placeholder="Enter Project Description" ></textarea>
              <input class="save_project" type="button" onClick="submit_form()" name="button" value="Save" />
              <span class="success_msg">Project saved.</span>
              </form>
               
              
              <!-- </div> -->
      </div>
      
    </div>    
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 _job_applied">
      <div class="_job_applied_head">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">My Projects</div>  
        </div>
      </div>
      <div class="_posted_job_list">
        <div class="row">
          <div class="col-xs-12">
            <div class="_posted_job_cat">             
              <div class="_posted_job_cat_block">
               <?php $categories = $this->categories;
      if(count($categories) > 0)
        {  $i=1;?>
        <ul class="classifieds_browse my_projects">
       <?php foreach( $this->categories as $cat ){?>
        <li class="set_projects">
                <div class="_posted_job_cat_items_lists">
                  <div class="row">
                  <div class='classifieds_browse_info project_title' title="<?php echo $cat['parent_desc'];?>">
                    
                    <h4><?php echo stripslashes($cat['parent_name']); ?>
                      <?php if($cat['shared_with'] != $cat['shared_by'] ){ ?>
                    <span class="shared_by">Shared by : <a href="profile/<?php echo $cat['owner_id'];?>"> <?php $user = Engine_Api::_()->getItem('user', $cat['owner_id']);
                    echo $ownername=$user['displayname'];?></a></span>
                    <?php }?>
                    </h4>
                    
                    <span class="my_proj_parent_desc"><p><?php echo $cat['parent_desc'];?> </p></span>
                  </div>
                     <!-- <div class='classifieds_browse_info project_socials'> -->
                    <div class="col-xs-12 _posted_job_cat_items_action">
                 <?php if($cat['shared_with'] == $cat['shared_by'] ){ ?>
         
                 <a class="_posted_action_btn smoothbox show_friends " href="<?php echo $this->baseUrl()."/classified/index/get-friends-shared/action_id/".$cat['parent_id'];?>">Shared</a>
                
         
                 <a class="_posted_action_btn smoothbox show_friends" href="<?php echo $this->baseUrl()."/classified/index/get-friends/action_id/".$cat['parent_id'];?>">Share</a>
         
                <a class="_posted_action_btn proj_edit" data_title="<?php echo htmlentities($cat['parent_name']); ?>"  href="javascript:void(0)" data_id="<?php echo $cat['parent_id']; ?>" data_desc="<?php echo $cat['parent_desc']; ?>">Edit</a>
                
                
                <a class="_posted_action_btn proj_delete" href="javascript:void(0)" onClick="remove_parent(this)" data_id="<?php echo $cat['parent_id']; ?>">Delete</a>
                <a class="_posted_action_btn proj_jobs" href="classifieds/jobs?parent_id=<?php echo $cat['parent_id']; ?>" data_id="<?php echo $cat['parent_id']; ?>">View</a>

                <a class="_posted_action_btn proj_postjob" href="classifieds/create?parent_id=<?php echo $cat['parent_id']; ?>" data_id="<?php echo $cat['parent_id']; ?>">Post New Job</a>
                <?php } else {
                 if( $cat['permission'] == 2 ) { ?>
                <a class="_posted_action_btn proj_edit" href="javascript:void(0)" data_id="<?php echo $cat['parent_id']; ?>" data_title="<?php echo $cat['parent_name']; ?>" data_desc="<?php echo $cat['parent_desc']; ?>" >Edit</a>
                <a class="_posted_action_btn proj_jobs" href="classifieds/jobs?parent_id=<?php echo $cat['parent_id']; ?>" data_id="<?php echo $cat['parent_id']; ?>">View</a>
        <?php } } ?>
                </div>
                  </div>
                </div></li><!-- end foreach -->

                <?php }?>
                </ul>
                <?php } else {?>
          <div class="tip">
            <span id="no-experience">No Projects. </span>
         </div>
        <?php }?>   
              </div>
            </div>

        </div>
      </div>
    </div>

  </div>
</div>
</div>
</div>
</div>
	
 <style>
    .black_overlay{
        display: none;
        position: absolute;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index:1001;
        -moz-opacity: 0.6;
        opacity:.80;
        filter: alpha(opacity=80);
    }
    .white_content {
        display: none;
        position: absolute;
        top: 25%;
        left: 25%;
        width: 50%;
        height: 50%;
        padding: 16px;
        border: 8px solid #444;
        background-color: white;
        z-index:1002;
        overflow: auto;
    }
</style>
<script type="text/javascript">
function stripslashes (str) {
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Ates Goral (http://magnetiq.com)
  // +      fixed by: Mick@el
  // +   improved by: marrtins
  // +   bugfixed by: Onno Marsman
  // +   improved by: rezna
  // +   input by: Rick Waldron
  // +   reimplemented by: Brett Zamir (http://brett-zamir.me)
  // +   input by: Brant Messenger (http://www.brantmessenger.com/)
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // *     example 1: stripslashes('Kevin\'s code');
  // *     returns 1: "Kevin's code"
  // *     example 2: stripslashes('Kevin\\\'s code');
  // *     returns 2: "Kevin\'s code"
  return (str + '').replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
    case '\\':
      return '\\';
    case '0':
      return '\u0000';
    case '':
      return '';
    default:
      return n1;
    }
  });
}
  //$$('.core_main_classified').getParent().addClass('active');
  jQuery('.custom_923').parent().addClass('active');
  jQuery('.classified_main_manage').parent().removeClass('active');
   $$('.custom_320').getParent().addClass('active');
    $$('.custom_331').getParent().addClass('active');
	 jQuery(document).ready(function(){
		jQuery('#add_new_parent').click(function(){
			add_new_parent('add');	  
	 	});
		jQuery('.proj_edit').click(function(){
			edit_parent(this);  
	 	});	
		
	   jQuery('html').click(function() {
			jQuery("#form_div").hide();
			jQuery("#overlay").hide();
		});
		
		jQuery('#form_div, #add_new_parent, .proj_edit').click(function(event){
			event.stopPropagation();
		});
		
	});

</script>
