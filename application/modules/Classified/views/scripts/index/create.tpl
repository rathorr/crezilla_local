<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: create.tpl 10110 2013-10-31 02:04:11Z andres $
 * @author     Jung
 */
?>
<script src="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.js"></script>
<link href="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.css" rel="stylesheet" />

<style type="text/css">
#descCharsRemaining-label{display:none;}
#descCharsRemaining-element{margin-left: 7px; font-weight: bold; margin-bottom: 7px; color: rgb(0, 0, 0); font-size: 18px;}
body {overflow-x: visible!important;}
#autoSuggestionsList > li {
                 background: none repeat scroll 0 0 #F3F3F3;
                 border-bottom: 1px solid #E3E3E3;
                 list-style: none outside none;
                 padding: 3px 15px 3px 15px;
                 text-align: left;
            }
            .auto_list {
                 border: 1px solid #E3E3E3;
                 border-radius: 5px 5px 5px 5px;
                 /*//position: absolute;*/
                 cursor:pointer;
                 z-index: 999 !important;
                 }
              
            
             #autoSuggestionsList > li:hover,#autoSuggestionsList > li.selected {
                background-color: white;
            }
#autoSuggestionsListl > li {
                 background: none repeat scroll 0 0 #F3F3F3;
                 border-bottom: 1px solid #E3E3E3;
                 list-style: none outside none;
                 padding: 3px 15px 3px 15px;
                 text-align: left;
            }
            .auto_listl {
                 border: 1px solid #E3E3E3;
                 border-radius: 5px 5px 5px 5px;
                 /*//position: absolute;*/
                 cursor:pointer;
                 z-index: 999 !important;
                 }
              
            
             #autoSuggestionsListl > li:hover,#autoSuggestionsListl > li.selected {
                background-color: white;
            }
</style>

<?php
  if (APPLICATION_ENV == 'production')
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.min.js');
  else
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');
?>

<script type="text/javascript">

  en4.core.runonce.add(function()
  {
    new Autocompleter.Request.JSON('tags', '<?php echo $this->url(array('controller' => 'tag', 'action' => 'suggest'), 'default', true) ?>', {
      'postVar' : 'text',

      'minLength': 1,
      'selectMode': 'pick',
      'autocompleteType': 'tag',
      'className': 'tag-autosuggest',
      'customChoices' : true,
      'filterSubset' : true,
      'multiple' : true,
      'injectChoice': function(token){
        var choice = new Element('li', {'class': 'autocompleter-choices', 'value':token.label, 'id':token.id});
        new Element('div', {'html': this.markQueryValue(token.label),'class': 'autocompleter-choice'}).inject(choice);
        choice.inputValue = token;
        this.addChoiceEvents(choice).inject(this.choices);
        choice.store('autocompleteChoice', token);
      }
    });
  });
  
  function validatevlassified(){
		
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery("#project_id").val()) == '0' && jQuery.trim(jQuery("#pro_title").val()) == ''){
			jQuery("#pro_title").focus();
			jQuery("#pro_title").addClass("com_form_error_validatin");
			return false;
	}

	if(jQuery.trim(jQuery("#title").val()) == ''){
			jQuery("#title").focus();
			jQuery("#title").addClass("com_form_error_validatin");
			return false;
	}

	
	if(jQuery.trim(jQuery(".field_73").val()) == ''){
			jQuery(".field_73").focus();
			jQuery("#0_0_73-element .SumoSelect > p.CaptionCont.SlectBox").addClass("com_form_error_validatin");	    
  		return false;
	}
  if(jQuery.trim(jQuery(".field_74").val()) == ''){
      jQuery(".field_74").focus();
      jQuery("#0_0_74-element .SumoSelect > p.CaptionCont.SlectBox").addClass("com_form_error_validatin");      
      return false;
  }
	if(jQuery('input[name="fields[0_0_72]"]:checked').length <= 0)
    {
     jQuery(".field_72").focus();
      jQuery("#0_0_72-element ul > li").addClass("com_form_error_validatin");             
      return false;
    }
  if(jQuery.trim(jQuery("#fields-0_0_61").val()) == ''){
      jQuery("#fields-0_0_61").focus();
      jQuery("#fields-0_0_61").addClass("com_form_error_validatin");
      return false;
  }
  if(jQuery.trim(jQuery("#fields-0_0_14").val()) == ''){
      jQuery("#fields-0_0_14").focus();
      jQuery("#fields-0_0_14").addClass("com_form_error_validatin");
      return false;
  }
  if(jQuery.trim(jQuery("#fields-0_0_41").val()) == ''){
      jQuery("#fields-0_0_41").focus();
      jQuery("#fields-0_0_41").addClass("com_form_error_validatin");
      return false;
  }
  if(jQuery('input[name="fields[0_0_71]"]:checked').length <= 0)
    {
     jQuery(".field_71").focus();
      jQuery("#0_0_71-element ul > li").addClass("com_form_error_validatin");             
      return false;
    }
	if(jQuery.trim(jQuery("#script_title").val()) == '' && jQuery.trim(jQuery("#script").val()) != ''){
			jQuery("#script_title").focus();
			jQuery("#script_title").addClass("com_form_error_validatin");
			return false;
	}
	
	if(jQuery.trim(jQuery("#script_title").val()) != '' && jQuery.trim(jQuery("#script").val()) == ''){
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Please upload additional information file.");
			jQuery("#signinerror").fadeOut(10000);
			jQuery(window).scrollTop(0);
			return false;
	}
	
	if(jQuery.trim(jQuery("#script").val()) != ""){
		var fName = jQuery("#script").val();
		var aFile = fName.split('.');
		if(aFile[aFile.length - 1].toLowerCase() != 'pdf'){
			jQuery("#signinerror").show();
			jQuery("#signinerror").html("Please upload only pdf file.");
			jQuery("#signinerror").fadeOut(10000);
			jQuery(window).scrollTop(0);
			return false;
		}	
	}
	
	
	jQuery("#classifieds_create").submit();
	  
   
  }
 
 function showhideprobox(){
		if(jQuery.trim(jQuery('#project_id').val()) != '' && jQuery.trim(jQuery('#project_id').val()) != '0'){
		//if(jQuery.trim(jQuery('#project_id').val()) == '0'){
				//window.location.href = "<?php echo $this->baseUrl();?>/classified/index/parent";
				jQuery('#pro_title-wrapper').hide();
				jQuery('#pro_desc-wrapper').hide();
		   }
		   else{
				jQuery('#pro_title-wrapper').show();
				jQuery('#pro_desc-wrapper').show();   
		   }
	 }
	
 
</script>

<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
    //'topLevelId' => (int) @$this->topLevelId,
    //'topLevelValue' => (int) @$this->topLevelValue
  ))
?>

<?php if (($this->current_count >= $this->quota) && !empty($this->quota)):?>
  <div class="tip">
    <span>
      <?php echo $this->translate('You have already created the maximum number of classified listings allowed.');?>
      <?php echo $this->translate('If you would like to create a new listing, please <a href="%1$s">delete</a> an old one first.', $this->url(array('action' => 'manage'), 'classified_extended'));?>
    </span>
  </div>
  <br/>
<?php else:?>
<div id="signinerror" style="display: none; position: sticky; left: 293px;"></div>
  <?php echo $this->form->render($this);?>
<?php endif; ?>


<script type="text/javascript">
   $$('.classified_main_manage').getParent().addClass('active');
   $$('.custom_319').getParent().addClass('active');
   jQuery('.custom_295').parent().addClass('active');
   $$('.custom_330').getParent().addClass('active');
   
   
jQuery(document).ready(function(){
  jQuery('<div id="autoSuggestionsListl" autocomplete="off" style="display: block;margin-top: -10px;width: 99%;"></div>').appendTo( jQuery( "#0_0_14-wrapper" ) );
  jQuery('#0_0_76-wrapper').css('display','none');
  jQuery("#fields-0_0_14").attr("autocomplete","off");
	
	jQuery('#fields-0_0_61').attr('autocomplete','off');
	jQuery('#fields-0_0_73').SumoSelect({selectAll: true,placeholder: 'Select Language'});
	jQuery('#fields-0_0_74').SumoSelect({selectAll: false,placeholder: 'Select Experience'});
	jQuery('#fields-0_0_76').SumoSelect({selectAll: false,placeholder: 'Select Skills'});
  jQuery('#fields-0_0_77').attr('placeholder','dd-mm-yyyy');
  jQuery('#fields-0_0_41').attr('placeholder','Ex: 2 month, 2 year');

		jQuery('.required').each(function(){
			var area	=	jQuery(this);
			jQuery( '<span class="job_requied_field" style="font-weight: bold; color: rgb(255, 0, 0); font-size: 16px;"> *</span>' ).insertAfter( area );
			
		});
		
		jQuery( '<span class="job_requied_field" style="font-weight: bold; color: rgb(255, 0, 0); font-size: 16px;"> *</span>' ).insertAfter( jQuery('#pro_title-label').find('label'));
		
		jQuery('input[type="checkbox"]').each(function(){
			jQuery(this).addClass('chk_inactive');
		});
		
		jQuery('input[type="radio"]').each(function(){
			jQuery(this).addClass('radio_inactive');
		});
    
	   jQuery('input[type="checkbox"]').on('click', function(){
			if(jQuery(this).is(':checked') ==true){
				jQuery(this).addClass('chk_active');
				jQuery(this).removeClass('chk_inactive');	
			}else{
				jQuery(this).addClass('chk_inactive');
				jQuery(this).removeClass('chk_active');	
			}   
	   });
	   
	   jQuery('input[type="radio"]').on('click', function(){
		   jQuery(this).parent().parent().find('input[type="radio"]').each(function(){
				jQuery(this).addClass('radio_inactive'); 
				jQuery(this).removeClass('radio_active');  
		   });
			if(jQuery(this).is(':checked') ==true){
				jQuery(this).addClass('radio_active');
				jQuery(this).removeClass('radio_inactive');	
			}else{
				jQuery(this).addClass('radio_inactive');
				jQuery(this).removeClass('radio_active');	
			}   
	   });
	   var parentid = "<?php echo $_GET['parent_id']; ?>";
	   console.log("parentid" +parentid);
	   jQuery('#project_id').val(parentid);
	   jQuery('<div id="autoSuggestionsList" style="display: block;margin-top: -10px;width: 99%;"></div>').appendTo( jQuery( "#0_0_61-wrapper" ) );
});

 jQuery(document).on('keyup','#fields-0_0_61', function(e) {
         console.log(jQuery(this).val().length);
         var inputString = jQuery(this).val();
         console.log(inputString.length);
        if(inputString.length < 2) {
                jQuery('#autoSuggestionsList').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/experience/index/getallroles";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                   // data = jQuery.parseJSON(data);
                    //data = JSON.parse(data);                    
                    if(data)
                    {
                    	jQuery('#autoSuggestionsList').show();
	                    jQuery('#autoSuggestionsList').addClass('auto_list');
	                    jQuery('#autoSuggestionsList').html(data);
                    }
                    else
                    {
                    	jQuery('#autoSuggestionsList').hide();
                    }
                           
                     });
                 
             }
            
            
    });
      jQuery(document).on('click','#autoSuggestionsList li',function(){ 
            var el=jQuery(this).text();
            var elid=jQuery(this).attr('id');
            console.log("elid"+elid);
           jQuery('#fields_0_0_61_id').val(elid);
           jQuery('#fields-0_0_61').val(el);
           // alert('hello'+el);
          // $('#search-column #submit-search').click();
        setTimeout("jQuery('#autoSuggestionsList').hide();", 200);
       });
      jQuery('#pro_title').keyup(function(e){
      //var len = e.value.length;
      var len = jQuery(this).val().length;
        if (len >= 30) {
          alert("You have reached the max limit of 30 char!");
          //$(this).val() = $(this).val().substring(0, 300);
           e.preventDefault();
          //return false;
        } else {
          console.log(30 - len);
         // $('#charNum').text(300 - len);
        }
    });

       jQuery('#pro_desc').keyup(function(e){
      //var len = e.value.length;
      var len = jQuery(this).val().length;
        if (len >= 70) {
          alert("You have reached the max limit of 70 char!");
          //$(this).val() = $(this).val().substring(0, 300);
           e.preventDefault();
          //return false;
        } else {
          console.log(70 - len);
         // $('#charNum').text(300 - len);
        }
    });

        jQuery('#title').keyup(function(e){
      //var len = e.value.length;
      var len = jQuery(this).val().length;
        if (len >= 40) {
          alert("You have reached the max limit of 40 char!");
          //$(this).val() = $(this).val().substring(0, 300);
           e.preventDefault();
          //return false;
        } else {
          console.log(40 - len);
         // $('#charNum').text(300 - len);
        }
    });
  jQuery(document).on('keyup','#fields-0_0_14', function(e) {
         var inputString = jQuery(this).val();
        if(inputString.length < 1) {
                jQuery('#autoSuggestionsListl').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/user/edit/getlocations";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                      if(data)
                        {
                            jQuery('#autoSuggestionsListl').show();
                            jQuery('#autoSuggestionsListl').addClass('auto_listl');
                            jQuery('#autoSuggestionsListl').html(data);
                        }
                        else
                        {
                            jQuery('#autoSuggestionsListl').hide();
                        }
                     });
             }
    });
      jQuery(document).on('click','#autoSuggestionsListl li',function(){ 
            var el=jQuery(this).text();
           jQuery('#fields-0_0_14').val(el);
        setTimeout("jQuery('#autoSuggestionsListl').hide();", 200);
       });
</script>
