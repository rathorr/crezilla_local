<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: manage.tpl 9987 2013-03-20 00:58:10Z john $
 * @author     Jung
 */
 
 $this->headScript()
	
       ->appendFile($this->baseUrl() . '/application/modules/User/externals/js/min.js')
	   ->appendFile($this->baseUrl() . '/application/modules/User/externals/assets/js/jquery-ui.min.js');
?>
<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<style type="text/css">
body {
    overflow-x: hidden!important;
    font-family: 'Lato' !important;
    font-size: 13px;
}
  ._job_applied_head {
    float: left;
    width: 100%;
    padding-bottom: 8px;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 10px;
    font-size: 18px;
    text-transform: uppercase;
}
  ._posted_job_right
  {
    float: right;
    padding-left: 15px !important;
    padding-right: 15px !important;
  }
  ._job_applied
  {
    float: left;
    padding-left: 15px !important;
    padding-right: 15px !important;
  }
  /*Poseted Job Style*/
._posted_job_cat{
  float: left;
  width: 100%;
  background: #fff;
  margin-bottom: 10px;
}
._posted_job_cat_items_lists ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
._posted_job_cat_heading{
    font-size: 18px;
    border-bottom: 1px solid #e5e5e5;
    padding-top: 10px;
    padding-bottom: 10px;
    color: #333 
}

._posted_job_cat_items_lists{
  font-size: 14px;
  color: #333;
  padding-top: 15px;
    padding-bottom: 15px;
    border-bottom: 1px solid #e5e5e5 !important;
        border-right: 1px solid #e5e5e5;
}
._posted_job_cat_items_lists:nth-child(even){
  /*border-left: 1px solid #e5e5e5;*/
}
._posted_job_cat_items_lists:last-of-type,
._posted_job_cat_items_lists:nth-last-of-type(2){
  border-bottom: none;
}
._posted_job_cat_items_lists ._posted_job_cat_label{
  font-size: 12px; 
  padding-left: 15px !important;
    padding-right: 15px !important;
}
._posted_job_cat_items_lists li{
  display: inline-block;
    margin-bottom: 10px;
    width: 100%;
}
.has_applicant{
  color: #2aa0b9;
}
._posted_job_cat_items_action {
    margin-top: 15px;
}
._posted_job_cat_items_action a._posted_action_btn {
    text-decoration: none;
    color: #333;
    text-transform: uppercase;
    border: 1px solid #333;
    width: 100px;
    height: 30px;
    display: inline-block;
    text-align: center;
    line-height: 30px;
    margin-right: 10px;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    -ms-transition: 0.3s ease-in-out;
    -o-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
._posted_job_cat_items_action a._posted_action_btn:hover{
  color: #fa933c;
  border:1px solid #fa933c;
}
.block_display{
  float: right;
  text-decoration: none;
  color: #cacaca !important;
}
._posted_job_cat_block {
  display: none;
    float: left;
    width: 100%;
}

._posted_job_right_actions {
    background: white;
    padding-top: 15px;
    margin-bottom: 25px;
}
._posted_job_right_actions_btn{
  float: left;
  width: 100%;
  background: #fa933c;
  text-align: center;
  color: #fff !important;
  font-size: 14px;
  height: 40px;
  line-height: 40px;
  margin-bottom: 15px;
  text-transform: uppercase !important;
}
._posted_job_right_actions_btn i{
  font-size: 18px;
  margin-right: 5px;
}
._posted_projct,
._posted_projct_list {
    float: left;
    width: 100%;
}
._posted_projct_list_heading {
    font-size: 18px;
    color: #000000;
    padding-bottom: 10px;
    margin-bottom: 20px;  
    border-bottom: 1px solid #e1e1e1;
}
._posted_projct_list ul{
  margin:0px;
  padding:0px;
  list-style: none; 
}
._posted_projct_list li {
  float: left;
  width: 100%;
  margin-bottom: 8px;
  font-size: 16px;
  color: #000;
  text-transform: uppercase;
}
._posted_projct_list li label {
  position: relative;
  cursor: pointer;
}
._posted_projct_list li input[type="checkbox"]{
  -webkit-appearance:none;
  -moz-appearance:none;
  -ms-appearance:none;
  -o-appearance:none;
  -webkit-appearance:none;
  width: 20px;
  height: 20px;
  border:1px solid #c9c9c9;
  margin-right: 8px;
    position: relative;
    top: 0px;
    outline: none;
}
._posted_projct_list li input[type="checkbox"] + i {
    position: absolute;
    top: 10px;
    left: 3px;
    color: #fa933c;
    font-size: 13px;
    opacity: 0;
}
._posted_projct_list li input[type="checkbox"]:checked{
  border:1px solid #fa933c;
}
._posted_projct_list li input[type="checkbox"]:checked + i{
  opacity: 1;
}
@media(max-width: 1024px){

._posted_job_cat_items_action a._posted_action_btn{
  margin-bottom: 10px;
}
}
@media(max-width: 767px){
 ._posted_job_cat_items_lists:nth-child(even){
  border-left: none;
  }
  ._posted_job_cat_items_lists:nth-last-of-type(2){
  border-bottom: 1px solid #e5e5e5;
  }
}
#global_content > div > div > div > div.generic_layout_container.layout_classified_browse_menu_quick
{
  display: none;
}
#global_content > div > div > div
{
  background-color: #e9eaed !important;
}

</style>

<?php
  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
    //'topLevelId' => (int) @$this->topLevelId,
    //'topLevelValue' => (int) @$this->topLevelValue
  ))
?>

  <?php if (($this->current_count >= $this->quota) && !empty($this->quota)):?>
    <div class="tip">
      <span>
        <?php echo $this->translate('You have already created the maximum number of listings allowed. If you would like to create a new listing, please delete an old one first.');?>
      </span>
    </div>
    <br/>
  <?php endif; ?>
  
<div class="container">
<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 _posted_job_right">
      <div class="col-xs-12 _posted_job_right_actions">
        <a href="classifieds/create" class="_posted_job_right_actions_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> post a new job</a>
        <a href="classified/index/parent" class="_posted_job_right_actions_btn"><i class="fa fa-desktop" aria-hidden="true"></i> My Project</a>
      </div>
      <div class="col-xs-12 _posted_projct">
        <div class="_posted_projct_list_heading">Projects</div>
        <div class="_posted_projct_list">

          <ul>
            <li><label><input type="checkbox" name=""><i class="fa fa-check" aria-hidden="true"></i> all</label></li>
            <?php foreach($this->projects as $project){
            $permission = $project['permission'];
            $owner_id = $project['owner_id'];
            $cuser_id = $project['user_id'];
            $project_id=$project['project_id'];
            $project_job_count=count($project['project_jobs']);
            $valtotal=Engine_Api::_()->getDbtable('classifieds', 'classified')->gettotaljobsProjectwise($project_id) ?: "0";  

          if($project_job_count > 0 ){?>
            <li><label><input type="checkbox" name=""><i class="fa fa-check" aria-hidden="true"></i> <?php echo $project['project_name']. "($valtotal)";?> </label></li>
            <?php } else {?>
            <li><label><input type="checkbox" name=""><i class="fa fa-check" aria-hidden="true"></i> <?php echo $project['project_name']. "($valtotal)";?> </label></li>
            <?php } }?>
          </ul>
        </div>
      </div>
    </div>  
<?php if( count($this->projects) > 0 ){  ?>
   <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 _job_applied">
      <div class="_job_applied_head">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">my posted jobs (<?php echo $this->totaljobs;?>)</div>  
        </div>
      </div>
     <?php foreach($this->projects as $project){
     //echo '<pre>';print_r($project);die();
    $permission = $project['permission'];
    $owner_id = $project['owner_id'];
    $cuser_id = $project['user_id'];
    $project_id=$project['project_id'];
    $project_job_count=count($project['project_jobs']);
    $valtotal=Engine_Api::_()->getDbtable('classifieds', 'classified')->gettotaljobsProjectwise($project_id) ?: "0";  

    if($project_job_count > 0 ){
    
    
    ?>
      <div class="_posted_job_list">
        <div class="row">
          <div class="col-xs-12">
          
            <div class="_posted_job_cat">
              <div class="col-xs-12 _posted_job_cat_heading"><?php echo $project['project_name']. "($valtotal)";?> 
                <a href="javascript:void(0);" class="block_display"><i class="fa fa-plus" aria-hidden="true"></i></a>
              </div>
              <div class="_posted_job_cat_block">
              <?php foreach($project['project_jobs'] as $item ){
               
        if($item->getIdentity() > 0){
       /* $page_detail  = $item['page_id'] ? Engine_Api::_()->getItem('page', $item['page_id']):'';
        $owner_detail= Engine_Api::_()->getItem('user', $item['owner_id']);
        echo $page_detail ? $page_detail->getTitle():$owner_detail;*/
       ?>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 _posted_job_cat_items_lists">
                  <div class="row">
                    <ul>
                      <li>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 _posted_job_cat_label">Title :</div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6"><?php echo $this->htmlLink($item->getHref(), $item->getTitle());?></div>
                      </li>
                      <li>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 _posted_job_cat_label">Designation :</div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6"> 
                        <?php $data  = Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedDataById($item->classified_id);
                foreach($data as $res){
                  
                    if($res['field_id'] == 61){
                      echo $res['values'];
                    }
                    }
                    ?></div>
                      </li>
                      <li>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 _posted_job_cat_label">Location :</div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6"><?php echo Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedLocationById($item->classified_id);?></div>
                      </li>
                      <li>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 _posted_job_cat_label">Created Date :</div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6"><?php echo date('F d, Y', strtotime($item->creation_date));?></div>
                      </li>
                      <?php 
                      $applyby= Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedsapplyBy($item->classified_id); 
                      if ($applyby) {?>
                      <li>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 _posted_job_cat_label">Apply By :</div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6"><?php 
                        echo date('F d, Y', strtotime($applyby));?></div>
                      </li>
                      <?php }?>
                      <li>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 _posted_job_cat_label">Applicants :</div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6 no-applicant has_applicant">
                        <?php
        $classified_id  = $item->getIdentity();
        $table = Engine_Api::_()->getDbtable('applys', 'classified'); 
    //$select = $table->select();
    $applyname = $table->info('name'); 
    
    $select = $table->select("$applyname.*")
        ->where("$applyname.classified_id = $classified_id", 1);
        
    $result = $table->fetchAll($select);
        $text = ($result->count()>0)? '('.$result->count().') applicants':'No Applicant Yet';
    $class  = ($result->count()>0)? 'yes-applicant':'no-applicant';
    $spanclass  = ($result->count()>0)? 'span-yes-applicant':'span-no-applicant';
         echo $this->htmlLink(array(
        'route' => 'classified_apply',
        'controller' => 'application',
        'action' => 'list',       
        'classified_id' => $item->getIdentity(),
        ), $text, array(
              'class' => $class
            )); ?></div>
                      </li>
                    </ul>
                   <?php if($permission == 2)
                    {?>
                    <div class="col-xs-12 _posted_job_cat_items_action">

                      <!-- <a href="javascript:void(0)" class="_posted_action_btn _posted_job_edit"> --><?php
                      echo $this->htmlLink(array(
                          'route' => 'classified_specific',
                          'action' => 'edit',
                          'classified_id' => $item->getIdentity(),
                        ), $this->translate('Edit'), array(
                          'class' => '_posted_action_btn _posted_job_edit jp_edit'
                        )) ; ?>
                      <!-- </a> -->
                      <!-- <a href="javascript:void(0)" class="_posted_action_btn _posted_job_close"> -->
                <?php
                      if( !$item->closed ){       
      echo $this->htmlLink(array(
                'route' => 'classified_specific',
                'action' => 'close',
                'classified_id' => $item->getIdentity(),
                'closed' => 1,
                'format' => 'smoothbox'
              ), $this->translate('Close'), array(
                'class' => '_posted_action_btn _posted_job_remove smoothbox jp_hold'
              ));
       } else{
      echo $this->htmlLink(array(
                'route' => 'classified_specific',
                'action' => 'close',
                'classified_id' => $item->getIdentity(),
                'closed' => 0,
                'format' => 'smoothbox'
              ), $this->translate('Open'), array(
                'class' => '_posted_action_btn _posted_job_remove smoothbox jp_open'
              ));
       }} ?>
        <!-- </a> -->
                      <!-- <a href="javascript:void(0)" class="_posted_action_btn _posted_job_remove"> -->
                      <?php
                       if($owner_id == $cuser_id )
                        {
                          echo $this->htmlLink(array(
                            'route' => 'default', 
                            'module' => 'classified', 
                            'controller' => 'index', 
                            'action' => 'delete', 
                            'classified_id' => $item->getIdentity(), 
                            'format' => 'smoothbox'), 
                            $this->translate('Delete'), array(
                            'class' => '_posted_action_btn _posted_job_remove smoothbox'
                          ));
                        }?>
                          
                      <!-- </a> -->
                    </div>  
                  </div>
                </div>
                <?php } }?>
              </div>
            </div>
      
          </div>
        </div>
      </div>
      <?php } else { ?>

      <div class="_posted_job_list">
        <div class="row">
          <div class="col-xs-12">
          
            <div class="_posted_job_cat">
              <div class="col-xs-12 _posted_job_cat_heading"><?php echo $project['project_name']. "($valtotal)";?> 
                <a href="javascript:void(0);" class="block_display"><i class="fa fa-plus" aria-hidden="true"></i></a>
              </div>
              <div class="_posted_job_cat_block">
               <p style="text-align: center;">No jobs created under this project.</p>
             
              </div>
            </div>
      
          </div>
        </div>
      </div>


      <?php } } } else{?>
        <div class="tip">
            <span id="no-experience">No Jobs. </span>
         </div>
    <?php } ?>
    </div>
   
</div>

<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('._posted_job_list').first().find('._posted_job_cat_block').show();
    jQuery('.block_display').first().find('i').removeClass('fa-plus').addClass('fa-minus');
     jQuery('.block_display').on('click', function(){
      jQuery(this).find('i').toggleClass('fa-plus');
      jQuery(this).find('i').toggleClass('fa-minus');
      jQuery(this).closest('._posted_job_cat').find('._posted_job_cat_block').slideToggle();
    })
  });
</script>
<script type="text/javascript">
  $$('.core_main_classified').getParent().addClass('active');
   $$('.custom_319').getParent().addClass('active');
   jQuery('.custom_295').parent().addClass('active');
   $$('.custom_923').getParent().addClass('active');
</script>
