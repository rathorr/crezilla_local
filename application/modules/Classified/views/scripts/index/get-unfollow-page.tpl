<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: create.tpl 10110 2013-10-31 02:04:11Z andres $
 * @author     Jung
 */
?>

 

 <script type="text/javascript" src="<?php echo $this->baseUrl(); ?>/public/js/jquery.min.js"></script>

<script type="text/javascript">

 function remove_parent(obj)
  {
	 
	   
		  var ajax_url = '<?php echo $this->baseUrl()."/classified/index/unfollow-page";?>';
		  var project_id = jQuery('#project_id').val();
		  
		   
		  jQuery.ajax({
				url: ajax_url,
				type: 'POST',
				data: { project_id: project_id },
				async: false,
				success: function(s){
					
					var msg= s.split("::");
 					if(jQuery.trim(msg[0]) == 'success')
					{
						parent.location.reload()
					  
					}
					else
					{
						jQuery("#signinerror").html(msg[1]);
					}
					
				}
			});
	  
  
  }
  


</script>

<!--<div class="headline">
  <h2>Are you sure you want to un-follow this page?
  </h2>
 
</div>-->
 

<div id="signinerror" style=" display:none; color:#ff0000; position: sticky; left: 293px;"></div>

<form name="share_form" id="share_form" class="global_form_popup">
<input type="hidden" name="project_id" id="project_id" value="<?php echo $this->project_id; ?>" />

<h3>Unfollow
  </h3>
 <p class="form-description" style="margin-top:5px;">Would you like to unfollow this page?</p>
<div class="form-elements">
<div id="buttons-wrapper" class="form-wrapper"><fieldset id="fieldset-buttons">

<button name="submit" id="submit" type="button" onclick="remove_parent();">Yes</button>

 or <a onclick="parent.Smoothbox.close();" href="javascript:void(0);" type="button" id="cancel" name="cancel">cancel</a></fieldset></div></div>
 
<!--<table cellpadding="10" cellspacing="10">
  
 <tr>
  
 <td colspan="2"><fieldset id="fieldset-buttons">

<button name="submit" id="submit" type="button" onclick="remove_parent();">Yes</button>

 or <a name="cancel" id="cancel" type="button" href="javascript:void(0);" onclick="parent.Smoothbox.close();">No</a></fieldset></td>
 </tr>
 
 <tr>
 <td colspan="2">&nbsp; </td>
 </tr>
  
</table>-->
</form>
	
	
	

	

 
 <style>
    .black_overlay{
        display: none;
        position: absolute;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index:1001;
        -moz-opacity: 0.8;
        opacity:.80;
        filter: alpha(opacity=80);
    }
    .white_content {
        display: none;
        position: absolute;
        top: 25%;
        left: 25%;
        width: 50%;
        height: 50%;
        padding: 16px;
        border: 16px solid orange;
        background-color: white;
        z-index:1002;
        overflow: auto;
    }
	.tag 
	{
	display: block;
	}
	.proj_delete 
	{
	background-color:#D00000;
	border-radius:5px !important;
	color:#FFFFFF;
	display:inline-block;
	padding:5px 10px;
	}

a:hover 
{ 
   text-decoration:none;
}
</style>

