<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: edit.tpl 10110 2013-10-31 02:04:11Z andres $
 * @author     Jung
 */


?>
<script src="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.js"></script>
<link href="<?php echo $this->baseUrl();?>/public/multiselect/multiselect.css" rel="stylesheet" />
<style type="text/css">
body {overflow-x: visible!important;}
#autoSuggestionsList > li {
                 background: none repeat scroll 0 0 #F3F3F3;
                 border-bottom: 1px solid #E3E3E3;
                 list-style: none outside none;
                 padding: 3px 15px 3px 15px;
                 text-align: left;
            }
            .auto_list {
                 border: 1px solid #E3E3E3;
                 border-radius: 5px 5px 5px 5px;
                 /*//position: absolute;*/
                 cursor:pointer;
                 z-index: 999 !important;
                 }
              
            
             #autoSuggestionsList > li:hover,#autoSuggestionsList > li.selected {
                background-color: white;
            }
  #autoSuggestionsListl > li {
                 background: none repeat scroll 0 0 #F3F3F3;
                 border-bottom: 1px solid #E3E3E3;
                 list-style: none outside none;
                 padding: 3px 15px 3px 15px;
                 text-align: left;
            }
            .auto_listl {
                 border: 1px solid #E3E3E3;
                 border-radius: 5px 5px 5px 5px;
                 /*//position: absolute;*/
                 cursor:pointer;
                 z-index: 999 !important;
                 }
              
            
             #autoSuggestionsListl > li:hover,#autoSuggestionsListl > li.selected {
                background-color: white;
            }
</style>
<?php
  if (APPLICATION_ENV == 'production')
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.min.js');
  else
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');
?>

<script type="text/javascript">
  en4.core.runonce.add(function()
  {
    new Autocompleter.Request.JSON('tags', '<?php echo $this->url(array('controller' => 'tag', 'action' => 'suggest'), 'default', true) ?>', {
      'postVar' : 'text',

      'minLength': 1,
      'selectMode': 'pick',
      'autocompleteType': 'tag',
      'className': 'tag-autosuggest',
      'filterSubset' : true,
      'multiple' : true,
      'injectChoice': function(token){
        var choice = new Element('li', {'class': 'autocompleter-choices', 'value':token.label, 'id':token.id});
        new Element('div', {'html': this.markQueryValue(token.label),'class': 'autocompleter-choice'}).inject(choice);
        choice.inputValue = token;
        this.addChoiceclassifieds(choice).inject(this.choices);
        choice.store('autocompleteChoice', token);
      }
    });
  });
  
 
  jQuery(document).ready(function(){
     jQuery('<div id="autoSuggestionsListl" autocomplete="off" style="display: block;margin-top: -10px;width: 99%;"></div>').appendTo( jQuery( "#0_0_14-wrapper" ) );
     jQuery('#0_0_76-wrapper').css('display','none');
  jQuery("#fields-0_0_14").attr("autocomplete","off");
  	jQuery('#fields-0_0_73').SumoSelect({selectAll: true,placeholder: 'Select Language'});
	jQuery('#fields-0_0_74').SumoSelect({selectAll: false,placeholder: 'Select Experience'});
	jQuery('#fields-0_0_76').SumoSelect({selectAll: false,placeholder: 'Select Skills'});
  jQuery('#fields-0_0_77').attr('placeholder','dd-mm-yyyy');
  jQuery('#fields-0_0_41').attr('placeholder','Ex: 2 month, 2 year');
	  jQuery('.required').each(function(){
			var area	=	jQuery(this);
			jQuery( '<span class="job_requied_field" style="font-weight: bold; color: rgb(255, 0, 0); font-size: 16px;"> *</span>' ).insertAfter( area );
			
		});
		jQuery( '<span class="job_requied_field" style="font-weight: bold; color: rgb(255, 0, 0); font-size: 16px;"> *</span>' ).insertAfter( jQuery('#pro_title-label'));
		
	  $$('.custom_330').getParent().addClass('active');
	  $$('.custom_319').getParent().addClass('active');
  	  $$('.custom_295').getParent().addClass('active');
		
		jQuery('input[type="checkbox"]').each(function(){
			jQuery(this).addClass('chk_inactive');
		});
		
		jQuery('input[type="radio"]').each(function(){
			jQuery(this).addClass('radio_inactive');
		});
		
		jQuery('input[type="checkbox"]:checked').each(function(){
			jQuery(this).addClass('chk_active');
			jQuery(this).removeClass('chk_inactive');
		});
		
		jQuery('input[type="radio"]:checked').each(function(){
			jQuery(this).addClass('radio_active'); 
			jQuery(this).removeClass('radio_inactive');
		});
		
	   jQuery('input[type="checkbox"]').on('click', function(){
			if(jQuery(this).is(':checked') ==true){
				jQuery(this).addClass('chk_active');
				jQuery(this).removeClass('chk_inactive');	
			}else{
				jQuery(this).addClass('chk_inactive');
				jQuery(this).removeClass('chk_active');	
			}   
	   });
	   
	   jQuery('input[type="radio"]').on('click', function(){
		   jQuery(this).parent().parent().find('input[type="radio"]').each(function(){
				jQuery(this).addClass('radio_inactive'); 
				jQuery(this).removeClass('radio_active');  
		   });
			if(jQuery(this).is(':checked') ==true){
				jQuery(this).addClass('radio_active');
				jQuery(this).removeClass('radio_inactive');	
			}else{
				jQuery(this).addClass('radio_inactive');
				jQuery(this).removeClass('radio_active');	
			}   
	   });
	   jQuery('<div id="autoSuggestionsList" style="display: block;margin-top: -10px;width: 99%;"></div>').appendTo( jQuery( "#0_0_61-wrapper" ) );
	   jQuery('<input type="hidden" name="fields_0_0_61_id" value="" class="hide_box" id="fields_0_0_61_id" style="display: none;">').appendTo( jQuery( "#0_0_61-wrapper" ) );
  jQuery('#fields-0_0_61').attr('autocomplete','off');

   jQuery(document).on('keyup','#fields-0_0_61', function(e) {
         console.log(jQuery(this).val().length);
         var inputString = jQuery(this).val();
         console.log(inputString.length);
        if(inputString.length < 2) {
                jQuery('#autoSuggestionsList').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/experience/index/getallroles";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                   // data = jQuery.parseJSON(data);
                    //data = JSON.parse(data);                    
                    if(data)
                    {
                    	jQuery('#autoSuggestionsList').show();
	                    jQuery('#autoSuggestionsList').addClass('auto_list');
	                    jQuery('#autoSuggestionsList').html(data);
                    }
                    else
                    {
                    	jQuery('#autoSuggestionsList').hide();
                    }
                           
                     });
                 
             }
            
            
    });
      jQuery(document).on('click','#autoSuggestionsList li',function(){ 
            var el=jQuery(this).text();
            var elid=jQuery(this).attr('id');
            console.log("elid"+elid);
           jQuery('#fields_0_0_61_id').val(elid);
           jQuery('#fields-0_0_61').val(el);
           // alert('hello'+el);
          // $('#search-column #submit-search').click();
        setTimeout("jQuery('#autoSuggestionsList').hide();", 200);
       });

  });
</script>

<?php

  /* Include the common user-end field switching javascript */
  echo $this->partial('_jsSwitch.tpl', 'fields', array(
    //'topLevelId' => (int) @$this->topLevelId,
    //'topLevelValue' => (int) @$this->topLevelValue
  ))
?>



<style>

#global_content h2 {
    color: #3faee8;
    font-family: robotoregular;
    font-size: 22px;
    font-weight: 700;
    margin: 0;
    padding: 10px 20px;
    text-transform: capitalize;
}
.global_form > div {
  width:100%;
}
.global_form > div > div {
  
  width:100%;
}
select {
  height:auto;
  width:100%;
}

div.field-privacy-selector > span.caret {
  
  display:none;
  
}


ul.form-errors > li, ul.form-notices > li {
 float:none;
}

.thumb_profile, .thumb_icon {border-radius:0 !important;}
.global_form > div > div {
  background:transparent !important;
  padding:0 10px !important;
  width:100%;
}
h2.create_pdf
{margin:6px !important;}
h2.create_pdf a
{padding:10px !important;}
</style>

<div class="headline">
  <h2>
     
      <?php echo $this->translate('Manage Jobs');?>
    
  </h2>
  <div class="tabs">
    <?php
      // Render the menu
        //echo $this->navigation()
       // ->menu()
      //  ->setContainer($this->navigation)
      // ->render();
		
    ?>
  </div>
</div>

<div  class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle" style="width:100%; background-color: #fff;>
<div class="generic_layout_container layout_core_content">

<div id="signinerror" style="display: none; position: sticky; left: 293px;"></div>
<form action="<?php echo $this->escape($this->form->getAction()) ?>" method="<?php echo $this->escape($this->form->getMethod()) ?>" class="global_form classifieds_browse_filters" enctype="multipart/form-data" id="classifieds_create">
  <div>
    <div>
      <h3>
        <?php echo $this->translate($this->form->getTitle()) ?>
      </h3>
    
      <div class="form-elements">
        <?php echo $this->form->getDecorator('FormErrors')->setElement($this->form)->render("");?>
        <?php echo $this->form->title; ?>
        <?php //echo $this->form->tags; ?>
        <?php if($this->form->category_id) echo $this->form->category_id; ?>
        <?php echo $this->form->body; ?>
        <?php if($this->form->skill_id) echo $this->form->skill_id; ?>
        <?php echo $this->form->getSubForm('fields'); ?>
        <?php if($this->form->auth_view)echo $this->form->auth_view; ?>
        <?php if($this->form->auth_comment)echo $this->form->auth_comment; ?>
        <?php 
        
        if($this->classified->script == ''){
        	$style="style='display:none;'";
        }else{
        	$style="style='display:block;'";
        }
        	//echo $this->form->script_title;
            //echo $this->form->script;
		
        if($this->classified->script != ''){
        ?>
        <div class="uploaded_script" <?php echo $style;?>>
            <?php echo $this->form->script_title;?>
            <?php echo $this->form->script;?>
           
            <a href="<?php echo $this->baseUrl().'/public/classified/scripts/'.$this->classified->script;?>" target="_blank" id="script_val">
            <img src="<?php echo $this->baseUrl().'/public/custom/images/view_pdf.png'; ?>" title="View" alt="View"/>
            </a>
            <a href="javascript:void(0);" class="delete_script" >X</a>
        </div>
        <?php }else{
        	echo $this->form->script_title;
            echo $this->form->script;
        } ?>
        <input type="hidden" name="remove_script" class="remove_script" value="0" />
       <div id="buttons-wrapper" class="form-wrapper"><div id="buttons-label" class="form-label">&nbsp;</div><div id="buttons-element" class="form-element">

<button name="execute" id="execute1" type="button" onclick="validatevlassified()">SAVE CHANGES</button>

<a name="cancel" id="cancel" type="button" href="/classifieds/manage" class="wp_init">CANCEL</a></div></div>
      </div>

      <?php echo $this->form->classified_id; ?>
      <?php echo $this->form->execute->render(); ?>
      
    </div>
  </div>
</form>
<?php  if($this->classified->script != ''){?>
<script type="text/javascript">
//jQuery('#script_title-wrapper').css("display", "none");
//jQuery('#script-wrapper').css("display", "none");
</script>
<?php } ?>

<script type="text/javascript">
  $$('.custom_951').getParent().addClass('active');
jQuery(document).on('click','.delete_script', function(){
	//jQuery('.uploaded_script').remove();
	jQuery('.remove_script').val(1);
	//jQuery('#script_title-wrapper').css("display", "block");
	jQuery('#script_title').val("");
	jQuery('#script').val("");
	jQuery('#script_val').remove();
	jQuery('.delete_script').remove();
	//jQuery('#script-wrapper').css("display", "block");
});
 function validatevlassified(){
		
	jQuery(".com_form_error_validatin").removeClass("com_form_error_validatin");
	if(jQuery.trim(jQuery(".field_73").val()) == ''){
      jQuery(".field_73").focus();
      jQuery("#0_0_73-element .SumoSelect > p.CaptionCont.SlectBox").addClass("com_form_error_validatin");      
      return false;
  }
  if(jQuery.trim(jQuery(".field_74").val()) == ''){
      jQuery(".field_74").focus();
      jQuery("#0_0_74-element .SumoSelect > p.CaptionCont.SlectBox").addClass("com_form_error_validatin");      
      return false;
  }
  if(jQuery('input[name="fields[0_0_72]"]:checked').length <= 0)
    {
     jQuery(".field_72").focus();
      jQuery("#0_0_72-element ul > li").addClass("com_form_error_validatin");             
      return false;
    }
  if(jQuery.trim(jQuery("#fields-0_0_61").val()) == ''){
      jQuery("#fields-0_0_61").focus();
      jQuery("#fields-0_0_61").addClass("com_form_error_validatin");
      return false;
  }
  if(jQuery.trim(jQuery("#fields-0_0_14").val()) == ''){
      jQuery("#fields-0_0_14").focus();
      jQuery("#fields-0_0_14").addClass("com_form_error_validatin");
      return false;
  }
  if(jQuery.trim(jQuery("#fields-0_0_41").val()) == ''){
      jQuery("#fields-0_0_41").focus();
      jQuery("#fields-0_0_41").addClass("com_form_error_validatin");
      return false;
  }
  if(jQuery('input[name="fields[0_0_71]"]:checked').length <= 0)
    {
     jQuery(".field_71").focus();
      jQuery("#0_0_71-element ul > li").addClass("com_form_error_validatin");             
      return false;
    }
	 jQuery(document).on('keyup','#fields-0_0_61', function(e) {
         console.log(jQuery(this).val().length);
         var inputString = jQuery(this).val();
         console.log(inputString.length);
        if(inputString.length < 2) {
                jQuery('#autoSuggestionsList').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/experience/index/getallroles";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                   // data = jQuery.parseJSON(data);
                    //data = JSON.parse(data);                    
                    if(data)
                    {
                    	jQuery('#autoSuggestionsList').show();
	                    jQuery('#autoSuggestionsList').addClass('auto_list');
	                    jQuery('#autoSuggestionsList').html(data);
                    }
                    else
                    {
                    	jQuery('#autoSuggestionsList').hide();
                    }
                           
                     });
                 
             }
            
            
    });
      jQuery(document).on('click','#autoSuggestionsList li',function(){ 
            var el=jQuery(this).text();
            var elid=jQuery(this).attr('id');
            console.log("elid"+elid);
           jQuery('#fields_0_0_61_id').val(elid);
           jQuery('#fields-0_0_61').val(el);
           // alert('hello'+el);
          // $('#search-column #submit-search').click();
        setTimeout("jQuery('#autoSuggestionsList').hide();", 200);
       });
      jQuery('#pro_title').keyup(function(e){
      //var len = e.value.length;
      var len = jQuery(this).val().length;
        if (len >= 30) {
          alert("You have reached the max limit of 30 char!");
          //$(this).val() = $(this).val().substring(0, 300);
           e.preventDefault();
          //return false;
        } else {
          console.log(30 - len);
         // $('#charNum').text(300 - len);
        }
    });

       jQuery('#pro_desc').keyup(function(e){
      //var len = e.value.length;
      var len = jQuery(this).val().length;
        if (len >= 70) {
          alert("You have reached the max limit of 70 char!");
          //$(this).val() = $(this).val().substring(0, 300);
           e.preventDefault();
          //return false;
        } else {
          console.log(70 - len);
         // $('#charNum').text(300 - len);
        }
    });

        jQuery('#title').keyup(function(e){
      //var len = e.value.length;
      var len = jQuery(this).val().length;
        if (len >= 40) {
          alert("You have reached the max limit of 40 char!");
          //$(this).val() = $(this).val().substring(0, 300);
           e.preventDefault();
          //return false;
        } else {
          console.log(40 - len);
         // $('#charNum').text(300 - len);
        }
    });
	
	jQuery("#classifieds_create").submit();
	  
   
  }

  jQuery(document).on('keyup','#fields-0_0_14', function(e) {
         var inputString = jQuery(this).val();
        if(inputString.length < 1) {
                jQuery('#autoSuggestionsListl').hide(); // Hide the suggestions box
            } else {
               jQuery.post('<?php echo $this->baseUrl()."/user/edit/getlocations";?>', {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
                      if(data)
                        {
                            jQuery('#autoSuggestionsListl').show();
                            jQuery('#autoSuggestionsListl').addClass('auto_listl');
                            jQuery('#autoSuggestionsListl').html(data);
                        }
                        else
                        {
                            jQuery('#autoSuggestionsListl').hide();
                        }
                     });
             }
    });
      jQuery(document).on('click','#autoSuggestionsListl li',function(){ 
            var el=jQuery(this).text();
           jQuery('#fields-0_0_14').val(el);
        setTimeout("jQuery('#autoSuggestionsListl').hide();", 200);
       });
</script>
</div>
</div>
</div>