<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: view.tpl 9987 2013-03-20 00:58:10Z john $
 * @author     Jung
 */
?>
<style>
 
body#global_page_classified-index-view .login_my_wrapper #global_content .layout_top{border-top:0px;}
a.submit_button {
    background-color: #3FAEE8;
    border-radius: 3px;
    color: #fff;
    padding: 5px 10px;}
  a.submit_button:hover{background-color:#398bcc; text-decoration:none !important;}

  body{
  overflow-x:hidden!important; 
  font-family: 'Lato';
  font-size: 13px;
}
  ._job_results,
._community_ads {
  padding: 0 15px!important;
}
._job_results_count {
    font-size: 18px;
    text-transform: uppercase;
}
._job_results_tab {
    text-align: right;
    font-size: 14px;
}
._job_results_tab ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
._job_results_tab ul li{
  display: inline-block;
}
._job_results_tab ul li a{
  padding: 8px 8px;
  text-decoration: none;
  color: #000;
  border-bottom: 2px solid transparent;
}
._job_results_tab ul li.active a{
  border-color:#f55731; 
}

._job_results_head{
  float:left;
  width: 100%;
  padding-bottom: 8px;
  border-bottom: 1px solid #e1e1e1;
  margin-bottom: 30px;
}
/*._job_results_list_item {
    background: #fff;
    padding: 10px 5px;
}*/

._job_list_item_block {
    background: #fff;
    padding: 10px 5px;
    float: left;
    width: 100%;
    margin-bottom: 30px;
}
._item_block_head{
  float: left;
  width: 100%;
  padding-bottom: 10px;
  padding-left: 10px;
}
._createdby,
.viewedby {
    font-size: 14px;
    color: #888888;
}
._createdby_name{
  color: #000000;
}
._createdby{
  float:left;
}
.viewedby{
  float: right;
  padding-right: 15px;
}
._item_block_content{
  float: left;
  width: 100%;
}
._job_item_img {
    float: left;
    height: 105px;
    width: 105px;
} 
._job_item_content{
  width: calc(100% - 105px);
    float: right;
    padding: 0 9px;
}
._job_title {
    padding-bottom: 10px;
}
._job_title span{
   color: #333;
    font-size: 18px;
 }
 ._job_details {
    font-size: 14px;
    color: #333;
    padding-bottom: 10px;
}
 ._job_details i {
  color: #cacaca;
  padding-right:3px;
 } 
 span._job_loc {
    padding-left: 25px;
}
._job_desc{
  font-size: 14px;
  color: #333;
}
._job_desc_title {
    font-size: 12px;
    color: #888;
    padding-bottom: 5px;
}
._item_block_footer {
    float: left;
    width: 100%;
    padding-left: 10px;
}
._addfolderr_icn{
  width: 18px;
  height: 18px;
  display: inline-block;
  background: url('public/jobs/add_folder_icn.png') no-repeat center center;
  background-size: cover;
  margin-right: 10px;
}
._item_block_footer a {
    float: left;
    width: 18px;
    margin-right: 10px;
    color: #cacaca;
    font-size: 18px;
}
._community_ads_area{
  background: #ffffff;
  border:1px solid #e6e7e8;
}
._community_ads_head {
    padding: 8px 0 3px;
}
._community_ads_head a{
  color: #666666;
  font-size: 14px;
  line-height: 1.4;
}
._community_ads_head a._createads{
  float: right;
  color: #f55731;
}
._community_ads_img{
  margin-bottom: 8px;
}
._community_ads_img img{
  width: 100%;
}
._community_ads_desc{
  font-size: 14px;
  color: #666666;
}
._community_ads_desc span{
  float: left;
  width: 100%;
}
._clear{clear:both;}
._community_ads_desc a{
  color: #666;
}
._community_ads_desc a._community_ads_title{
  color: #000;
  font-weight: bold;
}
._community_ads_items {
    border-bottom: 1px solid #e6e7e8;
    padding-top: 10px;
}
._community_ads_items:last-child{
    border-bottom: none;
    padding-bottom: 0px;
}
#global_content > div > div.generic_layout_container.layout_main > div > div > div._job_results_list_item > div > div._item_block_content > div._job_item_img > img
{
  max-height: 105px;
  max-width: 105px;
}
._job_results_list_item
{
  border-right: 1px solid #e8e8e8;
}
#job_results > div > div > div > div > div._item_block_content > div._job_item_img > img
{
  max-height: 105px;
  max-width: 105px;

}
#job_results > div > div > div > div > div._item_block_content > div._job_item_content > div._job_desc > p
{
  font-size: 14px !important;
  }
  #job_result > li > div > div > div._item_block_content > div._job_item_content > div._job_desc > p
  {
    font-size: 14px !important;
  }

#job_results > form > div.browsemembers_criteria2
{
  padding: 10px;
  background-color: #E9EAED;
  text-align: right;
  border-bottom: 1px solid #ccc;
  margin-bottom: 3%;
}
  #job_results > form > div > ul > li
  {
    display: inline;
    margin-top: 0;
    width: 10% !important;
    padding: 10px;
    margin-right: 10px;
    color: #000;
  }
 #job_results > form > div > ul > li > label > a
 {
  color: #000;
 }
 .jobselect
 {
  color: #fb9236 !important;
  border-bottom: 2px solid;
  border-color: #f55731 !important;
 }
 .browsemembers_criteria
 {
  background: url("public/jobs/banner-img.jpg") no-repeat center center;
    background-size: cover;
    min-height: 200px;
    position: relative;
    /*margin-bottom: 36px;*/
    padding: 0px;
 }
 #global_content > div > div > div.generic_layout_container.layout_user_browse_search > form > div > ul
 {
    padding-top: 6%;
    padding-left: 8%;
 }
 a._posted_action_btn {
    text-decoration: none;
    color: #333;
    text-transform: uppercase;
    border: 1px solid #333;
    width: 100px;
    height: 30px;
    display: inline-block;
    text-align: center;
    line-height: 30px;
    margin-right: 10px;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    -ms-transition: 0.3s ease-in-out;
    -o-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
a._posted_action_btn:hover
    {   
    color: #fa933c;
    border: 1px solid #fa933c;
    }
  #masthead
  {
    margin-top: 8%;
  }
  div.modal-dialog
  {
        padding-top: 5%;
  }
  .modal-footer_des
  {
    background-color: #e6f8ff !important;
    border: 1px solid #98d7f1 !important;
    overflow: hidden;
    margin: 10px 14px !important;
    padding: 10px !important;
  }
  div.modal-header > p
  {
    font-size: 14px;
    color: #777;
    margin-top: 0px;
  }
div.modal-header > h3
{
  font-size: 18px !important;
  color: #5a5a5a !important;
  margin-top: 10px !important;
  margin-bottom: 0px !important;
}
div.modal-header
{
     /*top-right-bottom-left*/
          padding: 0px 0px 0px 10px !important;
          border-bottom: none !important;
}
div.modal-content
{
  display: block !important;
    padding: 0% !important;
    overflow-y: auto;
    margin: 0px;
    padding-top: 15%;
}
div.modal-body
{
     padding: 0px 0px 0px 10px !important;
         margin-top: -15px !important;
}
div.modal-footer
{
  padding: 10px 0px 0px 10px !important;
    text-align: left !important;
    border-top: none !important;
}
div.modal-footer > button:nth-child(1)
{
  background-color: #eb8831 !important;
  border-radius: 5px !important;
  color: #FFFFFF!important;
    padding: 10px 15px !important;
    font-size: 12px !important;
}
div.modal-footer > button:nth-child(2)
{
  background-color: #5a5a5a;
    border-radius: 5px !important;
    color: #fff;
    padding: 10px 12px !important;
    text-transform: capitalize;
    font-size: 12px !important;
}
#popup_box { 
    display:none;
    position:fixed; 
    height:100px;  
    width:300px;  
    background: #fff;  
    left: 20%;
    top: 50%;
    /*margin-left: -100px;
    margin-top: -150px;*/
    z-index:100;     
    padding:15px;  
    font-size:12px;  
    -moz-box-shadow: 0 0 5px;
    -webkit-box-shadow: 0 0 5px;
    box-shadow: 0 0 5px;
    background-color: #dbdbdb;
}
#overlay {
    background:rgba(0,0,0,0.3);
    display:none;
    width:100%; height:100%;
    position:absolute; top:0; left:0; z-index:99998;
}
._applydate
{
  color: #000;
}
/*#job_result > li > div._job_results_list_item
{
  max-height: 300px;
  border-bottom: 1px solid #e8e8e8;
}*/
@media(max-width: 767px){
  #job_result > li > div > div > div._item_block_content > div._job_item_content > div._job_desc > p {
    max-height: none;
          }
    ._job_item_content {
      max-height: none;
    }
  }
  a i:before
  {
    font-size: 24px;
    padding-right: 5px;
  }
  #global_content > div > div.generic_layout_container.layout_main > div > div > div._job_results_list_item > div > div._item_block_head > div._createdby.left > span._createdby_name > a
  {
    color: #000;
  }
</style>

<?php if( !$this->classified): ?>
<?php echo $this->translate('The classified you are looking for does not exist or has been deleted.');?>
<?php return; // Do no render the rest of the script in this mode
endif; ?>

<script type="text/javascript">
  en4.core.runonce.add(function() {
    // Enable links
    $$('.classified_entrylist_entry_body').enableLinks();
  });
</script>

<div class="top_section">
<div class="buttons_section">
<?php if($this->is_owner){?>
<a class="_posted_action_btn" href="<?php echo $this->baseUrl().'/classifieds/manage';?>">My Jobs</a>
<a class="_posted_action_btn" href="<?php echo $this->baseUrl().'/classifieds/create';?>">Post Job</a>
<?php } ?>
</div>
</div>
<?php

       $data  = Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedDataById($this->classified->classified_id);
       $applyby= Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedsapplyBy($this->classified->classified_id);
         foreach($data as $res){
                  
                    if($res['field_id'] == 61){
                      $desig=$res['values'];
                    }
                    if($res['field_id'] == 74){
                      $expe1=$res['values'];
                      if($expe1=='')
                      {
                        $expe="0";
                      }
                      else
                      {
                        $expe= $expe1;
                      }
                    }
                    if($res['field_id'] == 73){                    
                      $lang=$res['values'];
                    }
                
                    if($res['field_id'] == 75){
                      $keyw=$res['values'];
                    }

                    if($res['field_id'] == 41){
                      $dura=$res['values'];
                    }
                    
                    if($res['field_id'] == 71){
                      $pay_term=$res['values'];
                    }
                                 
                    if($res['field_id'] == 14){
                      $work_loc=$res['values'];
                    }

                    if($res['field_id'] == 72){
                      $out_app=$res['values'];
                      if($out_app=='Yes')
                      {
                        $out_app="may";
                      }
                      else
                      {
                        $out_app= "need not";
                      }
                    }
                                                          
                }

                      $ownername='';
                       $jobownerpic = '';
                        $classified = Engine_Api::_()->getItem('classified', $this->classified->classified_id); 
                        if($this->classified->page_id != 0){
                            $companypage  = Engine_Api::_()->getItem('page', $this->classified->page_id);
                            $ownername=$companypage['displayname'];
                            $jobownerpic =Engine_Api::_()->getDbtable('users', 'user')->getUserJobDesigPicPath($desig);
                            $link = $companypage->getHref();
                            $jobownerpic  = ($jobownerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$jobownerpic;
                            $isApply  = Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($this->classified->classified_id, $this->userid); 

                        }else{
                           $user = Engine_Api::_()->getItem('user', $this->classified->owner_id);
                           //echo '<pre>'; print_r($user); 
                           $ownername=$user['displayname'];
                           $link =  $user->getHref();
                           $jobownerpic =Engine_Api::_()->getDbtable('users', 'user')->getUserJobDesigPicPath($desig); 
                           $jobownerpic = ($jobownerpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_profile.png':$jobownerpic;
                          $isApply  = Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($this->classified->classified_id, $this->userid);

                        }
                    //die();
      ?>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 _job_results_list_item">
            <div class="_job_list_item_block">
              <div class="_item_block_head">
                <div class="_createdby left">by <span class="_createdby_name"><?php echo $this->htmlLink($this->classified->getParent(), $this->classified->getParent()->getTitle()); ?>,</span> <span class="_dated"><?php
                echo date('d F Y',strtotime($this->classified->creation_date)); ?></span></div>
                <div class="viewedby right"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo $this->classified->view_count;?></div> 
              </div>
              <div class="_item_block_content">
                <div class="_job_item_img"><img src="<?php echo $jobownerpic;?>"></div>
                <div class="_job_item_content">
                  <div class="_job_title"><span title="<?php echo $this->classified->title; ?>"><?php echo $this->classified->title; ?></span></div>
                  <div class="_job_details">
                    <span class="_job_exp"><i class="fa fa-briefcase" aria-hidden="true"></i> <?php echo $expe ?></span>
                    <span class="_job_loc"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $work_loc ?></span>
                  </div>
                  <div class="_job_desc">
                    <div class="_job_desc_title">Job Description</div>
                    <b><?php echo $desig ?></b> wanted, with <b><?php echo $expe ?></b> of experience, fluent in <b><?php echo $lang ?></b> for a <b><?php echo $dura ?></b> assignment. Payment terms will be on a <b><?php echo $pay_term ?></b> basis. This position is primarily based in <b><?php echo $work_loc ?></b>.  Outstation candidates <b><?php echo $out_app ?></b> apply.</p>
                  </div>
                  <?php if($applyby){?>
                  <div class="_job_desc">
                    <div class="_job_desc_title">Apply By: <span class="_applydate"><?php echo date('d F Y',strtotime($applyby))?></span></div>                    
                  </div>
                  <?php }?>
                      <p class="_job_desc_title">Additional Information: 
            <?php if($this->classified->script != ''){?>
                <a href="<?php echo $this->baseUrl().'/public/classified/scripts/'.$this->classified->script;?>" target="_blank">
                <?php echo $this->classified->script_title;?>
                <img src="<?php echo $this->baseUrl().'/public/custom/images/view_pdf.png'; ?>" title="View" alt="View"/>
                </a>
            </p>     
            <?php }else{
              echo 'NA';
            } ?>
            <!-- <p class="">&nbsp;</p> -->
                </div>

              </div>
                    <!-- <div class="container"> -->
              <div class="_item_block_footer">
                <?php if( $this->userid == $this->classified->owner_id) {?>
            <!-- <a class="add_directory" title="Can't Apply"><i class="fa fa-ban"></i></a> -->
            <a href="/classifieds/delete/<?php echo $this->classified->classified_id ?>/format/smoothbox" class="smoothbox" title="Delete" ><i class="fa fa-trash-o"></i></a>
          
           <?php } else { if ($isApply) { ?>
                <!-- <a id="unapply_<?php echo $job['classified_id'];?>" class="smooth_box add_directory" title="Un Apply" data-index="<?php echo $job['classified_id'];?>"><i class="fa fa-remove"></i></a> -->
                <a class="smoothbox" href="/classifieds/application/applyremove/classified_id/<?php echo $this->classified->classified_id ?>" title="Un Apply"><i class="fa fa-remove" ></i></a>
                <?php } else {?>
                <!-- <a id="apply_<?php echo $job['classified_id'];?>"  class="smooth_box add_directory" title="Apply" data-index="<?php echo $job['classified_id'];?>"><i class="fa fa-check-square"></i></a> -->
                <a class="smoothbox" href="/classifieds/application/applysave/classified_id/<?php echo $this->classified->classified_id ?>" title="Apply"><i class="fa fa-check-square" aria-hidden="true"></i></a>
                <?php }} 
                if( $this->userid == $this->classified->owner_id || $this->canEdit ) {
                 if( !$this->classified->closed ){?>
              <a href="/classifieds/close/<?php echo $this->classified->classified_id ?>/closed/1?return_url=/classifieds/1/3/actor-required" class="smoothbox" title="Close"><i class="fa fa-unlock-alt"></i></a>
           <?php } else { ?>
              <a href="/classifieds/close/<?php echo $this->classified->classified_id ?>/closed/0?return_url=/classifieds/1/3/actor-required" class="smoothbox" title="Open"><i class="fa fa-lock"></i></a>
              <?php } }?>
                <!-- <a data-toggle="modal" data-target="#myModal_<?php echo $job['classified_id'];?>"><i class="fa fa-share-alt" aria-hidden="true" class="_shared_list_item"></i></a>  -->
                <a href="/activity/index/share/type/classified/id/<?php echo $this->classified->classified_id ?>/format/smoothbox" class="smoothbox" title= "Share"><i class="fa fa-share-alt"></i></a> 
          </div>
                
  <!-- Trigger the modal with a button -->
   <!-- Modal -->
  <div class="modal fade" id="myModal_<?php echo $job['classified_id'];?>" role="dialog">
    <div class="modal-dialog modal-sm" >
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
          <div class="form-elements">
                  <div id="body-wrapper" class="form-wrapper">
                  <div id="body-label" class="form-label">&nbsp;</div>
                  <div id="body-element" class="form-element">
                  <textarea name="body" id="share_textarea_<?php echo $job['classified_id'];?>" cols="40" rows="6" maxlength="300"></textarea>
                  </div>
                  </div>
          </div>
        </div>
        
      
      </div>
      
    </div>
  </div>
  
              <!-- </div>  -->           
            </div>            
          </div>

<script type="text/javascript">
  $$('.core_main_classified').getParent().addClass('active');
  $$('.custom_319').getParent().addClass('active');
  $$('.custom_295').getParent().addClass('active');
  $$('.custom_330').getParent().addClass('active');
</script>
