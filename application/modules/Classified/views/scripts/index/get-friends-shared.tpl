<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: create.tpl 10110 2013-10-31 02:04:11Z andres $
 * @author     Jung
 */
?>

 

 <script type="text/javascript" src="<?php echo $this->baseUrl();?>/public/js/jquery.min.js"></script>

<script type="text/javascript">

 function remove_parent(obj)
  {
	   
		  var ajax_url = '<?php echo $this->baseUrl()."/classified/index/remove-share";?>';
 		  var project_id = jQuery('#project_id').val();
		  var data_id = jQuery(obj).attr("data_id");
 		  jQuery.ajax({
				url: ajax_url,
				type: 'POST',
				data: { data_id: data_id, project_id: project_id },
				async: false,
				success: function(s){
					
					var msg= s.split("::");
					if(jQuery.trim(msg[0]) == 'success')
					{
						location.reload();
					  
					}
					else
					{
						jQuery("#signinerror").html(msg[1]);
					}
					
				}
			});
	   
  
  }
  


</script>

<div class="headline">
  <?php 
if(count($this->shared_list)>0)
{
?>
  <h2>Are you sure you want to un share this project?
  </h2>
<?php
}
?> 
</div>
 

<div id="signinerror" style=" display:none; color:#ff0000; position: sticky; left: 293px;"></div>

<form name="share_form" id="share_form">
<input type="hidden" name="project_id" id="project_id" value="<?php echo $this->project_id; ?>" />
<table cellpadding="10" cellspacing="10" style="width:100%;">

<?php 
if(count($this->shared_list)>0)
{
?>
<tr>
  
 <td  > <strong>Name</strong></td>
  <td  > <strong>Action</strong></td>
 </tr>
<?php
foreach($this->shared_list as $key =>$val)
{
 ?> 
 <tr>
  
 <td  > <strong><?php echo $val['displayname'];?></strong></td>
  <td  > 
  <a class="proj_delete" href="javascript:void(0)" onClick="remove_parent(this)" data_id="<?php echo $val['user_id']?>">Delete</a>
   </td>
 </tr>
 <?php
 }
 }else
 {
 ?>
 
 <td colspan="2" align="center" style="text-align:center; padding-top:10%;">Project is not shared with any friends</td>
 
 <?php
 }
 ?>
 <td colspan="2">&nbsp; </td>
 </tr>
  
</table>
</form>
	
	
	

	

 
 <style>
    .black_overlay{
        display: none;
        position: absolute;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index:1001;
        -moz-opacity: 0.8;
        opacity:.80;
        filter: alpha(opacity=80);
    }
    .white_content {
        display: none;
        position: absolute;
        top: 25%;
        left: 25%;
        width: 50%;
        height: 50%;
        padding: 16px;
        border: 16px solid orange;
        background-color: white;
        z-index:1002;
        overflow: auto;
    }
	.tag 
	{
	display: block;
	}
	.proj_delete 
	{
	background-color:#D00000;
	border-radius:5px !important;
	color:#FFFFFF;
	display:inline-block;
	padding:5px 10px;
	}

a:hover 
{ 
   text-decoration:none;
}
</style>

