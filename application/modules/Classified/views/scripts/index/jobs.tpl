<?php 
$this->headScript()
    ->appendFile($this->layout()->staticBaseUrl . 'externals/jquery/jquery-1.10.2.js');
$this->headScript()
    ->appendFile($this->layout()->staticBaseUrl . 'externals/jquery/jquery-ui.min.js');
 $this->headLink()
    ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/custom/custom.css');
?>
<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: create.tpl 10110 2013-10-31 02:04:11Z andres $
 * @author     Jung
 */
?>
<style type="text/css">
th{width:200px;}
.success_msg{ background-color: #3f7500; border-radius: 3px; bottom:5px !important; color: #fff;  font-family: robotoregular;font-size: 15px;height: 35px !important;line-height:13px !important;padding: 10px;text-align: center; vertical-align: top; display:none;width:51% !important; margin-bottom:10px; float:left;left:120px !important;   margin-top:10px !important;}
   .black_overlay{
        display: none;
        position: absolute;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index:1001;
        -moz-opacity: 0.6;
        opacity:.80;
        filter: alpha(opacity=80);
    }
    .white_content {
        display: none;
        position: absolute;
        top: 25%;
        left: 25%;
        width: 50%;
        height: 50%;
        padding: 16px;
        border: 8px solid #444;
        background-color: white;
        z-index:1002;
        overflow: auto;
    }

  ._manage_pages_section, ._community_ads, .member_list_section {
    padding: 0 15px!important;
}
._manage_pages_head, .member_list_head{
  float: left;
    width: 100%;
    padding-bottom: 8px;
    border-bottom: 1px solid #e1e1e1;
    margin-bottom: 30px;
}
._manage_pages_head_content, .member_list_head_content {
    font-size: 18px;
    text-transform: uppercase;
}
._manage_pages_list_item_area{
  float: left;
  width: 100%;
  background: #fff;
}
.padnone{
  padding: 0px;
}
._manage_pages_list_item_block {
    background: #fff;
    padding: 15px 15px;
    float: left;
    border-bottom: 1px solid #e5e5e5;
}
._manage_pages_list_item_block:last-of-type, ._manage_pages_list_item_block:nth-last-of-type(2) {
    border-bottom: none;
}
._manage_pages_list_item_block:nth-child(even) {
    border-left: 1px solid #e5e5e5;
}

._item_block_content {
    float: left;
    width: 100%;
}
._manage_pages_item_img {
    float: left;
    height: 105px;
    max-width: 100px;
}
._manage_pages_item_img img{
  width: 100%;
}
._manage_pages_item_content {
    width: calc(100% - 105px);
    float: right;
    padding: 0 9px;
}
  
._manage_pages_title a {
    color: #333;
    font-size: 18px;
}
._manage_pages_date{
  font-size: 14px;
  color: #999999;
}
._manage_pages_details {
    font-size: 14px;
    color: #999;
}
._manage_pages_desc {
    font-size: 14px;
    color: #333;
}
._item_block_footer {
    float: left;
    width: 100%;
}
._manage_pages_action {
  float: left;
  width: 100%;
    margin-top: 30px;
}
._manage_pages_action a._manage_pages_action_btn {
    text-decoration: none;
    color: #333;
    text-transform: uppercase;
    border: 1px solid #333;
    width: 85px;
    height: 30px;
    display: inline-block;
    text-align: center;
    line-height: 30px;
    margin-right: 10px;
    -webkit-transition: 0.3s ease-in-out;
    -moz-transition: 0.3s ease-in-out;
    -ms-transition: 0.3s ease-in-out;
    -o-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}
._manage_pages_action a._manage_pages_action_btn:last-child{margin-right: 0px;}
._manage_pages_action a._manage_pages_action_btn:hover {
    color: #fa933c;
    border: 1px solid #fa933c;
}
.member_list_item_area{
  background: #fff;
  padding:5px 5px 8px;
  float: left;
  width: 100%;
  min-height: 350px;
  max-height: 350px;
}

._member_list_item_title a {
    color: #333;
    font-size: 18px;
}
._member_list_item_subtitle{
  font-size: 14px;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
  word-wrap: ;
  white-space: nowrap;
}
._member_list_item_action{
  float: left;
  width: 100%;
}
._member_list_item_action a{
  text-decoration: none;
  color: #cacaca; 
}
._member_list_item_review {
  font-size:14px;
  color: #ffc100; 
  float: left;
  width: 100%;
  padding-top: 10px;
}
._member_list_item_review .fa-star{
  cursor: pointer;
}
.padrgtnone{padding-right: 0px; padding-left: 0px; float: left; color: #cacaca; text-align: left;}
.padlftnone{padding-left: 0px; text-align: right; float: right; }
._addfolderr_icn {
    width: 12px;
    height: 12px;
    display: inline-block;
    background: url(public/custom/images/add_folder_icn.png) no-repeat center center;
    background-size: cover;
    margin-right: 0;
    margin-left: 0px;
}
._addfolderr_icn2 {
    width: 12px;
    height: 12px;
    display: inline-block;
    background: url(public/custom/images/add_folder_icn2.png) no-repeat center center;
    background-size: cover;
    margin-right: 0;
    margin-left: 0px;
}
.member_list_item {
    margin-bottom: 30px;
    padding-left: 0px;
}
.member_list_search{
  float: left;
  width: 100%;
  padding-bottom: 20px;
}
.member_list_search input.search-input{
  border-radius: 0;
  box-shadow: none;
  margin-bottom: 10px;
}
.member_list_search input.search-submit{
  background: #ff9e20 !important;
  color: #fff !important;
  border-radius: 0;
  box-shadow: none;
  -webkit-transition: 0.5s ease-in-out;
  -moz-transition: 0.5s ease-in-out;
  -ms-transition: 0.5s ease-in-out;
  -o-transition: 0.5s ease-in-out;
  transition: 0.5s ease-in-out;
}
.member_list_search input.search-submit:hover{
  background: #1f1f1f;
}
._banner_section{
  background: url('img/banner-img.jpg') no-repeat center center;
  background-size: cover;
  min-height: 200px;  
  position: relative;
  margin-bottom: 36px;
  padding: 0px;
} 
._form_area{
  position: absolute;
  height: 40px;
  top: 50%;
  width: 100%;
  margin:-20px 0 0;
}
.browsemembers_criteria ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
.browsemembers_criteria ul li{
  display: inline-block;  
  min-width: 200px;
  max-width: 200px;
  margin-right: 28px;
  margin-bottom: 11px;
}
._job_results,
._community_ads {
  padding: 0 15px!important;
  float: right;
}
._job_results_count {
    font-size: 18px;
    text-transform: uppercase;
}
._job_results_tab {
    text-align: right;
    font-size: 14px;
}
._job_results_tab ul{
  margin:0px;
  padding:0px;
  list-style: none;
}
._job_results_tab ul li{
  display: inline-block;
}
._job_results_tab ul li a{
  padding: 8px 8px;
  text-decoration: none;
  color: #000;
  border-bottom: 2px solid transparent;
}
._job_results_tab ul li.active a{
  border-color:#f55731; 
}

._job_results_head{
  float:left;
  width: 100%;
  padding-bottom: 8px;
  border-bottom: 1px solid #e1e1e1;
  margin-bottom: 30px;
}
/*._job_results_list_item {
    background: #fff;
    padding: 10px 5px;
}*/

._job_list_item_block {
    background: #fff;
    padding: 10px 5px;
    float: left;
    width: 100%;
    margin-bottom: 30px;
}
._item_block_head{
  float: left;
  width: 100%;
  padding-bottom: 10px;
}
._createdby,
.viewedby {
    font-size: 14px;
    color: #888888;
}
._createdby_name{
  color: #000000;
}
._createdby{
  float:left;
}
.viewedby{
  float: right;
}
._item_block_content{
  float: left;
  width: 100%;
}
._job_item_img {
    float: left;
    height: 105px;
} 
._job_item_content{
  width: calc(100% - 105px);
    float: right;
    padding: 0 9px;
}
._job_title {
    padding-bottom: 10px;
}
._job_title a{
   color: #333;
    font-size: 18px;
 }
 ._job_details {
    font-size: 14px;
    color: #333;
    padding-bottom: 10px;
}
 ._job_details i {
  color: #cacaca;
  padding-right:3px;
 } 
 span._job_loc {
    padding-left: 25px;
}
._job_desc{
  font-size: 14px;
  color: #333;
}
._job_desc_title {
    font-size: 12px;
    color: #888;
}
._item_block_footer {
    float: left;
    width: 100%;
}
/*._addfolderr_icn{
  width: 18px;
  height: 18px;
  display: inline-block;
  background: url('img/add_folder_icn.png') no-repeat center center;
  background-size: cover;
  margin-right: 10px;
}*/
._item_block_footer a {
    float: left;
    width: 18px;
    margin-right: 10px;
    color: #cacaca;
    font-size: 18px;
}
._community_ads_area{
  background: #ffffff;
  border:1px solid #e6e7e8;
}
._community_ads_head {
    padding: 8px 0 3px;
}
._community_ads_head a{
  color: #666666;
  font-size: 14px;
  line-height: 1.4;
}
._community_ads_head a._createads{
  float: right;
  color: #f55731;
}
._community_ads_img{
  margin-bottom: 8px;
}
._community_ads_img img{
  width: 100%;
}
._community_ads_desc{
  font-size: 14px;
  color: #666666;
}
._community_ads_desc span{
  float: left;
  width: 100%;
}
._clear{clear:both;}
._community_ads_desc a{
  color: #666;
}
._community_ads_desc a._community_ads_title{
  color: #000;
  font-weight: bold;
}
._community_ads_items {
    border-bottom: 1px solid #e6e7e8;
    padding-top: 10px;
}
._community_ads_items:last-child{
    border-bottom: none;
    padding-bottom: 0px;
}

@media(max-width: 767px){
  ._community_ads
  {
    padding-left: 0px !important;
    padding-right: 0px !important;
  }
  .member_list_search {
    margin-left: -15px;
    width: 105% !important;
  }
  #tog_button > img
  {
    display: none;
  }
  #core_menu_mini_menu_mobile
  {
    display: none;
  }
  .browsemembers_criteria ul li {
      display: inline-block;
      min-width: 145px;
      max-width: 145px;
      margin-right: 18px;
      margin-bottom: 11px;
  }
  ._form_area {
      margin: -70px 0 0;
  }
  ._job_results_count,
  ._job_results_tab {
      font-size: 12px;
  }
}
.generic_layout_container .layout_middle
{
  background-color: #e8e8e8;
  width: 100% !important;
}
form > input.search-input.form-control
/*#displayname*/
{
  color: #555;  
  background-color: #f2f2f2 !important;
}

form > input.form-control
{
  display: block;
    width: 100%;
    height: 40px !important;
    padding: 6px 12px;
    font-size: 14px !important;
    line-height: 1.42857143;
    background-image: none;
    border: 1px solid #ccc !important;
}

.img-responsive
{  
  width: 100%;
  max-height: 245px;
  object-fit: cover;
  min-height: 245px;
}
#browsemembers_results > div > div > div._member_list_section > div.member_list._manage_pages_list > div > div > div > div._member_list_item_detials > div > span
{
  float: right;
}
div._member_list_section
{
  padding-left: 0px;
}
._member_list_head_content
{
  border-bottom: 1px solid #ccc;
    width: 100%;
    margin-bottom: 10px;
    padding-left: 0px;
}
._member_list_head
{
  text-transform:uppercase;
  /*text-transform: uppercase;
    border-bottom: 1px solid #ccc;
    width: 100%;
    margin-bottom: 10px;*/
}
.generic_layout_container .layout_left
{
  display: none;
}
#browsemembers_results > div > div > div._community_ads
{
 float: right;
}
.core_mini_profile span img.thumb_icon
{
  margin-right: 0px;
}
#jobs
{   
  width: 100%;
  height: 40px;
  padding-left: 5px;
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  margin-bottom: 10px;
}
#actions
{
  width: 100%;
  height: 40px;
  padding-left: 5px;
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
   margin-bottom: 10px;
}
#global_content > div > div.generic_layout_container.layout_main > div > div > div > div.col-lg-9.col-md-9.col-sm-8.col-xs-12._member_list_section > div.member_list._manage_pages_list > div > div > div > div._member_list_item_detials > div > span
{
 float: right;
}
#global_content > div > div.generic_layout_container.layout_main > div > div > div > div._member_list_section > div.member_list._manage_pages_list > div > div > div > div._member_list_item_detials > div > div > a
{
 margin-right: 5px;
}
</style>
<?php
  if (APPLICATION_ENV == 'production')
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.min.js');
  else
    $this->headScript()
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Observer.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Local.js')
      ->appendFile($this->layout()->staticBaseUrl . 'externals/autocompleter/Autocompleter.Request.js');
?>

<div class="layout_page_classified_index_jobs">
<div class="generic_layout_container layout_top">
<!-- <div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_user_browse_menu">
  <div class="headline">
  <h2>View Applicants Job Wise </h2>
  </div>
</div>
</div> -->
</div>

<div class="generic_layout_container layout_main">
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layout_core_content">
  <div class="container">
  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 _community_ads">
      <div class="member_list_search">
 <form id="jobapplicants" name="jobapplicants" method="post" action="classifieds/jobs?parent_id=<?php echo $this->parent_id;?>">
  <div class="form-element jobs_list">
  <select class="field_container jobs" id="jobs" name="jobs" >
  <option  value="">Select Job</option>
  <?php 
   if($this->jobs){
      foreach($this->jobs as $val){?>
        <option  value="<?php echo $val['classified_id'];?>"<?php echo ($val['classified_id'] ==$this->classified_id ?"selected='selected'":""); ?>><?php echo $val['title'];?></option>
      <?php }
   }?>
</select>
<select class="field_container jobs" id="actions" name="actions">
  <option  value="">Select Action</option>
  <option  value="accept"<?php echo ("accept" ==$this->action ?"selected='selected'":""); ?>>Selected</option>
  <option  value="hold"<?php echo ("hold" ==$this->action ?"selected='selected'":""); ?>>Shortlisted</option>
  <option  value="reject"<?php echo ("reject" ==$this->action ?"selected='selected'":""); ?>>Rejected</option>
  
</select>
<input type="submit" name="getactions" id="getactions" class="search-submit form-control" value="Select">
</div>
</form>
</div>
</div>
  <?php  $app = $this->reslist; 
  //echo '<pre>';print_r($app);echo '</pre>';
?>
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 _member_list_section">
      <div class="_member_list_head">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 _member_list_head_content">View Applicants Job Wise- (<?php echo count($app);?>)</div>
        </div>
      </div>
      <div class=" member_list _manage_pages_list">
        <div class="row">
        <?php if($app){ foreach($app as $key=>$val){ 
        $viewer = Engine_Api::_()->user()->getUser($val->user_id);
        //echo '<pre>';print_r($viewer); die();
        $userpic  = Engine_Api::_()->getDbtable('users', 'user')->getUserProfilePicPath($val->user_id);
        $userpic  = ($userpic=='')?'application/modules/User/externals/images/nophoto_user_thumb_normal.png':$userpic;
        $userating=Engine_Api::_()->getDbtable('reviews', 'user')->getAVGuserrating($viewer->user_id); 
        ?>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 member_list_item">
            <div class="member_list_item_area">
             <div class="col-md-6 col-sm-6 col-xs-6 padlftnone">
                  <i class="fa fa-eye" aria-hidden="true"></i> <?php echo $viewer->view_count;?>
                </div>  
            <img src="<?php echo $userpic;?>" class="img-responsive">
            <div class="_member_list_item_title"><a href="profile/<?php echo $viewer->user_id;?>" title=""><?php echo $viewer->displayname;?></a></div>
            <div class="_member_list_item_subtitle"><?php echo $userdesig=Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategoriesSearch($viewer->user_id); 
            ?></div>
            <div class="_member_list_item_detials">               
              <div class="_member_list_item_review">
                
                <div class="col-md-6 col-sm-6 col-xs-6 text-right padrgtnone">
                 <?php if($val->reject == 1){ ?>
                 <a title="Rejected" ><i class="fa fa-times-circle" style="color: #f14848;" aria-hidden="true"></i></a>
                 <?php } else {?>
                 <a data-job-id="<?php echo $val->classified_id;?>" data-user-id="<?php echo $val->user_id;?>" title="Reject" id="app_reject" ><i class="fa fa-times-circle" style="color: #cacaca;" aria-hidden="true"></i></a>
                 <?php }?>
                  <?php if($val->accept == 1){ ?>
                 <a  title="Accepted" ><i class="fa fa-check-circle" style="color: #2dd525;" aria-hidden="true"></i></a>
                 <?php } else {?>
                 <a data-job-id="<?php echo $val->classified_id;?>" data-user-id="<?php echo $val->user_id;?>" title="Accept" id="app_accept" ><i class="fa fa-check-circle" style="color: #cacaca;" aria-hidden="true"></i></a>
                 <?php }?>  
                  <?php if($val->hold == 1){?>                  
                  <a  title="Shortlisted"><i class="_addfolderr_icn2" aria-hidden="true"></i></a>
                  <?php } else {?>
                  <a data-job-id="<?php echo $val->classified_id;?>" data-user-id="<?php echo $val->user_id;?>" id="app_hold" title="Shortlist"><i class="_addfolderr_icn" aria-hidden="true"></i></a>                
                   <?php }?> 
                </div>
                <span>
                <?php for($x=1;$x<=$userating;$x++) {?>
                          <i class="fa fa-star" aria-hidden="true"></i>
                <?php } 
                if ((int) $userating != $userating) {  ?>
                         <i class="fa fa-star-half" aria-hidden="true"></i>
                 <?php }
                     if($x !=1) { while ($x<=5) { ?>
                          <i class="fa fa-star-o" aria-hidden="true"></i>
                  <?php $x++; } }?>
                  <!-- <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i> -->
                </span>
              </div>
            
            </div>
            </div>
          </div>    
          <?php }}?>
        </div>
      </div>
    </div>
  
  </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

  //$$('.core_main_classified').getParent().addClass('active');
  jQuery('.custom_923').parent().addClass('active');
  jQuery('.classified_main_manage').parent().removeClass('active');
   $$('.custom_320').getParent().addClass('active');
    $$('.custom_331').getParent().addClass('active');
	 jQuery(document).ready(function(){
		
	});

   jQuery(document).on('click', '#getactions',function(){
    if(jQuery('#jobs').val()=='')
    {
      alert('Please select job!');
      jQuery('#jobs').focus();
      return false;
    }
      if(jQuery('#actions').val()=='')
    {
      alert('Please select actions!');
      jQuery('#actions').focus();
      return false;
    }
   });

  jQuery(document).on('click', '#app_accept',function(){
    var classified_id = jQuery(this).data('job-id');
    var user_id = jQuery(this).data('user-id');
    //alert('classified_id'+classified_id + "user_id"+user_id);
    //return false;
    jQuery('.loading_image').show();
    formURL = "classifieds/application/accept/classified_id/" + classified_id;
      jQuery.ajax({
          url: formURL,
          type: "POST",
          data: "cid=" + classified_id+"&user_id="+user_id,
          dataType: 'json',
          success: function(resp) {
              alert(resp);
              jQuery('.loading_image').hide();
              location.reload();
              
          }
          
      })
  });

  jQuery(document).on('click', '#app_reject',function(){
    var classified_id = jQuery(this).data('job-id');
    var user_id = jQuery(this).data('user-id');
    jQuery('.loading_image').show();
    formURL = "classifieds/application/reject/classified_id/" + classified_id;
      jQuery.ajax({
          url: formURL,
          type: "POST",
          data: "cid=" + classified_id+"&user_id="+user_id,
          dataType: 'json',
          success: function(resp) {
              alert(resp);
              jQuery('.loading_image').hide();
              location.reload();
          }
          
      })
  });

  jQuery(document).on('click', '#app_hold',function(){
    var classified_id = jQuery(this).data('job-id');
    var user_id = jQuery(this).data('user-id');
    jQuery('.loading_image').show();
    formURL = "classifieds/application/hold/classified_id/" + classified_id;
      jQuery.ajax({
          url: formURL,
          type: "POST",
          data: "cid=" + classified_id+"&user_id="+user_id,
          dataType: 'json',
          success: function(resp) {
              alert(resp);
              jQuery('.loading_image').hide();
              location.reload();
          }
          
      })
  });
</script>
