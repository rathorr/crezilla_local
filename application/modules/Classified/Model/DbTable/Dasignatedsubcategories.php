<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Categories.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Model_DbTable_Dasignatedsubcategories extends Engine_Db_Table
{
  protected $_rowClass = 'Classified_Model_Dasignatedsubcategories';
  
  public function getdscById($dsc_id){
  	/*echo $dc_id;
  	die();*/
	  $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_dasignatedsubcategories', array( 'dasignatedsubcategory_name'))
        ->where('engine4_classified_dasignatedsubcategories.dasignatedsubcategory_id = ?', $dsc_id)
        ->query();
  
    return $result	=	$stmt->fetchAll();
 }
 public function getdesigall(){
   
    $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_dasignatedsubcategories')
        ->where('engine4_classified_dasignatedsubcategories.dasignatedcategory_id !=?', 12)
        ->query();
  
    return $result  = $stmt->fetchAll();
 }
 public function getsearchByname($sugg){
   
    $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_dasignatedsubcategories', array( 'dasignatedsubcategory_id','dasignatedsubcategory_name'))
        ->where('engine4_classified_dasignatedsubcategories.dasignatedsubcategory_name LIKE ?', '%'.$sugg.'%')
        ->where('engine4_classified_dasignatedsubcategories.dasignatedcategory_id !=?', 12)
        ->query();
  
    return $result  = $stmt->fetchAll();
 }
/* public function getsubdesigIDByname($name){
   
    $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_dasignatedsubcategories', array( 'dasignatedsubcategory_id'))
        ->where('engine4_classified_dasignatedsubcategories.dasignatedsubcategory_name = ?', $name)
        ->query();
  
    return $result  = $stmt->fetchRow();
 }*/

 public function getsubdesigIDByname($name){
  //echo $name;
    $select = $this->select()
         ->where("TRIM(dasignatedsubcategory_name) = ?", trim($name));
     $result  = $this->fetchRow($select);   
   return $result;
 }
 public function getsubcatjob()
 {

  $stmt = $this->select()
        ->from($this, array('dasignatedsubcategory_id', 'dasignatedsubcategory_name'))
        ->order('dasignatedsubcategory_name ASC')
        ->query();
    
  /*  $data = array();
  $data[] = 'Select Designation';
    foreach( $stmt->fetchAll() as $desig ) {
      $data[$desig['dasignatedsubcategory_id']] = $desig['dasignatedsubcategory_name'];
    }*/

    return $data=$stmt->fetchAll();
 }

 public function getcastingDesignations()
 {
   $stmt = $this->select()
        ->from($this, array('dasignatedsubcategory_id', 'dasignatedsubcategory_name'))
        ->where('engine4_classified_dasignatedsubcategories.dasignatedcategory_id = ?', 1)
        ->order('dasignatedsubcategory_name ASC')
        ->query();
  
    return $result  = $stmt->fetchAll();
 }

  public function getserviceDesignations()
 {
   $stmt = $this->select()
        ->from($this, array('dasignatedsubcategory_id', 'dasignatedsubcategory_name'))
        ->where("engine4_classified_dasignatedsubcategories.dasignatedcategory_id NOT IN ('1','12')")
        ->order('dasignatedsubcategory_name ASC')
        ->query();
   return $result  = $stmt->fetchAll();
 }

  public function getsuppliersDesignations()
 {
   $stmt = $this->select()
        ->from($this, array('dasignatedsubcategory_id', 'dasignatedsubcategory_name'))
        ->where('engine4_classified_dasignatedsubcategories.dasignatedcategory_id = ?', 12)
        ->order('dasignatedsubcategory_name ASC')
        ->query();
  
    return $result  = $stmt->fetchAll();
 }

public function getsearchedDesignations($id)
 {
  $arr=array();
  $where='';
    if($id==1)
    {
      $where='engine4_classified_dasignatedsubcategories.dasignatedcategory_id = 1';
    }elseif ($id==2) {
     $where="engine4_classified_dasignatedsubcategories.dasignatedcategory_id NOT IN ('1','12')";
    }elseif ($id==3) {
      $where='engine4_classified_dasignatedsubcategories.dasignatedcategory_id = 12';
    }
   $stmt = $this->select()
        ->from($this, array('dasignatedsubcategory_id', 'dasignatedsubcategory_name'))
        ->where($where)
        ->order('dasignatedsubcategory_name ASC')
        ->query();
  
     $result  = $stmt->fetchAll();
    foreach ($result as $key => $value) {
      $arr[$key]= $value['dasignatedsubcategory_id'];
    }
    $vals=implode(',', $arr);
    return $vals;
   // die();
 }
}