<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Albums.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Model_DbTable_Applys extends Engine_Db_Table
{
  protected $_rowClass = 'Classified_Model_Apply';
  
   /* IS APPLIED FOR JOB */
  public function isAlreadyApplied($job_id, $viewer_id){
	  
	 $select = $this->select()
			->where('user_id = ?', $viewer_id)
			->where('classified_id = ?', $job_id)
			;
	
	return (count($select->query()->fetchAll())>0)?true:false;
 }
 
 public function applyWithUpload($user_id,$classified_id,$file_name)
	{
		
		$curtime=date("Y-m-d h:i:s");
		   $query ="insert into engine4_classified_uploads   
				 set user_id=".$user_id.",
				 classified_id='".$classified_id."',
				 file_name='".$file_name."',
				 post_time='".$curtime."' ";
	 	
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$stmt = $db->query($query);
		 
	}
	
}