<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Categories.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Model_DbTable_Parenttypes extends Engine_Db_Table
{
  protected $_rowClass = 'Classified_Model_Parenttype';
  

  
    public function getParentAssoc($user)
	  {
	  
		$data = array();
		$data[] = 'Create New Project';
		/*
		if( $user instanceof User_Model_User ) {
		  $user = $user->getIdentity();
		} 
			$stmt = $this->select()
				->from('engine4_classified_parenttypes', array('parent_id', 'parent_name'))
				->where('engine4_classified_parenttypes.user_id = ?', $user)
				->order('parent_name ASC')
				->query();
		   
			*/
			
			$projects_list = $this->getListAssoc($user);
			if(count($projects_list)>0)
			{
				foreach( $projects_list as $parent ) {
				  $data[$parent['parent_id']] = stripslashes($parent['parent_name']);
				}
			}
	
		return $data;
	  }
  

    public function getListAssoc($user)
	  {
	  if( $user instanceof User_Model_User ) {
		  $user = $user->getIdentity();
		  
		  $query ="select up.user_id as shared_with,up.shared_by,up.permission, up.project_id, cp.parent_name, cp.parent_desc, cp.parent_id, cp.user_id as owner_id, up.user_id from engine4_classified_parenttypes as cp inner join engine4_user_projects as up on cp.parent_id=up.project_id where up.user_id='".$user."'";
				
				
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
		
		
		} 
		
	  }
	  
	  public function chkjobpermission($user_id, $proj_id){
		 $query ="select permission from engine4_user_projects where user_id='".$user_id."' and project_id='".$proj_id."'";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		$stmt = $db->query($query);
        $result = $stmt->fetchAll();
		if(count($result)>0){
			$result = $result[0];
			return ($result['permission']==2)? true:false;	
		}else{
			return false;
		}
	  }
	  
	  public function getProject($id)
	  {
			$query ="select cp.parent_name, cp.parent_id, cp.user_id from engine4_classified_parenttypes as cp  where parent_id='".$id."'";
				
				
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result[0];
		
		
		
		
	  }
	  
	  
	  public function getPermission($classified_id, $user_id)
	  {
		$file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
		
		 $query = "select up.permission from engine4_classified_classifieds as cc inner join engine4_user_projects as up on up.project_id = cc.project_id where classified_id='".$classified_id."' and up.user_id='".$user_id."'"; 
		 $stmt = $db->query($query);
         $result = $stmt->fetchAll();
		 $permission =  $result[0]['permission'];
		 return $permission;
		
	  }
  		
  	public function gettotaljobs($user_id)
  	{
  		$select	=	$this->select()
			->from ( array('t' => 'engine4_user_projects')) 
			->setIntegrityCheck(false)     
			->joinLeft ( array ('o' => 'engine4_classified_classifieds'), 'o.project_id = t.project_id', array( 'o.classified_id'))
			->where('t.user_id = ?', $user_id)
			->where("o.classified_id !=''")
			;
		$result = $this->fetchAll($select);
		return $result = count($result);
  	}
  	public function gettotaljobsProjectwise($user_id)
  	{
  		echo '<pre>';
  		$select	=	$this->select()
			->from ( array('t' => 'engine4_user_projects')) 
			->setIntegrityCheck(false)     
			->joinLeft ( array ('o' => 'engine4_classified_classifieds'), 'o.project_id = t.project_id', array( 'COUNT(o.classified_id)'))
			->where('t.user_id = ?', $user_id)
			->group('o.project_id')
			;
		echo ($select);
		$result = $this->fetchAll($select);
		print_r($result);
		die();
		return $result = count($result);
		//die();
  	}
  
  }