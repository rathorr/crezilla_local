<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Categories.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Model_DbTable_Dasignatedcategories extends Engine_Db_Table
{
  protected $_rowClass = 'Classified_Model_Dasignatedcategories';
  
  public function getdcById($dc_id){
  	/*echo $dc_id;
  	die();*/
	  $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_dasignatedcategories', array( 'dasignatedcategory_name'))
        ->where('engine4_classified_dasignatedcategories.dasignatedcategory_id = ?', $dc_id)
        ->query();
  
    return $result	=	$stmt->fetchAll();
 }
}