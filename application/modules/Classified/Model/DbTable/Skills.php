<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Categories.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Model_DbTable_Skills extends Engine_Db_Table
{
  protected $_rowClass = 'Classified_Model_Skill';
  
  public function getskillsAssoc()
  {
    $stmt = $this->select()
        ->from($this, array('skill_id', 'skill_name', 'type'))
        ->order('skill_name ASC')
        ->query();
    
    $data = array();
	$data[] = 'Select Skill';
    foreach( $stmt->fetchAll() as $category ) {
      $data[$category['skill_id']] = $category['skill_name'];
    }
    
    return $data;
  }
  
  public function getUserSkillsAssoc($user)
  {
    if( $user instanceof User_Model_User ) {
      $user = $user->getIdentity();
    } else if( !is_numeric($user) ) {
      return array();
    }
    
    $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_skills', array('skill_id', 'skill_name', 'type'))
        ->joinLeft('engine4_classified_classifieds', "engine4_classified_classifieds.skill_id = engine4_classified_skills.skill_id")
        ->group("engine4_classified_skills.skill_id")
        ->where('engine4_classified_classifieds.owner_id = ?', $user)
        ->where('engine4_classified_classifieds.draft = ?', "0")
        ->order('skill_name ASC')
        ->query();
    
    $data = array();
    foreach( $stmt->fetchAll() as $category ) {
      $data[$category['skill_id']] = $category['skill_name'];
    }
    
    return $data;
  }
  
  public function getJobSkillById($skill_id){
	  $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_skills', array( 'skill_name'))
        ->where('engine4_classified_skills.skill_id = ?', $skill_id)
        ->query();
  
    $result	=	$stmt->fetchAll();
	return $result[0]['skill_name'];
  }
  
  public function getSkillById($skill_id){
	  $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_skills', array( 'type', 'skill_name', 'skill_id'))
        ->where('engine4_classified_skills.skill_id = ?', $skill_id)
        ->query();
  
     $result	=	$stmt->fetchAll();
     return $result;
 }
 public function getskillsjob()
 {

  $stmt = $this->select()
        ->from($this, array('skill_id', 'skill_name'))
        ->order('skill_name ASC')
        ->query();

    return $data=$stmt->fetchAll();
 }
}