<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Categories.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Model_DbTable_Functionalareas extends Engine_Db_Table
{
  protected $_rowClass = 'Classified_Model_Functionalareas';
  
  public function getCategoriesAssoc()
  {
    $stmt = $this->select()
        ->from($this, array('functionalareas_id', 'functionalareas_name'))
        ->order('functionalareasfunctionalareas_name ASC')
        ->query();
    
    $data = array();
	$data[] = 'Select Currentstatus';
    foreach( $stmt->fetchAll() as $currentstatus ) {
      $data[$currentstatus['currentstatus_id']] = $currentstatus['currentstatus_name'];
    }
    
    return $data;
  }
  
  public function getUserCategoriesAssoc($user)
  {
    if( $user instanceof User_Model_User ) {
      $user = $user->getIdentity();
    } else if( !is_numeric($user) ) {
      return array();
    }
    
    $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_currentstatus', array('currentstatus_id', 'currentstatus_name'))
        ->joinLeft('engine4_classified_classifieds', "engine4_classified_classifieds.currentstatus_id = engine4_classified_currentstatus.currentstatus_id")
        ->group("engine4_classified_categories.currentstatus_id")
        ->where('engine4_classified_classifieds.owner_id = ?', $user)
        ->where('engine4_classified_classifieds.draft = ?', "0")
        ->order('currentstatus_name ASC')
        ->query();
    
    $data = array();
    foreach( $stmt->fetchAll() as $ccurrentstatus ) {
      $data[$currentstatus['currentstatus_id']] = $currentstatus['currentstatus_name'];
    }
    
    return $data;
  }
  
  public function getJobCurrentstatusById($cat_id){
	  $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_currentstatus', array( 'currentstatus_name'))
        ->where('engine4_classified_currentstatus.currentstatus_id = ?', $cat_id)
        ->query();
  
    $result	=	$stmt->fetchAll();
	return $result[0]['currentstatus_name'];
  }
  
  public function getfaById($fa_id){
	  $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_functionalareas', array( 'functionalareas_name'))
        ->where('engine4_classified_functionalareas.functionalareas_id = ?', $fa_id)
        ->query();
  
    return $result	=	$stmt->fetchAll();
 }
}