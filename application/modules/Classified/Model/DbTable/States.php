<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Categories.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Model_DbTable_States extends Engine_Db_Table
{
  protected $_rowClass = 'Classified_Model_States';
  
public function getstatesByname($sugg){
   
    $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_states', array('state as location'))
        ->where('engine4_classified_states.state LIKE ?', $sugg.'%')
        ->query();
  
    return $result  = $stmt->fetchAll();
 }
   public function getstatename($state){
    $select = $this->select("state")
               ->setIntegrityCheck(false)
              ->where('state=?', $state);
     $result  = $this->fetchRow($select)->state;
     $result  =($result ? true : false);
     return $result;   
 }

public function insertState($state)
{
	$this->insert(array(
          'state' => $state,
        ));
	return $this;
}

}