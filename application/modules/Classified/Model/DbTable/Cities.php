<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Categories.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Model_DbTable_Cities extends Engine_Db_Table
{
  protected $_rowClass = 'Classified_Model_Cities';
  
public function getcityByname($sugg){
   
    $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_cities', array('city_name as location'))
        ->where('engine4_classified_cities.city_name LIKE ?', $sugg.'%')
        ->query();
  
    return $result  = $stmt->fetchAll();
 }
  public function getcityname($city){
    $select = $this->select("city_name")
               ->setIntegrityCheck(false)
              ->where('city_name=?', $city);
     $result  = $this->fetchRow($select)->city_name;
     $result  =($result ? true : false);
     return $result;   
 }

public function insertCity($city,$state)
{
	$this->insert(array(
          'city_name' => $city,
          'city_state' => $state,
        ));
	return $this;
}

}