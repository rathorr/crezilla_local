<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Classifieds.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Model_DbTable_Classifieds extends Engine_Db_Table
{
  protected $_rowClass = "Classified_Model_Classified";

  /**
   * Gets a paginator for classifieds
   *
   * @param Core_Model_Item_Abstract $user The user to get the messages for
   * @return Zend_Paginator
   */
  public function getClassifiedsPaginator($params = array(),
      $customParams = null)
  {
    $paginator = Zend_Paginator::factory($this->getClassifiedsSelect($params, $customParams));
    if( !empty($params['page']) ) {
      $paginator->setCurrentPageNumber($params['page']);
    }
    if( !empty($params['limit']) ) {
      $paginator->setItemCountPerPage($params['limit']);
    }
    return $paginator;
  }

  /**
   * Gets a select object for the user's classified entries
   *
   * @param Core_Model_Item_Abstract $user The user to get the messages for
   * @return Zend_Db_Table_Select
   */
  public function getClassifiedsSelect($params = array(), $customParams = null)
  {
    $tableName = $this->info('name');
    
    $tagMapsTable = Engine_Api::_()->getDbtable('TagMaps', 'core');
    $tagMapsTableName = $tagMapsTable->info('name');

    $searchTable = Engine_Api::_()->fields()->getTable('classified', 'search');
    $searchTableName = $searchTable->info('name');

    $select = $this->select()
        ->from($this)
        ->order(!empty($params['orderby']) ? $tableName . '.' . $params['orderby'] . ' DESC'
                  : $tableName . '.creation_date DESC' );

    if( isset($customParams) ) {
      $select = $select
          ->joinLeft($searchTableName, "$searchTableName.item_id = $tableName.classified_id", null);

      $searchParts = Engine_Api::_()->fields()->getSearchQuery('classified', $customParams);
      foreach( $searchParts as $k => $v ) {
        $select->where("`{$searchTableName}`.{$k}", $v);
      }
    }

    if( !empty($params['user_id']) && is_numeric($params['user_id']) ) {
      $select->where($tableName . '.owner_id = ?', $params['user_id']);
    }

    if( !empty($params['user']) && $params['user'] instanceof User_Model_User ) {
      $select->where($tableName . '.owner_id = ?', $params['user_id']->getIdentity());
    }

    if( !empty($params['users']) ) {
      $str = (string) ( is_array($params['users']) ? "'" . join("', '",
                  $params['users']) . "'" : $params['users'] );
      $select->where($tableName . '.owner_id in (?)', new Zend_Db_Expr($str));
    }

    if( !empty($params['tag']) ) {
      $select
          ->joinLeft($tagMapsTableName, "$tagMapsTableName.resource_id = $tableName.classified_id", null)
          ->where($tagMapsTableName . '.resource_type = ?', 'classified')
          ->where($tagMapsTableName . '.tag_id = ?', $params['tag']);
    }

    if( !empty($params['category']) ) {
      $select->where($tableName . '.category_id = ?', $params['category']);
    }

	 if( !empty($params['project']) ) {
      $select->where($tableName . '.project_id = ?', $params['project']);
    }

	
    if( isset($params['closed']) && $params['closed'] != "" ) {
      $select->where($tableName . '.closed = ?', $params['closed']);
    }

    // Could we use the search indexer for this?
    if( !empty($params['search']) ) {
      $select->where($tableName . ".title LIKE ? OR " . $tableName . ".body LIKE ?",
          '%' . $params['search'] . '%');
    }

    if( !empty($params['start_date']) ) {
      $select->where($tableName . ".creation_date > ?",
          date('Y-m-d', $params['start_date']));
    }

    if( !empty($params['end_date']) ) {
      $select->where($tableName . ".creation_date < ?",
          date('Y-m-d', $params['end_date']));
    }

    if( !empty($params['has_photo']) ) {
      $select->where($tableName . ".photo_id > ?", 0);
    }
   
    return $select;
  }
  
  public function getClassifiedDataById($classified_id){
	$select	=	$this->select()
			->from ( array('t' => 'engine4_classified_fields_values')) 
			->setIntegrityCheck(false)     
			->joinLeft ( array ('o' => 'engine4_classified_fields_options'), 't.value = o.option_id', array('label as f_value'))
			->joinLeft ( array ('f' => 'engine4_classified_fields_meta'), 'f.field_id = t.field_id', array('label as f_title'))   
			->where('t.item_id = ?', $classified_id)
			->order('t.field_id asc')
			;
		$result = $select->query()->fetchAll();
    //echo '<pre>';print_r($result);die();
		if($result){
			$arr	=	array();
			$i=0;
			foreach($result as $res){
					$arr['field_id']	=	$res['field_id'];
					$arr['title']	=	$res['f_title'];
					if($res['field_id']	==	'71' || $res['field_id']  ==  '73' || $res['field_id']  ==  '74' || $res['field_id']  ==  '72'){
						$arr['values']	=	$res['f_value'];	
					}
          else{
						$arr['values']	=	$res['value'];
					}
					
					if($res['field_id'] == $field_id){
						$arr['values']	.= ', '.$result[$i-1]['f_value'];	
							
					}
					if($final_arr[$i-1]['field_id'] == $res['field_id']){
						unset($final_arr[$i-1]);
					}
					$final_arr[]	=	$arr;
					$field_id	=	$res['field_id'];
			$i++;
			}	
		}
	return $final_arr;
  }
  
  /* GET CLASSIFIED JOB LOCATION */
  public function getClassifiedLocationById($classified_id){
	$select	=	$this->select()
			->from ( array('t' => 'engine4_classified_fields_values')) 
			->setIntegrityCheck(false)     
			->joinLeft ( array ('o' => 'engine4_classified_fields_options'), 't.value = o.option_id', array('label as f_value'))
			->joinLeft ( array ('f' => 'engine4_classified_fields_meta'), 'f.field_id = t.field_id', array('label as f_title'))   
			->where('t.item_id = ?', $classified_id)
			->where('t.field_id = ?', '14')
			->order('t.field_id desc')
			;
		$result = $select->query()->fetchAll();
		if($result){
			$result	=	$result[0];
			return $result['value'];
		}
  }
  
  /* GET CLASSIFIED POSTING FOR */
  public function getClassifiedPostingForById($classified_id){
	  $select	=	$this->select()
			->from ( array('t' => 'engine4_classified_fields_values')) 
			->setIntegrityCheck(false)     
			->joinLeft ( array ('o' => 'engine4_classified_fields_options'), 't.value = o.option_id', array('label as f_value'))
			->joinLeft ( array ('f' => 'engine4_classified_fields_meta'), 'f.field_id = t.field_id', array('label as f_title'))   
			->where('t.item_id = ?', $classified_id)
			->where('t.field_id = ?', '26')
			->order('t.field_id desc')
			;
		$result = $select->query()->fetchAll();
		if($result){
			$result	=	$result[0];
			return ($result['f_value'])?$result['f_value']:'-';
		}
  }
  
   public function getClassifiedUserPortfolio($user_id,$classified_id)
  {
		  $query ="select * from engine4_classified_uploads   where  user_id='".$user_id."' and classified_id  =".$classified_id."";
						
	    $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
		
		 $stmt = $db->query($query);
        $result = $stmt->fetchAll();
		return $result;
		
  }
  
  public function getJobsByPage($page_id)
  {
    
    $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_classifieds', array('classified_id',))
        ->joinLeft('engine4_page_pages', "engine4_page_pages.page_id = engine4_classified_classifieds.page_id")
        ->where('engine4_classified_classifieds.page_id = ?', $page_id)
        ->query();
    
    $data = array();
    foreach( $stmt->fetchAll() as $category ) {
      $data[] = $category['classified_id'];
    }
    
    return $data;
  }

   
  
 public function getClassifiedskillById($classified_id){
    $select = $this->select()
      ->from ( array('t' => 'engine4_classified_fields_values')) 
      ->setIntegrityCheck(false)     
      ->joinLeft ( array ('o' => 'engine4_classified_fields_options'), 't.value = o.option_id', array('label as f_value'))
      ->joinLeft ( array ('f' => 'engine4_classified_fields_meta'), 'f.field_id = t.field_id', array('label as f_title'))   
      ->where('t.item_id = ?', $classified_id)
      ->where('t.field_id = ?', '76')
      ->order('t.field_id desc')
      ;
    $result = $select->query()->fetchAll();
    if($result){
      $array=array();
      foreach ($result as $key => $value) {
        $array[$key]=$value['f_value'];
      }
      /* echo '<pre>';
       print_r($array);
      die();*/
      return $result = $array;
    }
  }

  public function getjobsList($parent_id)
  {
    $stmt = $this->getAdapter()
        ->select()
        ->from('engine4_classified_classifieds', array( 'classified_id','title'))
        ->where('engine4_classified_classifieds.project_id = ?', $parent_id)
        ->query();
  
    return $result  = $stmt->fetchAll();
  }
  public function gettotaljobsProjectwise($project_id)
    {
      //echo $project_id;die("header");
      $select = $this->select()
      ->from ('engine4_classified_classifieds')
      ->where('project_id = ?', $project_id)
      ;
    $result = $this->fetchAll($select);
    return $result = count($result);
    //die();
    }
     public function getjobIDsList($project_id)
    {
      $select = $this->select()
      ->from ('engine4_classified_classifieds', array( 'classified_id'))
      ->where('project_id = ?', $project_id);
     // echo $select;
    $result = $this->fetchAll($select);
    $options = array();
        if($result){
          foreach($result as $key=>$ids){
            $options[$key] = $ids['classified_id']; 
          }
        }
      //print_r($udcoptions);
      $jobids=implode(',', $options);
 
    //echo '<pre>'; print_r($jobids);
    return $jobids;
    //die();
    }
    public function getjobNamebyId($classified_id)
    {
      $select = $this->select()
      ->from ('engine4_classified_classifieds', array( 'title'))
      ->where('classified_id = ?', $classified_id);
     // echo $select;
    $result = $this->fetchRow($select)->title;
    return $result;
    
    }
  public function getClassifiedsapplyBy($classified_id){
   /* $select = $this->select('t.value')
      ->from ( array('t' => 'engine4_classified_fields_values'),array('t.value')) 
      ->setIntegrityCheck(false) 
      ->where('t.item_id = ?', $classified_id)
      ->where('t.field_id = ?', '77')
      ;
      echo $select;
    $result = $select->query()->fetchAll();
     echo '<pre>';
       print_r($result );
      die();*/
      $query ="select value from engine4_classified_fields_values where item_id='".$classified_id."' and field_id  =77";
            
      $file = APPLICATION_PATH . '/application/settings/database.php';
        $options = include $file;
        $db = Zend_Db::factory($options['adapter'], $options['params']);
        $select = new Zend_Db_Select($db);
        $db->getConnection();
        //end DB setup
    
     $stmt = $db->fetchCol($query);
       
    return $stmt[0];
   
  }

  public function getAlljobsViews()
    {
      $select = $this->select()
      ->from ('engine4_classified_classifieds')      
      ->order('view_count DESC')
      ->limit(6);
    $result = $this->fetchAll($select);
    return $result;
    //die();
    }
    public function getPostedjobsViewWise($ownerid)
    {
      $select = $this->select()
      ->where('owner_id =?',$ownerid)      
      ->order('view_count DESC')
      ->limit(2);
    $result = $this->fetchAll($select);
    return $result;
    //die();
    }
}
