<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: IndexController.php 9893 2013-02-14 00:00:53Z shaun $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_IndexController extends Core_Controller_Action_Standard
{
  public function init()
  {
    //if( !$this->_helper->requireAuth()->setAuthParams('classified', null, 'view')->isValid() ) return;
	
	/*$user_id = $this->_getParam('user_id', null);
	if( $user_id && !Engine_Api::_()->core()->hasSubject() )
    {
      $user = Engine_Api::_()->getItem('user', $user_id);
      if( $user )
      {
        Engine_Api::_()->core()->setSubject($user);
      }
    }
	*/
  }
  
  
  // NONE USER SPECIFIC METHODS
  public function indexAction()
  {

    // Check auth
    if( !$this->_helper->requireAuth()->setAuthParams('classified', null, 'view')->isValid() ) return;
    
    $viewer = Engine_Api::_()->user()->getViewer();
    $this->view->can_create = $this->_helper->requireAuth()->setAuthParams('classified', null, 'create')->checkRequire();
    
    
    // Prepare form
    $this->view->form = $form = new Classified_Form_Search();
    
    if( !$viewer->getIdentity() ) {
      $form->removeElement('show');
    }

    // Populate form
    $categories = Engine_Api::_()->getDbtable('categories', 'classified')->getCategoriesAssoc();
    if( !empty($categories) && is_array($categories) && $form->getElement('category') ) {
      $form->getElement('category')->addMultiOptions($categories);
    }

    // Process form
    if( $form->isValid($this->_getAllParams()) ) {
      $values = $form->getValues();
    } else {
      $values = array();
    }
    $this->view->formValues = array_filter($values);

    
    $customFieldValues = array_intersect_key($values, $form->getFieldElements());
    
    // Process options
    $tmp = array();
    foreach( $customFieldValues as $k => $v ) {
      if( null == $v || '' == $v || (is_array($v) && count(array_filter($v)) == 0) ) {
        continue;
      } else if( false !== strpos($k, '_field_') ) {
        list($null, $field) = explode('_field_', $k);
        $tmp['field_' . $field] = $v;
      } else if( false !== strpos($k, '_alias_') ) {
        list($null, $alias) = explode('_alias_', $k);
        $tmp[$alias] = $v;
      } else {
        $tmp[$k] = $v;
      }
    }
    $customFieldValues = $tmp;
    
    // Do the show thingy
    if( @$values['show'] == 2 ) {
      // Get an array of friend ids to pass to getClassifiedsPaginator
      $table = Engine_Api::_()->getItemTable('user');
      $select = $viewer->membership()->getMembersSelect('user_id');
      $friends = $table->fetchAll($select);
      // Get stuff
      $ids = array();
      foreach( $friends as $friend )
      {
        $ids[] = $friend->user_id;
      }
      //unset($values['show']);
      $values['users'] = $ids;
    }

    // check to see if request is for specific user's listings
    if( ($user_id = $this->_getParam('user_id')) ) {
      $values['user_id'] = $user_id;
    }

    $this->view->assign($values);

    // items needed to show what is being filtered in browse page
    if( !empty($values['tag']) ) {
      $this->view->tag_text = Engine_Api::_()->getItem('core_tag', $values['tag'])->text;
    }
    
    $view = $this->view;
    $view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');

    $paginator = Engine_Api::_()->getItemTable('classified')->getClassifiedsPaginator($values, $customFieldValues);
    $items_count = (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('classified.page', 10);
    $paginator->setItemCountPerPage($items_count);
    $this->view->paginator = $paginator->setCurrentPageNumber( $this->_getParam('page',1) );

    if( !empty($values['category']) ) {
      $this->view->categoryObject = Engine_Api::_()->getDbtable('categories', 'classified')
          ->find($values['category'])->current();
    }

    // Render
    $this->_helper->content
        //->setNoRender()
        ->setEnabled()
        ;
  }

  public function viewAction()
  {
    //echo  "classified_id".$this->_getParam('classified_id'); die();
  
    $viewer = Engine_Api::_()->user()->getViewer();    
    $this->view->userid = $viewer->getIdentity();
    $classified = Engine_Api::_()->getItem('classified', $this->_getParam('classified_id'));
	//echo '<pre>'; print_r($classified); die;
	
    if( $classified )
    {
      Engine_Api::_()->core()->setSubject($classified);
    }

    // Check auth
    /*if( !$this->_helper->requireAuth()->setAuthParams($classified, null, 'view')->isValid() ) {
      return;
    }
*/
    $this->view->canEdit = $canEdit = $classified->authorization()->isAllowed(null, 'edit');
    $this->view->canDelete = $canDelete = $classified->authorization()->isAllowed(null, 'delete');
    $this->view->canUpload = $canUpload = $classified->authorization()->isAllowed(null, 'photo');

    // Get navigation
    $this->view->gutterNavigation = $gutterNavigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('classified_gutter');
    
    if( $classified ) {
      $this->view->owner = $owner = Engine_Api::_()->getItem('user', $classified->owner_id);
      $this->view->viewer = $viewer;

      if( !$owner->isSelf($viewer) ) {
        $classified->view_count++;
        $classified->save();
		$this->view->is_owner = FALSE;
      }else{
		$this->view->is_owner = TRUE;  
	  }

      $this->view->classified = $classified;
      if( $classified->photo_id ) {
        $this->view->main_photo = $classified->getPhoto($classified->photo_id);
      }
      
      // get tags
      $this->view->classifiedTags = $classified->tags()->getTagMaps();
      $this->view->userTags = $classified->tags()->getTagsByTagger($classified->getOwner());

      // get custom field values
      //$this->view->fieldsByAlias = Engine_Api::_()->fields()->getFieldsValuesByAlias($classified);
      // Load fields view helpers
      $view = $this->view;
      $view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
      $this->view->fieldStructure = $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($classified);

      // album material
      $this->view->album = $album = $classified->getSingletonAlbum();
      $this->view->paginator = $paginator = $album->getCollectiblesPaginator();
      $paginator->setCurrentPageNumber($this->_getParam('page', 1));
      $paginator->setItemCountPerPage(100);
	  
      if( $classified->category_id ) {
        $this->view->categoryObject = Engine_Api::_()->getDbtable('categories', 'classified')
            ->find($classified->category_id)->current();
      }
    }
	
	// CHK I HAVE APPLIED FOR THIS ALBUM OR NOT
		$this->view->isApply	=	Engine_Api::_()->getDbtable('applys', 'classified')
            ->isAlreadyApplied($classified->classified_id, $viewer->getIdentity()); 
    // Render
    $this->_helper->content
        //->setNoRender()
        ->setEnabled()
        ;
  }

  // USER SPECIFIC METHODS
  public function manageAction()
  {
  
    if( !$this->_helper->requireUser()->isValid() ) return;
    
	$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
	
	 if(strtolower($profile_type) == 'producer'){
		 $menu_type	=	'custom_31';
		 if( isset($is_company->value) && $is_company->value !="" )
		{
			//echo $is_company->value;
			$menu_type	=	'custom_34'; 
		}
	 }else{
		$menu_type	=	'user_edit'; 
	 }
	$this->view->navigation = Engine_Api::_()
       ->getApi('menus', 'core')
       ->getNavigation('user_edit');
	   
	
    // Render
    $this->_helper->content
        //->setNoRender()
        ->setEnabled()
        ;

    $viewer = Engine_Api::_()->user()->getViewer();
	
	//echo '<pre>';print_r($viewer);echo '</pre>';die;

    $this->view->can_create = $this->_helper->requireAuth()->setAuthParams('classified', null, 'create')->checkRequire();
    $this->view->allowed_upload = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'classified', 'photo');
    
    $this->view->form = $form = new Classified_Form_Search();
    $form->removeElement('show');

    // Populate form
    $categories = Engine_Api::_()->getDbtable('categories', 'classified')->getCategoriesAssoc();
    if( !empty($categories) && is_array($categories) && $form->getElement('category') ) {
      $form->getElement('category')->addMultiOptions($categories);
    }
    
    // Process form
    if( $form->isValid($this->getRequest()->getPost()) ) {
      $values = $form->getValues();
    } else {
      $values = array();
    }
	
    //$customFieldValues = $form->getSubForm('custom')->getValues();
	// $values['user_id'] = $viewer->getIdentity();
	
	
		$viewer = Engine_Api::_()->user()->getViewer();
		$this->view->user_id = $viewer->getIdentity();
		$projects = $categories = Engine_Api::_()->getDbtable('parenttypes', 'classified')->getListAssoc($viewer);
	
	$posted_jobs = array();
	$i = 0;
	if(count($projects) > 0)
	{
		foreach($projects as $project)
		{
		
			$values['project'] 						= $project['project_id']; 
			$posted_jobs[$i]['project_name'] 		= stripslashes($project['parent_name']);
			$posted_jobs[$i]['project_desc'] 		= $project['parent_desc'];
			$posted_jobs[$i]['project_id'] 			= $project['parent_id'];
			$posted_jobs[$i]['permission'] 			= $project['permission'];
			
			$posted_jobs[$i]['owner_id'] 			= $project['owner_id'];
			$posted_jobs[$i]['user_id'] 			= $project['user_id'];
			
			
			// custom field search
			$customFieldValues = array_intersect_key($values, $form->getFieldElements());
			// Get paginator
			$this->view->paginator = $paginator = Engine_Api::_()->getItemTable('classified')->getClassifiedsPaginator($values, $customFieldValues);
			$items_count = (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('classified.page', 10);
			$paginator->setItemCountPerPage($items_count);
			//$this->view->paginator = $paginator->setCurrentPageNumber( $this->_getParam('page',1) );
			$posted_jobs[$i]['project_jobs'] = $paginator->setCurrentPageNumber( $this->_getParam('page',1) );
			$view = $this->view;
			$view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');

			// maximum allowed classifieds
			//$this->view->quota = $quota = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'classified', 'max');
		//	$this->view->current_count = $paginator->getTotalItemCount();
			$i++;
		}
  }
    $this->view->totaljobs=Engine_Api::_()->getDbtable('parenttypes', 'classified')->gettotaljobs($viewer->getIdentity());
    //$this->view->totaljobsprojectwise=Engine_Api::_()->getDbtable('classifieds', 'classified')->gettotaljobsProjectwise();
		$this->view->projects = $posted_jobs;
    
		
	
  }

  public function createAction()
  {
    // Check auth
    if( !$this->_helper->requireUser()->isValid() ) return;
    if( !$this->_helper->requireAuth()->setAuthParams('classified', null, 'create')->isValid()) return;

    // Render
    $this->_helper->content
        //->setNoRender()
        ->setEnabled()
        ;
    
    $this->view->form = $form = new Classified_Form_Create();
    
    // set up data needed to check quota
    $viewer = Engine_Api::_()->user()->getViewer();
    $values['user_id'] = $viewer->getIdentity();
    $paginator = Engine_Api::_()->getItemTable('classified')->getClassifiedsPaginator($values);
    
    $this->view->quota = $quota = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'classified', 'max');
    $this->view->current_count = $paginator->getTotalItemCount();

    // If not post or form not valid, return
    if( !$this->getRequest()->isPost() ) {
      return;
    }

    if( !$form->isValid($this->getRequest()->getPost()) ) {
     // print_r($this->getRequest()->getPost()); die();
      return;
    }

//echo '<pre>'; print_r($values); echo '</pre>'; die;
    // Process
    $table = Engine_Api::_()->getItemTable('classified');
    $db = $table->getAdapter();
    $db->beginTransaction();

    try
    {
      // Create classified
      

	//echo '<pre>'; print_r($values); echo '</pre>'; 
	//echo $form->Filedata->getValue();die;
	
		
      $classified = $table->createRow();

	  //$classified->skill_id  = implode(',', $values['skill_id']);
	  // UPLOAD SCRIPT
	  $script_path_name	=	'';
	  if(!empty($_FILES['script']['tmp_name'])){
		 	$file_extension = trim(strtolower(end(explode('.', $_FILES['script']['name']))));
			$script_path_name	=	uniqid().'.'.$file_extension;
			//$classified->script	=	$script_path_name;
			$classified->script_title	=	$values['script_title'];
			move_uploaded_file($_FILES['script']['tmp_name'], APPLICATION_PATH_PUB.'/classified/scripts/'. $script_path_name);
			
	  }
	  
	  
	  
	  
	  $values = $form->getValues();
	  $values = array_merge($values, array(
        'owner_type' => $viewer->getType(),
        'owner_id' => $viewer->getIdentity(),
      ));
	  
	  if($values['pro_title'] !=''){
		  $protable 		= Engine_Api::_()->getDbtable('parenttypes', 'classified');
	 	  $data = array(
			'parent_name' => $values['pro_title'],
			'parent_desc'=>	$values['pro_desc'],
			'user_id' => $viewer->getIdentity());
			$qry = $protable->insert($data);  /******** Insert new event in db *********/
			$projectID = Engine_Db_Table::getDefaultAdapter()->lastInsertId();
			$values['project_id']  = 	$projectID;
			
			if($projectID > 0)
			{
				$pdata = array( 'user_id' => $viewer->getIdentity(),
						'project_id' =>$projectID,
						'permission' => 2,
						'shared_by' => $viewer->getIdentity());
					$user_friends 	= Engine_Api::_()->getDbtable('friendship', 'user')->setProjectsList($pdata);	
			}
	  }
	  
	  //$values['script_path_name']	=	$script_path_name;
	  $classified->setFromArray($values);
	  $classified->script	=	$script_path_name;
    $classified->category_id=$values['fields_0_0_61_id'];
    //print_r($classified['skill_id']);
    // print_r($classified->skill_id);
     $skilids= implode(',', $classified['skill_id']);
	  //echo '<pre>'; print_r($classified); echo '</pre>'; die;
    $classified->skill_id= $skilids;
    $classified->save();
		//die;
	  // ADD ACTIVITY ON PAGE FOR NEW JOB POST 
	  if($values['page_id']!=''){
		  $page	=	Engine_Api::_()->getItem('page', $values['page_id']);
		  $activityApi = Engine_Api::_()->getDbtable('actions', 'activity');
			$action = $activityApi->addActivity($viewer, $page, 'page_job_post');
			if ($action) {
			  $activityApi->attachActivity($action, $classified);
			}
	  }
			
      // Set photo
      if( !empty($values['photo']) ) {
        $classified->setPhoto($form->photo);
      }

      // Add tags
      //$tags = preg_split('/[,]+/', $values['tags']);
     // $tags = array_filter(array_map("trim", $tags));
      //$classified->tags()->addTagMaps($viewer, $tags);

      // Add fields
      $customfieldform = $form->getSubForm('fields');
     
      $customfieldform->setItem($classified);
      $customfieldform->saveValues();

      // Set privacy
      $auth = Engine_Api::_()->authorization()->context;
      $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');

      if( empty($values['auth_view']) ) {
        $values['auth_view'] = array("everyone");
      }
      if( empty($values['auth_comment']) ) {
        $values['auth_comment'] = array("everyone");
      }

      $viewMax = array_search($values['auth_view'], $roles);
      $commentMax = array_search($values['auth_comment'], $roles);

      foreach( $roles as $i => $role ) {
        $auth->setAllowed($classified, $role, 'view',    ($i <= $viewMax));
        $auth->setAllowed($classified, $role, 'comment', ($i <= $commentMax));
      }

      // Commit
      $db->commit();
    }

    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }

    $db->beginTransaction();
    try {
      $action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer, $classified, 'classified_new');
      if( $action != null ) {
        Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $classified);
      }
      $db->commit();
    }

    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }
    
    // Redirect
    $allowed_upload = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'classified', 'photo');
    if( $allowed_upload ) {
      return $this->_helper->redirector->gotoRoute(array('action' => 'manage'), 'classified_general', true);
    } else {
      //return $this->_helper->redirector->gotoUrl($classified->getHref(), array('prependBase' => false));
	  return $this->_helper->redirector->gotoRoute(array('action' => 'manage'), 'classified_general', true);
    }
  }

  public function editAction()
  {
    if( !$this->_helper->requireUser()->isValid() ) return;

    $viewer = Engine_Api::_()->user()->getViewer();
    $classified = Engine_Api::_()->getItem('classified', $this->_getParam('classified_id'));
    if( !Engine_Api::_()->core()->hasSubject('classified') ) {
      Engine_Api::_()->core()->setSubject($classified);
    }
    $this->view->classified = $classified;
    
    // Check auth
    if( !$this->_helper->requireSubject()->isValid() ) {
      return;
    }
    $project_per='no';
    $categories_permission='';
    $categories = Engine_Api::_()->getDbtable('parenttypes', 'classified')->getListAssoc($viewer);
    foreach ($categories as $value) {
      if($value['project_id'] == $classified->project_id)
      {
        $project_per='yes';
        $categories_permission=$value['permission'];
      }
    }
    /*echo '<pre>';
    echo $viewer->getIdentity();
    echo $this->_getParam('classified_id');
    print_r($categories['project_id']);
    print_r($classified->project_id);
    //die();
    echo $classified->project_id."--".$project_per;
    die();*/
    if( $categories_permission == 2  && $project_per =='yes') { }
    else
    {
        if( !$this->_helper->requireAuth()->setAuthParams($classified, $viewer, 'edit')->isValid() ) {
          return;
          }
    }
    
    // Get navigation
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('classified_main', array(), 'classified_main_manage');


    // Prepare form
    $this->view->form = $form = new Classified_Form_Edit(array(
      'item' => $classified
    ));
    
    $form->removeElement('photo');
    
    /*
    if( isset($classified->photo_id) && 
        $classified->photo_id != 0 &&
        !$classified->getPhoto($classified->photo_id) ) {
      $classified->addPhoto($classified->photo_id);
    }
    */
    
    $this->view->album = $album = $classified->getSingletonAlbum();
    $this->view->paginator = $paginator = $album->getCollectiblesPaginator();
    
    $paginator->setCurrentPageNumber($this->_getParam('page'));
    $paginator->setItemCountPerPage(100);
    
    foreach( $paginator as $photo ) {
      $subform = new Classified_Form_Photo_Edit(array('elementsBelongTo' => $photo->getGuid()));
      $subform->removeElement('title');

      $subform->populate($photo->toArray());
      $form->addSubForm($subform, $photo->getGuid());
      $form->cover->addMultiOption($photo->getIdentity(), $photo->getIdentity());
    }
    
    // Save classified entry
    $saved = $this->_getParam('saved');
    if( !$this->getRequest()->isPost() || $saved ) {

      if( $saved ) {
        $url = $this->_helper->url->url(array('user_id' => $viewer->getIdentity(), 'classified_id' => $classified->getIdentity()), 'classified_entry_view');
        $savedChangesNotice = Zend_Registry::get('Zend_Translate')->_("Your changes were saved. Click %s to view your listing.",'<a href="'.$url.'">here</a>');
        $form->addNotice($savedChangesNotice);
      }

      // prepare tags
      $classifiedTags = $classified->tags()->getTagMaps();
      //$form->getSubForm('custom')->saveValues();
      
      $tagString = '';
      foreach( $classifiedTags as $tagmap )
      {
        if( $tagString !== '' ) $tagString .= ', ';
        $tagString .= $tagmap->getTag()->getTitle();
      }

      $this->view->tagNamePrepared = $tagString;

      // etc
      $form->populate($classified->toArray());
      $auth = Engine_Api::_()->authorization()->context;
      $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
      foreach( $roles as $role )
      {
        if( $form->auth_view && 1 === $auth->isAllowed($classified, $role, 'view') )
        {
          $form->auth_view->setValue($role);
        }
        if( $form->auth_comment && 1 === $auth->isAllowed($classified, $role, 'comment') )
        {
          $form->auth_comment->setValue($role);
        }
      }

      return;
    }
    
    if( !$form->isValid($this->getRequest()->getPost()) ) {
      return;
    }

    // Process

    // handle save for tags
    

    $db = Engine_Db_Table::getDefaultAdapter();
    $db->beginTransaction();
    
    try {
		
	  if(!empty($_FILES['script']['tmp_name'])){
				$file_extension 	= trim(strtolower(end(explode('.', $_FILES['script']['name']))));
				$script_path_name	=	uniqid().'.'.$file_extension;
				move_uploaded_file($_FILES['script']['tmp_name'], APPLICATION_PATH_PUB.'/classified/scripts/'. $script_path_name);
				$script =	$script_path_name;
		  	    //$values['script_title']	=	$script_title;
	  }else{
		   $script = '';
		   
	  }
	  $values = $form->getValues();
	  if($script == ''){
		  unset($values['script_title']);
	  	  unset($values['script']);
	  }else{
			$values['script']	=	$script;  
	  }
      $tags = preg_split('/[,]+/', $values['tags']);
      $tags = array_filter(array_map("trim", $tags));
	  if($_POST['remove_script'] == 1 && empty($_FILES['script']['tmp_name'])){
			$values['script']	=	"";
			$values['script_title']	=	"";
	  }
      
      $classified->setFromArray($values);
      $classified->modified_date = date('Y-m-d H:i:s');
      $skilids= implode(',', $classified['skill_id']);
      //echo '<pre>'; print_r($classified); echo '</pre>'; die;
      $classified->skill_id= $skilids;
      $classified->tags()->setTagMaps($viewer, $tags);
      $classified->save();

      $cover = $values['cover'];

      // Process
      foreach( $paginator as $photo ) {
        $subform = $form->getSubForm($photo->getGuid());
        $subValues = $subform->getValues();
        $subValues = $subValues[$photo->getGuid()];
        unset($subValues['photo_id']);

        if( isset($cover) && $cover == $photo->photo_id) {
          $classified->photo_id = $photo->file_id;
          $classified->save();
        }

        if( isset($subValues['delete']) && $subValues['delete'] == '1' ) {
          if( $classified->photo_id == $photo->file_id ) {
            $classified->photo_id = 0;
            $classified->save();
          }
          $photo->delete();
        } else {
          $photo->setFromArray($subValues);
          $photo->save();
        }
      }

      // Save custom fields
      $customfieldform = $form->getSubForm('fields');
      $customfieldform->setItem($classified);
      $customfieldform->saveValues();

      // CREATE AUTH STUFF HERE
      $auth = Engine_Api::_()->authorization()->context;
      $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
      if( !empty($values['auth_view']) ) {
        $auth_view = $values['auth_view'];
      } else {
        $auth_view = "everyone";
      }
      $viewMax = array_search($auth_view, $roles);

      foreach( $roles as $i=>$role )
      {
        $auth->setAllowed($classified, $role, 'view', ($i <= $viewMax));
      }

      $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
      if( !empty($values['auth_comment']) ) {
        $auth_comment =$values['auth_comment'];
      } else {
        $auth_comment = "everyone";
      }
      $commentMax = array_search($auth_comment, $roles);
      
      foreach ($roles as $i=>$role)
      {
        $auth->setAllowed($classified, $role, 'comment', ($i <= $commentMax));
      }

      $db->commit();

    }
    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }


    $db->beginTransaction();
    try {
      // Rebuild privacy
      $actionTable = Engine_Api::_()->getDbtable('actions', 'activity');
      foreach( $actionTable->getActionsByObject($classified) as $action ) {
        $actionTable->resetActivityBindings($action);
      }

      $db->commit();
    }
    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }

    return $this->_helper->redirector->gotoRoute(array('action' => 'manage'), 'classified_general', true);
  }

  public function deleteAction()
  {
    $viewer = Engine_Api::_()->user()->getViewer();
    $classified = Engine_Api::_()->getItem('classified', $this->getRequest()->getParam('classified_id'));
    if( !$this->_helper->requireAuth()->setAuthParams($classified, null, 'delete')->isValid()) return;

    // In smoothbox
    $this->_helper->layout->setLayout('default-simple');
    
    $this->view->form = $form = new Classified_Form_Delete();

    if( !$classified )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_("Classified listing doesn't exist or not authorized to delete");
      return;
    }

    if( !$this->getRequest()->isPost() )
    {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
      return;
    }

    $db = $classified->getTable()->getAdapter();
    $db->beginTransaction();

    try
    {
      $classified->delete();
      $db->commit();
    }

    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }

    $this->view->status = true;
    $this->view->message = Zend_Registry::get('Zend_Translate')->_('Your classified listing has been deleted.');
    if( !($page = $this->_getParam('page')) ) {
		  $message	=	'Your classified listing has been deleted.';
		   return $this->_forward('success', 'utility', 'core', array(
			'smoothboxClose' => true,
			'parentRedirect'  => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'manage'), 'classified_general', true),
			'messages'       => array($message)
		  ));
    } else {
	   $message	=	'Your classified listing has been deleted.';
       return $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => true,
        'parentRefresh'  => true,
        'messages'       => array($message)
      ));
    }
	
	/*return $this->_forward('success' ,'utility', 'core', array(
      'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'manage'), 'classified_general', true),
      'messages' => array($this->view->message)
    ));*/
  }
  
  public function closeAction()
  {
    if( !$this->_helper->requireUser()->isValid() ) return;

    $viewer = Engine_Api::_()->user()->getViewer();
    $classified = Engine_Api::_()->getItem('classified', $this->_getParam('classified_id'));
    if( !Engine_Api::_()->core()->hasSubject('classified') ) {
      Engine_Api::_()->core()->setSubject($classified);
    }
    $this->view->classified = $classified;

    // Check auth
    if( !$this->_helper->requireSubject()->isValid() ) {
      return;
    }
    if( !$this->_helper->requireAuth()->setAuthParams($classified, $viewer, 'edit')->isValid() ) {
      return;
    }

    // @todo convert this to post only

    $table = $classified->getTable();
    $db = $table->getAdapter();
    $db->beginTransaction();

    try
    {
      $classified->closed = $this->_getParam('closed');
      $classified->save();

      $db->commit();
    }

    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }
	 $message	=	($this->_getParam('closed') == 0)?'Job Opened Successfully.':'Job Closed Successfully.';
if( !($returnUrl = $this->_getParam('return_url')) ) {
     
       return $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => false,
        'parentRefresh'  => false,
        'messages'       => array($message)
      ));
    } else {

      //return $this->_helper->redirector->gotoRoute(array('controller' => 'index', 'action' => 'view','userredirect' => 'novalue'), 'classified_entry_view',true);
		return $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => true,
        'parentRefresh'  => true,
        'messages'       => array($message)
      ));
      //return $this->_redirect(urldecode($returnUrl), array('prependBase' => false));
    }
	
	/*if( !($page = $this->_getParam('page')) ) {
		
		  return $this->_helper->redirector->gotoRoute(array('action' => 'manage'), 'classified_general', true);
    } else {
	   $message	=	($this->_getParam('closed') == 0)?'Job Opened Successfully.':'Job Closed Successfully.';
       return $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => true,
        'parentRefresh'  => true,
        'messages'       => array($message)
      ));
    }*/

    /*if( !($returnUrl = $this->_getParam('return_url')) ) {
      return $this->_helper->redirector->gotoRoute(array('action' => 'manage'), 'classified_general', true);
    } else {
      return $this->_redirect(urldecode($returnUrl), array('prependBase' => false));
    }*/
  }
  
  public function successAction()
  {
    if( !$this->_helper->requireUser()->isValid() ) return;

    // Get navigation
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('classified_main', array(), 'classified_main_manage');
    

    $viewer = Engine_Api::_()->user()->getViewer();
    $this->view->classified = $classified = Engine_Api::_()->getItem('classified', $this->_getParam('classified_id'));

    if( $viewer->getIdentity() != $classified->owner_id )
    {
      return $this->_forward('requireauth', 'error', 'core');
    }

    if( $this->getRequest()->isPost() && $this->getRequest()->getPost('confirm') == true )
    {
      return $this->_redirect("classifieds/photo/upload/subject/classified_".$this->_getParam('classified_id'));
    }
  }
  
  
  
						/************** Producer's job title category start***************/
						
	public function parentAction()
	  {
		if( !$this->_helper->requireUser()->isValid() ) return;


 
		// Get navigation
		$profile_type	=	Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
		
		$is_company		=	Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
		
		 if(strtolower($profile_type) == 'talent'){
			 $menu_type	=	'user_edit';
		 }else if(strtolower($profile_type) == 'agency'){
			$menu_type	=	'custom_33'; 
		 }else{
			$menu_type	=	'custom_31'; 
			
			if( isset($is_company->value) && $is_company->value !="" )
			{

				//echo $is_company->value;
				$menu_type	=	'custom_34'; 
			}
		 }
		$this->view->navigation = Engine_Api::_()
		   ->getApi('menus', 'core')
		   ->getNavigation('user_edit');
		   
		$user = Engine_Api::_()->user()->getViewer();

		$viewer = Engine_Api::_()->user()->getViewer();
		$this->view->user_id = $user->getIdentity();
		$this->view->categories = $categories = Engine_Api::_()->getDbtable('parenttypes', 'classified')->getListAssoc($user);

	  }
	  
	  public function addParentAction()
	  {

	  	$table 		= Engine_Api::_()->getDbtable('parenttypes', 'classified'); /******* Table connection ********/
		$user 		= Engine_Api::_()->user()->getViewer();
		$user_id 	= $user->getIdentity();
		
		$parent_id 			= $this->getRequest()->getPost('parent_id');
		//$parent_title		= str_replace('"','',$this->getRequest()->getPost('parent_name')); 
		$parent_title		= addslashes($this->getRequest()->getPost('parent_name')); 
		//$parent_desc		=	str_replace('"','',$this->getRequest()->getPost('parent_desc'));
		$parent_desc		=	$this->getRequest()->getPost('parent_desc');
		if($parent_id > 0)
		{
				/********** Request Data array ***********/
			$data = array( 'parent_name' => $parent_title,
							'parent_desc' => $parent_desc,
						   'user_id' => $user_id);
			/******* Update Command ********/
			$qry = $table->update($data, array('parent_id=?'=>$parent_id) ); 
			$msg = "Record updated successfully.";
		}
		else
		{
			
			$data = array(
			'parent_name' => $parent_title,
			'parent_desc'=>	$parent_desc,
			'user_id' => $user_id);
			$qry = $table->insert($data);  /******** Insert new event in db *********/
			$projectID = Engine_Db_Table::getDefaultAdapter()->lastInsertId();
			if($projectID > 0)
			{
				$pdata = array( 'user_id' => $user_id,
						'project_id' =>$projectID,
						'permission' => 2,
						'shared_by' => $user_id);
					$user_friends 	= Engine_Api::_()->getDbtable('friendship', 'user')->setProjectsList($pdata);	
			}	
			$msg = "Record inserted successfully.";
		}
		echo "success::".$msg;
		/*if($qry)
		{
		echo "success::".$msg;
		
		}
		else
		{
			echo "fail::".$msg;
			
		}*/
		die();
	  }
	  
	  
	  public function removeParentAction()
	  {
	  	$table 		= Engine_Api::_()->getDbtable('parenttypes', 'classified'); /******* Table connection ********/
		$user = Engine_Api::_()->user()->getViewer();
		$user_id = $user->getIdentity();
		
		$parent_id 			= $this->getRequest()->getPost('parent_id');
	
		/******* Delete Command ********/
		$qry = $table->delete(array('parent_id=?'=>$parent_id) ); 
		
			
		if($qry)
		{
		Engine_Api::_()->getDbtable('friendship', 'user')->unsetProjectsList($parent_id);	
		echo "success::Record deleted successfully.";
		
		}
		else
		{
			echo "fail::There must be some issue in record delete";
			
		}
		die();
	  }
	  
	  
	   public function removeShareAction()
	  {
		 
		$viewer_instance	= Engine_Api::_()->user()->getViewer();  
	  	$table 		= Engine_Api::_()->getDbtable('parenttypes', 'classified'); /******* Table connection ********/
		$user = Engine_Api::_()->user()->getViewer();
		$user_id = $user->getIdentity();
		
		$data_id 			= $this->getRequest()->getPost('data_id');
		$project_id 			= $this->getRequest()->getPost('project_id');
	
		/******* Delete Command ********/
 		
		
		
		 
			
		if($project_id !="" && $data_id!="")
		{
		Engine_Api::_()->getDbtable('friendship', 'user')->unsetProjectsListFriend($data_id,$project_id);	
		
		 
		
		echo "success::Record deleted successfully.";
		
		}
		else
		{
			echo "fail::There must be some issue in record delete";
			
		}
		die();
	  }
	  
	  
	  public function removeSharePageAction()
	  {
		 
		 
		$viewer_instance	= Engine_Api::_()->user()->getViewer();  
	  	$table 		= Engine_Api::_()->getDbtable('parenttypes', 'classified'); /******* Table connection ********/
		$user = Engine_Api::_()->user()->getViewer();
		$user_id = $user->getIdentity();
		
		$data_id 			= $this->getRequest()->getPost('data_id');
		$page_id 			= $this->getRequest()->getPost('page_id');
		$page_id=(int)$page_id;
		
	
		/******* Delete Command ********/
 		
		
		
		 
			
		if($page_id !="" && $data_id!="")
		{
		Engine_Api::_()->getDbtable('friendship', 'user')->unsetPageListFriend($data_id,$page_id);	
		
 
		
		echo "success::Record deleted successfully.";
		
		}
		else
		{
			echo "fail::There must be some issue in record delete";
			
		}
		die();
	  }
	  
	  public function suggestAction()
	  {

    $viewer = Engine_Api::_()->user()->getViewer();
	 
    if( !$viewer->getIdentity() ) {
      $data = null;
    } else {
      $data = array();
      $table = Engine_Api::_()->getItemTable('user');
      
      $usersAllowed = Engine_Api::_()->getDbtable('permissions', 'authorization')->getAllowed('messages', $viewer->level_id, 'auth');
	  
	  $text = $this->_getParam('search', $this->_getParam('value'));
	   $friend_list=Engine_Api::_()->getDbtable('friendship', 'user')->getUserFriendsListByDisplay($viewer->getIdentity(),$text);
	   $data=array();
	   foreach ($friend_list  as $key=> $val)
	   {
		   $userDetail=Engine_Api::_()->getItem('user', $val['user_id']);
		   $data[] = array(
          'type' => 'user',
          'id' => $userDetail->getIdentity(),
          'guid' => $userDetail->getGuid(),
          'label' => $userDetail->getTitle(),
          'photo' => $this->view->itemPhoto($userDetail, 'thumb.icon'),
          'url' => $userDetail->getHref(),
        );
	   }
	   
	    
	   
	   
	  //$usersAllowed = "everyone";

      

      // first get friend lists created by the user
      $listTable = Engine_Api::_()->getItemTable('user_list');
      $lists = $listTable->fetchAll($listTable->select()->where('owner_id = ?', $viewer->getIdentity()));
      $listIds = array();
      foreach( $lists as $list ) {
        $listIds[] = $list->list_id;
        $listArray[$list->list_id] = $list->title;
      }

      // check if user has friend lists
      if( $listIds ) {
        // get list of friend list + friends in the list
        $listItemTable = Engine_Api::_()->getItemTable('user_list_item');
        $uName = Engine_Api::_()->getDbtable('users', 'user')->info('name');
        $iName  = $listItemTable->info('name');

        $listItemSelect = $listItemTable->select()
          ->setIntegrityCheck(false)
          ->from($iName, array($iName.'.listitem_id', $iName.'.list_id', $iName.'.child_id',$uName.'.displayname'))
          ->joinLeft($uName, "$iName.child_id = $uName.user_id")
          //->group("$iName.child_id")
          ->where('list_id IN(?)', $listIds);

        $listItems = $listItemTable->fetchAll($listItemSelect);

        $listsByUser = array();
        foreach( $listItems as $listItem ) {
          $listsByUser[$listItem->list_id][$listItem->user_id]= $listItem->displayname ;
        }
        
        foreach ($listArray as $key => $value){
          if (!empty($listsByUser[$key])){
            $data[] = array(
              'type' => 'list',
              'friends' => $listsByUser[$key],
              'label' => $value,
            );
          }
        }
      }
    }

    if( $this->_getParam('sendNow', true) ) {
      return $this->_helper->json($data);
    } else {
      $this->_helper->viewRenderer->setNoRender(true);
      $data = Zend_Json::encode($data);
      $this->getResponse()->setBody($data);
    }
  }
	 public function getFriendsAction()
	  {
	  
				$currnet_user 			= Engine_Api::_()->user()->getViewer()->getIdentity();
				$dateTime				= strtotime(date("d-m-Y"));
			
				$user_friends 			= Engine_Api::_()->getDbtable('friendship', 'user')->getUserFriendsList($currnet_user);
				
				
				$this->view->project_id  = $project_id  = (int) $this->_getParam('action_id', null);
				$shared_list = Engine_Api::_()->getDbtable('friendship', 'user')->getprojectSharedList($project_id);
				$this->view->shared_list = $shared_list;
				
				
				$this->view->friends = $user_friends;
				
			
	  }
	  
	  function getFollowPageAction()
	  {
		$this->view->project_id  = $project_id  = (int) $this->_getParam('action_id', null); 
	  }
	  
	  function getUnfollowPageAction()
	  {
		$this->view->project_id  = $project_id  = (int) $this->_getParam('action_id', null); 
	  }
	  
	  
	  
	  
	  
	  public function getFriendsSharedAction()
	  {
	  
	  
				$currnet_user 			= Engine_Api::_()->user()->getViewer()->getIdentity();
				
				$dateTime				= strtotime(date("d-m-Y"));
			
				$user_friends 			= Engine_Api::_()->getDbtable('friendship', 'user')->getUserFriendsList($currnet_user);
				
				
				$this->view->project_id  = $project_id  = (int) $this->_getParam('action_id', null);
				$shared_list = Engine_Api::_()->getDbtable('friendship', 'user')->getprojectSharedListWithOther($project_id,$currnet_user);
				
				$this->view->shared_list = $shared_list;
				 
				
				 
				 
				
			
	  }
	  
	  public function followPageAction()
	  {
	  
	 
	   $currnet_user 			= Engine_Api::_()->user()->getViewer()->getIdentity();
	   $this->view->project_id  = $project_id  = (int) $this->_getParam('project_id', null);
	 
	  
	  $friends = Engine_Api::_()->getDbtable('friendship', 'user')->followPage($currnet_user,$this->view->project_id);
	  
	   echo "success::added";
	   die();
	 
	 
	  
 	  }
	  
	   public function unfollowPageAction()
	  {
	  
	 
	 
	   $currnet_user 			= Engine_Api::_()->user()->getViewer()->getIdentity();
	   $this->view->project_id  = $project_id  = (int) $this->_getParam('project_id', null);
	  
	  
	  $friends = Engine_Api::_()->getDbtable('friendship', 'user')->removepageFollowed($currnet_user,$this->view->project_id);
	   echo "success::deleted";
	   die();
	   
	  
 	  }
  
  	 public function shareProjectAction()
	  {
		  
		   
	 
	 // $table 			= Engine_Api::_()->getDbtable('projects', 'user');/******* Table connection ********/
	 
	  $viewer 			= Engine_Api::_()->user()->getViewer()->getIdentity();/**** Current user id *********/
	  $viewer_instance	= Engine_Api::_()->user()->getViewer();
	  
		/**** Get Post Data *********/
 		$user_ids=explode(",",$this->getRequest()->getPost('toValues'));
 		$permission 	   = $this->getRequest()->getPost('permission'); // Empty set
		$project_id 	   = $this->getRequest()->getPost('project_id');
	
		
		foreach( $user_ids as $id )
		{
			$data = array( 'user_id' => $id,
					'project_id' =>$project_id,
					'permission' => $permission,
					'shared_by' => $viewer);
				 $user_friends 	= Engine_Api::_()->getDbtable('friendship', 'user')->setProjectsList($data);	
			
			
		$user = Engine_Api::_()->getItem('user', $id);
		$project = array();
	
		
		$project = Engine_Api::_()->getDbtable('parenttypes', 'classified')->getProject($project_id);
 		$classified = Engine_Api::_()->getItem('classified', $project_id);
		 
		//$action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer_instance, $classified, 'classified_apply');
		
		//Engine_Api::_()->getDbtable('pages', 'page')->sharePageNotification($id,$viewer,'project_shared');	
		
   /* Engine_Api::_()->getDbtable('pages', 'page')->sharePageNotification($id,$viewer,'project_shared',array(
        'url' => "/classified/index/parent",
        'jobname' => $project['parent_name'],
        'username' => $viewer_instance->getTitle(),
          'appurl' => $viewer_instance->getHref()
      ));*/
    $params= '{"url":"\/classified\/index\/parent","jobname":"'.$project['parent_name'].'"}';
     /* $arrayparam=array(
        'url' => "/classified/index/parent",
        'jobname' => $project['parent_name'],
        'username' => $viewer_instance->getTitle(),
          'appurl' => $viewer_instance->getHref()
      );*/
    $DB = Zend_Db_Table_Abstract::getDefaultAdapter();
    $data = array(
      'user_id' => $id,
      'subject_type' => 'user',
      'subject_id' => $viewer,
      'object_type'=> 'user',
      'object_id'=>$id,   
      'type'=>'project_shared',
      'read'=> 0,
      'mitigated'=>0,
      'params' => $params,
      'date'=>date("Y-m-d h:i:s")
      );
    $DB->insert('engine4_activity_notifications', $data);

    //Send mail for accept Start
              $mailType = 'notify_project_share';
                $mailParams = array(
                  'host' => $_SERVER['HTTP_HOST'],
                  'date' => time(),
                  'recipient_title' => $user->getTitle(),
                  'sender_title' => $viewer_instance->getTitle(), 
                  'object_title' => $project['parent_name'], 
                  //'object_description' => $classified->body,
                  //'object_link' => $classified->getHref(),       
                );      
          
    $send = Engine_Api::_()->getApi('mail', 'core')->sendSystem( $user->email, $mailType,$mailParams);

    // Send mail for accept End

		$serverUrlHelper = new Zend_View_Helper_ServerUrl();
		$appurl = $serverUrlHelper->serverUrl(
		$this->getHelper('url')->url(
			   array(
				   'module' => 'classifieds',
				   'controller' => 'application',
				   'action' => 'va',
				   'classified_id'	=>	$id,
				   'aid'	=>	$viewer
			   ),
		  null,   // the route 
		 true));
		 
		/*Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification($user, $viewer_instance, $classified, 'project_shared', array(
          'message' => $this->view->BBCode($classified->body), // @todo make sure this works
          'url' => $this->getRequest()->getServer('HTTP_REFERER'),
		  'jobname' => $classified->title,
		  'username' => $viewer_instance->getTitle(),
		  'appurl' => $appurl
        ));*/
		
		}
		
		
		
		
			/*
		Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification($user, $viewer_instance, '', 'project_shared', array(
			  'message' => $this->view->BBCode('Ajen test project'), // @todo make sure this works
			  'url' => $this->getRequest()->getServer('HTTP_REFERER'),
			  'jobname' => 'Ajen test project',
			  'username' => $viewer_instance->getTitle()
			));
			
*/
		
		echo "Project assigned successfully";
		
		die;
	
			
	  }
	  
	  
	  
	  public function sharePageAction()
	  {
		  
		 
		   
	 
	 // $table 			= Engine_Api::_()->getDbtable('projects', 'user');/******* Table connection ********/
	 
	  $viewer 			= Engine_Api::_()->user()->getViewer()->getIdentity();/**** Current user id *********/
	  $viewer_instance	= Engine_Api::_()->user()->getViewer();
	  
		/**** Get Post Data *********/
 		$user_ids=explode(",",$this->getRequest()->getPost('toValues'));
 		$permission 	   = 2;
		$page_id 	   = $this->getRequest()->getPost('page_id');
	
		
		foreach( $user_ids as $id )
		{
			$data = array( 'user_id' => $id,
					'page_id' =>$page_id,
					'permission' => $permission,
					'shared_by' => $viewer);
				$user_friends 	= Engine_Api::_()->getDbtable('pages', 'page')->setPagesList($data);
				
				  Engine_Api::_()->getDbtable('pages', 'page')->sharePageNotification($id,$viewer,'page_shared');	
				
				
 				
					
 		
		}
		
 		 
		
		echo "Page assigned successfully";
		
		die;
	
			
	  }
	  
	  public function getskillsAction(){
		  	$this->_helper->layout->disableLayout();
			$this->view->type = $type	=	$_POST['type']; 
			$main_skills	=	array();
			if($type == 1){
				$skills = Engine_Api::_()->getDbtable('castingskills', 'classified')->fetchAll();
				if($skills){
					foreach($skills as $skill){
						$skil['skill_name']	=	$skill->castingskill_name;
						$skil['skill_id']	=	$skill->castingskill_id;
						$main_skills[]	=	$skil;
					}	
				}
			}else if($type == 2){
				$skills = Engine_Api::_()->getDbtable('jobskills', 'classified')->fetchAll();
				if($skills){
					foreach($skills as $skill){
						$skil['skill_name']	=	$skill->jobskill_name;
						$skil['skill_id']	=	$skill->jobskill_id;
						$main_skills[]	=	$skil;
					}	
				}	
			} 
			$this->view->skills	=	$main_skills;
			
	  }
  public function jobsAction()
  {
   
    //$parent_id= $_GET['parent_id'];
    $parent_id = $this->_getParam('parent_id');
    $this->view->parent_id=$parent_id;
    if( !$this->_helper->requireUser()->isValid() ) return;
  
    $profile_type = Engine_Api::_()->getDbtable('users', 'user')->getUserProfileType(Engine_Api::_()->user()->getViewer()->getIdentity());
    
    $is_company   = Engine_Api::_()->getDbtable('users', 'user')->getUserCompany(Engine_Api::_()->user()->getViewer()->getIdentity());
    
    $viewer = Engine_Api::_()->user()->getViewer();
    
    //echo '<pre>';print_r($viewer);echo '</pre>';die;
    $this->view->user_id = $viewer->getIdentity();
    $jobs = $categories = Engine_Api::_()->getDbtable('parenttypes', 'classified')->getListAssoc($viewer);
   
    //echo '<pre>';
    //print_r($jobs);
    //die();
     if($_POST['getactions'])
     {
      $jobs = Engine_Api::_()->getDbtable('classifieds', 'classified')->getjobsList($parent_id);
        $this->view->jobs =$jobs;
        $this->view->classified_id=$classified_id=$_POST['jobs'];
        $this->view->action=$action=$_POST['actions'];
       
        $table = Engine_Api::_()->getDbtable('applys', 'classified'); 
        //$select = $table->select();
        $applyname = $table->info('name'); 
        
        $select = $table->select("$applyname.*")
            ->where("$applyname.classified_id = $classified_id", 1)
            ->where("$applyname.".$action, 1);
            
        $result = $table->fetchAll($select);
        /*echo '<pre>';
        print_r($result);
        die();*/
        if($result->count() > 0){
           $this->view->reslist = $result;
           return $view;
        }
        else{
          $this->view->reslist = "No applicants found.";
           return $view;
        }
     }
   else
   {
      $jobs = Engine_Api::_()->getDbtable('classifieds', 'classified')->getjobsList($parent_id);
      $this->view->jobs =$jobs;

    $jobids = Engine_Api::_()->getDbtable('classifieds', 'classified')->getjobIDsList($parent_id);

      $table = Engine_Api::_()->getDbtable('applys', 'classified'); 
        //$select = $table->select();
        $applyname = $table->info('name'); 
        
        $select = $table->select("$applyname.*")
            ->where("$applyname.classified_id IN($jobids)", 1);
        $result = $table->fetchAll($select);
        /*echo '<pre>';
        print_r($result);
        die();*/
        if($result->count() > 0){
           $this->view->reslist = $result;
           return $view;
        }
        else{
          $this->view->reslist = "No applicants found.";
           return $view;
        }

   }
  }
  
}