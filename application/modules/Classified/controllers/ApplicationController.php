<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: PhotoController.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_ApplicationController extends Core_Controller_Action_Standard
{
  public function init()
  {
		
  }
  
  // JOB APPLICATION ACCEPTED
  public function acceptAction()
  {		
		$classified_id = $_POST['cid'];
		$user_id = $_POST['user_id'];
		$viewer_instance	=	$viewer = Engine_Api::_()->user()->getViewer();
		if( !$viewer->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
  
		$id = $classified_id;
		$viewer = $viewer->getIdentity();
		$user = Engine_Api::_()->getItem('user', $user_id);
		//print_r($user['email']);
		//CHECK IF ALREADY ACCEPTED
		$table = Engine_Api::_()->getDbtable('applys', 'classified');	
		$applyname = $table->info('name'); 
		$select = $table->select("$applyname.*")
				->where("$applyname.classified_id = $id", 1)
				->where("$applyname.user_id = $viewer", 1)
				->where("$applyname.accept = $viewer", 1);
				
				
		$result = $table->fetchAll($select);
	
		if($result->count() > 0){
			 $message = "You have already selected this job.";
			 $this->_helper->json($message);
			 
		}else{		
			//UPDATE JOB APPLICATION TO BE ACCEPTED
			$DB = Zend_Db_Table_Abstract::getDefaultAdapter();
			$data = array('hold' => 0,'accept' => 1,'reject' => 0);

			$where = array(
				'user_id = ?' => $_POST['user_id'],
				'classified_id = ?' => $classified_id,
			);

			$DB->update('engine4_classified_applys', $data, $where);
			
			
			// Add activity and Notification
			$classified = Engine_Api::_()->getItem('classified', $classified_id);
						
			$serverUrlHelper = new Zend_View_Helper_ServerUrl();
			$joburl = $serverUrlHelper->serverUrl(
			$this->getHelper('url')->url(
				   array(
					   'module' => 'classifieds',
					   'controller' => 'index',
					   'action' => 'view',
					   'user_id'	=>	$viewer,
					   'classified_id'	=>	$classified_id,
					   'slug'=>$classified->getTitle()
				   ),
			  null,   // the route 
			 true));
			
			Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification($user, $viewer_instance, $classified, 'classified_accept', array(
			  'message' => $this->view->BBCode($classified->body), // @todo make sure this works
			  'url' => $classified->getHref(),
			  'jobname' => $classified->title,
			  'username' => $viewer_instance->getTitle(),
		  	  'appurl' => $viewer_instance->getHref()
			));
			
			//Send mail for accept Start
              $mailType = 'notify_job_accept';
                $mailParams = array(
                  'host' => $_SERVER['HTTP_HOST'],
                  'date' => time(),
                  'recipient_title' => $user->getTitle(),
                  'sender_title' => $viewer_instance->getTitle(), 
                  'object_title' => $classified->title, 
                  'object_description' => $classified->body,
                  //'object_link' => $classified->getHref(),       
                );      
          /*echo '<pre>';
          echo 'mail'.$user->email;
          print_r($mailParams);
          die();*/
                $send = Engine_Api::_()->getApi('mail', 'core')->sendSystem( $user->email, $mailType,$mailParams);

              // Send mail for accept End
			$message = "Applicant is selected for this job.";
			$this->_helper->json($message);
		}
		die;
		 //$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(TRUE);
	
  }
  
  public function rejectAction()
  {
		$viewer_instance	=	$viewer = Engine_Api::_()->user()->getViewer();
		if( !$viewer->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
  
		$viewer = $viewer->getIdentity();
		$DB = Zend_Db_Table_Abstract::getDefaultAdapter();
		$data = array('hold' => 0,'accept' => 0,'reject' => 1);
		$classified_id = $_POST['cid'];
		$user_id = $_POST['user_id'];
		$where = array(
			'user_id = ?' => $user_id,
			'classified_id = ?' => $classified_id,
		);

		$DB->update('engine4_classified_applys', $data, $where);
		
		// Add activity and Notification
			$classified = Engine_Api::_()->getItem('classified', $classified_id);
			$user = Engine_Api::_()->getItem('user', $user_id);
			
			$serverUrlHelper = new Zend_View_Helper_ServerUrl();
			$joburl = $serverUrlHelper->serverUrl(
			$this->getHelper('url')->url(
				   array(
					   'module' => 'classifieds',
					   'controller' => 'index',
					   'action' => 'view',
					   'user_id'	=>	$viewer,
					   'classified_id'	=>	$classified_id,
					   'slug'=>$classified->getTitle()
				   ),
			  null,   // the route 
			 true));
			
			Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification($user, $viewer_instance, $classified, 'classified_reject', array(
			  'message' => $this->view->BBCode($classified->body), // @todo make sure this works
			  'url' => $classified->getHref(),
			  'jobname' => $classified->title,
			  'username' => $viewer_instance->getTitle(),
		  	  'appurl' => $viewer_instance->getHref()
			));

			//Send mail for accept Start
              $mailType = 'notify_job_reject';
                $mailParams = array(
                  'host' => $_SERVER['HTTP_HOST'],
                  'date' => time(),
                  'recipient_title' => $user->getTitle(),
                  'sender_title' => $viewer_instance->getTitle(), 
                  'object_title' => $classified->title, 
                  'object_description' => $classified->body,
                 //'object_link' => $classified->getHref(),       
                );      
          /*echo '<pre>';
          echo 'mail'.$user->email;
          print_r($mailParams);
          die();*/
                $send = Engine_Api::_()->getApi('mail', 'core')->sendSystem( $user->email, $mailType,$mailParams);

              // Send mail for accept End
		$message = "Applicant is rejected for this job.";
		echo  json_encode($message);
		die;
  }
  
  
  public function holdAction()
  {
		$viewer_instance	=	$viewer = Engine_Api::_()->user()->getViewer();
		if( !$viewer->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
  
		$viewer = $viewer->getIdentity();
		$DB = Zend_Db_Table_Abstract::getDefaultAdapter();
		$data = array('hold' => 1,'accept' => 0,'reject' => 0);
		$classified_id = $_POST['cid'];
		$user_id = $_POST['user_id'];
		$where = array(
			'user_id = ?' => $user_id,
			'classified_id = ?' => $classified_id,
		);

		$DB->update('engine4_classified_applys', $data, $where);
		
		// Add activity and Notification
			$classified = Engine_Api::_()->getItem('classified', $classified_id);
			$user = Engine_Api::_()->getItem('user', $user_id);
			
			$serverUrlHelper = new Zend_View_Helper_ServerUrl();
			$joburl = $serverUrlHelper->serverUrl(
			$this->getHelper('url')->url(
				   array(
					   'module' => 'classifieds',
					   'controller' => 'index',
					   'action' => 'view',
					   'user_id'	=>	$viewer,
					   'classified_id'	=>	$classified_id,
					   'slug'=>$classified->getTitle()
				   ),
			  null,   // the route 
			 true));
			
			Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification($user, $viewer_instance, $classified, 'classified_hold', array(
			  'message' => $this->view->BBCode($classified->body), // @todo make sure this works
			  'url' => $classified->getHref(),
			  'jobname' => $classified->title,
			  'username' => $viewer_instance->getTitle(),
		  	  'appurl' => $viewer_instance->getHref()
			));

			//Send mail for accept Start
              $mailType = 'notify_job_sortlist';
                $mailParams = array(
                  'host' => $_SERVER['HTTP_HOST'],
                  'date' => time(),
                  'recipient_title' => $user->getTitle(),
                  'sender_title' => $viewer_instance->getTitle(), 
                  'object_title' => $classified->title, 
                  'object_description' => $classified->body,
                  //'object_link' => $classified->getHref(),       
                );      
          /*echo '<pre>';
          echo 'mail'.$user->email;
          print_r($mailParams);
          die();*/
                $send = Engine_Api::_()->getApi('mail', 'core')->sendSystem( $user->email, $mailType,$mailParams);

              // Send mail for accept End

		$message = "Applicant is shortlisted for this job.";
		echo  json_encode($message);
		
		die;
  }

	public function applyAction()
	{
	
	$output_dir = "public/temporary/";
	 
	if(isset($_FILES["myfile"]))
		{
		$ret = array();
		
		$error =$_FILES["myfile"]["error"];
		{
		
		if(!is_array($_FILES["myfile"]['name'])) //single file
		{
			$fileName = $_FILES["myfile"]["name"];
			$fileExt=end(explode(".",$fileName));
			$unique_key = substr(md5(rand(0, 10000000)), 0, 7);
 			$fileName=time()."".$unique_key.".".$fileExt;
 			 
			move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $fileName);
			 //echo "<br> Error: ".$_FILES["myfile"]["error"];
			 
				 $ret=$fileName;
		}
		else
		{
			$fileCount = count($_FILES["myfile"]['name']);
			  for($i=0; $i < $fileCount; $i++)
			  {
				$fileName = $_FILES["myfile"]["name"][$i];
				$fileName = $_FILES["myfile"]["name"][$i];
			    $fileExt=end(explode(".",$fileName));
				$unique_key = substr(md5(rand(0, 10000000)), 0, 7);
 			    $fileName=time()."".$unique_key.".".$fileExt;
				$ret=$fileName;
				move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName );
			  }
		
		}
		}
		echo $ret;
 		die();
 		}
 	}
  
  
   public function createAction()
  { 
         $viewer_instance	=	$viewer = Engine_Api::_()->user()->getViewer();
		if( !$viewer->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
  
		$id = $this->_getParam('classified_id'); 
		$this->view->classified_id=$id;
 		$viewer = $viewer->getIdentity();
		$this->view->viewer_id=$viewer;

  }
  
  public function applysaveAction()
  { 
  		
  		//echo "applysave"; die();
         $viewer_instance	=	$viewer = Engine_Api::_()->user()->getViewer();
		if( !$viewer->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
		$id = $this->_getParam('classified_id'); 
 		$applytype= $this->_getParam('apply');
		
		$viewer = $viewer->getIdentity();
		
		$files_name=$_POST['file_name'];
		$files_name=explode("||",$files_name);
		 
		 if($files_name){
			foreach($files_name as $key=> $val)
			{
				if($val!="")
				{
				rename("public/temporary/".$val."", "public/classified/portfolio/".$val."");
				Engine_Api::_()->getDbTable('applys', 'classified')->applyWithUpload($viewer,$id,$val);
				}
			}
		 }
		 
   
		$table = Engine_Api::_()->getDbtable('applys', 'classified');	
		//$select = $table->select();
		$applyname = $table->info('name'); 

		$select = $table->select("$applyname.*")
				->where("$applyname.classified_id = $id", 1)
				->where("$applyname.user_id = $viewer", 1);
				
				
		$result = $table->fetchAll($select);
		
		/*if($result->count() > 0){
			 $this->view->status_message = $msg	=	"You have already applied for this job.";
			return $this->_forward('success' ,'utility', 'core', array(
				'smoothboxClose' => true,
				'parentRefresh' => true,
				 'messages' => array($msg)
			));
			 return $view;
		}*/
	
		$arr = array
		 (
		   'user_id'=>$viewer,
		   'classified_id'=>$id,
		   'creation_date'=>date('Y-m-d H:i:s'),
		 );
			 
		$table->insert($arr);
		
		// Add activity and Notification
		$classified = Engine_Api::_()->getItem('classified', $id);
		$user = Engine_Api::_()->getItem('user', $classified->owner_id);
        
		$action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer_instance, $classified, 'classified_apply');
      if( $action != null ) {
        Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $classified);
      }
		
		/*$action	=	Engine_Api::_()->getDbtable('actions', 'activity')
            ->addActivity($viewer_instance, $user, 'apply_classified', "{item:$subject} applied for {item:$owner}'s {item:$object:classified listing}.");
		*/
    	
		$serverUrlHelper = new Zend_View_Helper_ServerUrl();
		$appurl = $serverUrlHelper->serverUrl(
		$this->getHelper('url')->url(
			   array(
				   'module' => 'classifieds',
				   'controller' => 'application',
				   'action' => 'va',
				   'classified_id'	=>	$id,
				   'aid'	=>	$viewer
			   ),
		  null,   // the route 
		 true));
		 
		 $joburl = $classified->getHref();
		
		Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification($user, $viewer_instance, $classified, 'classified_apply', array(
          'message' => $this->view->BBCode($classified->body), // @todo make sure this works
          'url' => $joburl,
		  'jobname' => $classified->title,
		  'username' => $viewer_instance->getTitle(),
		  'appurl' => $appurl
        ));
		
		
		//echo "success::You have successfully applied for the job.";
		//die();
		if($applytype == "searchapply")
	      {
	        $this->_helper->json('You have successfully applied for the job.');
	        //die();
	      }
	    else
	    {
			return $this->_forward('success' ,'utility', 'core', array(
	      	'smoothboxClose' => true,
			'parentRefresh' => true,
	     	 'messages' => array('You have successfully applied for the job.')
	    	));
	    }

  }
  
   public function applyremoveAction()
  { 
  		
  		//echo "applysave"; die();
         $viewer_instance	=	$viewer = Engine_Api::_()->user()->getViewer();
		if( !$viewer->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
		$id = $this->_getParam('classified_id'); 
		/*echo "id-".$id;
		die();*/
		$applytype= $this->_getParam('apply');		
		$viewer = $viewer->getIdentity();
		
		$user_apply = Engine_Api::_()->getDbTable('applys', 'classified');
              $condition = array(
                'classified_id = ?' => $id,
                'user_id = ?' => $viewer
              );
              
        $user_apply->delete($condition);

    // Add activity and Notification
	$classified = Engine_Api::_()->getItem('classified', $id);
	$user = Engine_Api::_()->getItem('user', $classified->owner_id);
    
	$action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer_instance, $classified, 'classified_unapply');
  if( $action != null ) {
    Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $classified);
  }

  $serverUrlHelper = new Zend_View_Helper_ServerUrl();
		$appurl = $serverUrlHelper->serverUrl(
		$this->getHelper('url')->url(
			   array(
				   'module' => 'classifieds',
				   'controller' => 'application',
				   'action' => 'va',
				   'classified_id'	=>	$id,
				   'aid'	=>	$viewer
			   ),
		  null,   // the route 
		 true));
		 
		 $joburl = $classified->getHref();
		
		Engine_Api::_()->getDbtable('notifications', 'activity')->addNotification($user, $viewer_instance, $classified, 'classified_unapply', array(
          'message' => $this->view->BBCode($classified->body), // @todo make sure this works
          'url' => $joburl,
		  'jobname' => $classified->title,
		  'username' => $viewer_instance->getTitle(),
		  'appurl' => $appurl
        ));
		if($applytype == "searchapply")
	      {
	        $this->_helper->json('You have successfully Un-apply this job.');
	        //die();
	      }
	    else
	    {
			return $this->_forward('success' ,'utility', 'core', array(
	      	'smoothboxClose' => true,
			'parentRefresh' => true,
	     	 'messages' => array('You have successfully Un-apply this job.')
	   		 ));
		}

  }
  //GET THE LIST OF ALL APPLICATNTS FOR THIS JOB
  public function listAction()
  {
		$viewer = Engine_Api::_()->user()->getViewer();
		if( !$viewer->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
		$id = $this->_getParam('classified_id');
	
		//echo $viewer->getUsername();die;
		
		$this->view->viewer_id	=	$viewer = $viewer->getIdentity();
		$this->view->permission = Engine_Api::_()->getDbtable('parenttypes', 'classified')->getPermission($id, $viewer);
		$this->view->jobname = Engine_Api::_()->getDbtable('classifieds', 'classified')->getjobNamebyId($id);
   
   
		$table = Engine_Api::_()->getDbtable('applys', 'classified');	
		//$select = $table->select();
		$applyname = $table->info('name'); 
		
		$select = $table->select("$applyname.*")
				->where("$applyname.classified_id = $id", 1);
				
		$result = $table->fetchAll($select);
		
		if($result->count() > 0){
			 $this->view->reslist = $result;
			 return $view;
		}
		else{
			$this->view->reslist = "No applicants found.";
			 return $view;
		}
	
  }
  
  
  //VIEW APPLICANT SECTION
  public function vaAction()
  {
	$cid = $this->_getParam('classified_id');
	$uid = $this->_getParam('aid');
    // Don't render this if not authorized
    $viewer = Engine_Api::_()->user()->getViewer();
	
	
    // if( !Engine_Api::_()->core()->hasSubject() ) {
         // return $this->setNoRender();
    // }
	
	// Get subject and check auth
   $subject = Engine_Api::_()->user()->getUser($uid);
	// if( !$subject->authorization()->isAllowed($viewer, 'view') ) {
		// return $this->setNoRender();
	// }
	//die;
	
	$this->view->subject = $subject;

	
	// Calculate viewer-subject relationship
    $usePrivacy = ($subject instanceof User_Model_User);
    if( $usePrivacy ) {
      $relationship = 'everyone';
      if( $viewer && $viewer->getIdentity() ) {
        if( $viewer->getIdentity() == $subject->getIdentity() ) {
          $relationship = 'self';
        } else if( $viewer->membership()->isMember($subject, true) ) {
          $relationship = 'friends';
        } else {
          $relationship = 'registered';
        }
      }
    }
	
	

	
	// Load fields view helpers
    $view = $this->view;
    $view->addHelperPath(APPLICATION_PATH . '/application/modules/Fields/View/Helper', 'Fields_View_Helper');
   
    // Values
    $this->view->fieldStructure = $fieldStructure = Engine_Api::_()->fields()->getFieldsStructurePartial($subject);

    if( count($fieldStructure) <= 1 ) { // @todo figure out right logic
         return $this->setNoRender();
       }
	   
	//print_r($fieldStructure);die;
   
    $valuesStructure = array();
    $valueCount = 0;
	
	$show_hidden = $viewer->getIdentity()
                 ? ($subject->getOwner()->isSelf($viewer) || 'admin' === Engine_Api::_()->getItem('authorization_level', $viewer->level_id)->type)
                 : false;
				 
	
		
	
	foreach( $fieldStructure as $map )
       {
			// Get field meta object
			$field = $map->getChild();
			$value = $field->getValue($subject);

		    if( !$field || $field->type == 'profile_type' ) continue;
		    if( !$field->display && !$show_hidden ) continue;
		    $isHidden = !$field->display;
		  
		    // Get first value object for reference
		   $firstValue = $value;
		    if( is_array($value) && !empty($value) ) {
		    	$firstValue = $value[0];
		    }
			
			
			// Evaluate privacy
			if( $usePrivacy && !empty($firstValue->privacy) && $relationship != 'self' ) {
				if( $firstValue->privacy == 'self' && $relationship != 'self' ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'friends' && ($relationship != 'friends' && $relationship != 'self') ) {
				  $isHidden = true; //continue;
				} else if( $firstValue->privacy == 'registered' && $relationship == 'everyone' ) {
				  $isHidden = true; //continue;
				}
			}
			
						
		    //print_r($firstValue);
		    $helper = new Fields_View_Helper_FieldValueLoop();
			$helper->view = Zend_Registry::get('Zend_View');
		
			$tmp = $helper->getFieldValueString($field, $value, $subject, $map, $partialStructure);
			
			if( $field->type != 'heading' && (!$isHidden || $show_hidden )) {
				$val[$field->label] = $tmp;
			}
		}
	//echo '<pre>';print_r($val);echo '</pre>';die;
	
	$table = Engine_Api::_()->getDbtable('applys', 'classified');	
	//$select = $table->select();
	$applyname = $table->info('name'); 
	
	$select = $table->select("$applyname.*")
			->where("$applyname.classified_id = $cid", 1)
			->where("$applyname.user_id = $uid", 1);
			
	$result = $table->fetchAll($select);
	
	$this->view->fieldStructure = $val;
	$this->view->classified_id = $cid;
	$this->view->user_id = $uid;
	$this->view->result	=	$result;
	$portfolio=Engine_Api::_()->getDbtable('classifieds', 'classified')->getClassifiedUserPortfolio($uid,$cid);
	$this->view->portfolio = $portfolio ;
	 
	
	
    $this->view->video_paginator =  Engine_Api::_()->video()->getVideosPaginator(array(
      'user_id' => $subject->getIdentity(),
      'status' => 1,
      'search' => 1
    ));
	
	$this->view->album_paginator  = Engine_Api::_()->getItemTable('album')
      ->getAlbumPaginator(array('owner' => $subject));
    
  }
  
  public function videoAction()
	  {
		$id = $this->_getParam('id'); 
		$this->view->video = $id;
	  }

 public function audioAction()
	  {
		$id = $this->_getParam('id'); 
		$this->view->audio = $id;
	  }


}