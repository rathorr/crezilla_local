<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: PhotoController.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_JobController extends Core_Controller_Action_Standard
{
  public function init()
  {
   
  }

  public function searchAction()
  {
  	$viewer = Engine_Api::_()->user()->getViewer();
  	 if( !Engine_Api::_()->user()->getViewer()->getIdentity() ) {
		  return $this->_helper->redirector->gotoRoute(array(), 'default', true);
		}
		

    $this->view->dasignations= Engine_Api::_()->getDbtable('dasignatedsubcategories', 'classified')->getsubcatjob();
	$this->view->skills = Engine_Api::_()->fields()->getTable('classified', 'options')->getClassifiedskills();
	
	$this->view->langoptions = Engine_Api::_()->fields()->getTable('classified', 'options')->getClassifiedLang();
	// total jobs
	$table1 = Engine_Api::_()->getItemTable('classified');
		$jobTableName1 = $table1->info('name');
		$jobselect1 = $table1->select()
		  ->from($jobTableName1)
		  ->where("{$jobTableName1}.closed = ?", 0)
		  ->order("{$jobTableName1}.classified_id DESC")
		  ->limit(20,0);
		 
		$jobpaginator1 =$jobelse= $table1->fetchAll($jobselect1);
		$this->view->ttjobs = $jobpaginator1;
		// total jobs end

  	if($_POST)
  	{
  			    $postdata	=	$_POST;
  		$table = Engine_Api::_()->getItemTable('classified');
    	$jobTableName = $table->info('name');
  		$jobselect = $table->select()
			  ->from($jobTableName)
			  ->joinLeft("engine4_classified_fields_search", "`engine4_classified_fields_search`.`item_id` = `{$jobTableName}`.`classified_id`", null)
			  ->where("{$jobTableName}.closed = ?", 0);
			
			  
			if($postdata['skill_id'] !=''){
				$jobselect->where("FIND_IN_SET(".$postdata['skill_id'].",engine4_classified_fields_search.field_76)");
			}
			
			if($postdata['designation_id'] !=''){
				$jobselect->where("{$jobTableName}.category_id = ?", $postdata['designation_id']);
			}
			
			if(trim($postdata['location']) != ''){
				$jobselect->where("engine4_classified_fields_search.field_14 LIKE ?", "%".$postdata['location']."%") ; 	
			}
			
			if($postdata['language_id'] !=''){
			$jobselect->where("FIND_IN_SET(".$postdata['language_id'].",engine4_classified_fields_search.field_73)");
			}
			
			if(trim($postdata['keyword']) !=''){
				$jobselect->where("({$jobTableName}.title LIKE ? || {$jobTableName}.body LIKE ?)", "%{$postdata['keyword']}%");
			}

			 $jobselect->order("{$jobTableName}.classified_id DESC")
							->limit(20,$page);
			$jobpaginator = $table->fetchAll($jobselect);
		 	$this->view->jobs = $jobpaginator;
  	}
  	else
  	{
  	// //Related jobs
  	
  		$userdcs  = Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategories($viewer->getIdentity());
		  $this->view->userdcs=$userdcs;
		  //echo '<pre>';print_r($userdcs); die();
		  $udcoptions = array();
		    if($userdcs){
		      foreach($userdcs as $key=>$udsc){
		        $udcoptions[$key] = $udsc['dc_primary_id']; 
		      }
		    }
		 	$desigs=implode(',', $udcoptions);
		 	
		$table = Engine_Api::_()->getItemTable('classified');
		$jobTableName = $table->info('name');
		$jobselect = $table->select()
		  ->from($jobTableName)
		  ->where("{$jobTableName}.closed = ?", 0);
		  if($desigs != '')
		  {
		  	$jobselect->where("{$jobTableName}.category_id IN($desigs)");
		  }
		  else
		  {
		  	$jobselect->where("{$jobTableName}.category_id IN(0)");
		  }
		  		  
		  $jobselect->order("{$jobTableName}.classified_id DESC");
		  $jobselect->limit(20,0);
		$jobpaginator = $table->fetchAll($jobselect);
		if(count($jobpaginator) != 0)
		{ 
			$this->view->jobs = $jobpaginator;
		}
		else
		{
			$this->view->jobs = $jobelse;
		}
	}
		$this->view->userid=$viewer->getIdentity();
  }
  
  public function getjobresultsAction()
  {		
  	
  		$viewer = Engine_Api::_()->user()->getViewer();
  		$userdcs  = Engine_Api::_()->getDbtable('dasignatedcategories', 'user')->userDasignatedcategories($viewer->getIdentity());
		  $this->view->userdcs=$userdcs;
		  $udcoptions = array();
		    if($userdcs){
		      foreach($userdcs as $key=>$udsc){
		        $udcoptions[$key] = $udsc['dc_primary_id']; 
		      }
		    }
		 	$desigs=implode(',', $udcoptions);


	    $postdata	=	$_POST;
	  
	    $this->_helper->layout->disableLayout();
		$this->view->page		=	$page = $_POST['list_count'];
		
		$table = Engine_Api::_()->getItemTable('classified');
    	$jobTableName = $table->info('name');
		if($_POST['job_search_status']=='Relevant')
		{
			$table = Engine_Api::_()->getItemTable('classified');
			$jobTableName = $table->info('name');
			$jobselect = $table->select()
			  ->from($jobTableName)
			  ->where("{$jobTableName}.closed = ?", 0);
			  if($desigs != '')
			  {
			  	$jobselect->where("{$jobTableName}.category_id IN($desigs)");
			  }
			  else
			  {
			  	$jobselect->where("{$jobTableName}.category_id IN(0)");
			  }
			  
			  $jobselect->order("{$jobTableName}.classified_id DESC");
			  $jobselect->limit(20,0);
		}
		elseif($_POST['job_search_status']=='Trending')
		{
			$jobselect = $table->select()
			  ->from($jobTableName)
			  ->where("{$jobTableName}.closed = ?", 0)
			  ->order("{$jobTableName}.view_count DESC")
			  ->limit(20,0);
		}
		elseif($_POST['job_search_status']=='Latest')
		{
			$jobselect = $table->select()
			  ->from($jobTableName)
			  ->where("{$jobTableName}.closed = ?", 0)
			  ->order("{$jobTableName}.classified_id DESC")
			  ->limit(20,0);
		}
		else
		{	

			 $jobselect = $table->select()
			  ->from($jobTableName)
			  ->joinLeft("engine4_classified_fields_search", "`engine4_classified_fields_search`.`item_id` = `{$jobTableName}`.`classified_id`", null)
			  ->where("{$jobTableName}.closed = ?", 0);
			
			  
			if($postdata['skill_id'] !=''){
				$jobselect->where("FIND_IN_SET(".$postdata['skill_id'].",engine4_classified_fields_search.field_76)");
			}
			
			if($postdata['designation_id'] !=''){
				$jobselect->where("{$jobTableName}.category_id = ?", $postdata['designation_id']);
			}
			
			if(trim($postdata['location']) != ''){
				$jobselect->where("engine4_classified_fields_search.field_14 LIKE ?", "%".$postdata['location']."%") ; 	
			}
			
			if($postdata['language_id'] !=''){
			$jobselect->where("FIND_IN_SET(".$postdata['language_id'].",engine4_classified_fields_search.field_73)");
			}
			
			if(trim($postdata['keyword']) !=''){
				$jobselect->where("({$jobTableName}.title LIKE ? || {$jobTableName}.body LIKE ?)", "%{$postdata['keyword']}%");
			}

			 $jobselect->order("{$jobTableName}.classified_id DESC")
							->limit(20,$page);
		}
		
		
		 $jobpaginator = $table->fetchAll($jobselect);
		 $this->view->jobs = $jobpaginator;
		 $this->view->userid=$viewer->getIdentity();
			
  }
  
  
 

}