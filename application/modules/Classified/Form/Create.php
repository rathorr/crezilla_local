<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Create.php 9802 2012-10-20 16:56:13Z pamela $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Form_Create extends Engine_Form
{
  public function init()
  {
    $this->setTitle('POST NEW JOB')
      ->setAttrib('name', 'classifieds_create')
	  ->setAttrib('id', 'classifieds_create');
	  
	$user = Engine_Api::_()->user()->getViewer();
    $user_level = Engine_Api::_()->user()->getViewer()->level_id;
	
	$pages	=	Engine_Api::_()->getDbtable('pages', 'page')->getUserPagesAll($user->getIdentity());
	
 
	if($pages){
		$pageoptions	=	array();
		//$pageoptions['']	=	'Select Company';
    $pageoptions['']  = 'Individual';
		foreach($pages as $page){
			$pageoptions[$page['page_id']]	=	$page['title'];	
		}
	}
	
 	
	if( $pages && !$this->_item) {
      $this->addElement('Select', 'page_id', array(
        'label' => 'Posting As',
        'title'=>"Post a Job in your own Name or as a Page you have access to!",
        'multiOptions' => $pageoptions,
		
      ));
	}
	
    // prepare categories
    $categories = Engine_Api::_()->getDbtable('parenttypes', 'classified')->getParentAssoc($user);
  
  	if( !$this->_item ) {
      $this->addElement('Select', 'project_id', array(
        'label' => 'Project',
        'title'=>"Select an Existing Project or Create a new Project!",
		'required' => true,
        'multiOptions' => $categories,
		'onchange'	=>	'showhideprobox();',
      ));
	  
	  $this->addElement('Text', 'pro_title', array(
		  'label' => 'Project Title',
		  'placeholder'	=>	'Enter project Name',
      'title'=>"Give your Project a Title!",
		  'allowEmpty' => false,
		  'filters' => array(
			'StripTags',
			new Engine_Filter_Censor(),
		  ),
		));
		
		$this->addElement('Textarea', 'pro_desc', array(
		  'label' => 'Project Description',
		  'placeholder'	=>	'Enter project description',
      'title'=>"Give your Project a Short Description!",
		  'allowEmpty' => false,
		  'filters' => array(
			'StripTags',
			new Engine_Filter_Censor(),
		  ),
		));
	}
	
	//if(count($categories)<2){
		/*$text = new Zend_Form_Element_Text('descCharsRemaining');
		$textval	=	'First you need to create project for posting a new job. <a href="'.Zend_Controller_Front::getInstance()->getRouter()->assemble(array('module' => 'classified', 'controller' => 'index', 'action' => 'parent'), 'default', true).'">Click here</a> to create a project.';
		$text->setValue($textval)
			 ->helper = 'formNote';
	
		$this->addElement($text);*/
		
		
	//}
	
	/*$this->addElement('Text', 'pro_title', array(
      'label' => 'Project Title',
	  'placeholder'	=>	'Enter project Name',
      'allowEmpty' => false,
      //'required' => true,
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
      ),
    ));
	
	$this->addElement('Textarea', 'pro_desc', array(
      'label' => 'Project Description',
	  'placeholder'	=>	'Enter project description',
      'allowEmpty' => false,
      //'required' => true,
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
      ),
    ));*/
	
	
	/* $categories2 = Engine_Api::_()->getDbtable('categories', 'classified')->getCategoriesAssoc();
	
	 
  
       $this->addElement('Select', 'category_id', array(
        'label' => 'Category',
		'required' => true,
        'multiOptions' => $categories2,
      ));
   */
  	
    
    
   
    $this->addElement('Text', 'title', array(
      'label' => 'Job Title',
	  'placeholder'	=>	'Headline( eg Female Lead actor required for a feature film)',
    'title'=>"Give your Job Post a Catchy Title!",
      'allowEmpty' => false,
      'required' => true,
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
        new Engine_Filter_StringLength(array('max' => '63')),
      ),
    ));

    

    // init to
   /* $this->addElement('Text', 'tags', array(
      'label' => 'Tags',
      'autocomplete' => 'off',
	  'placeholder'	=>	'eg actor, signer, dance etc.',
      //'description' => 'Separate tags with commas.',
      'filters' => array(
        new Engine_Filter_Censor(),
        new Engine_Filter_HtmlSpecialChars(),
      ),
    ));
    $this->tags->getDecorator("Description")->setOption("placement", "append");*/


    // Element: description
    //$allowed_html = Engine_Api::_()->authorization()->getPermission($user_level, 'blog', 'auth_html');
//    $this->addElement('TinyMce', 'body', array(
//      'label' => 'About the Job',
//      /*'disableLoadDefaultDecorators' => true,*/
//	  'placeholder'	=>	"Enter detail about the job, and the talent you're looking for",
//      'allowEmpty' => false,
//      /*'decorators' => array(
//        'ViewHelper'
//      ),*/
//      'editorOptions' => array(
//        'upload_url' => $upload_url,
//        'html' => (bool) $allowed_html,
//      ),
//      'filters' => array(
//        new Engine_Filter_Censor(),
//        new Engine_Filter_Html(array('AllowedTags'=>$allowed_html))),
//    ));  
	
	/*$this->addElement('TinyMce', 'body', array(
      'label' => 'Description of Job',
      'title'=>"Help potential recruits and service providers understand what you're looking for by providing a detailed description!",
    ));

    $params = array(
      'mode' => 'exact',
      'elements' => 'body',
      'theme_advanced_buttons1' => array(
        'bold', 'italic', 'underline', 'strikethrough', '|',
        'justifyleft', 'justifycenter', 'justifyright', 'justifyfull', '|',
        'bullist', 'numlist', '|',
        'undo', 'redo', '|',
        'sub', 'sup', '|',
        'forecolor', 'forecolorpicker', 'backcolor', 'backcolorpicker', '|'
      ),
      'theme_advanced_buttons2' => array(
        'newdocument', 'code', 'image', 'media', 'preview', 'fullscreen', '|',
        'link', 'unlink', 'anchor', 'charmap', 'cleanup', 'hr', 'removeformat', 'blockquote', 'separator', 'outdent', 'indent', '|',
        'selectall', 'advimage'),
      'theme_advanced_buttons3' => array('formatselect', 'fontselect', 'fontsizeselect', 'styleselectchar', '|', 'table', '|'),
    );

    $this->getView()->getHelper('TinyMce')->setOptions($params);*/
	
    /*$this->addElement('Textarea', 'body', array(
      'label' => 'Job Description',
      'placeholder' =>  'Enter job description',
      'title'=>"Give your Job a Short Description!",
      'allowEmpty' => false,
      'filters' => array(
      'StripTags',
      new Engine_Filter_Censor(),
      ),
    ));*/

    // Element: upload photo
    /*$allowed_upload = Engine_Api::_()->authorization()->getPermission($user_level, 'classified', 'photo');
    if( $allowed_upload ) {
      $this->addElement('File', 'photo', array(
        'label' => 'Display Image'
      ));
      $this->photo->addValidator('Extension', false, 'jpg,png,gif,jpeg');
    }  */
  	
    // Add subforms
    if( !$this->_item ) {
      $customFields = new Classified_Form_Custom_Fields();
    } else {
      $customFields = new Classified_Form_Custom_Fields(array(
        'item' => $this->getItem()
      ));
    }
    if( get_class($this) == 'Classified_Form_Create' ) {
      $customFields->setIsCreation(true);
    }

    $this->addSubForms(array(
      'fields' => $customFields
    ));

    // Privacy
    $availableLabels = array(
      'everyone'            => 'Everyone',
      'registered'          => 'All Registered Members',
      'owner_network'       => 'Friends and Networks',
      'owner_member_member' => 'Friends of Friends',
      'owner_member'        => 'Friends Only',
      'owner'               => 'Just Me',
    );

    // View
    $viewOptions = (array) Engine_Api::_()->authorization()->getAdapter('levels')->getAllowed('classified', $user, 'auth_view');
    $viewOptions = array_intersect_key($availableLabels, array_flip($viewOptions));

    if( !empty($viewOptions) && count($viewOptions) >= 1 ) {
      // Make a hidden field
      if(count($viewOptions) == 1) {
        $this->addElement('hidden', 'auth_view', array('value' => key($viewOptions)));
      // Make select box
      } else {
        $this->addElement('Select', 'auth_view', array(
            'label' => 'Privacy',
            'description' => 'Who may see this classified listing?',
            'multiOptions' => $viewOptions,
            'value' => key($viewOptions),
        ));
        $this->auth_view->getDecorator('Description')->setOption('placement', 'append');
      }
    }

    // Comment
    $commentOptions = (array) Engine_Api::_()->authorization()->getAdapter('levels')->getAllowed('classified', $user, 'auth_comment');
    $commentOptions = array_intersect_key($availableLabels, array_flip($commentOptions));

    if( !empty($commentOptions) && count($commentOptions) >= 1 ) {
      // Make a hidden field
      if(count($commentOptions) == 1) {
        $this->addElement('hidden', 'auth_comment', array('value' => key($commentOptions)));
      // Make select box
      } else {
        $this->addElement('Select', 'auth_comment', array(
            'label' => 'Comment Privacy',
            'description' => 'Who may post comments on this classified listing?',
            'multiOptions' => $commentOptions,
            'value' => key($commentOptions),
        ));
        $this->auth_comment->getDecorator('Description')->setOption('placement', 'append');
      }
    }
    
//    $this->addElement('Hash', 'token', array(
//      'salt' => 'classifieds-' . crc32(__FILE__)
//    ));


	// BROWSE SCRIPT BUTTON
	$this->addElement('Text', 'script_title', array(
      'label' => 'Additional Information',
	  'placeholder'	=>	'Title( eg script for job)',
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
        new Engine_Filter_StringLength(array('max' => '63')),
      ),
    ));
	
	
	$this->addElement('File', 'script', array(
      'label'	=>	'',
	  'validators' => array(
       // array('Count', false, 1),
        // array('Size', false, 612000),
        array('Extension', false, 'pdf,docx,doc'),
      ),
    ));

      $this->addElement('hidden', 'fields_0_0_61_id', array(
        'label' => 'Code',
      'class' => 'hide_box',   
    'id' => 'fields_0_0_61_id',
      ));

    // Element: execute
    $this->addElement('Button', 'execute', array(
      'label' => 'SAVE CHANGES',
      'type' => 'button',
	  'onclick'	=>	'validatevlassified()',
      'ignore' => true,
      'decorators' => array(
        'ViewHelper',
      ),
    ));

    // Element: cancel
    $this->addElement('Cancel', 'cancel', array(
      'label' => 'CANCEL',
      'link' => true,
      /*'prependText' => ' or ',*/
      'href' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'manage'), 'classified_general', true),
      'onclick' => '',
      'decorators' => array(
        'ViewHelper',
      ),
    ));

    // DisplayGroup: buttons
    $this->addDisplayGroup(array(
      'execute',
      'cancel',
    ), 'buttons', array(
      'decorators' => array(
        'FormElements',
        'DivDivDivWrapper'
      ),
    ));
  }
  

}