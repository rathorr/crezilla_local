<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Category.php 9747 2012-07-26 02:08:08Z john $
 * @author     Jung
 */

/**
 * @category   Application_Extensions
 * @package    Classified
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Classified_Form_Admin_Skill extends Engine_Form
{
  protected $_field;

  public function init()
  {
    $this->setMethod('post');

    /*
    $type = new Zend_Form_Element_Hidden('type');
    $type->setValue('heading');
    */

    $label = new Zend_Form_Element_Text('label');
    $label->setLabel('Skill Title')
      ->addValidator('NotEmpty')
      ->setRequired(true)
      ->setAttrib('class', 'text');
	  
	$options	=	array();
	$options['']	=	'Select Type';
	$options['1']	=	'Casting';
	$options['2']	=	'Services';
	
	$label = new Zend_Form_Element_Text('label');
    $this->addElement('Select', 'type', array(
        'label' => 'Skill type',
		'required' => true,
        'multiOptions' => $options,
		
      ));

    $id = new Zend_Form_Element_Hidden('id');

    $this->addElements(array(
      //$type,
      $label,
      $id
    ));

    // Buttons
    $this->addElement('Button', 'submit', array(
      'label' => 'Add Skill',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));

    /*$this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'prependText' => ' or ',
      'href' => '',
      'onClick'=> 'javascript:parent.Smoothbox.close();',
      'decorators' => array(
        'ViewHelper'
      )
    ));*/
    $this->addDisplayGroup(array('submit'), 'buttons');
    $button_group = $this->getDisplayGroup('buttons');
  }

  public function setField($skill)
  {
    $this->_field = $skill;

    // Set up elements
    //$this->removeElement('type');
    $this->label->setValue($skill->skill_name);
    $this->id->setValue($skill->skill_id);
	$this->type->setValue($skill->type);
    $this->submit->setLabel('Edit Skill');

    // @todo add the rest of the parameters
  }
}