<?php
/**
 * SocialEngine
 *
 * @category   Application_Widget
 * @package    Rss
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     John
 */
 
return array(
  'package' => array(
    'type' => 'widget',
    'name' => 'landingpage',
    'version' => '4.2.8',
    'revision' => '$Revision: 9808 $',
    'path' => 'application/widgets/rss',
    'repository' => 'socialengine.com',
    'title' => 'Landing Page',
    'description' => 'Landing Page Redesign.',
    'author' => 'Webligo Developments',
    'changeLog' => array(
      '4.1.8' => array(
        'Controller.php' => 'Added caching support',
        'index.tpl' => 'Added caching support',
        'manifest.php' => 'Incremented version',
      ),
      '4.0.3' => array(
        'Controller.php' => 'Fixed issue with getting link',
        'index.tpl' => 'Fixed issue with getting link; added link enabling; added option to not strip HTML',
        'manifest.php' => 'Incremented version; added option to not strip HTML',
      ),
      '4.0.2' => array(
        'index.tpl' => 'Added styles',
        'manifest.php' => 'Incremented version',
      ),
    ),
    'directories' => array(
      'application/widgets/landingpage',
    ),
  ),

  // Backwards compatibility
  'type' => 'widget',
  'name' => 'landingpage',
  'version' => '4.0.2',
  'revision' => '$Revision: 9808 $',
  'title' => 'Landing Page',
  'description' => 'Landing Page Redesign.',
  'category' => 'Widgets',
  
  
) ?>